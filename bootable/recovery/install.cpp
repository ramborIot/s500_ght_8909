/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#include "common.h"
#include "install.h"
#include "mincrypt/rsa.h"
#include "minui/minui.h"
#include "minzip/SysUtil.h"
#include "minzip/Zip.h"
#include "mtdutils/mounts.h"
#include "mtdutils/mtdutils.h"
#include "roots.h"
#include "verifier.h"
#include "ui.h"

extern RecoveryUI* ui;

#define ASSUMED_UPDATE_BINARY_NAME  "META-INF/com/google/android/update-binary"
#define PUBLIC_KEYS_FILE "/res/keys"

// Default allocation of progress bar segments to operations
static const int VERIFICATION_PROGRESS_TIME = 60;
/*Add by chenyb for verify FIBOCOM string in 20161107 BEGIN*/
static const int READ_MACHINE_SBL1 = 0;
static const int READ_PACKAGE_SBL1 = 1;
static const char fibocom[33] = "OEM_IMAGE_VERSION_STRING=FIBOCOM";
/*Add by chenyb for verify FIBOCOM string in 20161107 END*/
static const float VERIFICATION_PROGRESS_FRACTION = 0.25;
static const float DEFAULT_FILES_PROGRESS_FRACTION = 0.4;
static const float DEFAULT_IMAGE_PROGRESS_FRACTION = 0.1;

/*Add by chenyb for verify FIBOCOM string in 20161107 BEGIN*/
bool 
read_sbl1_fibocom(ZipArchive *zip, const int type) {
    const char* sbl1;
    if (type == READ_MACHINE_SBL1) {
	    sbl1 = "/dev/block/platform/7824900.sdhci/by-name/sbl1";
    } else {
	    //Read sbl1.mbn in the ota package
	    const ZipEntry* sbl1_entry =
	            mzFindZipEntry(zip, "sbl1.mbn");
	    if (sbl1_entry == NULL) {
	        mzCloseZipArchive(zip);
		 ui->Print("No sbl1.mbn in the package!\n");
	        return false;
	    }
	    sbl1 = "/tmp/sbl1.mbn";
	    unlink(sbl1);
	    int fd2 = creat(sbl1, 0755);
	    if (fd2 < 0) {
	        mzCloseZipArchive(zip);
	        ui->Print("/tmp/sbl2.mbn can't make!\n");
	        return false;
	    }
	    bool ok1 = mzExtractZipEntryToFile(zip, sbl1_entry, fd2);
	    close(fd2);
	    //mzCloseZipArchive(zip);

	    if (!ok1) {
	        ui->Print("Can't extract zip file to sbl1_entry!\n");
	        return false;
	    }
    }
    FILE* fp = fopen(sbl1, "rb");
    if (fp == NULL) {
	 ui->Print("sbl1.mbn is null!\n");
	 return false;
    }
    // obtain file size:
    fseek (fp , 0 , SEEK_END);
    long lSize = ftell (fp);
    rewind (fp);

    // allocate memory to contain the whole file:
    char * buffer1 = (char*) malloc (sizeof(char)*lSize);
    if (buffer1 == NULL) {
    	 ui->Print("Memory error\n");
        return false;
    }

    // copy the file into the buffer:
    size_t result = fread (buffer1, 1, lSize, fp);
    if (result != lSize) {
	 ui->Print("Read sbl1.mbn error\n");
        return false;
    }
    // the whole file is now loaded in the memory buffer.

    bool verify = false;
    /*if (strstr(buffer1, fibocom) != NULL) {
	 verify = true;
	 ui->Print("strstr Package matches!\n");
    }*/
    for (int i=0;i<lSize-33;i++) {
	 if (fibocom[0] == buffer1[i] && fibocom[1] == buffer1[i+1] && fibocom[2] == buffer1[i+2] && fibocom[3] == buffer1[i+3] && fibocom[4] == buffer1[i+4] &&
	 	fibocom[5] == buffer1[i+5] && fibocom[6] == buffer1[i+6] && fibocom[7] == buffer1[i+7] && fibocom[8] == buffer1[i+8] && fibocom[9] == buffer1[i+9] &&
	 	fibocom[10] == buffer1[i+10] && fibocom[11] == buffer1[i+11] && fibocom[12] == buffer1[i+12] && fibocom[13] == buffer1[i+13] && fibocom[14] == buffer1[i+14] &&
	 	fibocom[15] == buffer1[i+15] && fibocom[16] == buffer1[i+16] && fibocom[17] == buffer1[i+17] && fibocom[18] == buffer1[i+18] && fibocom[19] == buffer1[i+19] &&
	 	fibocom[20] == buffer1[i+20] && fibocom[21] == buffer1[i+21] && fibocom[22] == buffer1[i+22] && fibocom[23] == buffer1[i+23] && fibocom[24] == buffer1[i+24] &&
	 	fibocom[25] == buffer1[i+25] && fibocom[26] == buffer1[i+26] && fibocom[27] == buffer1[i+27] && fibocom[28] == buffer1[i+28] && fibocom[29] == buffer1[i+29] &&
	 	fibocom[30] == buffer1[i+30] && fibocom[31] == buffer1[i+31]) {
		verify = true;
	 }
    }

    fclose (fp);
    free (buffer1);
 
    if (verify) {
    	 return true;
    }

    return false;
}
/*Add by chenyb for verify FIBOCOM string in 20161107 END*/

// If the package contains an update binary, extract it and run it.
static int
try_update_binary(const char *path, ZipArchive *zip, int* wipe_cache) {
	
#if 0   
   /*Add by chenyb for verify FIBOCOM string in 20161107 BEGIN*/
    if (read_sbl1_fibocom(zip, READ_MACHINE_SBL1) && read_sbl1_fibocom(zip, READ_PACKAGE_SBL1)) {
	 ui->Print("Package matches!\n");
    } else {
    	 ui->Print("Package mismatches!\n");
	 return INSTALL_CORRUPT;
    }
	eeeee
    /*Add by chenyb for verify FIBOCOM string in 20161107 END*/
#endif

    const ZipEntry* binary_entry =
            mzFindZipEntry(zip, ASSUMED_UPDATE_BINARY_NAME);
    if (binary_entry == NULL) {
        mzCloseZipArchive(zip);
        return INSTALL_CORRUPT;
    }
    const char* binary = "/tmp/update_binary";
    unlink(binary);
    int fd = creat(binary, 0755);
    if (fd < 0) {
        mzCloseZipArchive(zip);
        LOGE("Can't make %s\n", binary);
        return INSTALL_ERROR;
    }
    bool ok = mzExtractZipEntryToFile(zip, binary_entry, fd);
    close(fd);
    mzCloseZipArchive(zip);

    if (!ok) {
        LOGE("Can't copy %s\n", ASSUMED_UPDATE_BINARY_NAME);
        return INSTALL_ERROR;
    }

    int pipefd[2];
    pipe(pipefd);

    // When executing the update binary contained in the package, the
    // arguments passed are:
    //
    //   - the version number for this interface
    //
    //   - an fd to which the program can write in order to update the
    //     progress bar.  The program can write single-line commands:
    //
    //        progress <frac> <secs>
    //            fill up the next <frac> part of of the progress bar
    //            over <secs> seconds.  If <secs> is zero, use
    //            set_progress commands to manually control the
    //            progress of this segment of the bar
    //
    //        set_progress <frac>
    //            <frac> should be between 0.0 and 1.0; sets the
    //            progress bar within the segment defined by the most
    //            recent progress command.
    //
    //        firmware <"hboot"|"radio"> <filename>
    //            arrange to install the contents of <filename> in the
    //            given partition on reboot.
    //
    //            (API v2: <filename> may start with "PACKAGE:" to
    //            indicate taking a file from the OTA package.)
    //
    //            (API v3: this command no longer exists.)
    //
    //        ui_print <string>
    //            display <string> on the screen.
    //
    //   - the name of the package zip file.
    //

    const char** args = (const char**)malloc(sizeof(char*) * 5);
    args[0] = binary;
    args[1] = EXPAND(RECOVERY_API_VERSION);   // defined in Android.mk
    char* temp = (char*)malloc(10);
    sprintf(temp, "%d", pipefd[1]);
    args[2] = temp;
    args[3] = (char*)path;
    args[4] = NULL;

    pid_t pid = fork();
    if (pid == 0) {
        umask(022);
        close(pipefd[0]);
        execv(binary, (char* const*)args);
        fprintf(stdout, "E:Can't run %s (%s)\n", binary, strerror(errno));
        _exit(-1);
    }
    close(pipefd[1]);

    *wipe_cache = 0;

    char buffer[1024];
    FILE* from_child = fdopen(pipefd[0], "r");
    while (fgets(buffer, sizeof(buffer), from_child) != NULL) {
        char* command = strtok(buffer, " \n");
        if (command == NULL) {
            continue;
        } else if (strcmp(command, "progress") == 0) {
            char* fraction_s = strtok(NULL, " \n");
            char* seconds_s = strtok(NULL, " \n");

            float fraction = strtof(fraction_s, NULL);
            int seconds = strtol(seconds_s, NULL, 10);

            ui->ShowProgress(fraction * (1-VERIFICATION_PROGRESS_FRACTION), seconds);
        } else if (strcmp(command, "set_progress") == 0) {
            char* fraction_s = strtok(NULL, " \n");
            float fraction = strtof(fraction_s, NULL);
            ui->SetProgress(fraction);
        } else if (strcmp(command, "ui_print") == 0) {
            char* str = strtok(NULL, "\n");
            if (str) {
                ui->Print("%s", str);
            } else {
                ui->Print("\n");
            }
            fflush(stdout);
        } else if (strcmp(command, "wipe_cache") == 0) {
            *wipe_cache = 1;
        } else if (strcmp(command, "clear_display") == 0) {
            ui->SetBackground(RecoveryUI::NONE);
        } else if (strcmp(command, "enable_reboot") == 0) {
            // packages can explicitly request that they want the user
            // to be able to reboot during installation (useful for
            // debugging packages that don't exit).
            ui->SetEnableReboot(true);
        } else {
            LOGE("unknown command [%s]\n", command);
        }
    }
    fclose(from_child);

    int status;
    waitpid(pid, &status, 0);
    if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
        LOGE("Error in %s\n(Status %d)\n", path, WEXITSTATUS(status));
        return INSTALL_ERROR;
    }

    return INSTALL_SUCCESS;
}

static int
really_install_package(const char *path, int* wipe_cache, bool needs_mount)
{
    ui->SetBackground(RecoveryUI::INSTALLING_UPDATE);
    ui->Print("Finding update package...\n");
    // Give verification half the progress bar...
    ui->SetProgressType(RecoveryUI::DETERMINATE);
    ui->ShowProgress(VERIFICATION_PROGRESS_FRACTION, VERIFICATION_PROGRESS_TIME);
    LOGI("Update location: %s\n", path);

    // Map the update package into memory.
    ui->Print("Opening update package...\n");

    if (path && needs_mount) {
        if (path[0] == '@') {
            ensure_path_mounted(path+1);
        } else {
            ensure_path_mounted(path);
        }
    }

    MemMapping map;
    if (sysMapFile(path, &map) != 0) {
        LOGE("failed to map file\n");
        return INSTALL_CORRUPT;
    }

    int numKeys;
    Certificate* loadedKeys = load_keys(PUBLIC_KEYS_FILE, &numKeys);
    if (loadedKeys == NULL) {
        LOGE("Failed to load keys\n");
        return INSTALL_CORRUPT;
    }
    LOGI("%d key(s) loaded from %s\n", numKeys, PUBLIC_KEYS_FILE);

    ui->Print("Verifying update package...\n");

    int err;
    err = verify_file(map.addr, map.length, loadedKeys, numKeys);
    free(loadedKeys);
    LOGI("verify_file returned %d\n", err);
    if (err != VERIFY_SUCCESS) {
        LOGE("signature verification failed\n");
        sysReleaseMap(&map);
        return INSTALL_CORRUPT;
    }

    /* Try to open the package.
     */
    ZipArchive zip;
    err = mzOpenZipArchive(map.addr, map.length, &zip);
    if (err != 0) {
        LOGE("Can't open %s\n(%s)\n", path, err != -1 ? strerror(err) : "bad");
        sysReleaseMap(&map);
        return INSTALL_CORRUPT;
    }

    /* Verify and install the contents of the package.
     */
    ui->Print("Installing update...\n");
    ui->SetEnableReboot(false);
    int result = try_update_binary(path, &zip, wipe_cache);
    ui->SetEnableReboot(true);
    ui->Print("\n");

    sysReleaseMap(&map);

    return result;
}

int
install_package(const char* path, int* wipe_cache, const char* install_file,
                bool needs_mount)
{
    FILE* install_log = fopen_path(install_file, "w");
    if (install_log) {
        fputs(path, install_log);
        fputc('\n', install_log);
    } else {
        LOGE("failed to open last_install: %s\n", strerror(errno));
    }
    int result;
    if (setup_install_mounts() != 0) {
        LOGE("failed to set up expected mounts for install; aborting\n");
        result = INSTALL_ERROR;
    } else {
        result = really_install_package(path, wipe_cache, needs_mount);
    }
    if (install_log) {
        fputc(result == INSTALL_SUCCESS ? '1' : '0', install_log);
        fputc('\n', install_log);
        fclose(install_log);
    }
    return result;
}
