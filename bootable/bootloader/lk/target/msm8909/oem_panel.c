/* Copyright (c) 2014-2015, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  * Neither the name of The Linux Foundation nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <debug.h>
#include <err.h>
#include <smem.h>
#include <msm_panel.h>
#include <board.h>
#include <mipi_dsi.h>
#include <target/display.h>
#include <platform/gpio.h>
#include "include/panel.h"
#include "panel_display.h"
//#include "include/panel_jd9367_720p_video.h"
#include "include/panel_hx8394d_720p_video.h"
#include "include/panel_sharp_qhd_video.h"
#include "include/panel_truly_wvga_cmd.h"
#include "include/panel_hx8379a_fwvga_skua_video.h"
#include "include/panel_ili9806e_fwvga_video.h"
#include "include/panel_hx8394d_qhd_video.h"
#include "include/panel_hx8379c_fwvga_video.h"
#include "include/panel_st7796s_320p_video.h"
#include "include/panel_otm1287a_720p_video.h"
#include "include/panel_otm1287a_yitian_720p_video.h"
#include "include/panel_otm1287a_yushun_720p_video.h"
#include "include/panel_ili9881c_720p_video.h"
#define DISPLAY_MAX_PANEL_DETECTION 2
#define ILI9806E_FWVGA_VIDEO_PANEL_POST_INIT_DELAY 68
#define GENERIC_720P_CMD_SIGNATURE 0x210000 
#define TLMM_LCD_ID_GPIO	1


#define LCD_9LEI_HX8394D   0

enum {
	QRD_SKUA = 0x00,
	QRD_SKUC = 0x08,
	QRD_SKUE = 0x09,
};

/*---------------------------------------------------------------------------*/
/* static panel selection variable                                           */
/*---------------------------------------------------------------------------*/
//static uint32_t auto_pan_loop = 0;
int displaycount = 0;

enum {
	ILI9881C_720P_VIDEO_PANEL_XINDALU,
	ILI9881C_720P_VIDEO_PANEL,
	OTM9605A_QHD_VIDEO_PANEL,
	HX8394D_720P_VIDEO_PANEL,
	SHARP_QHD_VIDEO_PANEL,
	TRULY_WVGA_CMD_PANEL,
	HX8379A_FWVGA_SKUA_VIDEO_PANEL,
	ILI9806E_FWVGA_VIDEO_PANEL,
	HX8394D_QHD_VIDEO_PANEL,
	HX8379C_FWVGA_VIDEO_PANEL,
	ST7796S_320P_VIDEO_PANEL,
	OTM1287A_720P_VIDEO_PANEL,
	OTM1287A_YITIAN_720P_VIDEO_PANEL,
	OTM1287A_YUSHUN_720P_VIDEO_PANEL,
	HX8394D_720P_VIDEO_PANEL_9LEI,
	OTM1287A_ZHUOYI_720P_VIDEO_PANEL,
	UNKNOWN_PANEL
};

/*
 * The list of panels that are supported on this target.
 * Any panel in this list can be selected using fastboot oem command.
 */
static struct panel_list supp_panels[] = {
	{"ili9881c_720p_video_xindalu", ILI9881C_720P_VIDEO_PANEL_XINDALU},
	{"ili9881c_720p_video", ILI9881C_720P_VIDEO_PANEL},
	{"otm9605a_qhd_video_panel",OTM9605A_QHD_VIDEO_PANEL},
	{"hx8394d_720p_video", HX8394D_720P_VIDEO_PANEL},
	{"sharp_qhd_video", SHARP_QHD_VIDEO_PANEL},
	{"truly_wvga_cmd", TRULY_WVGA_CMD_PANEL},
	{"hx8379a_fwvga_skua_video", HX8379A_FWVGA_SKUA_VIDEO_PANEL},
	{"ili9806e_fwvga_video",ILI9806E_FWVGA_VIDEO_PANEL},
	{"hx8394d_qhd_video", HX8394D_QHD_VIDEO_PANEL},
	{"hx8379c_fwvga_video",HX8379C_FWVGA_VIDEO_PANEL},
	{"st7796s_320p_video",ST7796S_320P_VIDEO_PANEL},
	{"otm1287a_720p_video", OTM1287A_720P_VIDEO_PANEL},
	{"otm1287a_yituan_720p_video", OTM1287A_YITIAN_720P_VIDEO_PANEL},
	{"otm1287a_yushun_720p_video", OTM1287A_YUSHUN_720P_VIDEO_PANEL},
	{"hx8394d_720p_video9lei", HX8394D_720P_VIDEO_PANEL_9LEI},

};

static uint32_t panel_id;
#if 0
static int get_lcd_id_status(void)
{
	uint8_t status = 0;

	gpio_tlmm_config(TLMM_LCD_ID_GPIO, 0, GPIO_INPUT, GPIO_PULL_UP, GPIO_8MA, GPIO_ENABLE);

	mdelay(20);

	status = gpio_status(TLMM_LCD_ID_GPIO);

	return status;
}
#endif 
extern  board_id_fibocom;
static int get_lcd_id_status(void)
{
        uint8_t status = 0;
       printf(" BID%d \n",board_id_fibocom);
       if(board_id_fibocom==2){ 
                printf(" lcd  ID16 \n");
        gpio_tlmm_config(16, 1, GPIO_INPUT, GPIO_PULL_UP, GPIO_8MA, GPIO_ENABLE);
        mdelay(20);

        status = gpio_status(16);
        }else{
                printf(" lcd ID1\n");
        gpio_tlmm_config(TLMM_LCD_ID_GPIO, 0, GPIO_INPUT, GPIO_PULL_UP, GPIO_8MA, GPIO_ENABLE);

        mdelay(20);

        status = gpio_status(TLMM_LCD_ID_GPIO);
        }
        return status;
}




int oem_panel_rotation()
{
	return NO_ERROR;
}

int oem_panel_on()
{
	/*
	 * OEM can keep there panel specific on instructions in this
	 * function
	 */
	if (panel_id == ILI9806E_FWVGA_VIDEO_PANEL)
		mdelay(ILI9806E_FWVGA_VIDEO_PANEL_POST_INIT_DELAY);

	return NO_ERROR;
}

int oem_panel_off()
{
	/*
	 * OEM can keep their panel specific off instructions
	 * in this function
	 */
	return NO_ERROR;
}

static int init_panel_data(struct panel_struct *panelstruct,
			struct msm_panel_info *pinfo,
			struct mdss_dsi_phy_ctrl *phy_db)
{
	int pan_type = PANEL_TYPE_DSI;

	switch (panel_id) {
/*	case HX8394D_720P_VIDEO_PANEL_9LEI:   
		 panelstruct->paneldata    = &hx8394d_720p_video_panel_data;
                panelstruct->panelres     = &hx8394d_720p_video_panel_res;
                panelstruct->color                = &hx8394d_720p_video_color;
                panelstruct->videopanel   = &hx8394d_720p_video_video_panel;
                panelstruct->commandpanel = &hx8394d_720p_video_command_panel;
                panelstruct->state                = &hx8394d_720p_video_state;
                panelstruct->laneconfig   = &hx8394d_720p_video_lane_config;
                panelstruct->paneltiminginfo
                                         = &hx8394d_720p_video_timing_info;
                panelstruct->panelresetseq
                                         = &hx8394d_720p_video_panel_reset_seq;
                panelstruct->backlightinfo = &hx8394d_720p_video_backlight;
                pinfo->mipi.panel_cmds
                                        = hx8394d_720p_video_on_command;
                pinfo->mipi.num_of_panel_cmds
                                        = HX8394D_720P_VIDEO_ON_COMMAND;
                memcpy(phy_db->timing,
                                hx8394d_720p_video_timings, TIMING_SIZE);
		pinfo->mipi.signature = 0xffff;
	*/	
case ILI9881C_720P_VIDEO_PANEL:	
dprintf(CRITICAL, "jack panel id ILI9881C_720P_VIDEO_PANEL\n");  
		panelstruct->paneldata    = &ili9881c_720p_video_panel_data;
                panelstruct->panelres     = &ili9881c_720p_video_panel_res;
                panelstruct->color                = &ili9881c_720p_video_color;
                panelstruct->videopanel   = &ili9881c_720p_video_video_panel;
                panelstruct->commandpanel = &ili9881c_720p_video_command_panel;
                panelstruct->state                = &ili9881c_720p_video_state;
                panelstruct->laneconfig   = &ili9881c_720p_video_lane_config;
                panelstruct->paneltiminginfo
                                         = &ili9881c_720p_video_timing_info;
                panelstruct->panelresetseq
                                         = &ili9881c_720p_video_reset_seq;
                panelstruct->backlightinfo = &ili9881c_720p_video_backlight;
                pinfo->mipi.panel_cmds
                                        = ili9881c_720p_video_on_command;
		pinfo->mipi.panel_read_cmds
					= &ili9881c_720p_video_read_id_cmd_s;
                pinfo->mipi.num_of_panel_cmds
                                        = ili9881c_720p_VIDEO_ON_COMMAND ;
                memcpy(phy_db->timing,
                                ili9881c_720p_video_timings, TIMING_SIZE);
		pinfo->mipi.signature = ili9881c_720p_VIDEO_SIGNATURE;
		break;
	case HX8394D_720P_VIDEO_PANEL:
	dprintf(CRITICAL, "jack panel id HX8394D_720P_VIDEO_PANEL\n");   
		panelstruct->paneldata	  = &hx8394d_720p_video_panel_data;
		panelstruct->panelres	  = &hx8394d_720p_video_panel_res;
		panelstruct->color		  = &hx8394d_720p_video_color;
		panelstruct->videopanel   = &hx8394d_720p_video_video_panel;
		panelstruct->commandpanel = &hx8394d_720p_video_command_panel;
		panelstruct->state		  = &hx8394d_720p_video_state;
		panelstruct->laneconfig   = &hx8394d_720p_video_lane_config;
		panelstruct->paneltiminginfo
					 = &hx8394d_720p_video_timing_info;
		panelstruct->panelresetseq
					 = &hx8394d_720p_video_panel_reset_seq;
		panelstruct->backlightinfo = &hx8394d_720p_video_backlight;
		pinfo->mipi.panel_cmds
					= hx8394d_720p_video_on_command;
		pinfo->mipi.panel_read_cmds
					= &hx8394d_720p_video_read_id_cmd_s;
		pinfo->mipi.num_of_panel_cmds
					= HX8394D_720P_VIDEO_ON_COMMAND;
		memcpy(phy_db->timing,
				hx8394d_720p_video_timings, TIMING_SIZE);
		pinfo->mipi.signature = HX8394D_720P_VIDEO_SIGNATURE;
		break;
        case SHARP_QHD_VIDEO_PANEL:
		panelstruct->paneldata    = &sharp_qhd_video_panel_data;
		panelstruct->panelres     = &sharp_qhd_video_panel_res;
		panelstruct->color        = &sharp_qhd_video_color;
		panelstruct->videopanel   = &sharp_qhd_video_video_panel;
		panelstruct->commandpanel = &sharp_qhd_video_command_panel;
		panelstruct->state        = &sharp_qhd_video_state;
		panelstruct->laneconfig   = &sharp_qhd_video_lane_config;
		panelstruct->paneltiminginfo
					= &sharp_qhd_video_timing_info;
		panelstruct->panelresetseq
					= &sharp_qhd_video_panel_reset_seq;
		panelstruct->backlightinfo = &sharp_qhd_video_backlight;
		pinfo->mipi.panel_cmds
					= sharp_qhd_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= SHARP_QHD_VIDEO_ON_COMMAND;
		memcpy(phy_db->timing, sharp_qhd_video_timings, TIMING_SIZE);
		break;
      case TRULY_WVGA_CMD_PANEL:
               panelstruct->paneldata    = &truly_wvga_cmd_panel_data;
               panelstruct->panelres     = &truly_wvga_cmd_panel_res;
               panelstruct->color        = &truly_wvga_cmd_color;
               panelstruct->videopanel   = &truly_wvga_cmd_video_panel;
               panelstruct->commandpanel = &truly_wvga_cmd_command_panel;
               panelstruct->state        = &truly_wvga_cmd_state;
               panelstruct->laneconfig   = &truly_wvga_cmd_lane_config;
               panelstruct->paneltiminginfo
                                        = &truly_wvga_cmd_timing_info;
               panelstruct->panelresetseq
                                        = &truly_wvga_cmd_reset_seq;
               panelstruct->backlightinfo = &truly_wvga_cmd_backlight;
               pinfo->mipi.panel_cmds
                                       = truly_wvga_cmd_on_command;
               pinfo->mipi.num_of_panel_cmds
                                      = TRULY_WVGA_CMD_ON_COMMAND;
               memcpy(phy_db->timing,
                       truly_wvga_cmd_timings, TIMING_SIZE);
               break;
	case HX8379A_FWVGA_SKUA_VIDEO_PANEL:
		panelstruct->paneldata	  = &hx8379a_fwvga_skua_video_panel_data;
		panelstruct->panelres	  = &hx8379a_fwvga_skua_video_panel_res;
		panelstruct->color	  = &hx8379a_fwvga_skua_video_color;
		panelstruct->videopanel   = &hx8379a_fwvga_skua_video_video_panel;
		panelstruct->commandpanel = &hx8379a_fwvga_skua_video_command_panel;
		panelstruct->state	  = &hx8379a_fwvga_skua_video_state;
		panelstruct->laneconfig   = &hx8379a_fwvga_skua_video_lane_config;
		panelstruct->paneltiminginfo
					 = &hx8379a_fwvga_skua_video_timing_info;
		panelstruct->panelresetseq
					 = &hx8379a_fwvga_skua_video_reset_seq;
		panelstruct->backlightinfo = &hx8379a_fwvga_skua_video_backlight;
		pinfo->mipi.panel_cmds
					= hx8379a_fwvga_skua_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= HX8379A_FWVGA_SKUA_VIDEO_ON_COMMAND;
		memcpy(phy_db->timing,
				hx8379a_fwvga_skua_video_timings, TIMING_SIZE);
		pinfo->mipi.signature = HX8379A_FWVGA_SKUA_VIDEO_SIGNATURE;
		break;
	case ILI9806E_FWVGA_VIDEO_PANEL:
                panelstruct->paneldata    = &ili9806e_fwvga_video_panel_data;
                panelstruct->panelres     = &ili9806e_fwvga_video_panel_res;
                panelstruct->color        = &ili9806e_fwvga_video_color;
                panelstruct->videopanel   = &ili9806e_fwvga_video_video_panel;
                panelstruct->commandpanel = &ili9806e_fwvga_video_command_panel;
                panelstruct->state        = &ili9806e_fwvga_video_state;
                panelstruct->laneconfig   = &ili9806e_fwvga_video_lane_config;
                panelstruct->paneltiminginfo
                                         = &ili9806e_fwvga_video_timing_info;
                panelstruct->panelresetseq
                                         = &ili9806e_fwvga_video_reset_seq;
                panelstruct->backlightinfo = &ili9806e_fwvga_video_backlight;
                pinfo->mipi.panel_cmds
                                        = ili9806e_fwvga_video_on_command;
                pinfo->mipi.num_of_panel_cmds
                                        = ILI9806E_FWVGA_VIDEO_ON_COMMAND;
                memcpy(phy_db->timing,
                                ili9806e_fwvga_video_timings, TIMING_SIZE);
                pinfo->mipi.signature = ILI9806E_FWVGA_VIDEO_SIGNATURE;
                break;
	case HX8394D_QHD_VIDEO_PANEL:
		panelstruct->paneldata	  = &hx8394d_qhd_video_panel_data;
		panelstruct->panelres	  = &hx8394d_qhd_video_panel_res;
		panelstruct->color		  = &hx8394d_qhd_video_color;
		panelstruct->videopanel   = &hx8394d_qhd_video_video_panel;
		panelstruct->commandpanel = &hx8394d_qhd_video_command_panel;
		panelstruct->state		  = &hx8394d_qhd_video_state;
		panelstruct->laneconfig   = &hx8394d_qhd_video_lane_config;
		panelstruct->paneltiminginfo
					 = &hx8394d_qhd_video_timing_info;
		panelstruct->panelresetseq
					 = &hx8394d_qhd_video_panel_reset_seq;
		panelstruct->backlightinfo = &hx8394d_qhd_video_backlight;
		pinfo->mipi.panel_cmds
					= hx8394d_qhd_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= HX8394D_QHD_VIDEO_ON_COMMAND;
		memcpy(phy_db->timing,
				hx8394d_qhd_video_timings, TIMING_SIZE);
		pinfo->mipi.signature = HX8394D_QHD_VIDEO_SIGNATURE;
		break;
	case HX8379C_FWVGA_VIDEO_PANEL:
		panelstruct->paneldata    = &hx8379c_fwvga_video_panel_data;
		panelstruct->panelres     = &hx8379c_fwvga_video_panel_res;
		panelstruct->color        = &hx8379c_fwvga_video_color;
		panelstruct->videopanel   = &hx8379c_fwvga_video_video_panel;
		panelstruct->commandpanel = &hx8379c_fwvga_video_command_panel;
		panelstruct->state        = &hx8379c_fwvga_video_state;
		panelstruct->laneconfig   = &hx8379c_fwvga_video_lane_config;
		panelstruct->paneltiminginfo
					= &hx8379c_fwvga_video_timing_info;
		panelstruct->panelresetseq
					= &hx8379c_fwvga_video_reset_seq;
		panelstruct->backlightinfo = &hx8379c_fwvga_video_backlight;
		pinfo->mipi.panel_cmds
					= hx8379c_fwvga_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= HX8379C_FWVGA_VIDEO_ON_COMMAND;
		memcpy(phy_db->timing,
					hx8379c_fwvga_video_timings, TIMING_SIZE);
		pinfo->mipi.signature = HX8379C_FWVGA_VIDEO_SIGNATURE;
		break;
	case ST7796S_320P_VIDEO_PANEL:
		panelstruct->paneldata    = &st7796s_320p_video_panel_data;
		panelstruct->panelres     = &st7796s_320p_video_panel_res;
		panelstruct->color        = &st7796s_320p_video_color;
		panelstruct->videopanel   = &st7796s_320p_video_video_panel;
		panelstruct->commandpanel = &st7796s_320p_video_command_panel;
		panelstruct->state        = &st7796s_320p_video_state;
		panelstruct->laneconfig   = &st7796s_320p_video_lane_config;
		panelstruct->paneltiminginfo
					= &st7796s_320p_video_timing_info;
		panelstruct->panelresetseq
					= &st7796s_320p_video_reset_seq;
		panelstruct->backlightinfo = &st7796s_320p_video_backlight;
		pinfo->mipi.panel_cmds
					= st7796s_320p_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= ST7796S_320P_VIDEO_ON_COMMAND;
		memcpy(phy_db->timing,
					st7796s_320p_video_timings, TIMING_SIZE);
		pinfo->mipi.signature = ST7796S_320P_VIDEO_SIGNATURE;
		break;
	case OTM1287A_720P_VIDEO_PANEL:
		panelstruct->paneldata    = &otm1287a_720p_video_panel_data;
		panelstruct->panelres     = &otm1287a_720p_video_panel_res;
		panelstruct->color        = &otm1287a_720p_video_color;
		panelstruct->videopanel   = &otm1287a_720p_video_video_panel;
		panelstruct->commandpanel = &otm1287a_720p_video_command_panel;
		panelstruct->state        = &otm1287a_720p_video_state;
		panelstruct->laneconfig   = &otm1287a_720p_video_lane_config;
		panelstruct->paneltiminginfo
			= &otm1287a_720p_video_timing_info;
		panelstruct->panelresetseq
			= &otm1287a_720p_video_reset_seq;
		panelstruct->backlightinfo = &otm1287a_720p_video_backlight;
		pinfo->mipi.panel_cmds
			= otm1287a_720p_video_on_command;
		pinfo->mipi.num_of_panel_cmds
			= OTM1287A_720P_VIDEO_ON_COMMAND;
		memcpy(phy_db->timing,
				otm1287a_720p_video_timings, TIMING_SIZE);
		///pinfo->mipi.signature = OTM1287A_720P_VIDEO_SIGNATURE;
		pinfo->mipi.signature = GENERIC_720P_CMD_SIGNATURE;
		break;
	case OTM1287A_YUSHUN_720P_VIDEO_PANEL:
		panelstruct->paneldata    = &otm1287a_yushun_720p_video_panel_data;
     		panelstruct->panelres     = &otm1287a_yushun_720p_video_panel_res;
     		panelstruct->color        = &otm1287a_yitian_720p_video_color;
     		panelstruct->videopanel   = &otm1287a_yushun_720p_video_video_panel;
     		panelstruct->commandpanel = &otm1287a_yushun_720p_video_command_panel;
     		panelstruct->state        = &otm1287a_yushun_720p_video_state;
     		panelstruct->laneconfig   = &otm1287a_yitian_720p_video_lane_config;
     		panelstruct->paneltiminginfo
             		= &otm1287a_yushun_720p_video_timing_info;
     		panelstruct->panelresetseq
             		= &otm1287a_yushun_720p_video_reset_seq;
     		panelstruct->backlightinfo = &otm1287a_yushun_720p_video_backlight;
     		pinfo->mipi.panel_cmds
             		= otm1287a_yushun_720p_video_on_command;
    		pinfo->mipi.num_of_panel_cmds
             		= OTM1287A_YUSHUN_720P_VIDEO_ON_COMMAND;
     		memcpy(phy_db->timing,
                     otm1287a_yushun_720p_video_timings, TIMING_SIZE);
     		pinfo->mipi.signature = OTM1287A_YUSHUN_720P_VIDEO_SIGNATURE;
		break;
	case OTM1287A_YITIAN_720P_VIDEO_PANEL:
#if 1
		panelstruct->paneldata    = &otm1287a_yitian_720p_video_panel_data;
		panelstruct->panelres     = &otm1287a_yitian_720p_video_panel_res;
		panelstruct->color        = &otm1287a_yitian_720p_video_color;
		panelstruct->videopanel   = &otm1287a_yitian_720p_video_video_panel;
		panelstruct->commandpanel = &otm1287a_yitian_720p_video_command_panel;
		panelstruct->state        = &otm1287a_yitian_720p_video_state;
		panelstruct->laneconfig   = &otm1287a_yitian_720p_video_lane_config;
		panelstruct->paneltiminginfo
			= &otm1287a_yitian_720p_video_timing_info;
		panelstruct->panelresetseq
			= &otm1287a_yitian_720p_video_reset_seq;
		panelstruct->backlightinfo = &otm1287a_yitian_720p_video_backlight;
		pinfo->mipi.panel_cmds
			= otm1287a_yitian_720p_video_on_command;
		pinfo->mipi.num_of_panel_cmds
			= OTM1287A_YITIAN_720P_VIDEO_ON_COMMAND;
		memcpy(phy_db->timing,
				otm1287a_yitian_720p_video_timings, TIMING_SIZE);
		pinfo->mipi.signature = OTM1287A_YITIAN_720P_VIDEO_SIGNATURE;
#endif
#if 0
		panelstruct->paneldata    = &otm1287a_720p_video_panel_data;
                panelstruct->panelres     = &otm1287a_720p_video_panel_res;
                panelstruct->color        = &otm1287a_720p_video_color;
                panelstruct->videopanel   = &otm1287a_720p_video_video_panel;
                panelstruct->commandpanel = &otm1287a_720p_video_command_panel;
                panelstruct->state        = &otm1287a_720p_video_state;
                panelstruct->laneconfig   = &otm1287a_720p_video_lane_config;
                panelstruct->paneltiminginfo
                        = &otm1287a_720p_video_timing_info;
                panelstruct->panelresetseq
                        = &otm1287a_720p_video_reset_seq;
                panelstruct->backlightinfo = &otm1287a_720p_video_backlight;
                pinfo->mipi.panel_cmds
                        = otm1287a_720p_video_on_command;
                pinfo->mipi.num_of_panel_cmds
                        = OTM1287A_720P_VIDEO_ON_COMMAND;
                memcpy(phy_db->timing,
                                otm1287a_720p_video_timings, TIMING_SIZE);
                ///pinfo->mipi.signature = OTM1287A_720P_VIDEO_SIGNATURE; 
		pinfo->mipi.signature = GENERIC_720P_CMD_SIGNATURE;
#endif 
		break;
 	/*	
   	case OTM1287A_ZHUOYI_720P_VIDEO_PANEL:
               #if 1
               panelstruct->paneldata    = &jd9367_720p_video_panel_data;
               panelstruct->panelres     = &jd9367_720p_video_panel_res;
               panelstruct->color        = &jd9367_720p_video_color;
               panelstruct->videopanel   = &jd9367_720p_video_video_panel;
               panelstruct->commandpanel = &jd9367_720p_video_command_panel;
               panelstruct->state        = &jd9367_720p_video_state;
               panelstruct->laneconfig   = &jd9367_720p_video_lane_config;
               panelstruct->paneltiminginfo
                       = &jd9367_720p_video_timing_info;
               panelstruct->panelresetseq
                       = &jd9367_720p_video_reset_seq;
               panelstruct->backlightinfo = &jd9367_720p_video_backlight;
               pinfo->mipi.panel_cmds
                       = jd9367_720p_video_on_command;
               pinfo->mipi.num_of_panel_cmds
                       =  JD9367_720P_VIDEO_ON_COMMAND;
               memcpy(phy_db->timing,
                               jd9367_720p_video_timings, TIMING_SIZE);
               pinfo->mipi.signature = JD9367_720P_VIDEO_SIGNATURE;
       	       #endif
               break;

*/
	case UNKNOWN_PANEL:
	default:
		memset(panelstruct, 0, sizeof(struct panel_struct));
		memset(pinfo->mipi.panel_cmds, 0, sizeof(struct mipi_dsi_cmd));
		pinfo->mipi.num_of_panel_cmds = 0;
		memset(phy_db->timing, 0, TIMING_SIZE);
		pan_type = PANEL_TYPE_UNKNOWN;
		break;
	}
	return pan_type;
}

uint32_t oem_panel_max_auto_detect_panels()
{
        return target_panel_auto_detect_enabled() ?
                        DISPLAY_MAX_PANEL_DETECTION : 0;
}

int oem_panel_select(const char *panel_name, struct panel_struct *panelstruct,
			struct msm_panel_info *pinfo,
			struct mdss_dsi_phy_ctrl *phy_db)
{
	uint32_t hw_id = board_hardware_id();
	uint32_t platform_subtype = board_hardware_subtype();
	int32_t panel_override_id;
	
    dprintf(CRITICAL, "hw_id:%d\n",hw_id); 
	if (panel_name) {
		panel_override_id = panel_name_to_id(supp_panels,
				ARRAY_SIZE(supp_panels), panel_name);

		if (panel_override_id < 0) {
			dprintf(CRITICAL, "Not able to search the panel:%s\n",
					 panel_name + strspn(panel_name, " "));
		} else if (panel_override_id < UNKNOWN_PANEL) {
			/* panel override using fastboot oem command */
			panel_id = panel_override_id;

			dprintf(CRITICAL, "OEM panel override:%s\n",
					panel_name + strspn(panel_name, " "));
			goto panel_init;
		}
	}
	switch (hw_id) {
	case HW_PLATFORM_SURF:
	case HW_PLATFORM_MTP:
	case HW_PLATFORM_RCM:
	dprintf(CRITICAL, "hw_id surf_mtp_rcm\n");   
		//panel_id = ILI9881C_720P_VIDEO_PANEL;

		//panel_id = HX8394D_720P_VIDEO_PANEL;
		//break;
		if(displaycount==0)
		{
			panel_id = ILI9881C_720P_VIDEO_PANEL;
			displaycount++;
			break;
		}
		if(displaycount==1)
		{
			panel_id = HX8394D_720P_VIDEO_PANEL;
			displaycount++;
			break;
		}
	case HW_PLATFORM_QRD:
		dprintf(CRITICAL, "hw_id HW_PLATFORM_QRD\n");  
		switch (platform_subtype) {
			case QRD_SKUA:
				panel_id = HX8379A_FWVGA_SKUA_VIDEO_PANEL;
				break;
			case QRD_SKUC:
				panel_id = ILI9806E_FWVGA_VIDEO_PANEL;
				break;
			case QRD_SKUE:
				panel_id = HX8379C_FWVGA_VIDEO_PANEL;
				break;
			default:
				dprintf(CRITICAL, "QRD Display not enabled for %d type\n",
						platform_subtype);
				return PANEL_TYPE_UNKNOWN;
		}
		break;
	default:
		dprintf(CRITICAL, "Display not enabled for %d HW type\n",
			hw_id);
		return PANEL_TYPE_UNKNOWN;
	}

panel_init:
	phy_db->regulator_mode = DSI_PHY_REGULATOR_LDO_MODE;
	return init_panel_data(panelstruct, pinfo, phy_db);
}

/*
int oem_panel_select(const char *panel_name, struct panel_struct *panelstruct,
			struct msm_panel_info *pinfo,
			struct mdss_dsi_phy_ctrl *phy_db)
{
	uint32_t platform_subtype = board_hardware_subtype();

	//panel_id = ST7796S_320P_VIDEO_PANEL;
	if(get_lcd_id_status())
	{  	printf("ID level 1 lcd yushun ,  \n");
		panel_id =    OTM1287A_YUSHUN_720P_VIDEO_PANEL;/// OTM1287A_720P_VIDEO_PANEL;///OTM1287A_YUSHUN_720P_VIDEO_PANEL;///OTM1287A_720P_VIDEO_PANEL;
	}
	else
	{  	printf("ID level 0 lcd yitian\n");
		panel_id =   OTM1287A_YITIAN_720P_VIDEO_PANEL;
	}
          if(LCD_9LEI_HX8394D||board_id_fibocom==35)
		panel_id =   HX8394D_720P_VIDEO_PANEL_9LEI;
          //if(board_id_fibocom==0x1e){
		//panel_id = OTM1287A_ZHUOYI_720P_VIDEO_PANEL;
               // lcd_zhuoyi_pwron();
	 //}
         
	phy_db->regulator_mode = DSI_PHY_REGULATOR_LDO_MODE;
	return init_panel_data(panelstruct, pinfo, phy_db);
}
*/
