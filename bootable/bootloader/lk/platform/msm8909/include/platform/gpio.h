/* Copyright (c) 2014, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __PLATFORM_MSM8909_GPIO_H
#define __PLATFORM_MSM8909_GPIO_H

#include <bits.h>
#include <gpio.h>

/* GPIO TLMM: Direction */
#define GPIO_INPUT      0
#define GPIO_OUTPUT     1

/* GPIO TLMM: Pullup/Pulldown */
#define GPIO_NO_PULL    0
#define GPIO_PULL_DOWN  1
#define GPIO_KEEPER     2
#define GPIO_PULL_UP    3

/* GPIO TLMM: Drive Strength */
#define GPIO_2MA        0
#define GPIO_4MA        1
#define GPIO_6MA        2
#define GPIO_8MA        3
#define GPIO_10MA       4
#define GPIO_12MA       5
#define GPIO_14MA       6
#define GPIO_16MA       7

/* GPIO TLMM: Status */
#define GPIO_ENABLE     0
#define GPIO_DISABLE    1

/* GPIO_IN_OUT register shifts. */
#define GPIO_IN         BIT(0)
#define GPIO_OUT        BIT(1)

void gpio_config_uart_dm(uint8_t id);

/* for SPI */
#define SPI_MOSI_FUNC(f)	(((f) >> SPI_MOSI_FUNC_SHIFT) & 0xff)
#define SPI_MISO_FUNC(f)	(((f) >> SPI_MISO_FUNC_SHIFT) & 0xff)
#define SPI_CS_FUNC(f)     	(((f) >> SPI_CS_FUNC_SHIFT) & 0xff)
#define SPI_CLK_FUNC(f)		(((f) >> SPI_CLK_FUNC_SHIFT) & 0xff)

#define SPI_MOSI_FUNC_SHIFT	0
#define SPI_MISO_FUNC_SHIFT	8
#define SPI_CS_FUNC_SHIFT	16
#define SPI_CLK_FUNC_SHIFT	24

#define SPI_BLSP1_QUP1_MOSI	4
#define SPI_BLSP1_QUP1_MISO	5
#define SPI_BLSP1_QUP1_CS	6
#define SPI_BLSP1_QUP1_CLK	7
#define SPI_BLSP1_QUP1_FUNC	((1 << SPI_MOSI_FUNC_SHIFT) |\
				(1 << SPI_MISO_FUNC_SHIFT) |\
				(1 << SPI_CS_FUNC_SHIFT) |\
				(1 << SPI_CLK_FUNC_SHIFT))

#define SPI_BLSP1_QUP2_MOSI	20
#define SPI_BLSP1_QUP2_MISO	21
#define SPI_BLSP1_QUP2_CS	111
#define SPI_BLSP1_QUP2_CLK	112
#define SPI_BLSP1_QUP2_FUNC	((2 << SPI_MOSI_FUNC_SHIFT) |\
				(2 << SPI_MISO_FUNC_SHIFT) |\
				(1 << SPI_CS_FUNC_SHIFT) |\
				(1 << SPI_CLK_FUNC_SHIFT))

#define SPI_BLSP1_QUP3_MOSI	0
#define SPI_BLSP1_QUP3_MISO	1
#define SPI_BLSP1_QUP3_CS	2
#define SPI_BLSP1_QUP3_CLK	3
#define SPI_BLSP1_QUP3_FUNC	((1 << SPI_MOSI_FUNC_SHIFT) |\
				(1 << SPI_MISO_FUNC_SHIFT) |\
				(1 << SPI_CS_FUNC_SHIFT) |\
				(1 << SPI_CLK_FUNC_SHIFT))

#define SPI_BLSP1_QUP4_MOSI	12
#define SPI_BLSP1_QUP4_MISO	13
#define SPI_BLSP1_QUP4_CS	14
#define SPI_BLSP1_QUP4_CLK	15
#define SPI_BLSP1_QUP4_FUNC	((1 << SPI_MOSI_FUNC_SHIFT) |\
				(1 << SPI_MISO_FUNC_SHIFT) |\
				(1 << SPI_CS_FUNC_SHIFT) |\
				(1 << SPI_CLK_FUNC_SHIFT))

#define SPI_BLSP1_QUP5_MOSI	16
#define SPI_BLSP1_QUP5_MISO	17
#define SPI_BLSP1_QUP5_CS	18
#define SPI_BLSP1_QUP5_CLK	19
#define SPI_BLSP1_QUP5_FUNC	((1 << SPI_MOSI_FUNC_SHIFT) |\
				(1 << SPI_MISO_FUNC_SHIFT) |\
				(1 << SPI_CS_FUNC_SHIFT) |\
				(1 << SPI_CLK_FUNC_SHIFT))

#define SPI_BLSP1_QUP6_MOSI	8
#define SPI_BLSP1_QUP6_MISO	9
#define SPI_BLSP1_QUP6_CS	10
#define SPI_BLSP1_QUP6_CLK	11
#define SPI_BLSP1_QUP6_FUNC	((1 << SPI_MOSI_FUNC_SHIFT) |\
				(1 << SPI_MISO_FUNC_SHIFT) |\
				(1 << SPI_CS_FUNC_SHIFT) |\
				(1 << SPI_CLK_FUNC_SHIFT))

#endif
