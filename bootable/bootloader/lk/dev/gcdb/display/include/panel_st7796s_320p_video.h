/* Copyright (c) 2014, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  * Neither the name of The Linux Foundation nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*---------------------------------------------------------------------------
 * This file is autogenerated file using gcdb parser. Please do not edit it.
 * Update input XML file to add a new entry or update variable in this file
 * VERSION = "1.0"
 *---------------------------------------------------------------------------*/

#ifndef _PANEL_ST7796S_320P_VIDEO_H_
#define _PANEL_ST7796S_320P_VIDEO_H_
/*---------------------------------------------------------------------------*/
/* HEADER files                                                              */
/*---------------------------------------------------------------------------*/
#include "panel.h"

#define MIPI_DSI_CMD_WAIT(cmd,ms) {sizeof(cmd), cmd, ms}
#define ARRAY_SIZES(arr) (sizeof(arr)/sizeof(arr[0]))

/*---------------------------------------------------------------------------*/
/* Panel configuration                                                       */
/*---------------------------------------------------------------------------*/
static struct panel_config st7796s_320p_video_panel_data = {
  "qcom,mdss_dsi_st7796s_320p_video", "dsi:0:", "qcom,mdss-dsi-panel",
  10, 0, "DISPLAY_1", 0, 0, 60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

static struct panel_resolution st7796s_320p_video_panel_res = {
  320, 480, 12, 2, 24, 0, 8, 20, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

static struct color_info st7796s_320p_video_color = {
  24, 0, 0xff, 0, 0, 0
};

static char st7796s_320p_video_on_cmd0[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x01, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd1[] = {
0x02, 0x00, 0x39, 0xC0,
0xf0, 0xc3, 0xFF, 0xFF,
};

static char st7796s_320p_video_on_cmd2[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x01, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd3[] = {
0x02, 0x00, 0x39, 0xC0,
0xf0, 0x96, 0xFF, 0xFF,
};

static char st7796s_320p_video_on_cmd4[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x01, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd5[] = {
0x02, 0x00, 0x39, 0xC0,
0xb9, 0x02, 0xFF, 0xFF,
};

static char st7796s_320p_video_on_cmd6[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x08, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd7[] = {
0x09, 0x00, 0x39, 0xC0,
0xe8, 0x40, 0x8a, 0x00,
0x00, 0x29, 0x19, 0xa5,
0x33, 0xFF, 0xFF, 0xFF,
};

static char st7796s_320p_video_on_cmd8[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x01, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd9[] = {
0x02, 0x00, 0x39, 0xC0,
0xc2, 0xa7, 0xFF, 0xFF,
};

static char st7796s_320p_video_on_cmd10[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x01, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd11[] = {
0x02, 0x00, 0x39, 0xC0,
0xc5, 0x28, 0xFF, 0xFF,
};

static char st7796s_320p_video_on_cmd12[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x02, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd13[] = {
0x03, 0x00, 0x39, 0xC0,
0xb1, 0x80, 0x10, 0xFF,
};

static char st7796s_320p_video_on_cmd14[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x14, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd15[] = {
0x0f, 0x00, 0x39, 0xC0,
0xe0, 0xf0, 0x09, 0x0b,
0x06, 0x04, 0x15, 0x2f,
0x54, 0x42, 0x3c, 0x17,
0x14, 0x18, 0x1b, 0xFF,
};

static char st7796s_320p_video_on_cmd16[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x14, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd17[] = {
0x0f, 0x00, 0x39, 0xC0,
0xe1, 0xf0, 0x09, 0x0b,
0x06, 0x04, 0x03, 0x2d,
0x43, 0x42, 0x3b, 0x16,
0x14, 0x17, 0x1b, 0xFF,
};

static char st7796s_320p_video_on_cmd18[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x01, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd19[] = {
0x02, 0x00, 0x39, 0xC0,
0xf0, 0x3c, 0xFF, 0xFF,
};

static char st7796s_320p_video_on_cmd20[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x01, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd21[] = {
0x02, 0x00, 0x39, 0xC0,
0xf0, 0x69, 0xFF, 0xFF,
};

static char st7796s_320p_video_on_cmd22[] = {
0x03, 0x00, 0x39, 0xC0,
0xb7, 0x50, 0x03, 0xFF,
};

static char st7796s_320p_video_on_cmd23[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x01, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd24[] = {
0x02, 0x00, 0x39, 0xC0,
0x3a, 0x77, 0xFF, 0xFF,
};

static char st7796s_320p_video_on_cmd25[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x01, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd26[] = {
0x02, 0x00, 0x39, 0xC0,
0x36, 0x48, 0xFF, 0xFF,
};

static char st7796s_320p_video_on_cmd27[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x01, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd28[] = {
0x02, 0x00, 0x39, 0xC0,
0x35, 0x00, 0xFF, 0xFF,
};

static char st7796s_320p_video_on_cmd29[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x00, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd30[] = {
0x11, 0x00, 0x05, 0x80,
};

static char st7796s_320p_video_on_cmd31[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x00, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd32[] = {
0x29, 0x00, 0x05, 0x80,
};

static char st7796s_320p_video_on_cmd33[] = {
0x03, 0x00, 0x39, 0xC0,
0xbc, 0x00, 0x00, 0xFF,
};

static char st7796s_320p_video_on_cmd34[] = {
0x2c, 0x00, 0x05, 0x80,
};

static struct mipi_dsi_cmd st7796s_320p_video_on_command[] = {
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd0, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd1, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd2, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd3, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd4, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd5, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd6, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd7, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd8, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd9, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd10, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd11, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd12, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd13, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd14, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd15, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd16, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd17, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd18, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd19, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd20, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd21, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd22, 1),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd23, 1),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd24, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd25, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd26, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd27, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd28, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd29, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd30, 120),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd31, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd32, 10),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd33, 1),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_on_cmd34, 0),
};

#define ST7796S_320P_VIDEO_ON_COMMAND ARRAY_SIZE(st7796s_320p_video_on_command)

static char st7796s_320p_video_off_cmd0[] = {
0x28, 0x00, 0x05, 0x80,
};

static char st7796s_320p_video_off_cmd1[] = {
0x10, 0x00, 0x05, 0x80,
};

static struct mipi_dsi_cmd st7796s_320p_video_off_command[] = {
MIPI_DSI_CMD_WAIT(st7796s_320p_video_off_cmd0, 0),
MIPI_DSI_CMD_WAIT(st7796s_320p_video_off_cmd1, 0),
};

#define ST7796S_320P_VIDEO_OFF_COMMAND ARRAY_SIZE(st7796s_320p_video_off_command)

static struct command_state st7796s_320p_video_state = {
  0, 1
};

static struct commandpanel_info st7796s_320p_video_command_panel = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

static struct videopanel_info st7796s_320p_video_video_panel = {
  1, 0, 0, 0, 1, 1, 2, 0, 0x9
};

static struct lane_configuration st7796s_320p_video_lane_config = {
  1, 0, 1, 0, 0, 0
};

static const uint32_t st7796s_320p_video_timings[] = {
    0x79, 0x1a, 0x12, 0x00, 0x3e, 0x42, 0x16, 0x1e, 0x15, 0x03, 0x04, 0x00
};

static struct panel_timing st7796s_320p_video_timing_info = {
    0, 4, 0x04, 0x1b
};

static struct panel_reset_sequence st7796s_320p_video_reset_seq = {
  { 1, 0, 1, }, { 20, 1, 20, }, 2
};

static struct backlight st7796s_320p_video_backlight = {
    1, 1, 255, 100, 1, "bl_ctrl_pwm"
};

#define ST7796S_320P_VIDEO_SIGNATURE 0xFFFF

#endif /*_PANEL_ST7796S_320P_VIDEO_H_*/
