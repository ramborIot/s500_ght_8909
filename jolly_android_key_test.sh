#!/bin/bash
#put this file in android source root directory 
#android source has builded success.then run this sh
#
#replace this keys
#
#jolly_android_key_test/keystore.img      -->  user flash keystore partion
#jolly_android_key_test/verity.der        -->  
#jolly_android_key_test/oem_keystore.h    -->  bootable/bootloader/lk/platform/msm_shared/include/oem_keystore.h
#jolly_android_key_test/verity_key        -->  build/target/product/security/verity_key

#jolly_android_key_test/verity.pk8        -->  build/target/product/security/verity.pk8
#jolly_android_key_test/verity.x509.pem   -->  build/target/product/security/verity.x509.pem


#jolly_android_key_test/media.pk8         -->  build/target/product/security/media.pk8
#jolly_android_key_test/media.x509.pem    -->  build/target/product/security/media.x509.pem

#jolly_android_key_test/platform.pk8      -->  build/target/product/security/platform.pk8
#jolly_android_key_test/platform.x509.pem -->  build/target/product/security/platform.x509.pem

#jolly_android_key_test/shared.pk8        -->  build/target/product/security/shared.pk8
#jolly_android_key_test/shared.x509.pem   -->  build/target/product/security/shared.x509.pem

#jolly_android_key_test/shared.pk8        -->  build/target/product/security/shared.pk8
#jolly_android_key_test/shared.x509.pem   -->  build/target/product/security/shared.x509.pem

#jolly_android_key_test/testkey.pk8       -->  build/target/product/security/testkey.pk8
#jolly_android_key_test/testkey.x509.pem  -->  build/target/product/security/testkey.x509.pem


mkdir -p jolly_android_key_test

development/tools/make_key jolly_android_key_test/testkey  '/C=CN/ST=Guangdong/L=Shenzhen View/O=Jolly/OU=Jolly/CN=xumingliu/emailAddress=xumingliu@jollytech.com.cn'
development/tools/make_key jolly_android_key_test/releasekey  '/C=CN/ST=Guangdong/L=Shenzhen View/O=Jolly/OU=Jolly/CN=xumingliu/emailAddress=xumingliu@jollytech.com.cn'
development/tools/make_key jolly_android_key_test/platform '/C=CN/ST=Guangdong/L=Shenzhen View/O=Jolly/OU=Jolly/CN=xumingliu/emailAddress=xumingliu@jollytech.com.cn'
development/tools/make_key jolly_android_key_test/shared   '/C=CN/ST=Guangdong/L=Shenzhen View/O=Jolly/OU=Jolly/CN=xumingliu/emailAddress=xumingliu@jollytech.com.cn'
development/tools/make_key jolly_android_key_test/media    '/C=CN/ST=Guangdong/L=Shenzhen View/O=Jolly/OU=Jolly/CN=xumingliu/emailAddress=xumingliu@jollytech.com.cn'
development/tools/make_key jolly_android_key_test/verity   '/C=CN/ST=Guangdong/L=Shenzhen View/O=Jolly/OU=Jolly/CN=xumingliu/emailAddress=xumingliu@jollytech.com.cn'
#cp /data4/msm8909_1.2.3-10210_sc20_pub/build/target/product/security/verity.pk8 ./jolly_android_key_test/
#cp /data4/msm8909_1.2.3-10210_sc20_pub/build/target/product/security/verity.x509.pem ./jolly_android_key_test/
source build/envsetup.sh
lunch la0920-userdebug
mmm system/extras/verity/

out/host/linux-x86/bin/generate_verity_key -convert jolly_android_key_test/verity.x509.pem  jolly_android_key_test/verity_key
cp jolly_android_key_test/verity_key.pub jolly_android_key_test/verity_key
openssl rsa -in jolly_android_key_test/verity.pk8 -inform DER -pubout -outform DER -out jolly_android_key_test/verity.der
java -Xmx512M -jar out/host/linux-x86/framework/KeystoreSigner.jar jolly_android_key_test/verity.pk8 jolly_android_key_test/verity.x509.pem jolly_android_key_test/keystore.img jolly_android_key_test/verity.der

function generate_oem_keystore_h() 
{ 
  echo \#ifndef __OEM_KEYSTORE_H 
  echo \#define __OEM_KEYSTORE_H 
  xxd -i $1 | sed -e 's/unsigned char .* = {/const unsigned char OEM_KEYSTORE[] = {/g' -e 's/unsigned int .* =.*;//g' 
  echo \#endif
} 
generate_oem_keystore_h jolly_android_key_test/keystore.img > jolly_android_key_test/oem_keystore.h 

function replace_newkey()
{

cp jolly_android_key_test/oem_keystore.h         bootable/bootloader/lk/platform/msm_shared/include/oem_keystore.h
cp jolly_android_key_test/verity_key             build/target/product/security/verity_key

cp jolly_android_key_test/verity.pk8             build/target/product/security/verity.pk8
cp jolly_android_key_test/verity.x509.pem        build/target/product/security/verity.x509.pem

cp jolly_android_key_test/media.pk8              build/target/product/security/media.pk8
cp jolly_android_key_test/media.x509.pem         build/target/product/security/media.x509.pem

cp jolly_android_key_test/platform.pk8           build/target/product/security/platform.pk8
cp jolly_android_key_test/platform.x509.pem      build/target/product/security/platform.x509.pem

cp jolly_android_key_test/shared.pk8             build/target/product/security/shared.pk8
cp jolly_android_key_test/shared.x509.pem        build/target/product/security/shared.x509.pem

cp jolly_android_key_test/shared.pk8             build/target/product/security/shared.pk8
cp jolly_android_key_test/shared.x509.pem        build/target/product/security/shared.x509.pem

cp jolly_android_key_test/testkey.pk8            build/target/product/security/testkey.pk8
cp jolly_android_key_test/testkey.x509.pem       build/target/product/security/testkey.x509.pem 

cp jolly_android_key_test/releasekey.pk8            build/target/product/security/releasekey.pk8
cp jolly_android_key_test/releasekey.x509.pem       build/target/product/security/releasekey.x509.pem 
}

replace_newkey



