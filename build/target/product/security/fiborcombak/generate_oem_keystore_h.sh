#!/bin/bash

echo \#ifndef __OEM_KEYSTORE_H
echo \#define __OEM_KEYSTORE_H
xxd -i $1 | sed -e 's/unsigned char .* = {/const unsigned char OEM_KEYSTORE[] = {/g' -e 's/unsigned int .* =.*;//g'
echo \#endif