openssl version

../../../../development/tools/make_key releasekey  '/C=CN/ST=Guangdong/L=Shenzhen/O=Fibocom/OU=Solution/CN=zhangwj/emailAddress=zhangwj@fibocom.com'
../../../../development/tools/make_key platform '/C=CN/ST=Guangdong/L=Shenzhen/O=Fibocom/OU=Solution/CN=zhangwj/emailAddress=zhangwj@fibocom.com'
../../../../development/tools/make_key shared   '/C=CN/ST=Guangdong/L=Shenzhen/O=Fibocom/OU=Solution/CN=zhangwj/emailAddress=zhangwj@fibocom.com'
../../../../development/tools/make_key media    '/C=CN/ST=Guangdong/L=Shenzhen/O=Fibocom/OU=Solution/CN=zhangwj/emailAddress=zhangwj@fibocom.com'
../../../../development/tools/make_key verity   '/C=CN/ST=Guangdong/L=Shenzhen/O=Fibocom/OU=Solution/CN=zhangwj/emailAddress=zhangwj@fibocom.com'

#By default, the system partition is signed with a keypair under:
# Public key 每 build/target/product/security/verity_key
# Private key 每 build/target/product/security/verity
# 	The private key file exists with the suffixes x509.pem (certificate) and .pk8 (key)
../../../../out/host/linux-x86/bin/generate_verity_key  -convert verity.x509.pem verity
mv verity.pub verity_key


#Generate OEM＊s RSA_PUBLIC_KEY_PEM with openssl:
#PRODUCT_VERITY_SIGNING_KEY := build/target/product/security/verity
#openssl pkcs8 -inform DER -nocrypt -in <PRODUCT_VERITY_SIGNING_KEY> -out <RSA_PUBLIC_KEY_PEM>
openssl pkcs8 -inform DER -nocrypt -in verity.pk8 -out RSA_PUBLIC_KEY.pem

#Generate OEM＊s RSA_PUBLIC_KEY_DER with openssl:
#openssl rsa 每in <RSA_PUBLIC_KEY_PEM> -pubout 每outform DER 每out <RSA_PUBLIC_KEY_DER>
openssl rsa -in RSA_PUBLIC_KEY.pem -pubout -outform DER  -out RSA_PUBLIC_KEY.der

#Generate an OEM keystore.img by running the following command at the top of the Android build tree:
#keystore_signer <PRODUCT_VERITY_SIGNING_KEY> <verity.x509.pem> <KEYSTORE_IMG> <RSA_PUBLIC_KEY_DER>
#source build/envsetup.sh
#lunch la0910-user
#mmm system/extras/verity/
java -Xmx512M -jar ../../../../out/host/linux-x86/framework/KeystoreSigner.jar verity.pk8 verity.x509.pem KEYSTORE.img RSA_PUBLIC_KEY.der
#../../../../out/host/linux-x86/bin/keystore_signer verity.pk8 verity.x509.pem KEYSTORE.img RSA_PUBLIC_KEY.der


#Generate oem_keystore.h for the Android boot loader (LK):
#LK needs a header file (oem_keystore.h) that was generated with the contents of the OEM keystore.img generated in step 2.
#a. Run the following code to generate this header file:
#function generate_oem_keystore_h()
#{
#echo \#ifndef __OEM_KEYSTORE_H
#echo \#define __OEM_KEYSTORE_H
#xxd -i $1 | sed -e 's/unsigned char .* = {/const unsigned char OEM_KEYSTORE[] = {/g' -e 's/unsigned int .* =.*;//g'
#echo \#endif
#}
./generate_oem_keystore_h.sh KEYSTORE.img > oem_keystore.h

#b. Copy the oem_keystore.h file to location bootable/bootloader/lk/platform/msm_shared/include.
mv oem_keystore.h ../../../../bootable/bootloader/lk/platform/msm_shared/include
#5. Compile the Android boot loader.