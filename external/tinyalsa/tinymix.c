/* tinymix.c
**
** Copyright 2011, The Android Open Source Project
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of The Android Open Source Project nor the names of
**       its contributors may be used to endorse or promote products derived
**       from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY The Android Open Source Project ``AS IS'' AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL The Android Open Source Project BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
** OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
** DAMAGE.
*/

#include <tinyalsa/asoundlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

static void tinymix_list_controls(struct mixer *mixer);
static void tinymix_detail_control(struct mixer *mixer, const char *control,
                                   int print_all);
static void tinymix_set_value(struct mixer *mixer, const char *control,
                              char **values, unsigned int num_values);
static void tinymix_print_enum(struct mixer_ctl *ctl, int print_all);

int main(int argc, char **argv);
char *speaker1[]={
	"tinymix", "RX3 MIX1 INP1","RX1"};
char *speaker2[]={
	"tinymix", "SPK DAC Switch","1"};
char *speaker3[]={	
	"tinymix", "PRI_MI2S_RX Audio Mixer MultiMedia1", "1"};
char *speaker4[]={
        "tinymix" ,"RX3 MIX1 INP1" ,"ZERO"};


char *all0[]={
        "tinymix", "PRI_MI2S_RX Audio Mixer MultiMedia1", "0"};

char *all1[]={"tinymix" ,"MultiMedia1 Mixer TERT_MI2S_TX" , "0"};

char *headset1[]={
        "tinymix", "MI2S_RX Channels" , "Two"};
char *headset2[]={
       "tinymix","RX1 MIX1 INP1", "RX1"};
char *headset3[]={"tinymix" ,"RX2 MIX1 INP1", "RX2"};
char *headset4[]={"tinymix" ,"RDAC2 MUX",  "RX2"};
char *headset5[]={"tinymix" ,"HPHL","Switch" };
char *headset6[]={"tinymix" ,"HPHR", "Switch"};
char *headset7[]={"tinymix" , "PRI_MI2S_RX Audio Mixer MultiMedia1","1"};

char *headset8[]={"tinymix" ,"HPHL","ZERO" };
char *headset9[]={"tinymix" ,"HPHR", "ZERO"};



char *headsetmic11[]={"tinymix" ,"MultiMedia1 Mixer TERT_MI2S_TX" , "1"};
char *headsetmic12[]={"tinymix" ,"ADC2 Volume"  ,"6"};
char *headsetmic13[]={"tinymix" ,"DEC1 MUX" , "ADC2"};
char *headsetmic14[]={"tinymix" ,"ADC2 MUX" , "INP2"};
char *headsetmic15[]={"tinymix" ,"IIR1 INP1 MUX" ,"DEC1" };

char *headsetmic16[]={"tinymix" ,"IIR1 INP1 MUX" , "ZERO" };






char *headsetmic1[]={"tinymix" ,"MultiMedia1 Mixer TERT_MI2S_TX" , "1"};
char *headsetmic2[]={"tinymix" ,"ADC1 Volume"  ,"6" };
char *headsetmic3[]={"tinymix" ,"DEC1 MUX" , "ADC1"};
char *headsetmic4[]={"tinymix" ,"IIR1 INP1 MUX" , "DEC1" };


char *headsetmic5[]={"tinymix" ,"IIR1 INP1 MUX" , "ZERO" };


char *asistancemic1[]={"tinymix" ,"MultiMedia1 Mixer TERT_MI2S_TX" , "1"};
char *asistancemic2[]={"tinymix" ,"ADC3 Volume"  ,"6" };
char *asistancemic3[]={"tinymix" ,"DEC1 MUX" , "ADC2"};
char *asistancemic4[]={"tinymix" ,"ADC2 MUX" , "INP3" };



char *mic1[]={"tinymix" ,"MultiMedia1 Mixer TERT_MI2S_TX" , "1"};
char *mic2[]={"tinymix" ,"ADC1 Volume"  ,"19"};
char *mic3[]={"tinymix" ,"DEC1 MUX",  "ADC1"};
char *mic4[]={"tinymix" ,"DEC1 MUX",  "ZERO"};



char *earpiece1[]={"tinymix" ,"RX1 MIX1 INP1" , "RX1"};
char *earpiece2[]={"tinymix" ,"RDAC2 MUX",  "RX1" };
char *earpiece3[]={"tinymix" ,"RX1 Digital Volume" , "84"};
char *earpiece4[]={"tinymix" ,"EAR PA Gain" , "POS_6_DB" };
char *earpiece5[]={"tinymix" ,"EAR_S"  ,  "Switch" };



void audio_sel_ght(char path) 
{

      if(path == 1)
	{
  		//main(3,all0);
  		//main(3,all1);
		main(3,speaker1);
		main(3,speaker2);
		main(3,speaker3);
                main(3,headset8);
                main(3,headset9);
		main(3,mic1);
		main(3,mic2);
		main(3,mic3);
		main(3,headsetmic5);
	}
     	if(path == 2)
        {
                main(3,headset1);
                main(3,headset2);
                main(3,headset3);
                main(3,headset4);
                main(3,headset5);
                main(3,headset6);
                main(3,headset7);
		main(3,speaker4);
                main(3,mic1);
                main(3,mic2);
                main(3,mic3);
   		main(3,headsetmic5);
        }

	if(path == 3)
        {
                main(3,speaker1);
                main(3,speaker2);
                main(3,speaker3);
 		main(3,headset8);
              	main(3,headset9);                

               /***    
		main(3,headsetmic1);
		main(3,headsetmic2);
		main(3,headsetmic3);
		main(3,headsetmic4);*/
		main(3,headsetmic11);
		main(3,headsetmic12);
		main(3,headsetmic13);
		main(3,headsetmic14);
		main(3,headsetmic15);
        }
	if(path == 4)
        {
  		//main(3,all0);
  		//main(3,all1);
                main(3,headset1);
                main(3,headset2);
                main(3,headset3);
                main(3,headset4);
                main(3,headset5);
                main(3,headset6);
                main(3,headset7);
                main(3,speaker4);
                
                main(3,headsetmic11);
                main(3,headsetmic12);
                main(3,headsetmic13);
                main(3,headsetmic14);
                main(3,headsetmic15);
                main(3,speaker3);
 
        }

       if(path == 7) ///ear out
        {
                main(3,earpiece1);
                main(3,earpiece2);
                main(3,earpiece3);
                main(3,earpiece4);
                main(3,earpiece5);
                main(3,headset9);
                main(3,speaker4);


                main(3,headsetmic11);
                main(3,headsetmic12);
                main(3,headsetmic13);
                main(3,headsetmic14);
                main(3,headsetmic15);
        }
 	if(path == 8) ///ear out,1st 8 then 2 no
        {
		main(3,earpiece1);
                main(3,earpiece2);
                main(3,earpiece3);
                main(3,earpiece4);
                main(3,earpiece5);
                main(3,speaker4);

                main(3,mic1);
                main(3,mic2);
                main(3,mic3);
        }
 	if(path == 5) ///mic3.wj910
        {
		main(3,speaker1);
                main(3,speaker2);
                main(3,speaker3); 
                main(3,speaker4);

                main(3,asistancemic1);
                main(3,asistancemic2);
                main(3,asistancemic3);
                main(3,asistancemic4);
        }
 	if(path == 6) ///ear out,1st 8 then 2 no
        {
		main(3,headset1);
                main(3,headset2);
                main(3,headset3);
                main(3,headset4);
                main(3,headset5);
                main(3,headset6);
                main(3,headset7);
                main(3,speaker4); 
                main(3,speaker4);
                 main(3,asistancemic1);
                main(3,asistancemic2);
                main(3,asistancemic3);
                main(3,asistancemic4);
        }
 	if(path == 9) ///ear out,1st 8 then 2 no
        {
 		main(3,earpiece1);
                main(3,earpiece2);
                main(3,earpiece3);
                main(3,earpiece4);
                main(3,earpiece5);
                main(3,speaker4);
                main(3,speaker4);
                main(3,speaker4);

		main(3,mic1);
		main(3,mic2);
		main(3,mic3);
#if 0
                main(3,asistancemic1);
                main(3,asistancemic2);
                main(3,asistancemic3);
                main(3,asistancemic4);
#endif 
        }
}

int main(int argc, char **argv)
{
    struct mixer *mixer;
    int card = 0;

    if ((argc > 2) && (strcmp(argv[1], "-D") == 0)) {
        argv++;
        if (argv[1]) {
            card = atoi(argv[1]);
            argv++;
            argc -= 2;
        } else {
            argc -= 1;
        }
    }

    mixer = mixer_open(card);
    if (!mixer) {
        fprintf(stderr, "Failed to open mixer\n");
        return EXIT_FAILURE;
    }


    if (argc == 1) {
        printf("Mixer name: '%s'\n", mixer_get_name(mixer));
        tinymix_list_controls(mixer);
    } else if (argc == 2) {
        tinymix_detail_control(mixer, argv[1], 1);
    } else if (argc >= 3) {
        tinymix_set_value(mixer, argv[1], &argv[2], argc - 2);
    } else {
        printf("Usage: tinymix [-D card] [control id] [value to set]\n");
    }

    mixer_close(mixer);

    return 0;
}

static void tinymix_list_controls(struct mixer *mixer)
{
    struct mixer_ctl *ctl;
    const char *name, *type;
    unsigned int num_ctls, num_values;
    unsigned int i;

    num_ctls = mixer_get_num_ctls(mixer);

    printf("Number of controls: %d\n", num_ctls);

    printf("ctl\ttype\tnum\t%-40s value\n", "name");
    for (i = 0; i < num_ctls; i++) {
        ctl = mixer_get_ctl(mixer, i);

        name = mixer_ctl_get_name(ctl);
        type = mixer_ctl_get_type_string(ctl);
        num_values = mixer_ctl_get_num_values(ctl);
        printf("%d\t%s\t%d\t%-40s", i, type, num_values, name);
        tinymix_detail_control(mixer, name, 0);
    }
}

static void tinymix_print_enum(struct mixer_ctl *ctl, int print_all)
{
    unsigned int num_enums;
    unsigned int i;
    const char *string;

    num_enums = mixer_ctl_get_num_enums(ctl);

    for (i = 0; i < num_enums; i++) {
        string = mixer_ctl_get_enum_string(ctl, i);
        if (print_all)
            printf("\t%s%s", mixer_ctl_get_value(ctl, 0) == (int)i ? ">" : "",
                   string);
        else if (mixer_ctl_get_value(ctl, 0) == (int)i)
            printf(" %-s", string);
    }
}

static void tinymix_detail_control(struct mixer *mixer, const char *control,
                                   int print_all)
{
    struct mixer_ctl *ctl;
    enum mixer_ctl_type type;
    unsigned int num_values;
    unsigned int i;
    int min, max;

    if (isdigit(control[0]))
        ctl = mixer_get_ctl(mixer, atoi(control));
    else
        ctl = mixer_get_ctl_by_name(mixer, control);

    if (!ctl) {
        fprintf(stderr, "Invalid mixer control \n");
        return;
    }

    type = mixer_ctl_get_type(ctl);
    num_values = mixer_ctl_get_num_values(ctl);

    if (print_all)
        printf("%s:", mixer_ctl_get_name(ctl));

    for (i = 0; i < num_values; i++) {
        switch (type)
        {
        case MIXER_CTL_TYPE_INT:
            printf(" %d", mixer_ctl_get_value(ctl, i));
            break;
        case MIXER_CTL_TYPE_BOOL:
            printf(" %s", mixer_ctl_get_value(ctl, i) ? "On" : "Off");
            break;
        case MIXER_CTL_TYPE_ENUM:
            tinymix_print_enum(ctl, print_all);
            break;
         case MIXER_CTL_TYPE_BYTE:
            printf(" 0x%02x", mixer_ctl_get_value(ctl, i));
            break;
        default:
            printf(" unknown");
            break;
        };
    }

    if (print_all) {
        if (type == MIXER_CTL_TYPE_INT) {
            min = mixer_ctl_get_range_min(ctl);
            max = mixer_ctl_get_range_max(ctl);
            printf(" (range %d->%d)", min, max);
        }
    }
    printf("\n");
}

static void tinymix_set_value(struct mixer *mixer, const char *control,
                              char **values, unsigned int num_values)
{
    struct mixer_ctl *ctl;
    enum mixer_ctl_type type;
    unsigned int num_ctl_values;
    unsigned int i;

    if (isdigit(control[0]))
        ctl = mixer_get_ctl(mixer, atoi(control));
    else
        ctl = mixer_get_ctl_by_name(mixer, control);

    if (!ctl) {
        fprintf(stderr, "Invalid mixer control\n");
        return;
    }

    type = mixer_ctl_get_type(ctl);
    num_ctl_values = mixer_ctl_get_num_values(ctl);

    if (isdigit(values[0][0])) {
        if (num_values == 1) {
            /* Set all values the same */
            int value = atoi(values[0]);

            for (i = 0; i < num_ctl_values; i++) {
                if (mixer_ctl_set_value(ctl, i, value)) {
                    fprintf(stderr, "Error: invalid value\n");
                    return;
                }
            }
        } else {
            /* Set multiple values */
            if (num_values > num_ctl_values) {
                fprintf(stderr,
                        "Error: %d values given, but control only takes %d\n",
                        num_values, num_ctl_values);
                return;
            }
            for (i = 0; i < num_values; i++) {
                if (mixer_ctl_set_value(ctl, i, atoi(values[i]))) {
                    fprintf(stderr, "Error: invalid value for index %d\n", i);
                    return;
                }
            }
        }
    } else {
        if (type == MIXER_CTL_TYPE_ENUM) {
            if (num_values != 1) {
                fprintf(stderr, "Enclose strings in quotes and try again\n");
                return;
            }
            if (mixer_ctl_set_enum_by_string(ctl, values[0]))
                fprintf(stderr, "Error: invalid enum value\n");
        } else {
            fprintf(stderr, "Error: only enum types can be set with strings\n");
        }
    }
}

