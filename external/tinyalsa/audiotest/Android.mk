###LOCAL_PATH:= $(call my-dir)


#########################################################################
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)
LOCAL_C_INCLUDES:= external/tinyalsa/include
LOCAL_SRC_FILES:=  ../tinymix.c
LOCAL_MODULE := libtinymix
LOCAL_SHARED_LIBRARIES:= libcutils libutils libtinyalsa
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)
LOCAL_PRELINK_MODULE := false
###include $(BUILD_HOST_STATIC_LIBRARY)
include $(BUILD_SHARED_LIBRARY)

###include audiotest/Android.mk
############################################
include $(CLEAR_VARS)

LOCAL_SRC_FILES:=         \
        SineSource.cpp    \
        audiotest.cpp

LOCAL_MODULE:= atest
LOCAL_SHARED_LIBRARIES := \
        libstagefright liblog libutils libbinder libstagefright_foundation libtinymix

LOCAL_C_INCLUDES:= \
        frameworks/av/media/libstagefright \
        $(TOP)/frameworks/native/include/media/openmax

LOCAL_CFLAGS += -Wno-multichar

LOCAL_MODULE_TAGS := optional

###LOCAL_MODULE:= atest

include $(BUILD_EXECUTABLE)

############################################################################################

################################################################################
