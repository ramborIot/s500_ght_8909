
#ifndef __PRNT_FCK_H__
#define __PRNT_FCK_H__

#include "prnt_mgnt.h"


int fck_prnt_init( int step );
int fck_prnt_start( int step );
int fck_prnt_get_status( void );
int fck_prnt_printf( char *fmt, ... );
int fck_prnt_graphics( int offset, char *path );
int fck_prnt_command( unsigned char *cmd, int len );
int fck_prnt_matrix( int offset, int w, int h, unsigned char *data, int len );
int fck_prnt_fill_data( unsigned char *data, int len );
int fck_prnt_set_gray( int gray );




#endif

