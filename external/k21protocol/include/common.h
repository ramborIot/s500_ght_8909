/***********************************************************
*
*      File name   : common.h
*      Description : common defintion
*      Date        :
*      Revision    : V.1.00
*      Author      :
*      Copyright (c) 2013   Inc.                  
*
************************************************************/
#ifndef __COMMON_H__
#define __COMMON_H__

/***********************************************************
*
*     Macro definition
*
************************************************************/

//#define DEBUG_MODE   1
//#define TEST_MODE   1
//#define LIB_TEST_MODE   1
#define VRFN_DRV_SUPPORT 1                          /* Verifone driver support */
#define MATRIX_CCODE_ATTR_SUPPORT   1

/* type defininition */
typedef unsigned long long u64;
typedef unsigned int u32;
typedef unsigned short u16;
typedef unsigned char u8;
typedef signed int s32;
typedef short s16;
typedef signed char s8;

/* status definition */
#define     ON          1
#define     OFF         0

#define     N_ON        0
#define     N_OFF       1

//#define     NULL        0

#define     FALSE       0
#define     TRUE        1

#define     DISABLE     0
#define     ENABLE       1

#define     OK          0
#define     ERROR       (-1)
#define     ERROR_INVALID		(-2)
#define     ERROR_BUSY		(-3)
#define     ERROR_FAIL		(-4)


/***********************************************************
*
*      Public variable declaration
*
************************************************************/


/***********************************************************
* 
*      Public function declaration                         
*
************************************************************/


#endif

