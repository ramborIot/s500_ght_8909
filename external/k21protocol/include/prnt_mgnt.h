/***********************************************************
*
*      File name   : prnt_mgnt.h
*      Description : printer management functions
*      Date        :
*      Revision    : V.1.00
*      Author      :
*      Copyright (c) 2013   Inc.                  
*
************************************************************/
#ifndef __PRNT_MGNT_H__
#define __PRNT_MGNT_H__

#include <android/log.h>
#include "prnt_str.h"

/***********************************************************
*
*     Macro definition
*
************************************************************/

#ifdef TEST_MODE
#define PRNT_DEV_NAME       "sss"
#else
#define PRNT_DEV_NAME       "/dev/printer"
#endif

// #define DEBUG_MODE

// #define printf(...) 	__android_log_print(ANDROID_LOG_INFO, "Printer",##__VA_ARGS__);

#ifdef DEBUG_MODE

#define printf(...) 	do{\
							FILE *FP; \
							FP = fopen("/storage/sdcard0/log.txt", "a+"); \
							fprintf(FP, ##__VA_ARGS__); \
							fclose(FP); \
						}while(0)
#endif

/* mechanical */
#define PRNT_DOT_MAX            384					/* Total dots per line */
#define PRNT_DOT_BYTE_MAX           (PRNT_DOT_MAX/8)
// #define PRNT_DOT_BYTE_CMD_MAX    (PRNT_DOT_BYTE_MAX+2)  /* 50, add 2 cmd bytes for Verifone driver */
#define PRNT_DOT_BYTE_CMD_MAX    (PRNT_DOT_BYTE_MAX)  /* 50, add 2 cmd bytes for Verifone driver */

#define PRNT_PAPER_WIDTH    58		/* mm */
#define PRNT_PRINT_WIDTH    48		/* mm */

#define PRNT_DOT_PER_MM_W   8
#define PRNT_DOT_PER_MM_H   16


#define PRNT_PRINT_DEPTH_1  1
#define PRNT_PRINT_DEPTH_2  2
#define PRNT_PRINT_DEPTH_3  3
#define PRNT_PRINT_DEPTH_4  4
#define PRNT_PRINT_DEPTH_DEFAULT    PRNT_PRINT_DEPTH_1

#define PRNT_TERMINAL_ID     "P"

/* Line attributes */
#define PRNT_LEFT_SPACE_DEFAULT             0					/* dot */
#define PRNT_RIGHT_SPACE_DEFAULT            0
#define PRNT_LINE_DOT_MAX_DEFAULT       PRNT_DOT_MAX
#define PRNT_LINE_HIGHT_DEFAULT             PRNT_CHAR_FULL_HIGHT_DEFAULT
#define PRNT_LINE_HIGHT_MIN             8
#define PRNT_LINE_HIGHT_MAX             64
#define PRNT_LINE_DOUBLE_HIGHT_DEFAULT      FALSE
#define PRNT_LINE_DOUBLE_WIDTH_DEFAULT      FALSE

/* Character attributes */
#define PRNT_CHAR_DOUBLE_HIGHT_DEFAULT	FALSE
#define PRNT_CHAR_DOUBLE_WIDTH_DEFAULT	FALSE
#define PRNT_CHAR_HALF_HIGHT_DEFAULT             24
#define PRNT_CHAR_HALF_WIDTH_DEFAULT             16
#define PRNT_CHAR_FULL_HIGHT_DEFAULT             24
#define PRNT_CHAR_FULL_WIDTH_DEFAULT             24

#define PRNT_CHAR_FULL_HIGHT_MAX 	( 64 )
#define PRNT_CHAR_FULL_HIGHT_MAX	( 64 )


/* other */
#define PRNT_INVERSE_MODE_DEFAULT   FALSE

/* print command result */
#define PRNT_PRINT_DATA_OK      0
#define PRNT_PRINT_DATA_LEFT     1

#define PRNT_STRING_LEFT_MAX            PRNT_DOT_MAX / PRNT_FONT_DOT_WIDTH_MIN
#define PRNT_STRING_LEFT_BUF_SIZE   PRNT_DOT_MAX / PRNT_FONT_DOT_WIDTH_MIN + 1

#define PRNT_STRING_INPUT_MAX             1024

#define PRN_DRV_TYPE_PRINT     		0x55          	//�����д�ӡ����
#define PRN_DRV_TYPE_FEED      		0xAA          	//�����н�ֽ����

enum PRINTING_STATUS
{
	STATUS_IDLE=0x00,	
	STATUS_PRINTING,
	STATUS_STEPING,
	STATUS_NOPAPER,
	STATUS_OVERTEMP,
	STATUS_ERROR,	
	STATUS_FINISH,	
	STATUS_CANCE,
};


typedef struct _print_matrix_attr_t {
    int offset;		//Offset of x
    int height;	    //height of the font
    int width; 		//width of the font
    int blank; 		//blank dot line after print
    int font_attribute; //NOT supported - 20131108
} print_matrix_attr_t;


/***********************************************************
*
*      Public variable declaration
*
************************************************************/

extern unsigned char str_left[PRNT_STRING_LEFT_BUF_SIZE];
extern prnt_char_attr_t *pstr_left_parsed;
extern int str_left_num;
extern int print_speed;

/* Line attributes */
extern int left_margin;
extern int right_margin;

extern int line_hight_cfg;
extern int line_double_hight;
extern int line_double_width;

extern int line_hight;

/* Character attributes */
extern int chr_inverse_mode;
extern int chr_half_font_lib_idx;
extern int chr_full_font_lib_idx;
extern int chr_half_hight_cfg;
extern int chr_full_hight_cfg;
extern u8 chr_double_hight;
extern int chr_half_width_cfg;
extern int chr_full_width_cfg;
extern u8 chr_double_width;


/***********************************************************
* 
*      Public function declaration                         
*
************************************************************/

int set_char_attr_default (void);
int set_printer_attr_default (void);

int printer_open (const char* filename);
int printer_close (int fd);
int printer_read (int fd, const char* cmd, void* result, int* type, int max_len);
int printer_command (int fd, unsigned char* cmd, int len);
int printer_graphics (int fd, int xOffset, const char* bmpFilePath);
int printer_printf(int fd, char* state, const char *format, ...);
int printer_matrix (int fd, print_matrix_attr_t* attr, unsigned char* data, int length);

void showVarsInTerminal(void);
int showBufferInTerminal(unsigned char*  data, int height, int width, int flag);

#endif
