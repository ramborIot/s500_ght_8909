#ifndef  _LOAD_FONT_H_
#define  _LOAD_FONT_H_

int InitFontLib();

int SetFontsize(int pixel);

int FillBmp(char *text);

unsigned char *GetBmpBuffer();

int PirnterGetDotLines();

void showBmp();

#endif