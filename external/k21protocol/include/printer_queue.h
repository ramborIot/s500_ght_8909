#ifndef __PRINTER_QUEUE_H__
#define __PRINTER_QUEUE_H__

typedef enum {
	PRT_ERR_START = -90000,
	PRT_ERR_NOT_INITED, // -89999
	PRT_ERR_PARAM, // -89998
	PRT_ERR_NULL, // -89997
	PRT_ERR_NODATA, // -89996
	PRT_ERR_NOPAPER, // -89995
	PRT_ERR_OVERHEAT, // -89994
	PRT_ERR_LOW_VOLT, // -89993
	PRT_ERR_HIGH_VOLT,  // -89992
	PRT_ERR_BUF_FULL, // -89991
	PRT_ERR_DIRTY_DATA, // -89990
	PRT_ERR_BUSY, // -89989
	PRT_ERR_CONTROL, // -89988
	PRT_ERR_DAMAGE,  // -89987

    PRT_OK = 0,                           /**< Operation succeed */
} PRT_Return_t;


int prtQueInit(void);
/*
fill dot line data to print buffer
not use in ISR, need cirtical
*/
int prtQueFillBuf(char *buf, int len);
/*
used in ISR
*/
int prtQueGetBufFromISR(char *buf, int len);
/*
NOT USE
*/
int prtQueGetBuf(unsigned char *buf, int len);
/*
check how many dot line can be fill
used NOT in ISR
*/
int prtQueCheckCapacity(void);
/*
check how many dot line exist
used only in ISR, needn't critical 
*/
int prtQueCheckLines(void);
/*
reset the prn buffer
*/
int prtQueReset(void);


#endif

