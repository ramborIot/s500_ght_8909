#ifndef _DEBUG_H_
#define _DEBUG_H_

#include <android/log.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define DEBUG

#ifdef DEBUG
#define TAG		"ddiapi" 		

#define LOG_PREFIX 		"ddiapi debug info[%s:%s:%d]"

#define LOG(...)	do {\
								char info[4096];\
								char *p;\
								memset(info, 0x00, sizeof(info));\
								sprintf(info, LOG_PREFIX, __FILE__,__FUNCTION__,__LINE__);\
								p = strstr(info, "jni/");\
								if(p != NULL)\
								{\
									p=p+4;\
								}\
								else \
								{ \
									p = info;\
								}\
								__android_log_print(ANDROID_LOG_INFO, TAG,"%s",p);\
								__android_log_print(ANDROID_LOG_INFO, TAG,##__VA_ARGS__);\
							}while(0)

#define LOGHEX(hint, Array, len) do{\
										char logbuf[4096];\
										int m = 0;\
										memset(logbuf, 0x00, sizeof(logbuf)); \
										strcpy(logbuf, hint);\
										sprintf(logbuf+strlen(logbuf), "len(%d)\n", len); \
										for(m=0;m<(len);m++)\
										{\
											if(m>=2048) \
											{ \
												break; \
											} \
											sprintf(logbuf+strlen(logbuf),"%02X ",(Array)[m]);\
										}\
										\
										LOG("%s",logbuf);\
									}while(0)
#else
#define LOG(...)

#define LOGHEX(hint, Array, len) 
#endif

#endif

