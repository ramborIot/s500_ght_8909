/***********************************************************
*
*      File name   : prnt_str.h
*      Description : printer print string functions
*      Date        :
*      Revision    : V.1.00
*      Author      :
*      Copyright (c) 2013   Inc.                  
*
************************************************************/
#ifndef __PRNT_STR_H__
#define __PRNT_STR_H__

#include "common.h"

/***********************************************************
*
*     extern 
*
************************************************************/
extern int getHexData(int fontIndex, const char* pChar, unsigned char** data, int* lengthOfData);


/***********************************************************
*
*     Macro definition
*
************************************************************/

/* ASCII */
#define ASCII_S	0x20
#define ASCII_E	0x7E

/* GB18030-2005 code value range */
#define GB18030_2_1_1_S	0xB0
#define GB18030_2_1_1_E	0xF7
#define GB18030_2_1_2_S	0xA1
#define GB18030_2_1_2_E	0xFE
#define GB18030_2_2_1_S	0x81
#define GB18030_2_2_1_E	0xA0
#define GB18030_2_2_2_S	0x40
#define GB18030_2_2_2_E	0xFE
#define GB18030_2_3_1_S	0xAA
#define GB18030_2_3_1_E	0xFE
#define GB18030_2_3_2_S	0x40
#define GB18030_2_3_2_E	0xA0
#define GB18030_4_1_1_S	0x81
#define GB18030_4_1_1_E	0x82
#define GB18030_4_1_2_S	0x30
#define GB18030_4_1_2_E	0x39
#define GB18030_4_1_3_S	0x81
#define GB18030_4_1_3_E	0xFE
#define GB18030_4_1_4_S	0x30
#define GB18030_4_1_4_E	0x39
#define GB18030_4_2_1_S	0x95
#define GB18030_4_2_1_E	0x98
#define GB18030_4_2_2_S	0x30
#define GB18030_4_2_2_E	0x39
#define GB18030_4_2_3_S	0x81
#define GB18030_4_2_3_E	0xFE
#define GB18030_4_2_4_S	0x30
#define GB18030_4_2_4_E	0x39


/* Character attributes */

#define CTRL_CODE_ASCII			       0           /* ASCII Control code */
#define CHAR_CODE_ASCII			1           /* ASCII 0x20~0x7f */
#define CHAR_CODE_EXT1                      2           /* 2 bytes chinese */
#define CHAR_CODE_EXT2                      4           /* 4 bytes chinese */
#define CHAR_CODE_MAX                       CHAR_CODE_EXT2
#define CTRL_CODE_ASCII_LENGTH	1   /* byte */
#define CHAR_CODE_ASCII_LENGTH	1   /* byte */
#define CHAR_CODE_EXT1_LENGTH       2
#define CHAR_CODE_EXT2_LENGTH       4
#define CHAR_CODE_LENGTH_MAX        CHAR_CODE_EXT2_LENGTH


/* Font */
#define EN_16_FID     0x01     /* 16*8  EN */
#define CN_16_FID     0x02     /* 16*16 CN */
#define EN_24_FID     0x03     /* 24*16 EN */
#define CN_24_FID     0x04     /* 24*24 CN */
#define EN_12_FID     0x05     /* 12*6 EN */
#define CN_12_FID     0x06     /* 12*12 CN */
#define EN_8_FID      0x07     /* 8*8 EN */

#define PRNT_HALF_FONT_DEFAULT      EN_24_FID
#define PRNT_FULL_FONT_DEFAULT      CN_24_FID

#define PRNT_FONT_DOT_HIGHT_MIN     8
#define PRNT_FONT_DOT_WIDTH_MIN     4
#define PRNT_FONT_DOT_HIGHT_MAX     64
#define PRNT_FONT_DOT_WIDTH_MAX     64
#define PRNT_FONT_DOT_BUF_MAX       PRNT_FONT_DOT_HIGHT_MAX*PRNT_FONT_DOT_WIDTH_MAX/8



typedef struct _prnt_char_attr_t {
    int flag;
    int idx;
    int chr_type;
    int chr_len;
    u8 chr_code[CHAR_CODE_LENGTH_MAX];
    int chr_font_lib_idx;
    int chr_hight_cfg;
    int chr_width_cfg;
    u8 chr_double_hight;
    u8 chr_double_width;
    int chr_hight;
    int chr_width;
    int chr_inverse_mode;
    u8 chr_dot_buf[PRNT_FONT_DOT_BUF_MAX];
} prnt_char_attr_t;

typedef struct _prnt_font_attr_t {
    int id;
    int hight;
    int width;
}prnt_font_attr_t;



/***********************************************************
*
*      Public variable declaration
*
************************************************************/




/***********************************************************
* 
*      Public function declaration                         
*
************************************************************/
int is_print_char_type (int type);
int reverse_byte (unsigned char *p, unsigned short len);
int string_parse (unsigned char *pstr, int len, prnt_char_attr_t *pparsed, int *num, int sstr, int sparsed);
int matrix_parse (unsigned char *pstr, int cMun, prnt_char_attr_t *pparsed, int cHeight, int cWidth);

#endif

