
#ifndef __FILL_MONO_BMP2DOTBUF_H__
#define __FILL_MONO_BMP2DOTBUF_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_DOT_LINE		( 1600 )
static unsigned char k_DotBuf[MAX_DOT_LINE][48];

static int k_CurDotCol = 0;
static int k_CurDotLine = 0;

#define uchar unsigned char
#define ushort unsigned short
#define uint unsigned int

#define PRN_OUTOFMEMORY		( -404 )

#define BITMAP_PIXEL_1 		( 1 )
#define PRN_LINE_PIXEL_MAX	( 384 )
/*********************************************************************
*  Function Name: 	FillImage2DotBuf
* 
*  Parameter:
*    const char *ImageFile
*    uchar ImageType
*
*  Return: 
*     0 - success
*	 -1 - invalid file name
*	 -2 - invalid file type
*	 -3 - unable to parse file
*
*  Description: API, print image
*
*********************************************************************/
//static int FillMonoBMP2DotBuf(const char *ImageFile)
int BmpToDotBuffer(char *filepath);


unsigned char *GetDotBuffer(void);

int GetLineCount(void);


#endif
