
#define FT_FREETYPE_H

#include <errno.h>
#include <string.h>

extern "C"{
#include <ft2build.h>
#include FT_FREETYPE_H

#include "freetype.h"
}

#include "loadfont.h"
#include "debug.h"

#define FONTFACE        "/storage/sdcard0/simfang.ttf"


  FT_Library    library;
  FT_Face       face;
int fontsize = 16;

int row = 0;

int x =0;
int y = 0;

#define MAX_DOT_LINE        48
#define MAX_LINE        16000
unsigned char printerBuf[MAX_LINE][MAX_DOT_LINE];

void SetY(int offset)
{
    y = offset;
}

void resetPrinterBuf()
{
    memset(printerBuf, 0x00, sizeof(printerBuf));
}

int InitFontLib()
{
    int error = 0;

    error = FT_Init_FreeType( &library);

    if ( error )
    {
        LOG("FT_Init_FreeType failed:%d", error);
        goto OUT;   
    }

    error = FT_New_Face( library, FONTFACE,0,&face );
    LOG("!!!!!!!!!!!!!!!FT_New_Face return:%d", error);

    if ( error == FT_Err_Unknown_File_Format )
    {
        LOG("the font file could be opened and read, but it appears,that its font format is unsupported!!");   
    }
    else if ( error )
    {
        LOG("another error code means that the font file could not be opened or read, or that it is broken...!!");   
    }

OUT:
    return error;
}

void releaseFace()
{
    FT_Done_Face    ( face );
    FT_Done_FreeType( library );
}

int SetFontsize(int pixel)
{
    int error = 0;

    if(face == NULL)
    {
        error = -1;
        LOG("face is NULL!!!");
        goto OUT;
    }

    fontsize = pixel;
    error = FT_Set_Pixel_Sizes(
            face,   /* handle to face object */
            0,      /* pixel_width           */
            pixel );   /* pixel_height          */
    OUT:
    return error;
}


int draw_bitmap(FT_Bitmap*  bitmap)
{
  FT_Int  i, j, p, q;
  FT_Int  height = y+bitmap->rows;
  int left, right, top, bottom;

    if(x>=MAX_DOT_LINE)
    {
        LOG("over flow!!!mx is:%d cur:%d", MAX_DOT_LINE, x);
        // x = 0;
        return -1;
    }

    // p = bitmap->pitch * 2;
    // left = (p - bitmap->width)/2 - 1;

    // if(left < 0)
    // {
    //     left = 0;
    // }

    // right = bitmap->width+left+1;

    // top = (fontsize - bitmap->rows)/2 - 1;

    // if(top < 0)
    // {
    //     top = 0;
    // }

    // bottom = bmtmap->rows + top + 1;

    // for(i = top; i<bottom; i++)
    // {
    //     for(j = left; j<right; j++)
    //     {
    //         printerBuf[y+i][] |= 
    //     }
    // }

    for ( j = y; j < height; j++)
    {
       LOGHEX("buffer:", bitmap->buffer+j*bitmap->pitch, bitmap->pitch);
       memcpy(printerBuf[j]+x*bitmap->pitch, bitmap->buffer+j*bitmap->pitch, bitmap->pitch);
    }

    // x+=bitmap->pitch;

    return 0;
}


int PirnterGetDotLines()
{
    return y;
}


int FillBmp(char *text)
{
    int error = 0;
    int i = 0;
    int offset = 0;
    FT_GlyphSlot  slot;
    FT_UInt  glyph_index;

    InitFontLib();
    slot = face->glyph;  /* a small shortcut */
   SetFontsize(16);
    LOG("Y:%d------text is:%s", y,text);

    for(i = 0; i<strlen(text); i++)
    {
 #if 1  
    #if 0     
        error = FT_Load_Char( face, text[i], FT_LOAD_MONOCHROME | FT_LOAD_RENDER );

        if ( error )
        {
            LOG("FT_Load_Char failed:%d", error);
            continue;  /* ignore errors */
        }

        FT_Render_Glyph( slot, FT_RENDER_MODE_MONO);
    #else
        FT_ULong  charcode;
        FT_UInt   gindex;

        charcode = FT_Get_First_Char( face, &gindex );

        while ( gindex != 0 )
        {
            charcode = FT_Get_Next_Char( face, charcode, &gindex );
        }
/* load glyph image into the slot (erase previous one) */
        error = FT_Load_Glyph( face, glyph_index, FT_LOAD_MONOCHROME );

        if ( error )
        {
            LOG("FT_Load_Glyph failed:%d", error);
            continue;  /* ignore errors */
        }

        /* convert to an anti-aliased bitmap */
        error = FT_Render_Glyph( face->glyph, FT_RENDER_MODE_MONO );

        if ( error )
        {
            LOG("FT_Render_Glyph failed:%d", error);
            continue;  /* ignore errors */
        }
    #endif
  #else
        /* retrieve glyph index from character code */
        glyph_index = FT_Get_Char_Index( face, text[i] );
        LOG("%c==%ld", text[i], glyph_index);

        /* load glyph image into the slot (erase previous one) */
        error = FT_Load_Glyph( face, glyph_index, FT_LOAD_MONOCHROME );

        if ( error )
        {
            LOG("FT_Load_Glyph failed:%d", error);
            continue;  /* ignore errors */
        }

        /* convert to an anti-aliased bitmap */
        error = FT_Render_Glyph( face->glyph, FT_RENDER_MODE_MONO );

        if ( error )
        {
            LOG("FT_Render_Glyph failed:%d", error);
            continue;  /* ignore errors */
        }

  #endif      
     
        error = draw_bitmap(&slot->bitmap);
        x += slot->bitmap.pitch;

        if(error != 0)
        {
            goto OUT;
        }
    }

OUT:
    x = 0;
    y += fontsize;
    releaseFace();
    return offset;
}

unsigned char *GetBmpBuffer()
{
    // showBmp();
    return (unsigned char *)printerBuf;
}

void showBmp()
{
    FILE *FP = 0;
    int i = 0;
    int j = 0;
    int n = 0;

    FP = fopen("/storage/emulated/0/bmpinfo.txt", "w+");

    if(FP == NULL)
    {
        LOG("open file failed:%s", strerror(errno));
        return ;
    }

    for(i=0;i<y;i++)
    {
        for(j = 0; j<48; j++)
        {
            for(n = 7;n>=0;n--)
            {
                fprintf(FP, "%c", (((printerBuf[i][j]>>n) & 0x01) == 0) ?' ':'*');
            }
        }

        fprintf(FP, "%s", "\n");
    }

    fclose(FP);
}
