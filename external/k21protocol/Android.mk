# This is the Android makefile for K21 SPI so that we can
# build it with the Android NDK.


LOCAL_PATH := $(call my-dir)
 
 

include $(CLEAR_VARS)

LOCAL_C_INCLUDES:=  \
					$(LOCAL_PATH) \
					$(LOCAL_PATH)/include \
					$(LOCAL_PATH)/printer \
					$(LOCAL_PATH)/printer/esc \
					$(LOCAL_PATH)/freetype2/src \
					$(LOCAL_PATH)/freetype2/include \
					$(LOCAL_PATH)/freetype2/include/freetype \
					$(LOCAL_PATH)/libiconv-1.14/include/ \

					
					
LOCAL_PREBUILT_LIBS := libspipack.so 

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE := libspipack


# wildcard printer/*.c wildcard printer/esc/*.c)

$(warning $(LOCAL_SRC_FILES))

LOCAL_SRC_FILES := spidev_pack.c \
	devApi.c\
	loadfont.cpp \
	safety_jni.cpp \
	rsalib/des.c\
	rsalib/digit.c\
	rsalib/nn.c\
	rsalib/md5.c\
	rsalib/prime.c\
	rsalib/r_dh.c\
	rsalib/r_encode.c\
	rsalib/r_enhanc.c\
    rsalib/r_keygen.c\
    rsalib/r_random.c\
    rsalib/rsa.c\
    rsalib/rsalib.c\
    rsalib/r_stdlib.c\
	rsalib/softhash.c \
	printer/conf.c \
	printer/genBitmapFromChar.c \
	printer/esc/printer_queue.c \
	printer/esc/prnt_cmd.c \
	printer/esc/prnt_drv.c \
	printer/esc/prnt_fck.c \
	printer/esc/prnt_img.c \
	printer/esc/prnt_mgnt.c \
	printer/esc/prnt_str.c \
	printer/esc/queue.c \
	uart_dowload.c \

	

#LOCAL_LDFLAGS += $(LOCAL_PATH)/lib/libfreetype2-static.a
LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils \
	liblog \
	libfreetype2 \
	libiconv
	
include $(BUILD_SHARED_LIBRARY)

include $(call all-makefiles-under,$(LOCAL_PATH))



