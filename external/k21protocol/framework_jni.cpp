
#define LOG_NDEBUG 0

#undef LOG_TAG
#define LOG_TAG "Ddi_jni"


#include <stdio.h>  
#include <stdlib.h>  
#include <unistd.h>  
#include <sys/types.h>  
#include <sys/stat.h>  
#include <fcntl.h>  
#include <assert.h>  
#include "jni.h"  
// #include "JNIHelp.h"  
// #include "android_runtime/AndroidRuntime.h"  

//#include "SkBitmap.h"
//#include "SkDevice.h"
//#include "SkPaint.h"
//#include "SkRect.h"
//#include "SkImageEncoder.h"
//#include "SkTypeface.h"
//#include "SkCanvas.h"

extern "C" {
#include "devApi.h"
//#include <android/log.h>
}

//#define LOG_TAG "kris_ril"
//#include <utils/Log.h>

// #include <utils/Log.h>
// #include <utils/String16.h>
//#define REPORT_FUNCTION() ALOGD("%s....................................................................\n", __PRETTY_FUNCTION__)
#define REPORT_FUNCTION()
#define ATR_LEN 33
#define SN_LEN 17
#define ID_LEN 17
#define RANDOM_LEN 4
#define PASSWARD_LEN 6
#define APDU_MAX_LEN   512
#define TRACK1_LEN 79+1
#define TRACK2_LEN 37+1
#define TRACK3_LEN 104+1
#define IMG_MAX_LEN 384*240
#define IMG_NAME_MAX_LEN 256
#define PRN_TEXT_MAX_LEN 256
#define COMB_PRN_MAX  256
#define COMB_PRN_TEXT_MAX_LEN 256
#define INNER_KEY_MAX_LEN 16
#define INNER_DATA_MAX_LEN 1028
#define CERT_DATA_MAX_LEN 10000
#define DUKPT_KEY_MAX_LEN 24
#define DUKPT_SN_MAX_LEN 20
#define PINBLOCK_LEN 8    //000
#define COM_MAX_LEN   1024
#define HASH_LEN 142
#define SIGISSUSE_LEN 48
#define VERSION_LEN 22
#define HASH1_LEN 72
#define SIG_LEN 518
#define HASHDATA_LEN SIGISSUSE_LEN+VERSION_LEN+HASH1_LEN+SIG_LEN+2
#define IOCTRL_MAX_LEN   512
#define SECURE_STATU_LEN 12
static void sub_sys_init(JNIEnv *env, jobject obj)
{
	spi_ddi_sys_init();
}



static jint sub_ddi_sys_getCertHash(JNIEnv *env, jobject obj,jbyteArray hash)
{
	int ret,i,plen;
	jbyte *hash_j;
	
	unsigned char hash_c[HASH_LEN];
	plen = env->GetArrayLength(hash);
	if(plen<HASH_LEN)
		return -10;
	
	hash_j = env->GetByteArrayElements(hash, false);
	ret = ddi_sys_getCertHash(hash_c);
	for(i=0;i<HASH_LEN;i++)
	{
		hash_j[i] = hash_c[i];
	}
	env->ReleaseByteArrayElements(hash, hash_j, 0);
	
	return ret;
}



static jint sub_ddi_get_debugStatus(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_get_debugStatus();
	
	return ret;
}

static JNINativeMethod gMethods[] = {  

	{"ddi_ddi_sys_init", "()V", (void *)sub_sys_init}, 
	{"ddi_sys_getCertHash", "([B)I", (void *)sub_ddi_sys_getCertHash},  
	{"ddi_get_debugStatus", "()I", (void *)sub_ddi_get_debugStatus}, 
};  

#define NELEM(a) sizeof((a)) / sizeof(JNINativeMethod)

static int register_xinguodu_ddi(JNIEnv *env)  
{  
	// return android::AndroidRuntime::registerNativeMethods(env, "com/xinguodu/ddiframework/Ddi", gMethods, NELEM(gMethods));  
		jclass ddiclaz;
	int ret = 0;

	ddiclaz = env->FindClass("com/jolly/s500/Ddi");
	
	ret =  env->RegisterNatives(ddiclaz, gMethods, NELEM(gMethods));  

	env->DeleteLocalRef(ddiclaz);
	return ret;
}  

#if 0
static jint JNI_OnLoad(JavaVM *vm, void *reserved)  
{  
	JNIEnv *env = NULL;  
	if (vm->GetEnv((void **)&env, JNI_VERSION_1_4) != JNI_OK) {  
		printf("Error GetEnv\n");  
		return -1;  
	}  
	assert(env != NULL);  
	if (register_xinguodu_ddi(env) < 0) {  
		printf("register_xinguodu_ddi error.\n");  
		return -1;  
	}  
	return JNI_VERSION_1_6;  
}
#endif
