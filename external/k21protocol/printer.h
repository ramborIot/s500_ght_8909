#ifndef _PRINTER_H
#define _PRINTER_H



#ifndef BYTE
#define BYTE unsigned char
#endif

#ifndef WORD
#define WORD unsigned short
#endif

#ifndef DWORD
#define DWORD unsigned long
#endif
/*
=============================================================================
	PRN_STB1  ------- GPIO23
	PRN_STB234------- GPIO24
	PRN_STB56 ------- GPIO25
	
	PRN_MTA  ------- GPIO16
	/PRN_MTA ------- GPIO17
	PRN_MTB  ------- GPIO18
	/PRN_MTB ------- GPIO19

	/PRN_PWR ------- GPIO22
	PRN_LAT  ------- GPIO21

	PRN_DATA ------- MOSI
	PRN_SCLK ------- SCK
	PRN_SPI_CS ----- SS1

	PRN_SENSE ------- GPIO26

    PRN_TM   -------- ADC_IN2
    PRN_CO   -------- ADC_IN3

	注： 打印机与IC卡共用SPI模块，只是CS片选不同。

==============================================================================
*/
//#define VPOS312_V40   //针对v4.0以上的机器需要定义这个常量，如果是4.0以内的机器，需要手工加上反定义 2012-03-10.
#undef VPOS312_V40
#ifdef VPOS312_V40
#define  PRN_STB1       ((unsigned short)1<<(24-16))
#define  PRN_STB2       ((unsigned short)1<<(23-16))
#define  PRN_STB3       ((unsigned short)1<<(23-16))
#define  PRN_STB4       ((unsigned short)1<<(24-16))
#define  PRN_STB5       ((unsigned short)1<<(25-16))
#define  PRN_STB6       ((unsigned short)1<<(25-16))
#else //VPOS312_V1.0,3.0
#define  PRN_STB1       ((unsigned short)1<<(23-16))
#define  PRN_STB2       ((unsigned short)1<<(24-16))
#define  PRN_STB3       ((unsigned short)1<<(24-16))
#define  PRN_STB4       ((unsigned short)1<<(24-16))
#define  PRN_STB5       ((unsigned short)1<<(25-16))
#define  PRN_STB6       ((unsigned short)1<<(25-16))
#endif

#define  PRN_MTAP       ((unsigned short)1<<(16-16))
#define  PRN_MTAS       ((unsigned short)1<<(17-16))
#define  PRN_MTBP       ((unsigned short)1<<(18-16))
#define  PRN_MTBS       ((unsigned short)1<<(19-16))
//#define  PRN_POUT       (1<<3)
#define  PRN_PWR        ((unsigned short)1<<(22-16)) 

#define  PRN_LAT        ((unsigned short)1<<(21-16)) 
//#define  PRN_MCTR       ((unsigned short)1<<(27-16))

#define  PRN_SENSE      ((unsigned short)1<<(26-16)) 
#define  PRN_TM_ADC     (1<<2)
#define  PRN_CO_ADC     (1<<3)



#define  PRN_SS1        ((unsigned short)1<<4) 
//add on ding 2009-11-28
#define PRN_STAT	    ((unsigned short)1<<12) 


//end
#define   LOW              0
#define   HIGH             1

#define PRN_OUT_VALUE    800// 920   //max 1024   min 500~700 mod by ding 20100116
#define STEP_TIME_BASE   8000
/******   error no   **********************************************/

#define PRN_BUSY                 (-4001)
#define PRN_NOPAPER              (-4002)
#define PRN_DATAERR              (-4003)
#define PRN_FAULT                (-4004)
#define PRN_TOOHEAT              (-4005)
#define PRN_UNFINISHED           (-4006)
#define PRN_NOFONTLIB            (-4007)
#define PRN_BUFFOVERFLOW         (-4008)
#define PRN_NOBLACKFLAG          (-4009)

#define PRN_OK                   (0)

#endif //_PRINTER_H


