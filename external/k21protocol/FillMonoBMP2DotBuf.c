
#include "FillMonoBMP2DotBuf.h"
/*********************************************************************
*  Function Name: 	FillImage2DotBuf
* 
*  Parameter:
*    const char *ImageFile
*    uchar ImageType
*
*  Return: 
*     0 - success
*	 -1 - invalid file name
*	 -2 - invalid file type
*	 -3 - unable to parse file
*
*  Description: API, print image
*
*********************************************************************/
//static int FillMonoBMP2DotBuf(const char *ImageFile)
int BmpToDotBuffer(char *filepath)
{
	uchar  *fileBuffer;
	ushort bitsPerPixel = 0;
	int  handle      = 0;
	int  len         = 0;
	int  iWReal      = 0;
	int  iHReal      = 0;
	int  iW          = 0;
	int  iH          = 0;
	int  iWOmit      = 0;
	int  iHOmit      = 0;
	int  x           = 0;
	int  y           = 0;
	int  iCnt        = 0;
	int  temp        = 0;
	int  i           = 0;
	int  m           = 0;
	int  n           = 0;
	int  skip        = 0;
	int  residue     = 0;
	int  rowByte     = 0;
	int  LeftBytes   = 0;
	int  LeftBits    = 0;
	uchar  *pDotBuf  = NULL;
	uchar  *pBitmap  = NULL;
	uchar  medium    = 0;
	uchar  dotTmp    = 0;
	int  lastDotLine = 0;
	int j;


    FILE *fp = NULL;
    unsigned char *pData;
    int iLen, itmpLen, itmp;

	k_CurDotLine = 0;

	
    fp = fopen(filepath, "rb");
    if(fp == NULL)
    {
//        printf("fopen err\n");
        goto end;
    }   
	
    fseek(fp, 0, SEEK_END);
    iLen = ftell(fp);
	
    if(iLen <= 0)
    {
//        printf("file size err: %d\n", iLen);
        goto end;
    }
	
//    printf("file size: %d\n", iLen);

    pData = (unsigned char *)malloc(iLen);

    if(pData == NULL)
    {
//        printf("pData malloc err\n");
        goto end;
    }

    //fseek(fp, 0, SEEK_SET);
    fclose(fp);
    fp = fopen(filepath, "rb");
    if(fp == NULL)
    {
//        printf("fopen err\n");
        goto end;
    }
	
    itmpLen = 0;
	
    while(itmpLen < iLen)
    {
        itmp = fread(pData+itmpLen, 1, iLen, fp);
//        printf("itmp = %d\n", itmp);
        itmpLen += itmp;
    }
    
    
    fileBuffer = pData;
    
	LeftBytes = k_CurDotCol / 8;
    LeftBits  = k_CurDotCol % 8;
    
	//adds file format check
	iWReal = (fileBuffer[21] << 24) + (fileBuffer[20 ]<< 16) 
	    + (fileBuffer[19] << 8) + fileBuffer[18]; 

	iHReal = (fileBuffer[25] << 24) + (fileBuffer[24] << 16) 
	    + (fileBuffer[23] << 8) + fileBuffer[22]; 

//	printf("iWReal: %d, iHReal: %d\r\n", iWReal, iHReal);

	bitsPerPixel = fileBuffer[28];

	if (bitsPerPixel != BITMAP_PIXEL_1)
	{
//		printf("what the fck err: %d\r\n", bitsPerPixel);
		goto end;
	}

	rowByte = iWReal / 8;
	if((iWReal % 8) != 0)
	{
		rowByte++;
	}
				
	residue = rowByte % 4;
	if (residue)
	{
		skip = 4 - residue;
	}

	iW = iWReal;
	iH = iHReal;
	
	//if pixels in a line can not be divided by 4, add some 0 backward
	if ((iWReal + k_CurDotCol) > PRN_LINE_PIXEL_MAX)
	{
		iW = PRN_LINE_PIXEL_MAX - k_CurDotCol;
		iWOmit = iWReal + k_CurDotCol - PRN_LINE_PIXEL_MAX;
	}

	if ((iHReal + k_CurDotLine) > MAX_DOT_LINE)
	{
//		printf("err: iHReal:%d, k_CurDotLine: %d\r\n", iHReal, k_CurDotLine);
		return PRN_OUTOFMEMORY;
	}
	lastDotLine = k_CurDotLine + iH;

	iCnt = iHOmit * (rowByte + skip);
		
	n = iW / 8;
	
	if ((iW % 8) != 0)
	{
		n = iW / 8 + 1;
	}
	
	for (x = 0; x < iH; x++)
	{
		pDotBuf = &(k_DotBuf[lastDotLine][LeftBytes]);
		pBitmap = &(fileBuffer[62 + x * (rowByte + skip)]);
			
		memcpy(pDotBuf, pBitmap, n);

		for (y = 0; y < n; y++)
		{
			pDotBuf[y] = ~(pDotBuf[y]);
		}

		if (LeftBits != 0)
		{
			y = 0;
			medium = pDotBuf[y]; 
			pDotBuf[y] = (pDotBuf[y] >> (8-LeftBits));
			
			for (y = 1; y < n-1; y++)
			{
				dotTmp = pDotBuf[y]; 
				pDotBuf[y] = (pDotBuf[y] >> (8-LeftBits));
				medium = medium << LeftBits;
				pDotBuf[y] = pDotBuf[y] | medium;
				medium = dotTmp;
			}
		}
		
		iCnt += iWOmit/8;
		iCnt += skip;
		
		lastDotLine--;
	}
	
	k_CurDotLine += iH;
	
	
//	for(i = 0; i < MAX_DOT_LINE; i++)
//	{
//		for(j = 0; j < 48; j++)
//		{
//			
//			printf("0x%02x,", k_DotBuf[i][j]);	
//		}	
//		printf("\n");
//	}
		
	return 0;
end:
	return 0;
}


unsigned char *GetDotBuffer()
{
	return (unsigned char *)k_DotBuf;
}

int GetLineCount(void)
{
	return k_CurDotLine;
}

