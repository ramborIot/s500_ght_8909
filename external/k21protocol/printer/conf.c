#include "conf.h"
#include <string.h>

int
getConfValue (char *confFile, char *section_name, char *conf_name,
	      void *value, CAST_TYPE type)
{
	FILE *fp = NULL;
	char dname[128] = { 0 };
	char cname[128] = { 0 };


	fp = fopen (confFile, "r");
	if (fp == NULL)
	{
		printf ("fail to open conf file\n");
		return -1;
	}

  //read section_name  
	while (1)
	{
		bzero (dname, 128);
		fgets (dname, 128, fp);
		if (feof (fp))
		{
//			printf ("reach end of file\n");
			fclose (fp);
			return -1;
		}

		if ('[' == dname[0])
		{
			if (!strncmp (dname + 1, section_name, strlen (section_name)))
			{
				break;
			}
		}
	}

	//read configure value  
	while (1)
	{
		bzero (cname, 0);
		fgets (cname, 128, fp);
		if (feof (fp))
		{
///			printf ("reach end of file\n");
			fclose (fp);
			return -1;
		}

		if (!strncmp (cname, conf_name, strlen (conf_name)))
		{
			cname[strlen (cname) - 1] = 0;
			char *sp = strchr (cname, '=');
			if (sp == NULL)
			{
				printf ("error to conf file\n");
				fclose (fp);
				return -1;
			}

			if (type == TYPE_INT)
			{
				*(int *) value = atoi (sp + 1);
			}

			if (type == TYPE_STRING)
			{
				strcpy ((char *) value, sp + 1);
			}

			break;
		}
	}

	fclose (fp);
	return 0;
}

int getTable(char table[30][128])
{
	
	FILE *fp = NULL;
	char dname[128] = { 0 };
		
	int i=0;
	// fp = fopen ("print_font.conf", "r");
	fp = fopen ("/storage/sdcard0/test/print_font.conf", "r");
	
	if (fp == NULL)
	{
		printf ("fail to open conf file\n");
		return -1;
	}
	while (1)
	{
		bzero (dname, 128);
		fgets (dname, 128, fp);

		if (feof (fp))
		{
//			printf ("reach end of file\n");
			fclose (fp);
			break;
		}

		if ('[' == dname[0])
		{
			memcpy(table[i],dname+1,strlen(dname+1)-2);
			i++;
		}
	}
	return i+1;

}
#if 0
int main()
{
	char buffer[128];
	getConfValue("print_font.conf","FONT","font_eng_path",buffer,TYPE_STRING);
	printf("%s\n",buffer);	
}
#endif

