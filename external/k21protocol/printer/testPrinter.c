#include <stdlib.h>
#include <stdio.h>
#include "debug.h"

#include "printer_queue.h"


unsigned char buf[1024];

static void revsImage(unsigned char *p, unsigned char cnt)
{
  unsigned char i;
  unsigned short temp;
  
  for(i=0; i<cnt; i++)
  {
  	temp = (unsigned short) (p[i]<<8);   
	temp |= ((temp&0x8000)>>15);
    temp |= ((temp&0x4000)>>13);
	temp |= ((temp&0x2000)>>11);
    temp |= ((temp&0x1000)>>9); 
	temp |= ((temp&0x0800)>>7);
    temp |= ((temp&0x0400)>>5);
	temp |= ((temp&0x0200)>>3);
    temp |= ((temp&0x0100)>>1);  
    p[i] = (unsigned char) (temp);  
  }   
}

void PintData(unsigned char *data)
{
    int error;
    int i, j, offset,n;
    unsigned char *name;
    error = spi_ddi_printer_init(40);

	LOG("spi_ddi_printer_init:%d", error);

	spi_ddi_printer_clear_fill_finish();

	memset(buf, 0xFF, 48);
	
	for(n = 0; n<280; n++)
	{
		error = spi_ddi_printer_fill(buf, 48);

		if(error != 0)
		{
			LOG("spi_ddi_printer_fill:%d", error);
		}
	}
	
		error = spi_ddi_printer_start(10);
 #if 0   
    revsImage(data, 24*3);
	name = data;
	int value = 40;
	int k;
	// LOG(" ");
	for(k=0; k<value; k++)
        {

        	for(i=0; i<24; i++)
        	{
			for(j=0; j<48/3; j++)
          		{
          		#if 0
          			if(j%2==0) name = wang;
           			else name = ming;
			#endif
						LOG("i:j:%d:%d", i, j);
            			*(buf+3*j+0)  = *(name+i*3+0);
            			*(buf+3*j+1)  = *(name+i*3+1);
            			*(buf+3*j+2)  = *(name+i*3+2);	  
						// offset += 3;
          		}
				  
				 error =  spi_ddi_printer_fill(buf, 48);
				 LOG("spi_ddi_printer_fill:%d", error);
		}
        }

		spi_ddi_printer_set_fill_finish();
		// LOG("spi_ddi_printer_start:%d", error);
		// SetY(offset);
		// Printer_Print_BmpFile("a.txt");
#endif

		spi_ddi_printer_set_fill_finish();
}

#if 0

void u_prn_test(void);


int main(int argc, char** argv)
{
	#if 0
	unsigned char p[128] = "许总";//"A";
	unsigned char* hex=NULL;
	int hexLength = 0;
	unsigned char r[128] = "C";

	int i = 0,j=0;

    spi_ddi_sys_init();

	printf("8x16\n");   
	system( "date" );
	while(i < 1)
	{
		getHexData( 1, r, &hex,&hexLength );
		i++;
	}
	system( "date" );
showBufferValInTerminal( hex );

	printf("16x16\n");
	i = 0; 
	while(i < 1)
	{
		getHexData( 2, p, &hex,&hexLength );
		i++;
	}
	system( "date" );
showBufferValInTerminal( hex );

	printf("16x24\n"); 
	i = 0; 
	while(i < 1)
	{
		getHexData( 3, r, &hex,&hexLength );
		i++;
	}
	system( "date" );
showBufferValInTerminal( hex );

	printf("24x24\n"); 
	i = 0; 
	while(i < 1)
	{
		getHexData( 4, p, &hex,&hexLength );
		i++;
	}
    PintData(hex);
	system( "date" );
showBufferValInTerminal( hex );


	printf("8x8\n");   
	system( "date" );
	i = 0; 
	while(i < 1)
	{
		getHexData( 7, r, &hex,&hexLength );
		i++;
	}
	system( "date" );
	for (i=0;i<8;i++)
	{
		for(j=0;j<8;j++)
		{
			if ((hex[i]&(0x01<<j%8))!=0)    
				printf("0");
			else 
				printf("-");
		}
		printf("\n");
	}

	printf("12x12\n");   
	system( "date" );
	i = 0; 
	while(i < 1)
	{
		getHexData( 6, p, &hex,&hexLength );
		i++;
	}
	system( "date" );
	int posByte = 0;
	int posInByte = 0;
	for( j = 0;j< 12;j++)
	{
		for( i = 0;i< 16;i++ )
		{
			posByte = (j*16+i)/8;
			posInByte =  (j*16+i)%8;
			
			if(  ( hex[posByte]  &  (1<< posInByte) ) == 0  )
			{
				printf(" ");
			}
			else
			{
				printf("*");
			}
		}
		printf("\n");
	}



	printf("freetype 16x16\n");
	i = 0; 
	while(i < 1)
	{
		getHexData( 10, p, &hex,&hexLength );
		i++;
	}
	system( "date" );
showBufferValInTerminal( hex );
	printf("24x24\n");
	i = 0; 
	while(i < 1)
	{
		getHexData( 11, p, &hex,&hexLength );
		i++;
	}
	system( "date" );
showBufferValInTerminal( hex );
 	//PintData(hex);
	i=0;
#endif

	
	spi_ddi_sys_init();

#if 0
		int ret = 0;
		char state = 0;

		printf("______________________\r\n");
		
  		spi_ddi_sys_init();
		spi_ddi_printer_init(20);
		ret = printer_printf(0, &state,"ABCDEFGHIJKLMNOPQRSTUVWXYZ");
		printer_printf(0, &state,"ABCDEFGHIJKLMNOPQRSTUVWXYZ");
		printer_printf(0, &state,"ABCDEFGHIJKLMNOPQRSTUVWXYZ");
		printer_printf(0, &state,"ABCDEFGHIJKLMNOPQRSTUVWXYZ");
		printer_printf(0, &state,"012345678909876543210");
		spi_ddi_printer_start(10);
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");
		printer_printf(0, &state,"国国国国国国国国国国国国国国国国国国");

		
		spi_ddi_printer_set_fill_finish();
#endif
/*
	while(i < 250)
	{
//		printf("index=%d\n",j);
		getHexData( 1, r, &hex,&hexLength );
showBufferValInTerminal( hex );
		getHexData( 2, p, &hex,&hexLength );
showBufferValInTerminal( hex );
		getHexData( 3, r, &hex,&hexLength );
showBufferValInTerminal( hex );
		getHexData( 4, p, &hex,&hexLength );
showBufferValInTerminal( hex );
		//str += 2;
		i++;
	}
*/
//	showBufferValInTerminal( hex );
//	system( "date" );

	u_prn_test();

	return 0;
}
#endif


int fck_printer_start( int step )
{
	int ret = -1;
	int lines;
	unsigned char data[48];
	int cnt = 10;
	int i;
	//ret = spi_ddi_printer_start(lines);

	// 寮�濮嬫椂锛屽敖鍙兘濉厖澶氭暟鎹�
	lines = prtQueCheckLines();
	lines = lines > cnt ? cnt : lines;
	LOG("prtQueCheckLines, line:%d\r\n", lines);
	for(i = 0; i < lines; i++)
	{
		prtQueGetBuf((unsigned char *)data, sizeof(data));
		spi_ddi_printer_fill(data, sizeof(data));
	}

	// 濡傛灉buff鏈韩瓒冲瀛樻斁锛屼笉闇�瑕佽竟鎵撳嵃杈瑰～鍏咃紝鍒欒缃～鍏呭畬鎴愭爣璇�
	if(prtQueCheckLines() == 0) 
	{
		spi_ddi_printer_set_fill_finish();
		spi_ddi_printer_start(step);
		//LOG("1-spi_ddi_printer_start(%d), ret:%d", step, ret);
	}
   else
   {
		ret = spi_ddi_printer_start(step); // 鍏堝惎鍔ㄦ墦鍗帮紝涔嬪悗鍐嶅～鍏呭墿浣欐暟鎹�
		//LOG("2-spi_ddi_printer_start(%d), ret:%d", step, ret);
		
		// 濡傛灉闇�瑕佽竟鎵撳嵃杈瑰～鍏咃紝鍒欏惎鍔ㄦ墦鍗颁竴浼�(缁忛獙鍊�)涔嬪悗缁х画濉厖鍓╀綑鏁版嵁
		//DelayMs(500);

		do {
			lines = spi_ddi_printer_check_queue();

			for(i = 0; i < lines && prtQueCheckLines() > 0; i++)
			{
				prtQueGetBuf((unsigned char *)data, sizeof(data));
				spi_ddi_printer_fill(data, sizeof(data));
			}
		} while( prtQueCheckLines() > 0 && spi_ddi_printer_get_status() == PRT_ERR_BUSY); // 姝ゆ椂鎵撳嵃鏈哄繖涓烘甯哥姸鎬侊紝鍚﹀垯灏卞凡缁忎笉姝ｅ父浜�

		spi_ddi_printer_set_fill_finish();
	}

	while( (ret = spi_ddi_printer_get_status()) == PRT_ERR_BUSY )
		;

	return ret;
}


void u_prn_test(void)
{
    unsigned char code[3];
	unsigned char cmd[] = { 0x0A, ';' };
	int state;
	int ret;
	
    memset(code, 0x00, sizeof(code));

	ret = spi_ddi_printer_init( 100 );
	printf("spi_ddi_printer_init, ret: %d\r\n", ret);

	prtQueInit();

	//printer_printf(10, &state, "%s", "\r\n");

#if 1
    // 打印GB2312字符集全部字符
    for(code[0]=0xA1; 0 && code[0]<=0xA9; code[0]++)
    {
        for(code[1]=0xA1; code[1]<=0xFE; code[1]++)
        {
            printer_printf(10, &state, "%s", code);
        }
        printer_printf(10, &state,"  0x%x\n", code[0]);
    }
    //for(code[0]=0xB0; code[0]<=0xF7; code[0]++)
	for(code[0]=0xB0; code[0]<=0xB7; code[0]++)
    {
		for(code[1]=0xA1; code[1]<=0xFE; code[1]++)
        {
            printer_printf(10, &state,"%s", code);
        }
		
        printer_printf(10, &state,"  0x%x\n", code[0]);

		//printer_command( 10, cmd, sizeof(cmd) );
    }
#endif
	
	ret = fck_printer_start( 200 );

	printf("fck_printer_start, ret: %d\r\n", ret);
	
}




