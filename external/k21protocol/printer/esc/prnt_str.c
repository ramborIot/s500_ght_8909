/***********************************************************
* 
*      File name   : prnt_str.c
*      Description : printer print string functions
*      Date        :
*      Revision    : V.1.00
*      Author      :
*      Copyright (c) 2013   Inc.                  
*
************************************************************/
#define __PRNT_STR_C__

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <stdarg.h>
#include <sys/types.h>
#include <fcntl.h>

#include "prnt_str.h"
#include "prnt_cmd.h"
#include "prnt_mgnt.h"

/***********************************************************
*      Function     : is_print_char_type()
*      Description  : check if is a valid print character
*      Input        : 
*      Output       : 
*      Return       :
************************************************************/
int is_print_char_type (int type) 
{

    if (type == CHAR_CODE_ASCII || type == CHAR_CODE_EXT1 || type == CHAR_CODE_EXT2) {
        return TRUE;
    }

    return FALSE;
}


/***********************************************************
*      Function     : reverse_byte()
*      Description  : reverse data in buffer
*      Input        : 
*      Output       : 
*      Return       :
************************************************************/
int reverse_byte (unsigned char *p, unsigned short len)
{
    int i, j;
    unsigned short temp;
    unsigned char tmp;

    //return OK;

    if (NULL == p || len < 0) {
        return ERROR_INVALID;
    }

    for (i=0; i<len; i++) {
        temp = (unsigned short) (p[i]<<8);   	
        temp |= ((temp&0x8000)>>15);        
        temp |= ((temp&0x4000)>>13);	
        temp |= ((temp&0x2000)>>11);        
        temp |= ((temp&0x1000)>>9); 	
        temp |= ((temp&0x0800)>>7);        
        temp |= ((temp&0x0400)>>5);	
        temp |= ((temp&0x0200)>>3);        
        temp |= ((temp&0x0100)>>1);          
        p[i] = (unsigned char) (temp);    
    }   

    return OK;
}



/***********************************************************
*      Function     : string_parse()
*      Description  : parse the attribute of input string and save 
*      Input        :   unsigned char *pstr - string need to parse
*                           int len - length of the input string
*                           int sstr - start of string
*                           int sparsed - start of the parsed data
*      Output       :   prnt_char_attr_t *pparsed - the parsed result
*                           int *num - the number of characters parsed
*      Return       :   0 - OK
*                           other - error
************************************************************/
int string_parse (unsigned char *pstr, int len, prnt_char_attr_t *pparsed, int *num, int sstr, int sparsed)
{
    int ret = OK;
    int i, j, dataLen;
    unsigned char* phex = NULL;

#ifdef DEBUG_MODE
    printf("%s: start - len(%d) sstr(%d) sparsed(%d)\n", __func__, len, sstr, sparsed);
#endif

    if (NULL == pstr || len <= 0 || NULL == pparsed || NULL == num || sstr < 0 || sparsed < 0) {
        return ERROR_INVALID;
    }

    /* parse the characters and read corresponding dot matrix */
    for (i = sstr, j = sparsed; i < len; j++) {
        if (is_valid_print_control_code(pstr[i]) == TRUE) {
            /* ASCII control code */
            pparsed[j].idx = i;
            pparsed[j].chr_type = CTRL_CODE_ASCII;
            pparsed[j].chr_len = CTRL_CODE_ASCII_LENGTH;
            pparsed[j].chr_code[0] = pstr[i];
            i += pparsed[j].chr_len;
        }
        else if (pstr[i] >= ASCII_S && pstr[i] <= ASCII_E ) {
            /* ASCII printable */
            pparsed[j].idx = i;
            pparsed[j].chr_type = CHAR_CODE_ASCII;
            pparsed[j].chr_len = CHAR_CODE_ASCII_LENGTH;
            pparsed[j].chr_code[0] = pstr[i];
            pparsed[j].chr_font_lib_idx = chr_half_font_lib_idx;
            pparsed[j].chr_hight_cfg = chr_half_hight_cfg;
            pparsed[j].chr_width_cfg = chr_half_width_cfg;
            pparsed[j].chr_double_hight = chr_double_hight;
            pparsed[j].chr_double_width = chr_double_width;
            pparsed[j].chr_inverse_mode = chr_inverse_mode;
#ifdef LIB_TEST_MODE
            //memset(pparsed[j].chr_dot_buf, 0x0f, pparsed[j].chr_hight_cfg*pparsed[j].chr_width_cfg/8);
            if (j % 2 == 0) {
                memset(pparsed[j].chr_dot_buf, 0x00, pparsed[j].chr_hight_cfg*pparsed[j].chr_width_cfg/8);
           } else {
                memset(pparsed[j].chr_dot_buf, 0xff, pparsed[j].chr_hight_cfg*pparsed[j].chr_width_cfg/8);
            }
#else
            ret = getHexData(pparsed[j].chr_font_lib_idx, pparsed[j].chr_code, &phex, &dataLen);
            if (!ret && phex != NULL) {
                memcpy(pparsed[j].chr_dot_buf, phex, dataLen);
                //showBufferInTerminal(pparsed[j].chr_dot_buf, pparsed[j].chr_hight_cfg, pparsed[j].chr_width_cfg, 1);
            } else {
                return ERROR_FAIL;
            }
#endif                
            i += pparsed[j].chr_len;
        } 
        else if ((pstr[i] >= GB18030_2_1_1_S && pstr[i] <= GB18030_2_1_1_E  
        		&& pstr[i+1] >= GB18030_2_1_2_S && pstr[i+1] <= GB18030_2_1_2_E) || 
        		(pstr[i] >= GB18030_2_2_1_S && pstr[i] <= GB18030_2_2_1_E  
        		&& pstr[i+1] >= GB18030_2_2_2_S && pstr[i+1] <= GB18030_2_2_2_E)||
        		 (pstr[i] >= GB18030_2_3_1_S && pstr[i] <= GB18030_2_3_1_E  
        		&& pstr[i+1] >= GB18030_2_3_2_S && pstr[i+1] <= GB18030_2_3_2_E)) {
		/* 2 bytes chinese */
            pparsed[j].idx = i;
            pparsed[j].chr_type = CHAR_CODE_EXT1;
            pparsed[j].chr_len = CHAR_CODE_EXT1_LENGTH;
            pparsed[j].chr_code[0] = pstr[i];
            pparsed[j].chr_code[1] = pstr[i+1];
            pparsed[j].chr_font_lib_idx = chr_full_font_lib_idx;
            pparsed[j].chr_hight_cfg = chr_full_hight_cfg;
            pparsed[j].chr_width_cfg = chr_full_width_cfg;
            pparsed[j].chr_double_hight = chr_double_hight;
            pparsed[j].chr_double_width = chr_double_width;
            pparsed[j].chr_inverse_mode = chr_inverse_mode;
#ifdef LIB_TEST_MODE
            //memset(pparsed[j].chr_dot_buf, 0x33, pparsed[j].chr_hight_cfg*pparsed[j].chr_width_cfg / 8);
            if (j % 2 == 0) {
                memset(pparsed[j].chr_dot_buf, 0x00, pparsed[j].chr_hight_cfg*pparsed[j].chr_width_cfg/8);
           } else {
                memset(pparsed[j].chr_dot_buf, 0xff, pparsed[j].chr_hight_cfg*pparsed[j].chr_width_cfg/8);
            }
#else
            ret = getHexData(pparsed[j].chr_font_lib_idx, pparsed[j].chr_code, &phex, &dataLen);
            if (!ret && phex != NULL) {
                memcpy(pparsed[j].chr_dot_buf, phex, dataLen);
                //showBufferInTerminal(pparsed[j].chr_dot_buf, pparsed[j].chr_hight_cfg, pparsed[j].chr_width_cfg, 1);
            } else {
                return ERROR_FAIL;
            }
#endif                
            i += pparsed[j].chr_len;
        } 
        else if ((pstr[i] >= GB18030_4_1_1_S && pstr[i] <=GB18030_4_1_1_E  
        		&& pstr[i+1] >= GB18030_4_1_2_S && pstr[i+1] <= GB18030_4_1_2_E
        		&& pstr[i+2] >= GB18030_4_1_3_S && pstr[i+2] <= GB18030_4_1_3_E
        		&& pstr[i+3] >= GB18030_4_1_4_S && pstr[i+3] <= GB18030_4_1_4_E	) ||
        		(pstr[i] >= GB18030_4_2_1_S && pstr[i] <= GB18030_4_2_1_E  
        		&& pstr[i+1] >= GB18030_4_2_2_S && pstr[i+1] <= GB18030_4_2_2_E
        		&& pstr[i+2] >= GB18030_4_2_3_S && pstr[i+2] <= GB18030_4_2_3_E
        		&& pstr[i+3] >= GB18030_4_2_4_S && pstr[i+3] <= GB18030_4_2_4_E	)) {
		/* 4 bytes chinese */
            pparsed[j].idx = i;
            pparsed[j].chr_type = CHAR_CODE_EXT2;
            pparsed[j].chr_len = CHAR_CODE_EXT2_LENGTH;
            pparsed[j].chr_code[0] = pstr[i];
            pparsed[j].chr_code[1] = pstr[i+1];
            pparsed[j].chr_code[2] = pstr[i+2];
            pparsed[j].chr_code[3] = pstr[i+3];
            pparsed[j].chr_font_lib_idx = chr_full_font_lib_idx;
            pparsed[j].chr_hight_cfg = chr_full_hight_cfg;
            pparsed[j].chr_width_cfg = chr_full_width_cfg;
            pparsed[j].chr_double_hight = chr_double_hight;
            pparsed[j].chr_double_width = chr_double_width;
            pparsed[j].chr_inverse_mode = chr_inverse_mode;
#ifdef LIB_TEST_MODE
            //memset(pparsed[j].chr_dot_buf, 0x55, pparsed[j].chr_hight_cfg*pparsed[j].chr_width_cfg/8);
            if (j % 2 == 0) {
                memset(pparsed[j].chr_dot_buf, 0x00, pparsed[j].chr_hight_cfg*pparsed[j].chr_width_cfg/8);
           } else {
                memset(pparsed[j].chr_dot_buf, 0xff, pparsed[j].chr_hight_cfg*pparsed[j].chr_width_cfg/8);
            }
#else
            ret = getHexData(pparsed[j].chr_font_lib_idx, pparsed[j].chr_code, &phex, &dataLen);
            if (!ret && phex != NULL) {
                memcpy(pparsed[j].chr_dot_buf, phex, dataLen);
                //showBufferInTerminal(pparsed[j].chr_dot_buf, pparsed[j].chr_hight_cfg, pparsed[j].chr_width_cfg, 1);
            } else {
                return ERROR_FAIL;
            }
#endif                
            i += pparsed[j].chr_len;
        }
        else {
#ifdef DEBUG_MODE
            printf("%s: Invalid char format\n", __func__);
#endif
            //return ERROR_INVALID;
			/* 2 bytes chinese */
            pparsed[j].idx = i;
            pparsed[j].chr_type = CHAR_CODE_EXT1;
            pparsed[j].chr_len = CHAR_CODE_EXT1_LENGTH;
            pparsed[j].chr_code[0] = pstr[i];
            pparsed[j].chr_code[1] = pstr[i+1];
            pparsed[j].chr_font_lib_idx = chr_full_font_lib_idx;
            pparsed[j].chr_hight_cfg = chr_full_hight_cfg;
            pparsed[j].chr_width_cfg = chr_full_width_cfg;
            pparsed[j].chr_double_hight = chr_double_hight;
            pparsed[j].chr_double_width = chr_double_width;
            pparsed[j].chr_inverse_mode = chr_inverse_mode;

			memset(pparsed[j].chr_dot_buf, 0xff, pparsed[j].chr_hight_cfg*pparsed[j].chr_width_cfg/8);

            i += pparsed[j].chr_len;
        }
    }

    *num = j;
    
#ifdef DEBUG_MODE
    printf("%s: end - parsed char num(%d) ret(%d)\n", __func__, *num, ret);
#endif

    return ret;
}


/***********************************************************
*      Function     : matrix_parse()
*      Description  : parse the attribute of input font matrix and save 
*      Input        :   unsigned char *pstr - string need to parse
*                           int cMun - number of the input string
*                           int cHeight - height of font
*                           int cWidth - width of font
*      Output       :   prnt_char_attr_t *pparsed - the parsed result
*      Return       :   0 - OK
*                           <0 - error
************************************************************/
int matrix_parse (unsigned char *pstr, int cMun, prnt_char_attr_t *pparsed, int cHeight, int cWidth)
{
    int ret = OK;
    int j;

#ifdef DEBUG_MODE
    printf("%s: start - cMun(%d) cHeight(%d) cWidth(%d)\n", __func__, cMun, cHeight, cWidth);
#endif

    if (NULL == pstr || cMun < 0 || NULL == pparsed || cHeight <= 0 || cWidth <= 0) {
        return ERROR_INVALID;
    }

    /* parse the characters and read corresponding dot matrix */
    for (j = 0; j < cMun; j ++) {
            pparsed[j].chr_hight_cfg = cHeight;
            pparsed[j].chr_width_cfg = cWidth;
#ifdef MATRIX_CCODE_ATTR_SUPPORT
            pparsed[j].chr_double_hight = chr_double_hight;
            pparsed[j].chr_double_width = chr_double_width;
            pparsed[j].chr_inverse_mode = chr_inverse_mode;
#endif            
            memcpy(pparsed[j].chr_dot_buf, &pstr[j * (cHeight * cWidth / 8)], (cHeight * cWidth / 8));
            //showBufferInTerminal(pparsed[j].chr_dot_buf, pparsed[j].chr_hight_cfg, pparsed[j].chr_width_cfg, 1);
    }

    /* add "\n" at the end */
    pparsed[j].chr_type = CTRL_CODE_ASCII;
    pparsed[j].chr_len = CTRL_CODE_ASCII_LENGTH;
    pparsed[j].chr_code[0] = CCODE_LF;

    return ret;
}




