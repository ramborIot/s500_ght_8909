/***********************************************************
*
*      File name   : prnt_mgnt.c
*      Description : printer management functions
*      Date        :
*      Revision    : V.1.00
*      Author      :
*      Copyright (c) 2013   Inc.                  
* 
************************************************************/
#define __PRNT_MGNT_C__

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <stdarg.h>
#include <sys/types.h>
#include <fcntl.h>

#include "common.h"
#include "prnt_mgnt.h"
#include "prnt_img.h"
#include "prnt_drv.h"
#include "prnt_cmd.h"
#include "prnt_str.h"


#define fck_dbg		//printf

/*  General */
unsigned char str_left[PRNT_STRING_LEFT_BUF_SIZE] = "";
prnt_char_attr_t *pstr_left_parsed = NULL;
int str_left_num = 0;
int print_speed = PRINT_SPEED_DEFAULT;

/* Line attributes */
int left_margin = PRNT_LEFT_SPACE_DEFAULT;
int right_margin = PRNT_RIGHT_SPACE_DEFAULT;

int line_hight_cfg = PRNT_LINE_HIGHT_DEFAULT;
int line_double_hight = PRNT_LINE_DOUBLE_HIGHT_DEFAULT;
int line_double_width = PRNT_LINE_DOUBLE_WIDTH_DEFAULT;

int line_hight = PRNT_LINE_HIGHT_DEFAULT;

/* Character attributes */
int chr_inverse_mode = PRNT_INVERSE_MODE_DEFAULT;

int chr_half_font_lib_idx = PRNT_HALF_FONT_DEFAULT;
int chr_full_font_lib_idx = PRNT_FULL_FONT_DEFAULT;

int chr_half_hight_cfg = PRNT_CHAR_HALF_HIGHT_DEFAULT;
int chr_full_hight_cfg = PRNT_CHAR_FULL_HIGHT_DEFAULT;
u8 chr_double_hight = PRNT_CHAR_DOUBLE_HIGHT_DEFAULT;
int chr_half_width_cfg = PRNT_CHAR_HALF_WIDTH_DEFAULT;
int chr_full_width_cfg = PRNT_CHAR_FULL_WIDTH_DEFAULT;
u8 chr_double_width = PRNT_CHAR_DOUBLE_WIDTH_DEFAULT;



int Printer_Fill(unsigned char *buf, int len)
{
    int block_size = 48*10;
    int left = len;
    int send = block_size;

    // while(left > 0)
    // {
    //     send = (left > send)?block_size:left;

    //     spi_ddi_printer_fill(buf+(len-left), send);
    //     left -= send;
    // }

    // spi_ddi_printer_set_fill_finish();
    // spi_ddi_printer_start(0);
    prtQueFillBuf((unsigned char*)buf,len);

    return 0;
}


/***********************************************************
*      Function     : showVarsInTerminal()
*      Description  : 
*      Input        : None
*      Output       : 
*      Return       : 
************************************************************/
void showVarsInTerminal(void)
{
#ifdef DEBUG_MODE
    printf("%s: =======================\n", __func__);
    printf("%s: str_left(%s)\n", __func__, str_left);
    printf("%s: print_speed(%d)\n", __func__, print_speed);
    printf("%s: left_margin(%d)\n", __func__, left_margin);
    printf("%s: right_margin(%d)\n", __func__, right_margin);
    printf("%s: line_hight_cfg(%d)\n", __func__, line_hight_cfg);
    printf("%s: line_double_hight(%d)\n", __func__, line_double_hight);
    printf("%s: line_double_width(%d)\n", __func__, line_double_width);
    printf("%s: line_hight(%d)\n", __func__, line_hight);
    printf("%s: chr_inverse_mode(%d)\n", __func__, chr_inverse_mode);
    printf("%s: chr_half_font_lib_idx(%d)\n", __func__, chr_half_font_lib_idx);
    printf("%s: chr_full_font_lib_idx(%d)\n", __func__, chr_full_font_lib_idx);
    printf("%s: chr_half_hight_cfg(%d)\n", __func__, chr_half_hight_cfg);
    printf("%s: chr_full_hight_cfg(%d)\n", __func__, chr_full_hight_cfg);
    printf("%s: chr_double_hight(%d)\n", __func__, chr_double_hight);
    printf("%s: chr_half_width_cfg(%d)\n", __func__, chr_half_width_cfg);
    printf("%s: chr_full_width_cfg(%d)\n", __func__, chr_full_width_cfg);
    printf("%s: chr_double_width(%d)\n", __func__, chr_double_width);
    printf("%s: =======================\n", __func__);
#endif
}


/***********************************************************
*      Function     : showBufferInTerminal()
*      Description  : 
*      Input        : None
*      Output       : 
*      Return       : 
************************************************************/
int showBufferInTerminal(unsigned char*  data, int height, int width, int flag)
{
   	int i = 0;
	int j = 0;
	int posByte = 0;
	int posInByte = 0;

#if 0
	printf("\n");
	for(j = 0; j < height; j++)
	{
		//printf("%02d ", j);
		for( i = 0;i< width;i++ )
		{
			posByte = (j*width+i)/8;
			posInByte =  (j*width+i)%8;

                    if (flag) {
                        /* LSB */
                        if((data[posByte]  &  (1<< posInByte)) == 0) {
                            printf("_");
                        } else {
                            printf("*");
                        }
                    } else {
                        /* MSB */
                        if((data[posByte]  &  (0x80 >> posInByte)) == 0) {
                            printf("_");
                        } else {
                            printf("*");
                        }
                    }
		}
		printf("\n");
	}
#endif    
    return OK;
}


/***********************************************************
*      Function     : set_char_attr_default()
*      Description  : printer character attribute reset functions
*      Input        : None
*      Output       : 
*      Return       : 
************************************************************/
int set_char_attr_default (void)
{
    int ret = OK;

    chr_inverse_mode = PRNT_INVERSE_MODE_DEFAULT;

    return  ret;
}


/***********************************************************
*      Function     : set_printer_attr_default()
*      Description  : printer  attribute reset functions
*      Input        : None
*      Output       : 
*      Return       : 
************************************************************/
int set_printer_attr_default (void)
{
    int ret = OK;

    /*  General */
    memset(str_left, 0x0, sizeof(str_left));
    if (pstr_left_parsed) {
        free(pstr_left_parsed);
        pstr_left_parsed = NULL;
    }
    str_left_num = 0;
    print_speed= PRINT_SPEED_DEFAULT;

    /* Line attributes */
    left_margin = PRNT_LEFT_SPACE_DEFAULT;
    right_margin = PRNT_RIGHT_SPACE_DEFAULT;

    line_hight_cfg = PRNT_LINE_HIGHT_DEFAULT;
    line_double_hight = PRNT_LINE_DOUBLE_HIGHT_DEFAULT;
    line_double_width = PRNT_LINE_DOUBLE_WIDTH_DEFAULT;

    line_hight = PRNT_LINE_HIGHT_DEFAULT;

    /* Character attributes */
    chr_inverse_mode = PRNT_INVERSE_MODE_DEFAULT;

    chr_half_font_lib_idx = PRNT_HALF_FONT_DEFAULT;
    chr_full_font_lib_idx = PRNT_FULL_FONT_DEFAULT;

    chr_half_hight_cfg = PRNT_CHAR_HALF_HIGHT_DEFAULT;
    chr_full_hight_cfg = PRNT_CHAR_FULL_HIGHT_DEFAULT;
    chr_double_hight = PRNT_CHAR_DOUBLE_HIGHT_DEFAULT;
    chr_half_width_cfg = PRNT_CHAR_HALF_WIDTH_DEFAULT;
    chr_full_width_cfg = PRNT_CHAR_FULL_WIDTH_DEFAULT;
    chr_double_width = PRNT_CHAR_DOUBLE_WIDTH_DEFAULT;

    return  ret;
}


/***********************************************************
*      Function     : printer_open()
*      Description  : printer open functions
*      Input        : None
*      Output       : 
*      Return       : the handler of the printer
************************************************************/
int printer_open (const char* filename)
{
    #if 0
    int fd;

    if (NULL == filename) {
        return ERROR_INVALID;
    }
    fd = open(filename, O_RDWR);
    return  fd;
    #else
    spi_ddi_printer_init();
    return 10;
    #endif
}


/***********************************************************
*      Function     : printer_close()
*      Description  : printer close functions
*      Input        : None
*      Output       : 
*      Return       :
************************************************************/
int printer_close (int fd)
{
    #if 0
    if (fd < 0) {
        return ERROR_INVALID;
    }

    return close(fd);
    #else
    spi_ddi_printer_set_fill_finish();
    #endif
	return 0;
}


/***********************************************************
*      Function     : printer_read()
*      Description  : printer read handle functions
*      Input        : None
*      Output       : 
*      Return       :
************************************************************/
int printer_read (int fd, const char* cmd, void* result, int* type, int max_len)
{
    int ret = OK;
    unsigned char *pbuf = NULL;

    if (fd < 0 || NULL == cmd || NULL == result || NULL == type || max_len == 0) {
        return ERROR_INVALID;
    }

    if (cmd[0] != CCODE_ESC) {
        return ERROR_INVALID;
    }

    ret = exec_esc_r_cmd(fd, cmd[1], cmd[2], type, result, max_len);

    return ret;
}


/***********************************************************
*      Function     : printer_command()
*      Description  : printer command handle functions
*      Input        : None
*      Output       : 
*      Return       :
************************************************************/
int printer_command (int fd, unsigned char* cmd, int len)
{
    int ret = OK;
    unsigned char *pbuf = NULL;
    int i, j, k;

    if (fd < 0 || NULL == cmd || len <= 0 ) {
        return ERROR_INVALID;
    }

    /* copy the cmd */
    pbuf = malloc(len + 1);
    if (pbuf == NULL) {
        return ERROR_BUSY;
    }
	memset(pbuf, 0x00, len+1);
    //memcpy(pbuf, cmd, (len+1));
    memcpy(pbuf, cmd, len);
    pbuf[len] = '\0';

    for (i = 0, j = 0; i < len; j++) {
 #ifdef DEBUG_MODE
    printf("%s: i(%d) j(%d)\n", __func__, i, j);
#endif
       if (pbuf[i] == CCODE_ESC) {
            /* ESC cmd */
            ret = exec_esc_cmd(fd, pbuf[i+1], pbuf[i+2], pbuf[i+3]);
            if (ret) {
                goto cmdend;
            }
            /* find next cmd */
            for ( ; i < len ; i++) {
                if (pbuf[i] == ';' && (ret = is_valid_control_code(pbuf[i+1])) == TRUE) {
                    i ++;
                    break;
                }
            }

        } else if (pbuf[i] < CCODE_SPACE) {
            /* Control code */
            ret = exec_ccode(fd, pbuf[i]);
            if (ret) {
                goto cmdend;
            }
            i += 2;
        } else {
            ret =  ERROR_INVALID;
            goto cmdend;
        }
        
    }
    
cmdend:

    if (pbuf) {
        free(pbuf);
    }
    return ret;
}

/***********************************************************
*      Function     : printer_graphics()
*      Description  : printer print image functions
*      Input        : None
*      Output       : 
*      Return       :
************************************************************/
int printer_graphics (int fd, int xOffset, const char* bmpFilePath)
{
    int ret = OK;
    unsigned char *pbuf = NULL, *pdotbuf = NULL;
    u32 biSizeImage, biHeight, biWidth, lineByte;
    int i, freeDotLine, dotLineByte;
    int dl, dw, bPosByte, bPosInByte, dPosByte, dPosInByte;
    int dotByteCmdMax = 0;

    if (fd < 0 || NULL == bmpFilePath || xOffset >= PRNT_DOT_MAX ) {
        return ERROR_INVALID;
    }

    /* read bmp */
    ret = read_bmp(bmpFilePath, xOffset, &pbuf, &biSizeImage, &biHeight, &biWidth, &lineByte);
#ifdef DEBUG_MODE
    printf("%s: read(%s), xOffset(%d) biHeight(%d) biWidth(%d) biSizeImage(%d) lineByte(%d) \n", __func__, bmpFilePath, xOffset, biHeight, biWidth, biSizeImage, lineByte);
#endif
    if (pbuf == NULL || ret != OK) {
        printf("%s: read_bmp(%s) fail ret(%d)\n", __func__, bmpFilePath, ret);
        goto imgend;
    }

   /* check driver buffer */
    freeDotLine = get_free_dotline_num(fd);
    if (freeDotLine < biHeight) {
        ret = ERROR_BUSY;
        goto imgend;
    }

#ifdef VRFN_DRV_SUPPORT
    dotByteCmdMax = PRNT_DOT_BYTE_CMD_MAX;
#else
    dotByteCmdMax = PRNT_DOT_BYTE_MAX;
#endif
    pdotbuf = malloc(biHeight * dotByteCmdMax);
    if (pdotbuf == NULL) {
        ret = ERROR_BUSY;
        goto imgend;
    }
    memset(pdotbuf, 0x0, (biHeight * dotByteCmdMax));
    /* fill up the dot data buffer */
    for (dl = 0; dl < biHeight; dl ++) {
        dw = xOffset;
#ifdef DEBUG_MODE
        //printf("%s: dl(%02d) dw(%d) biHeight(%d)\n", __func__, dl, dw, biHeight);
#endif
	for (i = 0; i < biWidth; i ++) {
		bPosByte = dl * lineByte + i /8;
		bPosInByte = i % 8;
		dPosByte = (dl * (dotByteCmdMax*8) + dw) / 8;
		dPosInByte = (dl * (dotByteCmdMax*8) + dw) % 8;
		if((pbuf[bPosByte]  &  (0x80 >> bPosInByte)) != 0) {
			pdotbuf[dPosByte] = pdotbuf[dPosByte] | (0x80 >> dPosInByte);
		}
		dw ++;
	}
#ifdef VRFN_DRV_SUPPORT
        /* setup print cmd */
        // pdotbuf[dl * dotByteCmdMax + dotByteCmdMax - 2] = PRN_DRV_TYPE_PRINT;
        // pdotbuf[dl * dotByteCmdMax + dotByteCmdMax - 1] = 0x0;        
#endif    
    }

    showBufferInTerminal(pdotbuf, biHeight, (dotByteCmdMax * 8), 0);

    /* send to printer driver */
   #if 0 
    ret = write(fd, pdotbuf, (biHeight * dotByteCmdMax));
    #else
    Printer_Fill(pdotbuf, (biHeight * dotByteCmdMax));
    #endif
   #if 0 
    if (ret != (biHeight * dotByteCmdMax)) {
#ifdef DEBUG_MODE
        printf("%s write fail (%d)%s ", __func__, ret, strerror(errno));
#endif
        ret = ERROR_FAIL;
        goto imgend;
    }
    #endif
#ifdef DEBUG_MODE
    printf("%s: write success (%d)\n", __func__, ret);
#endif
    ret = OK;

imgend:
    if (pbuf) {
        free(pbuf);
    }
    if (pdotbuf) {
        free(pdotbuf);
    }
    
    return ret;
}



/***********************************************************
*      Function     : printer_print()
*      Description  : printer print handle functions
*      Input        :   int fd - the device handle
*                       const char *format - the content need to print   
*      Output       :   char* state -   0:    success
*                                       1:  left half line string in buffer
*      Return       :   >=0 the length of string sent
*                           < 0 error
************************************************************/
int printer_printf(int fd, char* state, const char *format, ...)
{
    int ret = OK;
    va_list args;
    unsigned char inBuf[PRNT_STRING_INPUT_MAX+1];
    unsigned char *pbuf = NULL;
    prnt_char_attr_t *pstr_parsed = NULL;
    unsigned char *plinebuf = NULL;
    int freeDotLine = 0;
    int stringLen, parsedNum = 0, strBufSize;
    int i, j, k, l, h, w;
    int lineSIdx = 0, lineEIdx = 0, lineCNum = 0, lineCHMax = 0, lineH = 0, lineW = 0;
    int sLen = 0, sentIdx = 0, sentParsed = 0;
    int dl, dw, cl, cw, dPosByte, dPosInByte, cPosByte, cPosInByte;
    int dotByteCmdMax = 0;
    int chH = 0, chW = 0;
	int error;
	int fck;
	
#if 0
    if (fd < 0 || NULL == state || NULL == format) {
        return ERROR_INVALID;
    }
#endif    
    // if (fd < 0) {
    //     printf("%s(%d): print  fail - ret(%d) fd < 0\n", __func__, __LINE__, ret);
    //     return ERROR_INVALID;
    // }
    if (NULL == state) {
        printf("%s(%d): print  fail - ret(%d) NULL == state\n", __func__, __LINE__, ret);
        return ERROR_INVALID;
    }
    if (NULL == format) {
        printf("%s(%d): print  fail - ret(%d) NULL == format\n", __func__, __LINE__, ret);
        return ERROR_INVALID;
    }

    *state = PRNT_PRINT_DATA_OK;
    
    memset(inBuf, 0x0, sizeof(inBuf));
    /* read the input string to print */
    va_start(args, format);
    vsnprintf(inBuf, PRNT_STRING_INPUT_MAX, format, args);
	va_end(args);
    inBuf[sizeof(inBuf) -1] = '\0';
    stringLen = strlen(inBuf);
	
	fck_dbg("------- input string(%s), len:%d\n", inBuf, stringLen);
	
    if (stringLen == 0) {
        ret = ERROR_INVALID;
        printf("%s(%d): print  fail - ret(%d) 0 == inBuf\n", __func__, __LINE__, ret);
        return ret;
    }

    /* check if driver free dotline number is enough for one line */
    freeDotLine = get_free_dotline_num(fd);

	fck_dbg("get_free_dotline_num, freeDotLine: %d\n", freeDotLine);
	
    chH = ((PRNT_CHAR_FULL_HIGHT_MAX*2) > line_hight) ? (PRNT_CHAR_FULL_HIGHT_MAX*2) : line_hight;
    if (freeDotLine < chH) {
        return ERROR_BUSY;
    }

    /* check the left string */
    if (str_left[0] != '\0') {
        stringLen = stringLen + strlen(str_left);
    }
    strBufSize = stringLen + 1;
	
	fck_dbg("str_left(%s), len: %d\n", str_left, strlen(str_left));
	
    /* prepare char analyze buffer */
    pbuf = malloc(strBufSize);
    if (pbuf == NULL) {
        ret = ERROR_BUSY;
        goto printfend;
    }
    memset(pbuf, 0x0, strBufSize);
	
	// pstr_parsed为每个字符编码的数据
    pstr_parsed = malloc(sizeof(prnt_char_attr_t) * stringLen);
    if (pstr_parsed == NULL) {
        ret = ERROR_BUSY;
        goto printfend;
    }
    memset (pstr_parsed, 0x0, sizeof(prnt_char_attr_t) * stringLen);

    /* copy print string */
    strncpy(pbuf, str_left, strBufSize);
    strncat(pbuf, inBuf, (strBufSize - strlen(str_left)));

	fck_dbg("pbuf(%s), len: %d\n", pbuf, strlen(pbuf));
	
    if (pstr_left_parsed && str_left_num) {
        memcpy(pstr_parsed, pstr_left_parsed, sizeof(prnt_char_attr_t) * str_left_num);
    }

	fck_dbg("strBufSize(%d) stringLen(%d) pbuf(%s) str_left_len(%d) str_left_num(%d)\n",
		strBufSize, stringLen, pbuf, strlen(str_left), str_left_num);

#ifdef DEBUG_MODE
    printf("%s(%d): strBufSize(%d) stringLen(%d) pbuf(%s) str_left_len(%d) str_left_num(%d)\n", __func__, __LINE__, 
                strBufSize, stringLen, pbuf, strlen(str_left), str_left_num);
#endif
    /* Parse the input string and get the attribute such as dot matrix data */
    ret = string_parse(pbuf, stringLen, pstr_parsed, &parsedNum, strlen(str_left), str_left_num);
    if (ret != OK) {
        goto printfend;
    }

	fck_dbg("parsedNum: %d\n", parsedNum);

#ifdef VRFN_DRV_SUPPORT
    dotByteCmdMax = PRNT_DOT_BYTE_CMD_MAX;
#else
    dotByteCmdMax = PRNT_DOT_BYTE_MAX;
#endif

#ifdef DEBUG_MODE
    printf("%s(%d): parsedNum(%d) dotByteCmdMax(%d)\n", __func__, __LINE__, parsedNum, dotByteCmdMax);
#endif

    /* check if driver free dotline number is enough for this string */
    chH = 0;
    chW = 0;
    for (i = 0; i < parsedNum; i++) {
        if (is_print_char_type(pstr_parsed[i].chr_type)) {
            /* calculate the char hight and width */
            if (pstr_parsed[i].chr_double_hight) {
                pstr_parsed[i].chr_hight = pstr_parsed[i].chr_hight_cfg * 2;
			} else if (line_double_hight) {
                pstr_parsed[i].chr_hight = pstr_parsed[i].chr_hight_cfg * 2;
            } else {
                pstr_parsed[i].chr_hight = pstr_parsed[i].chr_hight_cfg;
            }
            if (pstr_parsed[i].chr_double_width) {
                pstr_parsed[i].chr_width = pstr_parsed[i].chr_width_cfg * 2;
            } else if (line_double_width) {
                pstr_parsed[i].chr_width = pstr_parsed[i].chr_width_cfg * 2;
            } else {
                pstr_parsed[i].chr_width = pstr_parsed[i].chr_width_cfg;
            }
            /* save the max value */
            if (chH < pstr_parsed[i].chr_hight) {
                chH = pstr_parsed[i].chr_hight;
            }
            if (chW < pstr_parsed[i].chr_width) {
                chW = pstr_parsed[i].chr_width;
            }
        }
    }
    if (freeDotLine < ((parsedNum /((PRNT_DOT_MAX - left_margin - right_margin)/chW) +1)*chH)) {
            ret = ERROR_BUSY;
            goto printfend;
    }

    /* clear str_left buffer */
    memset(str_left, 0x0, sizeof(str_left));
    if (pstr_left_parsed) {
        free(pstr_left_parsed);
        pstr_left_parsed = NULL;
        str_left_num = 0;
    }

    /* Orgnize and Send dot data to driver one by one line */
    for (i = 0; i < parsedNum; i++) {
		
        if (is_valid_print_control_code(pstr_parsed[i].chr_code[0]) == TRUE) {
			
			fck_dbg("i(%d) lineW(%d) lineCHMax(%d) lineCNum(%d) chr_code(0x%02x) lineSIdx(%d) lineEIdx(%d)\n", 
           		i, lineW, lineCHMax, lineCNum, pstr_parsed[i].chr_code[0], lineSIdx, lineEIdx);
			
            /* Handle a control code */
#ifdef DEBUG_MODE
   printf("%s(%d): i(%d) lineW(%d) lineCHMax(%d) lineCNum(%d) chr_code(0x%02x) lineSIdx(%d) lineEIdx(%d)\n", 
           __func__, __LINE__, i, lineW, lineCHMax, lineCNum, pstr_parsed[i].chr_code[0], lineSIdx, lineEIdx);
#endif
            /* Print contents of line buffer and advance to next line  */
            if (pstr_parsed[i].chr_code[0] == CCODE_LF || pstr_parsed[i].chr_code[0] == CCODE_CR) {

				fck_dbg("get LF char, lineCNum: %d...\r\n", lineCNum);
				
                lineEIdx = i - 1;

                if (lineCNum != 0) {
				
                    /* find the hight of this line */
                    if (lineCHMax > line_hight) {
                        lineH = lineCHMax;
                    } else {
                        lineH = line_hight;
                    }
                    /* check free dot line count */
                    if (freeDotLine < lineH) {
                        ret = ERROR_BUSY;
                        goto printfend;
                    }
                    /* print one line */
                    if (plinebuf) {
                        free(plinebuf);
                        plinebuf = NULL;
                    }
                    plinebuf = malloc(dotByteCmdMax * lineH);
                    if (plinebuf == NULL) {
                        ret = ERROR_BUSY;
                        goto printfend;
                    }
                    memset(plinebuf, 0x0, dotByteCmdMax * lineH);
                    /* fill up the dot data buffer */
                    for (dl = 0; dl < lineH; dl ++) {
                        dw = left_margin;
                        for (j = lineSIdx; j <= lineEIdx; j ++) {
                            cl = pstr_parsed[j].chr_hight - (lineH -dl);        /* line in char font */
                            cw = pstr_parsed[j].chr_width;
                            if (cl >= 0) {
#ifdef DEBUG_MODE
                            printf("%s: j(%02d) cl(%02d) dl(%02d) lineH(%d) chr_hight(%d)\n", __func__, j, cl, dl, lineH, pstr_parsed[j].chr_hight);
#endif
                                if (pstr_parsed[j].chr_hight == pstr_parsed[j].chr_hight_cfg) {
                                    h = 1;
                                } else {
                                    h = 2;
                                }
                                if (pstr_parsed[j].chr_width == pstr_parsed[j].chr_width_cfg) {
                                    /* normal width */
                                    w = 1;
                                } else {   
                                    /* double width */
                                    w = 2;
                                }
                                for (k = 0; k < pstr_parsed[j].chr_width_cfg; k ++) {
                                    cPosByte = ((cl/h) * pstr_parsed[j].chr_width_cfg + k) / 8;
                                    cPosInByte = ((cl/h) * pstr_parsed[j].chr_width_cfg + k) % 8;
                                    if (pstr_parsed[j].chr_inverse_mode == TRUE) {
                                        if((pstr_parsed[j].chr_dot_buf[cPosByte]  &  (1 << cPosInByte)) == 0) {
                                            for (l = 0; l < w; l ++) {
                                                dPosByte = (dl * (dotByteCmdMax * 8) + dw + l) / 8;
                                                dPosInByte = (dl * (dotByteCmdMax * 8) + dw + l) % 8;
                                                plinebuf[dPosByte] = plinebuf[dPosByte] | (1 << dPosInByte);
                                         }
                                        }
                                    } else {
                                        if((pstr_parsed[j].chr_dot_buf[cPosByte]  &  (1 << cPosInByte)) != 0) {
                                            for (l = 0; l < w; l ++) {
                                                dPosByte = (dl * (dotByteCmdMax * 8) + dw + l) / 8;
                                                dPosInByte = (dl * (dotByteCmdMax * 8) + dw + l) % 8;
                                                plinebuf[dPosByte] = plinebuf[dPosByte] | (1 << dPosInByte);
                                          }
                                        }
                                    }
                                    dw += w;
                                }
                            } else {
                                dw += cw;
                            }
                        }
#ifdef VRFN_DRV_SUPPORT
                        /* reverse LSB to MSB */
                        reverse_byte(&(plinebuf[dl * dotByteCmdMax]), (dotByteCmdMax - 0));
                        /* setup print cmd */
                        // plinebuf[dl * dotByteCmdMax + dotByteCmdMax - 2] = PRN_DRV_TYPE_PRINT;
                        // plinebuf[dl * dotByteCmdMax + dotByteCmdMax - 1] = 0x0;        
#endif
                    }

                    showBufferInTerminal(plinebuf, lineH, (dotByteCmdMax * 8), 0);
                    //printf("showBufferInTerminal over (dotByteCmdMax * lineH):%d\n",dotByteCmdMax * lineH);
                    /* send a string line to driver */
                    // sLen = write (fd, plinebuf, dotByteCmdMax * lineH);

                    error = Printer_Fill(plinebuf, dotByteCmdMax * lineH);
                    //printf("spi_ddi_printer_fill return:%d\n", error);
                    // if (sLen != dotByteCmdMax * lineH) {
                    //     ret = ERROR_FAIL;
                    //     goto printfend;
                    // }

                    if (plinebuf) {
                        free(plinebuf);
                        plinebuf = NULL;
                    }
                    freeDotLine -= lineH;
                    lineH = 0;
                    lineW = 0;
                    lineCNum = 0;
                    lineCHMax = 0;
                    lineSIdx = i + 1;
                    sentIdx = pstr_parsed[i].idx + pstr_parsed[i].chr_len;
                    sentParsed = i + 1;

                   /* clear character attribute */
                    chr_double_hight = FALSE;
                    chr_inverse_mode = FALSE;

#ifdef DEBUG_MODE
                       printf("%s(%d): chr_double_hight(%d)\n", __func__, __LINE__, chr_double_hight);
                       printf("%s(%d): chr_inverse_mode(%d)\n", __func__, __LINE__, chr_inverse_mode);
#endif
                    continue;

                } else {
                    /* nothing to print, advance paper for "\r" and "\n" */
                    ret = exec_print_ccode(fd, pstr_parsed[i].chr_code[0]);
                    if (ret) {
                        goto printfend;
                    }
                    lineSIdx = i + 1;
                    sentIdx = pstr_parsed[i].idx + pstr_parsed[i].chr_len;
                    sentParsed = i + 1;
                }
            }
            else {
				//printf("-----> other control code, maybe an error!!\r\n");
                /* for other control code */
                ret = exec_print_ccode(fd, pstr_parsed[i].chr_code[0]);
                if (ret) {
                    goto printfend;
                }
                lineSIdx = i + 1;
                sentIdx = pstr_parsed[i].idx + pstr_parsed[i].chr_len;
                sentParsed = i + 1;
            }
        }
        else if (is_print_char_type(pstr_parsed[i].chr_type)){
			fck_dbg("i(%d) lineW(%d) lineCHMax(%d) lineCNum(%d) chr_code(%c) lineSIdx(%d) lineEIdx(%d)\n", 
				i, lineW, lineCHMax, lineCNum, pstr_parsed[i].chr_code[0], lineSIdx, lineEIdx);
            /* Handle a printable character */
#ifdef DEBUG_MODE
   printf("%s(%d): i(%d) lineW(%d) lineCHMax(%d) lineCNum(%d) chr_code(%c) lineSIdx(%d) lineEIdx(%d)\n", 
           __func__, __LINE__, i, lineW, lineCHMax, lineCNum, pstr_parsed[i].chr_code[0], lineSIdx, lineEIdx);
#endif

            /* calculate the char hight and width */
            if (pstr_parsed[i].chr_double_hight) {
                pstr_parsed[i].chr_hight = pstr_parsed[i].chr_hight_cfg * 2;
           } else if (line_double_hight) {
                pstr_parsed[i].chr_hight = pstr_parsed[i].chr_hight_cfg * 2;
            } else {
                pstr_parsed[i].chr_hight = pstr_parsed[i].chr_hight_cfg;
            }
            if (pstr_parsed[i].chr_double_width) {
                pstr_parsed[i].chr_width = pstr_parsed[i].chr_width_cfg * 2;
           } else if (line_double_width) {
                pstr_parsed[i].chr_width = pstr_parsed[i].chr_width_cfg * 2;
            } else {
                pstr_parsed[i].chr_width = pstr_parsed[i].chr_width_cfg;
            }

            if (lineW + pstr_parsed[i].chr_width + left_margin + right_margin < PRNT_DOT_MAX) {
                 /* find the highest character */
                if (lineCHMax < pstr_parsed[i].chr_hight) {
                    lineCHMax = pstr_parsed[i].chr_hight;
                }
                /* calculate the line width */
                lineW += pstr_parsed[i].chr_width;
                lineCNum += 1;
                lineEIdx = i;
               continue;
            } else if (lineW + pstr_parsed[i].chr_width + left_margin + right_margin == PRNT_DOT_MAX) {
                 /* find the highest character */
                if (lineCHMax < pstr_parsed[i].chr_hight) {
                    lineCHMax = pstr_parsed[i].chr_hight;
                }
                /* calculate the line width */
                lineW += pstr_parsed[i].chr_width;
                lineCNum += 1;
                lineEIdx = i;
            } else {
                lineEIdx = i - 1;
            }

            /* prepare to print one line */
            if (lineW + pstr_parsed[i].chr_width + left_margin + right_margin >= PRNT_DOT_MAX) {
                /* find the hight of this line */
                if (lineCHMax > line_hight) {
                    lineH = lineCHMax;
                } else {
                    lineH = line_hight;
                }
                /* check free dot line count */
                if (freeDotLine < lineH) {
                    ret = ERROR_BUSY;
                    goto printfend;
                }
                /* print one line */
                if (plinebuf) {
                    free(plinebuf);
                    plinebuf = NULL;
                }
                plinebuf = malloc(dotByteCmdMax * lineH);
                if (plinebuf == NULL) {
                    ret = ERROR_BUSY;
                    goto printfend;
                }
                memset(plinebuf, 0x0, dotByteCmdMax * lineH);
                /* fill up the dot data buffer */
                for (dl = 0; dl < lineH; dl ++) {   /* per dot line */
                    dw = left_margin;
                    for (j = lineSIdx; j <= lineEIdx; j ++) {   /* per character */
                        cl = pstr_parsed[j].chr_hight - (lineH -dl);        /* line in char font */
                        cw = pstr_parsed[j].chr_width;
                        if (cl >= 0) {
                            if (pstr_parsed[j].chr_hight == pstr_parsed[j].chr_hight_cfg) {
                                h = 1;
                            } else {
                                h = 2;
                            }
                            if (pstr_parsed[j].chr_width == pstr_parsed[j].chr_width_cfg) {
                                /* normal width */
                                w = 1;
                            } else {   
                                /* double width */
                                w = 2;
                            }
                            for (k = 0; k < pstr_parsed[j].chr_width_cfg; k ++) {
                                cPosByte = ((cl/h) * pstr_parsed[j].chr_width_cfg + k) / 8;
                                cPosInByte = ((cl/h) * pstr_parsed[j].chr_width_cfg + k) % 8;
                                if (pstr_parsed[j].chr_inverse_mode == TRUE) {
                                    if((pstr_parsed[j].chr_dot_buf[cPosByte]  &  (1 << cPosInByte)) == 0) {
                                        for (l = 0; l < w; l ++) {
                                            dPosByte = (dl * (dotByteCmdMax * 8) + dw + l) / 8;
                                            dPosInByte = (dl * (dotByteCmdMax *8) + dw + l) % 8;
                                            plinebuf[dPosByte] = plinebuf[dPosByte] | (1 << dPosInByte);
                                        }
                                    }
                                } else {
                                    if((pstr_parsed[j].chr_dot_buf[cPosByte]  &  (1 << cPosInByte)) != 0) {
                                        for (l = 0; l < w; l ++) {
                                            dPosByte = (dl * (dotByteCmdMax *8) + dw + l) / 8;
                                            dPosInByte = (dl * (dotByteCmdMax * 8) + dw + l) % 8;
                                            plinebuf[dPosByte] = plinebuf[dPosByte] | (1 << dPosInByte);
                                        }
                                    }
                                }
                                dw += w;
                            }
                        } else {
                            /* blank area, need not fill up */
                            dw +=cw;
                        }
                    }
#ifdef VRFN_DRV_SUPPORT
                    /* reverse LSB to MSB */
                    reverse_byte(&(plinebuf[dl * dotByteCmdMax]), (dotByteCmdMax - 0));
                    /* setup print cmd */
                    // plinebuf[dl * dotByteCmdMax + dotByteCmdMax - 2] = PRN_DRV_TYPE_PRINT;
                    // plinebuf[dl * dotByteCmdMax + dotByteCmdMax - 1] = 0x0;        
#endif
                }

                showBufferInTerminal(plinebuf, lineH, (dotByteCmdMax * 8), 0);
                //printf("showBufferInTerminal over (dotByteCmdMax * lineH):%d\n",dotByteCmdMax * lineH);
                /* send a string line to driver */
  #if 0              
                sLen = write (fd, plinebuf, dotByteCmdMax * lineH);
  #else
                Printer_Fill(plinebuf, dotByteCmdMax * lineH);
  #endif              
                // if (sLen != dotByteCmdMax * lineH) {
                //     ret = ERROR_FAIL;
                //     goto printfend;
                // }

                if (plinebuf) {
                    free(plinebuf);
                    plinebuf = NULL;
                }
                freeDotLine -= lineH;
                lineH = 0;
                lineW = 0;
                lineCNum = 0;
                lineCHMax = 0;
                lineSIdx = lineEIdx + 1;
                sentIdx = pstr_parsed[lineEIdx].idx + pstr_parsed[i].chr_len;
                sentParsed = lineEIdx + 1;
                i = lineEIdx ;
            }
        }
        else {
            /* Invalid type */
            printf("%s(%d): i(%d) Invalid character\n", __func__, __LINE__, i);
            ret = ERROR_INVALID;
            goto printfend;
        }
    
    }

    if (pstr_left_parsed) {
        free(pstr_left_parsed);
        pstr_left_parsed = NULL;
        str_left_num = 0;
    }
	
    /* Is there data left ? */
    if (i == parsedNum && lineW > 0) {		
        strncpy(str_left, &pbuf[sentIdx], sizeof(str_left));
        pstr_left_parsed = malloc(sizeof(prnt_char_attr_t) * (parsedNum - sentParsed));
        if (pstr_left_parsed == NULL) {
            ret = ERROR_BUSY;
            goto printfend;
        }
        memcpy (pstr_left_parsed, &(pstr_parsed[sentParsed]), sizeof(prnt_char_attr_t) * (parsedNum - sentParsed));
        for (i = 0; i < (parsedNum - sentParsed); i++) {
            pstr_left_parsed[i].idx -=  sentIdx;
        }
        str_left_num = (parsedNum - sentParsed);
        *state = PRNT_PRINT_DATA_LEFT;
		
        fck_dbg("i(%d) lineW(%d) lineCHMax(%d) str_left(%s) str_left_num(%d)\n", 
			i, lineW, lineCHMax, str_left, str_left_num);
		
#ifdef DEBUG_MODE
        printf("%s(%d): i(%d) lineW(%d) lineCHMax(%d) str_left(%s) str_left_num(%d)\n", __func__, __LINE__, 
                    i, lineW, lineCHMax, str_left, str_left_num);
#endif
    } else {
        /* clear api buffer */
        memset(str_left, 0x0, sizeof(str_left));
        *state = PRNT_PRINT_DATA_OK;
    }

printfend:   

    if (pbuf) {
        free(pbuf); 
    }
    if (pstr_parsed) {
        free(pstr_parsed);
    }
    if (plinebuf) {
        free(plinebuf);
    }

#ifdef DEBUG_MODE
    printf("%s(%d): ret(%d) sentIdx(%d) state(%d) str_left(%s)\n", __func__, __LINE__, ret, sentIdx, *state, str_left);
#endif
    if (ret < 0) {
        return ret;
    } else {
        return (sentIdx);
    }
}

/***********************************************************
*      Function     : printer_matrix()
*      Description  : printer print handle functions
*      Input        :   int fd - the device handle
*                       print_matrix_attr_t* attr - the attribute of the this action   
*                       unsigned char* data - the font data need to print   
*                       int length - the font data length need to print   
*      Return       :   >=0 the length of string sent
*                       < 0 error
************************************************************/
int printer_matrix (int fd, print_matrix_attr_t* attr, unsigned char* data, int length)
{
    int ret = OK;
    prnt_char_attr_t *pstr_parsed = NULL;
    unsigned char *plinebuf = NULL;
    int freeDotLine = 0;
    int i, j, k, l, h, w;
    int lineSIdx = 0, lineEIdx = 0, lineCNum = 0, lineCHMax = 0, lineH = 0, lineW = 0;
    int sLen = 0;
    int dl, dw, cl, cw, dPosByte, dPosInByte, cPosByte, cPosInByte;
    int dotByteCmdMax = 0;
    int xOffset, cHeight, cWidth, fNum, cNum, cPerLine, cDotLines;
	
    if (fd < 0 || NULL == attr || NULL == data || length <= 0) {
        return ERROR_INVALID;
    }

    /* check the left string */
    if (str_left[0] != '\0') {
        char state;
#ifdef DEBUG_MODE
        printf("%s: Start str_left(%s)\n", __func__, str_left);
#endif
        ret = printer_printf(fd, &state, "\n");    
#ifdef DEBUG_MODE
        printf("%s: End ret(%d) state(%d) str_left(%s)\n", __func__, ret, state, str_left);
#endif
        if (ret < 0) {
            goto matrixend;
        }        
    }

    /* get the matrix parameters */
    xOffset = attr->offset;
    cHeight = attr->height;
    cWidth = attr->width;
    fNum = attr->blank;
    if (xOffset < 0 || cHeight < 0 || cWidth < 0 || fNum < 0) {
        return ERROR_INVALID;
    }
#ifdef MATRIX_CCODE_ATTR_SUPPORT
    cPerLine = (PRNT_DOT_MAX - xOffset - right_margin) / cWidth;
#else
    cPerLine = (PRNT_DOT_MAX - xOffset) / cWidth;
#endif
    if (cPerLine <= 0) {
        return ERROR_INVALID;
    }
    cNum = length / (attr->height * attr->width / 8);
    cDotLines = (cNum + cPerLine - 1) / cPerLine * cHeight;

    /* check driver free dotline number */
    freeDotLine = get_free_dotline_num(fd);
    if (freeDotLine < cDotLines) {
        return ERROR_BUSY;
    }

    /* prepare char analyze buffer */
    pstr_parsed = malloc(sizeof(prnt_char_attr_t) * (cNum+1));
    if (pstr_parsed == NULL) {
        ret = ERROR_BUSY;
        goto matrixend;
    }
    memset (pstr_parsed, 0x0, sizeof(prnt_char_attr_t) * (cNum+1));

#ifdef DEBUG_MODE
    printf("%s(%d): length(%d) cNum(%d) xOffset(%d) cHeight(%d) cWidth(%d) fNum(%d) cDotLines(%d)\n", __func__, __LINE__, 
                length, cNum, attr->offset,  attr->height, attr->width, attr->blank, cDotLines);
#endif
    /* Parse the input matrix and get the attribute such as dot matrix data */
    ret = matrix_parse(data, cNum, pstr_parsed, cHeight, cWidth);
    if (ret != OK) {
        goto matrixend;
    }

#ifdef VRFN_DRV_SUPPORT
    dotByteCmdMax = PRNT_DOT_BYTE_CMD_MAX;
#else
    dotByteCmdMax = PRNT_DOT_BYTE_MAX;
#endif

    /* Orgnize and Send dot data to driver one by one line */
    for (i = 0; i < cNum+1; i++) {
        if (is_valid_print_control_code(pstr_parsed[i].chr_code[0]) == TRUE) {
            /* Handle a control code */
#ifdef DEBUG_MODE
    printf("%s(%d): i(%d) lineW(%d) lineCHMax(%d) lineCNum(%d) chr_code(0x%02x) lineSIdx(%d) lineEIdx(%d)\n", 
            __func__, __LINE__, i, lineW, lineCHMax, lineCNum, pstr_parsed[i].chr_code[0], lineSIdx, lineEIdx);
#endif
            /* Print contents of line buffer and advance to next line  */
            if (pstr_parsed[i].chr_code[0] == CCODE_LF || pstr_parsed[i].chr_code[0] == CCODE_CR) {

                lineEIdx = i - 1;

                if (lineCNum != 0) {
                    /* find the hight of this line */
                    if (lineCHMax > line_hight) {
                        lineH = lineCHMax;
                    } else {
                        lineH = line_hight;
                    }
                    /* check free dot line count */
                    if (freeDotLine < lineH) {
                        ret = ERROR_BUSY;
                        goto matrixend;
                    }
                    /* print one line */
                    if (plinebuf) {
                        free(plinebuf);
                        plinebuf = NULL;
                    }
                    plinebuf = malloc(dotByteCmdMax * lineH);
                    if (plinebuf == NULL) {
                        ret = ERROR_BUSY;
                        goto matrixend;
                    }
                    memset(plinebuf, 0x0, dotByteCmdMax * lineH);
                    /* fill up the dot data buffer */
                    for (dl = 0; dl < lineH; dl ++) {
                        dw = xOffset;
                        for (j = lineSIdx; j <= lineEIdx; j ++) {
                            cl = pstr_parsed[j].chr_hight - (lineH -dl);        /* line in char font */
                            cw = pstr_parsed[j].chr_width;
                            if (cl >= 0) {
#ifdef DEBUG_MODE
                            //printf("%s: j(%02d) cl(%02d) dl(%02d) lineH(%d) chr_hight(%d)\n", __func__, j, cl, dl, lineH, pstr_parsed[j].chr_hight);
#endif
                                if (pstr_parsed[j].chr_hight == pstr_parsed[j].chr_hight_cfg) {
                                    h = 1;
                                } else {
                                    h = 2;
                                }
                                if (pstr_parsed[j].chr_width == pstr_parsed[j].chr_width_cfg) {
                                    /* normal width */
                                    w = 1;
                                } else {   
                                    /* double width */
                                    w = 2;
                                }
                                for (k = 0; k < pstr_parsed[j].chr_width_cfg; k ++) {
                                    cPosByte = ((cl/h) * pstr_parsed[j].chr_width_cfg + k) / 8;
                                    cPosInByte = ((cl/h) * pstr_parsed[j].chr_width_cfg + k) % 8;
                                    if (pstr_parsed[j].chr_inverse_mode == TRUE) {
                                        if((pstr_parsed[j].chr_dot_buf[cPosByte]  &  (1 << cPosInByte)) == 0) {
                                            for (l = 0; l < w; l ++) {
                                                dPosByte = (dl * (dotByteCmdMax * 8) + dw + l) / 8;
                                                dPosInByte = (dl * (dotByteCmdMax * 8) + dw + l) % 8;
                                                plinebuf[dPosByte] = plinebuf[dPosByte] | (1 << dPosInByte);
                                         }
                                        }
                                    } else {
                                        if((pstr_parsed[j].chr_dot_buf[cPosByte]  &  (1 << cPosInByte)) != 0) {
                                            for (l = 0; l < w; l ++) {
                                                dPosByte = (dl * (dotByteCmdMax * 8) + dw + l) / 8;
                                                dPosInByte = (dl * (dotByteCmdMax * 8) + dw + l) % 8;
                                                plinebuf[dPosByte] = plinebuf[dPosByte] | (1 << dPosInByte);
                                          }
                                        }
                                    }
                                    dw += w;
                                }
                            } else {
                                dw += cw;
                            }
                        }
#ifdef VRFN_DRV_SUPPORT
                        /* reverse LSB to MSB */
                        reverse_byte(&(plinebuf[dl * dotByteCmdMax]), (dotByteCmdMax - 0));
                        /* setup print cmd */
                        // plinebuf[dl * dotByteCmdMax + dotByteCmdMax - 2] = PRN_DRV_TYPE_PRINT;
                        // plinebuf[dl * dotByteCmdMax + dotByteCmdMax - 1] = 0x0;        
#endif
                    }

                    plinebuf[lineH * dotByteCmdMax - 1] = fNum;        
                    showBufferInTerminal(plinebuf, lineH, (dotByteCmdMax * 8), 0);
                    
                    /* send a string line to driver */
                  //  sLen = write (fd, plinebuf, dotByteCmdMax * lineH);
                    Printer_Fill(plinebuf, dotByteCmdMax * lineH);
                    #if 0
                    if (sLen != dotByteCmdMax * lineH) {
                        ret = ERROR_FAIL;
                        goto matrixend;
                    }
                    #endif
                     ret = soft_feed_paper(fd, fNum);
                    if (ret) {
                        goto matrixend;
                    }
                   if (plinebuf) {
                        free(plinebuf);
                        plinebuf = NULL;
                    }
                    freeDotLine -= lineH;
                    lineH = 0;
                    lineW = 0;
                    lineCNum = 0;
                    lineCHMax = 0;
                    lineSIdx = i + 1;

                    continue;
                } else {
                    /* nothing to print, advance paper for "\r" and "\n" */
                    ret = soft_feed_paper(fd, fNum);
                    if (ret) {
                        goto matrixend;
                    }
                    lineSIdx = i + 1;
                }
            }
        }
        else {
            /* Handle a printable character */
#ifdef DEBUG_MODE
//    printf("%s(%d): i(%d) lineW(%d) lineCHMax(%d) lineCNum(%d) chr_code(%c) lineSIdx(%d) lineEIdx(%d)\n", 
//            __func__, __LINE__, i, lineW, lineCHMax, lineCNum, pstr_parsed[i].chr_code[0], lineSIdx, lineEIdx);
#endif

#ifdef MATRIX_CCODE_ATTR_SUPPORT
            /* calculate the char hight and width */
            if (pstr_parsed[i].chr_double_hight) {
                pstr_parsed[i].chr_hight = pstr_parsed[i].chr_hight_cfg * 2;
            } else if (line_double_hight) {
                pstr_parsed[i].chr_hight = pstr_parsed[i].chr_hight_cfg * 2;
            } else {
                pstr_parsed[i].chr_hight = pstr_parsed[i].chr_hight_cfg;
            }
            if (pstr_parsed[i].chr_double_width) {
                pstr_parsed[i].chr_width = pstr_parsed[i].chr_width_cfg * 2;
            } else if (line_double_width) {
                pstr_parsed[i].chr_width = pstr_parsed[i].chr_width_cfg * 2;
            } else {
                pstr_parsed[i].chr_width = pstr_parsed[i].chr_width_cfg;
            }
#else
            pstr_parsed[i].chr_hight = pstr_parsed[i].chr_hight_cfg;
            pstr_parsed[i].chr_width = pstr_parsed[i].chr_width_cfg;
#endif

#ifdef MATRIX_CCODE_ATTR_SUPPORT
            if (lineW + pstr_parsed[i].chr_width + xOffset + right_margin < PRNT_DOT_MAX) 
#else                
            if (lineW + pstr_parsed[i].chr_width + xOffset < PRNT_DOT_MAX) 
#endif
            {
                 /* find the highest character */
                if (lineCHMax < pstr_parsed[i].chr_hight) {
                    lineCHMax = pstr_parsed[i].chr_hight;
                }
                /* calculate the line width */
                lineW += pstr_parsed[i].chr_width;
                lineCNum += 1;
                lineEIdx = i;
               continue;
            } 
#ifdef MATRIX_CCODE_ATTR_SUPPORT
            else if (lineW + pstr_parsed[i].chr_width + xOffset + right_margin == PRNT_DOT_MAX) 
#else                
            else if (lineW + pstr_parsed[i].chr_width + xOffset == PRNT_DOT_MAX) 
#endif                
            {
                 /* find the highest character */
                if (lineCHMax < pstr_parsed[i].chr_hight) {
                    lineCHMax = pstr_parsed[i].chr_hight;
                }
                /* calculate the line width */
                lineW += pstr_parsed[i].chr_width;
                lineCNum += 1;
                lineEIdx = i;
            } 
            else 
            {
                lineEIdx = i - 1;
            }

            /* prepare to print one line */
#ifdef MATRIX_CCODE_ATTR_SUPPORT
            if (lineW + pstr_parsed[i].chr_width + xOffset + right_margin >= PRNT_DOT_MAX) 
#else                
            if (lineW + pstr_parsed[i].chr_width + xOffset >= PRNT_DOT_MAX) 
#endif
            {
                /* find the hight of this line */
                if (lineCHMax > line_hight) {
                    lineH = lineCHMax;
                } else {
                    lineH = line_hight;
                }
                /* check free dot line count */
                if (freeDotLine < lineH) {
                    ret = ERROR_BUSY;
                    goto matrixend;
                }
                /* print one line */
                if (plinebuf) {
                    free(plinebuf);
                    plinebuf = NULL;
                }
                plinebuf = malloc(dotByteCmdMax * lineH);
                if (plinebuf == NULL) {
                    ret = ERROR_BUSY;
                    goto matrixend;
                }
                memset(plinebuf, 0x0, dotByteCmdMax * lineH);
                /* fill up the dot data buffer */
                for (dl = 0; dl < lineH; dl ++) {   /* per dot line */
                    dw = xOffset;
                    for (j = lineSIdx; j <= lineEIdx; j ++) {   /* per character */
                        cl = pstr_parsed[j].chr_hight - (lineH -dl);        /* line in char font */
                        cw = pstr_parsed[j].chr_width;
                        if (cl >= 0) {
                            if (pstr_parsed[j].chr_hight == pstr_parsed[j].chr_hight_cfg) {
                                h = 1;
                            } else {
                                h = 2;
                            }
                            if (pstr_parsed[j].chr_width == pstr_parsed[j].chr_width_cfg) {
                                /* normal width */
                                w = 1;
                            } else {   
                                /* double width */
                                w = 2;
                            }
                            for (k = 0; k < pstr_parsed[j].chr_width_cfg; k ++) {
                                cPosByte = ((cl/h) * pstr_parsed[j].chr_width_cfg + k) / 8;
                                cPosInByte = ((cl/h) * pstr_parsed[j].chr_width_cfg + k) % 8;
                                if (pstr_parsed[j].chr_inverse_mode == TRUE) {
                                    if((pstr_parsed[j].chr_dot_buf[cPosByte]  &  (1 << cPosInByte)) == 0) {
                                        for (l = 0; l < w; l ++) {
                                            dPosByte = (dl * (dotByteCmdMax * 8) + dw + l) / 8;
                                            dPosInByte = (dl * (dotByteCmdMax *8) + dw + l) % 8;
                                            plinebuf[dPosByte] = plinebuf[dPosByte] | (1 << dPosInByte);
                                        }
                                    }
                                } else {
                                    if((pstr_parsed[j].chr_dot_buf[cPosByte]  &  (1 << cPosInByte)) != 0) {
                                        for (l = 0; l < w; l ++) {
                                            dPosByte = (dl * (dotByteCmdMax *8) + dw + l) / 8;
                                            dPosInByte = (dl * (dotByteCmdMax * 8) + dw + l) % 8;
                                            plinebuf[dPosByte] = plinebuf[dPosByte] | (1 << dPosInByte);
                                        }
                                    }
                                }
                                dw += w;
                            }
                        } else {
                            /* blank area, need not fill up */
                            dw +=cw;
                        }
                    }
#ifdef VRFN_DRV_SUPPORT
                    /* reverse LSB to MSB */
                    reverse_byte(&(plinebuf[dl * dotByteCmdMax]), (dotByteCmdMax - 0));
                    /* setup print cmd */
                    //plinebuf[dl * dotByteCmdMax + dotByteCmdMax - 2] = PRN_DRV_TYPE_PRINT;
                    //plinebuf[dl * dotByteCmdMax + dotByteCmdMax - 1] = 0x0;        
#endif
                }

                showBufferInTerminal(plinebuf, lineH, (dotByteCmdMax * 8), 0);

                /* send a string line to driver */
              #if 0
                sLen = write (fd, plinebuf, dotByteCmdMax * lineH);
                if (sLen != dotByteCmdMax * lineH) {
                    ret = ERROR_FAIL;
                    goto matrixend;
                }
                #else
                Printer_Fill(plinebuf, (lineH * dotByteCmdMax));
                #endif
                if (plinebuf) {
                    free(plinebuf);
                    plinebuf = NULL;
                }
                freeDotLine -= lineH;
                lineH = 0;
                lineW = 0;
                lineCNum = 0;
                lineCHMax = 0;
                lineSIdx = lineEIdx + 1;
                i = lineEIdx ;
            }
        }
  
    }

matrixend:   

    if (pstr_parsed) {
        free(pstr_parsed);
    }
    if (plinebuf) {
        free(plinebuf);
    }

#ifdef DEBUG_MODE
    printf("%s(%d): ret(%d) cNum(%d) length(%d)\n", __func__, __LINE__, ret, cNum, length);
#endif
    if (ret < 0) {
        return ret;
    } else {
        return (length);
    }
}


