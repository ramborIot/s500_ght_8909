/***********************************************************
*
*      File name   : prnt_cmd.c
*      Description : printer command handle functions
*      Date        :
*      Revision    : V.1.00
*      Author      :
*      Copyright (c) 2013   Inc.                  
*
************************************************************/
#define __PRNT_CMD_C__

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <stdarg.h>
#include <sys/types.h>
#include <fcntl.h>

#include "common.h"
#include "prnt_cmd.h"
#include "prnt_mgnt.h"


prnt_font_attr_t build_in_font_attr[] = {
        {EN_16_FID, 16, 8},
        {CN_16_FID, 16, 16},
        {EN_24_FID, 24, 16},
        {CN_24_FID, 24, 24},
        {EN_12_FID, 12, 6},
        {CN_12_FID, 12, 16},
        {EN_8_FID, 8, 8},
};

#define BUILD_IN_FONT_ID_MAX   sizeof(build_in_font_attr)/sizeof(prnt_font_attr_t)

unsigned char valid_ccode []  = {
        CCODE_LF, 
        CCODE_FF, 
        CCODE_CR,
        CCODE_DC1,
        CCODE_DC2,
        CCODE_CAN,
        CCODE_ESC,
        CCODE_RS,
        CCODE_US
};

#define VALID_CCODE_NUM     sizeof(valid_ccode)

unsigned char print_ccode[] = {
        CCODE_LF, 
        CCODE_FF, 
        CCODE_CR,
        CCODE_DC1,
        CCODE_DC2,
        CCODE_RS,
        CCODE_US
};


/***********************************************************
*      Function     : printer_pattern_test()
*      Description  : Printer pattern test function
*      Input        : 
*      Output       : 
*      Return       :
************************************************************/
const unsigned char readver_cmd [] = {0x1b, 'C', 'S', ';'};
const unsigned char readID_cmd [] = {0x1b, 'i', ';'};
const unsigned char readStatus_cmd[] = {0x1b, 'd', ';'};
#define PATTERN_TEST_HEADER		"Thermal Printer\n"
#define PATTERN_BIG		    "ABCDEFGHIJKLMNOPQRSTUVWXYZ\n"
#define PATTERN_SMALL		"abcdefghijklmnopqrstuvwxyz\n"
#define PATTERN_NUMBER	    "1234567890\n"
#define PATTERN_SIGNAL	    "~!@#$%^&*()`[]{}|/\\:;,.<>?-_=+\n"
#define PATTERN_TEST_TEXT		"Printer test ��ӡ����\n"
#define PATTERN_TEST_5_1		"12PRINTER34"
#define PATTERN_TEST_5_2		"56TEST7890"
//#define PATTERN_TEST_5_1		"12����34����5"
//#define PATTERN_TEST_5_2		"6����78����90\n"
const unsigned char set_cmds0_1[] = {
    0x11, ';',  /* DC1 - Select/Deselect characters double height */
    0x1e, ';',  /* RS - Select characters double width */
    0x1b, 'S', 0, ';',     /* Sets printer speed */
    0x0
    };
const unsigned char set_cmds0_2[] = {
    0x1f, ';',  /* US - Select characters normal width */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char set_cmds1[] = {
    0x12, ';',  /* DC2 - Select/Deselect characters inverse printing */
    0x1b, 'a', 16, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 1, ';',     /* Selects English font library for printing */
    0x1b, 'L', 2, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 2, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char set_cmds2[] = {
    0x12, ';',  /* DC2 - Select/Deselect characters inverse printing */
    0x1b, 'a', 24, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 3, ';',     /* Selects English font library for printing */
    0x1b, 'L', 4, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 0, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char set_cmds3[] = {
    0x12, ';',  /* DC2 - Select/Deselect characters inverse printing */
    0x1b, 'a', 16, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 1, ';',     /* Selects English font library for printing */
    0x1b, 'L', 2, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char set_cmds4[] = {
    0x12, ';',  /* DC2 - Select/Deselect characters inverse printing */
    0x1b, 'a', 24, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 3, ';',     /* Selects English font library for printing */
    0x1b, 'L', 4, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 2, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char set_cmds5_1[] = {
//    0x11, ';',  /* DC1 - Select/Deselect characters double height */
    0x12, ';',  /* DC2 - Select/Deselect characters inverse printing */
    0x1b, 'e', 240, ';',     /* <ESC>e<n>; - right margin */
    0x1b, 'E', 10, ';',     /* <ESC>E<n>; - left margin */
    0x1b, 'a', 16, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 1, ';',     /* Selects English font library for printing */
    0x1b, 'L', 2, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 0, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char set_cmds5_2[] = {
    0x0A, ';',  /* LF - Print contents of buffer and advance to next line */
    0x0
    };
const unsigned char set_cmds5_3[] = {
    0x11, ';',  /* DC1 - Select/Deselect characters double height */
    0x12, ';',  /* DC2 - Select/Deselect characters inverse printing */
    0x1e, ';',  /* RS - Select characters double width */
    0x0
    };
const unsigned char set_cmdsEnd[] = {
    0x0C, ';',  /* FF - Print contents of buffer and advance paper about 20mm */
    0x0
    };


/***********************************************************
*      Function     : print_version()
*      Description  :Print printer API, Driver and Hardware version
*      Input        : 
*      Output       : 
*      Return       :
************************************************************/
int print_version (int fd)
{
    int ret = OK, rtype;
    char state;
    unsigned char result[256];

    if (fd < 0) {
        return ERROR_INVALID;
    }

    memset(result, 0x0, sizeof(result));
    ret = printer_read(fd, readver_cmd, result, &rtype, sizeof(result));
    if (ret < 0 || rtype != TYPE_STRING) {
        printf("%s: read version fail -ret(%d)\n", __func__, ret);
        return ERROR_FAIL;
    }
    ret = printer_printf(fd, &state, "%s", result);
    if (ret < 0) {
        printf("%s(%d): print version fail - ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    return ret;
}


/***********************************************************
*      Function     : print_ID()
*      Description  :Print printer ID
*      Input        : 
*      Output       : 
*      Return       :
************************************************************/
int print_ID (int fd)
{
    int ret = OK, rtype;
    char state;
    unsigned char result[256];

    if (fd < 0) {
        return ERROR_INVALID;
    }

    memset(result, 0x0, sizeof(result));
    ret = printer_read(fd, readID_cmd, result, &rtype, sizeof(result));
    if (ret < 0 || rtype != TYPE_STRING) {
        printf("%s: read ID fail -ret(%d)\n", __func__, ret);
        return ERROR_FAIL;
    }
    else {
        ret = printer_printf(fd, &state, "Printer ID : %s\n", result);
        if (ret < 0) {
            printf("%s(%d): print ID fail - ret(%d)\n", __func__, __LINE__, ret);
            return ret;
        }
    }

    return ret;
}


/***********************************************************
*      Function     : print_status()
*      Description  :Print printer status
*      Input        : 
*      Output       : 
*      Return       :
************************************************************/
int print_status (int fd)
{
    int ret = OK, rtype;
    char state, status;

    if (fd < 0) {
        return ERROR_INVALID;
    }

    ret = printer_read(fd, readStatus_cmd, &status, &rtype, sizeof(status));
    if (ret < 0 || rtype != TYPE_BYTE) {
        printf("%s: read status fail -ret(%d)rtype(%d)\n", __func__, ret, rtype);
        return ERROR_FAIL;
    }
    else {
        switch (status) {
            case STATUS_IDLE:
                ret = printer_printf(fd, &state, "Printer Status : IDLE\n");
                break;
            case STATUS_PRINTING:
                ret = printer_printf(fd, &state, "Printer Status : PRINTING\n");
                break;
            case STATUS_STEPING:
                ret = printer_printf(fd, &state, "Printer Status : STEPING\n");
                break;
            case STATUS_NOPAPER:
                ret = printer_printf(fd, &state, "Printer Status : NOPAPER\n");
                break;
            case STATUS_OVERTEMP:
                ret = printer_printf(fd, &state, "Printer Status : OVERTEMP\n");
                break;
            case STATUS_ERROR:	
                ret = printer_printf(fd, &state, "Printer Status : ERROR\n");
                break;
            case STATUS_FINISH:	
                ret = printer_printf(fd, &state, "Printer Status : FINISH\n");
                break;
            case STATUS_CANCE:
                ret = printer_printf(fd, &state, "Printer Status : CANCEL\n");
                break;
            default:
                ret = printer_printf(fd, &state, "Printer Status : UNKNOWN\n");
        }

        if (ret < 0) {
                printf("%s(%d): print status fail - ret(%d)\n", __func__, __LINE__, ret);
                return ret;
        }
    }

    return ret;
}


int printer_pattern_test (int fd)
{
    int ret = OK;
    char state;
    int i = 0;
    unsigned int status=0;

    if (fd < 0) {
        return ERROR_INVALID;
    }

    /* print header */
    ret = printer_command(fd, set_cmds0_1, sizeof(set_cmds0_1));
    if(ret < 0) {
        printf("%s: set printer set_cmds0_1 fail -ret(%d)\n", __func__, ret);
        return ret;
    }
    ret = printer_printf(fd, &state, PATTERN_TEST_HEADER);
    if (ret < 0) {
        printf("%s: print header fail -ret(%d)\n", __func__, ret);
        return ret;
    }

    ret = printer_command(fd, set_cmds0_2, sizeof(set_cmds0_2));
    if(ret < 0) {
        printf("%s: set printer set_cmds0_2 fail -ret(%d)\n", __func__, ret);
        return ret;
    }
    /* print version */
    ret = print_version(fd);
    if (ret < 0) {
        printf("%s: Print version fail -ret(%d)\n", __func__, ret);
        return ret;
    }
    ret = printer_printf(fd, &state, "==============================\n");
    if (ret < 0) {
        printf("%s(%d): print fail - ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

#if 0
    /* print image */
    ret = printer_graphics(prntfd, 0, "test.bmp");
    if (ret < 0) {
        printf("%s: printer_graphics fail - ret(%d)\n", __func__, ret);
        return ret;
    }
#endif

    /* print string */
    ret = printer_command(fd, set_cmds1, sizeof(set_cmds1));
    if(ret < 0) {
        printf("%s: set printer set_cmds1 fail -ret(%d)\n", __func__, ret);
    }
    ret = printer_printf(fd, &state, "%s%s"PATTERN_BIG, PATTERN_TEST_TEXT);
    if (ret < 0) {
        printf("%s(%d): print fail - ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    ret = printer_command(fd, set_cmds2, sizeof(set_cmds2));
    if(ret < 0) {
        printf("%s: set printer set_cmds2 fail -ret(%d)\n", __func__, ret);
    }
    ret = printer_printf(fd, &state, PATTERN_SMALL);
    if (ret < 0) {
        printf("%s: print PATTERN_SMALL fail - ret(%d)\n", __func__, ret);
        return ret;
    }

    ret = printer_command(fd, set_cmds3, sizeof(set_cmds3));
    if(ret < 0) {
        printf("%s: set printer set_cmds3 fail -ret(%d)\n", __func__, ret);
    }
    ret = printer_printf(fd, &state, PATTERN_NUMBER);
    if (ret < 0) {
        printf("%s: print PATTERN_NUMBER fail - ret(%d)\n", __func__, ret);
        return ret;
    }

    ret = printer_command(fd, set_cmds4, sizeof(set_cmds4));
    if(ret < 0) {
        printf("%s: set printer set_cmds4 fail -ret(%d)\n", __func__, ret);
    }
    ret = printer_printf(fd, &state, PATTERN_SIGNAL);
    if (ret < 0) {
        printf("%s: print PATTERN_SIGNAL fail - ret(%d)\n", __func__, ret);
        return ret;
    }
#if 0
    ret = printer_command(prntfd, set_cmds5_1, sizeof(set_cmds5_1));
    if(ret < 0) {
        printf("%s: set printer command5_1 fail -ret(%d)\n", __func__, ret);
    }
    ret = printer_printf(prntfd, &state, PATTERN_TEST_5_1);
    if (ret < 0) {
        printf("%s: print text5_1 fail - ret(%d)\n", __func__, ret);
        return ret;
    }
    ret = printer_command(prntfd, set_cmds5_3, sizeof(set_cmds5_3));
    if(ret < 0) {
        printf("%s: set printer command5_2 fail -ret(%d)\n", __func__, ret);
    }
    ret = printer_printf(prntfd, &state, PATTERN_TEST_5_2);
    if (ret < 0) {
        printf("%s: print text5_2 fail - ret(%d)\n", __func__, ret);
        return ret;
    }
#endif

    ret = printer_printf(fd, &state, "\n\n");
    if (ret < 0) {
        printf("%s(%d): print  fail - ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    return ret;
}


#define PRINT_CCODE_NUM     sizeof(print_ccode)

static int CmdNULL(int);

/**************************************************************
*    Control codes less than 0x20
***************************************************************/
static int CmdLF(int);
static int CmdFF(int);
static int CmdCR(int);
static int CmdDC1(int);
static int CmdDC2(int);
static int CmdCAN(int);
static int CmdRS(int);
static int CmdUS(int);

char const CCODE_CMD_IDX_TBL[] =
{
/*          0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F   */
/* 00 */    0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 2 , 3 , 0 , 0 ,
/* 10 */    0 , 4 , 5 , 0 , 0 , 0 , 0 , 0 , 6 , 0 , 0 , 0 , 0 , 0 , 7 , 8 ,
/* 20 */    0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
/* 30 */    0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 
};

static int (*const CCODE_CMD_TBL[])(int) = 
{
            CmdNULL, 
/* 00 */    CmdLF , CmdFF , CmdCR ,
/* 10 */    CmdDC1, CmdDC2, CmdCAN, CmdRS, CmdUS
};


static int CmdNULL1(int, unsigned char, unsigned char);
/***************************************************************
*  ESC command
****************************************************************/
static int ESC_Cmd_a(int, unsigned char, unsigned char);
static int ESC_Cmd_b(int, unsigned char, unsigned char);
static int ESC_Cmd_c(int, unsigned char, unsigned char);
static int ESC_Cmd_e(int, unsigned char, unsigned char);
static int ESC_Cmd_E(int, unsigned char, unsigned char);
static int ESC_Cmd_f(int, unsigned char, unsigned char);
static int ESC_Cmd_h(int, unsigned char, unsigned char);
static int ESC_Cmd_l(int, unsigned char, unsigned char);
static int ESC_Cmd_L(int, unsigned char, unsigned char);
static int ESC_Cmd_S(int, unsigned char, unsigned char);
static int ESC_Cmd_s(int, unsigned char, unsigned char);
static int ESC_Cmd_t(int, unsigned char, unsigned char);
static int ESC_Cmd_w(int, unsigned char, unsigned char);

char const ESC_CMD_IDX_TBL[] =
{
/*          0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F   */
/* 00 */    0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
/* 10 */    0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
/* 20 */    0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
/* 30 */    0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
/* 40 */    0 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 2 , 0 , 0 , 0 ,
/* 50 */    0 , 0 , 0 , 3 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
/* 60 */    0 , 4 , 5 , 6 , 0 , 7 , 8 , 0 , 9 , 0 , 0 , 0 , 10, 0 , 0 , 0 ,
/* 70 */    0 , 0 , 0 , 11 , 12, 0 , 0 , 13, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  
};

static int (*const ESC_CMD_TBL[])(int, unsigned char, unsigned char) =
{
           CmdNULL1,
/* 10 */    
/* 20 */   
/* 30 */    
/* 40 */    ESC_Cmd_E, ESC_Cmd_L,
/* 50 */    ESC_Cmd_S,
/* 60 */    ESC_Cmd_a, ESC_Cmd_b, ESC_Cmd_c, ESC_Cmd_e, ESC_Cmd_f, ESC_Cmd_h, ESC_Cmd_l, 
/* 70 */    ESC_Cmd_s, ESC_Cmd_t, ESC_Cmd_w  
};


static int CmdNULL2(int fd, unsigned char, int*, void*, int);
static int ESC_Cmd_C(int, unsigned char, int*, void*, int);
static int ESC_Cmd_d(int, unsigned char, int*, void*, int);
static int ESC_Cmd_i(int, unsigned char, int*, void*, int);

char const ESC_R_CMD_IDX_TBL[] =
{
/*          0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F   */
/* 00 */    0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
/* 10 */    0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
/* 20 */    0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
/* 30 */    0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
/* 40 */    0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
/* 50 */    0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
/* 60 */    0 , 0 , 0 , 0 , 2 , 0 , 0 , 0 , 0 , 3 , 0 , 0 , 0 , 0 , 0 , 0 ,
/* 70 */    0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  
};

static int (*const ESC_R_CMD_TBL[])(int, unsigned char, int*, void*, int) =
{
            CmdNULL2,
/* 10 */    
/* 20 */   
/* 30 */    
/* 40 */    ESC_Cmd_C,
/* 50 */    
/* 60 */    ESC_Cmd_d, ESC_Cmd_i 
/* 70 */    
};

/***********************************************************
*
*  FUNCTION    :  CmdNULL
*  DESCRIPTION : Not applicable processing
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int CmdNULL(int fd)
{
    return OK;
}


/***********************************************************
*
*  FUNCTION    :  CmdNULL
*  DESCRIPTION : Not applicable processing
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int CmdNULL1(int fd, unsigned char param1, unsigned char param2)
{
    return OK;
}


/***********************************************************
*
*  FUNCTION    :  CmdNULL
*  DESCRIPTION : Not applicable processing
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int CmdNULL2(int fd, unsigned char param, int *type, void *value, int max_len)
{
    return OK;
}


/***********************************************************
*
*  FUNCTION    : CmdLF
*  DESCRIPTION :  Line Feed handler
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int CmdLF(int fd)
{
    int ret = OK;
    int num = 0;
    char state = -1;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    /* check the left string */
    if (str_left[0] != '\0') {
#ifdef DEBUG_MODE
        printf("%s: Start str_left(%s)\n", __func__, str_left);
#endif
        ret = printer_printf(fd, &state, "\n");    
#ifdef DEBUG_MODE
        printf("%s: End ret(%d) state(%d) str_left(%s)\n", __func__, ret, state, str_left);
#endif
    } else {
        /* clear character attribute */
        chr_double_hight = FALSE;
        chr_inverse_mode = FALSE;

        num = line_hight;
#ifdef VRFN_DRV_SUPPORT
        ret = soft_feed_paper(fd, num);
#else
        ret = set_advance_paper(fd, num);
#endif

#ifdef DEBUG_MODE
        printf("%s: advance_paper ret(%d) num(%d)\n", __func__, ret, num);
#endif
    }

    return ret;
}


/***********************************************************
*
*  FUNCTION    : CmdFF
*  DESCRIPTION :  Form Feed
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int CmdFF(int fd)
{
    int ret = OK;
    int num = 0;
    char state = -1;
	   
#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    /* check the left string */
    if (str_left[0] != '\0') {
#ifdef DEBUG_MODE
        printf("%s: Start str_left(%s)\n", __func__, str_left);
#endif
        ret = printer_printf(fd, &state, "\n");    
#ifdef DEBUG_MODE
        printf("%s: End ret(%d) state(%d) str_left(%s)\n", __func__, ret, state, str_left);
#endif
    }

    /* clear character attribute */
    chr_double_hight = FALSE;
    chr_inverse_mode = FALSE;

    num = COCODE_FORM_FEED_PITCH;
    ret = set_advance_paper(fd, num);

#ifdef DEBUG_MODE
    printf("%s: advance_paper ret(%d) num(%d)\n", __func__, ret, num);
#endif
    return ret;
}


/***********************************************************
*
*  FUNCTION    :  CmdCR
*  DESCRIPTION :  Carriage Return
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int CmdCR(int fd)
{
    int ret = OK;
    int num = 0;
    char state;
	    
#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    /* check the left string */
    if (str_left[0] != '\0') {
#ifdef DEBUG_MODE
        printf("%s: Start str_left(%s)\n", __func__, str_left);
#endif
        ret = printer_printf(fd, &state, "\n");    
#ifdef DEBUG_MODE
        printf("%s: End ret(%d) state(%d) str_left(%s)\n", __func__, ret, state, str_left);
#endif
    } else {
        /* clear character attribute */
        chr_double_hight = FALSE;
        chr_inverse_mode = FALSE;

        num = line_hight;
#ifdef VRFN_DRV_SUPPORT
        ret = soft_feed_paper(fd, num);
#else
        ret = set_advance_paper(fd, num);
#endif

#ifdef DEBUG_MODE
        printf("%s: advance_paper ret(%d) num(%d)\n", __func__, ret, num);
#endif
    }

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  CmdDC1
*  DESCRIPTION : Select/Deselect characters double height
*  INPUT       : 
*  RETURN      : 
*
***********************************************************/
static int CmdDC1(int fd)
{
    int ret = OK;
	    
#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    if (chr_double_hight == FALSE) {
        chr_double_hight = TRUE;
    } else {
        chr_double_hight = FALSE;
    }

#ifdef DEBUG_MODE
    printf("%s(%d): chr_double_hight(%d)\n", __func__, __LINE__, chr_double_hight);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  CmdDC2
*  DESCRIPTION : Select/Deselect inverse printing
*  INPUT       : 
*  RETURN      : 
*
***********************************************************/
static int CmdDC2(int fd)
{
    int ret = OK;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    if (chr_inverse_mode == FALSE) {
        chr_inverse_mode = TRUE;
    } else {
        chr_inverse_mode = FALSE;
    }

#ifdef DEBUG_MODE
    printf("%s: chr_inverse_mode(%d)\n", __func__, chr_inverse_mode);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  CmdCAN
*  DESCRIPTION : Empty Print Buffer and Cancel Character Attributes 
*  INPUT       : 
*  RETURN      : 
*
***********************************************************/
static int CmdCAN(int fd)
{
    int ret = OK;
    
#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

#ifdef DEBUG_MODE
    printf("%s: enter\n", __func__);
#endif

    /* Cancel Character Attributes */
    set_char_attr_default();
    
    /* clear api buffer */
    memset(str_left, 0x0, sizeof(str_left));
    if (pstr_left_parsed) {
        free(pstr_left_parsed);
        pstr_left_parsed = NULL;
    }
    str_left_num = 0;

    /* clear driver */
    ret =set_clear_cancel(fd);

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  CmdRS
*  DESCRIPTION : Select characters double width
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int CmdRS(int fd)
{
    int ret = OK;
    
#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    chr_double_width = TRUE;

#ifdef DEBUG_MODE
    printf("%s: chr_double_width(%d)\n", __func__, chr_double_width);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  CmdUS
*  DESCRIPTION : Select characters normal width
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int CmdUS(int fd)
{
    int ret = OK;
    
#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    chr_double_width = FALSE;

#ifdef DEBUG_MODE
    printf("%s: chr_double_width(%d)\n", __func__, chr_double_width);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_a
*  DESCRIPTION : Sets line height.
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_a(int fd, unsigned char param1, unsigned char param2)
{
    int ret = OK;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    if (param1 < PRNT_LINE_HIGHT_MIN || param1 > PRNT_LINE_HIGHT_MAX) {
        param1 = PRNT_LINE_HIGHT_DEFAULT;
    }

    line_hight_cfg = param1;
    line_hight = line_hight_cfg;
#if 0    
    if (line_double_hight) {
        line_hight = line_hight_cfg * 2;
    } else {
        line_hight = line_hight_cfg;
    }
#endif

#ifdef DEBUG_MODE
    printf("%s: line_hight(%d)\n", __func__, line_hight);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_b
*  DESCRIPTION : Ejects <n> lines. 
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_b(int fd, unsigned char param1, unsigned char param2)
{
    int ret = OK;
    int num;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

#if 0
    if (param1 < PRNT_LINE_EJECT_MIN || param1 > PRNT_LINE_EJECT_MAX) {
        return ERROR_INVALID;
    }
#endif

    num = line_hight * param1;
    if (num > 0) {
#ifdef VRFN_DRV_SUPPORT
        ret = soft_feed_paper(fd, num);
#else
        ret = set_advance_paper(fd, num);
#endif
    }

#ifdef DEBUG_MODE
    printf("%s: Ejects(%d) lines, num(%d) ret(%d)\n", __func__, param1, num, ret);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_c
*  DESCRIPTION : Resets printer to power-up state.
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_c(int fd, unsigned char param1, unsigned char param2)
{
    int ret = OK;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    ret = set_printer_attr_default();
    ret = set_reset(fd);

#ifdef DEBUG_MODE
    printf("%s: ret(%s)\n", __func__, ret);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_e
*  DESCRIPTION : Sets right margin. 
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_e(int fd, unsigned char param1, unsigned char param2)
{
    int ret = OK;
    int num = 0;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    /* check validation */
    if (param2 == ';') {
        num = param1;
    } else if (param2 == 1) {
        num = param1 + 256;
    } else {
        return ERROR_INVALID;
    }
    if (num > PRNT_DOT_MAX) {
        return ERROR_INVALID;
    }

    right_margin = num;

#ifdef DEBUG_MODE
    printf("%s: right_margin(%d)\n", __func__, right_margin);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_E
*  DESCRIPTION : Sets left margin. 
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_E(int fd, unsigned char param1, unsigned char param2)
{
    int ret = OK;
    int num = 0;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    /* check validation */
    /* check validation */
    if (param2 == ';') {
        num = param1;
    } else if (param2 == 1) {
        num = param1 + 256;
    } else {
        return ERROR_INVALID;
    }
    if (num > PRNT_DOT_MAX) {
        return ERROR_INVALID;
    }

    left_margin = num;

#ifdef DEBUG_MODE
    printf("%s: left_margin(%d)\n", __func__, left_margin);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_f
*  DESCRIPTION : Selects line attributes. (normal/double height)
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_f(int fd, unsigned char param1, unsigned char param2)
{
    int ret = OK;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    if (param1 == PRNT_LINE_HIGHT_DOUBLE) {
        line_double_hight = TRUE;
//        line_hight = line_hight_cfg * 2;
    } else if (param1 == PRNT_LINE_NORMAL) {
        line_double_hight = FALSE;
//        line_hight = line_hight_cfg;
    } else {
        return ERROR_INVALID;
    }

#ifdef DEBUG_MODE
    printf("%s: line_double_hight(%d)\n", __func__, line_double_hight);
//    printf("%s: line_hight(%d)\n", __func__, line_hight);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_l
*  DESCRIPTION : Selects English font library for printing
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_l(int fd, unsigned char param1, unsigned char param2)
{
    int ret = OK;
    int i;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    for (i = 0; i < BUILD_IN_FONT_ID_MAX; i++) {
        if (build_in_font_attr[i].id == param1) {
            /* save half font lib id */
            chr_half_font_lib_idx = param1;
            /* save new char size */
            chr_half_hight_cfg = build_in_font_attr[i].hight;
            chr_half_width_cfg = build_in_font_attr[i].width;
            break;
        }
    }

    if (i == BUILD_IN_FONT_ID_MAX) {
        return ERROR_INVALID;
    }

#ifdef DEBUG_MODE
    printf("%s: chr_half_font_lib_idx(%d)\n", __func__, chr_half_font_lib_idx);
    printf("%s: chr_half_hight_cfg(%d)\n", __func__, chr_half_hight_cfg);
    printf("%s: chr_half_width_cfg(%d)\n", __func__, chr_half_width_cfg);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_L
*  DESCRIPTION : Selects Chinese font library for printing
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_L(int fd, unsigned char param1, unsigned char param2)
{
    int ret = OK;
    int i;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    for (i = 0; i < BUILD_IN_FONT_ID_MAX; i++) {
        if (build_in_font_attr[i].id == param1) {
            /* save full font lib id */
            chr_full_font_lib_idx = param1;
            /* save new char size */
            chr_full_hight_cfg = build_in_font_attr[i].hight;
            chr_full_width_cfg = build_in_font_attr[i].width;
            break;
        }
    }

    if (i == BUILD_IN_FONT_ID_MAX) {
        return ERROR_INVALID;
    }

#ifdef DEBUG_MODE
    printf("%s: chr_full_font_lib_idx(%d)\n", __func__, chr_full_font_lib_idx);
    printf("%s: chr_full_hight_cfg(%d)\n", __func__, chr_full_hight_cfg);
    printf("%s: chr_full_width_cfg(%d)\n", __func__, chr_full_width_cfg);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_S
*  DESCRIPTION : Sets printer speed
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_S(int fd, unsigned char param1, unsigned char param2)
{
    int ret = OK;
    int num;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    num = param1;
    ret = set_print_speed(fd, num);
    print_speed = num;

#ifdef DEBUG_MODE
    printf("%s: ret(%d)\n", __func__, ret);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_s
*  DESCRIPTION : Start the printer, when paper is prepared after out of paper.
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_s(int fd, unsigned char param1, unsigned char param2)
{
    int ret = OK;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    ret = set_print_start(fd);

#ifdef DEBUG_MODE
    printf("%s: ret(%d)\n", __func__, ret);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_h
*  DESCRIPTION : Sets double high
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_h(int fd, unsigned char param1, unsigned char param2)
{
    int ret = OK;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    if (param1 == PRNT_LINE_HIGHT_DOUBLE) {
        line_double_hight = TRUE;
//        line_hight = line_hight_cfg * 2;
    } else if (param1 == PRNT_LINE_NORMAL) {
        line_double_hight = FALSE;
//        line_hight = line_hight_cfg;
    } else {
        return ERROR_INVALID;
    }

#ifdef DEBUG_MODE
    printf("%s: line_double_hight(%d)\n", __func__, line_double_hight);
//    printf("%s: line_hight(%d)\n", __func__, line_hight);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_w
*  DESCRIPTION : Sets Double width or Double width and height
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_w(int fd, unsigned char param1, unsigned char param2)
{
    int ret = OK;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    /* <ESC>w */
    if (param1 == 'h') {
        /* <ESC>wh<n> */
        if (param2 == PRNT_LINE_HIGHT_DOUBLE) {
            line_double_hight = TRUE;
//            line_hight = line_hight_cfg * 2;
            line_double_width = TRUE;
        } else if (param2 == PRNT_LINE_NORMAL){
            line_double_hight = FALSE;
//            line_hight = line_hight_cfg;
            line_double_width = FALSE;
        } else {
            return ERROR_INVALID;
        }

#ifdef DEBUG_MODE
    printf("%s: line_double_hight(%d)\n", __func__, line_double_hight);
    printf("%s: line_double_width(%d)\n", __func__, line_double_width);
//    printf("%s: line_hight(%d)\n", __func__, line_hight);
#endif

    } else {
        if (param1 == PRNT_LINE_WIDTH_DOUBLE) {
            line_double_width = TRUE;
        } else {
            line_double_width = FALSE;
        }
#ifdef DEBUG_MODE
    printf("%s: line_double_width(%d)\n", __func__, line_double_width);
#endif

    }

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_t
*  DESCRIPTION : Prints a test pattern.
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_t(int fd, unsigned char param1, unsigned char param2)
{
    int ret = OK;

#if 1
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

#ifdef DEBUG_MODE
    printf("%s: Start\n", __func__);
#endif
    ret = printer_pattern_test(fd);
#ifdef DEBUG_MODE
    printf("%s: End ret(%d) str_left(%s)\n", __func__, ret, str_left);
#endif

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_C
*  DESCRIPTION : Return library and driver version.
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_C(int fd, unsigned char param, int *type, void *value, int max_len)
{
    int ret = OK;
    char str[256]={0}, drv[128]={0};

    if (NULL == type || NULL == value || max_len <= 0) {
        return ERROR_INVALID;
    }

    if (param != 'S') {
        return ERROR_INVALID;
    }

    /* read diver version */
    ret = get_hardware_ver(fd, (int *)drv);
    if (!ret) {
        sprintf(str, "API version : %s\nDriver version : %s", PRNT_API_VER, drv);
    } else {
        sprintf(str, "API version : %s\nDriver version : unknown\n", PRNT_API_VER);
    }

    if (strlen(str) < max_len) {
        strncpy((char *)value, str, max_len);
        *type = TYPE_STRING;
    } else {
        return ERROR_INVALID;
    }

    return OK;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_d
*  DESCRIPTION : Requests printer status.
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_d(int fd, unsigned char param, int *type, void *value, int max_len)
{
    int ret = OK;
    int *pnum = value;

    if (NULL == type || NULL == value || max_len <= 0) {
        return ERROR_INVALID;
    }

    /* read diver version */
    ret = get_hardware_status(fd, pnum);
    if (!ret) {
        *type = TYPE_BYTE;
    }

    return ret;
}


/***********************************************************
*
*  FUNCTION    :  ESC_Cmd_i
*  DESCRIPTION : Requests printer ID. Terminal ID is "P". 
*  INPUT       : 
*  RETURN      : 
*
********************************************************** */
static int ESC_Cmd_i(int fd, unsigned char param, int *type, void *value, int max_len)
{
    int ret = OK;
    int num;
    char str[256];

    if (NULL == type || NULL == value || max_len <= 0) {
        return ERROR_INVALID;
    }

    if (strlen(str) < max_len) {
        strncpy(value, PRNT_TERMINAL_ID, max_len);
        *type = TYPE_STRING;
    } else {
        return ERROR_INVALID;
    }

    return ret;
}


/***********************************************************
*      Function     : exec_ccode()
*      Description  : excute control code
*      Input        : 
*      Output       : 
*      Return       :
************************************************************/
int exec_ccode (int fd, unsigned char ccode)
{
    int ret;

    if (fd < 0) {
        return ERROR_INVALID;
    }

    ret = (*CCODE_CMD_TBL[ CCODE_CMD_IDX_TBL[ccode] ])(fd);

    return ret;
}


/***********************************************************
*      Function     : exec_print_ccode()
*      Description  : excute print control code
*      Input        : 
*      Output       : 
*      Return       :
************************************************************/
int exec_print_ccode (int fd, unsigned char ccode)
{
    int ret;

    if (fd < 0) {
        return ERROR_INVALID;
    }

    /* only 2 print control code supported in phase 1 !!! */
    if (ccode != CCODE_LF && ccode != CCODE_CR) {
        return ERROR_INVALID;
    }

    ret = (*CCODE_CMD_TBL[ CCODE_CMD_IDX_TBL[ccode] ])(fd);

    return ret;
}


/***********************************************************
*      Function     : exec_esc_cmd()
*      Description  : excute control code
*      Input        : 
*      Output       : 
*      Return       :
************************************************************/
int exec_esc_cmd (int fd, unsigned char cmd, unsigned char param1, unsigned char param2)
{
    int ret;

    if (fd < 0) {
        return ERROR_INVALID;
    }

#ifdef DEBUG_MODE
    printf("%s: cmd(%c) param1(%02x) param2(%02x)\n", __func__, cmd, param1, param2);
#endif

    ret = (*ESC_CMD_TBL[ ESC_CMD_IDX_TBL[cmd] ])(fd, param1, param2);

    return ret;
}


/***********************************************************
*      Function     : exec_esc_r_cmd()
*      Description  : excute esc read command
*      Input        : 
*      Output       : 
*      Return       :
************************************************************/
int exec_esc_r_cmd (int fd, unsigned char cmd, unsigned char param, int *type, void *value, int max_len)
{
    int ret;

    if (fd < 0 || NULL == type || NULL == value) {
        return ERROR_INVALID;
    }

    ret = (*ESC_R_CMD_TBL[ ESC_R_CMD_IDX_TBL[cmd] ])(fd, param, type, value, max_len);

    return ret;
}


/***********************************************************
*      Function     : is_valid_control_code()
*      Description  : Check if it is a valid control code supported
*      Input        : 
*      Output       : 
*      Return       :
************************************************************/
int is_valid_control_code (unsigned char ccode)
{
    int i;

    for (i = 0; i < VALID_CCODE_NUM; i++) {
        if (valid_ccode[i] == ccode) {
            return TRUE;
        }
    }
    return FALSE;
}

/***********************************************************
*      Function     : is_valid_print control_code()
*      Description  : Check if it is a valid print control code supported
*      Input        : 
*      Output       : 
*      Return       :
************************************************************/
int is_valid_print_control_code (unsigned char ccode)
{
    int i;

    for (i = 0; i < PRINT_CCODE_NUM; i++) {
        if (print_ccode[i] == ccode) {
            return TRUE;
        }
    }
    return FALSE;
}


/***********************************************************
*      Function     : soft_feed_paper()
*      Description  : advance paper
*      Input        : the handler of the printer
*      Output       : 
*      Return       : 
************************************************************/
int soft_feed_paper (int fd, int value)
{
    int ret = OK;
    unsigned char buf[PRNT_DOT_BYTE_CMD_MAX];
    int num,i;

	//printf("soft_feed_paper, value: %d\n", value);

	return ret;

    if (fd < 0 || value < 0) {
        return ERROR_INVALID;
    }

    num = value;
    
    //buf[48] = PRN_DRV_TYPE_FEED;	
    //buf[49] = num;

    // if ((ret = write(fd, buf, PRNT_DOT_BYTE_CMD_MAX)) != PRNT_DOT_BYTE_CMD_MAX) {
    //     return ERROR_FAIL;
    // }
    //spi_ddi_printer_feed(value);
    memset( buf, 0x00, sizeof(buf) );
    for( i = 0; i < num; i++ )
    {
		prtQueFillBuf( buf, 48 );
	}

#ifdef DEBUG_MODE
    printf("%s: soft advance value(%d) ret(%d)\n", __func__, value, ret);
#endif

    return OK;
}



