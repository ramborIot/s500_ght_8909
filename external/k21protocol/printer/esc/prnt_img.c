/***********************************************************
*                                                          
*            	File name   : prnt_img.c
*            	Description : print image functions
*            	Date          :
*            	Revision     : V.1.00
*            	Author        :
*            	Copyright (c) 2013   Inc.                  
*                                                          
************************************************************/
#define __PRNT_IMG_C__

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <stdarg.h>
#include <sys/types.h>
#include <fcntl.h>

#include "prnt_img.h"
#include "prnt_mgnt.h"


/***********************************************************
*      Function     : inverse_buffer()
*      Description  : inverse buffer functions
*      Input        : None
*      Output       : 
*      Return       :
************************************************************/
int inverse_buffer (unsigned char *pbuf, int len)
{
    int ret = OK;
    int i, j;
    unsigned char tmp;

    if (NULL == pbuf || len == 0) {
        return ERROR_INVALID;
    }

    for (i = 0; i < len; i++) {
        pbuf[i] = 0xff - pbuf[i];
#if 0
        tmp = 0x0;
        for (j = 0; j < 8; j++) {
            if (pbuf[i] & (1 << j)) {
                tmp = tmp | (0x80 >> j);
            }
        }
        pbuf[i] = tmp;
#endif        
    }

    return ret;
}


/***********************************************************
*      Function     : read_bmp()
*      Description  : read bmp data to buffer
*      Input        : None
*      Output       : 
*      Return       :
************************************************************/
int read_bmp (const char *pname, int xOffset, unsigned char **pbuf, u32 *pbiSizeImage, u32 *pbiHeight, u32 *pbiWidth, u32 *plineByte) 
{
    int ret = OK;
    FILE *fp;
    bmp_file_hearder_t file_hdr;
    bmp_info_header_t info_hdr;
    u32 bfOffBits;
    long offset;
    int i, j;
    

    if (NULL == pname || NULL == pbiSizeImage || 
        NULL == pbiHeight || NULL == pbiWidth || NULL == plineByte) {
        return ERROR_INVALID;
    }

    fp=fopen(pname, "rb");
    if (fp == NULL) {
        return ERROR_FAIL;
    }

    /* read bmp file header */
    fread(&file_hdr, sizeof(bmp_file_hearder_t), 1, fp);
    /* check the file header */
    if (file_hdr.bfType[0] != 'B' || file_hdr.bfType[1] != 'M') {
        /* not a bmp file */
		fclose(fp);
		return ERROR_INVALID;
    } 
    fread(&info_hdr, sizeof(bmp_info_header_t), 1, fp);
    if (info_hdr.biBitCount != 1) {
		fclose(fp);
        /* not a monochrome */
        return ERROR_INVALID;
    }
    if (info_hdr.biCompression != 0) {
		fclose(fp);
        /* Compressed */
        return ERROR_INVALID;
    }
    if (info_hdr.biWidth > PRNT_DOT_MAX - xOffset) {
		fclose(fp);
        /* out of print range */
        return ERROR_INVALID;
    }

    bfOffBits = file_hdr.bfOffBits;
    *pbiHeight = info_hdr.biHeight;
    *pbiWidth = info_hdr.biWidth;
    *pbiSizeImage = info_hdr.biSizeImage;
    *plineByte = info_hdr.biSizeImage / info_hdr.biHeight;

    *pbuf = malloc(*pbiSizeImage);
    if (*pbuf == NULL) {
		fclose(fp);
        return ERROR_BUSY;
    }

    /* read bit data and convert to printable format */
    memset(*pbuf, 0x0, *pbiSizeImage);
    for (i = *pbiHeight, j =0; i > 0; i--, j++) {
        offset = bfOffBits +  (i -1) * (*plineByte);
#ifdef DEBUG_MODE
        //printf("%s: offset(%08x)\n", __func__, offset);
#endif
        ret = fseek(fp, offset, SEEK_SET);
        if (ret) {
            free(*pbuf);
			fclose(fp);
            return ERROR_FAIL;
        }
        ret = fread((*pbuf + *plineByte * j), *plineByte, 1, fp);
        if (ret != 1) {
            free(*pbuf);
			fclose(fp);
            return ERROR_FAIL;
        }
    }

    inverse_buffer(*pbuf, *pbiSizeImage);

    if (*pbuf) {
        showBufferInTerminal(*pbuf, info_hdr.biHeight, (*plineByte * 8), 0);
    }
	
	fclose(fp);
    return OK;    
}


