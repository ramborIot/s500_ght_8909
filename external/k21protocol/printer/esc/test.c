/***********************************************************
*                                                          
*            	File name   : test.c
*            	Description : main
*            	Date          :
*            	Revision     : V.1.00
*            	Author        :
*            	Copyright (c) 2013   Inc.                  
*                                                          
************************************************************/
#define __TEST_C__

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <stdarg.h>
#include <sys/types.h>
#include <fcntl.h>

#include "prnt_mgnt.h"
#include "prnt_cmd.h"

int matrix_test(int fd);
time_t time_start;

typedef struct _cmd_str_t {
unsigned char cmd[256];
} cmd_str_t;


const unsigned char setcmds[] = {
    0x0A, ';',  /* LF -Print contents of buffer and advance to next line */
    0x0C, ';',  /* FF - Print contents of buffer and advance paper about 20mm */
    0x0D, ';',  /* CR - Print contents of buffer and advance to next line */
    0x11, ';',  /* DC1 - Select/Deselect characters double height */
    0x11, ';',  /* DC1 - Select/Deselect characters double height */
    0x12, ';',  /* DC2 - Select/Deselect characters inverse printing */
    0x12, ';',  /* DC2 - Select/Deselect characters inverse printing */
    0x1e, ';',  /* RS - Select characters double width */
    0x1f, ';',  /* US - Select characters normal width */
    0x18, ';',  /* CAN - Empty print buffer and cancel character attributes */
    0x1b, 'a', PRNT_LINE_HIGHT_DEFAULT, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'b', 1, ';',     /* <ESC>b<n>;  - Ejects <n> lines */
    0x1b, 'c', ';',         /* <ESC>c - Resets printer to power-up state. */
    0x1b, 'e', 1, ';',     /* <ESC>e<n>; - Sets right margin */
    0x1b, 'E', 1, ';',     /* <ESC>E<n>; - Sets left margin */
    0x1b, 'f', 1, ';',     /* <ESC>f<n>; - Selects line attributes. (double height) */
    0x1b, 'f', 0, ';',     /* <ESC>f<n>; - Selects line attributes. (normal height) */
    0x1b, 'l', 3, ';',     /* Selects English font library for printing */
    0x1b, 'L', 4, ';',     /* Selects Chinese font library for printing */
    0x1b, 's', ';',         /* <ESC>s - Start the printer, when paper is prepared after out of paper. */
    0x1b, 'S', 4, ';',     /* Sets printer speed */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 1, ';',     /* Sets line Double width */
    0x1b, 'w', 0, ';',     /* Sets line Double width */
    0x1b, 'h', 1, ';',     /* Sets line double height */
    0x1b, 'h', 0, ';',     /* Sets line double height */
    0x1b, 'w', 'h', 1, ';',     /* Double width and height of the line */
    0x1b, 'w', 'h', 0, ';',      /* Double width and height of the line */
    0x1b, 't', ';',         /* Prints a test pattern */
    0x0
    };


int prntfd;


int image_test(int fd, int xoffset)
{
    int ret = OK;
    char state;

    if (fd < 0 || xoffset < 0 || xoffset > PRNT_DOT_MAX) {
        return ERROR_INVALID;
    }
    
#ifdef DEBUG_MODE
    printf("===%s: Image Print Test Start ===\n", __func__);
#endif
    /* print interval */
    ret = print_divide(fd, "Image Print Test");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = printer_graphics(fd, xoffset, "test.bmp");
    if (ret) {
        printf("printer_graphics fail - %d\n", ret);
    } else {
        printf("printer_graphics ok \n");
    }

    ret = printer_printf(fd, &state, "\n");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
#ifdef DEBUG_MODE
    printf("===%s: Image Test End - ret(%d) ===\n\n", __func__, ret);
#endif
    return ret;
}


int command_test(void)
{
    int ret = OK;

#ifdef DEBUG_MODE
    printf("=== %s: Command Test start (%d) ===\n", __func__, sizeof(setcmds));
#endif
    ret = printer_command(prntfd, setcmds, sizeof(setcmds));

#ifdef DEBUG_MODE
    printf("===%s: Command Test end - ret(%d) ===\n\n", __func__, ret);
#endif
    return ret;
}


const unsigned char print_pattern_setcmds[] = {
    0x1b, 't', ';',         /* Prints a test pattern */
    0x0
    };

int print_pattern_test(void)
{
    int ret = OK;

#ifdef DEBUG_MODE
    printf("===%s: Print Pattern Test start (%d) ===\n", __func__, sizeof(setcmds));
#endif
    ret = printer_command(prntfd, print_pattern_setcmds, sizeof(print_pattern_setcmds));

#ifdef DEBUG_MODE
    printf("===%s: Print Pattern Test end ret(%d) ===\n\n", __func__, ret);
#endif
    return ret;
}


#define PRINT_STRING	"%s��������%s��������%s\n"
#define PRINT_BIG		"ABCDEFGHIJKLMNOPQRSTUVWXYZ\n"
#define PRINT_SMALL		"abcdefghijklmnopqrstuvwxyz"
#define PRINT_NUMBER	"1234567890"

int print_test (void) 
{
    int ret = OK;
    char state;

#ifdef DEBUG_MODE
    printf("===%s: String Print Test start===\n", __func__);
#endif
    strncpy(str_left, "==", sizeof(str_left));
    ret = printer_printf(prntfd, &state, PRINT_STRING, PRINT_BIG, PRINT_SMALL, PRINT_NUMBER);    
#ifdef DEBUG_MODE
    printf("===%s: String Print Test end ret(%d) ===\n\n", __func__, ret);
#endif

    return ret;
}

#define PRINTER_TEST_HEADER		"====== Printer Tes Start ======\n"
#define PRINTER_TEST_STRING_ALL	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890~!@#$%^&*()`[]{}|/\\:;,.<>?-_=+��֮���Ա���\n\r"
//#define PRINTER_TEST_STRING_ALL	"ABCDEFGHIJKLMNOPQRSTUVWXYZ\n"
#define PRINTER_TEST_TEXT0		"Inverse Print Test ��ת��ӡ����\n"
#define PRINTER_TEST_TEXT		"Print test ��ӡ����\n"
const unsigned char setcmds_default[] = {
    0x1b, 'e', 0, ';',     /* <ESC>e<n>; - right margin */
    0x1b, 'E', 0, ';',     /* <ESC>E<n>; - left margin */
    0x1b, 'a', 24, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 3, ';',     /* Selects English font library for printing */
    0x1b, 'L', 4, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char setcmds0[] = {
    0x12, ';',  /* DC2 - Select/Deselect characters inverse printing */
    0x0
    };
const unsigned char setcmds_c_dh[] = {
    0x11, ';',  /* DC2 - Select/Deselect characters inverse printing */
    0x0
    };
const unsigned char setcmds_c_nw[] = {
    0x1f, ';',  /* DC2 - Select/Deselect characters inverse printing */
    0x0
    };
const unsigned char setcmds_c_dw[] = {
    0x1e, ';',  /* DC2 - Select/Deselect characters inverse printing */
    0x0
    };
const unsigned char setcmds1[] = {
    0x1b, 'a', 16, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 1, ';',     /* Selects English font library for printing */
    0x1b, 'L', 2, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char setcmds2[] = {
    0x1b, 'a', 24, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 3, ';',     /* Selects English font library for printing */
    0x1b, 'L', 4, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x1b, 'w', 1, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char setcmds3[] = {
    0x1b, 'a', 16, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 1, ';',     /* Selects English font library for printing */
    0x1b, 'L', 2, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x1b, 'h', 1, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char setcmds4[] = {
    0x1b, 'a', 24, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 3, ';',     /* Selects English font library for printing */
    0x1b, 'L', 4, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 1, ';',      /* Set Double width and height of the line */
    0x0
    };


#define PRINTER_TEST_INTERVAL		"*"
int print_divide (int fd, char *pstr)
{
    int ret = OK;
    char state;

    if (fd < 0 || NULL == pstr) {
        return ERROR_INVALID;
    }

    ret = printer_printf(fd, &state, "%s %s %s\n", PRINTER_TEST_INTERVAL, pstr, PRINTER_TEST_INTERVAL);
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    
    return ret;
}


const unsigned char set_eject_line_cmd[] = {
    0x1b, 'b', 1, ';',     /* <ESC>b<n>;  - Ejects <n> lines */
    0x0
    };
int eject_line_test (int fd, int num)
{
    int ret = OK, i;

    if (fd < 0 || num < 0) {
        return ERROR_INVALID;
    }

    ret = print_divide(fd, "Eject line Test");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    for (i=0; i<num; i++) {
        ret = printer_command(fd, set_eject_line_cmd, sizeof(set_eject_line_cmd));
        if(ret < 0) {
            printf("%s(%d): set printer command fail -ret(%d)\n", __func__, __LINE__, ret);
            return ret;
        }
    }

    return ret;
}


const unsigned char setcmdsEnd[] = {
    0x0C, ';',  /* FF - Print contents of buffer and advance paper about 20mm */
    0x0
    };
int form_feed_test (int fd)
{
    int ret = OK;

    if (fd < 0) {
        return ERROR_INVALID;
    }

    ret = print_divide(fd, "Form Feed Test");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = printer_command(fd, setcmdsEnd, sizeof(setcmdsEnd));
    if(ret < 0) {
        printf("%s: set printer commandEnd fail -ret(%d)\n", __func__, ret);
    }

    return ret;
}


const unsigned char setcmd_CAN[] = {
    0x18, ';',  /* CAN - Empty print buffer and cancel character attributes */
    0x0
    };
int CANcel_test (int fd)
{
    int ret = OK;

    if (fd < 0) {
        return ERROR_INVALID;
    }

#if 0
    ret = print_divide(fd, "CANcel Test");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
#endif    
    ret = printer_command(fd, setcmd_CAN, sizeof(setcmd_CAN));
    if(ret < 0) {
        printf("%s: set printer CANcel fail -ret(%d)\n", __func__, ret);
    }

    return ret;
}


const unsigned char setcmd_Start[] = {
    0x1b, 's', ';',         /* <ESC>s - Start the printer, when paper is prepared after out of paper. */
    0x0
    };
int Start_test (int fd)
{
    int ret = OK;

    if (fd < 0) {
        return ERROR_INVALID;
    }

#if 0
    ret = print_divide(fd, "Start Test");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
#endif
    ret = printer_command(fd, setcmd_Start, sizeof(setcmd_Start));
    if(ret < 0) {
        printf("%s: set printer Start fail -ret(%d)\n", __func__, ret);
    }

    return ret;
}


const unsigned char setcmd_Reset[] = {
    0x1b, 'c', ';',         /* <ESC>c - Resets printer to power-up state. */
    0x0
    };
int Reset_test (int fd)
{
    int ret = OK;

    if (fd < 0) {
        return ERROR_INVALID;
    }

    ret = print_divide(fd, "Reset Test");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = printer_command(fd, setcmd_Reset, sizeof(setcmd_Reset));
    if(ret < 0) {
        printf("%s: set printer Reset fail -ret(%d)\n", __func__, ret);
    }

    return ret;
}


int read_printer_test (int fd)
{
    int ret = OK;
    char state;
    int i = 0, rtype;
    unsigned char result[256];

    if (fd < 0) {
        return ERROR_INVALID;
    }

#ifdef DEBUG_MODE
    printf("===%s: Read Printer Test start ===\n", __func__);
#endif

    /* print version */
    ret = print_version(fd);
    if (ret < 0) {
        printf("%s: print version fail -ret(%d)\n", __func__, ret);
        return ret;
    }

    /* print interval */
    ret = print_divide(fd, "Printer ID Print Test");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    /* print ID */
    ret = print_ID(fd);
    if (ret < 0) {
        printf("%s: print ID fail -ret(%d)\n", __func__, ret);
        return ret;
    }

    /* print interval */
    ret = print_divide(fd, "Printer Status Print Test");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    /* print ID */
    ret = print_status(fd);
    if (ret < 0) {
        printf("%s: print status fail -ret(%d)\n", __func__, ret);
        return ret;
    }

    ret = printer_printf(fd, &state, "\n");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
#ifdef DEBUG_MODE
    printf("===%s: read Printer Test end - ret(%d) ===\n\n", __func__, ret);
#endif
    return ret;
    
}


const unsigned char set_font_cmd_1[] = {
    0x1b, 'a', 16, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 1, ';',     /* Selects English font library for printing */
    0x1b, 'L', 2, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char set_font_cmd_2[] = {
    0x1b, 'a', 24, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 3, ';',     /* Selects English font library for printing */
    0x1b, 'L', 4, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char set_font_cmd_3[] = {
    0x1b, 'a', 12, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 7, ';',     /* Selects English font library for printing */
    0x1b, 'L', 6, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char set_font_cmd_4[] = {
    0x1b, 'a', 8, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 7, ';',     /* Selects English font library for printing */
    0x1b, 'L', 8, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
int string_print (int fd, char *pstr) {
    int ret = OK;
    char state;

    if (fd < 0 || NULL == pstr) {
        return ERROR_INVALID;
    }

    ret = print_divide(fd, "String Print test");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    
    /* set font to 16 */
    ret = printer_command(fd, set_font_cmd_1, sizeof(set_font_cmd_1));
    if(ret < 0) {
        printf("%s(%d): set printer command fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = printer_printf(fd, &state, "Font16 - %s", pstr);
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    /* set font to 24 */
    ret = printer_command(fd, set_font_cmd_2, sizeof(set_font_cmd_2));
    if(ret < 0) {
        printf("%s(%d): set printer command fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = printer_printf(fd, &state, "Font24 - %s", pstr);
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    /* set font to 12 */
    ret = printer_command(fd, set_font_cmd_3, sizeof(set_font_cmd_3));
    if(ret < 0) {
        printf("%s(%d): set printer command fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = printer_printf(fd, &state, "Font 12 - %s", pstr);
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
#if 0
    /* set font to 8 */
    ret = printer_command(fd, set_font_cmd_4, sizeof(set_font_cmd_4));
    if(ret < 0) {
        printf("%s(%d): set printer command fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = printer_printf(fd, &state, "Font 8 - %s", pstr);
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
#endif    
    return ret;
}


int char_attribute_test (int fd)
{
    int ret = OK;
    char state;

    if (fd < 0) {
        return ERROR_INVALID;
    }

    /* print interval */
    ret = print_divide(fd, "Inverse Print Test");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    /* inverse print test */
    ret = printer_command(fd, setcmds0, sizeof(setcmds0));
    if(ret < 0) {
        printf("%s(%d): set printer command0 fail -ret(%d)\n", __func__, __LINE__, ret);
    }
    ret = printer_printf(fd, &state, PRINTER_TEST_TEXT0);
    if (ret < 0) {
        printf("%s(%d): print text0 fail - ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    /* print interval */
    ret = print_divide(fd, "Char Attr  Print Test");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = printer_printf(fd, &state, "Char Normal - �ַ������߿� ");
    if (ret < 0) {
        printf("%s(%d): print text fail - ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = printer_command(fd, setcmds_c_dh, sizeof(setcmds_c_dh));
    if(ret < 0) {
        printf("%s(%d): set printer command fail -ret(%d)\n", __func__, __LINE__, ret);
    }
    ret = printer_printf(fd, &state, "Char Double Hight - �ַ����� ");
    if (ret < 0) {
        printf("%s(%d): print text fail - ret(%d)\n", __func__, __LINE__,  ret);
        return ret;
    }
    ret = printer_command(fd, setcmds_c_dh, sizeof(setcmds_c_dh));
    if(ret < 0) {
        printf("%s(%d): set printer command fail -ret(%d)\n", __func__, __LINE__, ret);
    }
    ret = printer_command(fd, setcmds_c_dw, sizeof(setcmds_c_dw));
    if(ret < 0) {
        printf("%s(%d): set printer command fail -ret(%d)\n", __func__, __LINE__, ret);
    }
    ret = printer_printf(fd, &state, "Char Double Width - �ַ����� ");
    if (ret < 0) {
        printf("%s(%d): print text fail - ret(%d)\n", __func__, __LINE__,  ret);
        return ret;
    }
    ret = printer_command(fd, setcmds_c_dh, sizeof(setcmds_c_dh));
    if(ret < 0) {
        printf("%s(%d): set printer command fail -ret(%d)\n", __func__, __LINE__, ret);
    }
    ret = printer_printf(fd, &state, "Char Double Hight and Width - �ַ����߱��� \n");
    if (ret < 0) {
        printf("%s(%d): print text fail - ret(%d)\n", __func__, __LINE__,  ret);
        return ret;
    }
    ret = printer_command(fd, setcmds_c_dw, sizeof(setcmds_c_dw));
    if(ret < 0) {
        printf("%s(%d): set printer command fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    ret = printer_printf(fd, &state, "\n");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    return ret;
}


int line_attribute_test (int fd)
{
    int ret = OK;
    char state;

    if (fd < 0) {
        return ERROR_INVALID;
    }

    ret = print_divide(fd, "Line Attr  Print Test");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = printer_command(fd, setcmds1, sizeof(setcmds1));
    if(ret < 0) {
        printf("%s: set printer command1 fail -ret(%d)\n", __func__, ret);
    }
    ret = printer_printf(fd, &state, "Font 16 Normal - ����16�������߿� \n");
    if (ret < 0) {
        printf("%s: print text1 fail - ret(%d)\n", __func__, ret);
        return ret;
    }
#if 0
    ret = eject_line_test(fd, 1);
    if(ret < 0) {
        printf("%s(%d): eject line fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
#endif
    ret = printer_command(fd, setcmds2, sizeof(setcmds2));
    if(ret < 0) {
        printf("%s: set printer command2 fail -ret(%d)\n", __func__, ret);
    }
    ret = printer_printf(fd, &state, "Font 24 Double Width - ����24�б���\n");
    if (ret < 0) {
        printf("%s: print text2 fail - ret(%d)\n", __func__, ret);
        return ret;
    }
#if 0
    ret = eject_line_test(fd, 1);
    if(ret < 0) {
        printf("%s(%d): eject line fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
#endif
    ret = printer_command(fd, setcmds3, sizeof(setcmds3));
    if(ret < 0) {
        printf("%s: set printer command3 fail -ret(%d)\n", __func__, ret);
    }
    ret = printer_printf(fd, &state, "Font 16 Double Hight - ����16�б���\n");
    if (ret < 0) {
        printf("%s: print text3 fail - ret(%d)\n", __func__, ret);
        return ret;
    }
#if 0
    ret = eject_line_test(fd, 1);
    if(ret < 0) {
        printf("%s(%d): eject line fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
#endif
    ret = printer_command(fd, setcmds4, sizeof(setcmds4));
    if(ret < 0) {
        printf("%s: set printer command4 fail -ret(%d)\n", __func__, ret);
    }
    ret = printer_printf(fd, &state, "Font 24 Double Hight and Width - ����24�б��߱���\n\n");
    if (ret < 0) {
        printf("%s: print text4 fail - ret(%d)\n", __func__, ret);
        return ret;
    }
#if 0
    ret = eject_line_test(fd, 1);
    if(ret < 0) {
        printf("%s(%d): eject line fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
#endif

    return ret;
}


#define PRINTER_MARGIN_TEXT		"Margin Print test ��ӡ�߽�����\n"
const unsigned char setcmds5_1[] = {
    0x1b, 'e', PRNT_DOT_MAX/2, ';',     /* <ESC>e<n>; - right margin */
    0x1b, 'E', 0, ';',     /* <ESC>E<n>; - left margin */
    0x1b, 'a', 24, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 3, ';',     /* Selects English font library for printing */
    0x1b, 'L', 4, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char setcmds5_2[] = {
    0x1b, 'e', PRNT_DOT_MAX/4, ';',     /* <ESC>e<n>; - right margin */
    0x1b, 'E', PRNT_DOT_MAX/4, ';',     /* <ESC>E<n>; - left margin */
    0x1b, 'a', 24, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 3, ';',     /* Selects English font library for printing */
    0x1b, 'L', 4, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
const unsigned char setcmds5_3[] = {
    0x1b, 'e', 0, ';',     /* <ESC>e<n>; - right margin */
    0x1b, 'E', PRNT_DOT_MAX/2, ';',     /* <ESC>E<n>; - left margin */
    0x1b, 'a', 24, ';',     /* <ESC>a<n>; - Sets line height */
    0x1b, 'l', 3, ';',     /* Selects English font library for printing */
    0x1b, 'L', 4, ';',     /* Selects Chinese font library for printing */
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x1b, 'w', 'h', 0, ';',      /* Set Double width and height of the line */
    0x0
    };
int margin_attribute_test (int fd)
{
    int ret = OK;
    char state;

    if (fd < 0) {
        return ERROR_INVALID;
    }
    
    ret = print_divide(fd, "Margin Attr Print Test");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = printer_command(prntfd, setcmds5_1, sizeof(setcmds5_1));
    if(ret < 0) {
        printf("%s: set printer command5_1 fail -ret(%d)\n", __func__, ret);
        return ret;
    }
    ret = printer_printf(prntfd, &state, "Right Margin Test\n%s", PRINTER_MARGIN_TEXT);
    if (ret < 0) {
        printf("%s: print text5_1 fail - ret(%d)\n", __func__, ret);
        return ret;
    }
    ret = printer_command(prntfd, setcmds5_2, sizeof(setcmds5_2));
    if(ret < 0) {
        printf("%s: set printer command5_2 fail -ret(%d)\n", __func__, ret);
        return ret;
    }
    ret = printer_printf(prntfd, &state, "Both Margin Test\n%s", PRINTER_MARGIN_TEXT);
    if (ret < 0) {
        printf("%s: print text5_2 fail - ret(%d)\n", __func__, ret);
        return ret;
    }
    ret = printer_command(prntfd, setcmds5_3, sizeof(setcmds5_3));
    if(ret < 0) {
        printf("%s: set printer command5_2 fail -ret(%d)\n", __func__, ret);
        return ret;
    }
    ret = printer_printf(prntfd, &state, "Left Margin Test\n%s\n", PRINTER_MARGIN_TEXT);
    if (ret < 0) {
        printf("%s: print text5_2 fail - ret(%d)\n", __func__, ret);
        return ret;
    }

    return ret;
}


const unsigned char setcmds_6_1[] = {
    0x1b, 'S', 0, ';',     /* Sets printer speed */
    0x0
    };
const unsigned char setcmds_6_2[] = {
    0x1b, 'S', 1, ';',     /* Sets printer speed */
    0x0
    };
const unsigned char setcmds_6_3[] = {
    0x1b, 'S', 2, ';',     /* Sets printer speed */
    0x0
    };
int print_speed_test (int fd)
{
    int ret = OK;
    char state;

    if (fd < 0) {
        return ERROR_INVALID;
    }
    
    ret = print_divide(fd, "Print Speed Test - fast normal slow");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = printer_command(fd, setcmds_6_1, sizeof(setcmds_6_1));
    if(ret < 0) {
        printf("%s(%d): set printer command6_1 fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = matrix_test(fd);
    if(ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = printer_command(fd, setcmds_6_2, sizeof(setcmds_6_2));
    if(ret < 0) {
        printf("%s(%d): set printer command6_1 fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = matrix_test(fd);
    if(ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = printer_command(fd, setcmds_6_3, sizeof(setcmds_6_3));
    if(ret < 0) {
        printf("%s(%d): set printer command6_1 fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    ret = matrix_test(fd);
    if(ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    return ret;
}


int printer_software_test (void)
{
    int ret = OK;
    int i = 0;
    char state;
    unsigned int status=0;

    /*  reset test*/
    ret = Reset_test(prntfd);
    if (ret < 0) {
        printf("%s(%d): reset printer fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    time(&time_start);
    	
    /* print start data */
    ret = printer_printf(prntfd, &state, "start time:%s\n",ctime(&time_start));
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }


    /* print header */
    ret = printer_printf(prntfd, &state, "---Printer reset ok!---\n");
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    /* print header */
    ret = printer_printf(prntfd, &state, PRINTER_TEST_HEADER);
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    /* read printer test */
    ret = read_printer_test(prntfd);
    if (ret < 0) {
        printf("%s(%d): read printer fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    /* print string test */
    ret = string_print(prntfd, PRINTER_TEST_STRING_ALL);
    if (ret < 0) {
        printf("%s(%d): string print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    /* print image test */
    ret = image_test(prntfd, 0);
    if (ret < 0) {
        printf("%s(%d): print image fail - ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    /* character attribute test */
    ret = char_attribute_test(prntfd);
    if (ret < 0) {
        printf("%s(%d): character attribute test fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    set_printer_attr_default();

    /* Line attribute test */
    ret = line_attribute_test(prntfd);
    if (ret < 0) {
        printf("%s(%d): line attribute test fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    set_printer_attr_default();

    /* print margin test */
    ret = margin_attribute_test(prntfd);
    if (ret < 0) {
        printf("%s(%d): print margin fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    set_printer_attr_default();

    /* speed test */
    ret = print_speed_test(prntfd);
    if(ret < 0) {
        printf("%s(%d): print speed test -ret(%d)\n", __func__, __LINE__, ret);
    }
    set_printer_attr_default();

    /* advance the paper */
    ret = eject_line_test(prntfd,  2);
    if (ret < 0) {
        printf("%s(%d): form feed fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }
    
    ret = form_feed_test(prntfd);
    if (ret < 0) {
        printf("%s(%d): form feed fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }



    return ret;
}




int Printer_Line_double_height(void)
{
   	int iret;
	char state;
	const unsigned char setcmd[] = {0x1b,'h',0,';', 0x0};
	const unsigned char cmd[] = {0x1b, 'h', 1, ';', 0x0};

   	//iret = open_printer();
	printer_printf(prntfd, &state, "�����и߲���HZAhzab####11\n");
	iret = printer_command(prntfd, cmd, sizeof(cmd));
	printf("Selects Double line height! iret = %d\n", iret);
	printer_printf(prntfd, &state, "˫����HZAhzab####12\n");

	//sleep(1);
	iret = printer_command(prntfd, setcmd, sizeof(setcmd));
	printf("Selects narmal line height! iret = %d\n", iret);
	printer_printf(prntfd, &state, "������HZAhzab####13\n");

	//close_printer();
	return 0;
}

int Printer_Line_double_width(void)
{
   	int iret;
	char state;
	const unsigned char setcmd[] = {0x1b,'w',0,';', 0x0};
	const unsigned char cmd[] = {0x1b, 'w', 1, ';', 0x0};

   //	iret = open_printer();
	printer_printf(prntfd, &state, "�����п�����HZAhzab####11\n");
	iret = printer_command(prntfd, cmd, sizeof(cmd));
	printf("Selects Double line width! iret = %d\n", iret);
	printer_printf(prntfd, &state, "˫����HZAhzab####12\n");

	//sleep(1);
	iret = printer_command(prntfd, setcmd, sizeof(setcmd));
	printf("Selects narmal line width! iret = %d\n", iret);
	printer_printf(prntfd, &state, "������HZAhzab####13\n");

//	close_printer();
	return 0;
}

int Printer_Line_double_height_width(void)
{
   	int iret;
	char state;
	const unsigned char setcmd[] = {0x1b,'w','h',0,';', 0x0};
	const unsigned char cmd[] =    {0x1b,'w','h',1,';', 0x0};

 //  	iret = open_printer();
	printer_printf(prntfd, &state, "�������߲���HZAhzab####11\n");
	iret = printer_command(prntfd, cmd, sizeof(cmd));
	printf("Selects Double line height_width! iret = %d\n", iret);
	printer_printf(prntfd, &state, "˫�����߲���HZAhzab####12\n");

	//sleep(1);
	iret = printer_command(prntfd, setcmd, sizeof(setcmd));
	printf("Selects narmal line height_width! iret = %d\n", iret);
	printer_printf(prntfd, &state, "�������߲���HZAhzab####13\n");

//	close_printer();
	return 0;
}

static unsigned char wang_24_24[24*3]={0,   0,   0,
                                 0,   0,   0,
                                 0,   0,   12,
                                 252, 247, 31,
                                 0,   28,  0,
                                 0,   28,  0,
                                 0,   28,  0,
                                 0,   28,  0,
                                 0,   28,  0,
                                 0,   28,  0,
                                 0,   28,  0,
                                 248, 255, 15,
                                 0,   28,  0,
                                 0,   28,  0,
                                 0,   28,  0,
                                 0,   28,  0,
                                 0,   28,  0,
                                 0,   28,  0,
                                 0,   28,  0,
                                 0,   28,  56,
                                 254, 255, 127,
                                 0,   0,   0,
                                 0,   0,   0,
                                 0,   0,   0};
static unsigned char ming_24_24[24*3]={0,   0,   0,
                                 0,  24,   24,
                                 0,  240,  31,
                                 248,51,   24,
                                 24, 51,   24,
                                 24, 51,   24,
                                 24, 51,   24,
                                 24, 243,  31,
                                 248,51,   24,
                                 24, 51,   24,
                                 24, 51,   24,
                                 24, 51,   24,
                                 24, 51,   24,
                                 24, 251,  31,
                                 248,59,   24,
                                 24, 24,   24,
                                 8,  24,   24,
                                 0,  24,   24,
                                 0,  8,    24,
                                 0,  6,    24,
                                 0,  131,  31,
                                 192,1,    14,
                                 0,   0,   0,
                                 0,   0,   0};

int matrix_test (int fd)
{
    int ret = OK;
    print_matrix_attr_t attr;
    unsigned char buf[24*3*1024];
    int i, j, k, num = 12;
    char state;
    
    if (fd < 0) {
        return ERROR_INVALID;
    }

#ifdef DEBUG_MODE
    printf("===%s: FONT LIB MATRIX PRINT TEST start ===\n", __func__);
#endif

    memset(&attr, 0x0, sizeof(attr));
    attr.offset = 10;
    attr.height = 24;
    attr.width = 24;
    attr.blank = 12;

    for (i = 1, k = 0; i <= num; i ++) {
        if (k + 24*3*i >= sizeof(buf)) {
            break;
        }
        for (j = 0; j < i; j ++) {
            memcpy(&buf[k], wang_24_24, 24*3);
            k += 24*3;
        }
        for (j = 0; j < i; j ++) {
            memcpy(&buf[k], ming_24_24, 24*3);
            k += 24*3;
        }
    }
    
    //printer_printf(prntfd, &state, "=== FONT LIB MATRIX PRINT TEST ===");
    ret = printer_matrix(prntfd, &attr, buf, k);
    if (ret < 0) {
        printf("printer_matrix fail - %d\n", ret);
    } else {
        printf("printer_matrix ok \n");
    }

#ifdef DEBUG_MODE
    printf("===%s: FONT LIB MATRIX PRINT TEST end  - ret(%d) ===\n\n", __func__, ret);
#endif
    return ret;
}



#if 0

int main (int argc, char * argv[]) 
{
    int ret = OK;
    int i;
    int num;
    char state;
    unsigned int status=0;

    spi_ddi_sys_init();
#ifdef DEBUG_MODE
    printf("====== Start the printer ======\n");
#endif
	prntfd = printer_open(PRNT_DEV_NAME);
	if (prntfd < 0) {
		printf("Can not open the printer : %s\n", strerror(errno));
		exit(-1);
	}
context_reprint:	


    time(&time_start);
    	
    /* print start data */
    ret = printer_printf(prntfd, &state, "start time:%s\n",ctime(&time_start));
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }

    if (argc >= 2) {
        if (memcmp(argv[1],"i",1)==0) {
            ret = image_test(prntfd, 0);
        } else if (memcmp(argv[1],"p",1)==0) {
            for(i=0; i<1; i++) {
                printf("\n%s(%d): === printer_software_test i(%d) ===\n", __func__, __LINE__, i+1);
                printer_software_test();
            }
        } else if (memcmp(argv[1],"r",1)==0) {
            read_printer_test(prntfd);
        } else if(memcmp(argv[1],"c",1)==0) {
            command_test();
        } else if (memcmp(argv[1],"s",1)==0) {
            ret = string_print(prntfd, PRINTER_TEST_STRING_ALL);
        } else if (memcmp(argv[1],"v",1)==0) {
            ret = print_version(prntfd);
        } else if (memcmp(argv[1],"t",1)==0) {
            print_pattern_test();
        } else if (memcmp(argv[1],"m",1)==0) {
         num = atoi(argv[2]);
         if(num >0 && num <30)
           for(i=0; i<num; i++) 
	   {
               matrix_test(prntfd);
	   }
         else
           printf("l: test matrix test times :little than 30\n"); 
        }
        else if (memcmp(argv[1],"l",1)==0) {
         num = atoi(argv[2]);
         if(num >0 && num <30)
           for(i=0; i<num; i++) 
	   {
              Printer_Line_double_height();
              Printer_Line_double_width();
              Printer_Line_double_height_width();
	   }
         else
           printf("l: test line test times :little than 30\n");
        }
    } else {
            printf("%s Usage:\n",argv[0]);
            printf("p: print test\n");
            printf("i: image test\n");
            printf("r: read test\n");
            printf("c: command test\n");
            printf("s: string test\n");
            printf("t: print test pattern test\n");
            printf("l: line attribute test\n");
            printf("m: matrix test\n");
   }

    time(&time_start);
    	
    /* print end buf data */
    ret = printer_printf(prntfd, &state, "buf time:%s\n",ctime(&time_start));
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }


#ifndef TEST_MODE
#ifdef VRFN_DRV_SUPPORT
	while(1)
	{
		ret = get_hardware_status(prntfd, &status);
		if(status==STATUS_FINISH)
			break;
		else if(status==STATUS_NOPAPER)
		{
			printf(" paper is out,plase assembly the paper!\npress 1 to print reserved!\npress 2 to print cancel!\npress 3 to restart print!\n");
			char c1,c2;
			c1 = getchar();
			c2 = getchar();//\n
//			printf("c1=%x,c2=%x\n",c1,c2);
			if(c1 == '1') {
			    ret = Start_test(prntfd);
				//set_print_start(prntfd);
                        }else if(c1 == '2') {
			    ret = CANcel_test(prntfd);
			   //	set_clear_cancel(prntfd);
//			   	break;
			}else if(c1 == '3'){
				 ret = CANcel_test(prntfd);
				goto context_reprint;
			}
		}
		usleep(1000);
	}
#endif    
#endif

    time(&time_start);
    time_start -= 2;	
    /* print end  data */
    ret = printer_printf(prntfd, &state, "end time:%s\n",ctime(&time_start));
    if (ret < 0) {
        printf("%s(%d): print fail -ret(%d)\n", __func__, __LINE__, ret);
        return ret;
    }



#ifndef TEST_MODE
#ifdef VRFN_DRV_SUPPORT
	while(1)
	{
		ret = get_hardware_status(prntfd, &status);
		if(status==STATUS_FINISH)
			break;
		else if(status==STATUS_NOPAPER)
		{
			printf(" paper is out,plase assembly the paper!\npress 1 to print reserved!\npress 2 to print cancel!\npress 3 to restart print!\n");
			char c1,c2;
			c1 = getchar();
			c2 = getchar();//\n
//			printf("c1=%x,c2=%x\n",c1,c2);
			if(c1 == '1') {
			    ret = Start_test(prntfd);
				//set_print_start(prntfd);
                        }else if(c1 == '2') {
			    ret = CANcel_test(prntfd);
			   //	set_clear_cancel(prntfd);
//			   	break;
			}else if(c1 == '3'){
				 ret = CANcel_test(prntfd);
				goto context_reprint;
			}
		}
		usleep(1000);
	}
#endif    
#endif

	ret = printer_close(prntfd);
	if (ret) {
		printf("Can not close the printer : %s\n", strerror(errno));
	}

#ifdef DEBUG_MODE
    printf("=====End the printer  =====\n");
#endif

}
#endif
