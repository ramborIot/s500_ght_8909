/***********************************************************
*
*      File name   : prnt_cmd.h
*      Description : printer command handle functions
*      Date        :
*      Revision    : V.1.00
*      Author      :
*      Copyright (c) 2013   Inc.                  
*
************************************************************/
#ifndef __PRNT_CMD_H__
#define __PRNT_CMD_H__


/***********************************************************
*
*     Macro definition
*
************************************************************/

#define PRNT_API_VER        "1.00"

/* value type definition */
#define TYPE_BYTE       1
#define TYPE_INT        2
#define TYPE_LONG      3
#define TYPE_STRING     4

#define PRNT_TERMINAL_ID    "P"

#define PRNT_LINE_NORMAL            0
#define PRNT_LINE_HIGHT_DOUBLE      1
#define PRNT_LINE_RESERVED1         2
#define PRNT_LINE_RESERVED2         3

#define PRNT_LINE_WIDTH_NORMAL  0
#define PRNT_LINE_WIDTH_DOUBLE  1

#define PRNT_LINE_EJECT_MIN  0
#define PRNT_LINE_EJECT_MAX  255

/***********************************************************
*
*      Control code
*
*********************************************************** */
#define     CCODE_ZERO  0x00
#define     CCODE_LF   0x0A		/* Print contents of buffer and advance to next line */
#define     CCODE_FF   0x0C		/* Print contents of buffer and advance paper about 20mm */
#define     CCODE_CR   0x0D		/* return */
#define     CCODE_DC1  0x11		/* Select/Deselect double height */
#define     CCODE_DC2  0x12		/* Select/Deselect inverse printing */
#define     CCODE_CAN  0x18		/* Empty print buffer and cancel character attributes */
#define     CCODE_ESC  0x1B		/* Signals start of escape sequence */
#define     CCODE_RS  0x1E		    /* Select double width */
#define     CCODE_US  0x1F		    /* Select normal width */
#define     CCODE_SPACE  0x20

#define COCODE_FORM_FEED_NUM                    20  /* mm */
#define PRNT_PAPER_FEED_PITCH_PER_MM    16
#define COCODE_FORM_FEED_PITCH                  (COCODE_FORM_FEED_NUM * PRNT_PAPER_FEED_PITCH_PER_MM)


/***********************************************************
*
*      Public variable declaration
*
************************************************************/




/***********************************************************
* 
*      Public function declaration                         
*
************************************************************/
int is_valid_control_code (unsigned char ccode);
int is_valid_print_control_code (unsigned char ccode);

int exec_ccode (int fd, unsigned char ccode);
int exec_print_ccode (int fd, unsigned char ccode);
int exec_esc_cmd (int fd, unsigned char cmd, unsigned char param1, unsigned char param2);
int exec_esc_r_cmd (int fd, unsigned char cmd, unsigned char param, int *type, void *value, int max_len);

int soft_feed_paper (int fd, int value);
int printer_get_status(int fd);

int print_version (int fd);
int print_ID (int fd);
int print_status (int fd);


#endif

