
#include <stdarg.h>
#include "prnt_fck.h"
#include "printer_queue.h"
#include "debug.h"

#define prt_dbg			//printf
#define Prt_Return(_x_)	do { if(_x_ < 0){prt_dbg("<line:%d> %s <Err:%d>\r\n", __LINE__, __FUNCTION__, _x_);} return (_x_); } while(0)


int fck_prnt_sys_init( void )
{
	return 0;
}

// 初始化并设置预走纸步数
int fck_prnt_init( int step )
{
	int ret;

	prtQueInit();

	spi_ddi_printer_stop();
	
	ret = spi_ddi_printer_init( step );
	if( ret != 0x00 )
	{
		Prt_Return ( ret );
	}

	ret = spi_ddi_printer_stop();
	
	Prt_Return ( ret );
}

// 启动打印并设置打印后走纸步数，此接口是阻塞的
int fck_prnt_start( int step )
{
#define PRT_SEND_LINES_MAX		( 14 )

	int ret = -1;
	int lines;
	unsigned char data[48*PRT_SEND_LINES_MAX];
	int i;
	int localLines, remoteCapacity;

	// 取打印缓存区的数据
	localLines = prtQueCheckLines();
	localLines = localLines > PRT_SEND_LINES_MAX ? PRT_SEND_LINES_MAX : localLines;
	if( localLines == 0x00 )
	{
		Prt_Return ( 0x00 );
	}

	// 按照一次最多10个点行来发送
	ret = prtQueGetBuf( data, localLines*48 );
	ret = spi_ddi_printer_fill( data, localLines*48 );
	if( ret != 0x00 )
	{
		//printf("spi_ddi_printer_fill, ret: %d, localLines:%d\r\n", ret, localLines);
		spi_ddi_printer_stop();
		Prt_Return ( ret );
	}

	// 如果缓存区里面没有数据了，那么就开始打印吧
	if(prtQueCheckLines() == 0) 
	{
		ret = spi_ddi_printer_set_fill_finish();
		if( ret != 0x00 )
		{
			//printf("spi_ddi_printer_set_fill_finish, ret: %d\r\n", ret);
			spi_ddi_printer_stop();
			Prt_Return ( ret );
		}
		
		ret = spi_ddi_printer_start(step);
		if( ret != 0x00 )
		{
			//printf("spi_ddi_printer_start, ret: %d\r\n", ret);
			spi_ddi_printer_stop();
			Prt_Return ( ret );
		}
	}
	else
	{
		ret = spi_ddi_printer_start(step);
		if( ret != 0x00 )
		{
			//printf("spi_ddi_printer_start, ret: %d\r\n", ret);
			spi_ddi_printer_stop();
			Prt_Return ( ret );
		}
		
		do {
			localLines = prtQueCheckLines();
			if( localLines == 0x00 )
				break ;
			
			localLines = localLines > PRT_SEND_LINES_MAX ? PRT_SEND_LINES_MAX : localLines;
			remoteCapacity = spi_ddi_printer_check_queue();
			//prt_dbg("localLines:%d, remoteCapacity:%d\n", localLines, remoteCapacity);
			if( remoteCapacity < 0 )
			{
				//printf("spi_ddi_printer_check_queue, ret: %d\r\n", ret);
				spi_ddi_printer_stop();
				Prt_Return ( remoteCapacity );
			}
			localLines = localLines > remoteCapacity ? remoteCapacity : localLines;

			if( localLines != 0x00 )
			{
				ret = prtQueGetBuf( data, localLines *48 );
				ret = spi_ddi_printer_fill( data, localLines *48 );
				if( ret != 0x00 )
				{
					//printf("spi_ddi_printer_fill, ret: %d\r\n", ret);
					spi_ddi_printer_stop();
					Prt_Return ( ret );
				}
			}
		} while( (ret = spi_ddi_printer_get_status()) == PRT_ERR_BUSY);

		ret = spi_ddi_printer_set_fill_finish();
		if( ret != 0x00 )
		{
			//printf("spi_ddi_printer_set_fill_finish, ret: %d\r\n", ret);
			spi_ddi_printer_stop();
			Prt_Return ( ret );
		}
	}

	while( (ret = spi_ddi_printer_get_status()) == PRT_ERR_BUSY )
		;

PRT_END:
	spi_ddi_printer_stop();
	Prt_Return( ret );
}

// 获取状态
int fck_prnt_get_status( void )
{
	int ret;
	
	ret = spi_ddi_printer_get_status();
	
	Prt_Return ( ret );
}

// 设置灰度值
int fck_prnt_set_gray( int gray )
{
	int ret;

	ret = spi_ddi_printer_set_gray( gray );

	Prt_Return ( ret );	
}

// 打印字符串
int fck_prnt_printf( char *fmt, ... )
{
    va_list marker;
    char buff[2048];
    int len;
	int state;
	int ret;
	
   	memset(buff, 0x00, sizeof(buff));
	
    va_start( marker, fmt );
    vsnprintf( buff, 1024, fmt, marker );
    va_end( marker );

	ret = printer_printf( 0, &state, "%s", buff );
	
	Prt_Return ( ret );
}

// 打印图片
int fck_prnt_graphics( int offset, char *path )
{
	int ret;

	ret = printer_graphics( 0, offset, path );
	
	Prt_Return ( ret ); 
}

// 打印数据块
int fck_prnt_matrix( int offset, int w, int h, unsigned char *data, int len )
{
	int ret;
	print_matrix_attr_t attr;

	attr.offset = offset;
	attr.width = w;
	attr.height = h;
	attr.blank = 0;
	attr.font_attribute = 0;

	ret = printer_matrix( 0, (print_matrix_attr_t *)&attr, data, len );

	return ret;
}

// 命令设置
int fck_prnt_command( unsigned char *cmd, int len )
{
	int ret;

	ret = printer_command( 0, cmd, len );
	
	Prt_Return ( ret ); 
}



int fck_prnt_fill_data( unsigned char *data, int len )
{
	int ret;

	ret = prtQueFillBuf( data, len );

	Prt_Return ( ret );
}

int data_convert( unsigned char data[][48], unsigned char *in, int width, int height )
{
	unsigned char tmp[48];
	int i, j, k;
	int x;
	
	for( j = 0; j < height; j++ )
	{
		k = 0;
		j = 7;
		for( i = 0; i < width; i++ )
		{
			tmp[k] |= ( in[i] << j);
			if(j-- < 0)
			{
				j = 7;
				k++;
			}
		}
		memcpy(data, tmp, sizeof(tmp));
		data++;
	}
	return 0;
}




