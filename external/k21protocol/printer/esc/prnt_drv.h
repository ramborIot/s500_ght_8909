/***********************************************************
*                                                          
*            	File name   : prnt_drv.h
*            	Description : printer driver functions
*            	Date          :
*            	Revision     : V.1.00
*            	Author        :
*            	Copyright (c) 2013   Inc.                  
*                                                          
************************************************************/
#ifndef __PRNT_DRV_H__
#define __PRNT_DRV_H__


/***********************************************************
*
*     Macro definition
*
************************************************************/

/* ioctl definition */
#ifdef VRFN_DRV_SUPPORT
#define BCM_PRINTER_CMD_RESET			0xf7
#define BCM_PRINTER_CMD_GET_VERSION		0xf8
#define BCM_PRINTER_CMD_CLEAR_BUF		0xf9
#define BCM_PRINTER_CMD_STEP_FWD   		0xfa
#define BCM_PRINTER_CMD_GET_STATUS  		0xfb
#define BCM_PRINTER_CMD_START	                0xfc
#define BCM_PRINTER_CMD_SET_SPEED     		0xfd
#define BCM_PRINTER_CMD_GET_FREE_BUF     	0xfe
#else
#define PRINT_FEED     _IOW('I', 1, int)
#define PRINT_CLEAR    _IO('I', 2)
#define PRINT_DEPTH    _IOW('I', 3, int)
#define PRINT_RESET    _IO('I', 4)
#define PRINT_STATUS   _IOR('I', 5, int)
#define PRINT_VERSION  _IOR('I', 6, int)
#define PRINT_FREE     _IOR('I', 7, int)
#define PRINT_PAPER    _IOW('I', 8, int)
#define PRINT_HW_TEST  _IO('I', 9)
#define PRINT_SPEED    _IOW('I', 10, int)
#define PRINT_START    _IO('I', 11)
#define PRINT_HALT     _IO('I', 12)
#define PRINT_HEADTEM  _IOW('I', 13, int)
#endif

/* print depth limit */
#define PRINT_DEPTH_MIN  1
#define PRINT_DEPTH_MAX  4
#define DEFAULT_PRINT_DEPTH  (1)


/* print paper limit */
#define PRINT_PAPER_MIN  1
#define PRINT_PAPER_MAX  16
#define DEFAULT_PRINT_PAPER  (1)

/* print speed limit */
#define PRINT_SPEED_FAST        (0)
#define PRINT_SPEED_NORMAL      (1)
#define PRINT_SPEED_SLOW        (2)
#define PRINT_SPEED_DEFAULT  PRINT_SPEED_NORMAL
#define PRINT_SPEED_MIN         PRINT_SPEED_FAST
#define PRINT_SPEED_MAX         PRINT_SPEED_SLOW

#define PRINT_HEADTEM_MIN  (-50)
#define PRINT_HEADTEM_MAX  (200)

#define DEFAULT_PRINT_HEADTEM_LOW_ALARM  (-10)
#define DEFAULT_PRINT_HEADTEM_LOW_NORMAL  (-5)
#define DEFAULT_PRINT_HEADTEM_HIGH_NORMAL  (75)
#define DEFAULT_PRINT_HEADTEM_HIGH_ALARM  (80)


#define PAPER_TF50KS_E2D   1
#define PAPER_PD150R       2
#define PAPER_PD160R_N     3
#define PAPER_F220VP       4
#define PAPER_AF50KS_E     5
#define PAPER_AF50KS_FZ    6
#define PAPER_F5041        7
#define PAPER_KT55F20      8
#define PAPER_P300         9
#define PAPER_UNDEFINED0  10
#define PAPER_UNDEFINED1  11
#define PAPER_UNDEFINED2  12
#define PAPER_UNDEFINED3  13
#define PAPER_UNDEFINED4  14
#define PAPER_UNDEFINED5  15
#define PAPER_UNDEFINED6  16
/***********************************************************
*
*      Public variable declaration
*
************************************************************/


/***********************************************************
* 
*      Public function declaration                         
*
************************************************************/

int set_clear_cancel (int fd);
int set_reset (int fd);
int set_advance_paper (int fd, int value);
int get_hardware_status (int fd, int *value);
int get_hardware_ver(int fd, int *value);
int get_free_dotline_num (int fd);
int set_print_paper (int fd, int value);
int set_hw_test(int fd);
int set_print_speed (int fd, int value);
int set_print_halt(int fd);
int set_print_start(int fd);
int set_print_headtem(int fd, int low_alarm, int low_normal, int high_normal, int high_alarm);


#endif

