/***********************************************************
*                                                          
*            	File name   : prnt_img.h
*            	Description : print image functions
*            	Date          :
*            	Revision     : V.1.00
*            	Author        :
*            	Copyright (c) 2013   Inc.                  
*                                                          
************************************************************/
#ifndef __PRNT_IMG_H__
#define __PRNT_IMG_H__

#include "common.h"

/***********************************************************
*
*     Macro definition
*
************************************************************/

typedef struct _bmp_file_hearder_t{
u8 bfType[2];                             /* should be 'BM' */
u32 bfSize;                             /* whole file size, bytes */
u16 bfReserved1;  
u16 bfReserved2; 
u32 bfOffBits;                          /* bit data offset, bytes */
}__attribute__ ((packed)) bmp_file_hearder_t;

typedef struct _bmp_info_header_t {
u32 biSize;     
s32 biWidth;                               /* width(pixel) */ 
s32 biHeight;                              /* hight(pixel) */
u16 biPlanes;    
u16 biBitCount;                         /* should be 1 for monochrome */
u32 biCompression;                  /* shoud be 0 for uncompressed */
u32 biSizeImage;                    /* bytes of bit data */
s32 biXPelsPerMeter;  
s32 biYPelsPerMeter;  
u32 biClrUsed;  
u32 biClrImportant; 
}__attribute__ ((packed)) bmp_info_header_t;


/***********************************************************
*
*      Public variable declaration
*
************************************************************/




/***********************************************************
* 
*      Public function declaration                         
*
************************************************************/

int inverse_buffer (unsigned char *pbuf, int len);
int read_bmp (const char *pname, int xOffset, unsigned char **pbuf, 
                            u32 *pbiSizeImage, u32 *pbiHeight, u32 *pbiWidth, u32 *plineByte); 









#endif

