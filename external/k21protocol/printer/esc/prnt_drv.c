/***********************************************************
*
*      File name   : prnt_drv.c
*      Description : printer driver handle functions
*      Date        :
*      Revision    : V.1.00
*      Author      :
*      Copyright (c) 2013   Inc.                  
*
************************************************************/
#define __PRNT_DRV_C__

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "common.h"
#include "prnt_drv.h"


/***********************************************************
*      Function     : set_clear_cancel()
*      Description  : Empty print buffer and cancel character attributes
*      Input        : the handler of the printer
*      Output       : 
*      Return       : 
************************************************************/
int set_clear_cancel (int fd)
{
    int ret = OK;

#if 0
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif
    
#ifndef TEST_MODE
#ifdef VRFN_DRV_SUPPORT
    ret = ioctl(fd, BCM_PRINTER_CMD_CLEAR_BUF);
#else
    ret = ioctl(fd, PRINT_CLEAR);
#endif
#endif

#ifdef DEBUG_MODE
    printf("%s: ret(%d)\n", __func__, ret);
#endif

    return ret;
}


/***********************************************************
*      Function     : set_reset()
*      Description  : Resets printer to power-up state.
*      Input        : the handler of the printer
*      Output       : 
*      Return       : 
************************************************************/
int set_reset (int fd)
{
    int ret = OK;
#if 0
#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif
    
#ifndef TEST_MODE
#ifdef VRFN_DRV_SUPPORT
    ret = ioctl(fd, BCM_PRINTER_CMD_RESET);
#else
    ret = ioctl(fd, PRINT_RESET);
#endif
#endif

#ifdef DEBUG_MODE
    printf("%s: ret(%d)\n", __func__, ret);
#endif
#else
    spi_ddi_printer_init();

#endif
    return ret;
}


/***********************************************************
*      Function     : set_advance_paper()
*      Description  : advance paper
*      Input        : the handler of the printer
*      Output       : 
*      Return       : 
************************************************************/
int set_advance_paper (int fd, int value)
{
    int ret = OK;
    int num;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    if (value < 0) {
        return ERROR_INVALID;
    }

    num = value;
    
#ifndef TEST_MODE
    if (num > 0) {
#ifdef VRFN_DRV_SUPPORT
    ret = ioctl(fd, BCM_PRINTER_CMD_STEP_FWD, &num);
#else
    ret = ioctl(fd, PRINT_FEED, &num);
#endif
    }
#endif

#ifdef DEBUG_MODE
    printf("%s: advance value(%d) ret(%d)\n", __func__, value, ret);
#endif

    return ret;
}


/***********************************************************
*      Function     : get_hardware_status()
*      Description  : get hardware status
*      Input        : the handler of the printer
*      Output       : 
*      Return       : 
************************************************************/
int get_hardware_status (int fd, int *value)
{
    int ret = OK;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif
    if (NULL == value) {
        return ERROR_INVALID;
    }

#ifndef TEST_MODE
#ifdef VRFN_DRV_SUPPORT
    ret = ioctl(fd, BCM_PRINTER_CMD_GET_STATUS, value);
#else
    ret = ioctl(fd, PRINT_STATUS, value);
#endif
#else
    *value = 0xaa;
#endif

#ifdef DEBUG_MODE
    printf("%s: ret(%d) value(0x%x)\n", __func__, ret, *value);
#endif

    return ret;
}


/***********************************************************
*      Function     : get_hardware_ver()
*      Description  : get hardware version
*      Input        : the handler of the printer
*      Output       : 
*      Return       : 
************************************************************/
int get_hardware_ver(int fd, int *value)
{
    int ret = OK;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif
    if (NULL == value) {
        return ERROR_INVALID;
    }

#ifndef TEST_MODE
#ifdef VRFN_DRV_SUPPORT
    ret = ioctl(fd, BCM_PRINTER_CMD_GET_VERSION, value);
#else
    ret = ioctl(fd, PRINT_VERSION, value);
#endif
#else
    strcpy((char *)value, "1.00");
    return ret;
#endif

#ifdef DEBUG_MODE
    printf("%s: ret(%d) value(%s)\n", __func__, ret, (char *)value);
#endif

    return ret;
}


/***********************************************************
*      Function     : get_free_dotline_num()
*      Description  : get free dot line  count from driver
*      Input        : the handler of the printer
*      Output       : 
*      Return       : 
************************************************************/
int get_free_dotline_num (int fd)
{
   #if 0 
    int ret = OK, num = 0;

#ifndef TEST_MODE
#ifdef VRFN_DRV_SUPPORT
    ret = ioctl(fd, BCM_PRINTER_CMD_GET_FREE_BUF, &num);
#else
    ret = ioctl(fd, PRINT_FREE, &num);
#endif
#else
    num = 1000;
#endif

#ifdef DEBUG_MODE
    printf("%s: ret(%d) num(%d)\n", __func__, ret, num);
#endif

    if (!ret) {
        return num;
    } else {
        return ret;
    }
   #else
    return prtQueCheckCapacity();
   #endif 
}


/***********************************************************
*      Function     : set_print_speed()
*      Description  : set print speed
*      Input        : the handler of the printer
*      Output       : 
*      Return       : 
************************************************************/
int set_print_speed (int fd, int value)
{
    int ret = OK;
    int num;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    num = value;

    if (num < PRINT_SPEED_MIN ||num > PRINT_SPEED_MAX) {
        return ERROR_INVALID;
    }
#ifndef TEST_MODE
#ifdef VRFN_DRV_SUPPORT
    ret = ioctl(fd, BCM_PRINTER_CMD_SET_SPEED, &num);
#else
    ret = ioctl(fd, PRINT_SPEED, &num); 
#endif
#endif

#ifdef DEBUG_MODE
    printf("%s: value(%d) ret(%d)\n", __func__, value, ret);
#endif

    return ret;
}


int set_print_start(int fd)
{
    int ret = OK;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

#ifndef TEST_MODE
#ifdef VRFN_DRV_SUPPORT
    ret = ioctl(fd, BCM_PRINTER_CMD_START);
#else
     ret = ioctl(fd, PRINT_START); 
#endif
#endif
  

#ifdef DEBUG_MODE
    printf("%s: ret(%d)\n", __func__, ret);
#endif
    return ret;
}

#ifndef VRFN_DRV_SUPPORT
/***********************************************************
*      Function     : set_hw_test()
*      Description  : Resets printer to power-up state.
*      Input        : the handler of the printer
*      Output       : 
*      Return       : 
************************************************************/
int set_hw_test(int fd)
{
    int ret = OK;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif
    

#ifndef TEST_MODE
   ret = ioctl(fd, PRINT_HW_TEST);
#endif
    

#ifdef DEBUG_MODE
    printf("%s: ret(%d)\n", __func__, ret);
#endif

    return ret;
}
/***********************************************************
*      Function     : set_print_paper()
*      Description  : set print paper
*      Input        : the handler of the printer
*      Output       : 
*      Return       : 
************************************************************/
int set_print_paper (int fd, int value)
{
    int ret = OK;
    int num;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif

    num = value;
    if (num < PRINT_PAPER_MIN || num > PRINT_PAPER_MAX) {
        return ERROR_INVALID;
    }

#ifndef TEST_MODE
    ret = ioctl(fd, PRINT_PAPER, &num);
#endif

#ifdef DEBUG_MODE
    printf("%s: value(%d) ret(%d)\n", __func__, value, ret);
#endif

    return ret;
}
int set_print_halt(int fd)
{
    int ret = OK;

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif
    

#ifndef TEST_MODE
     ret = ioctl(fd, PRINT_HALT); 
#endif


#ifdef DEBUG_MODE
    printf("%s: ret(%d)\n", __func__, ret);
#endif
    return ret;
}


int set_print_headtem(int fd, int low_alarm, int low_normal, int high_normal, int high_alarm)
{
    int ret = OK;
    int buf[4]={0};

#if 0    
    if (fd < 0) {
        return ERROR_INVALID;
    }
#endif
    

    buf[0]=low_alarm;
    buf[1]=low_normal;
    buf[2]=high_normal;
    buf[3]=high_alarm;

    if( (buf[0]<PRINT_HEADTEM_MIN) || (buf[1]<PRINT_HEADTEM_MIN) || (buf[2]>PRINT_HEADTEM_MAX) || (buf[3]>PRINT_HEADTEM_MAX) ){
        return ERROR_INVALID;
    }
#ifndef TEST_MODE
    ret = ioctl(fd, PRINT_HEADTEM, buf);
#endif


#ifdef DEBUG_MODE
    printf("%s: ret(%d)\n", __func__, ret);
#endif

    return ret;
}
#endif

