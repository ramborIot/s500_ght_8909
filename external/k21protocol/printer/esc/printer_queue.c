
#include "printer_queue.h"
#include "queue.h"

#define PRT_BYTES_PER_LINE ( 48 )
#define PRT_QUEUE_BUFFER_LINES ( 10000 ) 

static unsigned char prtPrnBuf[PRT_BYTES_PER_LINE*PRT_QUEUE_BUFFER_LINES+1];
static T_Queue prtPrnQue;

int prtQueInit(void)
{
	QueInit(&prtPrnQue, prtPrnBuf, sizeof(prtPrnBuf));
	return ( PRT_OK );
}

/*
fill dot line data to print buffer
not use in ISR, need cirtical
*/
int prtQueFillBuf(char *buf, int len)
{
	int elen;

//	cpsid();	
	elen = QueCheckCapability(&prtPrnQue);
//	cpsie();
	if (elen < len)
	{
		/*
		log("<%d, %d, %d, %d>(%d, %d, %d)(%d, %d, %d)", elen, len, dBytes, dLine, 
				prtPrnQue.head, prtPrnQue.tail, prtPrnQue.flag, head, tail, flag);
				*/
		return ( PRT_ERR_BUF_FULL );
	}
//	cpsid();
	QuePuts(&prtPrnQue, buf, len);
//	cpsie();
	
	return ( PRT_OK );
}

/*
used in ISR
*/
int prtQueGetBufFromISR(char *buf, int len)
{
	int glen;
	
	glen = QueGets(&prtPrnQue, buf, len);
	return glen;
}

/*
NOT USE
*/
int prtQueGetBuf(unsigned char *buf, int len)
{
	int glen;

	if(QueCheckElements(&prtPrnQue) < len)
		return 0;
//	cpsid();
	glen = QueGets(&prtPrnQue, buf, len);
//	cpsie();
	return glen;
}


/*
check how many dot line can be fill
used NOT in ISR
*/
int prtQueCheckCapacity(void)
{
	//int bytes;
	int line;

//	cpsid();
	line = QueCheckCapability(&prtPrnQue) / PRT_BYTES_PER_LINE;
	/*
	line = bytes/PRT_BYTES_PER_LINE;
	dBytes = bytes;
	dLine = line;
	head = prtPrnQue.head;
	tail = prtPrnQue.tail;
	flag = prtPrnQue.flag;
	*/
//	cpsie();
	
	return line;
}

/*
check how many dot line exist
used only in ISR, needn't critical 
*/
int prtQueCheckLines(void)
{
	int line;

	//portENTER_CRITICAL();
	line = QueCheckElements(&prtPrnQue) / PRT_BYTES_PER_LINE;
	//portEXIT_CRITICAL();
	return line;
}

/*
reset the prn buffer
*/
int prtQueReset(void)
{

//	portENTER_CRITICAL();
	QueReset(&prtPrnQue);
//	portEXIT_CRITICAL();
	return ( PRT_OK );
}

