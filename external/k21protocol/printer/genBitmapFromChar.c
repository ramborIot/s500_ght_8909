#include <errno.h>
#include <iconv.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <ft2build.h>
#include FT_FREETYPE_H

#include "genBitmapFromChar.h"
#include "conf.h"
#include "debug.h"

#define OUTLEN 255


#define MAXSIZE   64*64/8
//配置头个数
static int confLen = 0;
//配置文件
static int fontValue[30] = {0};
static int g_charwidth[30];
static int g_charheigh[30];
static char fontFileName[30][128];
static int fontType[30];
static int offsetValue1[30];
static int offsetValue2[30];
static int offsetMode[30];
static int mode[30];
//二进制数据
static unsigned char buffer[MAXSIZE];
//字库读到内存
unsigned char *getBuffer = NULL;
unsigned char *getEngBuffer = NULL;
//选用的字体序号在数组的位置
static int useValue=0;

static unsigned char *pFontFile[30];

static int GetCharBitmap(int iCharWidth,  int iCharHeigh, unsigned int uiCharCode);

static int setBufferVal( int x,int y,int w,int h,int val );


int code_convert(char *from_charset, char *to_charset, char *inbuf, int inlen, char *outbuf, int outlen)
{
	iconv_t cd;
	size_t ret;

	char *inp = inbuf;
	char *outp = outbuf;
	size_t il = (size_t)inlen;
	size_t ol = (size_t)outlen;

	LOG("form charset:%s to %s", from_charset, to_charset);

	cd = iconv_open(to_charset,from_charset);
	if ((iconv_t)-1 == cd) { 
		perror("iconv_open():");
		if (EINVAL == errno) {
			fprintf(stderr, "The conversion from %s to %s is not supported.\n", from_charset, to_charset);
		}
		return -1;
	}
	memset(outbuf,0,outlen);
	ret = iconv(cd, (char**)&inp, &il, (char**)&outp, &ol);
	if (-1 == ret) {
		perror("iconv():");
		return ret;
	}
	iconv_close(cd);
	return 0;
}

int setBufferVal( int x,int y,int w,int h,int val )
{
	int posByte = 0;
	int posInByte = 0;
	posByte = (y*w+x)/8;
	posInByte =  (y*w+x)%8;
	if( val == 0 )
	{
		buffer[posByte] =( buffer[posByte]  & (~(1<<posInByte)));
	}
	else if( val == 1 )
	{
		buffer[posByte] = (buffer[posByte]  |  ((1<<posInByte)));
	} 
	return 0;
}

int GetCharBitmap(int iCharWidth,  int iCharHeigh, unsigned int uiCharCode)
{
     FT_Library ftLibrary;
     FT_Error ftError = FT_Init_FreeType(&ftLibrary);
     if(ftError)
     {
         printf("Init freetype library fail!\n");
         return -1;
     }
     FT_Face ftFace;
     ftError = FT_New_Face( ftLibrary, fontFileName[useValue], 0, &ftFace );
     if(ftError == FT_Err_Unknown_File_Format)
     {
         printf("Error! Could not support this format!\n");
         FT_Done_FreeType(ftLibrary);
         return -1;
     }
     else if(ftError)
     {
         printf("Error! Could not open file %s!\n",fontFileName[useValue]);
         FT_Done_FreeType(ftLibrary);
         return -1;
     }

    ftError = FT_Set_Pixel_Sizes(ftFace, iCharWidth, iCharHeigh);
     if(ftError)
     {
         printf("Set pixel sizes to %d*%d error!\n", iCharWidth, iCharHeigh);
         FT_Done_Face(ftFace);
         FT_Done_FreeType(ftLibrary);
         return -1;
     }

     FT_UInt uiGlyphIndex = FT_Get_Char_Index(ftFace, uiCharCode);

     FT_Load_Glyph(ftFace,uiGlyphIndex, FT_LOAD_DEFAULT);

     FT_Render_Glyph(ftFace->glyph, FT_RENDER_MODE_MONO);

	//FT_Bitmap bitmap =  ftFace->glyph->bitmap;
    //int left =ftFace->glyph->bitmap_left;
    //int top =ftFace->glyph->bitmap_top;
     int iRow = 0, iCol = 0;
     int leftBlank = 0;
     int rightBlank = 0;
     int topBlank = 0;
     int bottomBlank = 0;
     leftBlank =  (iCharWidth - ftFace->glyph->bitmap.width)/2;
     rightBlank = iCharWidth - ftFace->glyph->bitmap.width - leftBlank;
     topBlank =  (iCharHeigh -  ftFace->glyph->bitmap.rows)/2;
     bottomBlank = iCharHeigh -  ftFace->glyph->bitmap.rows -topBlank;
     
     memset( buffer,sizeof(iCharWidth*iCharHeigh/8),0 );
     
     int i = 0;
     int j = 0;
     for( i = 0;i<topBlank;i++ )
     {
		 for(j = 0; j< iCharWidth; j++)
		 {
			setBufferVal(j,i,iCharWidth,iCharHeigh,0);
		 }
     }
     
     for(iRow = 0; iRow < ftFace->glyph->bitmap.rows; iRow++)
     {
       	 for(j = 0; j< leftBlank; j++)
       	 {
			setBufferVal(j,topBlank+iRow,iCharWidth,iCharHeigh,0);
	 }
         for(iCol = 0; iCol < ftFace->glyph->bitmap.width; iCol++)
         { 
              if((ftFace->glyph->bitmap.buffer[iRow * ftFace->glyph->bitmap.pitch + iCol/8] & (0xC0 >> (iCol % 8))) == 0)
              {
                        setBufferVal(leftBlank+iCol,topBlank+iRow,iCharWidth,iCharHeigh,0);
              }
			else
			{  
			    setBufferVal(leftBlank+iCol,topBlank+iRow,iCharWidth,iCharHeigh,1);
			}
         }
         for(j = 0; j< rightBlank; j++)
         {
			setBufferVal(leftBlank+ftFace->glyph->bitmap.width+j,topBlank+iRow,iCharWidth,iCharHeigh,0);
	}
     }
     
     for( i = 0;i<bottomBlank;i++ )
     {
		 for(j = 0; j< iCharWidth; j++)
		 {
			setBufferVal(j,topBlank+ftFace->glyph->bitmap.rows+i,iCharWidth,iCharHeigh,0);
		}
	
     }
     
      FT_Done_Face(ftFace);
      FT_Done_FreeType(ftLibrary);
     
     return 0;
}  

unsigned char *getFontDataToRAM( char *filename )
{
	FILE *file;
	int len;
	unsigned char *font;
	
	file = fopen(filename,"rb");
	if(file == NULL)
	{
		LOG("------------- open error, filename:%s\n", filename);
		return -1;
	}
	fseek(file, 0, SEEK_END);

	len = ftell(file);
	LOG("----font file<%s> len:%d\r\n", filename, len);

	font = malloc( len );
	if( font == NULL )
	{
		LOG("------------ malloc error\r\n");
		return -2;
	}

	fseek(file, 0, SEEK_SET);
	
	len = fread( font, 1, len, file);
	
	LOG("----fread, len:%d\r\n", len);
	
	fclose(file);

	return font;
}

#define FONT_EN_16_24		"/persist/fonts/en16x24.fon"	// "/storage/sdcard0/font/#en16x24.fon"
#define FONT_EN_8_16		"/persist/fonts/en8_16.fon"  	// "/storage/sdcard0/font/#en8_16.fon"
#define FONT_EN_8_8			"/persist/fonts/en8x8.fon"		// "/storage/sdcard0/font/#en8x8.fon"
#define FONT_EN_6_12		"/persist/fonts/eng6_12.bin"	// "/storage/sdcard0/font/eng6_12.bin"
#define FONT_CH_12			"/persist/fonts/gbk12.fon"		// "/storage/sdcard0/font/#gbk12.fon"
#define FONT_CH_16			"/persist/fonts/gbk16.fon"		// "/storage/sdcard0/font/#gbk16.fon"
#define FONT_CH_24			"/persist/fonts/gbk24.fon"		// "/storage/sdcard0/font/#gbk24.fon"


void font_file_open( void )
{
	FILE *file1, *file2, *file3, *file4, *file5, *file6, *file7;

	file1 = fopen(FONT_EN_16_24, "rb");
	if( file1 == NULL )
	{
		printf("fopen(%s) ERROR!!\r\n", FONT_EN_16_24);
	}
	else
	{
		printf("fopen(%s) OK: %p!!\r\n", FONT_EN_16_24, file1);
	}
	file2 = fopen(FONT_EN_8_16, "rb");
	if( file2 == NULL )
	{
		printf("fopen(%s) ERROR!!\r\n", FONT_EN_8_16);
	}
	else
	{
		printf("fopen(%s) OK: %p!!\r\n", FONT_EN_8_16, file2);
	}
	file3 = fopen(FONT_EN_8_8, "rb");
	if( file3 == NULL )
	{
		printf("fopen(%s) ERROR!!\r\n", FONT_EN_8_8);
	}
	else
	{
		printf("fopen(%s) OK: %p!!\r\n", FONT_EN_8_8, file3);
	}
	file4 = fopen(FONT_EN_6_12, "rb");
	if( file4 == NULL )
	{
		printf("fopen(%s) ERROR!!\r\n", FONT_EN_6_12);
	}
	else
	{
		printf("fopen(%s) OK: %p!!\r\n", FONT_EN_6_12, file4);
	}
	file5 = fopen(FONT_CH_12, "rb");
	if( file5 == NULL )
	{
		printf("fopen(%s) ERROR!!\r\n", FONT_CH_12);
	}
	else
	{
		printf("fopen(%s) OK: %p!!\r\n", FONT_CH_12, file5);
	}
	file6 = fopen(FONT_CH_16, "rb");
	if( file6 == NULL )
	{
		printf("fopen(%s) ERROR!!\r\n", FONT_CH_16);
	}
	else
	{
		printf("fopen(%s) OK: %p!!\r\n", FONT_CH_16, file6);
	}
	file7 = fopen(FONT_CH_24, "rb");
	if( file7 == NULL )
	{
		printf("fopen(%s) ERROR!!\r\n", FONT_CH_24);
	}
	else
	{
		printf("fopen(%s) OK: %p!!\r\n", FONT_CH_24, file7);
	}
}


int getInfoFromConfig( void )
{
	char conf[30][128];

	confLen = 7;

	fontType[0] = 1;
	mode[0] = 2;
	strcpy(&fontFileName[0], FONT_EN_8_16);
	g_charwidth[0] = 8;
	g_charheigh[0] = 16;
	offsetValue1[0] = 0;
	offsetValue2[0] = 0;
	offsetMode[0] = 0;
	fontValue[0] = 1;

	fontType[1] = 1;
	mode[1] = 2;
	strcpy(&fontFileName[1], FONT_CH_16);
	g_charwidth[1] = 16;
	g_charheigh[1] = 16;
	offsetValue1[1] = 64;
	offsetValue2[1] = 16;
	offsetMode[1] = 2;
	fontValue[1] = 2;

	fontType[2] = 1;
	mode[2] = 2;
	strcpy(&fontFileName[2], FONT_EN_16_24);
	g_charwidth[2] = 16;
	g_charheigh[2] = 24;
	offsetValue1[2] = 0;
	offsetValue2[2] = 0;
	offsetMode[2] = 0;
	fontValue[2] = 3;

	fontType[3] = 1;
	mode[3] = 2;
	strcpy(&fontFileName[3], FONT_CH_24);
	g_charwidth[3] = 24;
	g_charheigh[3] = 24;
	offsetValue1[3] = 64;
	offsetValue2[3] = 1552;
	offsetMode[3] = 2;
	fontValue[3] = 4;

	fontType[4] = 1;
	mode[4] = 2;
	strcpy(&fontFileName[4], FONT_EN_6_12);
	g_charwidth[4] = 6;
	g_charheigh[4] = 12;
	offsetValue1[4] = 32;
	offsetValue2[4] = 0;
	offsetMode[4] = 0;
	fontValue[4] = 5;

	fontType[5] = 1;
	mode[5] = 2;
	strcpy(&fontFileName[5], FONT_CH_12);
	g_charwidth[5] = 12;
	g_charheigh[5] = 12;
	offsetValue1[5] = 0;
	offsetValue2[5] = 1552;
	offsetMode[5] = 2;
	fontValue[5] = 6;

	fontType[6] = 1;
	mode[6] = 2;
	strcpy(&fontFileName[6], FONT_EN_8_8);
	g_charwidth[6] = 8;
	g_charheigh[6] = 8;
	offsetValue1[6] = 32;
	offsetValue2[6] = 0;
	offsetMode[6] = 0;
	fontValue[6] = 7;

	
#if 0
	confLen = getTable(conf);
			
	int i;
	for( i=0; i<confLen; i++)
	{
		#if 1
		getConfValue("/storage/sdcard0/test/print_font.conf",conf[i],"num",&(fontValue[i]),TYPE_INT);
		getConfValue("/storage/sdcard0/test/print_font.conf",conf[i],"type",&(fontType[i]),TYPE_INT);
		getConfValue("/storage/sdcard0/test/print_font.conf",conf[i],"font_path",fontFileName[i],TYPE_STRING);
		getConfValue("/storage/sdcard0/test/print_font.conf",conf[i],"width",&(g_charwidth[i]),TYPE_INT);
		getConfValue("/storage/sdcard0/test/print_font.conf",conf[i],"heigh",&(g_charheigh[i]),TYPE_INT);
		getConfValue("/storage/sdcard0/test/print_font.conf",conf[i],"offset1",&(offsetValue1[i]),TYPE_INT);
		getConfValue("/storage/sdcard0/test/print_font.conf",conf[i],"offset2",&(offsetValue2[i]),TYPE_INT);
		getConfValue("/storage/sdcard0/test/print_font.conf",conf[i],"offsetMode",&(offsetMode[i]),TYPE_INT);
		getConfValue("/storage/sdcard0/test/print_font.conf",conf[i],"mode",&(mode[i]),TYPE_INT);

		//pFontFile[i] = getFontDataToRAM( fontFileName[i] );
		
		#else

		getConfValue("./print_font.conf",conf[i],"num",&(fontValue[i]),TYPE_INT);
		getConfValue("./print_font.conf",conf[i],"type",&(fontType[i]),TYPE_INT);
		getConfValue("./print_font.conf",conf[i],"font_path",fontFileName[i],TYPE_STRING);
		getConfValue("./print_font.conf",conf[i],"width",&(g_charwidth[i]),TYPE_INT);
		getConfValue("./print_font.conf",conf[i],"heigh",&(g_charheigh[i]),TYPE_INT);
		getConfValue("./print_font.conf",conf[i],"offset1",&(offsetValue1[i]),TYPE_INT);
		getConfValue("./print_font.conf",conf[i],"offset2",&(offsetValue2[i]),TYPE_INT);
		getConfValue("./print_font.conf",conf[i],"offsetMode",&(offsetMode[i]),TYPE_INT);
		getConfValue("./print_font.conf",conf[i],"mode",&(mode[i]),TYPE_INT);
		#endif
		LOG("------------ font idx: %d--------------- \r\n", i);
		LOG("%d\n",fontValue[i]);
		LOG("%d\n",fontType[i]);
		LOG("%s\n",fontFileName[i]);
		LOG("%d\n",g_charwidth[i]);
		LOG("%d\n",g_charheigh[i]);
		LOG("%d\n",offsetValue1[i]);
		LOG("%d\n",offsetValue2[i]);
		LOG("%d\n",offsetMode[i]);
		LOG("%d\n",mode[i]);
		LOG("\n");
	}
#endif
	return 0;
}
int getGBKData( char *s)
{
	FILE *file;
	int readSize = 0;
	int offset;
	unsigned char h = s[0];
	unsigned char l = s[1];
	
	readSize = (g_charwidth[useValue] + 7) / 8 *g_charheigh[useValue];
	if(h >= 0x81 && l >= 0x40)
	{
		if(offsetMode[useValue] == 1)
			offset =   ((h - 0xa1) * 94 + l - 0xa1 + offsetValue1[useValue])*readSize + offsetValue2[useValue];
		else
			offset = ((h-0x81)*191 + (l-0x40) + offsetValue1[useValue])*readSize + offsetValue2[useValue];
	}
	else if (h >= 0xA1 && l >= 0xA0)
		offset = ((h-0xa1-15)*94 + (l-0xa1) + offsetValue1[useValue])*readSize + offsetValue2[useValue];
	else
		offset = 0;
	#if 1	
	file = fopen(fontFileName[useValue],"rb");
	if(file == NULL)
	{
		printf("open error\n");
		return -1;
	}
	fseek(file,offset,SEEK_SET);
	
	fread( buffer, 1, readSize, file);
	fclose(file);
	#else
	if( pFontFile[useValue] != NULL )
		memcpy(buffer, pFontFile[useValue]+offset, readSize);
	else
		memset(buffer, 0xFF, readSize);
	#endif
	
	return 0;
	
}
char isChinese(char *s)
{
    unsigned char us[3];
    memcpy(us, s, 2);

	if(us[0] >= 0x81 && us[1] >= 0x40)
	{
		return 1;
	}
	else if (us[0] >= 0xA1 && us[1] > 0xA0)
	{
		return 1;
	}
    return 0;
}
int getGBKbuffer(char *s)
{
	FILE *file;
	int readSize = 0;
	int offset;
	unsigned char h = s[0];
	unsigned char l = s[1];
	
	readSize = (g_charwidth[useValue] + 7) / 8 *g_charheigh[useValue];
	if(h >= 0x81 && l >= 0x40)
	{
		if(offsetMode[useValue] == 1)
			offset =   ((h - 0xa1) * 94 + l - 0xa1 + offsetValue1[useValue])*readSize + offsetValue2[useValue];
		else
		{
			offset = ((h-0x81)*191 + (l-0x40) + offsetValue1[useValue])*readSize + offsetValue2[useValue];
		}
	}
	else if (h >= 0xA1 && l >= 0xA0)
		offset = ((h-0xa1-15)*94 + (l-0xa1) + offsetValue1[useValue])*readSize + offsetValue2[useValue];
	else
		offset = 0;
	
	#if 1	
	if(getBuffer == NULL)
	{
		
		file = fopen(fontFileName[useValue],"rb");
		int len;
		if(file == NULL)
		{
			LOG("useValue:%d,open[%s] error---%s", useValue, fontFileName[useValue], strerror(errno));
			return -1;
		}
		fseek(file,0,SEEK_END);
		
		len = ftell(file);
		rewind(file);

		
		getBuffer = (unsigned char *)malloc(len);
		fread(getBuffer,1,len,file);
		fclose(file);
	}
	memcpy(buffer,(getBuffer+offset),readSize);
	#else
	
	if( pFontFile[useValue] != NULL )
		memcpy(buffer, pFontFile[useValue]+offset, readSize);
	else
		memset(buffer, 0xFF, readSize);
	#endif
	
	return 0;
}
int getEngData(char *s)
{
	return 0 ;
}
int getEngbuffer(char *s)
{
	FILE *file;
	int readSize = 0;
	int offset;
	int i,j;

	readSize = (g_charwidth[useValue] + 7) / 8 *g_charheigh[useValue];
//	printf("%d  %d\n",g_charwidth[useValue],g_charheigh[useValue]);
//	printf("%s\n",fontFileName[useValue]);
	offset = (*s - offsetValue1[useValue])*readSize+offsetValue2[useValue];
	
//	printf("offset  %d   %d\n",offset,(8+7)/8);
	if(getEngBuffer == NULL)
	{
		
		file = fopen(fontFileName[useValue],"rb");
		int len;
		if(file == NULL)
		{
			LOG("open[%s] error----%s\n",fontFileName[useValue], strerror(errno));
			return -1;
		}
		fseek(file,0,SEEK_END);
		
		len = ftell(file);
		rewind(file);

		
		getEngBuffer = (unsigned char *)malloc(len);
		fread(getEngBuffer,1,len,file);
		fclose(file);
	}
	memcpy(buffer,(getEngBuffer+offset),readSize);
	if(g_charwidth[useValue] ==16 && g_charheigh[useValue] == 24 && *s == 'A')
	{
		for(i=0;i<46;i++)
		{
			unsigned char Temp;
	  
			Temp = buffer[i];
			buffer[i] = buffer[i+1];
			buffer[i+1] = Temp;	
		}
	}
	else
	{
		for(j=0;j<(g_charwidth[useValue] + 7) / 8 *g_charheigh[useValue];j++)
		{
			unsigned char c=0;
			//printf("%x  ",c);
			for(i=0;i<8;i++)
			{
				unsigned char b = (buffer[j]&(0x80>>i))>>(7-i);

				c |= b<<i;
			}
			buffer[j] = c;
		}
	}
	
	return 0;
}
void freeBuf(int fontIndex)
{

	if((fontValue[useValue] != fontIndex) && (getBuffer!=NULL))
	{	
		free(getBuffer);
		getBuffer = NULL;
	}
	if((fontValue[useValue] != fontIndex) && (getEngBuffer!=NULL))
	{	
		free(getEngBuffer);
		getEngBuffer = NULL;
	}
}
int getHexData(int fontIndex, const char* pChar, unsigned char** data, int* lengthOfData )
 {
	assert( pChar );	
	char *newChar = (char *)pChar;
	unsigned char ucBuffer2[ 56 ];
	int i;
	static int flag = 0;
	if(flag == 0)
	{
		if(getInfoFromConfig() < -1)
			return -1;
		flag = 1;
	}

	//printf("fontIndex: %d\r\n", fontIndex);
	
	freeBuf(fontIndex);
	for( i=0; i<confLen;i++ )
	{
		if(fontValue[i] == fontIndex)
		{
			useValue = i;
			break;
		}
	}
	
	//printf("------------------ fontIndex: %d, useValue:%d\r\n", fontIndex, useValue);
	
	bzero(buffer,sizeof(buffer));
	// LOG("fontType[%d]:%d", useValue, fontType[useValue]);

	if(fontType[useValue] == 1)
	{
		if(isChinese(newChar))
		{
			// LOG("isChinese,mode[%d]:%d",mode[useValue]);

			if(mode[useValue] == 1)
				getGBKData(newChar);
			else if(mode[useValue] == 2)
				getGBKbuffer(newChar);
			int j,k;
		
			if(g_charwidth[useValue] ==16 && g_charheigh[useValue] == 16)
			{
				memset( ucBuffer2, 0, sizeof( ucBuffer2 ) );
				memcpy( ucBuffer2, buffer + 8, 8);
				memcpy( buffer + 8, buffer + 16, 8);
				memcpy( buffer + 16, ucBuffer2, 8);
				memset( ucBuffer2, 0, sizeof(ucBuffer2));

				for (k = 0; k < 32; k += 8)
				{
					for (i = 0; i < 8; i++)
					{
						for (j = 0; j < 8; j++)
						{
							ucBuffer2[i+k] |= ((buffer[j+k] >> i) & 0x01) << (7 - j);
						
						}
					}
				}
				// ----------------------------------------------
				// 横向点阵转换
				k = 0;
				for( i = 0; i < 2; i ++ )
				{
					for( j = 0; j < 8; j ++ )
					{
						buffer[ k++ ] = ucBuffer2[ j + i*16];
						buffer[ k++ ] = ucBuffer2[ j + 8 + i*16 ];
					}
				}
				
			}
		
			for(j=0;j<(g_charwidth[useValue] + 7) / 8 *g_charheigh[useValue];j++)
			{
				unsigned char c=0;
				//printf("%x  ",c);
				for(i=0;i<8;i++)
				{
					unsigned char b = (buffer[j]&(0x80>>i))>>(7-i);

					c |= b<<i;
				}
				buffer[j] = c;
			}
		}
		else
		{
			// LOG("mode[%d]:%d", useValue, mode[useValue]);

			if(mode[useValue] == 1)
				getEngData(newChar);
			else if(mode[useValue] == 2)
				getEngbuffer(newChar);	
		}				
	}
	else if(fontType[useValue] == 2)
	{
		unsigned char pnew[OUTLEN]="\0";
		code_convert("GBK", "UCS-2", (char* )newChar, (int)strlen( pChar ), (char *)pnew, (int)OUTLEN);
		//code_convert("UTF-8", "UCS-2", (char* )newChar, (int)strlen( pChar ), (char *)pnew, (int)OUTLEN);
		char tmp = pnew[0];
		pnew[0] = pnew[1];
		pnew[1] = tmp;

		unsigned int uiCharCode = *((unsigned int*)pnew);
		GetCharBitmap(g_charwidth[useValue],g_charheigh[useValue], uiCharCode);
	}
	*lengthOfData = (g_charwidth[useValue] + 7) / 8 *g_charheigh[useValue];

	*data = buffer;
	
	return 0;
}

 
int showBufferValInTerminal(  unsigned char *data)
{
    int i = 0;
	int j = 0;
	
	int posByte = 0;
	int posInByte = 0;
	
	for( j = 0;j< g_charheigh[useValue];j++)
	{
		for( i = 0;i< g_charwidth[useValue];i++ )
		{
			posByte = (j*g_charwidth[useValue]+i)/8;
			posInByte =  (j*g_charwidth[useValue]+i)%8;
			
			if(  ( data[posByte]  &  (1<< posInByte) ) == 0  )
			{
				printf(" ");
			}
			else
			{
				printf("*");
			}
		}
		printf("\n");
	}
	
	return 0;
}

int getconfLen( void )
{
	return confLen;
}

char *getfontFileName( int i )
{
	return (char *)&fontFileName[i];
}

