#include <stdio.h>
#include <stdlib.h>

typedef enum _tyep
{
  TYPE_INT = 0,
  TYPE_STRING
} CAST_TYPE;


int getConfValue (char *confFile, char *section_name, char *conf_name,void *value, CAST_TYPE type);

int getTable(char table[30][128]);
