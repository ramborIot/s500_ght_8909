
#include <stdio.h>
#include <stdlib.h>

#include "prnt_fck.h"
#include "test_fck.h"
#include "devapi.h"


#define uart_printk		printf
#define xxx_exit_prompt	printf
#define uart_print		printf
#define xxx_getkey		getchar

#define UART_MENU_LINES_PER_PAGE		(9)


int xxx_input_int(char *title, int *val)
{
	int i, j, iRet;
	int min_len = 1;
	int max_len = 9;
	unsigned char tmp[20];
	int ch;

	if(title == NULL)
		uart_print("\r\nxxx_input_int[key:0~9]:");
	else
		uart_print("\r\n%s", title);

	//flush(stdin);
	//PortReset(XXX_TEST_PRINT_PORT);
	memset(tmp, 0x00, sizeof(tmp));
	i = 0;
	do {
		//iRet = PortRecv(XXX_TEST_PRINT_PORT, &ch, 0);
		ch = getchar();
		//xxx_printf("xxxxxxxx PortRecv: iRet:%d, ch:%02x\r\n", iRet, ch);
		//if(iRet != 0)
		{
			if(i < max_len && ch >= '0' && ch <= '9')
			{
				tmp[i++] = (unsigned char)ch;
				//putchar(ch);
			}
			if(ch == 'z')
			{
				uart_print("\r\n");
				return -1;
			}
			if(ch == 'x')
				break;
		}
	} while(ch != '\r');
	
	if(i == 0)
	{
		uart_print("\r\n");
		return -1;
	}

	*val = atoi(tmp);

	uart_print("\r\n");
	return 0;
}


void uart_MenuSelect(struct menu_opt *menu, int menu_size)
{
	unsigned int  lines_per_page = UART_MENU_LINES_PER_PAGE;
	unsigned char key_down 	  = 'j';
	unsigned char key_up      = 'k';
	unsigned char key_exit 	  = 'z';

	int i;
	unsigned char ucKey;
	int cur_page = 0;
	int all_lines;
	int all_pages;
	//int lines_per_page;
	int lines_of_cur_page;

	all_lines		= menu_size - 1;
	//lines_per_page = all_lines;
	all_pages		= (all_lines + lines_per_page - 1 ) / lines_per_page; /* count from 1 */


	if(all_pages <= 0)
		return ;
	while(1)
	{
		if(cur_page == all_pages - 1 && all_lines % lines_per_page != 0) /* the last page */
			lines_of_cur_page = (all_lines) % lines_per_page;
		else
			lines_of_cur_page = lines_per_page;

        if(ucKey)
        {

		//ScrCls();
		//uart_ScrPrint(0, 0, 0x00, "%s", menu[0].str);
		//uart_ScrPrint(0, 0, 0x00, "[%d/%d]%s", cur_page+1, all_pages, menu[0].str);
		//uart_print("\r\n");
		uart_print("[%d/%d]********** %s ***********\r\n", cur_page+1, all_pages, menu[0].str);
		//xxx_ScrCls();
		//xxx_ScrPrint(0, 0, 0x00, "[%d/%d]%s", cur_page+1, all_pages, menu[0].str);
		for(i = 1; i <= lines_of_cur_page; i++)
		{
			//uart_ScrPrint(0, i, 0x00, "%d.%s", i, menu[cur_page*lines_per_page+i].str);
			uart_print("%d.%s\r\n", i, menu[cur_page*lines_per_page+i].str);
			//xxx_ScrPrint(0, i, 0x00, "%d.%s\r\n", i, menu[cur_page*lines_per_page+i].str);
		}

        }
        
		ucKey = xxx_getkey();
		if (ucKey == key_down)
		{
			if(++cur_page >= all_pages)
				cur_page = 0;
		}
		else if (ucKey == key_up)
		{
			if(--cur_page < 0)
				cur_page = all_pages-1;
		}
		else if (ucKey == key_exit)
		{
			return ;
		}
		else if (ucKey >= '1' && ucKey <= '0' + lines_of_cur_page)
		{
			if(menu[cur_page * lines_per_page + ucKey - '0'].pFun != NULL)
			{
				//ScrCls();
				uart_print("\r\n");
				//xxx_ScrCls();
				menu[cur_page * lines_per_page + ucKey - '0'].pFun();
			}
		}
	}
}

void uart_print_hex(char *title, unsigned char *hex, int n)
{
	int i;
	
	uart_print("%s", title);
	for(i = 0; i < n; i++)
	{
		if(i % 16 == 0 && i != 0)
			uart_print("\r\n");
		uart_print("%02x ", (unsigned char )hex[i]);
	}
	uart_print("\r\n");
}
// -----------------------------------------------------------------
// TODO: 1 打印机
static int fck_cnt = 0;

void fck_delay( int ms )
{
	volatile int i, j;

	for(i = 0; i < ms; i++ )
		for(j = 0; j < 3245; j++)
			;
}


// -----------------------------------------------------------------

void spi_ddi_printer_init_test( void )
{
	int ret;

	ret = spi_ddi_printer_init( 100 );

	printf("spi_ddi_printer_init, ret: %d\n", ret);
}

void spi_ddi_printer_start_test( void )
{
	int ret;

	ret = spi_ddi_printer_start( 100 );

	printf("spi_ddi_printer_start, ret: %d\n", ret);
}

void spi_ddi_printer_fill_test( void )
{
	int ret;
	unsigned char data[48*10];
	int len;

	memset(data, 0x33, sizeof(data));
	len = sizeof(data);

	ret = spi_ddi_printer_fill( data, len );

	printf("spi_ddi_printer_fill, ret: %d\n", ret);
}

void spi_ddi_printer_get_status_test( void )
{
	int ret;

	ret = spi_ddi_printer_get_status();

	printf("spi_ddi_printer_get_status, ret: %d\n", ret);
}

void spi_ddi_printer_set_fill_finish_test( void )
{
	int ret;

	ret = spi_ddi_printer_set_fill_finish();

	printf("spi_ddi_printer_set_fill_finish, ret: %d\n", ret);
}

void spi_ddi_printer_clear_fill_finish_test( void )
{
	int ret;

	ret = spi_ddi_printer_clear_fill_finish();

	printf("spi_ddi_printer_clear_fill_finish, ret: %d\n", ret);
}

void spi_ddi_printer_stop_test( void )
{
	int ret;

	ret = spi_ddi_printer_stop();

	printf("spi_ddi_printer_stop, ret: %d\n", ret);
}

void spi_ddi_printer_feed_test( void )
{
	int ret;

	ret = spi_ddi_printer_feed( 200 );

	printf("spi_ddi_printer_feed, ret: %d\n", ret);
}

void spi_ddi_printer_check_queue_test( void )
{
	int ret;

	ret = spi_ddi_printer_check_queue();

	printf("spi_ddi_printer_check_queue, ret: %d\n", ret);
}


// -----------------------------------------------------------------

void fck_prnt_terminal_info( void )
{
	int ret;

	// 支持的命令
	unsigned char cmd_double_height[] = {0x1b, 'h', 1, ';'};
	unsigned char cmd_normal_height[] = {0x1b, 'h', 0, ';'};
	unsigned char cmd_double_width[]  = {0x1b, 'w', 1, ';'};
	unsigned char cmd_normal_width[]  = {0x1b, 'w', 0, ';'};
	unsigned char cmd_set_left_margin[] = {0x1b, 'E', 110, ';'};
	unsigned char cmd_normal_left_margin[] = {0x1b, 'E', 0, ';'};
	unsigned char cmd_set_right_margin[] = {0x1b, 'e', 110, ';'};
	unsigned char cmd_normal_right_margin[] = {0x1b, 'e', 0, ';'};	
    unsigned char cmd_set_en_font[] = {0x1b, 'l', 3, ';'};
    unsigned char cmd_set_ch_font[] = {0x1b, 'L', 4, ';'};
	
	char *bmpFilePath = "/storage/sdcard0/test/C_190.bmp";
	
	char *name = "S500";
	char SN[32], ExSN[32];
	char pci_ver[32];
	char sp_boot[32], sp_firmware[32];
	char ap_boot[32], ap_firmware[32];
	int i, cnt;
	
	strcpy(SN, "7733401");
	strcpy(ExSN, "0123456789012345678922");
	strcpy(pci_ver, "V3.1");
	strcpy(sp_boot, "V1.0.0");
	strcpy(sp_firmware, "V1.1.1");
	strcpy(ap_boot, "V1.0.0");
	strcpy(ap_firmware, "V1.1.1");
	
	ret = fck_prnt_init( 0 );

	for( i = 0; i < 9; i++ )
	{
		fck_prnt_printf("************************\n");
		ret = fck_prnt_command( cmd_double_height, sizeof(cmd_double_height));
		fck_prnt_printf("****** 终 端  信 息 ******\n");
		ret = fck_prnt_command( cmd_normal_height, sizeof(cmd_normal_height));
		fck_prnt_printf("************************\n");

		ret = fck_prnt_command( cmd_double_height, sizeof(cmd_double_height));
		fck_prnt_printf("终端名称:%s\n", name);
		ret = fck_prnt_command( cmd_normal_height, sizeof(cmd_normal_height));

		fck_prnt_printf("SN     : %s\n", SN);
		fck_prnt_printf("ExSN   : %s\n", ExSN);
		fck_prnt_printf("PCI_VER: %s\n", pci_ver);

		ret = fck_prnt_command( cmd_double_height, sizeof(cmd_double_height));
		fck_prnt_printf("SP固件信息:\n");
		ret = fck_prnt_command( cmd_normal_height, sizeof(cmd_normal_height));

		fck_prnt_printf("BOOT    : %s\n", sp_boot);
		fck_prnt_printf("FIRMWARE: %s\n", sp_firmware);

		ret = fck_prnt_command( cmd_double_height, sizeof(cmd_double_height));
		fck_prnt_printf("AP固件信息:\n");
		ret = fck_prnt_command( cmd_normal_height, sizeof(cmd_normal_height));

		fck_prnt_printf("ap_boot    : %s\n", ap_boot);
		fck_prnt_printf("ap_firmware: %s\n", ap_firmware);

		ret = fck_prnt_command( cmd_double_height, sizeof(cmd_double_height));
		fck_prnt_printf("硬件信息:\n");
		ret = fck_prnt_command( cmd_normal_height, sizeof(cmd_normal_height));

		fck_prnt_printf("CPU : %s\n", "MAXIM3255x");
		fck_prnt_printf("MAIN: %s\n", "V01-SCH-20161101");
		fck_prnt_printf("RF  : [%c]\n", 'Y');
		fck_prnt_printf("ICC : [%c]\n", 'Y');
		fck_prnt_printf("PSAM: [%c]\n", '2');
		fck_prnt_printf("SIM : [%c]\n", '1');
		fck_prnt_printf("LAN : [%c]\n", 'Y');
		fck_prnt_printf("WIFI: [%c]\n", 'Y');
		fck_prnt_printf("BC  : [%c]\n", 'Y');
		//fck_prnt_printf("FLASH: %s\n", "1MB");
		ret = fck_prnt_command( cmd_double_height, sizeof(cmd_normal_height));
		fck_prnt_printf("字库信息:\n");
		ret = fck_prnt_command( cmd_normal_height, sizeof(cmd_normal_height));
#if 0	
		for( i = 0; i < getconfLen(); i++ )
		{
			fck_prnt_printf("%s\n", getfontFileName(i)+22);
			//printf("%s\n", getfontFileName(i));
		}
#endif
		ret = printer_graphics( 0, 40, bmpFilePath );
		//printf("printer_graphics, ret: %d\n", ret);
		
		fck_prnt_printf("01234567890\n012345678901234567890123\n01234\n");
	}
	
	ret = fck_prnt_start( 0 );	
	if( ret == -2 )
	{
		for(i = 0; i < 10; i++)
			printf("fck_prnt_start, result: %d, fck_cnt:%d\n", ret, fck_cnt);
		while( 1 );
	}
	if( ret == -89994 )
	{
		printf("fck_prnt_start, result: %d, fck_cnt:%d\n", ret, fck_cnt);
		fck_delay( 9245469 );
	}
}


void fck_prnt_terminal_loop_test( void )
{
	fck_cnt = 0;
	while( 1 )
	{
		fck_cnt++;
		fck_prnt_terminal_info();
		fck_delay( 999999 );
	}
}


void fck_prnt_test( void )
{
	int ret;
	int state;
	char en[] = "打发打发打发打发第三方中文:四四\nEnglish:XX\n";

	int i, j, k;

	unsigned char cmd_double_height[] = {0x1b, 'h', 1, ';'};
	unsigned char cmd_normal_height[] = {0x1b, 'h', 0, ';'};
	unsigned char cmd_double_width[]  = {0x1b, 'w', 1, ';'};
	unsigned char cmd_normal_width[]  = {0x1b, 'w', 0, ';'};
	unsigned char cmd_set_left_margin[] = {0x1b, 'E', 110, ';'};
	unsigned char cmd_normal_left_margin[] = {0x1b, 'E', 0, ';'};
	unsigned char cmd_set_right_margin[] = {0x1b, 'e', 110, ';'};
	unsigned char cmd_normal_right_margin[] = {0x1b, 'e', 0, ';'};	
    unsigned char cmd_set_en_font[] = {0x1b, 'l', 3, ';'};
    unsigned char cmd_set_ch_font[] = {0x1b, 'L', 4, ';'};
	
	ret = fck_prnt_init( 10 );

	for( k = 0; k < 1; k++ )
	{
	for( i = 1; i <= 3; i+=2 )	// 英文字体选择
	{
		for( j = 2; j <= 6; j+=2 )	// 中文字体选择
		{
			// 选择字体
			cmd_set_en_font[2] = i;
			cmd_set_ch_font[2] = j;
			ret = fck_prnt_command( cmd_set_en_font, sizeof(cmd_set_en_font));
			ret = fck_prnt_command( cmd_set_ch_font, sizeof(cmd_set_ch_font));
			
			ret = fck_prnt_printf("- 字体(En:%d,CH:%d)\n", i, j);
			
			// 正常
			ret = fck_prnt_printf("%s", en);
			// 倍高
			ret = fck_prnt_command( cmd_double_height, sizeof(cmd_double_height));
			ret = fck_prnt_printf("%s", en);
			ret = fck_prnt_command( cmd_normal_height, sizeof(cmd_normal_height));
			// 倍宽
			ret = fck_prnt_command( cmd_double_width, sizeof(cmd_double_width));
			ret = fck_prnt_printf("%s", en);
			// 倍宽倍高
			ret = fck_prnt_command( cmd_double_width, sizeof(cmd_double_width));
			ret = fck_prnt_command( cmd_double_height, sizeof(cmd_double_height));
			ret = fck_prnt_printf("%s", en);

			ret = fck_prnt_command( cmd_normal_height, sizeof(cmd_normal_height));
			ret = fck_prnt_command( cmd_normal_width, sizeof(cmd_normal_width));
			// 左右边界
			ret = fck_prnt_command( cmd_set_left_margin, sizeof(cmd_set_left_margin));
			ret = fck_prnt_command( cmd_set_right_margin, sizeof(cmd_set_right_margin));
			ret = fck_prnt_printf("%s", en);
			
			ret = fck_prnt_command( cmd_normal_left_margin, sizeof(cmd_normal_left_margin));
			ret = fck_prnt_command( cmd_normal_right_margin, sizeof(cmd_normal_right_margin));
		}
	}
	}
	//ret = fck_prnt_command( cmd_double_width, sizeof(cmd_double_width));
	//printf("fck_prnt_command: cmd_double_width, ret:%d\r\n", ret);
	//ret = fck_prnt_printf("%s", en);

	//fck_prnt_printf("%s", "--");

	
	//ret = printer_printf( 10, &state, "%s", en );
	//ret = printer_printf( 10, &state, "%s", ch );
	//printer_printf( 10, &state, "%s", "                                  " );
	//printf("printer_printf, ret:%d, state:%d\r\n", ret, state);

	//printer_printf_fck();
	
	ret = fck_prnt_start( 100 );	
	
}

void fck_prnt_data_test( void )
{
	fck_prnt_init( 0 );
	fck_prnt_printf("123456789\n");
	fck_prnt_printf("\n\n\n\n");
	fck_prnt_printf("123456789\n");
	fck_prnt_start( 40 );
}

void fck_prnt_matrix_test( void )
{
	unsigned char data[48*1024];
	int ret;
	int i;
	
	memset(data, 0xFF, sizeof(data));
	
	fck_prnt_init( 40 );
	for( i = 1; i <= 16; i++ )
	{
		fck_prnt_matrix( 0, 384, i*8, data, 384*i);
		fck_prnt_start( 40 );
		fck_delay( 50000 );
	}
}

void printer_sp_api_test( void )
{
	ST_MENU_OPT tMenuOpt[] = {
		" printer_sp_api_test ", NULL,
		"spi_ddi_printer_init_test", spi_ddi_printer_init_test,
		"spi_ddi_printer_start_test", spi_ddi_printer_start_test,
		"spi_ddi_printer_fill_test",spi_ddi_printer_fill_test,
		"spi_ddi_printer_get_status_test", spi_ddi_printer_get_status_test,
		"spi_ddi_printer_set_fill_finish_test", spi_ddi_printer_set_fill_finish_test,
		"spi_ddi_printer_clear_fill_finish_test", spi_ddi_printer_clear_fill_finish_test,
		"spi_ddi_printer_stop_test", spi_ddi_printer_stop_test,
		"spi_ddi_printer_feed_test",spi_ddi_printer_feed_test,
		"spi_ddi_printer_check_queue_test", spi_ddi_printer_check_queue_test,
	};

	uart_MenuSelect(tMenuOpt, sizeof(tMenuOpt)/sizeof(tMenuOpt[0]));
}


void printer_test( void )
{
	ST_MENU_OPT tMenuOpt[] = {
		" fck_test ", NULL,
		"printer_sp_api_test", printer_sp_api_test,
		"fck_prnt_terminal_loop_test", fck_prnt_terminal_loop_test,
		"fck_prnt_terminal_info", fck_prnt_terminal_info,
		"fck_prnt_test",fck_prnt_test,
		"fck_prnt_data_test", fck_prnt_data_test,
		"fck_prnt_matrix_test", fck_prnt_matrix_test,

	};

	uart_MenuSelect(tMenuOpt, sizeof(tMenuOpt)/sizeof(tMenuOpt[0]));
}

// TODO: 2 ICC & PSAM

#define ddi_iccpsam_open				spi_ddi_iccpsam_open
#define ddi_iccpsam_close				spi_ddi_iccpsam_close
#define ddi_iccpsam_poweroff			spi_ddi_iccpsam_poweroff
#define ddi_iccpsam_poweron				spi_ddi_iccpsam_poweron
#define ddi_iccpsam_get_status			spi_ddi_iccpsam_get_status
#define ddi_iccpsam_exchange_apdu		spi_ddi_iccpsam_exchange_apdu

static int test_cur_slot = 0;
unsigned char selectFile[] = {
    0x00, 0xA4, 0x04, 0x00,         /*select df file */
    14,                             /* payload length (Lc byte) */
    '1', 'P', 'A', 'Y', '.', 'S', 'Y', 'S', '.', 'D', 'D', 'F', '0', '1',
    0x00,                           /*answer length (Le byte) */
};


void sc_select_test_slot( void )
{

}

void ddi_iccpsam_open_test(void)
{
	int ret;

	ret = ddi_iccpsam_open(test_cur_slot);
	
	xxx_exit_prompt("ddi_iccpsam_open, ret: %d\r\n", ret);
}

void ddi_iccpsam_close_test(void)
{
	int ret;
	ret = ddi_iccpsam_close(test_cur_slot);
	xxx_exit_prompt("ddi_iccpsam_close, ret: %d\r\n", ret);
}

void ddi_iccpsam_poweroff_test(void)
{
	int ret;

	ret = ddi_iccpsam_poweroff(test_cur_slot);
	xxx_exit_prompt("ddi_iccpsam_poweroff, ret: %d\r\n", ret);
}

void ddi_iccpsam_poweron_test(void)
{
	int ret;
	unsigned char atr[64];

	ret = ddi_iccpsam_poweron(test_cur_slot, atr);
	if(ret == 0x00)
	{
		uart_print_hex("ATR::", &atr[1], atr[0]);
	}
	xxx_exit_prompt("ddi_iccpsam_poweron, ret: %d\r\n", ret);
}

void ddi_iccpsam_exchange_apdu_test(void)
{
	int ret;
	unsigned char apdu_cmd[512];
	int apdu_cmd_len;
	unsigned char apdu_rsp[512];
	int apdu_rsp_len = sizeof(apdu_rsp);

	memcpy(apdu_cmd, selectFile, sizeof(selectFile));
	apdu_cmd_len = sizeof(selectFile)/sizeof(selectFile[0]);

	uart_print_hex("apdu_cmd::", apdu_cmd, apdu_cmd_len);
	ret = ddi_iccpsam_exchange_apdu(test_cur_slot, apdu_cmd, apdu_cmd_len,
		apdu_rsp, &apdu_rsp_len, apdu_rsp_len);
	if(ret == 0)
	{
		uart_print_hex("apdu_rsp::\r\n", apdu_rsp, apdu_rsp_len);
	}
	
	xxx_exit_prompt("ddi_iccpsam_exchange_apdu, ret: %d\r\n", ret);
}

void ddi_iccpsam_get_status_test(void)
{
	int ret;

	ret = ddi_iccpsam_get_status(test_cur_slot);
	
	xxx_exit_prompt("ddi_iccpsam_get_status, ret: %d\r\n", ret);
}

void ddi_iccpsam_test(void)
{
	ST_MENU_OPT tMenuOpt[] = {
		" SCAPI_test ", NULL,
		"sc_select_test_slot", sc_select_test_slot,
		"ddi_iccpsam_open_test", ddi_iccpsam_open_test,
		"ddi_iccpsam_close_test", ddi_iccpsam_close_test,
		"ddi_iccpsam_poweroff_test", ddi_iccpsam_poweroff_test,
		"ddi_iccpsam_poweron_test", ddi_iccpsam_poweron_test,
		"ddi_iccpsam_exchange_apdu_test", ddi_iccpsam_exchange_apdu_test,
		"ddi_iccpsam_get_status_test", ddi_iccpsam_get_status_test,
 	};

	uart_MenuSelect(tMenuOpt, sizeof(tMenuOpt)/sizeof(tMenuOpt[0]));
}


// test
#define gmGetVersion	spi_ddi_gm_get_ver
#define gmGetUID		spi_ddi_gm_get_uid
#define gmHSMBist		spi_ddi_gm_hsm_bist
#define gmGetRandom		spi_ddi_gm_get_random
#define gmHashInit		spi_ddi_gm_hash_init
#define gmHashUpdate	spi_ddi_gm_hash_update
#define gmHashFinal		spi_ddi_gm_hash_final
#define gmCipherInit	spi_ddi_gm_cipher_init
#define gmCipherUpdate	spi_ddi_gm_cipher_update
#define gmCipherFinal	spi_ddi_gm_cipher_final
#define gmGenECKey		spi_ddi_gm_gen_ecc_key
#define gmExportECKey	spi_ddi_gm_export_ecc
#define gmImportECKey	spi_ddi_gm_import_ecc
#define gmSM2EncryptInit		spi_ddi_gm_sm2encrypt_init
#define gmSM2EncryptUpdate		spi_ddi_gm_sm2encrypt_update
#define gmSM2EncryptFinal		spi_ddi_gm_sm2encrypt_final
#define gmSM2Encrypt			spi_ddi_gm_sm2encpyt
#define gmSM2Encrypt2			spi_ddi_gm_sm2encpyt2
#define gmSM2DecryptInit		spi_ddi_gm_sm2decrypt_init
#define gmSM2DecryptUpdate		spi_ddi_gm_sm2decrypt_update
#define gmSM2DecryptFinal		spi_ddi_gm_sm2decrypt_final
#define gmSM2Decrypt			spi_ddi_gm_sm2decrypt
#define gmSM2Decrypt2			spi_ddi_gm_sm2decrypt2
#define gmSM2SignHash			spi_ddi_gm_sm2sign_hash
#define gmSM2SignInit			spi_ddi_gm_sm2sign_init
#define gmSM2SignUpdate			spi_ddi_gm_sm2sign_update
#define gmSM2SignFinal			spi_ddi_gm_sm2sign_final
#define gmECDSASignHash			spi_ddi_gm_ecdsa_sign_hash
#define gmSM2VerifyHash			spi_ddi_gm_sm2verify_hash
#define gmSM2VerifyInit			spi_ddi_gm_sm2verify_init
#define gmSM2VerifyUpdate		spi_ddi_gm_sm2verify_update
#define gmSM2VerifyFinal		spi_ddi_gm_sm2verify_final
#define gmECDSAVerifyHash		spi_ddi_gm_ecdsa_verify_hash

void gmGetVersion_test( void )
{
	unsigned char ver[4];
	int ver_len;
	int ret;

	memset( ver, 0xFF, sizeof(ver) );
	ret = gmGetVersion( ver, &ver_len );
	uart_printk("gmGetVersion, ret:%d\r\n", ret);
	if( ret == 0x00 )
	{
		uart_print_hex("gmGetVersion::", ver, ver_len);
	}
}

void gmGetUID_test( void )
{
	int ret;
	unsigned char uid[16];
	int len;

	memset( uid, 0xFF, sizeof(uid));
	
	ret = gmGetUID( uid, &len );

	uart_printk("gmGetUID, ret:%d\r\n", ret);
	uart_print_hex("gmGetUID::\r\n", uid, len);
}

void gmGetUID_loop_test( void )
{
	int ret;
	unsigned char uid[16];
	unsigned char tmp[] = {
		0x75,0x00,0x00,0x00,0x8a,0xff,0xff,0xff,0x04,0x33,0x5f,0x1d,0xfb,0xcc,0xa0,0xe2,
	};
	int len;
	int Err, cnt;
	

	cnt = 0;
	Err = 0;
	while( cnt < 10000 )
	{
		cnt++;
		memset( uid, 0xFF, sizeof(uid));
		ret = gmGetUID( uid, &len );
		if( ret != 0x00 || memcmp(uid, tmp, len))
		{
			Err++;
			uart_printk("Loop Test: cnt=%d, Err=%d\r\n", cnt, Err);
		}
	}
	
	uart_printk("Loop Test END!! cnt=%d, Err=%d\r\n", cnt, Err);
}

void gmHSMBist_test( void )
{
	unsigned char bist[16];
	int ret;
	int len;

	memset( bist, 0xFF, sizeof(bist) );
	ret = gmHSMBist( bist, &len );
	uart_printk("gmHSMBist, ret:%d\r\n", ret);
	uart_print_hex("gmHSMBist::", bist, len);
}

// 
static int test_hash_p2;

void gmHashInit_test( void )
{
	int ret;
	
	if( xxx_input_int("gmHashInit, mode::", &test_hash_p2) < 0x00 || test_hash_p2 > 3 )
	{
		return ;
	}

	ret = gmHashInit( test_hash_p2 );
	uart_printk("gmHashInit(%d), ret:%d\r\n", test_hash_p2, ret);
}

void gmHashUpdate_test( void )
{
	int ret;
	unsigned char data[1024];
	int len;
	int val;
	
	if( xxx_input_int("gmHashUpdate, len::", &len) < 0x00 )
	{
		return ;
	}
	if( xxx_input_int("gmHashUpdate, val::", &val) < 0x00 )
	{
		return ;
	}
	
	memset(data, val, sizeof(data));

	ret = gmHashUpdate(data, len);
	uart_printk("gmHashUpdate, ret:%d\r\n", ret);
	uart_print_hex("gmHashUpdate::", data, len);
}

void gmHashFinal_test( void )
{
	int ret;
	unsigned char hash[64];
	int hash_len;
	
	ret = gmHashFinal(hash, &hash_len);
	uart_printk("gmHashFinal, ret:%d\r\n", ret);
	uart_print_hex("gmHashFinal::", hash, hash_len);
}

// ECC Test
void gmGenECKey_test( void )
{
	int ret;
	int type;
	
	if( xxx_input_int("gmGenECKey, type::", &type) < 0x00 )
	{
		return ;
	}
	
	ret = gmGenECKey( type );
	uart_printk("gmGenECKey, ret:%d\r\n", ret);
}

void gmExportECKey_test( void )
{
	int ret;
	unsigned char d[32];
	unsigned char Px[32];
	unsigned char Py[32];
	
	ret = gmExportECKey(d, Px, Py);
	uart_printk("gmExportECKey, ret:%d\r\n", ret);
	uart_print_hex("gmExportECKey, d::", d, 0x20);
	uart_print_hex("gmExportECKey, Px::", Px, 0x20);
	uart_print_hex("gmExportECKey, Py::", Py, 0x20);
}

void gmImportECKey_test( void )
{
	int ret;
	unsigned char d[32];
	unsigned char Px[32];
	unsigned char Py[32];

	memset(d, 0x01, sizeof(d));
	memset(Px, 0x02, sizeof(Px));
	memset(Py, 0x02, sizeof(Py));

	ret = gmImportECKey(d, Px, Py);
	uart_printk("gmImportECKey, ret:%d\r\n", ret);
	uart_print_hex("gmImportECKey, d::", d, 0x20);
	uart_print_hex("gmImportECKey, Px::", Px, 0x20);
	uart_print_hex("gmImportECKey, Py::", Py, 0x20);
}

// ---------------------
static int test_cipher_p2;

void gmCipherInit_test( void )
{
	int ret;
	unsigned char key[32];
	unsigned char iv[16];
	unsigned char P2;
	int key_len, iv_len;
	
	int en, mode, type;
	
	memset(key, 0x01, sizeof(key));
	memset(iv, 0x00, sizeof(iv));
	
	if( xxx_input_int("gmCipherInit, (1:encypt,0:decrypt)::", &en) < 0x00 || ( en != 0 && en != 1))
	{
		return ;
	}
	if( xxx_input_int("gmCipherInit, (1:CBC,0:ECB)::", &mode) < 0x00 || (mode != 0 && mode != 1))
	{
		return ;
	}
	if( xxx_input_int("gmCipherInit, type::", &type) || type > 6 )
	{
		return ;
	}
	if( xxx_input_int("gmCipherInit, key_len::", &key_len) != 0x00)
	{
		return ;
	}
	if( xxx_input_int("gmCipherInit, iv_len::", &iv_len) != 0x00)
	{
		return ;
	}

	test_cipher_p2 = 0;
	test_cipher_p2 = ( en << 7 ) | ( mode << 4 ) | type;
	
	
	ret = gmCipherInit(test_cipher_p2, key, key_len, iv, iv_len );
	uart_printk("gmCipherInit(%d), ret:%d\r\n", test_cipher_p2, ret);
	uart_print_hex("gmCipherInit, key::", key, key_len);
	uart_print_hex("gmCipherInit, iv::", iv, iv_len);
}

void gmCipherUpdate_test( void )
{
	int ret;
	unsigned char data[1024];
	unsigned char out[1024];
	int len, val;

	if( xxx_input_int("gmCipherUpdate, len::", &len) < 0x00 )
	{
		return ;
	}
	
	if( xxx_input_int("gmCipherUpdate, val::", &val) < 0x00 )
	{
		return ;
	}
	printf("len:%d, val:%d\r\n", len, val);
	memset(data, val, sizeof(data));
	
	ret = gmCipherUpdate( data, len, out );
	uart_printk("gmCipherFinal(%d), ret:%d\r\n", test_cipher_p2, ret);
	uart_print_hex("gmCipherFinal, data::", data, len);
	uart_print_hex("gmCipherFinal, out::", out, len);
}

void gmCipherFinal_test( void )
{
	int ret;

	ret = gmCipherFinal();
	uart_printk("gmCipherFinal(%d), ret:%d\r\n", test_cipher_p2, ret);
}

void gmGetRandom_test( void )
{
	int ret;
	int len;
	unsigned char data[1024];
	
	if( xxx_input_int("gmGetRandom, Len::", &len) < 0x00 )
	{
		return ;
	}

	ret = gmGetRandom( data, len );
	uart_printk("gmGetRandom, ret: %d\r\n", ret);
	uart_print_hex("gmGetRandom, data::\r\n", data, len);
}


// SM2 Encrypt
static unsigned char c1[0x41];
static unsigned char c3[0x20];
static unsigned char c2[0x20];
static unsigned char c1_c2_c3[128];
static int c1_c2_c3_len, c1_len, c2_len, c3_len;

void gmSM2EncryptInit_test( void )
{
	int ret;
	int len;

	ret = gmSM2EncryptInit( c1, &len );
	
	uart_printk("gmSM2EncryptInit, ret: %d\r\n", ret);
	uart_print_hex("gmSM2EncryptInit, C1::\r\n", c1, len);
}

void gmSM2EncryptUpdate_test( void )
{
	int ret;
	unsigned char data[1024];
	int len, val;

	if( xxx_input_int("gmSM2EncryptUpdate, len::", &len) < 0x00 || len > sizeof(c2))
	{
		return ;
	}
	
	if( xxx_input_int("gmSM2EncryptUpdate, val::", &val) < 0x00 )
	{
		return ;
	}
	memset(data, val, sizeof(data));

	ret = gmSM2EncryptUpdate( data, len, c2, &c2_len );

	uart_printk("gmSM2EncryptUpdate, ret: %d\r\n", ret);
	uart_print_hex("gmSM2EncryptUpdate, C2::\r\n", c2, c2_len);
}

void gmSM2EncryptFinal_test( void )
{
	int ret;

	ret = gmSM2EncryptFinal( c3, &c3_len );
	
	uart_printk("gmSM2EncryptFinal, ret: %d\r\n", ret);
	uart_print_hex("gmSM2EncryptFinal, C3::\r\n", c3, c3_len);
}

void gmSM2Encrypt_test( void )
{
	int ret;
	unsigned char data[1024];
	int len, val;

	if( xxx_input_int("gmSM2Encrypt, len::", &len) < 0x00 || len > sizeof(c2))
	{
		return ;
	}
	
	if( xxx_input_int("gmSM2Encrypt, val::", &val) < 0x00 )
	{
		return ;
	}
	
	memset(data, val, sizeof(data));

	//c1_c2_c3_len = len + 97;
	
	ret = gmSM2Encrypt( data, len, c1_c2_c3, &c1_c2_c3_len );
	
	uart_printk("gmSM2Encrypt, ret: %d\r\n", ret);
	uart_print_hex("c1_c2_c3, encrypt::\r\n", c1_c2_c3, c1_c2_c3_len);
}


void gmSM2Encrypt2_test( void )
{
	int ret;
	unsigned char data[1024];
	int len, val;

	if( xxx_input_int("gmSM2Encrypt2, len::", &len) < 0x00 )
	{
		return ;
	}
	
	if( xxx_input_int("gmSM2Encrypt2, val::", &val) < 0x00 )
	{
		return ;
	}
	memset(data, val, sizeof(data));

	//c1_c2_c3_len = len + 97;
	ret = gmSM2Encrypt2( data, len, c1_c2_c3, &c1_c2_c3_len );
	
	uart_printk("gmSM2Encrypt2, ret: %d\r\n", ret);
	uart_print_hex("c1_c2_c3, encrypt2::\r\n", c1_c2_c3, c1_c2_c3_len);
}


void gmSM2DecryptInit_test( void )
{
	int ret;

	ret = gmSM2DecryptInit( c1 );
	
	uart_printk("gmSM2DecryptInit, ret: %d\r\n", ret);
	uart_print_hex("gmSM2DecryptInit, C1::\r\n", c1, sizeof(c1));
}


void gmSM2DecryptUpdate_test( void )
{
	int ret;
	int len, val;
	unsigned char out[128];
	int out_len;

	if( xxx_input_int("gmSM2DecryptUpdate, len::", &len) < 0x00 )
	{
		return ;
	}

	ret = gmSM2DecryptUpdate( c2, len, out, &out_len );
	uart_printk("gmSM2DecryptUpdate, ret: %d\r\n", ret);
	uart_print_hex("gmSM2DecryptUpdate, C2::\r\n", c2, sizeof(c2));
	uart_print_hex("gmSM2DecryptUpdate, OUT::\r\n", out, out_len);
}

void gmSM2DecryptFinal_test( void )
{
	int ret;

	ret = gmSM2DecryptFinal( c3 );
	uart_printk("gmSM2DecryptFinal, ret: %d\r\n", ret);
	uart_print_hex("gmSM2DecryptFinal, C3::\r\n", c3, sizeof(c3));
}

void gmSM2Decrypt_test( void )
{
	int ret;
	unsigned char out[128];
	int out_len;

	ret = gmSM2Decrypt(c1_c2_c3, c1_c2_c3_len, out, &out_len);
	
	uart_printk("gmSM2Decrypt, ret: %d\r\n", ret);
	uart_print_hex("c1_c2_c3, decrypt::\r\n", out, out_len);
}

void gmSM2Decrypt2_test( void )
{
	int ret;
	unsigned char out[128];
	int out_len;

	ret = gmSM2Decrypt2(c1_c2_c3, c1_c2_c3_len, out, &out_len);
	
	uart_printk("gmSM2Decrypt2, ret: %d\r\n", ret);
	uart_print_hex("c1_c2_c3, decrypt::\r\n", out, out_len);
}

// 签名 & 验签
static unsigned char e[0x20];
static unsigned char rs[0x40];
static unsigned char ers[0x60];
static unsigned char z[0x20];
static unsigned char id[0x20];
static int id_len, e_len, rs_len, ers_len, z_len;

void gmSM2SignHash_test( void )
{
	int ret;

	memset( e, 0xaa, sizeof(e));
	
	ret = gmSM2SignHash( e, rs, &rs_len );
	
	uart_printk("gmSM2SignHash, ret: %d\r\n", ret);
	uart_print_hex("gmSM2SignHash, e::\r\n", e, sizeof(e));
	uart_print_hex("gmSM2SignHash, rs::\r\n", rs, rs_len);
}

void gmSM2SignInit_test( void )
{
	int ret;
	int val;
	
	if( xxx_input_int("gmSM2SignInit, id_len::", &id_len) < 0x00 || id_len > sizeof(id) )
	{
		return ;
	}
	if( xxx_input_int("gmSM2SignInit, id_val::", &val) < 0x00 )
	{
		return ;
	}

	memset(id, val, sizeof(id));
	
	ret = gmSM2SignInit( id, id_len, z, &z_len );
	uart_printk("gmSM2SignInit, ret: %d\r\n", ret);
	uart_print_hex("gmSM2SignInit, id::\r\n", id, id_len);
	uart_print_hex("gmSM2SignInit, Z::\r\n", z, sizeof(z));
}

void gmSM2SignUpdate_test( void )
{
	int ret;

	ret = gmSM2SignUpdate( c2, sizeof(c2) );
	uart_printk("gmSM2SignUpdate, ret: %d\r\n", ret);
	uart_print_hex("gmSM2SignUpdate, data::\r\n", c2, sizeof(c2));
}

void gmSM2SignFinal_test( void )
{
	int ret;
	
	ret = gmSM2SignFinal( ers, &ers_len );
	uart_printk("gmSM2SignFinal, ret: %d\r\n", ret);
	uart_print_hex("gmSM2SignFinal, ers::\r\n", ers, ers_len);
}

void gmECDSASignHash_test( void )
{
	int ret;
	
	memset( e, 0xaa, sizeof(e));

	ret = gmECDSASignHash( e, rs, &rs_len );
	uart_printk("gmSM2SignHash, ret: %d\r\n", ret);
	uart_print_hex("gmSM2SignHash, e::\r\n", e, sizeof(e));
	uart_print_hex("gmSM2SignHash, rs::\r\n", rs, rs_len);
}

void gmSM2VerifyHash_test( void )
{
	int ret;

	ret = gmSM2VerifyHash( e, rs );
	uart_printk("gmSM2VerifyHash, ret: %d\r\n", ret);
	uart_print_hex("gmSM2VerifyHash, e::\r\n", e, sizeof(e));
	uart_print_hex("gmSM2VerifyHash, rs::\r\n", rs, sizeof(rs));
}

void gmSM2VerifyInit_test( void )
{
	int ret;
	int val;
	int z_len;
	
	if( xxx_input_int("gmSM2VerifyInit, id_len::", &id_len) < 0x00 || id_len > sizeof(id) )
	{
		return ;
	}
	if( xxx_input_int("gmSM2VerifyInit, id_val::", &val) < 0x00 )
	{
		return ;
	}

	memset(id, val, sizeof(id));
	
	ret = gmSM2VerifyInit( id, id_len, z, &z_len );
	uart_printk("gmSM2VerifyInit, ret: %d\r\n", ret);
	uart_print_hex("gmSM2VerifyInit, id::\r\n", id, id_len);
	uart_print_hex("gmSM2VerifyInit, Z::\r\n", z, z_len);
}

void gmSM2VerifyUpdate_test( void )
{
	int ret;

	ret = gmSM2VerifyUpdate( c2, sizeof(c2) );
	uart_printk("gmSM2VerifyUpdate, ret: %d\r\n", ret);
	uart_print_hex("gmSM2VerifyUpdate, data::\r\n", c2, sizeof(c2));
}

void gmSM2VerifyFinal_test( void )
{
	int ret;
	
	ret = gmSM2VerifyFinal( ers+0x20 );
	uart_printk("gmSM2VerifyFinal, ret: %d\r\n", ret);
	uart_print_hex("gmSM2VerifyFinal, ers::\r\n", ers, sizeof(ers));
}

void gmECDSAVerifyHash_test( void )
{
	int ret;

	ret = gmECDSAVerifyHash( e, rs );
	uart_printk("gmECDSAVerifyHash, ret: %d\r\n", ret);
	uart_print_hex("gmECDSAVerifyHash, e::\r\n", e, sizeof(e));
	uart_print_hex("gmECDSAVerifyHash, rs::\r\n", rs, sizeof(rs));
}

// ------------------------------------------------------------------
void gm_test(void)
{	
	ST_MENU_OPT tMenuOpt[] = {
		" gm_test ", NULL,
		"gmGetVersion", gmGetVersion_test,
		"gmGetUID", gmGetUID_test,
		"gmHSMBist", gmHSMBist_test,
		
		"gmGetRandom", gmGetRandom_test,
		
		"gmCipherInit", gmCipherInit_test,
		"gmCipherUpdate", gmCipherUpdate_test,
		"gmCipherFinal", gmCipherFinal_test,
		
		"gmHashInit", gmHashInit_test,
		"gmHashUpdate", gmHashUpdate_test,
		"gmHashFinal", gmHashFinal_test,
		
		"gmGenECKey", gmGenECKey_test,
		"gmExportECKey", gmExportECKey_test,
		"gmImportECKey", gmImportECKey_test,
		
		"gmSM2EncryptInit", gmSM2EncryptInit_test,
		"gmSM2EncryptUpdate", gmSM2EncryptUpdate_test,
		"gmSM2EncryptFinal", gmSM2EncryptFinal_test,
		"gmSM2Encrypt", gmSM2Encrypt_test,
		"gmSM2Encrypt2", gmSM2Encrypt2_test,
		
		"gmSM2DecryptInit", gmSM2DecryptInit_test,
		"gmSM2DecryptUpdate", gmSM2DecryptUpdate_test,
		"gmSM2DecryptFinal", gmSM2DecryptFinal_test,
		"gmSM2Decrypt", gmSM2Decrypt_test,
		"gmSM2Decrypt2", gmSM2Decrypt2_test,
		
		"gmSM2SignHash", gmSM2SignHash_test,
		"gmSM2SignInit", gmSM2SignInit_test,
		"gmSM2SignUpdate", gmSM2SignUpdate_test,
		"gmSM2SignFinal", gmSM2SignFinal_test,
		"gmSM2VerifyHash", gmSM2VerifyHash_test,
		
		"gmECDSASignHash", gmECDSASignHash_test,
		"gmSM2VerifyInit", gmSM2VerifyInit_test,
		"gmSM2VerifyUpdate", gmSM2VerifyUpdate_test,
		"gmSM2VerifyFinal", gmSM2VerifyFinal_test,
		"gmECDSAVerifyHash", gmECDSAVerifyHash_test,
		
		"gmGetUID_loop", gmGetUID_loop_test,
	};

	uart_MenuSelect(tMenuOpt, sizeof(tMenuOpt)/sizeof(tMenuOpt[0]));
}


extern void font_file_open( void );

void fck_test(void)
{
	ST_MENU_OPT tMenuOpt[] = {
		" ALL_fck_test ", NULL,
		"printer_test", printer_test,
		"ddi_iccpsam_test",ddi_iccpsam_test,
		"font_file_open", font_file_open,
		"gm_test", gm_test,
	};

	uart_MenuSelect(tMenuOpt, sizeof(tMenuOpt)/sizeof(tMenuOpt[0]));
}


int main(int argc, char** argv)
{
    spi_ddi_sys_init();

	fck_test();

	return 0;
}


