

#ifndef _BIOSSOFTHASH_H_
#define _BIOSSOFTHASH_H_


#define USESOFTHASHALTH

#ifdef  USESOFTHASHALTH

#define SHA_VERSION 1

#define SHA_BYTE_ORDER 1234

typedef unsigned long SHALONG;   

#define SHA_BLOCKSIZE         64
#define SHA_DIGESTSIZE        20

typedef struct //__attribute__ ((__packed__))
{
	SHALONG digest[5];      
	SHALONG count_lo, count_hi;  
	unsigned char data[SHA_BLOCKSIZE]; 
	int local;           
}PCISHA_INFO;



typedef struct {
   unsigned char data[64];
   unsigned int datalen;
   unsigned int bitlen[2];
   unsigned int state[8];
} SHA256_CTX;


extern void pcisha_init(PCISHA_INFO *sha_info);
extern void PCIsha_update(PCISHA_INFO *sha_info, unsigned char *buffer, int count);
extern void pcisha_final(unsigned char digest[20], PCISHA_INFO *sha_info);
extern void Lib_Hash(unsigned char * DataIn,unsigned int DataInLen,unsigned char* DataOut);
extern void sha256_init(SHA256_CTX *ctx);
extern void sha256_update(SHA256_CTX *ctx, unsigned char *data, unsigned int len);
extern void sha256_final(SHA256_CTX *ctx, unsigned char *hash);




#else


extern void Lib_Hash(unsigned char * DataIn,unsigned int DataInLen,unsigned char* DataOut);

#endif

#endif

