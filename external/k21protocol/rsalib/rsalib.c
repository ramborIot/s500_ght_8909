/**
 *Unit Name: crc32.cpp
 *Current Version: 1.0
 *Description:
 *   CRC32算法.
 *Author:
 *   fanzhiqiang@xinguodu.com
 *Copyright:
 *   Copyright(C) 2009.10
 *History:
 *TODO:
**/

//#include <linux/kernel.h>
//#include <linux/init.h>
//#include <linux/module.h>
// #include <math.h>
// #include <time.h>
// #include <config.h>
// #include <stdlib.h>

#include "rsalib.h"
#include <malloc.h>
/**
 * RSA import function.
 **/
#include "md5.h"
#include "des.h"
#include "rsa.h"

#ifndef NULL
#define NULL 0
#endif

#define RSA_PUBLIC_EXPONENT     65537       //E: RSA public exponent

/**
 *Description:
 *  MD5离散。
 *Input:
 *  Src - 源数据
 *  Len - 数据长度
 *Output：
 *  Dest: 输出的加密后的结果(16 Bytes)
 **/
void calcMD5(unsigned char* Dest, unsigned char* Src, int Len)
{
    MD5_CTX context;

    MD5Init(&context);
    MD5Update(&context, Src, Len);
    MD5Final(Dest, &context);
}
//---------------------------------------------------------------------------

/**
 *Description:
 *  DES加解密.
 *Input：
 *  Mode - 1:加密; 0:解密
 *  Src  - 输入数据
 *  Len  - 输入长度
 *  Key8 - 密钥(8 bytes)
 *Output：
 *  Dest - 输出数据
 *Return：
 *  0 - 参数问题
 * >0 - 输出数据长度
**/
int DesEncrypt(char Mode, unsigned char* Dest, const unsigned char* Src, const int Len, const unsigned char* Key8)
{
    unsigned char keyt[8] = {0};
    unsigned char iv[8] = {0};
    unsigned char tmp[8] = {0};
    unsigned char *pSrc = (unsigned char *)Src;
    unsigned char *pDest = (unsigned char *)Dest;
    DES_CBC_CTX ctx;

	int page ;
	int redu ;
	int iRet ;

	if (0 == Len) 
		return 0;

	page = Len / 8;
	redu = Len % 8;
	iRet = page * 8;

    memcpy(keyt, Key8, 8);

    

    DES_CBCInit(&ctx, keyt, iv, Mode);
    DES_CBCUpdate(&ctx, pDest, pSrc, iRet);
    if (redu > 0)
    {
        memcpy(tmp, &pSrc[page * 8], redu);
        DES_CBCUpdate(&ctx, &pDest[page * 8], tmp, 8);
        iRet += 8;
    }
    DES_CBCRestart(&ctx);

    return iRet;
}

/**
 *Description:
 *  3DES加解密.
 *Input：
 *  Mode- 1:加密; 0:解密
 *  Key16 - 密钥(16 bytes)
 *  Src - 输入数据
 *  Len - 输入长度
 *Output：
 *  Dest - 输出数据
 *Return：
 *  0 - 参数问题
 * >0 - 输出数据长度
**/
int Des3Encrypt(char Mode, unsigned char* Dest, const unsigned char* Src, const int Len, const unsigned char* Key16)
{
    int iRet = 0;

    unsigned char hKey[8] = {0}, lKey[8] = {0};
    memcpy(hKey, Key16, 8);
    memcpy(lKey, &Key16[8], 8);

    if (Mode == 1)
    {
        iRet = DesEncrypt(1, Dest, Src,  Len,  hKey);
        iRet = DesEncrypt(0, Dest, Dest, iRet, lKey);
        iRet = DesEncrypt(1, Dest, Dest, iRet, hKey);
    }
    else if (Mode == 0)
    {
        iRet = DesEncrypt(0, Dest, Src,  Len,  hKey);
        iRet = DesEncrypt(1, Dest, Dest, iRet, lKey);
        iRet = DesEncrypt(0, Dest, Dest, iRet, hKey);
    }

    return iRet;
}

//高斯随机数
/*  double gaussRand()
{
    static double V1, V2, S;
    static int phase = 0;
    double X;

    if(phase == 0)
    {
        do
        {
            double U1 = (double)rand() / RAND_MAX;
            double U2 = (double)rand() / RAND_MAX;

            V1 = 2 * U1 - 1;
            V2 = 2 * U2 - 1;
            S = V1 * V1 + V2 * V2;
        }
        while(S >= 1 || S == 0);

        X = V1 * sqrt(-2 * log(S) / S);
    }
    else
    {
        X = V2 * sqrt(-2 * log(S) / S);
    }

    phase = 1 - phase;

    return X;
}*/

static int WriteBigInt(unsigned char *pBuf, unsigned char *BitInt, unsigned int Len)
{
	int iRet = 0;

    while (*BitInt == 0 && Len > 0)
    {
        BitInt++;
        Len--;
    }
    if (Len == 0) return 0;

    for (; Len > 0; Len--, iRet++)
    {
        *pBuf++ = (unsigned char)(*BitInt++);
    }

    return iRet;
}

static void ReadBigInt(unsigned char *BigInt, unsigned int sizeBigInt, unsigned char *pBuf, unsigned int Len)
{
	int i;
    memset(BigInt, 0, sizeBigInt);

    BigInt += sizeBigInt - Len;
    for (i=0; i<(int)Len; i++)
    {
        *BigInt++ = *pBuf++;
    }
}

//输出公钥及随机数
static void OutputPK(unsigned char *PK, unsigned int *pkLen, R_RSA_PUBLIC_KEY *publicKey, R_RANDOM_STRUCT *randomStruct)
{
	int ret;
    *pkLen = 0;

    DW2P(PK, publicKey->bits);
    PK += 4;
    *pkLen += 4;
    ret = WriteBigInt(PK, publicKey->modulus, sizeof(publicKey->modulus));
    PK += ret;
    *pkLen += ret;
    ret = WriteBigInt(PK, randomStruct->state, sizeof(randomStruct->state));
    PK += ret;
    *pkLen += ret;
    ret = WriteBigInt(PK, randomStruct->output, sizeof(randomStruct->output));
    PK += ret;
    *pkLen += ret;
}

//输入公钥及随机数
static void InputPK(R_RSA_PUBLIC_KEY *publicKey, R_RANDOM_STRUCT *randomStruct, unsigned char *PK, unsigned int pkLen)
{
	int modulusLen;
	unsigned char *tmp;

    P2DW(publicKey->bits, PK);
    PK += 4;

    modulusLen = (publicKey->bits + 7) / 8;

    //E: public exponent
    memset(publicKey->exponent, 0, sizeof(publicKey->exponent));
    tmp = &publicKey->exponent[sizeof(publicKey->exponent) - 4 + 1];
    DW3P(tmp, RSA_PUBLIC_EXPONENT);   //modify by 2012-04-08

    ReadBigInt(publicKey->modulus, sizeof(publicKey->modulus), PK, modulusLen);
    PK += modulusLen;

    ReadBigInt(randomStruct->state, sizeof(randomStruct->state), PK, 16);
    PK += 16;
    ReadBigInt(randomStruct->output, sizeof(randomStruct->output), PK, 16);
    PK += 16;

    randomStruct->bytesNeeded = 0;
    randomStruct->outputAvailable = 0;
}

//输出私钥
static void OutputSK(unsigned char *SK, unsigned int *skLen, R_RSA_PRIVATE_KEY *privateKey)
{
	int ret;
    *skLen = 0;

    DW2P(SK, privateKey->bits);
    SK += 4;
    *skLen += 4;
    ret = WriteBigInt(SK, privateKey->modulus, sizeof(privateKey->modulus));
    SK += ret;
    *skLen += ret;
    ret = WriteBigInt(SK, privateKey->exponent, sizeof(privateKey->exponent));
    SK += ret;
    *skLen += ret;
    ret = WriteBigInt(SK, privateKey->prime[0], sizeof(privateKey->prime[0]));
    SK += ret;
    *skLen += ret;
    ret = WriteBigInt(SK, privateKey->prime[1], sizeof(privateKey->prime[1]));
    SK += ret;
    *skLen += ret;
    ret = WriteBigInt(SK, privateKey->primeExponent[0], sizeof(privateKey->primeExponent[0]));
    SK += ret;
    *skLen += ret;
    ret = WriteBigInt(SK, privateKey->primeExponent[1], sizeof(privateKey->primeExponent[1]));
    SK += ret;
    *skLen += ret;
    ret = WriteBigInt(SK, privateKey->coefficient, sizeof(privateKey->coefficient));
    SK += ret;
    *skLen += ret;
}

static void InputSK(R_RSA_PRIVATE_KEY *privateKey, unsigned char *SK, unsigned int skLen)
{
	unsigned char *tmp;
	int modulusLen;
	int primeLen;

    P2DW(privateKey->bits, SK);
    SK += 4;

    //E: public exponent
    memset(privateKey->publicExponent, 0, sizeof(privateKey->publicExponent));
    tmp = &privateKey->publicExponent[sizeof(privateKey->publicExponent) - 4 + 1];
    DW3P(tmp, RSA_PUBLIC_EXPONENT);   //modify by 2012-04-08
	modulusLen = (privateKey->bits + 7) / 8;
	primeLen = modulusLen / 2;

    ReadBigInt(privateKey->modulus, sizeof(privateKey->modulus), SK, modulusLen);
    SK += modulusLen;
    ReadBigInt(privateKey->exponent, sizeof(privateKey->exponent), SK, modulusLen);
    SK += modulusLen;
    ReadBigInt(privateKey->prime[0], sizeof(privateKey->prime[0]), SK, primeLen);
    SK += primeLen;
    ReadBigInt(privateKey->prime[1], sizeof(privateKey->prime[1]), SK, primeLen);
    SK += primeLen;
    ReadBigInt(privateKey->primeExponent[0], sizeof(privateKey->primeExponent[0]), SK, primeLen);
    SK += primeLen;
    ReadBigInt(privateKey->primeExponent[1], sizeof(privateKey->primeExponent[1]), SK, primeLen);
    SK += primeLen;
    ReadBigInt(privateKey->coefficient, sizeof(privateKey->coefficient), SK, primeLen);
    SK += primeLen;
}

/**
 *RSA 产生密钥对(512~4096bits)
 *512-bits:
 *   PK: 100 bytes; SK:  292 bytes; Block:  64 bytes; TIME: 0s
 *1024-bits:
 *   PK: 164 bytes; SK:  580 bytes; Block: 128 bytes; TIME: 1s
 *2048-bits:
 *   PK: 292 bytes; SK: 1156 bytes; Block: 256 bytes; TIME: 2~20s
 *4096-bits:
 *   PK: 548 bytes; SK: 2308 bytes; Block: 512 bytes; TIME: 180s~380s
 *Parameters:
 *  PK/pkLen[out] - 输出的公钥
 *  SK/skLen[out] - 输出的私钥
 *  bits[in] - 密钥位数
 *Return:
 *  0 - 成功
 * >0 - 失败
 **/
//extern int rng_getrandomnum(void *buf, int nbytes);
int RSA_GenPaires(unsigned char *PK, unsigned int *pkLen,
                  unsigned char *SK, unsigned int *skLen,
                  unsigned int bits)
{
    R_RANDOM_STRUCT   randomStruct;
    R_RSA_PROTO_KEY   protoKey;
    R_RSA_PUBLIC_KEY  publicKey;
    R_RSA_PRIVATE_KEY privateKey;
	int status; 
	// int i;
	static unsigned char seedByte[RANDOM_BYTES_NEEDED / 8] ;
	unsigned int bytesNeeded;

	if(bits == 0 || bits > 4096)
		bits = 1024;

    protoKey.bits = bits;
    protoKey.useFermat4 = 1;            //E:65537
	
    // srand((unsigned int)time(NULL));
	memset(seedByte, 0x00,sizeof(seedByte)/sizeof(seedByte[0]));
    R_RandomInit(&randomStruct);
    /* Initialize with all zero seed bytes, which will not yield an actual random number output. */
    while (1)
    {
        R_GetRandomBytesNeeded(&bytesNeeded, &randomStruct);
        if (bytesNeeded == 0) 
			break;

        // for (i=0; i<(int)sizeof(seedByte); i++)
        //     seedByte[i] = (unsigned char)((int)(gaussRand() * 1000) % 0xFF);
		//rng_getrandomnum(seedByte,sizeof(seedByte)/sizeof(seedByte[0]));
        R_RandomUpdate(&randomStruct, seedByte, sizeof(seedByte));
    }
	DEBUG_INFO();

    status = R_GeneratePEMKeys(&publicKey, &privateKey, &protoKey, &randomStruct);
	DEBUG_INFO();
    if (0 == status)
    {
		DEBUG_INFO();
        OutputPK(PK, pkLen, &publicKey, &randomStruct);
		DEBUG_INFO();
        OutputSK(SK, skLen, &privateKey);
		DEBUG_INFO();
    }

    R_RandomFinal (&randomStruct);
	DEBUG_INFO();

    return 0;
}
//EXPORT_SYMBOL_GPL(RSA_GenPaires);

/**
 *RSA 公钥加密.
 *Parameters:
 *  output/outputLen[out] - 输出加密数据
 *  input/inputLen[in] - 源数据
 *  PK/pkLen[in] - 公钥
 *Return:
 *  0 - 成功
 * >0 - 失败
 *Memo:
 *  使用RSA_SKDecrypt()进行解密.
 **/
int RSA_PKEncrypt(unsigned char *output, unsigned int *outputLen,
                  unsigned char *input,  unsigned int inputLen,
                  unsigned char *PK, unsigned int pkLen)
{
    R_RANDOM_STRUCT   randomStruct;
    R_RSA_PUBLIC_KEY  publicKey;

    InputPK(&publicKey, &randomStruct, PK, pkLen);

    return RSAPublicEncrypt(output, outputLen, input, inputLen, &publicKey, &randomStruct);
}
//EXPORT_SYMBOL_GPL(RSA_PKEncrypt);

/**
 *RSA 私钥解密.
 *Parameters:
 *  output/outputLen[out] - 输出解密数据
 *  input/inputLen[in] - 源数据
 *  SK/skLen[in] - 私钥
 *Return:
 *  0 - 成功
 * >0 - 失败
 *Memo:
 *  对RSA_PKEncrypt()的加密数据解密.
 **/
int RSA_SKDecrypt(unsigned char *output, unsigned int *outputLen,
                  unsigned char *input,  unsigned int inputLen,
                  unsigned char *SK, unsigned int skLen)
{
    R_RSA_PRIVATE_KEY privateKey;

    InputSK(&privateKey, SK, skLen);

    return RSAPrivateDecrypt(output, outputLen, input, inputLen, &privateKey);
}
//EXPORT_SYMBOL_GPL(RSA_SKDecrypt);
//-----------------------------------------------------------------------------

/**
 *RSA 私钥加密.
 *Parameters:
 *  output/outputLen[out] - 输出加密数据
 *  input/inputLen[in] - 源数据
 *  SK/skLen[in] - 私钥
 *Return:
 *  0 - 成功
 * >0 - 失败
 *Memo:
 *  使用RSA_PKDecrypt()进行解密.
 **/
int RSA_SKEncrypt(unsigned char *output, unsigned int *outputLen,
                  unsigned char *input, unsigned int inputLen,
                  unsigned char *SK, unsigned int skLen)
{
    R_RSA_PRIVATE_KEY privateKey;

    InputSK(&privateKey, SK, skLen);

    return RSAPrivateEncrypt(output, outputLen, input, inputLen, &privateKey);
}
//EXPORT_SYMBOL_GPL(RSA_SKEncrypt);

/**
 *RSA 公钥解密.
 *Parameters:
 *  output/outputLen[out] - 输出解密数据
 *  input/inputLen[in] - 源数据
 *  PK/pkLen[in] - 公钥
 *Return:
 *  0 - 成功
 * >0 - 失败
 *Memo:
 *  对RSA_SKEncrypt()的加密数据解密.
 **/
int RSA_PKDecrypt(unsigned char * output, unsigned int *outputLen,
                  unsigned char * input,  unsigned int inputLen,
                  unsigned char * PK, unsigned int pkLen)
{
    R_RANDOM_STRUCT   randomStruct;
    R_RSA_PUBLIC_KEY  publicKey;

    InputPK(&publicKey, &randomStruct, PK, pkLen);

    return RSAPublicDecrypt(output, outputLen, input, inputLen, &publicKey);
}
//EXPORT_SYMBOL_GPL(RSA_PKDecrypt);
//-----------------------------------------------------------------------------

unsigned char RSARecover(unsigned char *m, unsigned int mLen, unsigned char *e, unsigned int eLen,unsigned char *input,unsigned char *output)
{
	unsigned int i,out_len;
	R_RSA_PUBLIC_KEY publicKey;
	memset((unsigned char*)&publicKey,0,sizeof(publicKey));
	publicKey.bits = mLen*8;
	if(eLen == 0x01)
		publicKey.exponent[MAX_RSA_MODULUS_LEN-1] = *e;	
	else if(eLen == 0x03)
	{
		publicKey.exponent[MAX_RSA_MODULUS_LEN-1] = *e;
		publicKey.exponent[MAX_RSA_MODULUS_LEN-2] = *(e+1);
		publicKey.exponent[MAX_RSA_MODULUS_LEN-3] = *(e+2);	
	}
	else return 1;
	memcpy(&publicKey.modulus[MAX_RSA_MODULUS_LEN-mLen],m,mLen);
	i = RSAPublicDecrypt1(output,&out_len,input,mLen,&publicKey);
	return i;
}

//EXPORT_SYMBOL_GPL(RSARecover);

#if 0

#define DRV_NAME "okolle-rsa"
#define DRV_AUTH "zhongjun@xinguodu.com"
#define DRV_VERS "v1.00"
#define DRV_LISC "GPL"
#define DRV_DESC "okolle-rsa"

static const  char hex_to_ascii_table[16] = "0123456789ABCDEF";

void binarytohex(char *buf, long x, int nbytes)
{
	int i;
	int s = 4*(2*nbytes - 1);
	for (i = 0; i < 2*nbytes; i++)
	{
		buf[i] = hex_to_ascii_table[(x >> s) & 0xf];
		s -= 4;
	}
	buf[2*nbytes] = 0;
}

#define BL_ISPRINT(ch)          (((ch) >= ' ') && ((ch) < 128))
void hex_dump(unsigned char *data, size_t num)
{
	int i;
	long oldNum;
	char buf[90];
	char *bufp;
	int line_resid;

	while (num) 
	{
		bufp = buf;
		binarytohex(bufp, (unsigned long)data, 4);
		bufp += 8;
		*bufp++ = ':';
		*bufp++ = ' ';

		oldNum = num;

		for (i = 0; i < 16 && num; i++, num--) 
		{
			binarytohex(bufp, (unsigned long)data[i], 1);
			bufp += 2;
			*bufp++ = (i == 7) ? '-' : ' ';
		}

		line_resid = (16 - i) * 3;
		if (line_resid)
		{
			memset(bufp, ' ', line_resid);
			bufp += line_resid;
		}

		memcpy(bufp, "| ", 2);
		bufp += 2;

		for (i = 0; i < 16 && oldNum; i++, oldNum--)
			*bufp++ = BL_ISPRINT(data[i]) ? data[i] : '.';

		line_resid = 16 - i;
		if (line_resid)
		{
			memset(bufp, ' ', 16 - i);
			bufp += 16 - i;
		}

		*bufp++ = '\r';
		*bufp++ = '\n';
		*bufp++ = '\0';
		printk(buf);
		data += 16;
	}
}

void test_rsa(unsigned char * str,int len)
{
	unsigned char pk[1024],sk[1024];
	int pk_len,sk_len;
	
	unsigned char out_tmp[ 1024 ],in_tmp[1024];
	unsigned char out1_tmp[ 1024 ],in1_tmp[1024];
	int out_len,in_len;
	int out1_len,in1_len;
	printk("RSA_GenPaires");
	RSA_GenPaires(pk,&pk_len,sk,&sk_len,128*8);
	printk("\npk:\n");
	hex_dump(pk,pk_len);
	printk("\nsk:\n");
	hex_dump(sk+4,sk_len-4);

	printk("RSA_SKEncrypt");
	memset(in_tmp,0x00,sizeof(in_tmp));
	memset(out_tmp,0x00,sizeof(out_tmp));
	memcpy(in_tmp,str,len);
	in_len = len ;
	RSA_SKEncrypt(out_tmp,&out_len,in_tmp,in_len,sk,sk_len);
	printk("\nin:\n");
	hex_dump(in_tmp,in_len);
	printk("\nout:\n");
	hex_dump(out_tmp,out_len);

	printk("RSA_PKDecrypt");
	memset(in1_tmp,0x00,sizeof(in1_tmp));
	memset(out1_tmp,0x00,sizeof(out1_tmp));
	in1_len = out_len;
	memcpy(in1_tmp,out_tmp,in1_len);
	RSA_PKDecrypt(out1_tmp,&out1_len,in1_tmp,in1_len,pk,pk_len);
	printk("\nin:\n");
	hex_dump(in1_tmp,in1_len);
	printk("\nout:\n");
	hex_dump(out1_tmp,out1_len);
	printk("done:\n");
}

static int __init drv_init(void)
{

	char str[] = "123456789abcdef01234567";
	printk(KERN_INFO "%s register ok\n",DRV_NAME);  
	test_rsa(str,strlen(str));
	return 0;
}

static void __exit drv_exit(void)
{

	printk(KERN_INFO "%s unregister ok\n",DRV_NAME);    
	return;
}

module_init(drv_init);
module_exit(drv_exit);

MODULE_DESCRIPTION(DRV_DESC);
MODULE_AUTHOR(DRV_AUTH);
MODULE_LICENSE(DRV_LISC);
MODULE_VERSION(DRV_VERS);
#endif
