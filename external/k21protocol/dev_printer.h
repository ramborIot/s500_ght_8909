#ifndef __DEV_PRINTER_H
#define __DEV_PRINTER_H
//#include "drv_print_btm.h"

#include "font.h"
typedef unsigned char       uint8;  /*  8 bits */
typedef unsigned short int  uint16; /* 16 bits */
typedef unsigned int    uint32; /* 32 bits */

typedef char            int8;   /*  8 bits */
typedef short int           int16;  /* 16 bits */
typedef int             int32;  /* 32 bits */

typedef volatile int8       vint8;  /*  8 bits */
typedef volatile int16      vint16; /* 16 bits */
typedef volatile int32      vint32; /* 32 bits */

typedef volatile uint8      vuint8;  /*  8 bits */
typedef volatile uint16     vuint16; /* 16 bits */
typedef volatile uint32     vuint32; /* 32 bits */


typedef struct tagBITMAPFILEHEADER  //文件头  14B  
{ 
    uint16  bfType;   
    uint32  bfSize;   
    uint16  bfReserved1;   
    uint16  bfReserved2;   
    uint32  bfOffBits;   
} BITMAPFILEHEADER; 

typedef struct tagBITMAPINFOHEADER  //头文件信息
{ 
    uint32 biSize;   
    int32 biWidth;     
    int32 biHeight;     
    uint16 biPlanes;   
    uint16 biBitCount;  
    uint32 biCompression;   
    uint32 biSizeImage;   
    int32 biXPelsPerMeter;   
    int32 biYPelsPerMeter;   
    uint32 biClrUsed;  
    uint32 biClrImportant;  
} BITMAPINFOHEADER;

typedef struct tagRGBQUAD
{ 
uint8    rgbBlue;    
uint8    rgbGreen;     
uint8    rgbRed;     
uint8    rgbReserved;    
} RGBQUAD; 

#define LOGO0_STRING     "LOGO0.BMP"
#define LOGO0_ADDR_STA   (LOGO1_START_ADDR)     //logo0图片地址
#define LOGO1_STRING     "LOGO1.BMP"
#define LOGO1_ADDR_STA   (LOGO2_START_ADDR)     //logo0图片地址



#define DDI_THMPRN_CTL_VER 0
#define DDI_THMPRN_CTL_GRAY 1
#define DDI_THMPRN_CTL_LINEGAP 2
#define DDI_THMPRN_CTL_COLGAP 3

/*
typedef struct _strPrnTextCtrl
{
    u32  m_align;           //对齐方式
    u32  m_offset;           //列偏移
    u32  m_font;            //字体，关联ASCII字符
    u32  m_ascsize;         //ASCII字符字号
    u32  m_asczoom;        //ASCII字符放大方式
    u32  m_nativesize;       //国语字号
    u32  m_nativezoom;     //国语放大方式
} strPrnTextCtrl;


typedef struct _strPrnCombTextCtrl
{
    u32  m_x0;             //基于原点的横坐标 
    u32  m_y0;             //基于原点的纵坐标 
    u32  m_font;            //字体，关联ASCII字符
    u32  m_ascsize;         //ASCII字符字号
    u32  m_asczoom;        //ASCII字符放大方式
    u32  m_nativesize;       //国语字号
    u32  m_nativezoom;     //国语放大方式
    u8*  m_text;           //打印文本内容
} strPrnCombTextCtrl;
*/
extern s32 ddi_thmprn_open (void);
extern s32 ddi_thmprn_close(void);
extern s32 ddi_thmprn_feed_paper(u32 nPixels);
extern s32 ddi_thmprn_print_image(u32 nOrgLeft, u32 nImageWidth, u32 nImageHeight, const u8 *lpImage);
extern s32 ddi_thmprn_print_text (strPrnTextCtrl *lpPrnCtrl, const u8 *lpText );
extern s32 ddi_thmprn_get_status (void); 
extern s32 ddi_thmprn_ioctl(u32 nCmd, u32 lParam, u32 wParam);

extern s32 ddi_thmprn_print_image (u32 nOrgLeft, u32 nImageWidth, u32 nImageHeight, const u8 *lpImage);

extern s32 ddi_thmprn_print_image_file(u32 nOrgLeft, u32 nImageWidth, u32 nImageHeight, const u8 *lpImageName);


extern s32 ddi_thmprn_print_comb_text (u32 nNum, const strPrnCombTextCtrl* lpPrnCombTextCtrl[]);


extern s32 dev_printer_sleep(void);
#endif






