#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <linux/types.h>
#include "spidev.h"


static int verbose;


static void do_read(int fd, int len)
{
	unsigned char	buf[32], *bp;
	int		status;

	/* read at least 1 bytes, no more than 32 */
	if (len < 1)
		len = 1;
	else if (len > sizeof(buf))
		len = sizeof(buf);
	memset(buf, 0, sizeof buf);

	status = read(fd, buf, len);
	if (status < 0) {
		perror("read");
		return;
	}
	if (status != len) {
		fprintf(stderr, "short read\n");
		return;
	}

	printf("read(%2d, %2d): ", len, status);
	status -= 0;
	bp = buf + 0;
	while (status-- > 0)
		printf(" %02x", *bp++);
	printf("\n");
}
static void do_write(int fd, int len)
{
	
	unsigned char	buf[32]={0x0,0x2 ,0x58 ,0x47 ,0x44 ,0x54 ,0x45 ,0x49 ,0x0 ,0x68 ,0x0, 0x0, 0x3 ,0x1, 0x2, 0x3 ,0x3, 0xDA ,0x5A}, *bp;
	int			status,i;

	
//	memset(buf, 0, sizeof buf);

	if (len > sizeof buf)
		len = sizeof buf;
	

	printf("write:%2d \n", buf[len]);

	status = write(fd, &buf[len], 2);
	if (status < 0) {
		perror("write err");
		return;
	}

/*
	printf("write(%2d, %2d): ", len, status);
	for (bp = buf; len; len--)
		printf(" %02x", *bp++);
	printf("\n");
	*/
}
static void set_lsb(int fd,unsigned char  set)
{
	
			if (ioctl(fd, SPI_IOC_WR_LSB_FIRST, &set) < 0) {
					perror("SPI rd_lsb_fist");
					return;
			}
}
static void set_speed(int fd,unsigned int  set)
{
	
			if (ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &set) < 0) {
					perror("SPI rd_lsb_fist");
					return;
			}

}


static void dumpstat(const char *name, int fd)
{
	__u8	mode, lsb, bits;
	__u32	speed;

	if (ioctl(fd, SPI_IOC_RD_MODE, &mode) < 0) {
		perror("SPI rd_mode");
		return;
	}
	if (ioctl(fd, SPI_IOC_RD_LSB_FIRST, &lsb) < 0) {
		perror("SPI rd_lsb_fist");
		return;
	}
	if (ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits) < 0) {
		perror("SPI bits_per_word");
		return;
	}
	if (ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed) < 0) {
		perror("SPI max_speed_hz");
		return;
	}

	printf("%s: spi mode %d, %d bits %sper word, %d Hz max\n",
		name, mode, bits, lsb ? "(lsb first) " : "(msb first)", speed);
}

static void do_msg(int fd, int len)
{
	struct spi_ioc_transfer	xfer[2];
	unsigned char		buf[32], *bp;
	int			status;

	memset(xfer, 0, sizeof xfer);
	memset(buf, 0, sizeof buf);

	if (len > sizeof buf)
		len = sizeof buf;

	buf[0] = 0xaa;
	buf[1] = 0x0f;
	xfer[0].tx_buf = (unsigned long)buf;
	xfer[0].len = 2;

	xfer[1].rx_buf = (unsigned long) buf;
	xfer[1].len = len;

	status = ioctl(fd, SPI_IOC_MESSAGE(2), xfer);
	if (status < 0) {
		perror("SPI_IOC_MESSAGE");
		return;
	}

	printf("response(%2d, %2d): ", len, status);
	for (bp = buf; len; len--)
		printf(" %02x", *bp++);
	printf("\n");
}

#if 0
int main(int argc, char **argv)
{
	int		c;
	int		readcount = 0;
	int		msglen = 0;
	int		writelen = 0;
	unsigned char 		set=0;
	int		fd;
	unsigned int fre=0;
	const char	*name;

	while ((c = getopt(argc, argv, "hm:w:r:s:f:v")) != EOF) {
		switch (c) {
		case 'm':
			msglen = atoi(optarg);
			if (msglen < 0)
				goto usage;
			continue;
		case 'w':
			writelen = atoi(optarg);
			if (msglen < 0)
				goto usage;
			continue;
		case 'r':
			readcount = atoi(optarg);
			if (readcount < 0)
				goto usage;
			continue;
			
		case 's':
			set = atoi(optarg);
			if (set < 0)
				goto usage;
			continue;
		case 'f':
			fre = atoi(optarg);
			printf("fre=%s %d\n",optarg,fre);
			if (fre < 0)
				goto usage;
			continue;
		case 'v':
			verbose++;
			continue;
		case 'h':
		case '?':
usage:
			fprintf(stderr,
				"usage: %s [-h] [-m N] [-r N] [-s N] [-f N] /dev/spidevB.D\n",
				argv[0]);
			return 1;
		}
	}

	if ((optind + 1) != argc)
		goto usage;
	name = argv[optind];

	fd = open(name, O_RDWR);
	if (fd < 0) {
		perror("open");
		return 1;
	}
	if (set==0)//msb
		set_lsb(fd, 0);
	else//lsb
		set_lsb(fd, 1);

	set_speed(fd, 1000000);
	if (fre !=0 )//msb
		set_speed(fd, fre);
	printf("fre=%d\n",fre);
	
	dumpstat(name, fd);

	if (msglen)
		do_msg(fd, msglen);


	if (writelen)
		do_write(fd, writelen);

	if (readcount)
		do_read(fd, readcount);

	close(fd);
	return 0;
}
#endif