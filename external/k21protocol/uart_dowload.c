#include "devApi.h"

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <stdlib.h>
#include <sys/ioctl.h>
//#include "uart_com.h"
// #include "config_parse.h"

//#include "k21/test.h"
#include "devApi.h"
#define BUF_MAX_LEN 4096
#define NONE_DATA_FLAG BUF_MAX_LEN+1

#define MES_DOWNLOAD_SN 1

struct COMM_LOAD comm_load;
struct _COMM_FRAME comm_frame;
struct _SAVE_FILE ptest;
struct _SEND_DATA psend_COM;

u8 download_status[4] = {0};
static int fd_s0 = 0;
static int fd_hsl0 = 0;
static int fd_power = -1;
static int fd_multi = -1;


u8 avoid_stolen_status[5] = {0};
u8 BodyNumber_downloaded = 0;
u8 BodyNumber_Done_Read = 0;
#define power_path  "/sys/devices/soc.0/78b7000.spi/spi_master/spi0/spi0.0/spidev-k21/spidev-k21-0.0/scaner_power"
#define multi_uart_paht  "/sys/devices/soc.0/78b0000.serial/multi_uart"


void uart_comm_init();

int convert(unsigned char * str, int len)
{
	int i ,sum = 0;
	for(i = 0; i < len; i++){
		if(str[i]>='0' && str[i]<='9'){
			sum += sum*16 + str[i]-'0';
		
		}else if(str[i]<='f' && str[i]>='a'){
			sum += sum*16 + str[i]-'a'+1;
			
		}else if(str[i]<='F' && str[i]>='A'){
			sum += sum*16 + str[i]-'A'+1;
		}else {
			return -1;
		}
	}
	return sum;
}


int serial_opt_set(int fd,int nSpeed, int nBits, char nEvent, int nStop)
{
	struct termios newtio,oldtio;
	if  ( tcgetattr( fd,&oldtio)  !=  0) { 
		perror("SetupSerial 1");
		return -1;
	}
	bzero( &newtio, sizeof( newtio ) );
	newtio.c_cflag  |=  CLOCAL | CREAD; 
	newtio.c_cflag &= ~CSIZE; 

	switch( nBits ){
	case 7:
		newtio.c_cflag |= CS7;
		break;
	case 8:
		newtio.c_cflag |= CS8;
		break;
	}

	switch( nEvent ){
	case 'O':                     //��У��
		newtio.c_cflag |= PARENB;
		newtio.c_cflag |= PARODD;
		newtio.c_iflag |= (INPCK | ISTRIP);
		break;
	case 'E':                     //żУ��
	        newtio.c_iflag |= (INPCK | ISTRIP);
	        newtio.c_cflag |= PARENB;
	        newtio.c_cflag &= ~PARODD;
	        break;
	case 'N':                    //��У��
	        newtio.c_cflag &= ~PARENB;
	        break;
	}

	switch( nSpeed ){
	case 2400:
		cfsetispeed(&newtio, B2400);
		cfsetospeed(&newtio, B2400);
		break;
	case 4800:
		cfsetispeed(&newtio, B4800);
		cfsetospeed(&newtio, B4800);
		break;
	case 9600:
		cfsetispeed(&newtio, B9600);
		cfsetospeed(&newtio, B9600);
		break;
	case 115200:
		cfsetispeed(&newtio, B115200);
		cfsetospeed(&newtio, B115200);
		break;
	default:
		cfsetispeed(&newtio, B9600);
		cfsetospeed(&newtio, B9600);
		break;
	}
	if( nStop == 1 ){	
		newtio.c_cflag &=  ~CSTOPB;
		
	}else if ( nStop == 2 ){
		newtio.c_cflag |=  CSTOPB;
	}
	newtio.c_cc[VTIME]  = 0;
	newtio.c_cc[VMIN] = 0;
	tcflush(fd,TCIFLUSH);
	
	if((tcsetattr(fd,TCSANOW,&newtio))!=0){
		perror("com set error");
		return -1;
	}
	printf("set done!\n");
	return 0;
}

int close_port(void)
{
	if(fd_s0 != 0){
		close(fd_s0);
		fd_s0 = 0;
	}
	return 0;
}



int open_port(void)
{
	int i,ret;
	fd_s0 = open("/dev/ttyS0",O_RDWR|O_NOCTTY|O_NDELAY|O_NONBLOCK); 
	if(fd_s0 < 0){
		printf("open ttyS0 failed\n");
		return -1;	
	}
	ret = sys_com_usebyconsole();
	if(0!=ret)
	    return ret;
	if((i=serial_opt_set(fd_s0,115200,8,'N',1))<0){
		perror("serial_opt_set error");
		goto SET_OPT_ERR;
	}
	//printf("fd-open=%d\n",fd);
	uart_comm_init();  //000
    	return fd_s0;
		
SET_OPT_ERR:
	
	close_port();
	return -1;
	
}



int poweron_scanner(void) {
	
	fd_power= open(power_path, O_RDWR | O_NONBLOCK);
	if (fd_power < 0) {
		printf("open %s failed !\n", power_path);
		return -1;
	}
	fd_multi= open(multi_uart_paht, O_RDWR | O_NONBLOCK);
	if (fd_power < 0) {
		printf("open %s failed !\n", multi_uart_paht);
		close(fd_power);
		return -1;
	} 

	write(fd_power, "1", 1);
	write (fd_multi, "1", 1);
	return 0;
} 

int poweroff_scanner() {
	if (fd_power > 0 && fd_multi > 0) {
		write(fd_power, "0", 1);
		write (fd_multi, "0", 1);
		close(fd_power);
		close(fd_multi);
	}
	return 0;
}


int open_serialport(void)
{
	int i,ret;
	fd_hsl0 = open("/dev/ttyHSL0", O_RDWR|O_NOCTTY|O_NDELAY|O_NONBLOCK); 
	if(fd_hsl0 < 0){
		printf("open ttyHSL0 failed\n");
		return -1;	
	}

	if((i=serial_opt_set(fd_hsl0,115200,8,'N',1))<0){
		perror("serial_opt_set error");
		goto SET_OPT_ERR;
	}

    return fd_hsl0;
		
SET_OPT_ERR:
	
	close_serialport();
	return -1;
	
}

int close_serialport(void)
{
	if(fd_hsl0 != 0){
		close(fd_hsl0);
		fd_hsl0 = 0;
	}
	return 0;
}


int read_serial_data(char * buffer, int len)
{
	int nread;	
	nread = read(fd_hsl0, buffer, len);
	if(nread>= 0){
		return nread; 
	}
	return -1;	
}

int write_serial_data(char * buffer, int len)
{
	int nwrite;
	nwrite = write(fd_hsl0, buffer, len);
	if(nwrite >= 0)
		return nwrite;
	else
		return -1;
}





void process_done(void)
{
	close_port();
}



int serial_read_nb(int fd, char * buffer, int len)
{
	int nread;	
	nread = read(fd,buffer,len);
	if(nread>= 0){
		return nread; 
	}
	return -1;	
}

int serial_write_nb(int fd, char * buffer, int len)
{
	int nwrite;
	nwrite = write(fd, buffer, len);
	if(nwrite >= 0)
		return nwrite;
	else
		return -1;
}

void uart_comm_init()
{		
	comm_load.rev_frame_len=0;
	comm_load.frame_head_num=0;
	comm_load.timeover=0;
	comm_load.frame_send_len=0;
	comm_load.frame_send_bytes=0;
}

void strage_framedata(u32 start,u32 end)
{	
	u32 i;
	for(i=start;i<end;i++){
	       printf("[%02x]\n",comm_load.rev_buffer[i]);
		comm_load.rev_frame[comm_load.rev_frame_len]=comm_load.rev_buffer[i];
		comm_load.rev_frame_len++;
	}
}

//BccУ��
u8 CheckFrameBCC(int cmd_len)
{
	u8 checksum_read =0;
	u32 i=0;
    
	for(i=0;i<comm_frame.len+cmd_len -1;i++){
		checksum_read = checksum_read + comm_load.rev_frame[i];
	}
	if(checksum_read == comm_load.rev_frame[comm_frame.len+cmd_len -1]){
		return 1;
	}else{
		return 0;
	}
}


#ifdef MES_DOWNLOAD_SN

u8 BodyNumber_CheckFrameBCC(int cmd_len)
{
	u8 checksum_read =0;
	u32 i=0;
    
	for(i=0;i<comm_frame.len+cmd_len -1;i++){
		checksum_read = checksum_read + comm_load.rev_frame[i];
	}
	if(checksum_read == comm_load.rev_frame[comm_frame.len+cmd_len -1]){
		return 1;
	}else{
		return 0;
	}
}

#else

u8 BodyNumber_CheckFrameBCC(void)
{
	u8 checksum_read =0;
	u32 i=0;
    
	for(i=0;i<comm_frame.len+5;i++){
		checksum_read = checksum_read + comm_load.rev_frame[i];
	}
	if(checksum_read == comm_load.rev_frame[comm_frame.len+5]){
		return 1;
	}else{
		return 0;
	}
}

#endif

void write_send_frame(u8 cmd,u8 *send_buf,u32 len)
{
	u32 i=0,j=0;
	u8 checksum=0;

	comm_load.send_frame[0] = 0x01;
	comm_load.send_frame[1] = 0x01;
	comm_load.send_frame[2] = 0x01;
	comm_load.send_frame[3] = 0x01;
	comm_load.send_frame[4] = 0x01;
	comm_load.send_frame[5] = 0x45;
	comm_load.send_frame[6] = cmd;
	comm_load.send_frame[7] = (len>>8)&0x00FF;
	comm_load.send_frame[8] = len&0x00FF;
	for(i=0;i<len;i++){
		comm_load.send_frame[9+i] = send_buf[i];
	}
	comm_load.send_frame[i+9] = 0x03 ;
	for(j=4;j<(len+10);j++){
		checksum += comm_load.send_frame[j];
	}
	comm_load.send_frame[i+10] = checksum ;
	comm_load.frame_send_len = j+1;
	comm_load.rev_frame_len=0;	
	comm_frame.checksum = 0;
}

#ifdef MES_DOWNLOAD_SN
void BodyNumber_write_send_frame(u8 cmd,u8 *send_buf,u32 len)
{
	u32 i=0,j=0;
	u8 checksum=0;

	comm_load.send_frame[0] = 0x01;
	comm_load.send_frame[1] = 0x01;
	comm_load.send_frame[2] = 0x01;
	comm_load.send_frame[3] = 0x01;
	comm_load.send_frame[4] = 0x12;
	comm_load.send_frame[5] = 0x9f;
	comm_load.send_frame[6] = len;
	if(send_buf  != NULL ){
		for(i=0;i<len;i++){
			comm_load.send_frame[7+i] = send_buf[i];
		}
	}else{
		i = 0;
	}
	comm_load.send_frame[i+7] = 0x03 ;
	for(j=3;j<(len+8);j++){
		checksum += comm_load.send_frame[j];
	}
	comm_load.send_frame[i+8] = checksum ;
	comm_load.frame_send_len = j+1;
	comm_load.rev_frame_len=0;	
	comm_frame.checksum = 0;
}

#else
#if 0
void BodyNumber_write_send_frame(u8 cmd,u8 *send_buf,u32 len)
{
	u32 i=0,j=0;
	u8 checksum=0;

	comm_load.send_frame[0] = 0x01;
	comm_load.send_frame[1] = 0x01;
	comm_load.send_frame[2] = 0x01;
	comm_load.send_frame[3] = 0x01;
	//comm_load.send_frame[4] = 0x12;
	comm_load.send_frame[4] = 0x10;
	comm_load.send_frame[5] = cmd;
	comm_load.send_frame[6] = len;
	for(i=0;i<len;i++){
		comm_load.send_frame[7+i] = send_buf[i];
	}
	comm_load.send_frame[i+7] = 0x03 ;
	for(j=3;j<(len+8);j++){
		checksum += comm_load.send_frame[j];
	}
	comm_load.send_frame[i+8] = checksum ;
	comm_load.frame_send_len = j+1;
	comm_load.rev_frame_len=0;	
	comm_frame.checksum = 0;
}
#endif
#endif

void dev_ini_download(void)
{
	u32 i=0;
	u8 send_buf[1]={0x01};
	
	download_status[3] = 0x01;
	ptest.recv_flag.ini_flag = 0x01;
	ptest.ini_len = comm_frame.len;
	ptest.recv_flag.ini_file_checksum = comm_frame.checksum;//checksum_read;
	strncpy(ptest.ini_data,comm_frame.data,comm_frame.len);
	
   	write_send_frame(comm_frame.command,send_buf,sizeof(send_buf));
}

void dev_rtc_download(void)
{
#if 0
	struct rtc_time tm;
	struct rtc_time tmp;
#endif
	u32 i=0;
	u8 send_buf[1]={0x01};
	u8 send_buf1[1]= {0xff};
	u8 write_buf[20] = {0};
	u8 test_buf[20] ={0};
 #if 0
	for(i=0;i<comm_frame.len;i++){
   	   write_buf[i] = comm_frame.data[i];
   	}
   	memset(&tm,0,sizeof(tm));
   	tm.tm_year = (write_buf[0]-'0')*1000+(write_buf[1]-'0')*100+(write_buf[2]-'0')*10+(write_buf[3]-'0')*1;
   	tm.tm_mon = (write_buf[4]-'0')*10+(write_buf[5]-'0')*1;
   	tm.tm_mday = (write_buf[6]-'0')*10+(write_buf[7]-'0')*1;
   	tm.tm_hour = (write_buf[8]-'0')*10+(write_buf[9]-'0')*1;
   	tm.tm_min = (write_buf[10]-'0')*10+(write_buf[11]-'0')*1;
   	tm.tm_sec = (write_buf[12]-'0')*10+(write_buf[13]-'0')*1;
 #endif 
	
	download_status[2] = 0x01;
	write_send_frame(comm_frame.command,send_buf,sizeof(send_buf));

    //	rtcset(&tm);   ->��ʵ��
}

void dev_testerid_download(void)
{
	u32 i=0;
	u8 send_buf[1]={0x01};
    
	download_status[1] = 0x01;
	ptest.recv_flag.testid_flag = 0x01;
	ptest.testid_len = comm_frame.len;
	strncpy(ptest.testid_data,comm_frame.data,comm_frame.len);
	ptest.recv_flag.testid_checksum = comm_frame.checksum;//checksum_read;

	write_send_frame(comm_frame.command,send_buf,sizeof(send_buf));
}

void dev_pid_download(void)
{
	u32 i=0;
	u8 send_buf[1]={0x01};

	download_status[0] = 0x01;
	ptest.recv_flag.pid_flag = 0x01;
	ptest.pid_len = comm_frame.len;
	strncpy(ptest.pid_data,comm_frame.data,comm_frame.len);
	ptest.recv_flag.pid_checksum = comm_frame.checksum;//checksum_read;
   	
   	write_send_frame(comm_frame.command,send_buf,sizeof(send_buf));
}

void dev_com_recv(void)
{
	psend_COM.send_len= comm_frame.len;
	strncpy(psend_COM.send_data,comm_frame.data,comm_frame.len);
	psend_COM.ready = 1;
	printf("the psend_COM.send_data is :%s\r\n",psend_COM.send_data);
	printf("the psend_COM.ready is:%d\r\n",psend_COM.ready);
}

void fuse_version_handler(void)
{
	unsigned char  send_buf[2]={'V','2'};
	
	avoid_stolen_status[0] = 0x01;
	
   	write_send_frame(comm_frame.command,send_buf,sizeof(send_buf));
}

void cpuid_handler(void)
{
	int ret = 0,slen = 0;
	unsigned char get_buf[256];
	unsigned char  send_buf[512];

	memset(get_buf,0,sizeof(get_buf));
	ret = sys_getAt83ID(get_buf);
	if(ret > 0){
		slen = ret;
		memcpy(send_buf,get_buf,slen);

		memset(get_buf,0,sizeof(get_buf));
		ret = sys_getEmmcID(get_buf);
		if(ret > 0){
		
			memcpy(send_buf+slen,get_buf,ret);
			printf("KEVIN_Debug: cpuid = %s, cpuid_len = %d \n", send_buf, strlen(send_buf));

			avoid_stolen_status[1] = 0x01;
			
			//write_send_frame(comm_frame.command,send_buf,strlen(send_buf));
			write_send_frame(comm_frame.command,send_buf,slen + ret);
		}
	}else {
		//
	}
}

void flashid_handler(void)
{
	int ret = 0;
	unsigned char get_buf[BUF_MAX_LEN];
	unsigned char  send_buf[BUF_MAX_LEN];
	
	memset(get_buf,0,sizeof(get_buf));
	memset(send_buf,0,sizeof(send_buf));
	
	ret = ddi_sys_get_chipID(get_buf);
	if(ret == 0){
		printf("KEVIN_Debug: flashid = %s, flashid_len = %d \n", get_buf, strlen(get_buf));
		
		memcpy(send_buf,get_buf,(16+256));
		memcpy(send_buf+(16+256),"G870S",5);

		avoid_stolen_status[2] = 0x01;
		
		write_send_frame(comm_frame.command,send_buf,(16+256+5));
	}else {
		//
		write_send_frame(comm_frame.command,send_buf,(16+256+5));
	}
}

void encrypted_info_handler(void)
{
	u32 ret = 0;
	u8 send_buf1[1]={0x00};
	u8 send_buf2[1]={0x01};
	//unsigned char encrypted_info[BUF_MAX_LEN];

	//strncpy(encrypted_info,comm_frame.data,comm_frame.len);

	ret = spi_ddi_set_preventCut(comm_frame.data, comm_frame.len);
	if(ret == 0){

		avoid_stolen_status[3] = 0x01;
		
		write_send_frame(comm_frame.command,send_buf1,sizeof(send_buf1));
	}else{
		write_send_frame(comm_frame.command,send_buf2,sizeof(send_buf2));
	}
}

int judge_rage(unsigned char c){

	if(c >= 0x0 && c <= 0x0f)
		return 1;
	else
		return 0;
}

void macid_handler(void)
{
	u32 ret = 0;
	u8 send_buf1[1]={0x00};
	u8 send_buf2[1]={0x01};
	unsigned char  temp_buf[6];
	int i = 0,val  = 0;
	
	//k21_run_in_core(0,temp_buf);
	//usleep(1000*1000);
	//usleep(1000*1000);


	if(comm_frame.len == 12){
		memset(temp_buf,0,6);
		for(i = 0; i < 12; i = i+2){
		#if 0
			val = convert(&comm_frame.data[i], 2);
			if(val < 0){
				write_send_frame(comm_frame.command,send_buf2,sizeof(send_buf2));
				return;
				
			}else{
				temp_buf[i/2] = (unsigned char)val;
				val = 0;
			}
		#else
			if(judge_rage(comm_frame.data[i]) && judge_rage(comm_frame.data[i+1])){
				temp_buf[i/2] = comm_frame.data[i]*16 + comm_frame.data[i+1];
				
			}else{
				write_send_frame(comm_frame.command,send_buf2,sizeof(send_buf2));
				return;
			}
		#endif
		}
		
		ret = ddi_sys_setBTMAC(temp_buf,6);
		if(ret == 0){
			avoid_stolen_status[4] = 0x01;
			
			write_send_frame(comm_frame.command,send_buf1,sizeof(send_buf1));
			
		}else{
		
			write_send_frame(comm_frame.command,send_buf2,sizeof(send_buf2));
		}
	}else{
	
		write_send_frame(comm_frame.command,send_buf2,sizeof(send_buf2));
	}
}

#ifdef MES_DOWNLOAD_SN

void BodyNumber_download(void)
{
	u32 ret = 0;
	u8 send_buf[16]={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	u8 send_buf2[16]={0x03,0x03,0x03,0x04,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03};
	u8 send_buf3[16]={0x06,0x07,0x08,0x05,0x04,0x02,0x07,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03};
	
	ret = spi_ddi_sys_set_dsn(comm_frame.len, comm_frame.data);
	if(ret == 0){
		
		ret = spi_ddi_sys_read_dsn(send_buf);
		if(ret == -1){
			BodyNumber_write_send_frame(comm_frame.command,send_buf3,16);
			
		}else{
			BodyNumber_downloaded = 1;		
			BodyNumber_write_send_frame(comm_frame.command,send_buf,16);
		}
	}else{
		BodyNumber_write_send_frame(comm_frame.command,send_buf2,16);
	}
}

void BodyNumber_read(void)
{
	u32 ret = 0;
	u8 send_buf[16]={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	u8 send_buf2[16]={0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04};
	
	ret = spi_ddi_sys_read_dsn(send_buf);
	if(ret == -1){
		BodyNumber_write_send_frame(comm_frame.command,send_buf2,16);
		
	}else{
		BodyNumber_Done_Read = 1;
		BodyNumber_write_send_frame(comm_frame.command,send_buf,16);
	}

}

#else

void BodyNumber_download(void)
{
	u32 ret = 0;
	u8 send_buf1[1]={0x00};
	u8 send_buf2[1]={0x01};
//       printf("BodyNumber_download\n");
//       printf("comm_frame.data[15] = %02x\n",comm_frame.data[15]);
//        printf("comm_frame.data[16] = %02x\n",comm_frame.data[16]);
//        printf("comm_frame.data[17] = %02x\n",comm_frame.data[17]);
//       ret = spi_ddi_sys_set_dsn(comm_frame.data[16], &comm_frame.data[17]);
	ret = spi_ddi_sys_set_dsn(comm_frame.len, comm_frame.data);
	if(ret == 0){
		BodyNumber_downloaded = 1;
		BodyNumber_write_send_frame(comm_frame.command,send_buf1,sizeof(send_buf1));
	}else{
		BodyNumber_write_send_frame(comm_frame.command,send_buf2,sizeof(send_buf2));
	}
}
#endif

void command_process(void)
{
	switch(comm_frame.command){
	case PID_LOAD:	 
    	   	dev_pid_download();
    	    break;

    	case TESTER_LOAD:	
		dev_testerid_download();
    	    	break;

	case RTC_LOAD:
    	   	dev_rtc_download();
    	    	break;
    	    
    	case INI_LOAD:	
		dev_ini_download();
		break;

    	case 0x35:
		dev_com_recv();
		break;

	case QUERY_FUSE_VERSION:
		fuse_version_handler();
		
		break;
		
	case QUERY_CPUID:
		cpuid_handler();
		break;
		
	case QUERY_FLASHID:
		flashid_handler();
		break;
		
	case ENCRYPTED_INFO_LOAD:
		encrypted_info_handler();
		break;
		
	case MACID_LOAD:
		macid_handler();
		break;

	default:
		break;
	}
	comm_frame.ready = 0;
}


/*
type:
1=>download config files
2=>avoid stolen related
*/
void uart_comm(int type)
{	
	u32 n,m,i;
	u8 send_buf1[1]={0x02};
	u8 send_buf2[1]={0x01};
	
	if(fd_s0 != 0){    
		n=serial_read_nb(fd_s0, comm_load.rev_buffer, 64);		//��ѯ��ȡUART FIFO����,64 Bytes FIFO for  A83T
		if(n>0){	
			if(comm_load.rev_frame_len==0){		//ʶ��ͬ��ͷ
				for(i=0;i<n;i++){  
					if(comm_load.rev_buffer[i]==0x01) {
						comm_load.frame_head_num++;
						
					}else if(comm_load.frame_head_num>=3){  	//��Ч����֡���ֽ�
					  
						comm_load.rev_frame[0]=0x01;		//֡ͷҲ����У��
						comm_load.rev_frame_len=1;
						strage_framedata(i,n);
						comm_load.frame_head_num=0;
						comm_load.timeover=1;			//������ʱ������
						//============
					//	comm_frame.len=0;
						comm_frame.len = NONE_DATA_FLAG;
						comm_frame.ready=0;
						break;
					}else {
				    		comm_load.frame_head_num=0;
					}
				}   
			}else{		//֡��Ч����	
				
				strage_framedata(0,n);
			}	
		}
	}
	if(comm_load.timeover){
		comm_load.timeover++; //��ʱ
		//if(comm_load.rev_frame_len>=5&&comm_frame.len==0){	//ȡ�����ֽ�
		if(comm_load.rev_frame_len>=5&&comm_frame.len==NONE_DATA_FLAG){	//ȡ�����ֽ�
			comm_frame.flag=comm_load.rev_frame[1];
			comm_frame.command=comm_load.rev_frame[2];
			comm_frame.len=comm_load.rev_frame[3]*0x100+comm_load.rev_frame[4];
			
		}else if(comm_load.rev_frame_len>=(comm_frame.len+7))	{	//�ж�֡�����Ƿ�����

			if(CheckFrameBCC(7)){	//У����ȷ,֡�������ɣ�
				for(i=0;i<comm_frame.len;i++){
					comm_frame.data[i]=comm_load.rev_frame[5+i];
					comm_frame.checksum += comm_frame.data[i];
				} 
				comm_load.timeover=0;
				comm_frame.ready=1;	//֡����
			}else{	//֡����
				
				comm_load.timeover=0;
				comm_load.rev_frame_len=0;
				comm_load.frame_send_bytes=0;
    				//checksum_read = 0;    
    				if(type == 1){
    					write_send_frame(comm_frame.command,send_buf1,sizeof(send_buf1));
				}else if(type == 2){
					write_send_frame(comm_frame.command,send_buf2,sizeof(send_buf2));
				}
			}
		}
    }
	if(comm_load.timeover>FTAME_TIMEOVER){	//���ճ�ʱ
		
	    comm_load.timeover = 0;
		comm_load.rev_frame_len=0;		//��֡����
		comm_load.frame_head_num=0;
	}else if(comm_frame.ready) {
		command_process();	//�����
	}
	//---------------------------------------------------------
	//����֡����
	if(comm_load.frame_send_len){	//�����ݷ���
		
		m = comm_load.frame_send_len - comm_load.frame_send_bytes;
		if(m>64)
			m = 64;		//64 Bytes FIFO for A83T
		if(fd_s0 != 0){
			n= serial_write_nb(fd_s0, &comm_load.send_frame[comm_load.frame_send_bytes], m);	//д���ݵ�UART ���� 
			comm_load.frame_send_bytes +=n;
			if(comm_load.frame_send_bytes>=comm_load.frame_send_len){
				comm_load.frame_send_bytes=0;
				comm_load.frame_send_len=0;
			}
		}
	}

}

//======add for download BodyNumber,1217=======

#ifdef MES_DOWNLOAD_SN
void BodyNumber_uart_comm(void)
{	
	u32 n,m,i;
	u8 send_buf1[16]={0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01};
	u8 send_buf2[16]={0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02};
	
	if(fd_s0 != 0){    
		n=serial_read_nb(fd_s0, comm_load.rev_buffer, 64);		//��ѯ��ȡUART FIFO����,64 Bytes FIFO for  A83T
		if(n>0){	
			if(comm_load.rev_frame_len==0){		//ʶ��ͬ��ͷ
				for(i=0;i<n;i++){  
					if(comm_load.rev_buffer[i]==0x01) {
						comm_load.frame_head_num++;
						
					}else if(comm_load.frame_head_num>=3){  	//��Ч����֡���ֽ�
					  
						comm_load.rev_frame[0]=0x01;	
						comm_load.rev_frame_len=1;
						#if 1
						if(comm_load.rev_buffer[i] == 0x14){
							n = 14;
						}
						#endif
						
						strage_framedata(i,n);
						comm_load.frame_head_num=0;
						comm_load.timeover=1;			//������ʱ������
						//============
					//	comm_frame.len=0;
						comm_frame.len = NONE_DATA_FLAG;
						comm_frame.ready=0;
						break;
					}else {
				    		comm_load.frame_head_num=0;
					}
				}   
			}else{		//֡��Ч����	
				
				strage_framedata(0,n);
			}	
		}
	}
	if(comm_load.timeover){
		comm_load.timeover++; //��ʱ
		
		if ((comm_load.rev_frame_len>=4) &&(comm_load.rev_frame[2] == BODY_NUMBER_LOAD)){
			if(comm_frame.len==NONE_DATA_FLAG){	//ȡ�����ֽ�
					comm_frame.flag=comm_load.rev_frame[1];
					comm_frame.command=comm_load.rev_frame[2];
					comm_frame.len=comm_load.rev_frame[3];		
					
			}else if(comm_load.rev_frame_len>=(comm_frame.len+6))	{	//�ж�֡�����Ƿ�����

				if(CheckFrameBCC(6)){	//У����ȷ,֡�������ɣ�
					for(i=0;i<comm_frame.len;i++){
						comm_frame.data[i]=comm_load.rev_frame[4+i];
						comm_frame.checksum += comm_frame.data[i];
					} 
					comm_load.timeover=0;
					comm_frame.ready=1;	//֡����
				}else{	//֡����
					
					comm_load.timeover=0;
					comm_load.rev_frame_len=0;
					comm_load.frame_send_bytes=0;
	    				//checksum_read = 0;    
	    				
	    				BodyNumber_write_send_frame(comm_frame.command,send_buf2,16);
				}
			}
	     } else  if((comm_load.rev_frame_len>=5)&&(comm_load.rev_frame[2] == BODY_NUMBER_READ)){

			if(comm_frame.len == NONE_DATA_FLAG){	//ȡ�����ֽ�
				comm_frame.flag=comm_load.rev_frame[1];
				comm_frame.command=comm_load.rev_frame[2];
				comm_frame.len=comm_load.rev_frame[3]*0x100+comm_load.rev_frame[4];	
				
			}else if(comm_load.rev_frame_len>=(comm_frame.len+7)){	//�ж�֡�����Ƿ�����
				
				if(CheckFrameBCC(7)){	//У����ȷ,֡�������ɣ�
					for(i=0;i<comm_frame.len;i++){
						comm_frame.data[i]=comm_load.rev_frame[5+i];
						comm_frame.checksum += comm_frame.data[i];
					} 
					comm_load.timeover=0;
					comm_frame.ready=1;	//֡����
					
				}else {//֡����
					
					comm_load.timeover=0;
					comm_load.rev_frame_len=0;
					comm_load.frame_send_bytes=0;
	    				//checksum_read = 0;    
	    				
	    				BodyNumber_write_send_frame(comm_frame.command,send_buf1,16);

				}
			}
	     	}
	}
	if(comm_load.timeover>FTAME_TIMEOVER){	//���ճ�ʱ
		
	    comm_load.timeover = 0;
		comm_load.rev_frame_len=0;		//��֡����
		comm_load.frame_head_num=0;
	}else if(comm_frame.ready) {
		if(comm_frame.command == BODY_NUMBER_LOAD){
			BodyNumber_download();
		}else if (( comm_frame.command ==  BODY_NUMBER_READ) && (comm_frame.len >=4)){
			
			if((comm_frame.data[0] == 0x01) &&(comm_frame.data[1] == 0x16)&&(comm_frame.data[2] == 0x0)&&(comm_frame.data[3] == 0xf0))
				BodyNumber_read();
		}
		comm_frame.ready = 0;
	}
	//---------------------------------------------------------
	//����֡����
	if(comm_load.frame_send_len){	//�����ݷ���
		
		m = comm_load.frame_send_len - comm_load.frame_send_bytes;
		if(m>64)
			m = 64;		//64 Bytes FIFO for A83T
		if(fd_s0 != 0){
			n= serial_write_nb(fd_s0, &comm_load.send_frame[comm_load.frame_send_bytes], m);	//д���ݵ�UART ���� 
			comm_load.frame_send_bytes +=n;
			if(comm_load.frame_send_bytes>=comm_load.frame_send_len){
				comm_load.frame_send_bytes=0;
				comm_load.frame_send_len=0;
			}
		}
	}

}

#else
#if 0
void BodyNumber_uart_comm(void)
{	
	u32 n,m,i;
	u8 send_buf1[1]={0x00};
	u8 send_buf2[1]={0x01};
	int flag = 0;
	
	if(fd_s0 != 0){    
		n=serial_read_nb(fd_s0, comm_load.rev_buffer, 64);		//��ѯ��ȡUART FIFO����,64 Bytes FIFO for  A83T
		if(n>0){	
		        printf("read\n");
			if(comm_load.rev_frame_len==0){		//ʶ��ͬ��ͷ
				for(i=0;i<n;i++){  
					if(comm_load.rev_buffer[i]==0x01) {
						comm_load.frame_head_num++;
						
					}else if(comm_load.frame_head_num>=3){  	//��Ч����֡���ֽ�
					    if (flag == 0){
                                            flag = 1;
                                            i +=17;
					        }
						comm_load.rev_frame[0]=0x01;		//֡ͷҲ����У��
						comm_load.rev_frame_len=1;
						strage_framedata(i,n);
						comm_load.frame_head_num=0;
						comm_load.timeover=1;			//������ʱ������
						//============
					//	comm_frame.len=0;
						comm_frame.len = NONE_DATA_FLAG;
						comm_frame.ready=0;
						break;
					}else {
				    		comm_load.frame_head_num=0;
					}
				}   
			}else{		//֡��Ч����	
				
				strage_framedata(0,n);
			}	
		}
	}
	if(comm_load.timeover){
//	        printf("(comm_load.timeover)\n");
		comm_load.timeover++; //��ʱ
		//if(comm_load.rev_frame_len>=5&&comm_frame.len==0){	
		if(comm_load.rev_frame_len>=5&&comm_frame.len==NONE_DATA_FLAG){	//ȡ�����ֽ�
			comm_frame.flag=comm_load.rev_frame[1];
			comm_frame.command=comm_load.rev_frame[2];
			
			/*Difference:  
						2 bytes length for Config_download/AvoidStlone_Download protocal; 
						only 1 byte length for BodyNumber_Download protocal  
			*/
			comm_frame.len=comm_load.rev_frame[3];		
			
			
		}else if(comm_load.rev_frame_len>=(comm_frame.len+6))	{	//�ж�֡�����Ƿ�����

			if(CheckFrameBCC(6)){	//У����ȷ,֡�������ɣ�
				for(i=0;i<comm_frame.len;i++){
					comm_frame.data[i]=comm_load.rev_frame[4+i];
					comm_frame.checksum += comm_frame.data[i];
				} 
				comm_load.timeover=0;
				comm_frame.ready=1;	//֡����
			}else{	//֡����
				
				comm_load.timeover=0;
				comm_load.rev_frame_len=0;
				comm_load.frame_send_bytes=0;
    				//checksum_read = 0;    
    				printf("BodyNumber_CheckFrameBCC err\n");
    				BodyNumber_write_send_frame(comm_frame.command,send_buf2,sizeof(send_buf2));

			}
		}
    }
	if(comm_load.timeover>FTAME_TIMEOVER){	//���ճ�ʱ
		
	    comm_load.timeover = 0;
		comm_load.rev_frame_len=0;		//��֡����
		comm_load.frame_head_num=0;
	}else if(comm_frame.ready) {
	    printf("(comm_frame.ready),comm_frame.command = %02x\n",comm_frame.command);

//	      if(comm_frame.command == 0x50)
		if(comm_frame.command == BODY_NUMBER_LOAD)
		{
		printf("(comm_frame.command == BODY_NUMBER_LOAD)\n");
			BodyNumber_download();
		}
		comm_frame.ready = 0;
	}
	//---------------------------------------------------------
	//����֡����
	if(comm_load.frame_send_len){	//�����ݷ���
		
		m = comm_load.frame_send_len - comm_load.frame_send_bytes;
		if(m>64)
			m = 64;		//64 Bytes FIFO for A83T
		if(fd_s0 != 0){
			n= serial_write_nb(fd_s0, &comm_load.send_frame[comm_load.frame_send_bytes], m);	//д���ݵ�UART ���� 
			comm_load.frame_send_bytes +=n;
			if(comm_load.frame_send_bytes>=comm_load.frame_send_len){
				comm_load.frame_send_bytes=0;
				comm_load.frame_send_len=0;
			}
		}
	}

}
#endif
#endif

int BodyNumber_test(void)
{
	int nread,i, ret=-1,time = 0;
	int cnt = 100;
	printf("BodyNumber_test\r\n");
      ret = open_port();
      printf("open_port = ret%d \r\n",ret);
	while(1)
	{
		ret = BodyNumber_process_download();
		if(ret==0){
			break;
		}
		usleep(10*1000);
            time++;
            if(time>=3000)
                break;
	}

    	close_port();

	return ret;
}
int BodyNumber_process_download(void)
{
	int nread,i, ret=-1,time = 0;
	int cnt = 100;
	
//	ret = open_port();
//	if(ret < 0){
//		printf("open_port failed\n");
//		return -1;
//	}
//      printf("open_port success\n");
//	ret = k21_init();
//	if(ret != 0){
//		printf("k21_init failed\n");
//		return -2;
//	}

	ret = -1;
//	uart_comm_init();
//	while(1)
	{
	       
		BodyNumber_uart_comm();
		if(1==BodyNumber_downloaded){
			ret = 0;
		      BodyNumber_downloaded = 0;
//			break;
		}
//		usleep(10*1000);
//            time++;
//            if(time>=3)
//                break;
	}

//	k21_deinit();
//    	close_port();

	return ret;
}


int BodyNumber_process_serial_read(void)
{
	int nread,i, ret=-1,time = 0;
	int cnt = 100;
	
	ret = -1;
	       
	BodyNumber_uart_comm();
	if(1==BodyNumber_Done_Read){
		ret = 0;
		BodyNumber_Done_Read = 0;

	}

	return ret;
}


//================end=================

int download_process(void)
{
	int nread,i, ret;
	int cnt = 100;
	
	ret = open_port();
	if(ret < 0){
		return -1;
	}

	/*
	ret = open_config();
	if(ret < 0){
		close_port();
		return -2;
	}
	*/
//	system("mkdir /tmp/udisk/");
//	system("mount /dev/sda4 /tmp/udisk/");
	uart_comm_init();
	while(1){
		uart_comm(1);
		if((download_status[0]==0x01)&&(download_status[1]==0x01)&&(download_status[2]==0x01)&&(download_status[3]==0x01)){
		//	write_config();
			break;
		}
		usleep(10*1000);
	}
  //    system("unmount  /tmp/udisk/");
    	process_done();
		
//	if( get_ptecfg_status())
//		return 0;


	return -3;
}

int avoid_stolen_process(void)
{
	int nread,i, ret=-1;
	int cnt = 100;
	
	ret = open_port();
	if(ret < 0){
		printf("open_port failed\n");
		return -1;
	}

//	ret = k21_init();
//	if(ret != 0){
//		printf("k21_init failed\n");
//		return -2;
//	}

	ret = -1;
	uart_comm_init();
	while(1){
		//system("mount /dev/sda4 /tmp/udisk/");
		uart_comm(2);
		if((avoid_stolen_status[0]==0x01)&&(avoid_stolen_status[1]==0x01)&&(avoid_stolen_status[2]==0x01)&&(avoid_stolen_status[3]==0x01)&&(avoid_stolen_status[4]==0x01)){
			//write_config();
			ret = 0;
			break;
		}
		usleep(10*1000);
		 //system("unmount  /tmp/udisk/");
	}

//	k21_deinit();
    	close_port();

	return ret;
}



