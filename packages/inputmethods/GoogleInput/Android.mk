LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-subdir-java-files)
LOCAL_MODULE  := GoogleInput
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk

LOCAL_PREBUILT_JNI_LIBS := \
    @lib/armeabi-v7a/liben_data_bundle.so \
    @lib/armeabi-v7a/libgnustl_shared.so \
	@lib/armeabi-v7a/libhmm_gesture_hwr_zh.so  \
    @lib/armeabi-v7a/libhwrword.so  \
    @lib/armeabi-v7a/libpinyin_data_bundle.so 
LOCAL_MULTILIB :=32 
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_CERTIFICATE := platform
#LOCAL_MODULE_PATH := $(TARGET_OUT_DATA_APPS)
include $(BUILD_PREBUILT)
