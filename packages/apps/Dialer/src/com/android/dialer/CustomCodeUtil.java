/**
  * @author David
  * @date 2013-12-04
  * @version V1.0
  */

package com.android.dialer;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class CustomCodeUtil {

    public static final String TAG = CustomCodeUtil.class.getSimpleName();

    private static final boolean DEBUG = false;

    private static final String SEPARATOR = ":";

    private static final String ACTIVITY_KEY = "activity_custom_codes";
    private static final String RECEIVER_KEY = "receiver_custom_codes";

    private static final String CUSTOM_CODE_PREFIX = "*#";
    private static final String CUSTOM_CODE_POSTFIX = "#";
    private static final String CUSTOM_CODE_ACTION = "intent.action.CUSTOM_CODE";
    private static final String CUSTOM_CODE_MIMETYPE = "code";
    private static final int CUSTOM_CODES_MIN_LENGTH = 2;
    private static final String MMI_PACKAGE_NAME = "cn.gosomo.evt";

    private static ArrayList<String> mPackageList = new ArrayList<String>();
    private static HashMap<String, String> mActivityCodes = new HashMap<String, String>();
    private static HashMap<String, String> mReceiverCodes = new HashMap<String, String>();

    public static void initCustomCodes(Context context) {
        if (mActivityCodes.size() > 0 || mReceiverCodes.size() > 0) {
            return;
        }

        autoFindCustomCodes(context);
        //loadMmiCustomCodes(context); //Only for MMI test

        //Get the array of activity_custom_codes and receiver_custom_codes from xml
        for (String packageName : mPackageList) {
            Context extraContext;
            try {
                extraContext = context.createPackageContext(packageName,
                        Context.CONTEXT_IGNORE_SECURITY);
            } catch (NameNotFoundException e) {
                e.printStackTrace();
                continue;
            }
            Resources res = extraContext.getResources();
            int activityCodesId = res.getIdentifier(ACTIVITY_KEY, "array", packageName);
            if (activityCodesId != 0) {
                String[] codes = res.getStringArray(activityCodesId);
                for (String item : codes) {
                    tryToPutCode(context, item, mActivityCodes);
                }
            }
            int receiverCodesId = res.getIdentifier(RECEIVER_KEY, "array", packageName);
            if (receiverCodesId != 0) {
                String[] codes = res.getStringArray(receiverCodesId);
                for (String item : codes) {
                    tryToPutCode(context, item, mReceiverCodes);
                }
            }

            if (DEBUG) {
                Log.d(TAG, "================================");
                Log.d(TAG, "packageName=" + packageName);
                Log.d(TAG, "activityCodesId=" + activityCodesId);
                Log.d(TAG, "receiverCodesId=" + activityCodesId);
            }
        }

        if (DEBUG) {
            Log.d(TAG, "===== Activity Custom Code =====");
            Log.d(TAG, mActivityCodes.toString());
            Log.d(TAG, "================================");
            Log.d(TAG, "===== Receiver Custom Code =====");
            Log.d(TAG, mReceiverCodes.toString());
            Log.d(TAG, "================================");
        }

        initExtraCodes(context);
    }

    private static void autoFindCustomCodes(Context context) {
        //Query list of package which`s activity or receiver declare CUSTOM_CODE_ACTION.
        PackageManager pm = context.getPackageManager();
        Intent intent = new Intent(CUSTOM_CODE_ACTION);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setType(CUSTOM_CODE_MIMETYPE + "/*");
        List<ResolveInfo> activityList = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo ri : activityList) {
            String packageName = ri.activityInfo.packageName;
            if (!mPackageList.contains(packageName)) {
                mPackageList.add(packageName);
            }
        }

        intent = new Intent(CUSTOM_CODE_ACTION);
        intent.setType(CUSTOM_CODE_MIMETYPE + "/*");
        List<ResolveInfo> receiverList = pm.queryBroadcastReceivers(intent, 0);
        for (ResolveInfo ri : receiverList) {
            String packageName = ri.activityInfo.packageName;
            if (!mPackageList.contains(packageName)) {
                mPackageList.add(packageName);
            }
        }
    }

    private static void loadMmiCustomCodes(Context context) {
        if (!mPackageList.contains(MMI_PACKAGE_NAME)) {
            mPackageList.add(MMI_PACKAGE_NAME);
        }
    }

    public static void tryToPutCode(Context context, String codeStr, HashMap<String, String> map) {
        if (codeStr != null && !"".equals(codeStr)) {
            String[] strs = codeStr.split(SEPARATOR);
            if (strs.length > 0) {
                String code = "";
                String component = "";
                code = strs[0];
                if (strs.length > 1) {
                    component = strs[1];
                } else {
                    Intent intent = new Intent(CUSTOM_CODE_ACTION);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.setType(CUSTOM_CODE_MIMETYPE + "/" + code);
                    PackageManager pm = context.getPackageManager();
                    List<ResolveInfo> riList = pm.queryIntentActivities(intent, 0);
                    if (riList != null && riList.size() == 1) {
                        ResolveInfo ri = riList.get(0);
                        component = ri.activityInfo.packageName + "/" + ri.activityInfo.name;
                        if (DEBUG) {
                            Log.d(TAG, "codeStr=" + codeStr);
                            Log.d(TAG, "component=" + component);
                        }
                    }

                }
                //The custom must be unique!!!
                if (!map.containsKey(code)) {
                    map.put(code, component);
                }
            }
        }
    }

    public static boolean handleCustomCode(Context context, String input) {
        // Custom codes are in the form *#<code>#

        int len = input.length();
        int prefixLen = CUSTOM_CODE_PREFIX.length();
        int postfixLen = CUSTOM_CODE_POSTFIX.length();
        int maxLen = CUSTOM_CODES_MIN_LENGTH + prefixLen + postfixLen;
        if (input.length() > maxLen
                && input.startsWith(CUSTOM_CODE_PREFIX) && input.endsWith(CUSTOM_CODE_POSTFIX)) {
            String customCode = input.substring(prefixLen, len - postfixLen);
            if (isNumeric(customCode)) {
                if (mActivityCodes.containsKey(customCode)) {
                    String component = mActivityCodes.get(customCode);
                    if (component != null) {
                        Intent intent = new Intent();
                        ComponentName componentName = ComponentName.unflattenFromString(component);
                        intent.setComponent(componentName);
                        context.startActivity(intent);
                        return true;
                    }
                }
                if (mReceiverCodes.containsKey(customCode)) {
                    Intent intent = new Intent(CUSTOM_CODE_ACTION);
                    intent.setType(CUSTOM_CODE_MIMETYPE + "/" + customCode);
                    context.sendBroadcast(intent);
                    return true;
                }
            }
        }

        if (handleExtraCode(context, input)) {
            return true;
        }
        return false;
    }

    private static final String EXTRA_CODE_ACTION = "intent.action.EXTRA_CODE";
    private static final String EXTRA_CODE_MIMETYPE = "code";
    private static final int EXTRA_CODES_MIN_LENGTH = 4;

    private static ArrayList<String> mExtraCodes = new ArrayList<String>();
    private static HashMap<String, String> mExtraActivityComponents = new HashMap<String, String>();

    private static void initExtraCodes(Context context) {
        String[] codes = context.getResources().getStringArray(R.array.extra_code_list);
        for (String code : codes) {
            if (DEBUG) {
                Log.d(TAG, "EXTRA_CODE_LIST: " + code);
            }
            if (code != null && code.contains(SEPARATOR)) {
                final int index = code.indexOf(SEPARATOR);
                if (index > 0) {
                    final String component = code.substring(index + 1, code.length());
                    code = code.substring(0, index);
                    mExtraActivityComponents.put(code, component);
                    if (DEBUG) {
                        Log.d(TAG, "EXTRA_COMPONENT: " + component);
                    }
                }
            }
            if (DEBUG) {
                Log.d(TAG, "EXTRA_CODE: " + code);
            }
            mExtraCodes.add(code);
        }
    }

    private static boolean handleExtraCode(Context context, String input) {
        // Extra codes are in the form #<code># etc, the prefix and postfix are arbitrary.
        // @see packages/apps/Dialer/res/values/goso_values.xml
        /* Example:
          <array name="extra_code_list">
              <item>@null</item> //none (default)
              <item>"#38378#:cn.gosomo.evt/cn.gosomo.evt.project.EvtHaierWizard"</item> //activity
              <item>"*76278#"</item> //broadcast receiver
          </array>
        */

        if (mExtraCodes != null && mExtraCodes.size() > 0
                && input != null && input.length() >= EXTRA_CODES_MIN_LENGTH
                && mExtraCodes.contains(input)) {
            String extraCode = input.replaceAll("[^0123456789]", "");

            if (mExtraActivityComponents.containsKey(input)) {
                String component = mExtraActivityComponents.get(input);
                Intent intent = new Intent();
                if (component != null) {
                    ComponentName componentName = ComponentName.unflattenFromString(component);
                    intent.setComponent(componentName);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    return true;
                }
            } else {
                Intent intent = new Intent(EXTRA_CODE_ACTION);
                intent.setType(EXTRA_CODE_MIMETYPE + "/" + extraCode);
                context.sendBroadcast(intent);
                return true;
            }
        }
        return false;
    }

    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        return pattern.matcher(str).matches();
    }
}
