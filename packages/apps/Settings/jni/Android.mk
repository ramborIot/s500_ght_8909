LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libuatr

LOCAL_SRC_FILES := uatr.c

LOCAL_SHARED_LIBRARIES := \
	liblog \
	libcutils \
    libutils \
	
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)
