//串口相关的头文件
#include<stdio.h>      /*标准输入输出定义*/
#include<stdlib.h>     /*标准函数库定义*/
#include<unistd.h>     /*Unix 标准函数定义*/
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>      /*文件控制定义*/
#include<termios.h>    /*PPSIX 终端控制定义*/
#include<errno.h>      /*错误号定义*/
#include<string.h>
#include "uatr.h"
#include<android/log.h>

#define TAG    "ljk100"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,TAG ,__VA_ARGS__)

#define FALSE  -1
#define TRUE   0

int fd = -1;
JNIEXPORT jboolean JNICALL Java_com_uatr_UART_rs232_1OpenPort(JNIEnv *env,
		jobject obj, jstring lpFileName, jint baudRate, jchar parity,
		jint byteSize, jint stopBits) {
	const char *str = (*env)->GetStringUTFChars(env, lpFileName, 0);
	//LOGI("%s", str);
	fd = UART0_Open(str);
	(*env)->ReleaseStringUTFChars(env, lpFileName, str);
	//system("echo 4 > /sys/bus/platform/devices/extpwr.0/driver/extpwr_val");
	//system("echo 5 > /sys/bus/platform/devices/extpwr.0/driver/extpwr_val");
	//LOGI("结果 === %d", fd);
	//LOGI(" %s  %d  %c  %d  %d", str, baudRate, parity, byteSize, stopBits);
	int set = UART0_Set(fd, baudRate, 0, byteSize, stopBits, parity);
	//LOGI("设置 === %d", set);
	if (fd == 0 && set == 0) {
		return (jboolean) fd;
	} else {
		return (jboolean) fd;
	}
}

JNIEXPORT jboolean JNICALL Java_com_uatr_UART_rs232_1ClosePort(JNIEnv *env,
		jobject obj) {
	int a = UART0_Close();
	//LOGI("关闭 === %d", a);
	if (a == 0) {
		return (jboolean) 1;
	} else {
		return (jboolean) 0;
	}
}

JNIEXPORT jboolean JNICALL Java_com_uatr_UART_rs232_1WritePort(JNIEnv * env,
		jobject obj, jbyteArray strbyte, jint strlens) {
	char* str = (char*) (*env)->GetByteArrayElements(env, strbyte, 0);
	//LOGI("str  %s ", str);
	//LOGI("str len === %d ", strlens);
	int a = UART0_Send(str, strlens);
	tcflush(fd, TCIFLUSH);
	//LOGI("发送数据 === %d", a);
	if (a == strlens) {
		//LOGI("1");
		return (jboolean) 1;
	} else {
		//LOGI("0");
		return (jboolean) 0;
	}
}

JNIEXPORT jboolean JNICALL Java_com_uatr_UART_rs232_1ReadPort(JNIEnv * env,
		jobject obj, jbyteArray num, jbyteArray readbyte, jint outtimes) {
	char* str = (char*) (*env)->GetByteArrayElements(env, readbyte, 0);
	int readlen = (*env)->GetArrayLength(env, readbyte);
	char* numstr = (char*) (*env)->GetByteArrayElements(env, num, 0);
	int numlen = (*env)->GetArrayLength(env, readbyte);
	int len = 0, ret = 0;
	fd_set fs_read;
	struct timeval tv_timeout;
	FD_ZERO(&fs_read);
	FD_SET(fd, &fs_read);
	tv_timeout.tv_sec = outtimes / 1000;
	tv_timeout.tv_usec = (outtimes % 1000) * 1000;
	while (1) {
		ret = select(fd + 1, &fs_read, NULL, NULL, &tv_timeout);
		//如果返回0，代表在描述符状态改变前已超过timeout时间,错误返回-1
		switch (ret) {
		case 0:
			//LOGI("Time Out!!!!");
			goto OUT;
			// break;
		case -1:
			//LOGI("IO Error!!!!");
			goto OUT;
			// break;
		default:

			//if (FD_ISSET(fd, &fs_read)) {
				len = read(fd, str, readlen);
				//LOGI("len ==== %d", len);
				int i = 0;
				for (; i < len; i++) {
					//LOGI("%d",str[i]);

				}
				// tcflush(fd, TCIFLUSH);
				goto OUT;
			//}
			break;
		}
	}
	OUT: (*env)->ReleaseByteArrayElements(env, num, numstr, 0);
	(*env)->ReleaseByteArrayElements(env, readbyte, str, 0);
	if (len > 0) {
		return (jboolean) 1;
	} else {
		return (jboolean) 0;
	}
}

/*******************************************************************
 * 名称：                UART0_Set
 * 功能：                设置串口数据位，停止位和效验位
 * 入口参数：        fd        串口文件描述符
 *                              speed     串口速度
 *                              flow_ctrl   数据流控制
 *                           databits   数据位   取值为 7 或者8
 *                           stopbits   停止位   取值为 1 或者2
 *                           parity     效验类型 取值为N,E,O,,S
 *出口参数：          正确返回为1，错误返回为0
 *******************************************************************/
int UART0_Set(int fd, int speed, int flow_ctrl, int databits, int stopbits,
		int parity) {

	int i;
	int status;
//	int speed_arr[] = { B4000000, B115200, B19200, B9600, B4800, B2400, B1200, B300 };
//	int name_arr[] = { 28800, 115200, 19200, 9600, 4800, 2400, 1200, 300 };
	int speed_arr[] = { B4000000, B3500000, B3000000, B2500000, B2000000,
			B1500000, B1152000, B1000000, B921600, B576000, B500000, B460800,
			B230400, B115200, B57600, B38400, B19200, B9600, B4800, B2400,
			B1800, B1200, B300 };
	int name_arr[] = { 28800, 3500000, 3000000, 2500000, 2000000, 1500000,
			1152000, 1000000, 921600, 576000, 500000, 460800, 230400, 115200,
			57200, 38400, 19200, 9600, 4800, 2400, 1800, 1200, 300 };

	struct termios options;

	/*tcgetattr(fd,&options)得到与fd指向对象的相关参数，并将它们保存于options,该函数还可以测试配置是否正确，该串口是否可用等。若调用成功，函数返回值为0，若调用失败，函数返回值为1.
	 */
	if (tcgetattr(fd, &options) != 0) {
		LOGI("SetupSerial 1");
		return (FALSE);
	}

	//设置串口输入波特率和输出波特率
	for (i = 0; i < sizeof(speed_arr) / sizeof(int); i++) {
		if (speed == name_arr[i]) {
			LOGI("speed === %d", speed_arr[i]);
			cfmakeraw(&options);
			cfsetispeed(&options, speed_arr[i]);
			cfsetospeed(&options, speed_arr[i]);
			break;
		}
	}

	//修改控制模式，保证程序不会占用串口
	options.c_cflag |= CLOCAL;
	//修改控制模式，使得能够从串口中读取输入数据
	options.c_cflag |= CREAD;

	//设置数据流控制
	switch (flow_ctrl) {

	case 0: //不使用流控制
		options.c_cflag &= ~CRTSCTS;
		break;

	case 1: //使用硬件流控制
		options.c_cflag |= CRTSCTS;
		break;
	case 2: //使用软件流控制
		options.c_cflag |= IXON | IXOFF | IXANY;
		break;
	}
	//设置数据位
	//屏蔽其他标志位
	options.c_cflag &= ~CSIZE;
	switch (databits) {
	case 5:
		options.c_cflag |= CS5;
		break;
	case 6:
		options.c_cflag |= CS6;
		break;
	case 7:
		options.c_cflag |= CS7;
		break;
	case 8:
		options.c_cflag |= CS8;
		break;
	default:
		LOGI("Unsupported data size\n");
		return (FALSE);
	}
	//设置校验位
	switch (parity) {
	case 'n':
	case 'N': //无奇偶校验位。
		options.c_cflag &= ~PARENB;
		options.c_iflag &= ~INPCK;
		break;
	case 'o':
	case 'O': //设置为奇校验
		options.c_cflag |= (PARODD | PARENB);
		options.c_iflag |= INPCK;
		break;
	case 'e':
	case 'E': //设置为偶校验
		options.c_cflag |= PARENB;
		options.c_cflag &= ~PARODD;
		options.c_iflag |= INPCK;
		break;
	case 's':
	case 'S': //设置为空格
		options.c_cflag &= ~PARENB;
		options.c_cflag &= ~CSTOPB;
		break;
	default:
		LOGI("Unsupported parity\n");
		return (FALSE);
	}
	// 设置停止位
	switch (stopbits) {
	case 1:
		options.c_cflag &= ~CSTOPB;
		break;
	case 2:
		options.c_cflag |= CSTOPB;
		break;
	default:
		LOGI("Unsupported stop bits\n");
		return (FALSE);
	}

	//设置等待时间和最小接收字符
	options.c_cc[VTIME] = 1; /* 读取一个字符等待1*(1/10)s */
	options.c_cc[VMIN] = 1; /* 读取字符的最少个数为1 */

	//如果发生数据溢出，接收数据，但是不再读取 刷新收到的数据但是不读
	tcflush(fd, TCIFLUSH);

	//激活配置 (将修改后的termios数据设置到串口中）
	if (tcsetattr(fd, TCSANOW, &options) != 0) {
		LOGI("com set error!\n");
		return (FALSE);
	}
	return (TRUE);
}

/*******************************************************************
 * 名称：                  UART0_Open
 * 功能：                打开串口并返回串口设备文件描述
 * 入口参数：        fd    :文件描述符     port :串口号(ttyS0,ttyS1,ttyS2)
 * 出口参数：        正确返回为1，错误返回为0
 *******************************************************************/
int UART0_Open(char* port) {
	LOGI("%s", port);
	fd = open(port, O_RDWR | 0 | O_NONBLOCK);

	if (FALSE == fd) {
		LOGI("Can't Open Serial Port");
		return (FALSE);
	}
	LOGI("fd->open=%d\n", fd);
	return fd;
}

/*******************************************************************
 * 名称：                UART0_Close
 * 功能：                关闭串口并返回串口设备文件描述
 * 入口参数：        fd    :文件描述符
 * 出口参数：        void
 *******************************************************************/

int UART0_Close() {
	int a = close(fd);
	LOGI("fd->close=%d\n", a);
	return a;
}

/********************************************************************
 * 名称：                  UART0_Send
 * 功能：                发送数据
 * 入口参数：        fd                  :文件描述符
 *                              send_buf    :存放串口发送数据
 *                              data_len    :一帧数据的个数
 * 出口参数：        正确返回为1，错误返回为0
 *******************************************************************/
int UART0_Send(char *send_buf, int data_len) {
	int len = 0;

	len = write(fd, send_buf, data_len);

	if (len == data_len) {
		//LOGI("UART0_Send LEN1 === %d", len);
		return len;
	} else {
		tcflush(fd, TCOFLUSH);
		//LOGI("UART0_Send LEN2 === %d", len);
		return FALSE;
	}
}
