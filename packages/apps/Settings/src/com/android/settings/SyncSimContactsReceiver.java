/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.IccCardConstants;
import android.telephony.SubscriptionManager;
import com.android.internal.telephony.PhoneConstants;

//Add by linmeng at 2015-05-25 merged by wangjianmin for sim contacts begin
public class SyncSimContactsReceiver extends BroadcastReceiver {

    private static final String TAG = "SyncSimContactsReceiver";

    private static final String SYNC_SIM_ACITON = "android.intent.action.SYNC_SIM_CONTACTS";
    private static final String SYNC_SIM_BOOT = "persist.sys.sync_sim_boot_flag";

    public static final int DSDS_SLOT_1_ID = 0;
    public static final int DSDS_SLOT_2_ID = 1;
    public static final int DSDS_INVALID_SLOT_ID = SubscriptionManager.INVALID_SIM_SLOT_INDEX;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "SyncSimContactsReceiver intent.getAction():" + intent.getAction());

        /*if (!isUxFeature()) {
            return;
        }*/


        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            SystemProperties.set(SYNC_SIM_BOOT, "true");
            //Delay 120 seconds
            Calendar calendar=Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.add(Calendar.SECOND, 1);
            AlarmManager alarm= (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

            Intent mIntent = new Intent();
            mIntent.setAction(SYNC_SIM_ACITON);
            mIntent.setPackage("com.android.contacts");
            mIntent.putExtra("broadcast_flag", 0);

            PendingIntent sender = PendingIntent.getService(context,0, mIntent, 0);
            alarm.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender );
        }
        else if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(intent.getAction())) {
            int subId = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY, 0);
            int phoneId = intent.getIntExtra(PhoneConstants.PHONE_KEY, 0);
            Log.d(TAG, "SyncSimContactsReceiver SUBID:" + subId);
            Log.d(TAG, "SyncSimContactsReceiver phoneId:" + phoneId);

            Log.d(TAG, "SyncSimContactsReceiver SYNC_SIM_BOOT = " + SystemProperties.getBoolean(SYNC_SIM_BOOT, false));
            if (SystemProperties.getBoolean(SYNC_SIM_BOOT, false)){
            int slot = SubscriptionManager.getSlotId(subId);
            Log.d(TAG, "SyncSimContactsReceiver onReceive slot:" + slot);

            int simState = TelephonyManager.SIM_STATE_UNKNOWN;
            simState = TelephonyManager.getDefault().getSimState(slot);
            Log.d(TAG, "SyncSimContactsReceiver onReceive simState:" + simState);

            if (slot == DSDS_SLOT_1_ID) {   //Slot 1

                Intent mIntentSlot1 = new Intent();
                mIntentSlot1.setAction(SYNC_SIM_ACITON);
                mIntentSlot1.setPackage("com.android.contacts");
                mIntentSlot1.putExtra("broadcast_flag", 1);
                mIntentSlot1.putExtra("slot", slot);
                mIntentSlot1.putExtra("sim_state", simState);

                if (simState != TelephonyManager.SIM_STATE_READY) {
                    context.startService(mIntentSlot1);
                    Settings.System.putInt(context.getContentResolver(), Settings.System.SYNC_SIM1_FLAG, 0);
                }
                else {
                    int flag = Settings.System.getInt(context.getContentResolver(), Settings.System.SYNC_SIM1_FLAG, 0);
                    Log.d(TAG,"SyncSimContactsReceiver flag is " + flag);
                    if (flag == 0) {
                        context.startService(mIntentSlot1);
                        Settings.System.putInt(context.getContentResolver(), Settings.System.SYNC_SIM1_FLAG, 1);
                    }
                }
            } else {   //Slot 2

                Intent mIntentSlot2 = new Intent();
                mIntentSlot2.setAction(SYNC_SIM_ACITON);
                mIntentSlot2.setPackage("com.android.contacts");
                mIntentSlot2.putExtra("broadcast_flag", 1);
                mIntentSlot2.putExtra("slot", slot);
                mIntentSlot2.putExtra("sim_state", simState);

                if (simState != TelephonyManager.SIM_STATE_READY) {
                    context.startService(mIntentSlot2);
                    Settings.System.putInt(context.getContentResolver(), Settings.System.SYNC_SIM2_FLAG, 0);
                    } else {
                        if (Settings.System.getInt(context.getContentResolver(), Settings.System.SYNC_SIM2_FLAG, 0) == 0) {
                            context.startService(mIntentSlot2);
                            Settings.System.putInt(context.getContentResolver(), Settings.System.SYNC_SIM2_FLAG, 1);
                        }
                    }
                }
            }
        }
    }

    private  boolean isUxFeature() {
        String uxFeature = SystemProperties.get("ro.sf3g.feature", "");
        if ("ux".equals(uxFeature)) {
            return true;
        } else
            return false;
	}
}
//Add by linmeng at 2015-05-25 merged by wangjianmin for sim contacts end
