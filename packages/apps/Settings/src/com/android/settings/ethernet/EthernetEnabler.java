/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.ethernet;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.preference.CheckBoxPreference;
import android.net.ConnectivityManager;

import java.util.ArrayList;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.R;
import com.android.settings.widget.SwitchBar;
import com.android.settings.SettingsActivity;
import android.widget.Switch;
import android.net.EthernetManager;

public class EthernetEnabler implements SwitchBar.OnSwitchChangeListener {
    private static final String TAG = "EthernetEnabler";
    private Context mContext = null;
    private SwitchBar mSwitchBar = null;
    private EthernetDialog mEthernetDialog = null;
    private EthernetManager mEthernetManager = null;
    private boolean mListeningOnSwitchChange = false;

    public void setConfigDialog(EthernetDialog Dialog) {
        mEthernetDialog = Dialog;
    }

    public EthernetEnabler(Context context, SwitchBar switchBar, EthernetManager ethernetManager) {
        mContext = context;
        mSwitchBar = switchBar;
        mEthernetManager = ethernetManager;
        setupSwitchBar();
    }

    public void resume(Context context) {
        mContext = context;
        if (!mListeningOnSwitchChange) {
            mSwitchBar.addOnSwitchChangeListener(this);
            mListeningOnSwitchChange = true;
        }
    }

    public void pause() {
        if (mListeningOnSwitchChange) {
            mSwitchBar.removeOnSwitchChangeListener(this);
            mListeningOnSwitchChange = false;
        }
    }

    public void setupSwitchBar() {
        if (!mListeningOnSwitchChange) {
            mSwitchBar.addOnSwitchChangeListener(this);
            mListeningOnSwitchChange = true;
        }
        mSwitchBar.show();
        int enable = Settings.Global.getInt(mContext.getContentResolver(), Settings.Global.ETHERNET_ON, 0);
        if (enable == EthernetManager.ETHERNET_STATE_ENABLED) {
            mSwitchBar.setChecked(true);
        } else {
            mSwitchBar.setChecked(false);
        }
    }

    public void teardownSwitchBar() {
        if (mListeningOnSwitchChange) {
            mSwitchBar.removeOnSwitchChangeListener(this);
            mListeningOnSwitchChange = false;
        }
        mSwitchBar.hide();
    }

    @Override
    public void onSwitchChanged(Switch switchView, boolean isChecked) {
        if (isChecked) {
            if (mEthernetManager != null)
                mEthernetManager.start();
        } else {
            if (mEthernetManager != null)
                mEthernetManager.stop();
        }
        Settings.Global.putInt(mContext.getContentResolver(), Settings.Global.ETHERNET_ON,
            isChecked ? EthernetManager.ETHERNET_STATE_ENABLED : EthernetManager.ETHERNET_STATE_DISABLED);
        Intent intent = new Intent(ConnectivityManager.INET_CONDITION_ACTION);
        mContext.sendBroadcast(intent);
    }
}
