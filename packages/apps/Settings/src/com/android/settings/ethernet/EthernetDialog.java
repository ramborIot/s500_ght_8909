/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.ethernet;

import com.android.settings.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.util.Slog;
import android.view.inputmethod.InputMethodManager;
import android.net.IpConfiguration;
import android.net.IpConfiguration.IpAssignment;
import android.net.IpConfiguration.ProxySettings;
import android.os.Environment;
import android.util.SparseArray;
import android.net.StaticIpConfiguration;
import android.net.EthernetManager;
import java.net.InetAddress;
import java.net.Inet4Address;
import java.util.Iterator;
import android.text.TextUtils;
import android.net.LinkAddress;
import android.net.NetworkUtils;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import com.android.settings.Utils;
import android.widget.Toast;
import android.provider.Settings;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

class EthernetDialog extends AlertDialog implements DialogInterface.OnClickListener,
        DialogInterface.OnShowListener, DialogInterface.OnDismissListener {
    private static final String TAG = "EthernetDialog";
    private static final boolean LOCAL_LOGV = false;
    private static final int ETHERNET_DISABLE_DHCP = 0;
    private static final int ETHERNET_ENABLE_DHCP = 1;
    private static final int ETHERNET_USE_SUBNET_MASK = 0;
    private static final int ETHERNET_USE_PREFIX_LENGTH = 1;

    private StaticIpConfiguration mStaticIpConfiguration = null;
    private String mConfigurationSetErrorPrompt = null;
    private CheckBox mEnableDHCP = null;
    private CheckBox mUsePrefixLength = null;
    private TextView mSubnetMaskOrPrefixLengthTitle = null;
    private EditText mIP = null;
    private EditText mSubnetMaskOrPrefixLength = null;
    private EditText mGateway = null;
    private EditText mPreferredDns = null;
    private EditText mAlternateDns = null;
    private View mView = null;
    private Context mContext = null;
    private EthernetManager mEthernetManager = null;
    private ConnectivityManager mConnectivityManager = null;

    public EthernetDialog(Context context, EthernetManager em, ConnectivityManager cm) {
        super(context);
        mContext = context;
        mEthernetManager = em;
        mConnectivityManager = cm;
        buildEthernetDialog(context);
        setOnShowListener(this);
        setOnDismissListener(this);
    }

    public void onShow(DialogInterface dialog) {
        if (LOCAL_LOGV) Slog.d(TAG, "onShow");
        updateEthernetConfiguration();

        // soft keyboard pops up on the disabled EditText. Hide it.
        InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public void onDismiss(DialogInterface dialog) {
        if (LOCAL_LOGV) Slog.d(TAG, "onDismiss");
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case BUTTON_POSITIVE:
                saveConfigurationHandler();
                break;
            case BUTTON_NEGATIVE:
                //Don't need to do anything
                break;
            default:
        }
    }

    private void buildEthernetDialog(Context context) {
        this.setView(mView = getLayoutInflater().inflate(R.layout.ethernet_configure, null));
        mEnableDHCP = (CheckBox)mView.findViewById(R.id.cb_enable_dhcp);
        mUsePrefixLength = (CheckBox)mView.findViewById(R.id.cb_use_prefix);
        mSubnetMaskOrPrefixLengthTitle = (TextView)mView.findViewById(R.id.tv_mask_or_prefix);
        mIP = (EditText)mView.findViewById(R.id.et_ipaddr);
        mSubnetMaskOrPrefixLength = (EditText)mView.findViewById(R.id.et_mask_or_prefix);
        mGateway = (EditText)mView.findViewById(R.id.et_gateway);
        mPreferredDns = (EditText)mView.findViewById(R.id.et_preferred_dns);
        mAlternateDns = (EditText)mView.findViewById(R.id.et_alternate_dns);

        int iEnableDHCP = Settings.Global.getInt(mContext.getContentResolver(), Settings.Global.ETHERNET_ENABLE_DHCP, 1);
        int iUsePrefixLength = Settings.Global.getInt(mContext.getContentResolver(), Settings.Global.ETHERNET_USE_NETWORK_PREFIX_LENGTH, 0);
        mEnableDHCP.setChecked((iEnableDHCP == 1));
        mUsePrefixLength.setChecked((iUsePrefixLength == 1));
        mSubnetMaskOrPrefixLengthTitle.setText((iUsePrefixLength == 1) ? R.string.ethernet_prefix_length : R.string.ethernet_subnet_mask);
        mIP.setEnabled((iEnableDHCP == 0));
        mSubnetMaskOrPrefixLength.setEnabled((iEnableDHCP == 0));
        mGateway.setEnabled((iEnableDHCP == 0));
        mPreferredDns.setEnabled((iEnableDHCP == 0));
        mAlternateDns.setEnabled((iEnableDHCP == 0));
        initEthernetConfiguration((iEnableDHCP == 1));

        mEnableDHCP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                mIP.setEnabled(!isChecked);
                mSubnetMaskOrPrefixLength.setEnabled(!isChecked);
                mGateway.setEnabled(!isChecked);
                mPreferredDns.setEnabled(!isChecked);
                mAlternateDns.setEnabled(!isChecked);
                initEthernetConfiguration(isChecked);
                Settings.Global.putInt(mContext.getContentResolver(), Settings.Global.ETHERNET_ENABLE_DHCP,
                    isChecked ? ETHERNET_ENABLE_DHCP : ETHERNET_DISABLE_DHCP);
            }
        });

        mUsePrefixLength.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                if (isChecked) {
                    mSubnetMaskOrPrefixLengthTitle.setText(R.string.ethernet_prefix_length);
                    String subnetMask = mSubnetMaskOrPrefixLength.getText().toString();
                    if (isValidSubnetMaskAddress(subnetMask)) {
                        int iSubnetMask = NetworkUtils.inetAddressToInt(getIPv4Address(subnetMask));
                        int iPrefixLength = NetworkUtils.netmaskIntToPrefixLength(iSubnetMask);
                        mSubnetMaskOrPrefixLength.setText(Integer.toString(iPrefixLength));
                    } else {
                        mSubnetMaskOrPrefixLength.setText("");
                    }
                } else {
                    mSubnetMaskOrPrefixLengthTitle.setText(R.string.ethernet_subnet_mask);
                    String prefixLength = mSubnetMaskOrPrefixLength.getText().toString();
                    if (isValidPrefixLength(prefixLength)) {
                        int iPrefixLength = Integer.parseInt(prefixLength);
                        int iSubnetMask = NetworkUtils.prefixLengthToNetmaskInt(iPrefixLength);
                        mSubnetMaskOrPrefixLength.setText(NetworkUtils.intToInetAddress(iSubnetMask).getHostAddress());
                    } else {
                        mSubnetMaskOrPrefixLength.setText("");
                    }
                }
                Settings.Global.putInt(mContext.getContentResolver(), Settings.Global.ETHERNET_USE_NETWORK_PREFIX_LENGTH,
                    isChecked ? ETHERNET_USE_PREFIX_LENGTH : ETHERNET_USE_SUBNET_MASK);
            }
        });

        this.setInverseBackgroundForced(true);
        this.setButton(BUTTON_POSITIVE, context.getText(R.string.menu_save), this);
        this.setButton(BUTTON_NEGATIVE, context.getText(R.string.menu_cancel), this);
        updateEthernetConfiguration();
    }

    private void initEthernetConfiguration(boolean isDhcpEnable) {
        if (isDhcpEnable) {
            mIP.setText("");
            mSubnetMaskOrPrefixLength.setText("");
            mGateway.setText("");
            mPreferredDns.setText("");
            mAlternateDns.setText("");
        } else {
            if(TextUtils.isEmpty(mIP.getText().toString()))
                mIP.setText("192.168.1.189");
            if(TextUtils.isEmpty(mSubnetMaskOrPrefixLength.getText().toString()))
                mSubnetMaskOrPrefixLength.setText((mUsePrefixLength.isChecked()) ? "24" : "255.255.255.0");
            if(TextUtils.isEmpty(mGateway.getText().toString()))
                mGateway.setText("192.168.1.1");
            if(TextUtils.isEmpty(mPreferredDns.getText().toString()))
                mPreferredDns.setText("202.96.134.133");
            if(TextUtils.isEmpty(mAlternateDns.getText().toString()))
                mAlternateDns.setText("202.96.128.86");
        }
    }

    private void updateEthernetConfiguration() {
        int enable = Settings.Global.getInt(mContext.getContentResolver(), Settings.Global.ETHERNET_ON, 0);
        if (enable == EthernetManager.ETHERNET_STATE_ENABLED) {
            IpConfiguration ipcfg = mEthernetManager.getConfiguration();
            if (ipcfg != null) {
                int iEnableDHCP = Settings.Global.getInt(mContext.getContentResolver(), Settings.Global.ETHERNET_ENABLE_DHCP, 1);
                if (iEnableDHCP == 1) {
                    mEnableDHCP.setChecked(true);
                    if (mConnectivityManager != null) {
                        LinkProperties lp  = mConnectivityManager.getLinkProperties(ConnectivityManager.TYPE_ETHERNET);
                        if (lp != null) {
                            Iterator<InetAddress> ipAddrIterator = lp.getAddresses().iterator();
                            if (ipAddrIterator.hasNext()) {
                                mIP.setText(ipAddrIterator.next().getHostAddress());
                            }
                            String subnetMask = execCommandToGetDhcpInfo("getprop dhcp.eth0.mask");
                            if (subnetMask != null) {
                                if (!mUsePrefixLength.isChecked()) {
                                    mSubnetMaskOrPrefixLength.setText(subnetMask);
                                } else {
                                    int iSubnetMask = NetworkUtils.inetAddressToInt(getIPv4Address(subnetMask));
                                    int iPrefixLength = NetworkUtils.netmaskIntToPrefixLength(iSubnetMask);
                                    mSubnetMaskOrPrefixLength.setText(Integer.toString(iPrefixLength));
                                }
                            }
                            String gateway = execCommandToGetDhcpInfo("getprop dhcp.eth0.gateway");
                            if (gateway != null) {
                                mGateway.setText(gateway);
                            }
                            Iterator<InetAddress> dnsIterator = lp.getDnsServers().iterator();
                            if (dnsIterator.hasNext()) {
                                mPreferredDns.setText(dnsIterator.next().getHostAddress());
                                if (dnsIterator.hasNext()) {
                                    mAlternateDns.setText(dnsIterator.next().getHostAddress());
                                }
                            }
                        }
                    }
                } else {
                    mEnableDHCP.setChecked(false);
                    StaticIpConfiguration staticipcfg = ipcfg.getStaticIpConfiguration();
                    if (staticipcfg != null) {
                        if (staticipcfg.ipAddress != null) {
                            mIP.setText(staticipcfg.ipAddress.getAddress().getHostAddress());
                            if (!mUsePrefixLength.isChecked()) {
                                int iSubnetMask = NetworkUtils.prefixLengthToNetmaskInt(staticipcfg.ipAddress.getNetworkPrefixLength());
                                mSubnetMaskOrPrefixLength.setText(NetworkUtils.intToInetAddress(iSubnetMask).getHostAddress());
                            } else {
                                mSubnetMaskOrPrefixLength.setText(Integer.toString(staticipcfg.ipAddress.getNetworkPrefixLength()));
                            }
                        }
                        if (staticipcfg.gateway != null) {
                            mGateway.setText(staticipcfg.gateway.getHostAddress());
                        }
                        Iterator<InetAddress> dnsIterator = staticipcfg.dnsServers.iterator();
                        if (dnsIterator.hasNext()) {
                            mPreferredDns.setText(dnsIterator.next().getHostAddress());
                            if (dnsIterator.hasNext()) {
                                mAlternateDns.setText(dnsIterator.next().getHostAddress());
                            }
                        }
                    }
                }
            }
        }
    }

    private String execCommandToGetDhcpInfo(String systemCmd) {
        String out = null;
        try {
            Process process = Runtime.getRuntime().exec(systemCmd);
            InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            while (null != (out = bufferedReader.readLine())) {
                if (isValidIPv4Address(out)) {
                    break;
                }
            }
        } catch (IOException e) {
        }
        return out;
    }

    private void saveConfigurationHandler() {
        if (mEnableDHCP.isChecked()) {
            mEthernetManager.setConfiguration(new IpConfiguration(IpAssignment.DHCP, ProxySettings.NONE, null, null));
        } else {
            mStaticIpConfiguration = new StaticIpConfiguration();
            if (validateStaticIpConfigFields(mStaticIpConfiguration) != 0) {
                Toast.makeText(mContext, mConfigurationSetErrorPrompt, Toast.LENGTH_LONG).show();
                return;
            }
            mEthernetManager.setConfiguration(new IpConfiguration(IpAssignment.STATIC, ProxySettings.NONE, mStaticIpConfiguration, null));
        }
    }

    private int validateStaticIpConfigFields(StaticIpConfiguration staticIpConfiguration) {
        String ipAddr = mIP.getText().toString();
        if (TextUtils.isEmpty(ipAddr)) {
            mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_empty_error);
            return -1;
        }
        if (!isValidIPv4Address(ipAddr)) {
            mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_ipaddr_error);
            return -1;
        }
        Inet4Address inetIpAddr = getIPv4Address(ipAddr);
        if (inetIpAddr == null) {
            mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_ipaddr_error);
            return -1;
        }

        String subnetMaskOrPrefixLength = mSubnetMaskOrPrefixLength.getText().toString();
        if (TextUtils.isEmpty(subnetMaskOrPrefixLength)) {
            mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_empty_error);
            return -1;
        }
        int networkPrefixLength = 0;
        if (!mUsePrefixLength.isChecked()) {
            if (!isValidSubnetMaskAddress(subnetMaskOrPrefixLength)) {
                mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_subnet_mask_error);
                return -1;
            }
            Inet4Address inetSubnetMaskAddr = getIPv4Address(subnetMaskOrPrefixLength);
            if (inetSubnetMaskAddr == null) {
                mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_subnet_mask_error);
                return -1;
            }
            networkPrefixLength = NetworkUtils.netmaskIntToPrefixLength(NetworkUtils.inetAddressToInt(inetSubnetMaskAddr));
        } else {
            if (!isValidPrefixLength(subnetMaskOrPrefixLength)) {
                mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_prefix_length_error);
                return -1;
            }
            if ((networkPrefixLength = getNetworkPrefixLength(subnetMaskOrPrefixLength)) < 0) {
                mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_prefix_length_error);
                return -1;
            }
        }
        staticIpConfiguration.ipAddress = new LinkAddress(inetIpAddr, networkPrefixLength);

        String gateway = mGateway.getText().toString();
        if (TextUtils.isEmpty(gateway)) {
            mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_empty_error);
            return -1;
        }
        if (!isValidIPv4Address(gateway)) {
            mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_gateway_error);
            return -1;
        }
        Inet4Address inetGatewayAddr = getIPv4Address(gateway);
        if (inetGatewayAddr == null) {
            mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_gateway_error);
            return -1;
        }
        staticIpConfiguration.gateway = inetGatewayAddr;

        String preferredDns = mPreferredDns.getText().toString();
        if (TextUtils.isEmpty(preferredDns)) {
            mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_empty_error);
            return -1;
        }
        if (!isValidIPv4Address(preferredDns)) {
            mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_preferred_dns_error);
            return -1;
        }
        Inet4Address inetPreferredDnsAddr = getIPv4Address(preferredDns);
        if (inetPreferredDnsAddr == null) {
            mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_preferred_dns_error);
            return -1;
        }
        staticIpConfiguration.dnsServers.add(inetPreferredDnsAddr);

        String alternateDns = mAlternateDns.getText().toString();
        if (!TextUtils.isEmpty(alternateDns)) {
            if (!isValidIPv4Address(alternateDns)) {
                mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_alternate_dns_error);
                return -1;
            }
            Inet4Address inetAlternateDnsAddr = getIPv4Address(alternateDns);
            if (inetAlternateDnsAddr == null) {
                mConfigurationSetErrorPrompt = mContext.getResources().getString(R.string.ethernet_settings_alternate_dns_error);
                return -1;
            }
            staticIpConfiguration.dnsServers.add(inetAlternateDnsAddr);
        }

        return 0;
    }

    private boolean isValidIPv4Address(String ipString) {
        int start = 0;
        int end = ipString.indexOf('.');
        int numBlocks = 0;

        while (start < ipString.length()) {
            if (end == -1) {
                end = ipString.length();
            }

            try {
                int block = Integer.parseInt(ipString.substring(start, end));
                if (block < 0 || block > 255) {
                    return false;
                }
            } catch (NumberFormatException e) {
                return false;
            }

            numBlocks++;
            start = end + 1;
            end = ipString.indexOf('.', start);
        }
        return numBlocks == 4;
    }

    private boolean isValidSubnetMaskAddress(String subnetMaskString) {
        int subnetMask = 0;
        Inet4Address subnetMaskAddress = null;

        if (true == isValidIPv4Address(subnetMaskString)) {
            if (null != (subnetMaskAddress = getIPv4Address(subnetMaskString))) {
                subnetMask = NetworkUtils.inetAddressToInt(subnetMaskAddress);
                subnetMask = toLittleEndian(subnetMask);
                return (subnetMask - 1 | subnetMask) == 0xFFFFFFFF;
            }
        }
        return false;
    }

    private boolean isValidPrefixLength(String prefixString) {
        int networkPrefixLength = 0;

        try {
            networkPrefixLength = Integer.parseInt(prefixString);
        } catch (NumberFormatException e) {
            return false;
        }

        return (networkPrefixLength >= 0 && networkPrefixLength <= 32);
    }

    private Inet4Address getIPv4Address(String ipString) {
        try {
            return (Inet4Address)NetworkUtils.numericToInetAddress(ipString);
        } catch (IllegalArgumentException | ClassCastException e) {
            return null;
        }
    }

    private int getNetworkPrefixLength(String prefixString) {
        int networkPrefixLength = 0;

        try {
            networkPrefixLength = Integer.parseInt(prefixString);
        } catch (NumberFormatException e) {
            return -1;
        }
        return networkPrefixLength;
    }

    private int toLittleEndian(int in) {
        int ret = 0;

        ret = (ret << 8) | ((byte)((in >> 0) & 0xFF) & 0xFF);
        ret = (ret << 8) | ((byte)((in >> 8) & 0xFF) & 0xFF);
        ret = (ret << 8) | ((byte)((in >> 16) & 0xFF) & 0xFF);
        ret = (ret << 8) | ((byte)((in >> 24) & 0xFF) & 0xFF);
        return ret;
    }
}
