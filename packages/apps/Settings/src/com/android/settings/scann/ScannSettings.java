package com.android.settings.scann;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemProperties;
import android.content.Context;
import android.content.Intent;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.widget.EditText;
import android.provider.Settings;
import android.provider.Settings.Global;
import android.util.Log;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;

import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.R;
import android.hardware.ddi.DdiManager;


/**
 * Created by root on 16-12-16.
 */

public class ScannSettings extends SettingsPreferenceFragment implements OnPreferenceChangeListener {

    private static final String TAG = "ScannSettings";
	private static final String STARTREAD = "startread";

    private static final String KEY_SCANN_ON_OFF = "scann_onoff";
	private Context mContext;

    private SwitchPreference scann_onoff;
	private SwitchPreference scann_module_onoff;
    private CheckBoxPreference key_out;
    private CheckBoxPreference broad_out;

    private Preference broad_action_out;
    private Preference broad_data_out;

    private CheckBoxPreference sound_out;
    private CheckBoxPreference shock_out;
    private CheckBoxPreference enter_out;
	

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
		mContext = (Context) getActivity();
        addPreferencesFromResource(R.xml.scann);
        initUI();
        initData();
		
		
    }
	


    private void initUI() {
        scann_onoff = (SwitchPreference) findPreference(KEY_SCANN_ON_OFF);
        scann_onoff.setOnPreferenceChangeListener(this);
		
		scann_module_onoff = (SwitchPreference) findPreference("scann_module_onoff");
        scann_module_onoff.setOnPreferenceChangeListener(this);
		
        key_out = (CheckBoxPreference)findPreference("key_out");
        key_out.setOnPreferenceChangeListener(this);
        broad_out = (CheckBoxPreference)findPreference("broad_out");
        broad_out.setOnPreferenceChangeListener(this);
        broad_action_out = findPreference("broad_action_out");
        broad_data_out = findPreference("broad_data_out");
        sound_out = (CheckBoxPreference)findPreference("sound_out");
        sound_out.setOnPreferenceChangeListener(this);
        shock_out = (CheckBoxPreference)findPreference("shock_out");
        shock_out.setOnPreferenceChangeListener(this);
        enter_out = (CheckBoxPreference)findPreference("enter_out");
        enter_out.setOnPreferenceChangeListener(this);
    }

    private void initData() {
		
        if(SystemProperties.get("persist.sys.scann").equals("true")){
            scann_onoff.setChecked(true);
        }else {
            scann_onoff.setChecked(false);
        }
		
		 if(SystemProperties.get("persist.sys.module").equals("true")){
            scann_module_onoff.setChecked(true);
        }else {
			scann_onoff.setEnabled(false);
            scann_module_onoff.setChecked(false);
        }

//		int value = Settings.Global.getInt(mContext.getContentResolver(),
//				Settings.Global.SCANNER_ON, 0);
//		if (value == 1) {
//			scann_onoff.setChecked(true);
//		} else {
//			scann_onoff.setChecked(false);
//		}
		
        //String scannout = SystemProperties.get("persist.sys.scannout");
//        String mode = Settings.Global.getString(mContext.getContentResolver(),
//				Settings.Global.SCANNER_OUTPUT_MODE);
//		if (mode == null) {
//			Log.e(TAG, "scanner_output_mode is not set ");
//		} else if (mode.equals("key")){
//            key_out.setChecked(true);
//            broad_out.setChecked(false);
//            broad_action_out.setEnabled(false);
//            broad_data_out.setEnabled(false);
//        }else if (mode.equals("broad")){
//            broad_out.setChecked(true);
//            key_out.setChecked(false);
//            broad_action_out.setEnabled(true);
//            broad_data_out.setEnabled(true);
//        }

        String actiontxt =  SystemProperties.get("persist.sys.actiontext");
		//System.out.println("ScannSettings actiontxt = " + actiontxt);
        if ("".equals(actiontxt)){
			
			
			SystemProperties.set("persist.sys.actiontext","com.jl.scannCode");
        }else{
            broad_action_out.setSummary(actiontxt);
        }

//		String action = Settings.Global.getString(mContext.getContentResolver(),
//				Settings.Global.SCANNER_BROADCASTER_ACTION);
//		if (action != null)
//			broad_action_out.setSummary(action);

        String actiondata =  SystemProperties.get("persist.sys.actiondata");
		//System.out.println("ScannSettings actiondata = " + actiondata);
        if ("".equals(actiondata)){
		
			SystemProperties.set("persist.sys.actiondata","scannData");
        }else{
            broad_data_out.setSummary(actiondata);
        }
//		String data = Settings.Global.getString(mContext.getContentResolver(),
//				Settings.Global.SCANNER_BROADCASTER_DATA);
//		if (data != null)
//			broad_action_out.setSummary(data);

		
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference == broad_action_out){
            setBoradAction ();
        }else if (preference == broad_data_out){
            setBoradData ();
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    public boolean onPreferenceChange(Preference preference, Object o) {
        if (preference == scann_onoff) {
            if ((Boolean) o){
                ScannController.ScannOpen();
				ScannerService.startRread();
                SystemProperties.set("persist.sys.scann","true");
                System.out.println("ScannOpen");
				Intent starScann = new Intent(mContext,ScannerService.class);
				mContext.startService(starScann);
            }else {
                ScannController.ScannClose();
				ScannerService.stopRread();
                SystemProperties.set("persist.sys.scann","false");
                System.out.println("ScannClose");
            }

        }else if (preference == key_out){
            if ((Boolean) o){
                broad_out.setChecked(false);
                broad_action_out.setEnabled(false);
                broad_data_out.setEnabled(false);
                SystemProperties.set("persist.sys.scannout","key");
            }else {
                SystemProperties.set("persist.sys.scannout","");
            }
        }else if (preference == broad_out){
            if ((Boolean) o){
                key_out.setChecked(false);
                broad_action_out.setEnabled(true);
                broad_data_out.setEnabled(true);
                SystemProperties.set("persist.sys.scannout","broad");
            }else {
                SystemProperties.set("persist.sys.scannout","");
            }
        }else if (preference == sound_out){
            if ((Boolean) o){
                SystemProperties.set("persist.sys.soundout","true");
            }else {
                SystemProperties.set("persist.sys.soundout","false");
            }
        }else if (preference == shock_out){
            if ((Boolean) o){
                SystemProperties.set("persist.sys.shockout","true");
            }else {
                SystemProperties.set("persist.sys.shockout","false");
            }
        }else if (preference == enter_out){
            if ((Boolean) o){
                SystemProperties.set("persist.sys.enterout","true");
            }else {
                SystemProperties.set("persist.sys.enterout","false");
            }
        }else if (preference == scann_module_onoff){
			if ((Boolean) o){
				SystemProperties.set("persist.sys.module","true");
                System.out.println("open");
				scann_onoff.setEnabled(true);
                //ScannController.ScannOpen();
                
            }else {
				SystemProperties.set("persist.sys.module","false");
                System.out.println("close");
				scann_onoff.setEnabled(false);
				scann_onoff.setChecked(false);
				ScannerService.stopRread();
                ScannController.ScannClose();
                SystemProperties.set("persist.sys.scann","false");
            }
		}
        return true;
    }


    private AlertDialog.Builder ap;
    private Dialog androidup;
    private EditText et_broad_action;
    private void setBoradAction (){
        ap = new AlertDialog.Builder(getActivity());
        ap.setTitle("设置");
        ap.setMessage("广播动作：");
        et_broad_action = new EditText(getActivity());
        ap.setView(et_broad_action);
        ap.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String aciont = et_broad_action.getText().toString().trim();
                if (aciont.equals("")){

                }else {
                    broad_action_out.setSummary(aciont);
                    SystemProperties.set("persist.sys.actiontext",aciont);
                }
            }
        });
        androidup = ap.create();
        androidup.show();
    }

    private void setBoradData (){
        ap = new AlertDialog.Builder(getActivity());
        ap.setTitle("设置");
        ap.setMessage("广播数据标签：");
        et_broad_action = new EditText(getActivity());
        ap.setView(et_broad_action);
        ap.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String aciont = et_broad_action.getText().toString().trim();
                if (aciont.equals("")){

                }else {
                    broad_data_out.setSummary(aciont);
                    SystemProperties.set("persist.sys.actiondata",aciont);
                }
            }
        });
        androidup = ap.create();
        androidup.show();
    }
}
