package com.android.settings.scann;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.hardware.ddi.DdiManager;
import android.os.Build;
import android.provider.Settings.Secure;
import android.content.Intent;


/**
 * Created by root on 16-12-19.
 */

public class ScannOpenBroadcastReceiver extends BroadcastReceiver {

    private final String SCANN_ACTION = "com.jl.scanns";
	private final String GET_SYS_VERSION = "com.jl.getsysversion";
	
	private boolean mPackageInstallFlags;


    public void onReceive(Context context, Intent intent) {

		System.out.println("action === " + intent.getAction());

       if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)){
            String scannout = SystemProperties.get("persist.sys.scannout");
            System.out.println("sys.scannout === " + scannout);
            if (scannout.equals("") || scannout == null){
                SystemProperties.set("persist.sys.scannout","key");
            }

           DdiManager ddim = DdiManager.getInstance();
           byte[] data1 = new byte[2050];
           int [] data2 = new int[10];
           int ret = ddim.ddiSysGetConfig(data1,data2);
           System.out.println("ret == " + ret);
           String sysconfiginfo = "";
           sysconfiginfo = new String(data1);
		   //System.out.println(sysconfiginfo);

           if (ret == 0){
               if(sysconfiginfo.contains("EM3096")){
                   SystemProperties.set("persist.sys.scannconfig","EM3096");
                   System.out.println("EM3096");
               }else if (sysconfiginfo.contains("N3680")) {
                   SystemProperties.set("persist.sys.scannconfig","N3680");
                   System.out.println("=N3680=");
               }else if (sysconfiginfo.contains("EM1399")){
                   SystemProperties.set("persist.sys.scannconfig","EM1399");
                   System.out.println("EM1399");
               }else if (sysconfiginfo.contains("SD-MG1S02")){
                   SystemProperties.set("persist.sys.scannconfig","SD-MG1S02");
                   System.out.println("SD-MG1S02");
               }
			   else
			   {
				 SystemProperties.set("persist.sys.scannconfig","xxxxxxx");
				 System.out.println("no scanmode!");  
				   
			   }
           }else {
               System.out.println("get sysconfiginfo error");
           }

            Intent starScann = new Intent(context,ScannerService.class);
            context.startService(starScann);
        } else if (intent.getAction().equals(SCANN_ACTION)){
            ScannController.ScannCode();
        } else if (intent.getAction().equals(GET_SYS_VERSION)){
			mPackageInstallFlags = Secure.getInt(context.getContentResolver(),
							Secure.SET_INSTALL_VERIFICATION, 1) == 1;
			//版本号
			System.out.println("ap version === "+Build.DISPLAY);
			String versionstr = Build.DISPLAY;
			System.out.println("version === "+versionstr);
			if (mPackageInstallFlags){
				versionstr = versionstr.replace("D","U");  
				System.out.println("sig === "+versionstr);
			}else {
				versionstr = versionstr.replace("U","D");  
				System.out.println("no sig === "+versionstr);
			}
			Intent intentver = new Intent("com.jl.sendsysversion");
			intentver.putExtra("sysversion", versionstr);
			context.sendBroadcast(intentver);
		}

    }
}
