package com.android.settings.scann;

import com.uatr.UART;

import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import android.os.SystemProperties;

public class ScannController {
    private static final byte SYN = 22;
    private static final byte T = 84;
    private static final byte U = 85;
    private static final byte CR = 13;
    private static byte[] startLight = new byte[]{(byte)22, (byte)84, (byte)13};
    private static byte[] stopLight = new byte[]{(byte)22, (byte)85, (byte)13};
	private static byte[] xdlstartLight = new byte[]{0x1b,0x31};
    private static byte[] xdlstopLight = new byte[]{0x1b,0x30};
	private static byte[] xdlonestart = new byte[]{ 0x7E, 0x00, 0x08, 0x01, 0x00, 0x02, 0x01, 0x02,(byte) 0xDA };
    private static byte[] systart = new byte[]{ 0x16, 0x54, 0x0d };
    private static File power = new File("/sys/devices/soc.0/78ba000.spi/spi_master/spi0/spi0.0/spidev-k21/spidev-k21-0.0/scaner_power");
    private static File openCom = new File("/sys/devices/soc.0/78b0000.serial/multi_uart");
    private static File p800Poweron = new File("/sys/bus/platform/devices/extpwr.0/driver/extpwr_val");
    private static File p800Poweroff = new File("/sys/bus/platform/devices/extpwr.0/driver/extpwr_val");
    private static FileWriter writer = null;
    private static ScannController scann;
    private static byte[] readdata = new byte[512];
    private static byte[] readataalen = new byte[5];
    private static String fileNames500 = "/dev/ttyHSL1";
    private static String fileNamep800 = "/dev/ttyMT1";
    private static Timer timer;
	private static String getscannconfig = "";
	
	static{
		String scannconfig = SystemProperties.get("persist.sys.scannconfig");
		System.out.println("scannController + " + scannconfig);
		if (scannconfig.equals("EM3096")){
			getscannconfig = "EM3096";
		}else if (scannconfig.equals("N3680")) {
			getscannconfig = "N3680";
		}else if (scannconfig.equals("EM1399")) {
			getscannconfig = "EM1399";
		}else if (scannconfig.equals("SD-MG1S02")){
            getscannconfig = "SD-MG1S02";
        }
	}

    public ScannController() {
    	
    }

	
    public static boolean ScannOpen() {
		boolean open = false;
		if (getscannconfig.equals("EM3096") || getscannconfig.equals("SD-MG1S02")){
			open = UART.rs232_OpenPort(fileNames500, 9600, 'N', 8, 1);
		}else if (getscannconfig.equals("N3680")) {
			open = UART.rs232_OpenPort(fileNames500, 115200, 'N', 8, 1);
		}else if (getscannconfig.equals("EM1399")) {
			open = UART.rs232_OpenPort(fileNames500, 115200, 'N', 8, 1);
			System.out.println("em1399 115200");
		}
        
        if(open) {
            writeSysDev(power, "1");
            //writeSysDev(openCom, "1");
            return true;
        } else {
            return false;
        }
    }

    public static void ScannClose() {
        UART.rs232_ClosePort();
        writeSysDev(power, "0");
        //writeSysDev(openCom, "0");
    }

    public static void ScannPowerOn() {
        writeSysDev(power, "1");
    }

    public static void ScannPowerOff() {
        writeSysDev(power, "0");
    }

    private static void writeSysDev(File file, String value) {
        try {
            writer = new FileWriter(file);
            writer.write(value);
            writer.flush();
            writer.close();
        } catch (Exception var3) {
            var3.printStackTrace();
        }

    }

    public static void ScannCode() {
		if (getscannconfig.equals("EM3096")){
			//UART.rs232_WritePort(xdlstopLight, xdlstopLight.length);
			UART.rs232_WritePort(xdlstartLight, xdlstartLight.length);
			//System.out.println("====ScannCode====1");
		}else if (getscannconfig.equals("N3680")) {
			UART.rs232_WritePort(stopLight, stopLight.length);
			UART.rs232_WritePort(startLight, startLight.length);
			//System.out.println("====ScannCode====2");
		}else if (getscannconfig.equals("EM1399")){
			UART.rs232_WritePort(xdlonestart, xdlonestart.length);
		}else if (getscannconfig.equals("SD-MG1S02")){
            UART.rs232_WritePort(systart, systart.length);
        }
       
        //System.out.println("====ScannCode====");
    }

    public static void ContinuousScannCode(int time) {
        timer = new Timer();
        if(time < 200) {
            timer.schedule(new TimerScann(), 200, 200);
        } else {
            timer.schedule(new TimerScann(), time, time);
        }

    }

    public static void StopContinuousScannCode() {
        UART.rs232_WritePort(stopLight, stopLight.length);
        if(timer != null) {
            timer.cancel();
            timer = null;
        }

    }

    public static byte[] ScannReadData() {
        Arrays.fill(readdata, (byte)0);
        UART.rs232_ReadPort(readataalen, readdata, 0);
        //System.out.println("req === " + (new String(readdata)).trim());
        return readdata;
    }

    private static class TimerScann extends TimerTask {
        private TimerScann() {
        }

        public void run() {
            UART.rs232_WritePort(stopLight, stopLight.length);
            UART.rs232_WritePort(startLight, startLight.length);
        }
    }
}
