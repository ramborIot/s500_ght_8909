package com.android.settings.updatesys;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.json.JSONObject;

import com.google.gson.Gson;
//import com.jl.smartentry.app.ReadCardUtils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author fancheng
 * @date 2012-4-26
 * @blog http://blog.92coding.com
 */

public class UpdateManager {

	private static final int DOWNLOAD = 1;

	private static final int DOWNLOAD_FINISH = 2;

	private static final int UPDATEMSG = 3;

	private static final int appCode = 1;

	//HashMap<String, String> mHashMap;

	private String mSavePath;

	private int progress;

	private boolean cancelUpdate = false;

	private Context mContext;

	private ProgressBar mProgress;
	private Dialog mDownloadDialog;

	private static boolean isTest = false;

	//private final static String urlpatt = isTest ? "http://192.168.3.136:8080/JlSystemUpdate"
	//		: "http://120.76.134.101:18888/JlSystemUpdate";
	private final static String urlpatt = "http://120.76.134.101:888/guanghetong8909/ota.zip ";

	public final static String updateURL = urlpatt + "/UpdateServerlet";
    //private TextView  showprogress;
	private updateObj   update ; 
	private JSONObject  mJson;
/*
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case DOWNLOAD:

				//System.err.println(" kkkkk " +progress);
				//showprogress.setText(progress + "%");
				//showprogress.invalidate();
				//mProgress.setProgress(progress);
				break;
			case DOWNLOAD_FINISH:
				installApk();
				break;
			case UPDATEMSG:
				try {

					JSONObject json = new JSONObject(msg.obj.toString());
					int result = json.getInt("result");
					if (result == 1) {
					
						
						 Gson gson= new Gson() ;
						update= gson.fromJson(json.toString(), updateObj.class) ; 
						//showNoticeDialog(update.getVersionName());
						mSavePath = Environment.getExternalStorageDirectory()
								+ "/";
						//mSavePath = sdpath ;//+ "Download/";
						File apkFile = new File(mSavePath, update.getAppName());
						 if(apkFile.exists())
							 apkFile.deleteOnExit();
					}

				} catch (Exception e) {
					// TODO: handle exception
				}
				break;
			default:
				break;
			}
		};
	};

*/

	public UpdateManager(Context context) {
		this.mContext = context;
	}

	/**
	 *
	 */
	//public  checkUpdate() {
	//	isUpdate();
	//}

	/**
	 *
	 * @return
	 */
	public boolean isUpdate() {
		updateObj obj = new updateObj();
		obj.setAppVersion(1);
		obj.setAppCode(appCode);
		Gson gson1= new Gson() ;
		String version = getVersionFromHttp(gson1.toJson(obj));
		try {
			mJson = new JSONObject(version);
			Gson gson= new Gson() ;
			update= gson.fromJson(mJson.toString(), updateObj.class); 
			//showNoticeDialog(update.getVersionName());
			
			mSavePath = Environment.getExternalStorageDirectory()
					+ "/";
			//mSavePath = sdpath ;//+ "Download/";
			File apkFile = new File(mSavePath, update.getAppName());
			if(apkFile.exists())
				 apkFile.deleteOnExit();

			int result = mJson.getInt("result");
			if (result == 1) {
				return true;
			}
		} catch (Exception e) {
			
		} 
		
		return false;
	}

	/**
	 *
	 * 
	 * @param context
	 * @return
	 */
	private int getVersionCode(Context context) {
		int versionCode = 0;
		try {

			versionCode = context.getPackageManager().getPackageInfo(
					"com.jl.smartentry", 0).versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return versionCode;
	}

	/**
	 *
	 */
/*	private void showNoticeDialog(String version) {

		AlertDialog.Builder builder = new Builder(mContext);
		builder.setTitle(R.string.soft_update_title);
		 String format=mContext.getString(R.string.soft_update_info);
		builder.setMessage(String.format(format, version));

		builder.setPositiveButton(R.string.soft_update_updatebtn,
				new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();

						showDownloadDialog();
					}
				});

		builder.setNegativeButton(R.string.soft_update_later,
				new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		Dialog noticeDialog = builder.create();
		noticeDialog.show();
	}
*/
	/**
	 *
	 */
/*	private void showDownloadDialog() {

		AlertDialog.Builder builder = new Builder(mContext);
		builder.setTitle(R.string.soft_update_title);
		final LayoutInflater inflater = LayoutInflater.from(mContext);
		View v = inflater.inflate(R.layout.softupdate_progress, null);
		mProgress = (ProgressBar) v.findViewById(R.id.update_progress);
		showprogress=(TextView)v.findViewById(R.id.showprogress);
		builder.setView(v);
		builder.setNegativeButton(R.string.cancel, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				cancelUpdate = true;
			}
		});
		mDownloadDialog = builder.create();
		mDownloadDialog.show();
		downloadApk();
	}
*/


	public boolean downloadOtaFile() {
		try {
			if (Environment.getExternalStorageState().equals(
					Environment.MEDIA_MOUNTED)) {
		
				mSavePath = Environment.getExternalStorageDirectory()
						+ "/";
				//mSavePath = sdpath ;//+ "Download/";
				URL url = new URL( urlpatt);//+  update.getAppDownPath());
		
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.connect();
				int code = conn.getResponseCode();
				if (code != 200){
					return false;
				}
		
				int length = conn.getContentLength();
		
				InputStream is = conn.getInputStream();

				

				File file = new File(mSavePath);
		
				if (!file.exists()) {
					file.mkdir();
					file.canRead() ;
					file.canWrite() ;
				}
				File apkFile = new File(mSavePath, "slm753-ota-eng.Data.BU.zip");//"slm753-ota-eng.Data.BU.zip");//update.getAppName());
		
				FileOutputStream fos = new FileOutputStream(apkFile);
				int count = 0;
		
				byte buf[] = new byte[51200];
		
				do {
					int numread = is.read(buf);
					count += numread;
		
					progress = (int) (((float) count / length) * 100);
		
					//mHandler.sendEmptyMessage(DOWNLOAD);
					if (numread <= 0) {
		
						//mHandler.sendEmptyMessage(DOWNLOAD_FINISH);
						break;
					}
		
					fos.write(buf, 0, numread);
				} while (!cancelUpdate);//
				fos.close();
				is.close();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}



	/**
	 *
	 */
	private void installApk() {
		File apkfile = new File(mSavePath, update.getAppName());
		if (!apkfile.exists()) {
			return;
		}
	
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setDataAndType(Uri.parse("file://" + apkfile.toString()),
				"application/vnd.android.package-archive");
		mContext.startActivity(i);
	}

	public byte[] readStream(InputStream inStream) throws Exception {
		ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = -1;
		while ((len = inStream.read(buffer)) != -1) {
			outSteam.write(buffer, 0, len);
		}
		outSteam.close();
		inStream.close();
		return outSteam.toByteArray();
	}

	private String getVersionFromHttp(final String  json) {

		HttpURLConnection connection = null;
		OutputStream out = null;
		InputStream in = null;
		try {
			// System.err.println("kkk "+ json);
			URL url = new URL(updateURL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Charset", "UTF-8");
			// connection.setRequestProperty("MyProperty",
			// "this is me!");
			connection.setDoOutput(true);
			out = connection.getOutputStream();
			out.write(json.getBytes());

			int code = connection.getResponseCode();
			if (code == 200) {
				in = connection.getInputStream();
				byte[] bb = readStream(in);

				String date = new String(bb, "UTF-8").trim();

				// JSONObject json= new JSONObject( date);
				//Message ms = mHandler.obtainMessage();
				//ms.obj = date;

				//ms.what = UPDATEMSG;
				//mHandler.sendMessage(ms);
				return date;
			}

			//
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.disconnect();

				if (out != null)
					out.close();
				if (in != null)
					in.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
