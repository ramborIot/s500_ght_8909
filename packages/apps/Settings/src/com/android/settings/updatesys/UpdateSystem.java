package com.android.settings.updatesys;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.RecoverySystem;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;

import com.android.settings.R;
import com.android.settings.SettingsPreferenceFragment;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.ComponentName;


/**
 * Created by root on 16-12-20.
 */

public class UpdateSystem extends SettingsPreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String KEY_ANDROID_UPDATE = "android_update";
    private static final String KEY_SP_UPDATE = "sp_update";
	private Context mContext;
    private Preference androidpf;
    private Preference sppf;
    private localUpdateTask aduptastk;
	private remoteUpdateTask  remotetastk;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.updatesys);
		mContext = getActivity();
        initUI();

    }

    private void initUI() {
        androidpf = findPreference(KEY_ANDROID_UPDATE);
        sppf = findPreference(KEY_SP_UPDATE);
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        System.out.println("=====onPreferenceTreeClick=====");
        if (preference == androidpf) {
            androidUpdateDialog ();
        }else if (preference == sppf){
			Intent intent = new Intent(Intent.ACTION_MAIN);  
			intent.addCategory(Intent.CATEGORY_LAUNCHER);              
			ComponentName cn = new ComponentName("com.jl.updatesp", "com.jl.updatesp.MainActivity");              
			intent.setComponent(cn);  
			mContext.startActivity(intent); 
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }
	
	

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {

    }


    private AlertDialog.Builder ap;
    private Dialog androidup;
    private Button bt_loadupdate;
    private Button bt_remotelyupdate;
    private TextView tv_showinfo;
    private void androidUpdateDialog (){
        ap = new AlertDialog.Builder(getActivity());
        bt_loadupdate = new Button(getActivity());
        LinearLayout linearLayout = new LinearLayout(getActivity());
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 120);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        bt_loadupdate.setText(getString(R.string.local_update));
        bt_remotelyupdate = new Button(getActivity());
        bt_remotelyupdate.setText(getString(R.string.remote_update));
        tv_showinfo = new TextView(getActivity());
        linearLayout.addView(bt_loadupdate);
        linearLayout.addView(bt_remotelyupdate);
        linearLayout.addView(tv_showinfo);
        ap.setView(linearLayout);
        bt_loadupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("local_update");
                    aduptastk = new localUpdateTask();
                    //aduptastk.execute();
                    aduptastk.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
            }
        });

		bt_remotelyupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("remote_update");
                    remotetastk = new remoteUpdateTask();
                    //aduptastk.execute();
                    remotetastk.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
            }
        });
        ap.setTitle(getString(R.string.system_update));
        ap.setMessage(getString(R.string.update_mode));
        System.out.println("androidUpdateDialog");
        androidup = ap.create();
        androidup.show();
    }

    private void spUpdateDialog (){

    }

    private class localUpdateTask extends AsyncTask<String, Integer, String>{

        private String m_packageFileName = "";

        //
        protected void onPreExecute() {
            androidup.setCancelable(false);
            bt_remotelyupdate.setVisibility(View.GONE);
			bt_loadupdate.setEnabled (false);
            tv_showinfo.setTextSize(20);
            tv_showinfo.setText(getString(R.string.update_system_is_finding_file));
            tv_showinfo.setGravity(Gravity.CENTER);
        }
        //
        protected String doInBackground(String... params) {
            System.out.println("======doInBackground======");
            String sDStateString = Environment.getSecondaryStorageState();
            System.out.println("sd === " + sDStateString);
            if(sDStateString.equals(Environment.MEDIA_MOUNTED))
            {
                File SDFile = Environment.getSecondaryStorageDirectory();
				System.out.println("SDFile === " + SDFile.toString());
                File sdPath = new File(SDFile.getAbsolutePath());
                if(sdPath.listFiles().length > 0)
                {
                    for(File file : sdPath.listFiles())
                    {
                        System.out.println("name-->"+file.getName());
                        if (file.getName().equals("slm753-ota-eng.Data.BU.zip")){
                            m_packageFileName = file.getName();
                            System.out.println("找到文件 === " + m_packageFileName);
                            publishProgress(1);
                            break;
                        }
                    }
                }
            }
            if(m_packageFileName != ""){
                try {
                    System.out.println("进入更新");
					File file = new File("/sdcard/" + m_packageFileName);
					System.out.println("filepath === " + file.toString());
					String filename = file.getCanonicalPath();
					System.out.println("filename === " + filename);
					filename = file.getAbsolutePath();
					System.out.println("filename === " + filename);
					filename = file.getPath();
					System.out.println("filename === " + filename);
                    RecoverySystem.installPackage(getActivity(), file);
                    System.out.println("退出更新");
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            return "";
        }

        // 时时更新UI
        protected void onProgressUpdate(Integer... progresses) {
            int index = progresses[0];
            if (index == 1) {
                tv_showinfo.setText(getString(R.string.update_system_is_updateing));
            }

        }

        // 后台任务执行完毕，更新UI
        protected void onPostExecute(String result) {
            tv_showinfo.setText(getString(R.string.update_system_no_file_finded));
            androidup.setCancelable(true);
        }

    }

	private class remoteUpdateTask extends AsyncTask<String, Integer, String>{

        private String m_packageFileName = "";

        //
        protected void onPreExecute() {
            androidup.setCancelable(false);
            bt_loadupdate.setVisibility(View.GONE);
			bt_remotelyupdate.setEnabled (false);
            tv_showinfo.setTextSize(20);
            tv_showinfo.setText(getString(R.string.update_system_is_download_file));
            tv_showinfo.setGravity(Gravity.CENTER);
        }
        //
        protected String doInBackground(String... params) {
            System.out.println("======doInBackground======");
            String sDStateString = Environment.getExternalStorageState();
			UpdateManager updateManager = new UpdateManager(getActivity());
			//if (!updateManager.isUpdate()) {
			//	return "not connected or current version is newest";
			//}
			
			if (!updateManager.downloadOtaFile()) {
				return "download file failed";
			}
			
            System.out.println("sd === " + sDStateString);
            if(sDStateString.equals(Environment.MEDIA_MOUNTED))
            {
                File SDFile = Environment.getExternalStorageDirectory();
				System.out.println("SDFile === " + SDFile.toString());
                File sdPath = new File(SDFile.getAbsolutePath());
                if(sdPath.listFiles().length > 0)
                {
                    for(File file : sdPath.listFiles())
                    {
                        System.out.println("name-->"+file.getName());
                        if (file.getName().equals("slm753-ota-eng.Data.BU.zip")){
                            m_packageFileName = file.getName();
                            System.out.println("找到文件 === " + m_packageFileName);
                            publishProgress(1);
                            break;
                        }
                    }
                }
            }
            if(m_packageFileName != ""){
                try {
                    System.out.println("进入更新");
					File file = new File("/data/media/0/" + m_packageFileName);
					System.out.println("filepath === " + file.toString());
					String filename = file.getCanonicalPath();
					System.out.println("filename === " + filename);
					filename = file.getAbsolutePath();
					System.out.println("filename === " + filename);
					filename = file.getPath();
					System.out.println("filename === " + filename);
                    RecoverySystem.installPackage(getActivity(), file);
                    System.out.println("退出更新");
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            return "";
        }

        // 时时更新UI
        protected void onProgressUpdate(Integer... progresses) {
            int index = progresses[0];
            if (index == 1) {
                tv_showinfo.setText(getString(R.string.update_system_is_updateing));
            }

        }

        // 后台任务执行完毕，更新UI
        protected void onPostExecute(String result) {
            tv_showinfo.setText(getString(R.string.update_system_no_file_finded));
            androidup.setCancelable(true);
        }

    }
	

}
