package com.android.settings.updatesys;

public class updateObj {
	private int id;
	private String appName;
	private int appVersion;
	private String appDownPath;
	private String appUpDateTime;
	private String canUpdate;
	private String versionName;
	private String updateReson;
	private int appCode;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public int getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(int appVersion) {
		this.appVersion = appVersion;
	}

	public String getAppDownPath() {
		return appDownPath;
	}

	public void setAppDownPath(String appDownPath) {
		this.appDownPath = appDownPath;
	}

	public String getAppUpDateTime() {
		return appUpDateTime;
	}

	public void setAppUpDateTime(String appUpDateTime) {
		this.appUpDateTime = appUpDateTime;
	}

	public String getCanUpdate() {
		return canUpdate;
	}

	public void setCanUpdate(String canUpdate) {
		this.canUpdate = canUpdate;
	}

	public String getUpdateReson() {
		return updateReson;
	}

	public void setUpdateReson(String updateReson) {
		this.updateReson = updateReson;
	}

	public int getAppCode() {
		return appCode;
	}

	public void setAppCode(int appCode) {
		this.appCode = appCode;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public String getVersionName() {
		return versionName;
	}
}
