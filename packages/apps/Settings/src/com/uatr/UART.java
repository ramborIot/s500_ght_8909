package com.uatr;

public class UART {

	static {
		System.loadLibrary("uatr");
	}

	public native static boolean rs232_OpenPort(String lpFileName, int baudRate,
			char parity, int byteSize, int stopBits);

	public static native boolean rs232_ClosePort();

	public static native boolean rs232_WritePort(byte[] WriteBytes,int intSize);

	public static native boolean rs232_ReadPort(byte[] NumBytes,byte[] commRead,int OutTimes);

}
