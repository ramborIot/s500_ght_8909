/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.contacts.common.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.android.contacts.common.ContactsUtils;
import com.android.contacts.common.CallUtil;
import android.telephony.SubscriptionManager;
import android.graphics.drawable.Drawable;
import com.android.contacts.common.R;


public class SimUtils {
    private static final String TAG = "SimUtils";
    private static final boolean DEBUG = true;

    private static final String SIM_CONTENT_URI = "content://icc/adn";
    private static final String SIM_CONTENT_URI_WITH_SUBID = "content://icc/adn/subId/";

    public static final int DSDS_SLOT_1_ID = 0;
    public static final int DSDS_SLOT_2_ID = 1;
    public static final int DSDS_INVALID_SLOT_ID = SubscriptionManager.INVALID_SIM_SLOT_INDEX;

    private static final int MAX_ADN_NUMBER_LENGTH = 20;
    private static final int ADN_RECORD_FOOTER_SIZE_BYTES = 14;

    public static final int SIM_TYPE_UNKNOWN = 0;
    public static final int SIM_TYPE_SIM = 1;
    public static final int SIM_TYPE_USIM = 2;

    public static Uri getIccUri(int slotId) {

        if (DEBUG) Log.d(TAG, "getIccUri - slotId = " + slotId);

        int subId = getSubIdbySlotId(slotId);
        if (DEBUG) Log.d(TAG, "getIccUri - subId = " + subId);

        if (subId != SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
            return Uri.parse(SIM_CONTENT_URI_WITH_SUBID + subId);
        } else {
            return Uri.parse(SIM_CONTENT_URI);
        }
    }

    public static int getSubIdbySlotId(int slotId) {

        if (DEBUG) Log.d(TAG, "getSubIdbySlotId - slotId = " + slotId);

        int[] subIds = SubscriptionManager.getSubId(slotId);

        return (subIds != null) && (subIds.length > 0)
                        ? subIds[0] : SubscriptionManager.getDefaultSubId();
    }

    public static int getSimCapacity(Context context, int index) {
        if (context == null) {
            if (DEBUG) Log.d(TAG, "getSimCapacity - context is null!");
            return -1;
        }
        return -1;
    }

    public static int getFreeEntriesNumber(Context context, int index) {
        if (context == null) {
            if (DEBUG) Log.d(TAG, "getFreeEntriesNumber - context is null!");
            return -1;
        }
       return -1;
    }

    public static int getMaxNameLength(int slotId) {
        return ADN_RECORD_FOOTER_SIZE_BYTES;
    }

    public static int getMaxNumberLength(int slotId) {
        return MAX_ADN_NUMBER_LENGTH;
    }

    public static String getSimImsi(Context context, int index) {
        return null;
    }

    public static int getSimType(int slotid) {
                return SIM_TYPE_UNKNOWN;
    }

    public static int getSimState(int slotId) {
        return  TelephonyManager.getDefault().getSimState(slotId);
    }


    public static boolean isSimAccessible(Context context, int slotId) {

        return getSimState(slotId) == TelephonyManager.SIM_STATE_READY;
    }

    private boolean isSimEnabled(Context context, int subId) {

        if (context == null) {
            return false;
        }

        int slot = SubscriptionManager.getPhoneId(subId);
        int simEnabled = android.provider.Settings.Global.getInt(
                context.getContentResolver(),
                android.provider.Settings.Global.SIM_MODE_ENABLED + slot, 1);

        return simEnabled == 1;
    }

    public static boolean isSim1Ready(Context context) {
        int state = getSimState(DSDS_SLOT_1_ID);
        return state == TelephonyManager.SIM_STATE_READY;
    }

    public static boolean isSim2Ready(Context context) {
        int state = getSimState(DSDS_SLOT_2_ID);
        return state == TelephonyManager.SIM_STATE_READY;
    }

    public static Drawable getSimIconDrawable(Context context, int slotId) {

        if (context == null) {
            return null;
        }

        if (DEBUG) Log.d(TAG, "getSimIconDrawable slotId:" + slotId);

        Drawable drawable = null;

        switch (slotId) {
            case DSDS_SLOT_1_ID:
                drawable = context.getResources().getDrawable(R.drawable.ic_sim_card_1);
                drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                break;
            case DSDS_SLOT_2_ID:
                drawable = context.getResources().getDrawable(R.drawable.ic_sim_card_2);
                drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                break;
            default:
                drawable = null;
                break;
        }

        return drawable;
    }

}
