package com.android.contacts.quickcontact;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.RawContacts.Data;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.android.contacts.common.ContactPresenceIconUtil;
import com.android.contacts.common.ContactsUtils;
import com.android.contacts.common.util.SimUtils;
import com.android.contacts.R;

//Added by chenyb 2016-10-29 for sim contacts begin
public class SyncSimContactsService extends Service {
    private static final int MESSAGE_BOOT = 1001;
    private static final int MESSAGE_SIM1_USER = 1002;
    private static final int MESSAGE_SIM2_USER = 1003;
    private static final int MESSAGE_SIM1_NOT_USER = 1004;
    private static final int MESSAGE_SIM2_NOT_USER = 1005;

    private static final String CONTATC_NAME = "name";
    private static final String CONTATC_NUMBER = "number";
    private static final String mSecondUri = "content://icc2/adn";
    private static final String mPrimaryUri = "content://icc/adn";
    private int mBroadcastFlag;
    private MyHandler myHandler;
    public static final int DEFAULT_DATA_SIM = SimUtils.DSDS_SLOT_1_ID;

    private boolean ifStopInsertSim1 = false;
    private boolean ifStopInsertSim2 = false;


    /**
     * If SIM contact INSERT finished, will send the broadcast with this action.<br>
     * <b>Notice</b> This broad will take the result params uri(key="result")
     * and slotId(key="slot"):
     *
     */
    public static final String ACTION_INSERT_SIM_CONTACT_FINISHED = "com.android.icc.INSERT_CONTACT";

    /**
     * If SIM contact UPDATE finished, will send the broadcast with this action.<br>
     * <b>Notice</b> This broad will take the result params count(key="result")
     * and slotId(key="slot"):
     */
    public static final String ACTION_UPDATE_SIM_CONTACT_FINISHED = "com.android.icc.UPDATE_CONTACT";

    /**
     * If SIM contact DELETE finished, will send the broadcast with this action.<br>
     * <b>Notice</b> This broad will take the result params count(key="result")
     * and slotId(key="slot"):
     */
    public static final String ACTION_DELETE_SIM_CONTACT_FINISHED = "com.android.icc.DELETE_CONTACT";

    public static final String KEY_FLAG = "flag";
    public static final String KEY_SLOT_ID = "slot_id";
    public static final String KEY_CONTACT_ID = "contact_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_NUMBER = "number";
    public static final String KEY_RESULT = "result";

    public static final int MESSAGE_INSERT_SIM_CONTACT = 2001;
    public static final int MESSAGE_UPDATE_SIM_CONTACT = 2002;
    public static final int MESSAGE_DELETE_SIM_CONTACT = 2003;

    private IccContactOperation mIccContactOperation;

    private int mSlot;
    private int mSimState;

    @Override
    public void onCreate() {
        super.onCreate();

        HandlerThread handlerThread = new HandlerThread("handlerThread");
        handlerThread.start();
        myHandler = new MyHandler(handlerThread.getLooper(), new Handler());

        mIccContactOperation = new IccContactOperation(this, this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.d(TAG, "onStartCommand() intent = " + intent);
        if (intent != null) {
            mBroadcastFlag = intent.getIntExtra("broadcast_flag", 0);
            mSlot = intent.getIntExtra("slot", 0);
            mSimState = intent.getIntExtra("sim_state", 0);
            int flag = intent.getIntExtra(KEY_FLAG, 0);
            Log.d(TAG, "intent: mBroadcastFlag = " + mBroadcastFlag
                    + ", mSlot = " + mSlot + ", mSimState = " + mSimState
                    + ", flag = " + flag);
            switch (flag) {
            case MESSAGE_INSERT_SIM_CONTACT:
            case MESSAGE_UPDATE_SIM_CONTACT:
            case MESSAGE_DELETE_SIM_CONTACT:
                Message message = new Message();
                message.what = flag;
                message.obj = intent;
                myHandler.sendMessage(message);
//                        return super.onStartCommand(intent, flags, startId);
                break;
            default:
                if (mBroadcastFlag == 0) {
                    myHandler.sendEmptyMessage(MESSAGE_BOOT);
                } else {
                    if (mSlot == 0) {
                        switch (mSimState) {
                        case TelephonyManager.SIM_STATE_UNKNOWN:
                        case TelephonyManager.SIM_STATE_ABSENT:
                        case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                        case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                        case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                            myHandler.sendEmptyMessage(MESSAGE_SIM1_NOT_USER);
                            break;
                        case TelephonyManager.SIM_STATE_READY:
                            myHandler.sendEmptyMessage(MESSAGE_SIM1_USER);
                            break;
                        default:
                            break;
                        }
                    }else {
                        switch (mSimState) {
                        case TelephonyManager.SIM_STATE_UNKNOWN:
                        case TelephonyManager.SIM_STATE_ABSENT:
                        case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                        case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                        case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                            myHandler.sendEmptyMessage(MESSAGE_SIM2_NOT_USER);
                            break;
                        case TelephonyManager.SIM_STATE_READY:
                            myHandler.sendEmptyMessage(MESSAGE_SIM2_USER);
                            break;
                        default:
                            break;
                        }
                    }
                }
                break;
            }
        }
        return START_STICKY;//super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "jsm onDestroy");
        Intent intent = new Intent("android.intent.action.SYNC_SIM_CONTACTS");
        startService(intent);
    }

    private void updateUI() {
        Intent intent = new Intent();
        intent.setAction("com.android.intent.updateUI");
        sendBroadcast(intent);
    }

    public void deleteSimCardContacts(ArrayList<ContentProviderOperation> operationList) {
        operationList.add(ContentProviderOperation.newDelete(RawContacts.CONTENT_URI)
                .withSelection("raw_contact_sim_contact>?", new String []{String.valueOf(0)}).build());
    }

    public void deleteSimCard1Contacts(ArrayList<ContentProviderOperation> operationList) {
        operationList.add(ContentProviderOperation.newDelete(RawContacts.CONTENT_URI)
                .withSelection("raw_contact_sim_contact=?", new String []{String.valueOf(1)}).build());
    }

    public void deleteSimCard2Contacts(ArrayList<ContentProviderOperation> operationList) {

        operationList.add(ContentProviderOperation.newDelete(RawContacts.CONTENT_URI)
                .withSelection("raw_contact_sim_contact=?", new String []{String.valueOf(2)}).build());

    }

    // 获取SIM卡信息
    private void getSimCardInfo(ArrayList<ContentProviderOperation> operationList) {
        // 判断SIM卡是否可用
        Log.d(TAG, "getSimCardInfo isSim1CanUser():" + isSim1CanUser(this));
        Log.d(TAG, "getSimCardInfo isSim2CanUser():" + isSim2CanUser(this));
        //boolean isSim1PrimarySim = isPrimarySim(0);
        //android.util.Log.d("chenyb", "isSim1PrimarySim -> " + isSim1PrimarySim);
        if (isSim1CanUser(this)) {
            getSimCardInfo(operationList, 0);
        }

        if (isSim2CanUser(this)) {
            getSimCardInfo(operationList, 1);
        }
    }

    private void getSimCardInfo(ArrayList<ContentProviderOperation> operationList, int slot) {
        Cursor cursor = null;
        try {
            Uri uri = SimUtils.getIccUri(slot);
            String[] projection = { "_id", CONTATC_NAME, CONTATC_NUMBER };
            cursor = getContentResolver().query(uri, projection, null, null, CONTATC_NAME);

            Log.d(TAG, "jsm getSimCardInfo " + Arrays.toString(cursor.getColumnNames()));
            Log.d(TAG, "jsm getSimCardInfo cursor.getCount():" + cursor.getCount());
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String name = cursor.getString(cursor.getColumnIndex(CONTATC_NAME));
                    String number = cursor.getString(cursor.getColumnIndex(CONTATC_NUMBER));
                    // 插入到本地数据库
                    if(slot == 0 && !ifStopInsertSim1 || slot == 1 && !ifStopInsertSim2) {
                        insertIntoData(operationList, slot + 1, name, number);
                    }
                }
            }
        }catch(Exception e){
            Log.e(TAG, "jsm query exception" ,e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

    }

    public void insertIntoData(ArrayList<ContentProviderOperation> operationList, int simcard, String name, String number) {
        try {
            int backRef = operationList.size();

            if(backRef >= 496) {
                applyBatch(operationList);
                backRef = operationList.size();
            }

            operationList.add(ContentProviderOperation.newInsert(RawContacts.CONTENT_URI)
                    .withValue(RawContacts.AGGREGATION_MODE, RawContacts.AGGREGATION_MODE_DISABLED)
                    .withValue("raw_contact_sim_contact", simcard).build());
            operationList.add(ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(StructuredName.RAW_CONTACT_ID, backRef)
                    .withValue(android.provider.ContactsContract.Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(StructuredName.FAMILY_NAME, name)
                    .build());
            operationList.add(ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(Phone.RAW_CONTACT_ID,  backRef)
                    .withValue(android.provider.ContactsContract.Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
                    .withValue(Phone.TYPE,  Phone.TYPE_MOBILE)
                    .withValue(Phone.NUMBER,  number)
                    .withValue(android.provider.ContactsContract.Data.IS_PRIMARY, 1)
                    .build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isSim1CanUser(Context context) {
        Log.d(TAG, "isSim1CanUser ContactsUtils.isDualSimSupported():"
                + ContactsUtils.isDualSimSupported(context));
        Log.d(TAG, "isSim1CanUser SimUtils.isSim1Ready(this):"
                + SimUtils.isSim1Ready(context));
        //if (ContactsUtils.isDualSimSupported(context)) {
            if (SimUtils.isSim1Ready(context)) {
                return true;
            } else {
                return false;
            }
            //Added by tangxiaoshuang  150624 BEGIN
        //} else {
            //if (SimUtils.isPrimarySimAccessible(context)) {
            //    return true;
            //}
            //Added by tangxiaoshuang 150624 END
        //}
        //return false;
    }

    public static boolean isSim2CanUser(Context context) {
        Log.d(TAG, "isSim2CanUser ContactsUtils.isDualSimSupported():"
                + ContactsUtils.isDualSimSupported(context));
        Log.d(TAG, "isSim2CanUser SimUtils.isSim2Ready(this):"
                + SimUtils.isSim2Ready(context));
        if (ContactsUtils.isDualSimSupported(context)) {
            if (SimUtils.isSim2Ready(context)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /*
    public boolean isPrimarySim(int slot) {
        return (slot == getDataSimSettings());
    }

    private int getDataSimSettings() {
        final int dataSim = Settings.Global.getInt(getContentResolver(),
                Settings.Global.MOBILE_DATA_SIM, DEFAULT_DATA_SIM);
        return dataSim;
    }
    */

    private void applyBatch(ArrayList<ContentProviderOperation> operationList) {
        Log.d(TAG, "jsm operationList = " + operationList.size());
        if(!operationList.isEmpty()) {
            try {
                ContentProviderResult[] a = getContentResolver().applyBatch(ContactsContract.AUTHORITY, operationList);
                operationList.clear();
                for (int i =0; i < a.length; i ++) {
                    Log.d(TAG, "jsm a = " + a[i].count + " " + a[i].uri);
                }
                Log.d(TAG, "jsm jsm a end");
            } catch (RemoteException e) {
                e.printStackTrace();
                Log.d(TAG, "jsm jsm a RemoteException");
            } catch (OperationApplicationException e) {
                e.printStackTrace();
                Log.d(TAG, "jsm jsm a OperationApplicationException");
            }
        }
    }

    private class MyHandler extends Handler {
        private Handler mMianhandler;

        public MyHandler() {
        }

        public MyHandler(Looper looper, Handler handler) {
            super(looper);
            mMianhandler = handler;
        }

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            System.out.println("线程Id" + Thread.currentThread().getId());
            //boolean isSim1PrimarySim = isPrimarySim(0);
            //Log.d(TAG, "handleMessage isSim1PrimarySim -> " + isSim1PrimarySim);
            final ArrayList<ContentProviderOperation> operationList =
            new ArrayList<ContentProviderOperation>();
            switch (msg.what) {
            case MESSAGE_BOOT:
                Log.d(TAG, "ServiceHandler MESSAGE_BOOT:" + MESSAGE_BOOT);
                deleteSimCardContacts(operationList);
                getSimCardInfo(operationList);
                applyBatch(operationList);
                updateUI();
                break;

            case MESSAGE_SIM1_USER:
                ifStopInsertSim1 = false;
                Log.d(TAG, "ServiceHandler MESSAGE_SIM1_USER:" + MESSAGE_SIM1_USER);
                deleteSimCard1Contacts(operationList);
                // 在插入新的联系人
                getSimCardInfo(operationList, 0);
                applyBatch(operationList);
                updateUI();
                break;

            case MESSAGE_SIM2_USER:
                ifStopInsertSim2 = false;
                deleteSimCard2Contacts(operationList);
                Log.d(TAG, "ServiceHandler MESSAGE_SIM2_USER:" + MESSAGE_SIM2_USER);
                // 在插入新的联系人
                getSimCardInfo(operationList, 1);
                applyBatch(operationList);
                updateUI();
                break;

            case MESSAGE_SIM1_NOT_USER:
                ifStopInsertSim1 = true;
                deleteSimCard1Contacts(operationList);
                Log.d(TAG, "ServiceHandler MESSAGE_SIM1_NOT_USER:"
                        + MESSAGE_SIM1_NOT_USER);
                applyBatch(operationList);
                updateUI();
                break;

            case MESSAGE_SIM2_NOT_USER:
                ifStopInsertSim2 = true;
                deleteSimCard2Contacts(operationList);
                Log.d(TAG, "ServiceHandler MESSAGE_SIM2_NOT_USER:" + MESSAGE_SIM2_NOT_USER);

                applyBatch(operationList);
                updateUI();
                break;
            case MESSAGE_DELETE_SIM_CONTACT: {
                Intent deleteIntent = (Intent) msg.obj;
                int slotId = deleteIntent.getIntExtra(KEY_SLOT_ID, 0);
                long contactId = deleteIntent.getLongExtra(KEY_CONTACT_ID, 0);

                int count = mIccContactOperation.deleteSimContact(slotId, contactId);

                notifyDeleteFinished(slotId, count);
                break;
            }
            case MESSAGE_INSERT_SIM_CONTACT: {
                Intent insertIntent = (Intent) msg.obj;
                int slotId = insertIntent.getIntExtra(KEY_SLOT_ID, 0);
                String name = insertIntent.getStringExtra(KEY_NAME);
                String number = insertIntent.getStringExtra(KEY_NUMBER);

                Uri uri = mIccContactOperation.insertSimContact(operationList, slotId, name,
                        number, true);

                applyBatch(operationList);
                notifyInsertFinished(slotId, uri);
                break;
            }
            case MESSAGE_UPDATE_SIM_CONTACT: {
                Intent updateIntent = (Intent) msg.obj;
                int slotId = updateIntent.getIntExtra(KEY_SLOT_ID, 0);
                String name = updateIntent.getStringExtra(KEY_NAME);
                String number = updateIntent.getStringExtra(KEY_NUMBER);
                long contactId = updateIntent.getLongExtra(KEY_CONTACT_ID, 0);

                int count = mIccContactOperation.updateSimContact(
                        contactId, slotId, name, number);

                notifyUpdateFinished(slotId, count);
                break;
            }
            default:
                break;
            }
        }
    }

    private static final String TAG = "SyncSimContactsService";

    private void notifyInsertFinished(int slotId, Uri uri) {
        // TODO notify insert sim contac finished.
        Log.i(TAG, "notifyInsertFinished uri = " + uri);
        Intent intent = new Intent(ACTION_INSERT_SIM_CONTACT_FINISHED);
        String uriString = uri == null ? "" : uri.toString();
        intent.putExtra(KEY_RESULT, uriString);
        intent.putExtra(KEY_SLOT_ID, slotId);
        sendBroadcast(intent);
    }

    private void notifyUpdateFinished(int slotId, int count) {
        // TODO notify update sim contact finished.
        Log.i(TAG, "notifyUpdateFinished count = " + count);
        Intent intent = new Intent(ACTION_UPDATE_SIM_CONTACT_FINISHED);
        intent.putExtra(KEY_RESULT, count);
        intent.putExtra(KEY_SLOT_ID, slotId);
        sendBroadcast(intent);
    }

    private void notifyDeleteFinished(int slotId, int count) {
        // TODO notify delete sim contact finished.
        Log.i(TAG, "notifyDeleteFinished count = " + count);
        Intent intent = new Intent(ACTION_DELETE_SIM_CONTACT_FINISHED);
        intent.putExtra(KEY_RESULT, count);
        intent.putExtra(KEY_SLOT_ID, slotId);
        sendBroadcast(intent);
    }
}
// Added by chenyb 2016-10-29 for sim contacts end
