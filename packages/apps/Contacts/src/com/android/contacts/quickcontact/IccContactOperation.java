package com.android.contacts.quickcontact;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.text.TextUtils;
import android.util.Log;
import com.android.contacts.common.util.SimUtils;

//merged by wangjianmin for sim contacts
public class IccContactOperation {
    private final String TAG = "IccContactOperation";
    public static final String SIM_COLUMN = "raw_contact_sim_contact";

    private final String NAME = "tag";
    private final String NUMBER = "number";
    private final String EMAILS = "emails";

    private final String NEW_NAME = "newTag";
    private final String NEW_NUMBER = "newNumber";

    private final Uri ICC_CONTENT_URI = Uri.parse("content://icc/adn");
    private final Uri ICC2_CONTENT_URI = Uri.parse("content://icc2/adn");

    private Context mContext;
    SyncSimContactsService mSyncSimContactsService;

    public IccContactOperation(Context context,
            SyncSimContactsService syncSimContactsService) {
        super();
        this.mContext = context;
        this.mSyncSimContactsService = syncSimContactsService;
    }

    /**
     * query all SIM contacts.
     *
     * @param slotId
     *            Which SIM card to operate.
     */
    public void querySimContact(int slotId) {
        Cursor cursor = mContext.getContentResolver().query(getUri(slotId),
                null, null, null, null);

        String keyId = "_id";
        String keyName = "name";
        String keyNumber = "number";

        while (cursor.moveToNext()) {
            String id = cursor.getString(cursor.getColumnIndex(keyId));
            String name = cursor.getString(cursor.getColumnIndex(keyName));
            String phoneNumber = cursor.getString(cursor
                    .getColumnIndex(keyNumber));
        }
    }

    /**
     *
     * @param slotId
     *            SIM卡id
     * @param name
     *            联系人姓名
     * @param number
     *            联系人号码
     * @param checkDuplicate
     *            是否检测重复项
     * @return
     */
    public Uri insertSimContact(ArrayList<ContentProviderOperation> operationList, int slotId, String name, String number,
            boolean checkDuplicate) {
        String filterNumber = filterString(number);// 去掉空格
        Log.i(TAG, "IccContactOperation.insertSimContact -> slotId = " + slotId
                + ", name = " + name + ", filterNumber = " + filterNumber);

        if (checkDuplicate) {
            // TODO query this contact is exists or not.
            if (isContactDuplicate(slotId, name, filterNumber)) {
                return null;
            }
        }

        return insertSimContact(operationList, slotId, name, filterNumber);
    }

    private boolean isContactDuplicate(int slotId, String name, String number) {
        boolean duplicate = false;

        /*
         * ContentResolver cr = mContext.getContentResolver(); Cursor c =
         * cr.query(Contacts.CONTENT_LOOKUP_URI, null,
         * PhoneLookup.NORMALIZED_NUMBER + " = ? ", new String[] { number },
         * null); if (c != null) { String ids = ""; try { while (c.moveToNext())
         * { ids += c.getInt(c .getColumnIndex(PhoneLookup.NORMALIZED_NUMBER)) +
         * ","; }
         *
         * if (ids.length() > 0) { // delete last "," ids.substring(0,
         * ids.length() - 1); } } finally { c.close(); }
         *
         * if (!TextUtils.isEmpty(ids)) { int card =
         * mSyncSimContactsService.isPrimarySim(slotId) ? 1 : 2; c =
         * cr.query(RawContacts.CONTENT_URI, null, SIM_COLUMN + " = " + card +
         * " AND " + RawContacts.CONTACT_ID + " IN (?) ", new String[] { ids },
         * null); if (c != null) { if (c.getCount() > 0) { duplicate = true; }
         * c.close(); } } }
         */

        return duplicate;
    }

    private Uri insertSimContact(ArrayList<ContentProviderOperation> operationList, int slotId, String name, String number) {
        ContentValues values = new ContentValues();
        values.put(NAME, name);
        values.put(NUMBER, number);

        Uri newsimContactUri = mContext.getContentResolver().insert(
                getUri(slotId), values);

        Log.d(TAG, "insertSimContact ->  new sim contact uri = "
                + newsimContactUri);
        if (newsimContactUri != null) {
            // save the contact to local TEMP.
         mSyncSimContactsService.insertIntoData(operationList, slotId + 1, name, number);        }

        return newsimContactUri;
    }

    /**
     *
     * @param lookupUri
     *            本地存储联系人uri
     * @param slotId
     *            SIM卡id
     * @param newName
     *            更新后的名称
     * @param newNumber
     *            更新后的号码
     * @return
     */
    public int updateSimContact(long contactId, int slotId, String newName,
            String newNumber) {
        String oldName = "";
        String oldNumber = "";

        // TODO query the contact oldName and oldNumber by URI.
        Cursor c = mContext.getContentResolver().query(android.provider.ContactsContract.Data.CONTENT_URI, null,
                        Data.RAW_CONTACT_ID + " = ? " + " AND "
                        + ContactsContract.Data.MIMETYPE + " = ? ",
                        new String[] { String.valueOf(contactId),
                        StructuredName.CONTENT_ITEM_TYPE }, null);
        if (c != null) {
            try {
                if (c.moveToNext()) {
                    oldName = c.getString(c.getColumnIndex(StructuredName.FAMILY_NAME));
                }
            } finally {
                c.close();
            }
        }

        c = mContext.getContentResolver().query(android.provider.ContactsContract.Data.CONTENT_URI, null,
                        Data.RAW_CONTACT_ID + " = ? " + " AND "
                        + ContactsContract.Data.MIMETYPE + " = ? "
                        + " AND " + Phone.TYPE + " = ? ",
                        new String[] { String.valueOf(contactId),
                        Phone.CONTENT_ITEM_TYPE,
                        String.valueOf(Phone.TYPE_MOBILE) }, null);
        if (c != null) {
            try {
                if (c.moveToNext()) {
                    oldNumber = c.getString(c.getColumnIndex(Phone.NUMBER));
                }
            } finally {
                c.close();
            }
        }

        if ((TextUtils.isEmpty(oldName) && TextUtils.isEmpty(oldNumber))
                || (TextUtils.equals(oldName, newName) && TextUtils.equals(
                        oldNumber, newNumber))) {
            return 0;
        }

        Log.v(TAG, "IccContactOperation.updateSimContact -> slotId = " + slotId
                + ", oldName = " + oldName + ", oldNumber = " + oldNumber
                + ", newName = " + newName + ", newNumber = " + newNumber
                + ", >> contactId = " + contactId);

        return updateSimContact(contactId, slotId, oldName, oldNumber, newName,
                filterString(newNumber));
    }

    private int updateSimContact(long contactId, int slotId, String oldName,
            String oldNumber, String newName, String newNumber) {

        Log.i(TAG, "updateSimContact -> oldName = [" + oldName + "]");
        Log.i(TAG, "updateSimContact -> oldNumber = [" + oldNumber + "]");
        Log.i(TAG, "updateSimContact -> newName = [" + newName + "]");
        Log.i(TAG, "updateSimContact -> newNumber = [" + newNumber + "]");

        ContentValues values = new ContentValues();
        values.put(NAME, oldName);
        values.put(NUMBER, oldNumber);
        values.put(NEW_NAME, newName);
        values.put(NEW_NUMBER, newNumber);

        int count = mContext.getContentResolver().update(getUri(slotId),
                values, null, null);

        Log.v(TAG, "updateSimContact -> count = " + count
                + ", getUri(slotId) = " + getUri(slotId));
        if (count > 0) {
            // update TEMP SIM contact
            updateTempSimContact(contactId, newName, newNumber);
        }

        return count;
    }

    private void updateTempSimContact(long rawContactId, String newName,
            String newNumber) {
        updateContact(rawContactId, newName, newNumber);

    }

    private void updateContact(long contactId, String name, String phone) {

        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

        // update number
        ops.add(ContentProviderOperation
                .newUpdate(ContactsContract.Data.CONTENT_URI)
                .withSelection(

                        Data.RAW_CONTACT_ID + " = ? " + " AND "
                                + ContactsContract.Data.MIMETYPE + " = ? "
                                + " AND " + Phone.TYPE + " = ? ",
                        new String[] { String.valueOf(contactId),
                                Phone.CONTENT_ITEM_TYPE,
                                String.valueOf(Phone.TYPE_MOBILE) })
                .withValue(Phone.NUMBER, phone).build());

        // update name
        ops.add(ContentProviderOperation
                .newUpdate(ContactsContract.Data.CONTENT_URI)
                .withSelection(
                        Data.RAW_CONTACT_ID + " = ? " + " AND "
                                + ContactsContract.Data.MIMETYPE + " = ? ",
                        new String[] { String.valueOf(contactId),
                                StructuredName.CONTENT_ITEM_TYPE })
                .withValue(StructuredName.FAMILY_NAME, name).build());

        try {
            ContentProviderResult[] cpr = mContext.getContentResolver()
                    .applyBatch(ContactsContract.AUTHORITY, ops);
            Log.v(TAG, "updateContact -> cpr = " + Arrays.toString(cpr));
        } catch (Exception e) {
        }
    }

    /**
     *
     * @param slotId
     * @param contactId
     * @return
     */
    public int deleteSimContact(int slotId, long contactId) {
        int count = 0;
        Cursor c = mContext.getContentResolver().query(android.provider.ContactsContract.Data.CONTENT_URI, null,
                        Data.RAW_CONTACT_ID + " = ? " + " AND "
                        + ContactsContract.Data.MIMETYPE + " = ? ",
                        new String[] { String.valueOf(contactId),
                        StructuredName.CONTENT_ITEM_TYPE }, null);
        String name = null;
        if (c != null) {
            try {
                if (c.moveToNext()) {
                    name = c.getString(c.getColumnIndex(StructuredName.FAMILY_NAME));
                }
            } finally {
                c.close();
            }
        }

        String number = null;
        c = mContext.getContentResolver().query(android.provider.ContactsContract.Data.CONTENT_URI, null,
                        Data.RAW_CONTACT_ID + " = ? " + " AND "
                        + ContactsContract.Data.MIMETYPE + " = ? "
                        + " AND " + Phone.TYPE + " = ? ",
                        new String[] { String.valueOf(contactId),
                        Phone.CONTENT_ITEM_TYPE,
                        String.valueOf(Phone.TYPE_MOBILE) }, null);
        if (c != null) {
            try {
                if (c.moveToNext()) {
                    number = c.getString(c.getColumnIndex(Phone.NUMBER));
                }
            } finally {
                c.close();
            }
        }

        count = deleteSimContact(slotId, name, number, contactId);
        return count;

    }

    /**
     * 删除SIM卡联系人
     *
     * @param slotId
     *            SIM卡id
     * @param name
     *            被删除联系人的姓名
     * @param number
     *            被删除联系人的id
     * @param uriString
     *            TODO
     * @return
     */
    private int deleteSimContact(int slotId, String name, String number,
            long rawContactId) {
        Log.i(TAG, "IccContactOperation.deleteSimContact -> slotId = " + slotId
                + ", name = " + name + ", number = " + number
                + "\n getUri(slotId) = " + getUri(slotId));

        String where = NAME + " = '" + name + "' " + " AND " + NUMBER + " = '"
                + number + "' ";
        int count = mContext.getContentResolver().delete(getUri(slotId), where,
                null);

        Log.i(TAG, "deleteSimContact -> count = " + count);
        if (count > 0) {
            mContext.getContentResolver().delete(
                    ContentUris.withAppendedId(RawContacts.CONTENT_URI,
                            rawContactId), null, null);
        }

        return count;
    }

    private Uri getUri(int slotId) {
        /*
        Uri uri = mSyncSimContactsService.isPrimarySim(slotId) ? ICC_CONTENT_URI
                : ICC2_CONTENT_URI;
        */
        Uri uri = SimUtils.getIccUri(slotId);

        return uri;
    }

    private String filterString(String text) {
        if (!TextUtils.isEmpty(text)) {
            return text.replaceAll(" ", "");
        }
        return text;
    }
}
