

package com.android.packageinstaller;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

public class InstallVerifyFlagReceive extends BroadcastReceiver {
	private final static String action = "com.android.packageinstaller.flag";
	
    @Override
    public void onReceive(Context context, Intent intent) {
    	System.out.println("action  " + intent.getAction());
        if (action.equals(intent.getAction())) {
			int value = intent.getIntExtra("key", 0);
			System.out.println("value  " + value);
            Settings.Secure.putInt(context.getContentResolver(), 
						Settings.Secure.SET_INSTALL_VERIFICATION, value);
        }
    }
}


