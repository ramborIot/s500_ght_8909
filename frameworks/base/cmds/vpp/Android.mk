
LOCAL_PATH:=$(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-subdir-java-files)
LOCAL_MODULE := vpp
LOCAL_JAVA_LIBRARIES := bouncycastle
include $(BUILD_JAVA_LIBRARY)

include $(CLEAR_VARS)
ALL_PREBUILT += $(TARGET_OUT)/bin/vpp
$(TARGET_OUT)/bin/vpp : $(LOCAL_PATH)/vpp | $(ACP)
	$(transform-prebuilt-to-target)
	
	
	