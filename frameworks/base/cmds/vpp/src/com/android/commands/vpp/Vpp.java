/*
**
** Copyright 2013, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/


package com.android.commands.vpp;

import android.util.Log;

import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.android.org.bouncycastle.util.io.pem.PemObject;
import com.android.org.bouncycastle.util.io.pem.PemReader;

import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;

import java.math.BigInteger;

import android.content.Context;
import android.hardware.ddi.DdiManager;
import android.hardware.ddi.IDdiManager;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.AndroidException;
import android.os.Environment;




public class Vpp {
	private final static String TAG = "Vpp";
	private DdiManager mDdiManager;
	private byte[] pemdate = new byte[4096]; 
    /**
     * Command-line entry point.
     *
     * @param args The command-line arguments
     */
    public static void main(String[] args) {
        (new Vpp()).run(args);
    }

	public void init() {
		mDdiManager = DdiManager.getInstance();
		mDdiManager.ddiGetPemFile(pemdate);
	}

	private void run_test(String[] args) {
		String state = Environment.getSecondaryStorageState();
		Log.d(TAG, " " + state);
		if (state.equals(Environment.MEDIA_MOUNTED)) {
			File sdfile = Environment.getSecondaryStorageDirectory();
			File sdpath = new File(sdfile.getAbsolutePath());
			for (File file : sdpath.listFiles()) {
				Log.d(TAG, " " + file.getName());
			}
		} 
	}
    
    private void run(String[] args) {
		init();
		try {
			File file = new File("/sdcard/releasekey.x509.pem");
			FileInputStream fis = new FileInputStream(file);
			byte[] buffer = new byte[fis.available()];
			fis.read(buffer);
			BigInteger local = getModulus(buffer);
			BigInteger secure = getModulus(pemdate);
			if (local.equals(secure)) {
				Log.d(TAG, " local is equal to secure");
			} else {
				Log.d(TAG, " local is note equal to secure");
			}
			
		} catch (IOException e) {
			Log.e(TAG, "io failed  " + e);
		} 
    }


	private BigInteger getModulus(byte[] buffer) {
		BigInteger modulus = BigInteger.ZERO;
		try {
			ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
			InputStreamReader isr = new InputStreamReader(bais);
			PemReader reader = new PemReader(isr);

			CertificateFactory cf = CertificateFactory.getInstance("X509");
			List<X509Certificate> result = new ArrayList<X509Certificate>();
			PemObject o;
			while ((o = reader.readPemObject()) != null) {
				if (o.getType().equals("CERTIFICATE")) {
					Certificate cert = cf.generateCertificate(new ByteArrayInputStream(o.getContent()));
					result.add((X509Certificate) cert);
					RSAPublicKey rsaPK = (RSAPublicKey)((X509Certificate)cert).getPublicKey();
					BigInteger exponent =  rsaPK.getPublicExponent();
					modulus =  rsaPK.getModulus();
					Log.d(TAG, "exponent " +  exponent.toString());
					Log.d(TAG, "modulus " +  modulus.toString());
					Log.d(TAG, " " + ((X509Certificate)cert).getPublicKey());
					Log.d(TAG, " " + ((X509Certificate)cert).getSerialNumber());
					Log.d(TAG, " " + ((X509Certificate)cert).getVersion());
					Log.d(TAG, " " + ((X509Certificate)cert).getNotBefore());
					Log.d(TAG, " " + ((X509Certificate)cert).getNotAfter());
					Log.d(TAG, " " + ((X509Certificate)cert).getSigAlgName());
					Log.d(TAG, " " + new String(((X509Certificate)cert).getSignature()));
					Log.d(TAG, " " + ((X509Certificate)cert).getSigAlgOID());
					Log.d(TAG, " " + new String(((X509Certificate)cert).getSigAlgParams()));
				} else {
					Log.e(TAG, "not CERTIFICATE type");
				}
			}
			reader.close();	
		} catch (IOException e) {
			Log.e(TAG, "io failed  " + e);
		} catch (CertificateException c) {
			Log.e(TAG, "not such exception" + c);
		}

		return modulus;
	}
}





