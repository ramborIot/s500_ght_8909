
#define LOG_NDEBUG 0

#undef LOG_TAG
#define LOG_TAG "Ddi_jni"


#include <stdio.h>  
#include <stdlib.h>  
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h>  
#include <sys/stat.h>  
#include <fcntl.h>  
#include <assert.h>  
#include "jni.h"  
#include "loadfont.h"
#include "debug.h"
#include "JNIHelp.h"
#include "android_runtime/AndroidRuntime.h"
#include <utils/Log.h>
#include <utils/misc.h>

namespace android {
extern "C"{
#include "prnt_mgnt.h"
#include "printer_queue.h"
#include "prnt_fck.h"
}
// #include "JNIHelp.h"  
// #include "android_runtime/AndroidRuntime.h"  

//#include "SkBitmap.h"
//#include "SkDevice.h"
//#include "SkPaint.h"
//#include "SkRect.h"
//#include "SkImageEncoder.h"
//#include "SkTypeface.h"
//#include "SkCanvas.h"

extern "C" {
#include "devApi.h"
//#include <android/log.h>
}

#define ALOGE(...)

//#define LOG_TAG "kris_ril"
//#include <utils/Log.h>

// #include <utils/Log.h>
// #include <utils/String16.h>
//#define REPORT_FUNCTION() ALOGE("%s....................................................................\n", __PRETTY_FUNCTION__)
#define REPORT_FUNCTION()
#define ATR_LEN 33
#define SN_LEN 17
#define ID_LEN 17
#define RANDOM_LEN 4
#define PASSWARD_LEN 6
#define APDU_MAX_LEN   512
#define TRACK1_LEN 79+1
#define TRACK2_LEN 37+1
#define TRACK3_LEN 104+1
#define IMG_MAX_LEN 384*240
#define IMG_NAME_MAX_LEN 256
#define PRN_TEXT_MAX_LEN 256
#define COMB_PRN_MAX  256
#define COMB_PRN_TEXT_MAX_LEN 256
#define INNER_KEY_MAX_LEN 16
#define INNER_DATA_MAX_LEN 1028
#define CERT_DATA_MAX_LEN 10000
#define DUKPT_KEY_MAX_LEN 24
#define DUKPT_SN_MAX_LEN 20
#define PINBLOCK_LEN 8    //000
#define COM_MAX_LEN   1024
#define HASH_LEN 142
#define SIGISSUSE_LEN 48
#define VERSION_LEN 22
#define HASH1_LEN 72
#define SIG_LEN 518
#define HASHDATA_LEN SIGISSUSE_LEN+VERSION_LEN+HASH1_LEN+SIG_LEN+2
#define IOCTRL_MAX_LEN   512
#define SECURE_STATU_LEN 12
#define PEM_FILE_LEN 4096
#if 0
typedef struct _strPrnTextCtrl {
    int m_align;
    int m_offset;
    int m_font;
    int m_ascsize;
    int m_asczoom;
    int m_nativesize;
    int m_nativezoom;
}strPrnTextCtrl;

typedef struct _strPrnCombTextCtrl {
    int m_x0;
    int m_y0;
    int m_font;
    int m_ascsize;
    int m_asczoom;
    int m_nativesize;
    int m_nativezoom;
    unsigned char* m_text;
}strPrnCombTextCtrl;

typedef struct _strDukptInitInfo {
    unsigned char m_groupindex;
    unsigned char m_keyindex;
    unsigned char m_initkey[24];
    unsigned char m_keylen;
    unsigned char m_ksnindex;
    unsigned char m_initsn[20];
    unsigned char m_ksnlen;
}strDukptInitInfo;
#endif
#define MAX_DOT_LINE		( 1600 )
static unsigned char k_DotBuf[MAX_DOT_LINE][48];

static int k_CurDotCol = 0;
static int k_CurDotLine = 0;

#define uchar unsigned char
#define ushort unsigned short
#define uint unsigned int

#define PRN_OUTOFMEMORY		( -404 )

#define BITMAP_PIXEL_1 		( 1 )
#define PRN_LINE_PIXEL_MAX	( 384 )


int BmpToDotBuffer(char *filepath)
{
	uchar  *fileBuffer;
	ushort bitsPerPixel = 0;
	int  handle      = 0;
	int  len         = 0;
	int  iWReal      = 0;
	int  iHReal      = 0;
	int  iW          = 0;
	int  iH          = 0;
	int  iWOmit      = 0;
	int  iHOmit      = 0;
	int  x           = 0;
	int  y           = 0;
	int  iCnt        = 0;
	int  temp        = 0;
	int  i           = 0;
	int  m           = 0;
	int  n           = 0;
	int  skip        = 0;
	int  residue     = 0;
	int  rowByte     = 0;
	int  LeftBytes   = 0;
	int  LeftBits    = 0;
	uchar  *pDotBuf  = NULL;
	uchar  *pBitmap  = NULL;
	uchar  medium    = 0;
	uchar  dotTmp    = 0;
	int  lastDotLine = 0;
	int j;


    FILE *fp = NULL;
    unsigned char *pData;
    int iLen, itmpLen, itmp;

	k_CurDotLine = 0;

	
    fp = fopen(filepath, "rb");
    if(fp == NULL)
    {
//        printf("fopen err\n");
        goto end;
    }   
	
    fseek(fp, 0, SEEK_END);
    iLen = ftell(fp);
	
    if(iLen <= 0)
    {
//        printf("file size err: %d\n", iLen);
        goto end;
    }
	
//    printf("file size: %d\n", iLen);

    pData = (unsigned char *)malloc(iLen);

    if(pData == NULL)
    {
//        printf("pData malloc err\n");
        goto end;
    }

    //fseek(fp, 0, SEEK_SET);
    fclose(fp);
    fp = fopen(filepath, "rb");
    if(fp == NULL)
    {
//        printf("fopen err\n");
        goto end;
    }
	
    itmpLen = 0;
	
    while(itmpLen < iLen)
    {
        itmp = fread(pData+itmpLen, 1, iLen, fp);
//        printf("itmp = %d\n", itmp);
        itmpLen += itmp;
    }
    
    
    fileBuffer = pData;
    
	LeftBytes = k_CurDotCol / 8;
    LeftBits  = k_CurDotCol % 8;
    
	//adds file format check
	iWReal = (fileBuffer[21] << 24) + (fileBuffer[20 ]<< 16) 
	    + (fileBuffer[19] << 8) + fileBuffer[18]; 

	iHReal = (fileBuffer[25] << 24) + (fileBuffer[24] << 16) 
	    + (fileBuffer[23] << 8) + fileBuffer[22]; 

//	printf("iWReal: %d, iHReal: %d\r\n", iWReal, iHReal);

	bitsPerPixel = fileBuffer[28];

	if (bitsPerPixel != BITMAP_PIXEL_1)
	{
//		printf("what the fck err: %d\r\n", bitsPerPixel);
		goto end;
	}

	rowByte = iWReal / 8;
	if((iWReal % 8) != 0)
	{
		rowByte++;
	}
				
	residue = rowByte % 4;
	if (residue)
	{
		skip = 4 - residue;
	}

	iW = iWReal;
	iH = iHReal;
	
	//if pixels in a line can not be divided by 4, add some 0 backward
	if ((iWReal + k_CurDotCol) > PRN_LINE_PIXEL_MAX)
	{
		iW = PRN_LINE_PIXEL_MAX - k_CurDotCol;
		iWOmit = iWReal + k_CurDotCol - PRN_LINE_PIXEL_MAX;
	}

	if ((iHReal + k_CurDotLine) > MAX_DOT_LINE)
	{
//		printf("err: iHReal:%d, k_CurDotLine: %d\r\n", iHReal, k_CurDotLine);
		return PRN_OUTOFMEMORY;
	}
	lastDotLine = k_CurDotLine + iH;

	iCnt = iHOmit * (rowByte + skip);
		
	n = iW / 8;
	
	if ((iW % 8) != 0)
	{
		n = iW / 8 + 1;
	}
	
	for (x = 0; x < iH; x++)
	{
		pDotBuf = &(k_DotBuf[lastDotLine][LeftBytes]);
		pBitmap = &(fileBuffer[62 + x * (rowByte + skip)]);
			
		memcpy(pDotBuf, pBitmap, n);

		for (y = 0; y < n; y++)
		{
			pDotBuf[y] = ~(pDotBuf[y]);
		}

		if (LeftBits != 0)
		{
			y = 0;
			medium = pDotBuf[y]; 
			pDotBuf[y] = (pDotBuf[y] >> (8-LeftBits));
			
			for (y = 1; y < n-1; y++)
			{
				dotTmp = pDotBuf[y]; 
				pDotBuf[y] = (pDotBuf[y] >> (8-LeftBits));
				medium = medium << LeftBits;
				pDotBuf[y] = pDotBuf[y] | medium;
				medium = dotTmp;
			}
		}
		
		iCnt += iWOmit/8;
		iCnt += skip;
		
		lastDotLine--;
	}
	
	k_CurDotLine += iH;
	
	
//	for(i = 0; i < MAX_DOT_LINE; i++)
//	{
//		for(j = 0; j < 48; j++)
//		{
//			
//			printf("0x%02x,", k_DotBuf[i][j]);	
//		}	
//		printf("\n");
//	}
		
	return 0;
end:
	return 0;
}


unsigned char *GetDotBuffer()
{
	return (unsigned char *)k_DotBuf;
}

int GetLineCount(void)
{
	return k_CurDotLine;
}


static jint test(JNIEnv *env, jobject obj, jint a, jint b)  
{  
	#if (0)  /* NEOTEL: zhangzirui 20151019(13:45:25) */
	SkBitmap bitmap;
	bitmap.setConfig(SkBitmap::kARGB_8888_Config, 200, 200);
	bitmap.allocPixels();

//	REPORT_FUNCTION();
	ALOGE("test~~~~~");
	SkPaint paint;
	
	SkCanvas canvas(bitmap);
	SkRect r;
	//r.set(25, 25, 145, 145);
	//canvas.drawRect(r, paint);
	//paint.setARGB(255, 0, 255, 0);
	//r.offset(20, 20);
	//canvas.drawRect(r, paint);
	SkTypeface *font = SkTypeface::CreateFromFile("/system/fonts/fzcy.ttf");
	paint.setTextSize(24);
	 //paint.setARGB(255, 255, 0, 0);
	 char testString[] = "新国都";
	 canvas.drawText("abcd", 4*sizeof(char), 0, 100 , paint);
	if (font)
	{
		ALOGE("hava font~~~~");
		paint.setTypeface( font );
		paint.setTextSize(24);
//		canvas.drawText("HELLO!:)", 8, 200, 180, paint);
		canvas.drawText(testString, sizeof(testString), 100, 100 , paint);
	}
	else
	{
		ALOGE("no font~~~~");
	}
	SkImageEncoder::EncodeFile("/system/bin/123.bmp", bitmap,SkImageEncoder::kPNG_Type,100);
	#endif /* 0 */
	return (a * b);  
}  
/***************************** ÏµÍ³*************************************/
// int InitFontLib();
 void sub_sys_init(JNIEnv *env, jobject obj)
{
	ALOGD("spi_ddi_sys_init  start");
	spi_ddi_sys_init();
	ALOGD("spi_ddi_sys_init end");
	// InitFontLib();
}

 void sub_sys_uninit(JNIEnv *env, jobject obj)
{
	ALOGD("spi_ddi_sys_uninit  start");
	spi_ddi_sys_uninit();
	ALOGD("spi_ddi_sys_uninit end");
	// InitFontLib();
}

 jint sub_sys_read_dsn(JNIEnv *env, jobject obj, jbyteArray lpOut)
{
	int ret,i,plen;
	jbyte *lpOut_j;
	unsigned char lpOut_c[SN_LEN]= {0};

	plen = env->GetArrayLength(lpOut);
	if(plen<SN_LEN)
		return -10;
	lpOut_j = env->GetByteArrayElements(lpOut, false);
	
	ret = spi_ddi_sys_read_dsn(lpOut_c);

	for(i=0;i<SN_LEN;i++)
	{
		lpOut_j[i] = lpOut_c[i];
	}
	env->ReleaseByteArrayElements(lpOut, lpOut_j, 0);
	return ret;
}

static jint sub_sys_get_firmwarever(JNIEnv *env, jobject obj, jbyteArray lpOut, jint nType)
{
	int ret,i,plen;
	int type = nType;
	jbyte *lpOut_j;
	unsigned char lpOut_c[SN_LEN];
	plen = env->GetArrayLength(lpOut);
	if(plen<SN_LEN)
		return -10;
	lpOut_j = env->GetByteArrayElements(lpOut, false);
	
	ret = spi_ddi_sys_get_firmwarever(lpOut_c,nType);

	for(i=0;i<SN_LEN;i++)
	{
		lpOut_j[i] = lpOut_c[i];
	}
	LOG("lpOut_c == %s",lpOut_c);
	LOG("lpOut_j == %s",lpOut_j);
	env->ReleaseByteArrayElements(lpOut, lpOut_j, 0);
	return ret;
}

static jint sub_sys_bat_status(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_sys_bat_status();
	
	return ret;
}

static jint sub_sys_mainBat_status(JNIEnv *env, jobject obj)
{
	int ret;

	ret = sys_mainBat_status();
	
	return ret;
}

static jint sub_sys_poweroff(JNIEnv *env, jobject obj)
{
//	system("shutdown");
//	core_ddi_sys_poweroff();
	return 0;
}

static jint sub_sys_download(JNIEnv *env, jobject obj, jint nType)
{
	int ret;
	int type = nType;
//	system("shutdown");
//	ret = core_ddi_sys_download(type);
	return ret;
}

static jint sub_ddi_sys_set_timeout(JNIEnv *env, jobject obj, jint timeOut)
{
	int ret;

	ret = spi_ddi_sys_set_timeout(timeOut);
	
	return ret;
}

static jint sub_ddi_sys_get_timeout(JNIEnv *env, jobject obj,jintArray pTimeout)
{
	int ret;
	jint *pTimeout_j;
	
	unsigned int pTimeout_c[1];
	pTimeout_j = env->GetIntArrayElements(pTimeout, false);
	ret = spi_ddi_sys_get_timeout(pTimeout_c);
	pTimeout_j[0] = pTimeout_c[0];
	env->ReleaseIntArrayElements(pTimeout, pTimeout_j, 0);
	
	return ret;
}

static jint sub_ddi_sys_get_chipID(JNIEnv *env, jobject obj,jbyteArray ID)
{
	int ret,i,plen;
	jbyte *ID_j;
	unsigned char ID_c[ID_LEN];

	plen = env->GetArrayLength(ID);
	if(plen<ID_LEN)
		return -10;
	ID_j = env->GetByteArrayElements(ID, false);
	ret = ddi_sys_get_chipID(ID_c);
	for(i=0;i<ID_LEN;i++)
	{
		ID_j[i] = ID_c[i];
	}
	env->ReleaseByteArrayElements(ID, ID_j, 0);
	
	return ret;
}

static jint sub_ddi_security_rand(JNIEnv *env, jobject obj,jbyteArray rand)
{
	int ret,i,plen;
	jbyte *rand_j;
	unsigned char rand_c[RANDOM_LEN];

	plen = env->GetArrayLength(rand);
	if(plen<RANDOM_LEN)
		return -10;
	rand_j = env->GetByteArrayElements(rand, false);
	ret = spi_ddi_security_rand(rand_c);
	for(i=0;i<RANDOM_LEN;i++)
	{
		rand_j[i] = rand_c[i];
	}
	env->ReleaseByteArrayElements(rand, rand_j, 0);
	
	return ret;
}

static jint sub_ddi_get_debugStatus(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_get_debugStatus();
	
	return ret;
}

static jint sub_ddi_set_debugStatus(JNIEnv *env, jobject obj,jbyte status,jbyteArray password)
{
	int ret,i,plen;
	jbyte *password_j;
	
	unsigned char password_c[PASSWARD_LEN];
	plen = env->GetArrayLength(password);
	password_j = env->GetByteArrayElements(password, false);

	for(i=0;i<PASSWARD_LEN&&i<plen;i++)
	{
		password_c[i] = password_j[i];
	}
	ret = spi_ddi_set_debugStatus(status,password_c);
	
	env->ReleaseByteArrayElements(password, password_j, 0);
	
	return ret;
}

static jint sub_ddi_delete_cert_byPassword(JNIEnv *env, jobject obj,jbyteArray password)
{
	int ret,i,plen;
	jbyte *password_j;
	
	unsigned char password_c[PASSWARD_LEN];
	plen = env->GetArrayLength(password);
	password_j = env->GetByteArrayElements(password, false);

	for(i=0;i<PASSWARD_LEN&&i<plen;i++)
	{
		password_c[i] = password_j[i];
	}
	ret = spi_ddi_delete_cert_byPassword(password_c);
	
	env->ReleaseByteArrayElements(password, password_j, 0);
	
	return ret;
}

static jint sub_ddi_sys_set_beep(JNIEnv *env, jobject obj, jint time)
{
	int ret;

	ret = spi_ddi_sys_set_beep(time);
	
	return ret;
}

static jint sub_ddi_sys_beep_ctrl(JNIEnv *env, jobject obj, jint ctrl, jint freq, jint tm)
{
	int ret;

	ret = spi_ddi_sys_beep_ctrl(ctrl, freq, tm);
	
	return ret;
}

/*****************************uart*************************************/

static jint  sub_com_open(JNIEnv *env, jobject obj,jint nCom,jint baud,jint databits,jint parity,jint stopbits)
{
	int ret;
	strComAttr ComAttr;
	ComAttr.m_baud = baud;
	ComAttr.m_databits= databits;
	ComAttr.m_parity= parity;
	ComAttr.m_stopbits= stopbits;
	ret = spi_ddi_com_open(nCom,&ComAttr);
	
	return ret;
}

static jint sub_com_close(JNIEnv *env, jobject obj,jint nCom)
{
	int ret;

	ret = spi_ddi_com_close(nCom);
	
	return ret;
}

static jint sub_com_clear(JNIEnv *env, jobject obj,jint nCom)
{
	int ret;

	ret = spi_ddi_com_clear(nCom);
	
	return ret;
}

static jint sub_com_read(JNIEnv *env, jobject obj,jint nCom,jbyteArray lpOut,jint nLe)
{
	int ret,i,plen;
	jbyte *lpOut_j;
	unsigned char lpOut_c[COM_MAX_LEN] = {0};

	plen = env->GetArrayLength(lpOut);

	lpOut_j = env->GetByteArrayElements(lpOut, false);

	ret = spi_ddi_com_read(nCom,lpOut_c,nLe);

	if(ret>0&&ret<COM_MAX_LEN&&ret<=nLe)
	{
		for(i=0;i<ret;i++)
		{
			lpOut_j[i] = lpOut_c[i];
		}
	}

	env->ReleaseByteArrayElements(lpOut, lpOut_j, 0);
	return ret;
}

static jint sub_com_write(JNIEnv *env, jobject obj,jint nCom,jbyteArray lpIn,jint nLe)
{
	int ret,i,plen;
	jbyte *lpIn_j;
	unsigned char lpIn_c[COM_MAX_LEN] = {0};
	
	plen = env->GetArrayLength(lpIn);
	lpIn_j = env->GetByteArrayElements(lpIn, false);

	for(i=0;i<nLe&&i<COM_MAX_LEN&&i<plen;i++)
	{
		lpIn_c[i] = lpIn_j[i];
	}
	
	ret = spi_ddi_com_write(nCom,lpIn_c,nLe);

	env->ReleaseByteArrayElements(lpIn, lpIn_j, 0);
	return ret;
}

static jint sub_com_ioctl(JNIEnv *env, jobject obj,jint nCom,jint nCmd,jint lParam,jint wParam)
{
	int ret;
	int com = nCom;
	int cmd = nCmd;
	int lPar = lParam;
	int wPar = wParam;
	
	ret = spi_ddi_com_ioctl(com,cmd,lPar,wPar);

	return ret;
}


static jint sub_k21_com_clear(JNIEnv *env, jobject obj,jint nCom)
{
	int ret;

	ret = spi_k21_ddi_com_clear(nCom);
	
	return ret;
}

static jint sub_k21_com_read(JNIEnv *env, jobject obj,jint nCom,jbyteArray lpOut,jint nLe)
{
	int ret,i,plen;
	jbyte *lpOut_j;
	unsigned char lpOut_c[COM_MAX_LEN] = {0};

	plen = env->GetArrayLength(lpOut);
	if(plen<COM_MAX_LEN)
		return -10;
	lpOut_j = env->GetByteArrayElements(lpOut, false);

	ret = spi_k21_ddi_com_read(nCom,lpOut_c,nLe);

	if(ret>0&&ret<COM_MAX_LEN)
	{
		for(i=0;i<ret;i++)
		{
			lpOut_j[i] = lpOut_c[i];
		}
	}

	env->ReleaseByteArrayElements(lpOut, lpOut_j, 0);
	return ret;
}

static jint sub_k21_com_write(JNIEnv *env, jobject obj,jint nCom,jbyteArray lpIn,jint nLe)
{
	int ret,i,plen;
	jbyte *lpIn_j;
	unsigned char lpIn_c[COM_MAX_LEN] = {0};

	plen = env->GetArrayLength(lpIn);
	lpIn_j = env->GetByteArrayElements(lpIn, false);

	for(i=0;i<nLe&&i<COM_MAX_LEN&&i<plen;i++)
	{
		lpIn_c[i] = lpIn_j[i];
	}
	
	ret = spi_k21_ddi_com_write(nCom,lpIn_c,nLe);

	env->ReleaseByteArrayElements(lpIn, lpIn_j, 0);
	return ret;
}

/***************************** ŽÅ¿š*************************************/

static jint  sub_mag_open(JNIEnv *env, jobject obj)
{
	int ret;
	ALOGD("sub_mag_open  start");
	ret = spi_ddi_mag_open();
	ALOGD("sub_mag_open  end");
	return ret;
}

static jint sub_mag_close(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_mag_close();
	
	return ret;
}

static jint sub_mag_clear(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_mag_clear();
	
	return ret;
}

static jint sub_mag_read(JNIEnv *env, jobject obj,jbyteArray lpTrack1,jbyteArray lpTrack2,jbyteArray lpTrack3)
{
	int ret,i,plen;
	jbyte *lpTrack1_j;
	jbyte *lpTrack2_j;
	jbyte *lpTrack3_j;
	unsigned char lpTrack1_c[TRACK1_LEN] = {0};
	unsigned char lpTrack2_c[TRACK2_LEN] = {0};
	unsigned char lpTrack3_c[TRACK3_LEN] = {0};

	plen = env->GetArrayLength(lpTrack1);
	if(plen<TRACK1_LEN)
		return -10;
	plen = env->GetArrayLength(lpTrack2);
	if(plen<TRACK2_LEN)
		return -10;
	plen = env->GetArrayLength(lpTrack3);
	if(plen<TRACK3_LEN)
		return -10;
	
	lpTrack1_j = env->GetByteArrayElements(lpTrack1, false);
	lpTrack2_j = env->GetByteArrayElements(lpTrack2, false);
	lpTrack3_j = env->GetByteArrayElements(lpTrack3, false);

	ret = spi_ddi_mag_read(lpTrack1_c,lpTrack2_c,lpTrack3_c);
	ALOGD("\njni ret = %d\n ",ret);
	ALOGD("\nTrack1\n ");
	for(i=0;i<TRACK1_LEN;i++)
	{
		lpTrack1_j[i] = lpTrack1_c[i];
		ALOGD("%02x  ", lpTrack1_j[i] );
	}
	ALOGD("\nTrack2\n ");
	for(i=0;i<TRACK2_LEN;i++)
	{
		lpTrack2_j[i] =lpTrack2_c[i];
		ALOGD("%02x  ", lpTrack2_j[i] );
	}
	ALOGD("\nTrack3\n ");
	for(i=0;i<TRACK3_LEN;i++)
	{
		lpTrack3_j[i] = lpTrack3_c[i];
		ALOGD("%02x  ", lpTrack3_j[i] );
	}
	ALOGD("\n");
	env->ReleaseByteArrayElements(lpTrack1, lpTrack1_j, 0);
	env->ReleaseByteArrayElements(lpTrack2, lpTrack2_j, 0);
	env->ReleaseByteArrayElements(lpTrack3, lpTrack3_j, 0);
	return ret;
}

static jint sub_mag_ioctl(JNIEnv *env, jobject obj,jint nCmd,jint lParam,jint wParam)
{
	int ret;
	int cmd = nCmd;
	int lPar = lParam;
	int wPar = wParam;
	
	ret = spi_ddi_mag_ioctl(cmd,lPar,wPar);

	
	return ret;
}

static jint sub_mag_ioctl_for_java(JNIEnv *env, jobject obj,jint nCmd,
	jint lplen,jbyteArray lParam,jintArray wplen,jbyteArray wParam)
{
	int ret,i;

	unsigned int RApduLen[1];
	jbyte *lParam_j;
	jbyte *wParam_j;
	jint *wplen_j;
	
	unsigned int wplen_c[1];
	unsigned char lParam_c[IOCTRL_MAX_LEN] = {0};
	unsigned char wParam_c[IOCTRL_MAX_LEN] = {0};
	
//	lplen_ = env->GetArrayLength(lParam);
//	wplen_ = env->GetArrayLength(wParam);
	
	lParam_j = env->GetByteArrayElements(lParam, false);
	wParam_j = env->GetByteArrayElements(wParam, false);
	wplen_j = env->GetIntArrayElements(wplen, false);

	wplen_c[0] = wplen_j[0];
	
	for(i=0;i<lplen&&i<IOCTRL_MAX_LEN;i++)
	{
		lParam_c[i]=lParam_j[i];
	}
	for(i=0;i<wplen_c[0]&&i<IOCTRL_MAX_LEN;i++)
	{
		wParam_c[i]=wParam_j[i];
	}
	
	ret = spi_ddi_mag_ioctl_for_java(nCmd,lplen,lParam_c,wplen_c,wParam_c);

	wplen_j[0] = wplen_c[0];
//	if(wplen_<wplen_c[0])
//		return -10;
	for(i=0;i<wplen_c[0]&&i<IOCTRL_MAX_LEN;i++)
	{
		wParam_j[i]=wParam_c[i];
	}
	env->ReleaseByteArrayElements(lParam, lParam_j, 0);
	env->ReleaseByteArrayElements(wParam, wParam_j, 0);
	env->ReleaseIntArrayElements(wplen, wplen_j, 0);
	return ret;
}
/*****************************IC PSAM*************************************/

static jint sub_iccpsam_open(JNIEnv *env, jobject obj,jint nSlot)
{
	int slot = nSlot;
	int ret = 0;
//	LOGI("sub_iccpsam_open\n");
//	REPORT_FUNCTION();
	spi_ddi_sys_init();
	ret = spi_ddi_iccpsam_open(slot);
	REPORT_FUNCTION();
	return ret;
}

static jint sub_iccpsam_close(JNIEnv *env, jobject obj,jint nSlot)
{
	int slot = nSlot;
	int ret = 0;

//	LOGI("sub_iccpsam_close\n");
	ret = spi_ddi_iccpsam_close(slot);
	
	return ret;
}

static jint sub_iccpsam_poweron(JNIEnv *env, jobject obj,jint nSlot,jbyteArray lpAtr)
{
	int i,ret = 0,plen;
	int slot = nSlot;
	jbyte *lpAtr_j;
       unsigned char lpAtr_c[ATR_LEN] = {0};

//	LOGI("sub_iccpsam_poweron\n");
	plen = env->GetArrayLength(lpAtr);
	if(plen<ATR_LEN)
		return -10;
	lpAtr_j = env->GetByteArrayElements(lpAtr, false);

	ret = spi_ddi_iccpsam_poweron(slot,lpAtr_c);

	for(i=0;i<ATR_LEN;i++)
	{
		lpAtr_j[i] = lpAtr_c[i];
	}
	env->ReleaseByteArrayElements(lpAtr, lpAtr_j, 0);
	
	return ret;
}

static jint sub_iccpsam_poweroff(JNIEnv *env, jobject obj,jint nSlot)
{
	int ret = 0;
	int slot = nSlot;
//	LOGI("sub_iccpsam_poweroff\n");
	ret = spi_ddi_iccpsam_poweroff(slot);

	return ret;
}

static jint sub_iccpsam_get_status(JNIEnv *env, jobject obj,jint nSlot)
{
	int slot = nSlot;
	int ret = 0;
	ret = spi_ddi_iccpsam_get_status(slot);
	
	return ret;
}

static jint sub_iccpsam_exchange_apdu(JNIEnv *env, jobject obj,jint nSlot,const jbyteArray lpCApdu,
	jint lpCApduLen,jbyteArray lpRApdu,jintArray lpRApduLen,jint lpRApduSize)
{
	int i,ret = 0,clen,rlen;
	int slot = nSlot;
	int CApduLen = lpCApduLen;
	int RApduSize = lpRApduSize;
	unsigned int RApduLen[2];
	jbyte *lpCApdu_j;
	jbyte *lpRApdu_j;
	jint *lpRApduLen_j;
//	unsigned char* lpCApdu_p;
	unsigned char lpCApdu_c[APDU_MAX_LEN]= {0};
	unsigned char lpRApdu_c[APDU_MAX_LEN]= {0};
//	LOGI("sub_iccpsam_exchange_apdu\n");
	clen = env->GetArrayLength(lpCApdu);
	rlen = env->GetArrayLength(lpRApdu);
	
	lpCApdu_j = env->GetByteArrayElements(lpCApdu, false);
	lpRApdu_j = env->GetByteArrayElements(lpRApdu, false);
	lpRApduLen_j = env->GetIntArrayElements(lpRApduLen, false);
//	lpCApdu_p = (unsigned char*)lpCApdu_j;
	for(i=0;i<CApduLen&&i<APDU_MAX_LEN&&i<clen;i++)
	{
		lpCApdu_c[i]=lpCApdu_j[i];
	}
	
	ret = spi_ddi_iccpsam_exchange_apdu(slot,lpCApdu_c,CApduLen,lpRApdu_c,RApduLen,RApduSize);

	lpRApduLen_j[0] = (jint)RApduLen[0];
	if(rlen<RApduLen[0])
	{
		return -10;
	}
	for(i=0;i<RApduLen[0]&&i<APDU_MAX_LEN;i++)
	{
		lpRApdu_j[i]=lpRApdu_c[i];
	}
	env->ReleaseByteArrayElements(lpCApdu, lpCApdu_j, 0);
	env->ReleaseByteArrayElements(lpRApdu, lpRApdu_j, 0);
	env->ReleaseIntArrayElements(lpRApduLen, lpRApduLen_j, 0);
	
	return ret;
}

static jint sub_iccpsam_ioctl(JNIEnv *env, jobject obj,jint nCmd,jint lParam,jint wParam)
{
	int ret = 0;
	int cmd = nCmd;
	int lPar = lParam;
	int wPar = wParam;
	
	ret = spi_ddi_iccpsam_ioctl(cmd,lPar,wPar);

	
	return ret;
}

static jint sub_iccpsam_ioctl_for_java(JNIEnv *env, jobject obj,jint nCmd,
	jint lplen,jbyteArray lParam,jintArray wplen,jbyteArray wParam)
{
	int ret,i;

	unsigned int RApduLen[1];
	jbyte *lParam_j;
	jbyte *wParam_j;
	jint *wplen_j;
	
	unsigned int wplen_c[1];
	unsigned char lParam_c[IOCTRL_MAX_LEN] = {0};
	unsigned char wParam_c[IOCTRL_MAX_LEN] = {0};
	


	lParam_j = env->GetByteArrayElements(lParam, false);
	wParam_j = env->GetByteArrayElements(wParam, false);
	wplen_j = env->GetIntArrayElements(wplen, false);

	wplen_c[0] = wplen_j[0];
	
	for(i=0;i<lplen&&i<IOCTRL_MAX_LEN;i++)
	{
		lParam_c[i]=lParam_j[i];
	}
	for(i=0;i<wplen_c[0]&&i<IOCTRL_MAX_LEN;i++)
	{
		wParam_c[i]=wParam_j[i];
	}
	
	ret = spi_ddi_iccpsam_ioctl_for_java(nCmd,lplen,lParam_c,wplen_c,wParam_c);

	wplen_j[0] = wplen_c[0];
	
	for(i=0;i<wplen_c[0]&&i<IOCTRL_MAX_LEN;i++)
	{
		wParam_j[i]=wParam_c[i];
	}

	env->ReleaseByteArrayElements(lParam, lParam_j, 0);
	env->ReleaseByteArrayElements(wParam, wParam_j, 0);
	env->ReleaseIntArrayElements(wplen, wplen_j, 0);
	return ret;
}
/*****************************·ÇœÓ¿š*************************************/

static jint sub_rf_open(JNIEnv *env, jobject obj)
{
	int ret;
	spi_ddi_sys_init();
	ret = spi_ddi_rf_open();
	
	return ret;
}

static jint sub_rf_close(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_rf_close();
	
	return ret;
}

static jint sub_rf_poweron(JNIEnv *env, jobject obj,jint nType)
{
	int ret;
	int type = nType;
	
	ret = spi_ddi_rf_poweron(type);
	
	return ret;
}

static jint sub_rf_poweroff(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_rf_poweroff();
	
	return ret;
}

static jint sub_rf_get_status(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_rf_get_status();
	
	return ret;
}

static jint sub_rf_activate(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_rf_activate();
	
	return ret;
}

static jint sub_rf_exchange_apdu(JNIEnv *env, jobject obj,jbyteArray lpCApdu,jint lpCApduLen,
	jbyteArray lpRApdu,jintArray lpRApduLen,jint lpRApduSize)
{
	int i,ret,clen,rlen;
	int CApduLen = lpCApduLen;
	int RApduSize = lpRApduSize;
	unsigned int RApduLen[1];
	jbyte *lpCApdu_j;
	jbyte *lpRApdu_j;
	jint *lpRApduLen_j;
	unsigned char* lpCApdu_p;
	clen = env->GetArrayLength(lpCApdu);
	rlen = env->GetArrayLength(lpRApdu);
	unsigned char lpCApdu_c[APDU_MAX_LEN] = {0};
	unsigned char lpRApdu_c[APDU_MAX_LEN] = {0};
	
	lpCApdu_j = env->GetByteArrayElements(lpCApdu, false);
	lpRApdu_j = env->GetByteArrayElements(lpRApdu, false);
	lpRApduLen_j = env->GetIntArrayElements(lpRApduLen, false);
	lpCApdu_p = (unsigned char*)lpCApdu_j;
	for(i=0;i<lpCApduLen&&i<APDU_MAX_LEN&&i<clen;i++)
	{
		lpCApdu_c[i]=lpCApdu_p[i];
	}
	
	ret = spi_ddi_rf_exchange_apdu(lpCApdu_c,CApduLen,lpRApdu_c,RApduLen,RApduSize);

	lpRApduLen_j[0] = RApduLen[0];
	if(rlen<RApduLen[0])
	{
		return -10;
	}
	for(i=0;i<RApduLen[0]&&i<APDU_MAX_LEN;i++)
	{
		lpRApdu_j[i]=lpRApdu_c[i];
	}
	env->ReleaseByteArrayElements(lpCApdu, lpCApdu_j, 0);
	env->ReleaseByteArrayElements(lpRApdu, lpRApdu_j, 0);
	env->ReleaseIntArrayElements(lpRApduLen, lpRApduLen_j, 0);
	
	return ret;
}

static jint sub_rf_remove(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_rf_remove();
	
	return ret;
}

static jint sub_rf_ioctl(JNIEnv *env, jobject obj,jint nCmd,jint lParam,jint wParam)
{
	int ret;
	int cmd = nCmd;
	int lPar = lParam;
	int wPar = wParam;
	
	ret = spi_ddi_rf_ioctl(cmd,lPar,wPar);

	
	return ret;
}

static jint sub_rf_ioctl_for_java(JNIEnv *env, jobject obj,jint nCmd,
	jint lplen,jbyteArray lParam,jintArray wplen,jbyteArray wParam)
{
	int ret,i;

	unsigned int RApduLen[1];
	jbyte *lParam_j;
	jbyte *wParam_j;
	jint *wplen_j;
	
	unsigned int wplen_c[1];
	unsigned char lParam_c[IOCTRL_MAX_LEN] = {0};
	unsigned char wParam_c[IOCTRL_MAX_LEN] = {0};
	
	lParam_j = env->GetByteArrayElements(lParam, false);
	wParam_j = env->GetByteArrayElements(wParam, false);
	wplen_j = env->GetIntArrayElements(wplen, false);

	wplen_c[0] = wplen_j[0];
	
	for(i=0;i<lplen&&i<IOCTRL_MAX_LEN;i++)
	{
		lParam_c[i]=lParam_j[i];
	}
	for(i=0;i<wplen_c[0]&&i<IOCTRL_MAX_LEN;i++)
	{
		wParam_c[i]=wParam_j[i];
	}
	
	ret = spi_ddi_rf_ioctl_for_java(nCmd,lplen,lParam_c,wplen_c,wParam_c);

	wplen_j[0] = wplen_c[0];

	for(i=0;i<wplen_c[0]&&i<IOCTRL_MAX_LEN;i++)
	{
		wParam_j[i]=wParam_c[i];
	}

	env->ReleaseByteArrayElements(lParam, lParam_j, 0);
	env->ReleaseByteArrayElements(wParam, wParam_j, 0);
	env->ReleaseIntArrayElements(wplen, wplen_j, 0);
	
	return ret;
}
/*****************************ŽòÓ¡»ú*************************************/

static jint sub_thmprn_open(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_thmprn_open();
	
	return ret;
}

static jint sub_thmprn_close(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_thmprn_close();
	
	return ret;
}

static jint sub_thmprn_feed_paper(JNIEnv *env, jobject obj,jint nPixels)
{
	int ret;
	int pixels = nPixels;
	
	ret = spi_ddi_thmprn_feed_paper(pixels);
	
	return ret;
}

static jint sub_thmprn_print_image(JNIEnv *env, jobject obj,jint nOrgLeft, jint nImageWidth, jint nImageHeight, const jbyteArray lpImage)
{
	int ret,i,imageLen;
	int orgLeft = nOrgLeft;
	int imageWidth = nImageWidth;
	int imageHeight = nImageHeight;
	jbyte *lpImage_j;
	unsigned char lpImage_c[IMG_MAX_LEN] = {0};

	imageLen = env->GetArrayLength(lpImage);
	lpImage_j = env->GetByteArrayElements(lpImage, false);
	for(i=0;i<=imageLen && i<=IMG_MAX_LEN;i++)
	{
		lpImage_c[i]=lpImage_j[i];
	}
	
	ret = spi_ddi_thmprn_print_image(orgLeft,imageWidth,imageHeight,lpImage_c);

	env->ReleaseByteArrayElements(lpImage, lpImage_j, 0);
	return ret;
	
	return ret;
}
static jint sub_thmprn_print_image_file(JNIEnv *env, jobject obj,
	jint nOrgLeft, jint nImageWidth, jint nImageHeight, const jbyteArray lpImageName)
{
	int ret,i,nameLen;
	int orgLeft = nOrgLeft;
	int imageWidth = nImageWidth;
	int imageHeight = nImageHeight;
	jbyte *lpImageName_j;
	unsigned char lpImageName_c[IMG_NAME_MAX_LEN] = {0};

	nameLen = env->GetArrayLength(lpImageName);
	lpImageName_j = env->GetByteArrayElements(lpImageName, false);
	
	for(i=0; i<IMG_NAME_MAX_LEN && i<nameLen; i++)
	{
		lpImageName_c[i]=lpImageName_j[i];
	}
	
	ret = spi_ddi_thmprn_print_image_file(orgLeft,imageWidth,imageHeight,lpImageName_c);

	env->ReleaseByteArrayElements(lpImageName, lpImageName_j, 0);
	return ret;
}

static jint sub_thmprn_print_text(JNIEnv *env, jobject obj,jint align,jint offset,jint font,
	jint ascsize,jint asczoom,jint nativesize,jint nativezoom,jbyteArray lpText)
{
	int ret,textLen,i;
	strPrnTextCtrl TextCtrl;
	TextCtrl.m_align = align;
	TextCtrl.m_offset= offset;
	TextCtrl.m_font= font;
	TextCtrl.m_ascsize= ascsize;
	TextCtrl.m_asczoom= asczoom;
	TextCtrl.m_nativesize= nativesize;
	TextCtrl.m_nativezoom= nativezoom;

	jbyte *lpText_j;
	unsigned char lpText_c[PRN_TEXT_MAX_LEN] = {0};

	textLen = env->GetArrayLength(lpText);
	lpText_j = env->GetByteArrayElements(lpText, false);
//	ALOGE("print textLeng:%0d.....\n ",textLen);
	for(i=0; i<PRN_TEXT_MAX_LEN && i<textLen; i++)
	{
		lpText_c[i]=lpText_j[i];
	}

	ret =  spi_ddi_thmprn_print_text(&TextCtrl,lpText_c);

	
	env->ReleaseByteArrayElements(lpText, lpText_j, 0);
	return ret;
}

static jint sub_thmprn_print_comb_text
  (JNIEnv *env, jobject obj, jint nNum, jintArray m_x0, 
  jintArray m_y0, jintArray m_font, jintArray m_ascsize, 
  jintArray m_asczoom, jintArray m_nativesize, jintArray m_nativezoom, jobjectArray m_text)
{
	jint i,j,size,col,ret;
	jint *m_x0_j;
	jint *m_y0_j;
	jint *m_font_j;
	jint *m_ascsize_j;
	jint *m_asczoom_j;
	jint *m_nativesize_j;
	jint *m_nativezoom_j;
	jobjectArray m_text_j;
	jarray tempArray;
	jbyte *tempText;
	int num = nNum;
	unsigned char text[COMB_PRN_MAX][COMB_PRN_TEXT_MAX_LEN]= {0};
	strPrnCombTextCtrl TextCtrl[COMB_PRN_MAX]= {0};
	strPrnCombTextCtrl *PTextCtrl = TextCtrl;
	if(num>=256)
		return -1;
    	m_x0_j = env->GetIntArrayElements(m_x0, false);
	m_y0_j = env->GetIntArrayElements(m_y0, false);
	m_font_j = env->GetIntArrayElements(m_font, false);
	m_ascsize_j = env->GetIntArrayElements(m_ascsize, false);
	m_asczoom_j = env->GetIntArrayElements(m_asczoom, false);
	m_nativesize_j = env->GetIntArrayElements(m_nativesize, false);
	m_nativezoom_j = env->GetIntArrayElements(m_nativezoom, false);
	if((NULL == m_x0_j)||(NULL == m_y0_j)||(NULL == m_font_j)||(NULL == m_ascsize_j)||
		(NULL == m_asczoom_j)||(NULL == m_nativesize_j)||(NULL == m_nativezoom_j)) 
	{
       	return -1; /* exception occurred */
       }
//	size = env->GetArrayLength(m_text);
	
	for(i=0;i<num&&i<COMB_PRN_MAX;i++)
	{
		TextCtrl[i].m_x0 = m_x0_j[i];
		TextCtrl[i].m_y0 = m_y0_j[i];
		TextCtrl[i].m_font = m_font_j[i];
		TextCtrl[i].m_ascsize = m_ascsize_j[i];
		TextCtrl[i].m_asczoom = m_asczoom_j[i];
		TextCtrl[i].m_nativesize = m_nativesize_j[i];
		TextCtrl[i].m_nativezoom = m_nativezoom_j[i];

		tempArray = (jarray)env->GetObjectArrayElement(m_text, i);
      		col =  env->GetArrayLength(tempArray);
		tempText = env->GetByteArrayElements((jbyteArray)tempArray, 0 );

		for (j=0; j<col && j <COMB_PRN_TEXT_MAX_LEN; j++) 
	       {
	           text[i][j] = tempText[j];
	       }
	       env->ReleaseByteArrayElements( (jbyteArray)tempArray, tempText,0 );
		TextCtrl[i].m_text= &text[i][0];
	}


 //   ret = spi_ddi_thmprn_print_comb_text(num,&PTextCtrl);

	
	env->ReleaseIntArrayElements(m_x0, m_x0_j, 0);
	env->ReleaseIntArrayElements(m_y0, m_y0_j, 0);
	env->ReleaseIntArrayElements(m_font, m_font_j, 0);
	env->ReleaseIntArrayElements(m_ascsize, m_ascsize_j, 0);
	env->ReleaseIntArrayElements(m_asczoom, m_asczoom_j, 0);
	env->ReleaseIntArrayElements(m_nativesize, m_nativesize_j, 0);
	env->ReleaseIntArrayElements(m_nativezoom, m_nativezoom_j, 0);

	
	return ret;
}


static jint sub_thmprn_get_status(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_thmprn_get_status();
	
	return ret;
}

static jint sub_thmprn_ioctl(JNIEnv *env, jobject obj,jint nCmd,jint lParam,jint wParam)
{
	int ret;
	int cmd = nCmd;
	int lPar = lParam;
	int wPar = wParam;
	
	ret = spi_ddi_thmprn_ioctl(cmd,lPar,wPar);

	
	return ret;
}

static jint sub_thmprn_ioctl_for_java(JNIEnv *env, jobject obj,jint nCmd,
	jint lplen,jbyteArray lParam,jintArray wplen,jbyteArray wParam)
{
	int ret,i;

	unsigned int RApduLen[1];
	jbyte *lParam_j;
	jbyte *wParam_j;
	jint *wplen_j;
	
	unsigned int wplen_c[1];
	unsigned char lParam_c[IOCTRL_MAX_LEN] = {0};
	unsigned char wParam_c[IOCTRL_MAX_LEN] = {0};
	
	lParam_j = env->GetByteArrayElements(lParam, false);
	wParam_j = env->GetByteArrayElements(wParam, false);
	wplen_j = env->GetIntArrayElements(wplen, false);

	wplen_c[0] = wplen_j[0];
	
	for(i=0;i<lplen&&i<IOCTRL_MAX_LEN;i++)
	{
		lParam_c[i]=lParam_j[i];
	}
	for(i=0;i<wplen_c[0]&&i<IOCTRL_MAX_LEN;i++)
	{
		wParam_c[i]=wParam_j[i];
	}
	
	ret = spi_ddi_thmprn_ioctl_for_java(nCmd,lplen,lParam_c,wplen_c,wParam_c);

	wplen_j[0] = wplen_c[0];
	for(i=0;i<wplen_c[0]&&i<IOCTRL_MAX_LEN;i++)
	{
		wParam_j[i]=wParam_c[i];
	}

	env->ReleaseByteArrayElements(lParam, lParam_j, 0);
	env->ReleaseByteArrayElements(wParam, wParam_j, 0);
	env->ReleaseIntArrayElements(wplen, wplen_j, 0);
	
	return ret;
}

static jint sub_thmprn_test(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_thmprn_test();
	
	return ret;
}

static jint sub_printOneBitBMPImage(JNIEnv *env, jobject obj,jbyteArray imageName,jint nameLen)
{
	int ret,i,plen;
	jbyte *image_j;
	unsigned char image_c[IMG_NAME_MAX_LEN] = {0};

	plen = env->GetArrayLength(imageName);
	image_j = env->GetByteArrayElements(imageName, false);

	for(i=0;i<nameLen&&i<IMG_NAME_MAX_LEN&&i<plen; i++)
	{
		image_c[i]=image_j[i];
	}
	ret = printOneBitBMPImage(image_c);
	env->ReleaseByteArrayElements(imageName, image_j, 0);
	return ret;
}

static jint sub_printOneBitBMPImageByBuffer(JNIEnv *env, jobject obj,jbyteArray imageBuf,jint bufLen)
{
	int ret,i,plen,cursor,bfOffBits,width,high,org_width;
	jbyte *imageBuf_j;
	unsigned char *imageBuf_c;
	imageBuf_c = new unsigned char[bufLen];
	if(imageBuf_c==NULL)
	{
		return -9;
	}
	plen = env->GetArrayLength(imageBuf);
	if(plen<bufLen)
	{
		ALOGE("plen = %d\n", plen);
		ALOGE("bufLen = %d\n", bufLen);
		return -10;
	}
	imageBuf_j = env->GetByteArrayElements(imageBuf, false);

	cursor =  (int)imageBuf_j[10]&0xff;
	
    	width = (int)imageBuf_j[18]&0xff + 256* ((int)imageBuf_j[19]&0xff);
	org_width = width/8;
	if(width%8>0)
	{
	    org_width++;
	}
	org_width = (org_width+3)/4*4;
	
    	high = (int)imageBuf_j[22]&0xff + 256*((int)imageBuf_j[23]&0xff);
	high = (high+7)/8*8;
	if(org_width*high+cursor>bufLen)
	{
		ALOGE("cursor = %d\n", cursor);
		ALOGE("org_width = %d\n", org_width);
		ALOGE("high = %d\n", high);
		ALOGE("bufLen = %d\n", bufLen);
		ALOGE("width*high+cursor = %d\n", org_width*high+cursor);
		return -10;
	}
	
	for(i=0;i<bufLen; i++)
	{
		imageBuf_c[i]=imageBuf_j[i];
	}
	ret = printOneBitBMPImageByBuffer(imageBuf_c);
	env->ReleaseByteArrayElements(imageBuf, imageBuf_j, 0);
	delete imageBuf_c;
	return ret;
}

static jint sub_spi_ddi_thmprn_totalDot(JNIEnv *env, jobject obj,jbyteArray imageBuf,jint bufLen)
{
	int ret,i,plen;
	jbyte *imageBuf_j;
	unsigned char *imageBuf_c;
	imageBuf_c = new unsigned char[bufLen];
	if(imageBuf_c==NULL)
	{
		return -9;
	}
	plen = env->GetArrayLength(imageBuf);
	if(plen<bufLen)
	{
		return -10;
	}
	imageBuf_j = env->GetByteArrayElements(imageBuf, false);

	for(i=0;i<bufLen; i++)
	{
		imageBuf_c[i]=imageBuf_j[i];
	}
	ret = spi_ddi_thmprn_totalDot(imageBuf_c,bufLen);
	env->ReleaseByteArrayElements(imageBuf, imageBuf_j, 0);
	delete imageBuf_c;
	return ret;
}

static jint sub_printBlackBlock(JNIEnv *env, jobject obj,jint line)
{
	int ret;
	
	ret = printBlackBlock(line);

	
	return ret;
}
/*****************************ÄÚÖÃÃÜÔ¿*************************************/

static jint sub_innerkey_open(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_innerkey_open();
	
	return ret;
}

static jint sub_innerkey_close(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_innerkey_close();
	
	return ret;
}

static jint sub_innerkey_inject(JNIEnv *env, jobject obj,jint nKeyArea,jint nIndex,const jbyteArray lpKeyData)
{
	int ret,keyLen,i;
       jbyte *lpKeyData_j;
	unsigned char lpKeyData_c[INNER_KEY_MAX_LEN] = {0};
	int keyArea = nKeyArea;
	int index = nIndex;
	
	keyLen = env->GetArrayLength(lpKeyData);
	lpKeyData_j = env->GetByteArrayElements(lpKeyData, false);
	for(i=0; i<INNER_KEY_MAX_LEN && i<keyLen; i++)
	{
		lpKeyData_c[i]=lpKeyData_j[i];
	}

	ret = spi_ddi_innerkey_inject(keyArea,index,lpKeyData_c);

	env->ReleaseByteArrayElements(lpKeyData, lpKeyData_j, 0);
	return ret;
}

static jint sub_innerkey_encrypt(JNIEnv *env, jobject obj,jint nKeyArea,jint nIndex,
	jint nLen, const jbyteArray lpIn, jbyteArray lpOut)
{
	int ret,keyLen = nLen,i;
       jbyte *lpIn_j;
	jbyte *lpOut_j;
	unsigned char lpIn_c[INNER_DATA_MAX_LEN] = {0} ;
	unsigned char lpOut_c[INNER_DATA_MAX_LEN]= {0};
	int keyArea = nKeyArea;
	int index = nIndex;
	if(keyLen>INNER_DATA_MAX_LEN)
		return -1;
	
	lpIn_j = env->GetByteArrayElements(lpIn, false);
	lpOut_j = env->GetByteArrayElements(lpOut, false);
	for(i=0;i<keyLen; i++)
	{
		lpIn_c[i]=lpIn_j[i];
	}

	ret = spi_ddi_innerkey_encrypt(keyArea,index,keyLen,lpIn_c,lpOut_c);

	for(i=0;i<keyLen;i++)
	{
		lpOut_j[i] = lpOut_c[i];
	}
	env->ReleaseByteArrayElements(lpIn, lpIn_j, 0);
	env->ReleaseByteArrayElements(lpOut, lpOut_j, 0);
	return ret;
}

static jint sub_innerkey_decrypt(JNIEnv *env, jobject obj,jint nKeyArea,
	jint nIndex,jint nLen, const jbyteArray lpIn, jbyteArray lpOut)
{
	int ret,keyLen = nLen,i;
       jbyte *lpIn_j;
	jbyte *lpOut_j;
	unsigned char lpIn_c[INNER_DATA_MAX_LEN];
	unsigned char lpOut_c[INNER_DATA_MAX_LEN];
	int keyArea = nKeyArea;
	int index = nIndex;
	if(keyLen>INNER_DATA_MAX_LEN)
		return -1;
	
	lpIn_j = env->GetByteArrayElements(lpIn, false);
	lpOut_j = env->GetByteArrayElements(lpOut, false);
	for(i=0;i<keyLen; i++)
	{
		lpIn_c[i]=lpIn_j[i];
	}

	ret = spi_ddi_innerkey_decrypt(keyArea,index,keyLen,lpIn_c,lpOut_c);

	for(i=0;i<keyLen;i++)
	{
		lpOut_j[i] = lpOut_c[i];
	}
	env->ReleaseByteArrayElements(lpIn, lpIn_j, 0);
	env->ReleaseByteArrayElements(lpOut, lpOut_j, 0);
	return ret;
}

static jint sub_innerkey_ioctl(JNIEnv *env, jobject obj,jint nCmd,jint lParam,jint wParam)
{
	int ret;
	int cmd = nCmd;
	int lPar = lParam;
	int wPar = wParam;
	
//	ret = core_ddi_innerkey_ioctl(cmd,lPar,wPar);

	
	return ret;
}

static jint sub_innerkey_ioctl_for_java(JNIEnv *env, jobject obj,jint nCmd,
	jint lplen,jbyteArray lParam,jintArray wplen,jbyteArray wParam)
{
	int ret,i;

	unsigned int RApduLen[1];
	jbyte *lParam_j;
	jbyte *wParam_j;
	jint *wplen_j;
	
	unsigned int wplen_c[1];
	unsigned char lParam_c[IOCTRL_MAX_LEN] = {0};
	unsigned char wParam_c[IOCTRL_MAX_LEN] = {0};
	

	lParam_j = env->GetByteArrayElements(lParam, false);
	wParam_j = env->GetByteArrayElements(wParam, false);
	wplen_j = env->GetIntArrayElements(wplen, false);

	wplen_c[0] = wplen_j[0];
	
	for(i=0;i<lplen&&i<IOCTRL_MAX_LEN;i++)
	{
		lParam_c[i]=lParam_j[i];
	}
	for(i=0;i<wplen_c[0]&&i<IOCTRL_MAX_LEN;i++)
	{
		wParam_c[i]=wParam_j[i];
	}
	
	ret = spi_ddi_innerkey_ioctl_for_java(nCmd,lplen,lParam_c,wplen_c,wParam_c);
	ALOGE("wplen_c = %d\n",wplen_c[0]);
	wplen_j[0] = wplen_c[0];
	
	for(i=0;i<wplen_c[0]&&i<IOCTRL_MAX_LEN;i++)
	{
		ALOGE("[%02x]  ",wParam_c[i]);
		wParam_j[i]=wParam_c[i];
	}
	ALOGE("\n ");
	env->ReleaseByteArrayElements(lParam, lParam_j, 0);
	env->ReleaseByteArrayElements(wParam, wParam_j, 0);
	env->ReleaseIntArrayElements(wplen, wplen_j, 0);
	return ret;
}
/*****************************Ö€Êé*************************************/

static jint sub_ddi_certmodule_open(JNIEnv *env, jobject obj)
{
	int ret;
	REPORT_FUNCTION();
	ret = spi_ddi_certmodule_open();
	
	return ret;
}

static jint sub_ddi_certmodule_close(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_certmodule_close();
	
	return ret;
}

static jint sub_ddi_certmodule_save(JNIEnv *env, jobject obj,jbyteArray CertName,jbyteArray  StrID,
	jbyteArray  StrLabel,
		jbyte ObjectType,jbyte DataType,jbyteArray Length,
		jbyteArray pObjectData,jint nDataLength)
{
	int ret,textLen,i;
	HSM_ObjectProperty cert;
       int DataLength = nDataLength;
	jbyte *CertName_j;
	jbyte *StrID_j;
	jbyte *StrLabel_j;
	jbyte *StrPassword_j;
	jbyte *Signer_StrID_j;
	jbyte *Leng_j;
	
	jbyte *pObjectData_j;
	unsigned char pObjectData_c[CERT_DATA_MAX_LEN];
	REPORT_FUNCTION();
	cert.m_nObjectType = ObjectType;
	cert.m_nDataType = DataType;
	
	CertName_j = env->GetByteArrayElements(CertName, false);
	for(i=0; i<10; i++)
	{
		cert.m_CertName[i]=CertName_j[i];
	}
	StrID_j = env->GetByteArrayElements(StrID, false);
	for(i=0; i<32; i++)
	{
		cert.m_StrID[i]=StrID_j[i];
	}
	StrLabel_j = env->GetByteArrayElements(StrLabel, false);
	for(i=0; i<32; i++)
	{
		cert.m_StrLabel[i]=StrLabel_j[i];
	}
	Leng_j = env->GetByteArrayElements(Length, false);
	for(i=0; i<4; i++)
	{
		cert.m_nDataLength[i]=Leng_j[i];
	}
	
	pObjectData_j = env->GetByteArrayElements(pObjectData, false);
	for(i=0; i<nDataLength&&i<CERT_DATA_MAX_LEN; i++)
	{
		pObjectData_c[i]=pObjectData_j[i];
	}

//	ALOGE("\DataLength = %d\n ",DataLength);
	ret =  spi_ddi_certmodule_save(&cert,pObjectData_c,DataLength);

	
	env->ReleaseByteArrayElements(CertName, CertName_j, 0);
	env->ReleaseByteArrayElements(StrID, StrID_j, 0);
	env->ReleaseByteArrayElements(StrLabel, StrLabel_j, 0);
	env->ReleaseByteArrayElements(Length, Leng_j, 0);
	env->ReleaseByteArrayElements(pObjectData, pObjectData_j, 0);
	return ret;
}


static jint sub_ddi_certmodule_readByName(JNIEnv *env, jobject obj,jbyteArray CertName,jbyteArray  StrID,
	jbyteArray  StrLabel,
		jbyteArray ObjectType,jbyteArray DataType,jbyteArray Length,
		jbyteArray pObjectData,jintArray nDataLength)
{
	int ret,textLen,i;
	HSM_ObjectProperty cert;

	jbyte *CertName_j;
	jbyte *StrID_j;
	jbyte *StrLabel_j;
	jbyte *ObjectType_j;
	jbyte *DataType_j;
	jbyte *Length_j;
	
	jbyte *pObjectData_j;
	jint *nDataLength_j;
	unsigned char pObjectData_c[CERT_DATA_MAX_LEN];
	unsigned int nDataLength_c[2];
	
	

	
	CertName_j = env->GetByteArrayElements(CertName, false);
	for(i=0; i<10; i++)
	{
		cert.m_CertName[i]=CertName_j[i];
	}
	StrID_j = env->GetByteArrayElements(StrID, false);
	StrLabel_j = env->GetByteArrayElements(StrLabel, false);
	ObjectType_j = env->GetByteArrayElements(ObjectType, false);
	DataType_j = env->GetByteArrayElements(DataType, false);
	Length_j = env->GetByteArrayElements(Length, false);
	pObjectData_j = env->GetByteArrayElements(pObjectData, false);
	nDataLength_j = env->GetIntArrayElements(nDataLength, false);
	
	ret =  spi_ddi_certmodule_readByName(&cert,pObjectData_c,nDataLength_c);

	for(i=0; i<10; i++)
	{
		CertName_j[i] = cert.m_CertName[i];
	}
	for(i=0; i<32; i++)
	{
		StrID_j[i] = cert.m_StrID[i];
	}
	for(i=0; i<32; i++)
	{
		StrLabel_j[i] = cert.m_StrLabel[i];
	}
	ObjectType_j[0] = cert.m_nObjectType;
	DataType_j[0] = cert.m_nDataType;	
	for(i=0; i<4; i++)
	{
		Length_j[i] = cert.m_nDataLength[i];
	}
	nDataLength_j[0] = nDataLength_c[0];
	for(i=0;i<nDataLength_j[0];i++)
	{
		pObjectData_j[i] = pObjectData_c[i];
	}
	
	env->ReleaseByteArrayElements(CertName, CertName_j, 0);
	env->ReleaseByteArrayElements(StrID, StrID_j, 0);
	env->ReleaseByteArrayElements(ObjectType, ObjectType_j, 0);
	env->ReleaseByteArrayElements(DataType, DataType_j, 0);
	env->ReleaseByteArrayElements(StrLabel, StrLabel_j, 0);
	env->ReleaseByteArrayElements(pObjectData, pObjectData_j, 0);
	env->ReleaseIntArrayElements(nDataLength, nDataLength_j, 0);
	return ret;
}

#if 0
static jint sub_ddi_certmodule_readByID(JNIEnv *env, jobject obj,jbyteArray CertName,jbyteArray  StrID,
	jbyteArray  StrLabel,jbyteArray  StrPassword,
		jint ObjectType,jint DataType,jint CertUseMode,jint CertLevel,jbyteArray  Signer_StrID ,jbyteArray Signer_StrLabel,
		jbyteArray pObjectData,jintArray nDataLength)
{
	int ret,textLen,i;
	HSM_ObjectProperty cert;

	jbyte *CertName_j;
	jbyte *StrID_j;
	jbyte *StrLabel_j;
	jbyte *StrPassword_j;
	jbyte *Signer_StrID_j;
	jbyte *Signer_StrLabel_j;
	
	jbyte *pObjectData_j;
	jint *nDataLength_j;
	unsigned char pObjectData_c[CERT_DATA_MAX_LEN];
	unsigned int nDataLength_c[2];
	
	cert.m_nObjectType = ObjectType;
	cert.m_nDataType = DataType;
	cert.m_CertUseMode = CertUseMode;
	cert.m_CertLevel = CertLevel;
	
	CertName_j = env->GetByteArrayElements(CertName, false);
	for(i=0; i<10; i++)
	{
		cert.m_CertName[i]=CertName_j[i];
	}
	StrID_j = env->GetByteArrayElements(StrID, false);
	for(i=0; i<32; i++)
	{
		cert.m_StrID[i]=StrID_j[i];
	}
	StrLabel_j = env->GetByteArrayElements(StrLabel, false);
	for(i=0; i<32; i++)
	{
		cert.m_StrLabel[i]=StrLabel_j[i];
	}
	StrPassword_j = env->GetByteArrayElements(StrPassword, false);
	for(i=0; i<32; i++)
	{
		cert.m_StrPassword[i]=StrPassword_j[i];
	}
	Signer_StrID_j = env->GetByteArrayElements(Signer_StrID, false);
	for(i=0; i<32; i++)
	{
		cert.m_Signer_StrID[i]=Signer_StrID_j[i];
	}
	Signer_StrLabel_j = env->GetByteArrayElements(Signer_StrLabel, false);
	for(i=0; i<32; i++)
	{
		cert.m_Signer_StrLabel[i]=Signer_StrLabel_j[i];
	}
	
	pObjectData_j = env->GetByteArrayElements(pObjectData, false);
	nDataLength_j = env->GetIntArrayElements(nDataLength, false);
	
	ret =  spi_ddi_certmodule_readByID(&cert,pObjectData_c,nDataLength_c);

	nDataLength_j[0] = nDataLength_c[0];
	for(i=0;i<nDataLength_j[0];i++)
	{
		pObjectData_j[i] = pObjectData_c[i];
	}
	
	env->ReleaseByteArrayElements(CertName, CertName_j, 0);
	env->ReleaseByteArrayElements(StrID, StrID_j, 0);
	env->ReleaseByteArrayElements(StrLabel, StrLabel_j, 0);
	env->ReleaseByteArrayElements(StrPassword, StrPassword_j, 0);
	env->ReleaseByteArrayElements(Signer_StrID, Signer_StrID_j, 0);
	env->ReleaseByteArrayElements(Signer_StrLabel, Signer_StrLabel_j, 0);
	env->ReleaseByteArrayElements(pObjectData, pObjectData_j, 0);
	env->ReleaseIntArrayElements(nDataLength, nDataLength_j, 0);
	return ret;
}
#endif

static jint sub_ddi_certmodule_querycount(JNIEnv *env, jobject obj,jintArray certNum)
{
	int ret;
	unsigned int certNum_c[2];
	jint *certNum_j;

	certNum_j = env->GetIntArrayElements(certNum, false);
	
	ret = spi_ddi_certmodule_querycount(certNum_c);

	certNum_j[0] = certNum_c[0];
	env->ReleaseIntArrayElements(certNum, certNum_j, 0);
	return ret;
}

static jint sub_ddi_certmodule_delete(JNIEnv *env, jobject obj,jint certid,jbyteArray CertName,jbyteArray  StrID,
	jbyteArray  StrLabel,
		jbyte ObjectType,jbyte DataType,jbyteArray Length,
		jbyteArray verifydata)
{
	int ret,textLen,i;
	HSM_ObjectProperty cert;

	jbyte *CertName_j;
	jbyte *StrID_j;
	jbyte *StrLabel_j;
	jbyte *StrPassword_j;
	jbyte *Signer_StrID_j;
	jbyte *Signer_StrLabel_j;
	
	jbyte *verifydata_j;
	unsigned char verifydata_c[CERT_DATA_MAX_LEN];

	
	CertName_j = env->GetByteArrayElements(CertName, false);
	for(i=0; i<10; i++)
	{
		cert.m_CertName[i]=CertName_j[i];
	}
	StrID_j = env->GetByteArrayElements(StrID, false);
	StrLabel_j = env->GetByteArrayElements(StrLabel, false);
	verifydata_j = env->GetByteArrayElements(verifydata, false);
	for(i=0; i<sizeof(verifydata_j); i++)
	{
		verifydata_c[i]=verifydata_j[i];
	}


	ret =  spi_ddi_certmodule_delete(certid,&cert,verifydata_c);

	
	env->ReleaseByteArrayElements(CertName, CertName_j, 0);
	env->ReleaseByteArrayElements(StrID, StrID_j, 0);
	env->ReleaseByteArrayElements(StrLabel, StrLabel_j, 0);
	env->ReleaseByteArrayElements(verifydata, verifydata_j, 0);
	return ret;
}

static jint sub_ddi_certmodule_deleteall(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_certmodule_deleteall();
	
	return ret;
}

static jint sub_spi_ddi_pk_verify_hash(JNIEnv *env, jobject obj,jint nlen,jbyteArray data)
{
	int ret,i;
	jbyte *data_j;
	
	unsigned char data_c[50];
	data_j = env->GetByteArrayElements(data, false);
	if(nlen<50)
	{
		for(i=0;i<nlen;i++)
		{
			data_c[i] = data_j[i];
		}
	}
	else
		return -9;
	ret = spi_ddi_pk_verify_hash(nlen,data_c);
	
	env->ReleaseByteArrayElements(data, data_j, 0);
	
	
	return ret;
}

/*****************************LED*************************************/

static jint sub_led_open(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_led_open(); 
	
	return ret;
}

static jint sub_led_close(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_led_close();
	
	return ret;
}

static jint sub_led_sta_set(JNIEnv *env, jobject obj,jint nLed, jint nSta)
{
	int ret;
	int led = nLed,sta = nSta;
	printf("sub_led_sta_set:%d, %d\n", nLed, nSta);

	ret = spi_ddi_led_sta_set(led,sta);
	
	return ret;
}

static jint sub_led_ioctl_for_java(JNIEnv *env, jobject obj,jint nCmd,
	jint lplen,jbyteArray lParam,jintArray wplen,jbyteArray wParam)
{
	int ret,i;

	unsigned int RApduLen[1];
	jbyte *lParam_j;
	jbyte *wParam_j;
	jint *wplen_j;
	
	unsigned int wplen_c[1];
	unsigned char lParam_c[IOCTRL_MAX_LEN] = {0};
	unsigned char wParam_c[IOCTRL_MAX_LEN] = {0};
	
	lParam_j = env->GetByteArrayElements(lParam, false);
	wParam_j = env->GetByteArrayElements(wParam, false);
	wplen_j = env->GetIntArrayElements(wplen, false);

	wplen_c[0] = wplen_j[0];
	
	for(i=0;i<lplen&&i<IOCTRL_MAX_LEN;i++)
	{
		lParam_c[i]=lParam_j[i];
	}
	for(i=0;i<wplen_c[0]&&i<IOCTRL_MAX_LEN;i++)
	{
		wParam_c[i]=wParam_j[i];
	}
	
	ret = spi_ddi_led_ioctl_for_java(nCmd,lplen,lParam_c,wplen_c,wParam_c);

	wplen_j[0] = wplen_c[0];

	for(i=0;i<wplen_c[0]&&i<IOCTRL_MAX_LEN;i++)
	{
		wParam_j[i]=wParam_c[i];
	}
	env->ReleaseByteArrayElements(lParam, lParam_j, 0);
	env->ReleaseByteArrayElements(wParam, wParam_j, 0);
	env->ReleaseIntArrayElements(wplen, wplen_j, 0);
	return ret;
}

/*****************************key*************************************/

static jint sub_key_open(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_key_open(); 
	
	return ret;
}

static jint sub_key_close(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_key_close();
	
	return ret;
}

static jint sub_key_clear(JNIEnv *env, jobject obj)
{
	int ret;

	ret = spi_ddi_key_clear();
	
	return ret;
}

static jint sub_key_read(JNIEnv *env, jobject obj,jintArray lpKey)
{
	int ret;
	jint *key_j;
      unsigned int key_c[1] = {0};

//	LOGI("sub_iccpsam_poweron\n");
	key_j = env->GetIntArrayElements(lpKey, false);
	ret = spi_ddi_key_read(key_c);
	key_j[0] = key_c[0];
	env->ReleaseIntArrayElements(lpKey, key_j, 0);
	return ret;
}

static jint sub_key_ioctl(JNIEnv *env, jobject obj,jint nCmd,jint lParam,jint wParam)
{
	int ret;
	int cmd = nCmd;
	int lPar = lParam;
	int wPar = wParam;
	
	ret = spi_ddi_key_ioctl_for_java(cmd,lPar,wPar);

	
	return ret;
}
/*****************************dukpt*************************************/

static jint sub_dukpt_open(JNIEnv *env, jobject obj)
{
	int ret;

//	ret = core_ddi_dukpt_open();
	
	return ret;
}

static jint sub_dukpt_close(JNIEnv *env, jobject obj)
{
	int ret;

//	ret = core_ddi_dukpt_close();
	
	return ret;
}

static jint sub_dukpt_inject(JNIEnv *env, jobject obj,jbyte m_groupindex,jbyte m_keyindex,jbyteArray m_initkey,
		jbyte m_keylen,jbyte m_ksnindex,jbyteArray m_initksn,jbyte m_ksnlen)
{
	
	int ret,i,keylen = m_keylen,ksnlen = m_ksnlen;
	strDukptInitInfo InitInfo;
	jbyte *m_initkey_j;
	jbyte *m_initksn_j;

	m_initkey_j = env->GetByteArrayElements(m_initkey, false);
	m_initksn_j = env->GetByteArrayElements(m_initksn, false);
	
	InitInfo.m_groupindex = m_groupindex;
	InitInfo.m_keyindex= m_keyindex;
	InitInfo.m_keylen= m_keylen;
	InitInfo.m_ksnindex= m_ksnindex;
	InitInfo.m_ksnlen= m_ksnlen;
	for(i=0;i<keylen&&i<DUKPT_KEY_MAX_LEN; i++)
	{
		InitInfo.m_initkey[i]=m_initkey_j[i];
	}
	for(i=0;i<ksnlen&&i<DUKPT_SN_MAX_LEN; i++)
	{
		InitInfo.m_initksn[i]=m_initksn_j[i];
	}
	
//	ret = core_ddi_dukpt_inject(&InitInfo);

	env->ReleaseByteArrayElements(m_initkey, m_initkey_j, 0);
	env->ReleaseByteArrayElements(m_initksn, m_initksn_j, 0);
	return ret;
}

static jint sub_dukpt_encrypt(JNIEnv *env, jobject obj,jint nKeyGroup,
	jint nKeyIndex,jint nLen, const jbyteArray lpIn, jbyteArray lpOut, jint nMode)
{
	int ret,keyLen = nLen,i,mode = nMode;
       jbyte *lpIn_j;
	jbyte *lpOut_j;
	unsigned char lpIn_c[INNER_DATA_MAX_LEN];
	unsigned char lpOut_c[INNER_DATA_MAX_LEN];
	int keyGroup = nKeyGroup;
	int index = nKeyIndex;
	if(keyLen>INNER_DATA_MAX_LEN)
		return -1;
	
	lpIn_j = env->GetByteArrayElements(lpIn, false);
	lpOut_j = env->GetByteArrayElements(lpOut, false);
	for(i=0;i<keyLen; i++)
	{
		lpIn_c[i]=lpIn_j[i];
	}

//	ret = core_ddi_dukpt_encrypt(keyGroup,index,keyLen,lpIn_c,lpOut_c,mode);

	for(i=0;i<keyLen;i++)
	{
		lpOut_j[i] = lpOut_c[i];
	}
	env->ReleaseByteArrayElements(lpIn, lpIn_j, 0);
	env->ReleaseByteArrayElements(lpOut, lpOut_j, 0);
	return ret;
}

static jint sub_dukpt_decrypt(JNIEnv *env, jobject obj,jint nKeyGroup,
	jint nKeyIndex,jint nLen, const jbyteArray lpIn, jbyteArray lpOut, jint nMode)
{
	int ret,keyLen = nLen,i,mode = nMode;
       jbyte *lpIn_j;
	jbyte *lpOut_j;
	unsigned char lpIn_c[INNER_DATA_MAX_LEN];
	unsigned char lpOut_c[INNER_DATA_MAX_LEN];
	int keyGroup = nKeyGroup;
	int index = nKeyIndex;
	if(keyLen>INNER_DATA_MAX_LEN)
		return -1;
	
	lpIn_j = env->GetByteArrayElements(lpIn, false);
	lpOut_j = env->GetByteArrayElements(lpOut, false);
	for(i=0;i<keyLen; i++)
	{
		lpIn_c[i]=lpIn_j[i];
	}

//	ret = core_ddi_dukpt_decrypt(keyGroup,index,keyLen,lpIn_c,lpOut_c,mode);

	for(i=0;i<keyLen;i++)
	{
		lpOut_j[i] = lpOut_c[i];
	}
	env->ReleaseByteArrayElements(lpIn, lpIn_j, 0);
	env->ReleaseByteArrayElements(lpOut, lpOut_j, 0);
	return ret;
}

static jint sub_dukpt_getksn(JNIEnv *env, jobject obj,jint nKeyGroup,
	jint nKeyIndex,jintArray nLen, jbyteArray lpOut)
{
	int ret,i;
       jint *nLen_j;
	jbyte *lpOut_j;
	unsigned int nLen_c[2];
	unsigned char lpOut_c[INNER_DATA_MAX_LEN];
	int keyGroup = nKeyGroup;
	int index = nKeyIndex;
	
	nLen_j = env->GetIntArrayElements(nLen, false);
	lpOut_j = env->GetByteArrayElements(lpOut, false);
	
	nLen_c[0]=nLen_j[0];

//	ret = core_ddi_dukpt_getksn(keyGroup,index,nLen_c,lpOut_c);

	for(i=0;i<nLen_c[0];i++)
	{
		lpOut_j[i] = lpOut_c[i];
	}
	env->ReleaseIntArrayElements(nLen, nLen_j, 0);
	env->ReleaseByteArrayElements(lpOut, lpOut_j, 0);
	return ret;
}

static jint sub_dukpt_ioctl(JNIEnv *env, jobject obj,jint nCmd,jint lParam,jint wParam)
{
	int ret;
	int cmd = nCmd;
	int lPar = lParam;
	int wPar = wParam;
	
//	ret = core_ddi_dukpt_ioctl(cmd,lPar,wPar);
	
	return ret;
}

static jint sub_pin_input(JNIEnv *env, jobject obj,jint  pinlenmin,jint pinlenmax,jint timeout,jint bypassflag)
{
	int ret;
	
	ret = spi_ddi_pin_input(pinlenmin,pinlenmax,timeout,bypassflag);
	
	return ret;
}

static jint sub_pin_input_press(JNIEnv *env, jobject obj,jbyteArray keycode)
{
	int ret;
	jbyte *keycode_j;
	unsigned char keycode_c[1];

	keycode_j = env->GetByteArrayElements(keycode, false);
	ret = spi_ddi_pin_input_press(keycode_c);

	keycode_j[0] = keycode_c[0];
	env->ReleaseByteArrayElements(keycode, keycode_j, 0);
	return ret;
}



static jint sub_pin_input_cancel(JNIEnv *env, jobject obj)
{
	int ret;
	
	ret = spi_ddi_pin_input_cancel();

	return ret;
}


static jint sub_pin_getonlinepinblock(JNIEnv *env, jobject obj,jint  pinAlgorithmMode,
	jint keyindex,jint cardlen,jbyteArray carddata,jbyteArray pinblockdata)
{
	int ret,i,plen;
	jbyte *carddata_j;
	jbyte *pinblockdata_j;
	unsigned char carddata_c[56];
	unsigned char pinblockdata_c[PINBLOCK_LEN];

	plen = env->GetArrayLength(pinblockdata);
	if(plen<PINBLOCK_LEN)
		return -10;
	
	carddata_j = env->GetByteArrayElements(carddata, false);
	pinblockdata_j = env->GetByteArrayElements(pinblockdata, false);

	if(cardlen<56)
	{
		for(i=0;i<cardlen; i++)
		{
			carddata_c[i]=carddata_j[i];
		}
	}
	else
	{
		ALOGE("cardlen err-------");
		return -9;
	}
	ret = spi_ddi_pin_getonlinepinblock(pinAlgorithmMode,keyindex,cardlen,carddata_c,pinblockdata_c);

	if(0==ret)
	{
		for(i=0;i<PINBLOCK_LEN;i++)
		{
			pinblockdata_j[i] = pinblockdata_c[i];
		}
	}
	env->ReleaseByteArrayElements(carddata, carddata_j, 0);
	env->ReleaseByteArrayElements(pinblockdata, pinblockdata_j, 0);
	return ret;
}

static jint sub_ddi_pin_getofflineencpin(JNIEnv *env, jobject obj,jbyteArray offlineencdata,int dataLen)
{
	int ret,i,plen;
	jbyte *offlineencdata_j;
	unsigned char offlineencdata_c[PINBLOCK_LEN];

	plen = env->GetArrayLength(offlineencdata);
	offlineencdata_j = env->GetByteArrayElements(offlineencdata, false);

	if(plen<PINBLOCK_LEN)
		return -10;
	for(i=0;i<PINBLOCK_LEN; i++)
	{
		offlineencdata_c[i]=offlineencdata_j[i];
	}
	
	ret = spi_ddi_pin_getofflineencpin(offlineencdata_c,dataLen);

	for(i=0;i<PINBLOCK_LEN;i++)
	{
		offlineencdata_j[i] = offlineencdata_c[i];
	}

	env->ReleaseByteArrayElements(offlineencdata, offlineencdata_j, 0);
	return ret;
}


static jint sub_ddi_sys_getCertHash(JNIEnv *env, jobject obj,jbyteArray hash)
{
	int ret,i,plen;
	jbyte *hash_j;
	
	unsigned char hash_c[HASH_LEN];
	plen = env->GetArrayLength(hash);
	if(plen<HASH_LEN)
		return -10;
	
	hash_j = env->GetByteArrayElements(hash, false);
	ret = ddi_sys_getCertHash(hash_c);
	for(i=0;i<HASH_LEN;i++)
	{
		hash_j[i] = hash_c[i];
	}
	env->ReleaseByteArrayElements(hash, hash_j, 0);
	
	return ret;
}

static jint sub_ddi_sys_setCertHash(JNIEnv *env, jobject obj,jbyteArray hash)
{
	int ret,i,plen;
	jbyte *hash_j;
	
	unsigned char hash_c[HASHDATA_LEN];
	plen = env->GetArrayLength(hash);
	hash_j = env->GetByteArrayElements(hash, false);
	if(plen<HASHDATA_LEN)
		return -10;
	for(i=0;i<HASHDATA_LEN;i++)
	{
		hash_c[i] = hash_j[i] ;
	}
	ret = ddi_sys_setCertHash(hash_c);
	env->ReleaseByteArrayElements(hash, hash_j, 0);
	
	return ret;
}

static jint sub_setGpioValue(JNIEnv *env, jobject obj,jint gpio_no, jint gpio_val)
{
	int ret;
	
	
	ret = setGpioValue(gpio_no,gpio_val);
	
	return ret;
}

static jint sub_getGpioValue(JNIEnv *env, jobject obj,jint gpio_no)
{
	int ret;

	ret = getGpioValue(gpio_no);
	
	return ret;
}

static jint sub_setScannerEnable(JNIEnv *env, jobject obj,jint on)
{
	int ret;

	ret = setScannerEnable(on);
	
	return ret;
}

static jint sub_getScannerEnableStatus(JNIEnv *env, jobject obj)
{
	int ret;

	ret = getScannerEnableStatus();
	
	return ret;
}

static jint sub_setScannerPowerOn(JNIEnv *env, jobject obj,jint on)
{
	int ret;

	ret = setScannerPowerOn(on);
	
	return ret;
}

static jint sub_getScannerPowerOnStatus(JNIEnv *env, jobject obj)
{
	int ret;

	ret = getScannerPowerOnStatus();
	
	return ret;
}

static jint sub_setScannerUsbSwitch(JNIEnv *env, jobject obj,jint on)
{
	int ret;

	ret = setScannerUsbSwitch(on);
	
	return ret;
}

static jint sub_getScannerUsbSwitchStatus(JNIEnv *env, jobject obj)
{
	int ret;

	ret = getScannerUsbSwitchStatus();
	
	return ret;
}

static jint sub_ddi_BodyNumber_process_download(JNIEnv *env, jobject obj)
{
	int ret;

	ret = BodyNumber_process_download();
	
	return ret;
}

static jint sub_ddi_BodyNumber_process_serial_read(JNIEnv *env, jobject obj)
{
	int ret;

	ret = BodyNumber_process_serial_read();
	
	return ret;
}

static jint sub_ddi_open_port(JNIEnv *env, jobject obj)
{
	int ret;

	ret = open_port();
	
	return ret;
}
static jint sub_ddi_close_port(JNIEnv *env, jobject obj)
{
	int ret;

	ret = close_port();
	
	return ret;
}

static jint sub_ddi_security_getstatus(JNIEnv *env, jobject obj,jbyteArray status)
{
	int ret,i,plen;
	jbyte *status_j;
	
	unsigned char status_c[SECURE_STATU_LEN];
	plen = env->GetArrayLength(status);
	if(plen<SECURE_STATU_LEN)
		return -10;
	status_j = env->GetByteArrayElements(status, false);
	ret = spi_ddi_security_getstatus(status_c);
	for(i=0;i<SECURE_STATU_LEN;i++)
	{
		status_j[i] = status_c[i];
	}
	env->ReleaseByteArrayElements(status, status_j, 0);
	
	return ret;
}

static jint sub_ddi_spi_communication_test(JNIEnv *env, jobject obj,jbyteArray data,jint nlen)
{
	int ret,i;
	jbyte *data_j;
	
	unsigned char data_c[800];
	data_j = env->GetByteArrayElements(data, false);
	if(nlen<800)
	{
		for(i=0;i<nlen;i++)
		{
			data_c[i] = data_j[i];
		}
	}
	else
		return -9;
	ret = spi_ddi_spi_communication_test(data_c,nlen);
	
	env->ReleaseByteArrayElements(data, data_j, 0);
	
	
	return ret;
}

static jint sub_ddi_sys_get_Hardware_Ver(JNIEnv *env, jobject obj)
{
	int ret;

	ret = ddi_sys_get_Hardware_Ver();
	
	return ret;
}

static jint sub_spi_ddi_sys_set_dsn(JNIEnv *env, jobject obj,jint nlen,jbyteArray data)
{
	int ret,i;
	jbyte *data_j;
	
	unsigned char data_c[20];
	data_j = env->GetByteArrayElements(data, false);
	if(nlen<20)
	{
		for(i=0;i<nlen;i++)
		{
			data_c[i] = data_j[i];
		}
	}
	else
		return -9;
	ret = spi_ddi_sys_set_dsn(nlen,data_c);
	
	env->ReleaseByteArrayElements(data, data_j, 0);
	
	
	return ret;
}


#define LINE_SIZE			48
#define MAX_LINE_LOAD		15


int Printer_Print_BmpFile(char *filepath)
{
	int ret = 0;
	int i = 0;
	unsigned char *buf;
	int totalLine = 0;
	int sendline = 0;
	int cacheline = 0;
	int fill = 1;

	spi_ddi_printer_stop();
	spi_ddi_printer_init(40);
	spi_ddi_printer_clear_fill_finish();
	ret = BmpToDotBuffer(filepath);

	if(ret != 0)
	{
		// log("BmpToDotBuffer failed!:%d", ret);
		return -1;
	}
	
	totalLine = GetLineCount();
	buf = GetDotBuffer();
	// totalLine = PirnterGetDotLines();
	// buf = GetBmpBuffer();
	sendline = MAX_LINE_LOAD;
	
	LOG("totalLine:%d",totalLine);
	LOGHEX("dotbuf:", buf, totalLine*48);

	while(totalLine > 0)
	{
		if(totalLine < MAX_LINE_LOAD)
		{
			sendline = totalLine; 
		}
		
		ret = spi_ddi_printer_fill(buf+i*(LINE_SIZE * MAX_LINE_LOAD*fill), sendline*LINE_SIZE);
		
		if(ret != 0)
		{
			return ret;
		}
		
		totalLine -= (MAX_LINE_LOAD*fill);
		i++;

		if(i == 1)
		{
			spi_ddi_printer_start(0);
		}
	}

	ret = spi_ddi_printer_set_fill_finish();
	return ret;
}


static char *jstrToCstr(JNIEnv* env, char *encoding ,jstring jstr) {
	char* rtn;

	jclass clsstring = env->FindClass( "java/lang/String");
	jstring strencode = env->NewStringUTF( encoding);
	// jstring strencode = env->NewString();
	jmethodID mid = env->GetMethodID(clsstring, "getBytes",
			"(Ljava/lang/String;)[B");
	jbyteArray barr = (jbyteArray)env->CallObjectMethod( jstr, mid,
			strencode);
	jsize alen = env->GetArrayLength( barr);
	jbyte* ba = env->GetByteArrayElements(barr, JNI_FALSE);

	if (alen > 0) {
		rtn = (char*) malloc(alen + 1);

		if(rtn != NULL)
		{
			memcpy(rtn, ba, alen);
			rtn[alen] = 0;
		}
	}

	env->ReleaseByteArrayElements(barr, ba, 0);
	return rtn;
}

static jint sub_ddi_printer_print_receipt(JNIEnv *env, jobject obj, jstring receiptfilename)
{
	char *receiptFile;
	int ret = 0;
	
	receiptFile = jstrToCstr(env, "utf-8",receiptfilename);

	if(receiptFile == NULL)
	{
		return -1;
	}

	ret =  Printer_Print_BmpFile(receiptFile);
	free(receiptFile);
	return ret;
}

static jint sub_ddi_printer_print_text(JNIEnv *env, jobject obj, jstring text)
{
	char *cstr;
	int ret = 0;
	
	cstr = jstrToCstr(env, "utf-8",text);

	if(cstr == NULL)
	{
		return -1;
	}

	ret =  FillBmp(cstr);
	free(cstr);
	return ret;
}


static jint sub_ddi_printer_set_gray(JNIEnv *env, jobject obj, jint gray)
{
	int ret = 0;
	
	ret =  spi_ddi_printer_set_gray(gray);

	return ret;
}

static void revsImage(unsigned char *p, unsigned char cnt)
{
  unsigned char i;
  unsigned short temp;
  
  for(i=0; i<cnt; i++)
  {
  	temp = (unsigned short) (p[i]<<8);   
	temp |= ((temp&0x8000)>>15);
    temp |= ((temp&0x4000)>>13);
	temp |= ((temp&0x2000)>>11);
    temp |= ((temp&0x1000)>>9); 
	temp |= ((temp&0x0800)>>7);
    temp |= ((temp&0x0400)>>5);
	temp |= ((temp&0x0200)>>3);
    temp |= ((temp&0x0100)>>1);  
    p[i] = (unsigned char) (temp);  
  }   
}

void SetY(int offset);

static jint sub_ddi_printer_test(JNIEnv *env, jobject obj)
{
	int ret = 0;
	int offset = 480*48;
	unsigned char wang[24*3]={0,   0,   0,
									0,   0,   0,
									0,   0,   12,
									252, 247, 31,
									0,   28,  0,
									0,   28,  0,
									0,   28,  0,
									0,   28,  0,
									0,   28,  0,
									0,   28,  0,
									0,   28,  0,
									248, 255, 15,
									0,   28,  0,
									0,   28,  0,
									0,   28,  0,
									0,   28,  0,
									0,   28,  0,
									0,   28,  0,
									0,   28,  0,
									0,   28,  56,
									254, 255, 127,
									0,   0,   0,
									0,   0,   0,
									0,   0,   0};

	unsigned char ming[24*3]={0,   0,   0,
									0,  24,   24,
									0,  240,  31,
									248,51,   24,
									24, 51,   24,
									24, 51,   24,
									24, 51,   24,
									24, 243,  31,
									248,51,   24,
									24, 51,   24,
									24, 51,   24,
									24, 51,   24,
									24, 51,   24,
									24, 251,  31,
									248,59,   24,
									24, 24,   24,
									8,  24,   24,
									0,  24,   24,
									0,  8,    24,
									0,  6,    24,
									0,  131,  31,
									192,1,    14,
									0,   0,   0,
									0,   0,   0};
	unsigned char *buf = (unsigned char *)GetBmpBuffer();
	int i,j,p,q,n;
	unsigned char *name;
	int error = 0;

	// spi_ddi_printer_stop();
	error = spi_ddi_printer_init(40);

	LOG("spi_ddi_printer_init:%d", error);

	spi_ddi_printer_clear_fill_finish();

	memset(buf, 0xFF, offset);
	
	// for(n = 0; n<280; n++)
	// {
	// 	error = spi_ddi_printer_fill(buf, 48);

	// 	if(error != 0)
	// 	{
	// 		LOG("spi_ddi_printer_fill:%d", error);
	// 	}
	// }
	
	// memset();

	// error = spi_ddi_printer_start(10);
			// LOG("spi_ddi_printer_start:%d", error);
	revsImage(wang, 24*3);
    revsImage(ming, 24*3);
#if 1
	for(n = 0;n<280;n++)
	{
		for(i = 0; i<24;i++)
		for(n = 0; n<48/3;n++)
		{
			memcpy(buf+offset, ming+i*3, 3);
			offset += 3;
			// memcpy(buf+offset, ming[i], 3);
			// offset += 3;
		}
	}


#else

	name = ming;
	int value = 280;
	int k;
	// LOG(" ");
	for(k=0; k<value; k++)
        {

        	for(i=0; i<24; i++)
        	{
			for(j=0; j<48/3; j++)
          		{
          		#if 0
          			if(j%2==0) name = wang;
           			else name = ming;
			#endif
						LOG("i:j:%d:%d", i, j);
            			*(buf+offset+3*j+0)  = *(name+i*3+0);
            			*(buf+offset+3*j+1)  = *(name+i*3+1);
            			*(buf+offset+3*j+2)  = *(name+i*3+2);	  
						offset += 3;
          		}
				  
				//  error =  spi_ddi_printer_fill(buf, 384);
				//  LOG("spi_ddi_printer_fill:%d", error);
		}
        }

		// spi_ddi_printer_set_fill_finish();
		// error = spi_ddi_printer_start(10);
		// LOG("spi_ddi_printer_start:%d", error);
		SetY(offset);
		Printer_Print_BmpFile("a.txt");

#endif

	return ret;
}


static jint sub_ddi_printer_open(JNIEnv *env, jobject obj)
{
	int ret = 0;

	printer_open(NULL);

	prtQueInit();

	return ret;
}

static jint sub_ddi_printer_feed(JNIEnv *env, jobject obj, jint lines)
{
	int ret = 0;

	ret = spi_ddi_printer_feed(lines);

	return ret;
}

static jint sub_ddi_printer_get_status(JNIEnv *env, jobject obj){
	int ret = 0;

	ret = spi_ddi_printer_get_status();

	return ret;
}

static jint sub_ddi_printer_print_textp(JNIEnv *env, jobject obj, jstring text)
{
	char *cstr;
	int ret = 0;
	char state = 0;
	int i;
	
	cstr = jstrToCstr(env, "GBK", text);

	LOG("cstr == %d",strlen(cstr));
	for(i=0;i<strlen(cstr);i++)
	{
		LOG("str[%d]: %02x\r\n", i, cstr[i]);
	}

	if(cstr == NULL)
	{
		return -1;
	}
	
	ret =  printer_printf(10, &state, "%s", cstr);
	free(cstr);
	return ret;
}

static jint sub_ddi_printer_print_bmp(JNIEnv *env, jobject obj, jint offset, jstring file)
{
	char *cstr;
	int ret = 0;
	
	cstr = jstrToCstr(env, "utf-8", file);

	if(cstr == NULL)
	{
		return -1;
	}

	ret =  printer_graphics (-1, offset, (const char *)cstr);
	free(cstr);

	return ret;
}

static jint sub_ddi_printer_print_matrix(JNIEnv *env, jobject obj, jint width, jint height ,
								jint offset,  jint blank, jbyteArray data)
{
	jbyte *nativedata;
	int ret = 0;
	print_matrix_attr_t attr;
	
	nativedata = (env)->GetByteArrayElements(data,JNI_FALSE);
	attr.height = height;
	attr.width = width;
	attr.offset = offset;
	ret =  printer_matrix (-1, &attr, (unsigned char *)nativedata, (env)->GetArrayLength(data));
	env->ReleaseByteArrayElements(data, nativedata, 0);
	return ret;
}


static jint sub_ddi_printer_close(JNIEnv *env, jobject obj, jint lines)
{
	int ret = 0;

	printer_close(-1);



	return ret;
}

static jint sub_ddi_printer_set_fill_finish (){
	int ret = 0;
	ret = spi_ddi_printer_set_fill_finish();
	return ret;
}

static jint sub_ddi_printer_start(JNIEnv *env, jobject obj, jint step){
	int ret = -1;
	int lines;
	unsigned char data[48];
	int cnt = 10;
	int i;
	//ret = spi_ddi_printer_start(lines);

	// 开始时，尽可能填充多数据
	lines = prtQueCheckLines();
	lines = lines > cnt ? cnt : lines;
	LOG("prtQueCheckLines, line:%d\r\n", lines);
	for(i = 0; i < lines; i++)
	{
		prtQueGetBuf((unsigned char *)data, sizeof(data));
		spi_ddi_printer_fill(data, sizeof(data));
	}

	// 如果buff本身足够存放，不需要边打印边填充，则设置填充完成标识
	if(prtQueCheckLines() == 0) 
	{
		spi_ddi_printer_set_fill_finish();
        spi_ddi_printer_start(step);
		LOG("1-spi_ddi_printer_start(%d), ret:%d", step, ret);
	}
   else
   {
		ret = spi_ddi_printer_start(step); // 先启动打印，之后再填充剩余数据
		LOG("2-spi_ddi_printer_start(%d), ret:%d", step, ret);
		
		// 如果需要边打印边填充，则启动打印一会(经验值)之后继续填充剩余数据
		//DelayMs(500);

		do {
			lines = spi_ddi_printer_check_queue();

			for(i = 0; i < lines && prtQueCheckLines() > 0; i++)
			{
				prtQueGetBuf((unsigned char *)data, sizeof(data));
				spi_ddi_printer_fill(data, sizeof(data));
			}
		} while( prtQueCheckLines() > 0 && spi_ddi_printer_get_status() == PRT_ERR_BUSY); // 此时打印机忙为正常状态，否则就已经不正常了

		spi_ddi_printer_set_fill_finish();
    }

	while( (ret = spi_ddi_printer_get_status()) == PRT_ERR_BUSY )
		;

	return ret;
}


static jint sub_ddi_fck_prnt_init (JNIEnv *env, jobject obj, jint step){
	int ret = 0;
	ret = fck_prnt_init(step);
	return ret;
}


static jint sub_ddi_fck_prnt_start (JNIEnv *env, jobject obj, jint step){
	int ret = 0;
	ret = fck_prnt_start(step);
	return ret;
}

static jint sub_ddi_fck_prnt_set_gray (JNIEnv *env, jobject obj, jint gray){
	int ret = 0;
	ret = fck_prnt_set_gray(gray);
	return ret;
}

static jint sub_ddi_fck_prnt_get_status(JNIEnv *env, jobject obj){
	int ret = 0;

	ret = fck_prnt_get_status();

	return ret;
}

static jint sub_ddi_fck_prnt_printf(JNIEnv *env, jobject obj, jstring text)
{
	char *cstr;
	int ret = 0;
	char state = 0;
	int i;
	
	cstr = jstrToCstr(env, "GBK", text);

	LOG("cstr == %d",strlen(cstr));
	for(i=0;i<strlen(cstr);i++)
	{
		LOG("str[%d]: %02x\r\n", i, cstr[i]);
	}

	if(cstr == NULL)
	{
		return -1;
	}
	
	ret =  fck_prnt_printf(cstr);
	free(cstr);
	return ret;
}

static jint sub_ddi_fck_prnt_command(JNIEnv *env, jobject obj,jbyteArray data,jint nlen)
{
	int ret,i;
	jbyte *data_j;
	unsigned char data_c[800];
	
	data_j = env->GetByteArrayElements(data, false);
	if(nlen<800)
	{
		for(i=0;i<nlen;i++)
		{
			data_c[i] = data_j[i];
		}
	}
	else
		return -9;
	ret = fck_prnt_command((unsigned char *)data_c,nlen);
	
	env->ReleaseByteArrayElements(data, data_j, 0);
	
	
	return ret;
}

static jint sub_ddi_fck_prnt_graphics(JNIEnv *env, jobject obj,jint offset,jstring text)
{
	int ret;
	char *cstr;
	
	cstr = jstrToCstr(env, "GBK", text);
	if(cstr == NULL)
	{
		return -1;
	}

	ret = fck_prnt_graphics(offset, (char *)cstr);
		
	free(cstr);
	
	return ret;
}

static jint sub_ddi_fck_prnt_matrix(JNIEnv *env, jobject obj,jint offset,jint w,jint h,jbyteArray data,jint nlen)
{
	int ret,i;
	jbyte *data_j;
	unsigned char data_c[48*500];
	
	data_j = env->GetByteArrayElements(data, false);
	if(nlen<sizeof(data_c))
	{
		for(i=0;i<nlen;i++)
		{
			data_c[i] = data_j[i];
		}
	}
	else
		return -9;
	
	ret = fck_prnt_matrix(offset, w, h, (unsigned char *)data_c, nlen);
	
	env->ReleaseByteArrayElements(data, data_j, 0);
	return ret;
}


static jint sub_ddi_fck_prnt_fill_data(JNIEnv *env, jobject obj,jbyteArray data,jint nlen)
{
	int ret,i;
	jbyte *data_j;
	unsigned char data_c[48*500];
	
	data_j = env->GetByteArrayElements(data, false);
	if(nlen<sizeof(data_c))
	{
		for(i=0;i<nlen;i++)
		{
			data_c[i] = data_j[i];
		}
	}
	else
		return -9;
	
	ret = fck_prnt_fill_data((unsigned char *)data_c, nlen);
	
	env->ReleaseByteArrayElements(data, data_j, 0);
	
	return ret;
}

static jint sub_verify_publickey(JNIEnv *env, jobject obj, jbyteArray jdata, jint jlen) {
    int ret;
    jbyte *jbys = env->GetByteArrayElements(jdata, NULL);
    unsigned char *buf = (unsigned char *) jbys;
    ret = spi_ddi_verify_signature(buf, (int)jlen);
    env->ReleaseByteArrayElements(jdata, jbys, 0);
    return ret;
}

static jint sub_get_pem_file(JNIEnv *env, jobject obj, jbyteArray jdata) {
    int ret, i, datalen;
	jbyte *jbys;
	unsigned char buf[PEM_FILE_LEN] = {0};
	datalen = env->GetArrayLength(jdata);
	if (datalen < PEM_FILE_LEN) {
		return -1;
	}

	for (i=0; i<4; i++) {
		spi_ddi_get_pem_file(buf);
	}
	
	jbys = env->GetByteArrayElements(jdata, false);
    ret = spi_ddi_get_pem_file(buf);
	for(i=0;i<PEM_FILE_LEN;i++)
	{
		jbys[i] = buf[i];
	}
    env->ReleaseByteArrayElements(jdata, jbys, 0);

    return ret;
}


static jint sub_security_unlock(JNIEnv *env, jobject obj, jbyteArray jdata) {
    int ret;
    jbyte *jbys = env->GetByteArrayElements(jdata, NULL);
    unsigned char *buf = (unsigned char *) jbys;
    ret = spi_ddi_security_unlock(buf);
    env->ReleaseByteArrayElements(jdata, jbys, 0);
    return ret;
}


static void sub_poweron_scanner(JNIEnv *env, jobject obj) {
   
}

static void sub_poweroff_scanner(JNIEnv *env, jobject obj) {
   
}

static jboolean sub_open_serialport(JNIEnv *env, jobject obj) {
	
    return true;
}

static jboolean sub_close_serialport(JNIEnv *env, jobject obj) {
	
    return true;
}

static jboolean sub_write_serialport(JNIEnv *env, jobject obj, jbyteArray jdata, jint jlen) {

	
    return true;
}

static jboolean sub_read_serialport(JNIEnv *env, jobject obj, jbyteArray jnumbytes, jbyteArray jreads, jint jouttimes) {

    return true;
}

static jint sub_sys_get_config (JNIEnv *env,jobject obj,jbyteArray data1,jintArray data2){
	
	
	jbyte *lParam_j;
	jint *wplen_j;
	lParam_j = env->GetByteArrayElements(data1, NULL);
	unsigned char *buf = (unsigned char *) lParam_j;
	wplen_j = env->GetIntArrayElements(data2, NULL);
	int ret = spi_ddi_sys_get_config(buf,wplen_j);
	env->ReleaseByteArrayElements(data1, lParam_j, 0);
	env->ReleaseIntArrayElements(data2, wplen_j, 0);
	
	
	
	return ret;
}


static JNINativeMethod method_table[] = {  
	{"test", "(II)I", (void *)test},  
	{"ddi_ddi_sys_init", "()V", (void *)sub_sys_init}, 
	{"ddi_ddi_sys_uninit", "()V", (void *)sub_sys_uninit}, 
	{"ddi_sys_read_dsn", "([B)I", (void *)sub_sys_read_dsn}, 
	{"ddi_sys_get_firmwarever", "([BI)I", (void *)sub_sys_get_firmwarever}, 
	{"ddi_sys_bat_status", "()I", (void *)sub_sys_bat_status}, 
	{"ddi_sys_mainBat_status", "()I", (void *)sub_sys_mainBat_status}, 
	{"ddi_sys_poweroff", "()I", (void *)sub_sys_poweroff}, 
	{"ddi_sys_set_timeout", "(I)I", (void *)sub_ddi_sys_set_timeout}, 
	{"ddi_sys_get_timeout", "([I)I", (void *)sub_ddi_sys_get_timeout}, 
	{"ddi_sys_get_chipID", "([B)I", (void *)sub_ddi_sys_get_chipID}, 

	{"ddi_security_rand", "([B)I", (void *)sub_ddi_security_rand}, 
	{"ddi_get_debugStatus", "()I", (void *)sub_ddi_get_debugStatus}, 
	{"ddi_set_debugStatus", "(B[B)I", (void *)sub_ddi_set_debugStatus}, 
	{"ddi_delete_cert_byPassword", "([B)I", (void *)sub_ddi_delete_cert_byPassword}, 
	{"ddi_sys_set_beep", "(I)I", (void *)sub_ddi_sys_set_beep}, 
	{"ddi_sys_beep_ctrl", "(III)I", (void *)sub_ddi_sys_beep_ctrl},
	{"ddi_pk_verify_hash", "(I[B)I", (void *)sub_spi_ddi_pk_verify_hash}, 

	
	{"ddi_mag_open", "()I", (void *)sub_mag_open},  
	{"ddi_mag_close", "()I", (void *)sub_mag_close}, 
	{"ddi_mag_clear", "()I", (void *)sub_mag_clear}, 
	{"ddi_mag_read", "([B[B[B)I", (void *)sub_mag_read}, 
	{"ddi_mag_ioctl", "(III)I", (void *)sub_mag_ioctl}, 
	{"ddi_mag_ioctl_for_java", "(II[B[I[B)I", (void *)sub_mag_ioctl_for_java}, 

	{"ddi_com_open_sub", "(IIIII)I", (void *)sub_com_open},  
	{"ddi_com_close", "(I)I", (void *)sub_com_close}, 
	{"ddi_com_clear", "(I)I", (void *)sub_com_clear}, 
	{"ddi_com_read", "(I[BI)I", (void *)sub_com_read}, 
	{"ddi_com_write", "(I[BI)I", (void *)sub_com_write}, 
	{"ddi_com_ioctl", "(IIII)I", (void *)sub_com_ioctl}, 

	{"ddi_k21_com_clear", "(I)I", (void *)sub_k21_com_clear}, 
	{"ddi_k21_com_read", "(I[BI)I", (void *)sub_k21_com_read}, 
	{"ddi_k21_com_write", "(I[BI)I", (void *)sub_k21_com_write}, 

	
	
	{"ddi_iccpsam_open", "(I)I", (void *)sub_iccpsam_open},  
	{"ddi_iccpsam_close", "(I)I", (void *)sub_iccpsam_close}, 
	{"ddi_iccpsam_poweron", "(I[B)I", (void *)sub_iccpsam_poweron}, 
	{"ddi_iccpsam_poweroff", "(I)I", (void *)sub_iccpsam_poweroff}, 
	{"ddi_iccpsam_get_status", "(I)I", (void *)sub_iccpsam_get_status}, 
	{"ddi_iccpsam_exchange_apdu", "(I[BI[B[II)I", (void *)sub_iccpsam_exchange_apdu}, 
	{"ddi_iccpsam_ioctl", "(III)I", (void *)sub_iccpsam_ioctl}, 
	{"ddi_iccpsam_ioctl_for_java", "(II[B[I[B)I", (void *)sub_iccpsam_ioctl_for_java}, 
	
	{"ddi_rf_open", "()I", (void *)sub_rf_open},  
	{"ddi_rf_close", "()I", (void *)sub_rf_close}, 
	{"ddi_rf_poweron", "(I)I", (void *)sub_rf_poweron}, 
	{"ddi_rf_poweroff", "()I", (void *)sub_rf_poweroff}, 
	{"ddi_rf_get_status", "()I", (void *)sub_rf_get_status}, 
	{"ddi_rf_activate", "()I", (void *)sub_rf_activate}, 
	{"ddi_rf_exchange_apdu", "([BI[B[II)I", (void *)sub_rf_exchange_apdu}, 
	{"ddi_rf_remove", "()I", (void *)sub_rf_remove}, 
	{"ddi_rf_ioctl", "(III)I", (void *)sub_rf_ioctl}, 
	{"ddi_rf_ioctl_for_java", "(II[B[I[B)I", (void *)sub_rf_ioctl_for_java}, 
	
	
	{"ddi_thmprn_open", "()I", (void *)sub_thmprn_open},  
	{"ddi_thmprn_close", "()I", (void *)sub_thmprn_close}, 
	{"ddi_thmprn_feed_paper", "(I)I", (void *)sub_thmprn_feed_paper}, 
	{"ddi_thmprn_print_image", "(III[B)I", (void *)sub_thmprn_print_image}, 
	{"ddi_thmprn_print_image_file", "(III[B)I", (void *)sub_thmprn_print_image_file}, 
	{"ddi_thmprn_print_text_sub", "(IIIIIII[B)I", (void *)sub_thmprn_print_text}, 
	{"ddi_thmprn_print_comb_text_sub", "(I[I[I[I[I[I[I[I[Ljava/lang/Object;)I", (void *)sub_thmprn_print_comb_text}, 
	{"ddi_thmprn_get_status", "()I", (void *)sub_thmprn_get_status}, 
	{"ddi_thmprn_ioctl", "(III)I", (void *)sub_thmprn_ioctl}, 
	{"ddi_thmprn_ioctl_for_java", "(II[B[I[B)I", (void *)sub_thmprn_ioctl_for_java}, 
	{"ddi_thmprn_test", "()I", (void *)sub_thmprn_test},  
	{"ddi_thmprn_print_oneBitBMPImage", "([BI)I", (void *)sub_printOneBitBMPImage},  
	{"ddi_thmprn_print_oneBitBMPImageByBuffer", "([BI)I", (void *)sub_printOneBitBMPImageByBuffer},  
	{"ddi_thmprn_totalDot", "([BI)I", (void *)sub_spi_ddi_thmprn_totalDot},  
	{"ddi_thmprn_print_blackBlock", "(I)I", (void *)sub_printBlackBlock},  
	
	{"ddi_innerkey_open", "()I", (void *)sub_innerkey_open},  
	{"ddi_innerkey_close", "()I", (void *)sub_innerkey_close}, 
	{"ddi_innerkey_inject", "(II[B)I", (void *)sub_innerkey_inject}, 
	{"ddi_innerkey_encrypt", "(III[B[B)I", (void *)sub_innerkey_encrypt}, 
	{"ddi_innerkey_decrypt", "(III[B[B)I", (void *)sub_innerkey_decrypt}, 
	{"ddi_innerkey_ioctl", "(III)I", (void *)sub_innerkey_ioctl}, 
	{"ddi_innerkey_ioctl_for_java", "(II[B[I[B)I", (void *)sub_innerkey_ioctl_for_java}, 

	{"ddi_led_open", "()I", (void *)sub_led_open},  
	{"ddi_led_close", "()I", (void *)sub_led_close}, 
	{"ddi_led_sta_set", "(II)I", (void *)sub_led_sta_set}, 
	{"ddi_led_ioctl_for_java", "(II[B[I[B)I", (void *)sub_led_ioctl_for_java}, 

	{"ddi_dukpt_open", "()I", (void *)sub_dukpt_open},  
	{"ddi_dukpt_close", "()I", (void *)sub_dukpt_close}, 
	{"ddi_dukpt_inject_sub", "(BB[BBB[BB)I", (void *)sub_dukpt_inject},  
	{"ddi_dukpt_encrypt", "(III[B[BI)I", (void *)sub_dukpt_encrypt}, 
	{"ddi_dukpt_decrypt", "(III[B[BI)I", (void *)sub_dukpt_decrypt}, 
	{"ddi_dukpt_getksn", "(II[I[B)I", (void *)sub_dukpt_getksn}, 
	{"ddi_dukpt_ioctl", "(III)I", (void *)sub_dukpt_ioctl}, 

	{"setGpioValue", "(II)I", (void *)sub_setGpioValue},  
	{"getGpioValue", "(I)I", (void *)sub_getGpioValue}, 
	{"setScannerEnable", "(I)I", (void *)sub_setScannerEnable}, 
	{"getScannerEnableStatus", "()I", (void *)sub_getScannerEnableStatus}, 
	{"setScannerPowerOn", "(I)I", (void *)sub_setScannerPowerOn}, 
	{"getScannerPowerOnStatus", "()I", (void *)sub_getScannerPowerOnStatus}, 
	{"setScannerUsbSwitch", "(I)I", (void *)sub_setScannerUsbSwitch}, 
	{"getScannerUsbSwitchStatus", "()I", (void *)sub_getScannerUsbSwitchStatus}, 

	
	{"spi_ddi_certmodule_open", "()I", (void *)sub_ddi_certmodule_open},  
	{"spi_ddi_certmodule_close", "()I", (void *)sub_ddi_certmodule_close}, 
	{"spi_ddi_certmodule_save_sub", "([B[B[BBB[B[BI)I", (void *)sub_ddi_certmodule_save}, 
	{"spi_ddi_certmodule_readByName_sub", "([B[B[B[B[B[B[B[I)I", (void *)sub_ddi_certmodule_readByName}, 
	{"spi_ddi_certmodule_querycount", "([I)I", (void *)sub_ddi_certmodule_querycount}, 
	{"spi_ddi_certmodule_delete_sub", "(I[B[B[BBB[B[B)I", (void *)sub_ddi_certmodule_delete}, 
	{"spi_ddi_certmodule_deleteall", "()I", (void *)sub_ddi_certmodule_deleteall}, 

	{"ddi_key_open", "()I", (void *)sub_key_open},  
	{"ddi_key_close", "()I", (void *)sub_key_close}, 
	{"ddi_key_clear", "()I", (void *)sub_key_clear}, 
	{"ddi_key_read", "([I)I", (void *)sub_key_read}, 
	{"ddi_key_ioctl", "(III)I", (void *)sub_key_ioctl}, 


	{"ddi_pin_input", "(IIII)I", (void *)sub_pin_input}, 
	{"ddi_pin_input_press", "([B)I", (void *)sub_pin_input_press},  
	{"ddi_pin_input_cancel", "()I", (void *)sub_pin_input_cancel}, 
	{"ddi_pin_getonlinepinblock", "(III[B[B)I", (void *)sub_pin_getonlinepinblock}, 


	{"ddi_sys_getCertHash", "([B)I", (void *)sub_ddi_sys_getCertHash},  
	{"ddi_sys_setCertHash", "([B)I", (void *)sub_ddi_sys_setCertHash},  

	{"ddi_BodyNumber_process_download", "()I", (void *)sub_ddi_BodyNumber_process_download},  
	{"ddi_BodyNumber_process_serial_read", "()I", (void *)sub_ddi_BodyNumber_process_serial_read},  
	{"ddi_open_port", "()I", (void *)sub_ddi_open_port},  
	{"ddi_close_port", "()I", (void *)sub_ddi_close_port},  

	{"ddi_security_getstatus", "([B)I", (void *)sub_ddi_security_getstatus}, 

	{"ddi_spi_communication_test", "([BI)I", (void *)sub_ddi_spi_communication_test}, 

	{"ddi_sys_get_Hardware_Ver", "()I", (void *)sub_ddi_sys_get_Hardware_Ver}, 

	{"Ddi_Printer_set_gray", "(I)I", (void *)sub_ddi_printer_set_gray}, 
	{"Ddi_Print_Receipt", "(Ljava/lang/String;)I", (void *)sub_ddi_printer_print_receipt}, 
	{"Ddi_Printer_ft_fill_text", "(Ljava/lang/String;)I", (void *)sub_ddi_printer_print_text}, 
	{"Ddi_Printer_Test", "()V", (void *)sub_ddi_printer_test}, 
	{"ddi_spi_ddi_sys_set_dsn", "(I[B)I", (void *)sub_spi_ddi_sys_set_dsn}, 

//new api for printer
	{"ddi_printer_open", "()I", (void *)sub_ddi_printer_open}, 
	{"ddi_printer_feed", "(I)I", (void *)sub_ddi_printer_feed}, 
	{"ddi_printer_get_status", "()I", (void *)sub_ddi_printer_get_status},
	{"ddi_printer_print", "(Ljava/lang/String;)I", (void *)sub_ddi_printer_print_textp}, 
	{"ddi_printer_printBmp", "(ILjava/lang/String;)I", (void *)sub_ddi_printer_print_bmp}, 
	{"ddi_printer_start", "(I)I", (void *)sub_ddi_printer_start},
	{"ddi_printer_printMatrix", "(III[B)I", (void *)sub_ddi_printer_print_matrix}, 
	{"ddi_printer_close", "()I", (void *)sub_ddi_printer_close},
	{"ddi_printer_set_fill_finish","()I",(void *)sub_ddi_printer_set_fill_finish},
	//spi_ddi_printer_set_fill_finish
	//spi_ddi_printer_get_status

	//add new print
	{"ddi_prnt_init","(I)I",(void *)sub_ddi_fck_prnt_init},
	{"ddi_prnt_start","(I)I",(void *)sub_ddi_fck_prnt_start},
	{"ddi_prnt_get_status","()I",(void *)sub_ddi_fck_prnt_get_status},
	{"ddi_prnt_printf","(Ljava/lang/String;)I",(void *)sub_ddi_fck_prnt_printf},
	{"ddi_prnt_command","([BI)I",(void *)sub_ddi_fck_prnt_command},
	{"ddi_prnt_graphics","(I[B)I", (void *)sub_ddi_fck_prnt_graphics},
	{"ddi_prnt_matrix","(III[BI)I", (void *)sub_ddi_fck_prnt_matrix},
	{"ddi_prnt_set_gray","(I)I",(void *)sub_ddi_fck_prnt_set_gray},
	
	{"ddi_verify_publickey", "([BI)I", (void *)sub_verify_publickey},
	{"ddi_get_pem_file", "([B)I", (void *)sub_get_pem_file},
	{"ddi_security_unlock", "([B)I", (void *)sub_security_unlock},

	{"powerOnScanner", "()V", (void *)sub_poweron_scanner},
	{"powerOffScanner", "()V", (void *)sub_poweroff_scanner},
	{"openSerialPort", "()Z", (void *)sub_open_serialport},
	{"closeSerialPort", "()Z", (void *)sub_close_serialport},
	{"writeSerialPort", "([BI)Z", (void *)sub_write_serialport},
	{"readSerialPort", "([B[BI)Z", (void *)sub_read_serialport},
	{"ddi_sys_get_config", "([B[I)I", (void *)sub_sys_get_config},
	
};  


#define NELEM(a) sizeof((a)) / sizeof(JNINativeMethod)
/*
static int register_xinguodu_ddi(JNIEnv *env)  
{  
	// return android::AndroidRuntime::registerNativeMethods(env, "com/xinguodu/ddiinterface/Ddi", gMethods, NELEM(gMethods));  
	jclass ddiclaz;
	int ret = 0;

	ddiclaz = env->FindClass("com/jl/Ddi");
	
	ret =  env->RegisterNatives(ddiclaz, gMethods, NELEM(gMethods));  

	env->DeleteLocalRef(ddiclaz);

	return ret;
} 

jint JNI_OnLoad(JavaVM *vm, void *reserved)  
{  
	JNIEnv *env = NULL;  

	if (vm->GetEnv((void **)&env, JNI_VERSION_1_6) != JNI_OK) {
		printf("Error GetEnv\n");  
		return -1;  
	}  

	assert(env != NULL);  
	if (register_xinguodu_ddi(env) < 0) {  
		printf("register_JL_ddi error.\n");  
		return -1;  
	}  
	return JNI_VERSION_1_6;  
}
*/
int register_android_server_ddi_Ddi(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "com/android/server/ddi/Ddi",
            method_table, NELEM(method_table));
}


};