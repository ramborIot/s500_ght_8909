package com.android.server.manage;

import android.util.Log;

public class LogManage {
	private static boolean mLogFlag=true;
	public static void e(String tag,String msg){
		if(mLogFlag)
		    Log.e(tag, msg);
	}
	public static void d(String tag,String msg){
		if(mLogFlag)
		    Log.d(tag, msg);
	}
	public static void i(String tag,String msg){
		if(mLogFlag)
			Log.i(tag, msg);
	}
	public static void w(String tag,String msg){
		if(mLogFlag)
			Log.w(tag, msg);
	}

}
