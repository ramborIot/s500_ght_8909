package com.android.server.manage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import android.content.Context;

public class CertManager {	
	private static String TAG="CertManager";
	public static boolean checkSigtures(X509Certificate x509,X509Certificate checkx509){		
    	boolean flag=false;
    	if(x509!=null&&checkx509!=null){
    		LogManage.w(TAG, "dst-->"+x509.getSubjectDN().getName()+"\n"+x509.getIssuerDN().getName());
    		LogManage.w(TAG, "app-->"+checkx509.getSubjectDN().getName()+"\n"+checkx509.getIssuerDN().getName());
			try {
				checkx509.verify(x509.getPublicKey());
				flag=true;
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchProviderException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SignatureException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			/*
			if(checkx509.getIssuerDN().getName().equals(checkx509.getSubjectDN().getName())){
    			try {
    				checkx509.verify(x509.getPublicKey());
    				flag=true;
    			} catch (InvalidKeyException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (CertificateException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (NoSuchAlgorithmException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (NoSuchProviderException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (SignatureException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    			catch (Exception e) {
					e.printStackTrace();
				}
    		}
    		else{
    			if(x509.getSubjectDN().getName().equals(checkx509.getIssuerDN().getName())){
    				try {
    					checkx509.verify(x509.getPublicKey());
    					flag=true;
    				} catch (InvalidKeyException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				} catch (CertificateException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				} catch (NoSuchAlgorithmException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				} catch (NoSuchProviderException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				} catch (SignatureException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
    				catch (Exception e) {
    					e.printStackTrace();
    				}
    				
    			}
    		}
    		*/
    	}	
    	return flag;
    }
	public static X509Certificate parseSignature(String fileName){
    	File file=new File(fileName);
    	X509Certificate certificate=null;
    	if(file.exists()){
    		try {
        		CertificateFactory cert=CertificateFactory.getInstance("X.509");
        		certificate = (X509Certificate) cert.generateCertificate(new FileInputStream(file));
        		LogManage.i(TAG,"cert:"+certificate.getIssuerDN().getName()+":"+certificate.getSubjectDN().getName());
        	} catch (CertificateException e) {
        		// TODO Auto-generated catch block
        		e.printStackTrace();
        	}
        	catch (Exception e) {
    			e.printStackTrace();
    		}
    	}    	
    	return certificate;
    }
	public static X509Certificate parseSignature(byte[] signature){
    	X509Certificate certificate=null;
    	try {
			LogManage.e(TAG,"解析证书开始");
    		CertificateFactory cert=CertificateFactory.getInstance("X.509");
    		certificate=(X509Certificate) cert.generateCertificate(new ByteArrayInputStream(signature));
    		LogManage.e(TAG,"certSign:"+certificate.getIssuerDN().getName()+":"+certificate.getSubjectDN().getName());
    	} catch (CertificateException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return certificate;
    }
}
