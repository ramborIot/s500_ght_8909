package com.android.server.ddi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import com.android.server.ddi.ScannController;
import com.android.server.ddi.UART;

import android.R.integer;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Typeface;
import android.os.Build;
import android.hardware.ddi.IDdiManager;
import android.content.Context;
import android.util.Slog;
import android.util.Log;


//import xinguodu.ddiinterface.class;
//import com.xinguodu.ddiinterface;
//#define COMB_PRN_TEXT_MAX_LEN 256
import android.os.Environment;
import android.util.Log;

class strPrnTextCtrl {
	private int m_align;
	private int m_offset;
	private int m_font;
	private int m_ascsize;
	private int m_asczoom;
	private int m_nativesize;
	private int m_nativezoom;

	public int getAlign() {
		return m_align;
	}

	public int getOffset() {
		return m_offset;
	}

	public int getFont() {
		return m_font;
	}

	public int getAscsize() {
		return m_ascsize;
	}

	public int getAsczoom() {
		return m_asczoom;
	}

	public int getNativesize() {
		return m_nativesize;
	}

	public int getNativezoom() {
		return m_nativezoom;
	}

	public int setAlign(int align) {
		m_align = align;
		return 0;
	}

	public int setOffset(int offset) {
		m_offset = offset;
		return 0;
	}

	public int setFont(int font) {
		m_font = font;
		return 0;
	}

	public int setAscsize(int ascsize) {
		m_ascsize = ascsize;
		return 0;
	}

	public int setAsczoom(int asczoom) {
		m_asczoom = asczoom;
		return 0;
	}

	public int setNativesize(int nativesize) {
		m_nativesize = nativesize;
		return 0;
	}

	public int setNativezoom(int nativezoom) {
		m_nativezoom = nativezoom;
		return 0;
	}
}

class strPrnCombTextCtrl {
	private int i;
	private int m_x0;
	private int m_y0;
	private int m_font;
	private int m_ascsize;
	private int m_asczoom;
	private int m_nativesize;
	private int m_nativezoom;
	private byte[] m_text = new byte[256];

	public int getX0() {
		return m_x0;
	}

	public int getY0() {
		return m_y0;
	}

	public int getFont() {
		return m_font;
	}

	public int getAscsize() {
		return m_ascsize;
	}

	public int getAsczoom() {
		return m_asczoom;
	}

	public int getNativesize() {
		return m_nativesize;
	}

	public int getNativezoom() {
		return m_nativezoom;
	}

	public byte[] getText() {
		return m_text;
	}

	public int setX0(int x0) {
		m_x0 = x0;
		return 0;
	}

	public int setY0(int y0) {
		m_y0 = y0;
		return 0;
	}

	public int setFont(int font) {
		m_font = font;
		return 0;
	}

	public int setAscsize(int ascsize) {
		m_ascsize = ascsize;
		return 0;
	}

	public int setAsczoom(int asczoom) {
		m_asczoom = asczoom;
		return 0;
	}

	public int setNativesize(int nativesize) {
		m_nativesize = nativesize;
		return 0;
	}

	public int setNativezoom(int nativezoom) {
		m_nativezoom = nativezoom;
		return 0;
	}

	public int setText(byte[] text) {
		for (i = 0; i < 256; i++) {
			m_text[i] = text[i];
		}
		return 0;
	}
}

class strDukptInitInfo {
	private int i;
	private byte m_groupindex;
	private byte m_keyindex;
	private byte[] m_initkey = new byte[24];
	private byte m_keylen;
	private byte m_ksnindex;
	private byte[] m_initksn = new byte[20];
	private byte m_ksnlen;

	public byte getGroupindex() {
		return m_groupindex;
	}

	public byte getKeyindex() {
		return m_keyindex;
	}

	public byte getKeylen() {
		return m_keylen;
	}

	public byte getKsnindex() {
		return m_ksnindex;
	}

	public byte getKsnlen() {
		return m_ksnlen;
	}

	public byte[] getInitkey() {
		return m_initkey;
	}

	public byte[] getInitksn() {
		return m_initksn;
	}

	public int setGroupindex(byte groupindex) {
		m_groupindex = groupindex;
		return 0;
	}

	public int setKeyindex(byte keyindex) {
		m_keyindex = keyindex;
		return 0;
	}

	public int setKeylen(byte keylen) {
		m_keylen = keylen;
		return 0;
	}

	public int setKsnindex(byte ksnindex) {
		m_ksnindex = ksnindex;
		return 0;
	}

	public int setKsnlen(byte ksnlen) {
		m_ksnlen = ksnlen;
		return 0;
	}

	public int setInitkey(byte[] initkey) {
		for (i = 0; i < 24; i++) {
			m_initkey[i] = initkey[i];
		}
		return 0;
	}

	public int setInitksn(byte[] initksn) {
		for (i = 0; i < 20; i++) {
			m_initksn[i] = initksn[i];
		}
		return 0;
	}
}

class HSM_ObjectProperty {

	private int i;
	private byte[] m_CertName = new byte[10];
	private byte[] m_StrID = new byte[32];
	private byte[] m_StrLabel = new byte[32];
	byte m_nObjectType;
	byte m_nDataType;
	private byte[] m_nDataLength = new byte[4];

	public byte[] getCertName() {
		return m_CertName;
	}

	public byte[] getStrID() {
		return m_StrID;
	}

	public byte[] getStrLabel() {
		return m_StrLabel;
	}

	public byte getObjectType() {
		return m_nObjectType;
	}

	public byte getDataType() {
		return m_nDataType;
	}

	public byte[] getDataLength() {
		return m_nDataLength;
	}

	public int setCertName(byte[] CertName) {
		System.err.println("CertName.length: " + CertName.length + "\n");
		for (i = 0; i < 10 && i < CertName.length; i++) {
			m_CertName[i] = CertName[i];
		}
		return 0;
	}

	public int setStrID(byte[] StrID) {
		for (i = 0; i < 32 && i < StrID.length; i++) {
			m_StrID[i] = StrID[i];
		}
		return 0;
	}

	public int setStrLabel(byte[] StrLabel) {
		for (i = 0; i < 32 && i < StrLabel.length; i++) {
			m_StrLabel[i] = StrLabel[i];
		}
		return 0;
	}

	public int setObjectType(byte ObjectType) {
		m_nObjectType = ObjectType;
		return 0;
	}

	public int setDataType(byte DataType) {
		m_nDataType = DataType;
		return 0;
	}

	public int setDataLength(byte[] DataLength) {
		for (i = 0; i < 4 && i < DataLength.length; i++) {
			m_nDataLength[i] = DataLength[i];
		}
		return 0;
	}

}

class strMfAuth {

	private int i;
	private byte m_authmode;
	private byte[] m_key = new byte[6];
	private byte[] m_uid = new byte[10];
	private byte m_block;

	public byte getAuthmode() {
		return m_authmode;
	}

	public byte[] getKey() {
		return m_key;
	}

	public byte[] getUid() {
		return m_uid;
	}

	public byte getBlock() {
		return m_block;
	}

	public int setAuthmode(byte Authmode) {
		m_authmode = Authmode;
		return 0;
	}

	public int setKey(byte[] Key) {
		for (i = 0; i < 6 && i < Key.length; i++) {
			m_key[i] = Key[i];
		}
		return 0;
	}

	public int setUid(byte[] Uid) {
		for (i = 0; i < 10 && i < Uid.length; i++) {
			m_uid[i] = Uid[i];
		}
		return 0;
	}

	public int setBlock(byte Block) {
		m_block = Block;
		return 0;
	}

}

class CL_PARAM {

	private int i;
	private byte ModGsP;
	private byte RFCfg_A;
	private byte RFCfg_B;
	private byte RFOLevel;
	private byte RxTreshold_A;
	private byte RxTreshold_B;
	private byte[] RFU = new byte[25];
	private byte crc;

	public byte getModGsP() {
		return ModGsP;
	}

	public byte getRFCfg_A() {
		return RFCfg_A;
	}

	public byte getRFCfg_B() {
		return RFCfg_B;
	}

	public byte getRFOLevel() {
		return RFOLevel;
	}

	public byte getRxTreshold_A() {
		return RxTreshold_A;
	}

	public byte getRxTreshold_B() {
		return RxTreshold_B;
	}

	public byte[] getRFU() {
		return RFU;
	}

	public byte getcrc() {
		return crc;
	}

	public int setModGsP(byte m) {
		ModGsP = m;
		return 0;
	}

	public int setRFCfg_A(byte a) {
		RFCfg_A = a;
		return 0;
	}

	public int setRFCfg_B(byte b) {
		RFCfg_B = b;
		return 0;
	}

	public int setRFOLevel(byte l) {
		RFOLevel = l;
		return 0;
	}

	public int setRxTreshold_A(byte a) {
		RxTreshold_A = a;
		return 0;
	}

	public int setRxTreshold_B(byte b) {
		RxTreshold_B = b;
		return 0;
	}

	public int setRFU(byte[] r) {
		for (i = 0; i < 10 && i < r.length; i++) {
			RFU[i] = r[i];
		}
		return 0;
	}

	public int setcrc(byte c) {
		crc = c;
		return 0;
	}
}

class strSle4428 {

	private int i;
	private int m_protect;
	private int m_addr;
	private int m_le;

	public int getProtect() {
		return m_protect;
	}

	public int getAddr() {
		return m_addr;
	}

	public int getLe() {
		return m_le;
	}

	public int setProtect(int Protect) {
		m_protect = Protect;
		return 0;
	}

	public int setAddr(int Addr) {
		m_addr = Addr;
		return 0;
	}

	public int setLe(int Le) {
		m_le = Le;
		return 0;
	}
}

class strSle4442 {

	private int i;
	private int m_area;
	private int m_addr;
	private int m_le;

	public int getArea() {
		return m_area;
	}

	public int getAddr() {
		return m_addr;
	}

	public int getLe() {
		return m_le;
	}

	public int setArea(int Area) {
		m_area = Area;
		return 0;
	}

	public int setAddr(int Addr) {
		m_addr = Addr;
		return 0;
	}

	public int setLe(int Le) {
		m_le = Le;
		return 0;
	}
}

class strAT24CXX {

	private int i;
	private int m_type;
	private int m_addr;
	private int m_le;

	public int getType() {
		return m_type;
	}

	public int getAddr() {
		return m_addr;
	}

	public int getLe() {
		return m_le;
	}

	public int setType(int Type) {
		m_type = Type;
		return 0;
	}

	public int setAddr(int Addr) {
		m_addr = Addr;
		return 0;
	}

	public int setLe(int Le) {
		m_le = Le;
		return 0;
	}
}

class strAT88CXX {

	private int i;
	private int m_type;
	private int m_addr;
	private int m_le;

	public int getType() {
		return m_type;
	}

	public int getAddr() {
		return m_addr;
	}

	public int getLe() {
		return m_le;
	}

	public int setType(int Type) {
		m_type = Type;
		return 0;
	}

	public int setAddr(int Addr) {
		m_addr = Addr;
		return 0;
	}

	public int setLe(int Le) {
		m_le = Le;
		return 0;
	}
}

class strAT88CXXReadEc {

	private int i;
	private int m_type;
	private int m_mode;
	private int m_index;

	public int getType() {
		return m_type;
	}

	public int getMode() {
		return m_mode;
	}

	public int getIndex() {
		return m_index;
	}

	public int setType(int Type) {
		m_type = Type;
		return 0;
	}

	public int setMode(int Mode) {
		m_mode = Mode;
		return 0;
	}

	public int setIndex(int Index) {
		m_index = Index;
		return 0;
	}
}

class strAT88CXXVerify {

	private int i;
	private int m_type;
	private int m_mode;
	private byte[] m_key = new byte[3];
	private byte m_index;

	public int getType() {
		return m_type;
	}

	public int getMode() {
		return m_mode;
	}

	public byte[] getKey() {
		return m_key;
	}

	public byte getIndex() {
		return m_index;
	}

	public int setType(int Type) {
		m_type = Type;
		return 0;
	}

	public int setMode(int Mode) {
		m_mode = Mode;
		return 0;
	}

	public int setKey(byte[] Key) {
		for (i = 0; i < 3 && i < Key.length; i++) {
			m_key[i] = Key[i];
		}
		return 0;
	}

	public byte setIndex(byte Index) {
		m_index = Index;
		return 0;
	}
}

class strIs23sc1604 {

	private int i;
	private int m_addr;
	private int m_le;

	public int getAddr() {
		return m_addr;
	}

	public int getLe() {
		return m_le;
	}

	public int setAddr(int Addr) {
		m_addr = Addr;
		return 0;
	}

	public int setLe(int Le) {
		m_le = Le;
		return 0;
	}
}

class strIs23sc1604ReadRc {

	private int i;
	private int m_zone;
	private int m_mode;

	public int getZone() {
		return m_zone;
	}

	public int getMode() {
		return m_mode;
	}

	public int setZone(int Zone) {
		m_zone = Zone;
		return 0;
	}

	public int setMode(int Mode) {
		m_mode = Mode;
		return 0;
	}
}

class strIs23sc1604Verify {

	private int i;
	private int m_zone;
	private int m_mode;
	private byte[] m_key = new byte[2];

	public int getZone() {
		return m_zone;
	}

	public int getMode() {
		return m_mode;
	}

	public byte[] getKey() {
		return m_key;
	}

	public int setZone(int Zone) {
		m_zone = Zone;
		return 0;
	}

	public int setMode(int Mode) {
		m_mode = Mode;
		return 0;
	}

	public int setKey(byte[] Key) {
		for (i = 0; i < 2 && i < Key.length; i++) {
			m_key[i] = Key[i];
		}
		return 0;
	}
}

class strTkey {
	private int i;
	private byte[] m_indata = new byte[512];
	private byte[] m_outdata = new byte[512];
	private int m_len;

	public byte[] getIndata() {
		return m_indata;
	}

	public byte[] getOutdata() {
		return m_outdata;
	}

	public int getLen() {
		return m_len;
	}

	public void setIndata(byte[] m_indata) {
		this.m_indata = m_indata;
	}

	public void setOutdata(byte[] m_outdata) {
		this.m_outdata = m_outdata;
	}

	public int setLen(int len) {
		m_len = len;
		return 0;
	}
}

class strHkey {
	private int i;
	private int m_area;
	private int m_index;
	private int m_half;
	private byte[] m_indata = new byte[512];
	private byte[] m_outdata = new byte[512];
	private int m_len;

	public int getArea() {
		return m_area;
	}

	public int getIndex() {
		return m_index;
	}

	public int getHalf() {
		return m_half;
	}

	public byte[] getIndata() {
		return m_indata;
	}

	public byte[] getOutdata() {
		return m_outdata;
	}

	public int getLen() {
		return m_len;
	}

	public int setArea(int Area) {
		m_area = Area;
		return 0;
	}

	public int setIndex(int Index) {
		m_index = Index;
		return 0;
	}

	public int setHalf(int Half) {
		m_half = Half;
		return 0;
	}

	public void setIndata(byte[] m_indata) {
		this.m_indata = m_indata;
	}

	public void setOutdata(byte[] m_outdata) {
		this.m_outdata = m_outdata;
	}

	public int setLen(int len) {
		m_len = len;
		return 0;
	}
}

class strKeyTAKpara {
	private int m_area;
	private int m_target_index;
	private int m_source_index;
	private int m_save_index;

	public int getArea() {
		return m_area;
	}

	public int getTargetIndex() {
		return m_target_index;
	}

	public int getSourceIndex() {
		return m_source_index;
	}

	public int getSavetIndex() {
		return m_save_index;
	}

	public int setArea(int Area) {
		m_area = Area;
		return 0;
	}

	public byte setTargetIndex(int Index) {
		m_target_index = Index;
		return 0;
	}

	public byte setSourceIndex(int Index) {
		m_source_index = Index;
		return 0;
	}

	public byte setSaveIndex(int Index) {
		m_save_index = Index;
		return 0;
	}
}

class strLedGleamPara {
	private int m_led;
	private int m_ontime;
	private int m_offtime;
	private int m_duration;

	public int getLed() {
		return m_led;
	}

	public int getOntime() {
		return m_ontime;
	}

	public int getOfftime() {
		return m_offtime;
	}

	public int getDuration() {
		return m_duration;
	}

	public int setLed(int Led) {
		m_led = Led;
		return 0;
	}

	public byte setOntime(int Ontime) {
		m_ontime = Ontime;
		return 0;
	}

	public byte setOfftime(int Offtime) {
		m_offtime = Offtime;
		return 0;
	}

	public byte setDuration(int Duration) {
		m_duration = Duration;
		return 0;
	}
}

class strComAttr {
	private int m_baud;
	private int m_databits;
	private int m_parity;
	private int m_stopbits;

	public int getBaud() {
		return m_baud;
	}

	public int getDatabits() {
		return m_databits;
	}

	public int getParity() {
		return m_parity;
	}

	public int getStopbits() {
		return m_stopbits;
	}

	public int setBaud(int Baud) {
		m_baud = Baud;
		return 0;
	}

	public byte setDatabits(int Databits) {
		m_databits = Databits;
		return 0;
	}

	public byte setParity(int Parity) {
		m_parity = Parity;
		return 0;
	}

	public byte setStopbits(int Stopbits) {
		m_stopbits = Stopbits;
		return 0;
	}
}

public final class Ddi extends IDdiManager.Stub {
	private static final String TAG = "DdiService";
	private Printer printer;

	final Context mContext;

	public Ddi(Context context) {
		mContext = context;
		// TODO Auto-generated constructor stub.
		printer = new Printer();
		ddi_ddi_sys_init();
	}

	public void ddiDdiSysInit(){
		ddi_ddi_sys_init();
	}
	
	public void ddiDdiSysUnInit(){
		ddi_ddi_sys_uninit();
		System.out.println("===ddiDdiSysUnInit===");
	}

	public int ddiMagcardOpen() {
		return ddi_mag_open();
	}

	public int ddiMagcardClose () {
		return ddi_mag_close();
	}

	public int ddiMagcardClear() {
		return ddi_mag_clear();
	}

	public int ddiMagcardRead(byte[] lpTrack1, byte[] lpTrack2, byte[] lpTrack3) {
		return ddi_mag_read(lpTrack1, lpTrack2, lpTrack3);
	}
	
	public int ddiMagcardIoctl(int nCmd, int lParam, int wParam) {
		return ddi_mag_ioctl(nCmd, lParam, wParam);
	}

	public int ddiMagcardIoctlGetVersion(byte[] wParam) {
		return ddi_mag_ioctl_getVer(wParam);
	}

	public int ddiSysReadDsn(byte[] lpOut) {
		return ddi_sys_read_dsn(lpOut);
	}

	public int ddiSysGetFirmwarever(byte[] lpOut, int nType) {
		return ddi_sys_get_firmwarever(lpOut, nType);
	}

	public int ddiSysBatStatus() {
		return ddi_sys_bat_status();
	}

	public int ddiSysMainBatStatus() {
		return ddi_sys_mainBat_status();
	}

	public int ddiSysPoweroff() {
		return ddi_sys_poweroff();
	}

	public int ddiSysDownload(int nType) {
		return ddi_sys_download(nType);
	}

	public int ddiSysSetTimeout(int nTime) {
		return ddi_sys_set_timeout(nTime);
	}

	public int ddiSysGetTimeout(int[] nTime) {
		return ddi_sys_get_timeout(nTime);
	}

	public int ddiSysGetChipID(byte[] nType) {
		return ddi_sys_get_chipID(nType);
	}

	public int ddiSecurityRand(byte[] rand) {
		return ddi_security_rand(rand);
	}

	public int ddiGetDebugStatus() {
		return ddi_get_debugStatus();
	}

	public int ddiSetDebugStatus(byte status, byte[] password) {
		return ddi_set_debugStatus(status, password);
	}

	public int ddiDeleteCertByPassword(byte[] password) {
		return ddi_delete_cert_byPassword(password);
	}

	public int ddiSysSetBeep(int nTime) {
		return ddi_sys_set_beep(nTime);
	}

	public int ddiSysGetCertHash(byte[] hash) {
		return ddi_sys_getCertHash(hash);
	}

	public int ddiSysSetCertHash(byte[] hash) {
		return ddi_sys_setCertHash(hash);
	}

	public int ddiBodyNumberProcessDownload() {
		return ddi_BodyNumber_process_download();
	}

	public int ddiBodyNumberProcessSerialRead() {
		return ddi_BodyNumber_process_serial_read();
	}

	public int ddiOpenPort() {
		return ddi_open_port();
	}

	public int ddiClosePort() {
		return ddi_open_port();
	}

	public int ddiComOpen(int nCom, strComAttr ComAttr) {
		return ddi_com_open(nCom, ComAttr);
	}

	public int ddiComClose(int nCom) {
		return ddi_com_close(nCom);
	}

	public int ddiComClear(int nCom) {
		return ddi_com_clear(nCom);
	}

	public int ddiComRead(int nCom, byte[] lpOut, int nLe) {
		return ddi_com_read(nCom, lpOut, nLe);
	}

	public int ddiComWrite(int nCom, byte[] lpIn, int nLe) {
		return ddi_com_write(nCom, lpIn, nLe);
	}

	public int ddiComIoctl(int nCom, int nCmd, int lParam, int wParam) {
		return ddi_com_ioctl(nCom, nCmd, lParam, wParam);
	}

	public int ddiK21ComClear(int nCom) {
		return ddi_k21_com_clear(nCom);
	}

	public int ddiK21ComRead(int nCom, byte[] lpOut, int nLe) {
		return ddi_k21_com_read(nCom, lpOut, nLe);
	}

	public int ddiK21ComWrite(int nCom, byte[] lpIn, int nLe) {
		return ddi_k21_com_write(nCom, lpIn, nLe);
	}

	public int ddiIccpsamOpen(int nSlot) {
		return ddi_iccpsam_open(nSlot);
	}

	public int ddiIccpsamClose(int nSlot) {
		return ddi_iccpsam_close(nSlot);
	}

	public int ddiIccpsamPoweron(int nSlot, byte[] lpAtr) {
		return ddi_iccpsam_poweron(nSlot, lpAtr);
	}

	public int ddiIccpsamPoweroff(int nSlot) {
		return ddi_iccpsam_poweroff(nSlot);
	}

	public int ddiIccpsamGetStatus(int nSlot) {
		return ddi_iccpsam_get_status(nSlot);
	}

	public int ddiIccpsamExchangeApdu(int nSlot, byte[] lpCApdu, int lpCApduLen, byte[] lpRApdu,
			int[] lpRApduLen, int lpRApduSize) {
		return ddi_iccpsam_exchange_apdu(nSlot, lpCApdu, lpCApduLen, lpRApdu, lpRApduLen, lpRApduSize);
	}


	public int ddiRfOpen() {
		return ddi_rf_open();
	}

	public int ddiRfClose() {
		return ddi_rf_close();
	}

	public int ddiRfPoweron(int nType) {
		return ddi_rf_poweron(nType);
	}

	public int ddiRfPoweroff() {
		return ddi_rf_poweroff();
	}

	public int ddiRfGetStatus() {
		return ddi_rf_get_status();
	}

	public int ddiRfActivate() {
		return ddi_rf_activate();
	}

	public int ddiRfExchangeApdu(byte[] lpCApdu, int lpCApduLen, byte[] lpRApdu, int[] lpRApduLen,
			int lpRApduSize) {
		return ddi_rf_exchange_apdu(lpCApdu, lpCApduLen, lpRApdu, lpRApduLen, lpRApduSize);
	}


	public int ddiPrintInit(int step) {
		return ddi_prnt_init(step);
	}
	public int ddiPrintStart(int step) {
		return ddi_prnt_start(step);
	}
	public int ddiPrintGetStatus() {
		return ddi_prnt_get_status();
	}
	public int ddiPrintPrintf(byte[] text) {
		String str = new String(text);
		return ddi_prnt_printf(str);
	}
	public int ddiPrintCommand(byte[] comm,int len) {
		return ddi_prnt_command(comm, len);
	}
	public int ddiPrintGraphics(int offset,byte [] imagepath) {
		return ddi_prnt_graphics(offset, imagepath);
	}
	public int ddiPrintMatrix(int offset,int w,int h,byte[] data,int nlen) {
		return ddi_prnt_matrix(offset, w, h, data, nlen);
	}
	public int ddiSysBeepCtrl(int o1,int o2,int o3) {
		return ddi_sys_beep_ctrl(o1, o2, o3);
	}
	public int ddiPrintSetGray(int len) {
		return ddi_prnt_set_gray(len);
	}
	

	public int ddiInnerkeyOpen() {
		return ddi_innerkey_open();
	}

	public int ddiInnerkeyClose() {
		return ddi_innerkey_close();
	}

	public int ddiInnerkeyInject(int nKeyArea, int nIndex, byte[] lpKeyData) {
		return ddi_innerkey_inject(nKeyArea, nIndex, lpKeyData);
	}
	public int ddiInnerkeyEncrypt(int nKeyArea, int nIndex, int nLen, byte[] lpIn, byte[] lpOut) {
		return ddi_innerkey_encrypt(nKeyArea, nIndex, nLen, lpIn, lpOut);
	}

	public int ddiInnerkeyDecrypt(int nKeyArea, int nIndex, int nLen, byte[] lpIn, byte[] lpOut) {
		return ddi_innerkey_decrypt(nKeyArea, nIndex, nLen, lpIn, lpOut);
	}


	public int ddiLedOpen() {
		return ddi_led_open();
	}

	public int ddiLedClose() {
		return ddi_led_close();
	}

	public int ddiLedSetStatus(int nLed, int nSta) {
		return ddi_led_sta_set(nLed, nSta);
	}


	public int ddiLedIoctlGleam(strLedGleamPara Para) {
		return ddi_led_ioctl_gleam(Para);
	}
	
	
	public int ddiSysGetConfig(byte[] data1,int [] data2){
		return ddi_sys_get_config(data1,data2);
	}

	public native int test(int a, int b);

	public native void ddi_ddi_sys_init();
	
	public native void ddi_ddi_sys_uninit();

	public native int ddi_sys_read_dsn(byte[] lpOut);

	public native int ddi_sys_get_firmwarever(byte[] lpOut, int nType);

	public native int ddi_sys_bat_status();

	public native int ddi_sys_mainBat_status();

	public native int ddi_sys_poweroff();

	public native int ddi_sys_download(int nType);

	public native int ddi_sys_set_timeout(int nTime);

	public native int ddi_sys_get_timeout(int[] nTime);

	public native int ddi_sys_get_chipID(byte[] nType);

	public native int ddi_security_rand(byte[] rand);

	public native int ddi_get_debugStatus();

	public native int ddi_set_debugStatus(byte status, byte[] password);

	public native int ddi_delete_cert_byPassword(byte[] password);

	public native int ddi_sys_set_beep(int nTime);

	public native int ddi_sys_getCertHash(byte[] hash);

	public native int ddi_sys_setCertHash(byte[] hash);

	public native int ddi_BodyNumber_process_download();

	public native int ddi_BodyNumber_process_serial_read();

	public native int ddi_open_port();

	public native int ddi_close_port();

	public native int ddi_com_open_sub(int nCom, int baud, int databits, int parity, int stopbits);

	public int ddi_com_open(int nCom, strComAttr ComAttr) {
		int baud, databits, parity, stopbits;
		baud = ComAttr.getBaud();
		databits = ComAttr.getDatabits();
		parity = ComAttr.getParity();
		stopbits = ComAttr.getStopbits();
		return ddi_com_open_sub(nCom, baud, databits, parity, stopbits);
	}

	public native int ddi_com_close(int nCom);

	public native int ddi_com_clear(int nCom);

	public native int ddi_com_read(int nCom, byte[] lpOut, int nLe);

	public native int ddi_com_write(int nCom, byte[] lpIn, int nLe);

	public native int ddi_com_ioctl(int nCom, int nCmd, int lParam, int wParam);

	public native int ddi_k21_com_clear(int nCom);

	public native int ddi_k21_com_read(int nCom, byte[] lpOut, int nLe);

	public native int ddi_k21_com_write(int nCom, byte[] lpIn, int nLe);

	public native int ddi_mag_open();

	public native int ddi_mag_close();

	public native int ddi_mag_clear();

	public native int ddi_mag_read(byte[] lpTrack1, byte[] lpTrack2, byte[] lpTrack3);

	public native int ddi_mag_ioctl(int nCmd, int lParam, int wParam);

	public native int ddi_mag_ioctl_for_java(int nCmd, int lplen, byte[] lParam, int[] wplen, byte[] wParam);

	public int ddi_mag_ioctl_getVer(byte[] wParam) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		ret = ddi_mag_ioctl_for_java(0, 0, buf, wlen, wParam);

		return ret;
	}

	public native int ddi_iccpsam_open(int nSlot);

	public native int ddi_iccpsam_close(int nSlot);

	public native int ddi_iccpsam_poweron(int nSlot, byte[] lpAtr);

	public native int ddi_iccpsam_poweroff(int nSlot);

	public native int ddi_iccpsam_get_status(int nSlot);

	public native int ddi_iccpsam_exchange_apdu(int nSlot, byte[] lpCApdu, int lpCApduLen, byte[] lpRApdu,
			int[] lpRApduLen, int lpRApduSize);

	public native int ddi_iccpsam_ioctl(int nCmd, int lParam, int wParam);

	public native int ddi_iccpsam_ioctl_for_java(int nCmd, int lplen, byte[] lParam, int[] wplen, byte[] wParam);

	public int ddi_iccpsam_ioctl_getVer(byte[] wParam) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		ret = ddi_iccpsam_ioctl_for_java(0, 0, buf, wlen, wParam);

		return ret;
	}

	public int ddi_iccpsam_ioctl_etud(byte slot, byte etu) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];
		wlen[0] = 1;
		buf[0] = slot;
		buf2[1] = etu;
		ret = ddi_iccpsam_ioctl_for_java(1, 1, buf, wlen, buf2);

		return ret;
	}

	public int ddi_iccpsam_ioctl_memory_poweroff() {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];
		wlen[0] = 0;
		ret = ddi_iccpsam_ioctl_for_java(2, 0, buf, wlen, buf2);

		return ret;
	}

	public int ddi_iccpsam_ioctl_sle4428_reset() {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];
		wlen[0] = 0;
		ret = ddi_iccpsam_ioctl_for_java(3, 0, buf, wlen, buf);

		return ret;
	}

	public int ddi_iccpsam_ioctl_sle4428_read(strSle4428 Sle4428, byte[] data, int[] dataLen) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(Sle4428.getProtect());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(Sle4428.getAddr());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		bufTemp = IntToByte(Sle4428.getLe());
		System.arraycopy(bufTemp, 0, buf, 8, 4);
		wlen[0] = 0;
		ret = ddi_iccpsam_ioctl_for_java(4, 12, buf, dataLen, data);

		return ret;
	}

	public int ddi_iccpsam_ioctl_sle4428_write(strSle4428 Sle4428, byte[] data, int dataLen) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(Sle4428.getProtect());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(Sle4428.getAddr());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		bufTemp = IntToByte(Sle4428.getLe());
		System.arraycopy(bufTemp, 0, buf, 8, 4);
		wlen[0] = dataLen;
		ret = ddi_iccpsam_ioctl_for_java(5, 12, buf, wlen, data);

		return ret;
	}

	public int ddi_iccpsam_ioctl_sle4428_readec(int[] time) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];

		ret = ddi_iccpsam_ioctl_for_java(6, 0, buf, wlen, buf2);
		//
		time[0] = ByteToInt(buf2);
		return ret;
	}

	public int ddi_iccpsam_ioctl_sle4428_verify(byte[] password, int[] time) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];

		ret = ddi_iccpsam_ioctl_for_java(7, 2, password, wlen, buf);

		time[0] = ByteToInt(buf);
		return ret;
	}

	public int ddi_iccpsam_ioctl_sle4428_updatesc(byte[] password) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];

		ret = ddi_iccpsam_ioctl_for_java(8, 0, buf, wlen, password);

		return ret;
	}

	public int ddi_iccpsam_ioctl_sle4442_reset() {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];
		wlen[0] = 0;
		ret = ddi_iccpsam_ioctl_for_java(9, 0, buf, wlen, buf2);

		return ret;
	}

	public int ddi_iccpsam_ioctl_sle4442_read(strSle4442 Sle4442, byte[] data, int[] dataLen) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(Sle4442.getArea());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(Sle4442.getAddr());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		bufTemp = IntToByte(Sle4442.getLe());
		System.arraycopy(bufTemp, 0, buf, 8, 4);
		wlen[0] = 0;
		ret = ddi_iccpsam_ioctl_for_java(10, 12, buf, dataLen, data);

		return ret;
	}

	public int ddi_iccpsam_ioctl_sle4442_write(strSle4442 Sle4442, byte[] data, int dataLen) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(Sle4442.getArea());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(Sle4442.getAddr());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		bufTemp = IntToByte(Sle4442.getLe());
		System.arraycopy(bufTemp, 0, buf, 8, 4);
		wlen[0] = dataLen;
		ret = ddi_iccpsam_ioctl_for_java(11, 12, buf, wlen, data);

		return ret;
	}

	public int ddi_iccpsam_ioctl_sle4442_readec(int[] time) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];

		ret = ddi_iccpsam_ioctl_for_java(12, 0, buf, wlen, buf2);

		time[0] = ByteToInt(buf2);
		return ret;
	}

	public int ddi_iccpsam_ioctl_sle4442_verify(byte[] password, int[] time) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];

		ret = ddi_iccpsam_ioctl_for_java(13, 2, password, wlen, buf);

		time[0] = ByteToInt(buf);
		return ret;
	}

	public int ddi_iccpsam_ioctl_sle4442_updatesc(byte[] password) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];

		ret = ddi_iccpsam_ioctl_for_java(14, 0, buf, wlen, password);

		return ret;
	}

	public int ddi_iccpsam_ioctl_AT24CXX_read(strAT24CXX AT24CXX, byte[] data, int[] dataLen) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(AT24CXX.getType());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(AT24CXX.getAddr());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		bufTemp = IntToByte(AT24CXX.getLe());
		System.arraycopy(bufTemp, 0, buf, 8, 4);
		wlen[0] = 0;
		ret = ddi_iccpsam_ioctl_for_java(15, 12, buf, dataLen, data);

		return ret;
	}

	public int ddi_iccpsam_ioctl_AT24CXX_write(strAT24CXX AT24CXX, byte[] data, int dataLen) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(AT24CXX.getType());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(AT24CXX.getAddr());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		bufTemp = IntToByte(AT24CXX.getLe());
		System.arraycopy(bufTemp, 0, buf, 8, 4);
		wlen[0] = dataLen;
		ret = ddi_iccpsam_ioctl_for_java(16, 12, buf, wlen, data);

		return ret;
	}

	public int ddi_iccpsam_ioctl_AT88SCXX_reset(byte type) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];
		buf[0] = type;
		wlen[0] = 0;
		ret = ddi_iccpsam_ioctl_for_java(17, 1, buf, wlen, buf2);

		return ret;
	}

	public int ddi_iccpsam_ioctl_AT88SCXX_read(strAT88CXX AT88CXX, byte[] data, int[] dataLen) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(AT88CXX.getType());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(AT88CXX.getAddr());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		bufTemp = IntToByte(AT88CXX.getLe());
		System.arraycopy(bufTemp, 0, buf, 8, 4);
		wlen[0] = 0;
		ret = ddi_iccpsam_ioctl_for_java(18, 12, buf, dataLen, data);

		return ret;
	}

	public int ddi_iccpsam_ioctl_AT88SCXX_write(strAT88CXX AT88CXX, byte[] data, int dataLen) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(AT88CXX.getType());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(AT88CXX.getAddr());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		bufTemp = IntToByte(AT88CXX.getLe());
		System.arraycopy(bufTemp, 0, buf, 8, 4);

		wlen[0] = dataLen;
		ret = ddi_iccpsam_ioctl_for_java(19, 12, buf, wlen, data);

		return ret;
	}

	public int ddi_iccpsam_ioctl_AT88SCXX_readec(strAT88CXXReadEc AT88CXXReadEc, int[] time) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(AT88CXXReadEc.getType());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(AT88CXXReadEc.getMode());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		bufTemp = IntToByte(AT88CXXReadEc.getIndex());
		System.arraycopy(bufTemp, 0, buf, 8, 4);

		ret = ddi_iccpsam_ioctl_for_java(20, 12, buf, wlen, buf2);

		time[0] = ByteToInt(buf);
		return ret;
	}

	public int ddi_iccpsam_ioctl_AT88SCXX_verify(strAT88CXXVerify AT88CXXVerify, int[] time) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(AT88CXXVerify.getType());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(AT88CXXVerify.getMode());
		System.arraycopy(bufTemp, 0, buf, 4, 4);

		bufTemp = AT88CXXVerify.getKey();
		System.arraycopy(bufTemp, 0, buf, 8, 3);
		buf[11] = AT88CXXVerify.getIndex();

		ret = ddi_iccpsam_ioctl_for_java(21, 12, buf, wlen, buf2);

		time[0] = ByteToInt(buf);
		return ret;
	}

	public int ddi_iccpsam_ioctl_AT88SCXX_updatesc(strAT88CXXVerify AT88CXXVerify) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(AT88CXXVerify.getType());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(AT88CXXVerify.getMode());
		System.arraycopy(bufTemp, 0, buf, 4, 4);

		bufTemp = AT88CXXVerify.getKey();
		System.arraycopy(bufTemp, 0, buf, 8, 3);
		buf[11] = AT88CXXVerify.getIndex();
		wlen[0] = 0;

		ret = ddi_iccpsam_ioctl_for_java(22, 12, buf, wlen, buf2);

		return ret;
	}

	public int ddi_iccpsam_ioctl_IS23SC1604_reset() {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];
		wlen[0] = 0;
		ret = ddi_iccpsam_ioctl_for_java(23, 0, buf, wlen, buf2);

		return ret;
	}

	public int ddi_iccpsam_ioctl_IS23SC1604_read(strIs23sc1604 Is23sc1604, byte[] data, int[] dataLen) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(Is23sc1604.getAddr());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(Is23sc1604.getLe());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		wlen[0] = 0;
		ret = ddi_iccpsam_ioctl_for_java(24, 8, buf, dataLen, data);

		return ret;
	}

	public int ddi_iccpsam_ioctl_IS23SC1604_write(strIs23sc1604 Is23sc1604, byte[] data, int dataLen) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(Is23sc1604.getAddr());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(Is23sc1604.getLe());
		System.arraycopy(bufTemp, 0, buf, 4, 4);

		wlen[0] = dataLen;
		ret = ddi_iccpsam_ioctl_for_java(25, 8, buf, wlen, data);

		return ret;
	}

	public int ddi_iccpsam_ioctl_IS23SC1604_erase(strIs23sc1604 Is23sc1604, byte[] data, int[] dataLen) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(Is23sc1604.getAddr());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(Is23sc1604.getLe());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		wlen[0] = 0;
		ret = ddi_iccpsam_ioctl_for_java(26, 8, buf, dataLen, data);

		return ret;
	}

	public int ddi_iccpsam_ioctl_IS23SC1604_readec(strIs23sc1604ReadRc Is23sc1604ReadRc, int[] time) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(Is23sc1604ReadRc.getZone());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(Is23sc1604ReadRc.getMode());
		System.arraycopy(bufTemp, 0, buf, 4, 4);

		ret = ddi_iccpsam_ioctl_for_java(27, 8, buf, wlen, buf2);

		time[0] = ByteToInt(buf);
		return ret;
	}

	public int ddi_iccpsam_ioctl_IS23SC1604_verify(strIs23sc1604Verify Is23sc1604Verify, int[] time) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(Is23sc1604Verify.getZone());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(Is23sc1604Verify.getMode());
		System.arraycopy(bufTemp, 0, buf, 4, 4);

		bufTemp = Is23sc1604Verify.getKey();
		System.arraycopy(bufTemp, 0, buf, 8, 2);
		ret = ddi_iccpsam_ioctl_for_java(28, 10, buf, wlen, buf2);

		time[0] = ByteToInt(buf);
		return ret;
	}

	public int ddi_iccpsam_ioctl_IS23SC1604_updatesc(strIs23sc1604Verify Is23sc1604Verify) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[256];
		byte[] bufTemp = new byte[20];

		bufTemp = IntToByte(Is23sc1604Verify.getZone());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(Is23sc1604Verify.getMode());
		System.arraycopy(bufTemp, 0, buf, 4, 4);

		bufTemp = Is23sc1604Verify.getKey();
		System.arraycopy(bufTemp, 0, buf, 8, 2);

		ret = ddi_iccpsam_ioctl_for_java(22, 10, buf, wlen, buf2);

		return ret;
	}

	public native int ddi_rf_open();

	public native int ddi_rf_close();

	public native int ddi_rf_poweron(int nType);

	public native int ddi_rf_poweroff();

	public native int ddi_rf_get_status();

	public native int ddi_rf_activate();

	public native int ddi_rf_exchange_apdu(byte[] lpCApdu, int lpCApduLen, byte[] lpRApdu, int[] lpRApduLen,
			int lpRApduSize);

	public native int ddi_rf_remove();

	public native int ddi_rf_ioctl(int nCmd, int lParam, int wParam);
	
	public native int ddi_m1_auth(int block, int authtype, byte [] key,byte [] uid);
	public native int ddi_m1_read(int block, byte [] readdata);
	public native int ddi_m1_write(int block, byte [] writedata);
	public native int ddi_m1_operate(int block, int type, double value);
	
	

	public native int ddi_rf_ioctl_for_java(int nCmd, int lplen, byte[] lParam, int[] wplen, byte[] wParam);

	public int ddi_rf_ioctl_getVer(byte[] wParam) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		ret = ddi_rf_ioctl_for_java(0, 0, buf, wlen, wParam);

		return ret;
	}

	public int ddi_rf_ioctl_Sak(byte[] wParam) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		ret = ddi_rf_ioctl_for_java(1, 0, buf, wlen, wParam);
		return ret;
	}

	public int ddi_rf_ioctl_Uid(byte[] wParam) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		ret = ddi_rf_ioctl_for_java(2, 0, buf, wlen, wParam);
		return ret;
	}

	public int ddi_rf_ioctl_Mf_Auth(strMfAuth MfAuth) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];
		byte[] bufTemp = new byte[20];

		buf[0] = MfAuth.getAuthmode();
		Arrays.fill(bufTemp, (byte) 0x00);
		bufTemp = MfAuth.getKey();
		for (i = 0; i < 6; i++) {
			buf[i + 1] = bufTemp[i];
		}
		Arrays.fill(bufTemp, (byte) 0x00);
		bufTemp = MfAuth.getUid();
		for (i = 0; i < 10; i++) {
			buf[i + 7] = bufTemp[i];
		}
		buf[17] = MfAuth.getBlock();
		ret = ddi_rf_ioctl_for_java(3, 18, buf, wlen, buf2);
		return ret;
	}

	public int ddi_rf_ioctl_readRaw(byte block, byte[] wParam) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		buf[0] = block;
		ret = ddi_rf_ioctl_for_java(4, 1, buf, wlen, wParam);
		return ret;
	}

	public int ddi_rf_ioctl_writeRaw(byte block, byte[] wParam) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		buf[0] = block;
		wlen[0] = 16;
		ret = ddi_rf_ioctl_for_java(5, 1, buf, wlen, wParam);
		return ret;
	}

	public int ddi_rf_ioctl_readValue(byte block, int[] wParam) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		buf[0] = block;
		ret = ddi_rf_ioctl_for_java(6, 1, buf, wlen, buf);

		wParam[0] = ByteToInt(buf);
		return ret;
	}

	public int ddi_rf_ioctl_writeValue(byte block, int wParam) {
		int ret;
		int[] wlen = new int[1];
		byte[] bufTemp = new byte[20];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];
		buf[0] = block;

		bufTemp = IntToByte(wParam);
		System.arraycopy(bufTemp, 0, buf2, 0, 4);
		wlen[0] = 4;
		ret = ddi_rf_ioctl_for_java(7, 1, buf, wlen, buf2);

		return ret;
	}

	public int ddi_rf_ioctl_incValue(byte block, int wParam) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];
		byte[] bufTemp = new byte[20];
		buf[0] = block;

		bufTemp = IntToByte(wParam);
		System.arraycopy(bufTemp, 0, buf2, 0, 4);
		wlen[0] = 4;
		ret = ddi_rf_ioctl_for_java(8, 1, buf, wlen, buf2);

		return ret;
	}

	public int ddi_rf_ioctl_decValue(byte block, int wParam) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];
		byte[] bufTemp = new byte[20];
		buf[0] = block;

		bufTemp = IntToByte(wParam);
		System.arraycopy(bufTemp, 0, buf2, 0, 4);
		wlen[0] = 4;
		ret = ddi_rf_ioctl_for_java(9, 1, buf, wlen, buf2);

		return ret;
	}

	public int ddi_rf_ioctl_baclupValue(byte fblock, byte tblock) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];
		buf[0] = fblock;
		buf2[0] = tblock;
		wlen[0] = 1;

		ret = ddi_rf_ioctl_for_java(10, 1, buf, wlen, buf2);

		return ret;
	}

	public int ddi_rf_ioctl_setParam(CL_PARAM param) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[60];
		byte[] buf2 = new byte[60];
		byte[] bufTemp = new byte[60];

		buf[0] = param.getModGsP();
		buf[1] = param.getRFCfg_A();
		buf[2] = param.getRFCfg_B();
		buf[3] = param.getRFOLevel();
		buf[4] = param.getRxTreshold_A();
		buf[5] = param.getRxTreshold_B();
		Arrays.fill(bufTemp, (byte) 0x00);
		bufTemp = param.getRFU();
		for (i = 0; i < 25; i++) {
			buf[i + 6] = bufTemp[i];
		}
		buf[31] = param.getcrc();

		ret = ddi_rf_ioctl_for_java(11, 32, buf, wlen, buf2);
		return ret;
	}

	public int ddi_rf_ioctl_getParam(CL_PARAM param) {
		int ret, i;
		int[] wlen = new int[1];
		byte[] buf = new byte[60];
		byte[] buf2 = new byte[60];
		byte[] bufTemp = new byte[60];

		ret = ddi_rf_ioctl_for_java(12, 0, buf, wlen, buf2);
		if (0 == ret && 32 == wlen[0]) {
			param.setModGsP(buf2[0]);
			param.setRFCfg_A(buf2[1]);
			param.setRFCfg_B(buf2[2]);
			param.setRFOLevel(buf2[3]);
			param.setRxTreshold_A(buf2[4]);
			param.setRxTreshold_B(buf2[5]);
			for (i = 0; i < 25; i++) {
				bufTemp[i] = buf2[i + 6];
			}
			param.setRFU(bufTemp);
			param.setcrc(buf2[31]);
		}
		return ret;
	}

	public native int ddi_thmprn_open();

	public native int ddi_thmprn_close();

	public native int ddi_thmprn_feed_paper(int nPixels);

	public native int ddi_thmprn_print_image(int nOrgLeft, int nImageWidth, int nImageHeight, byte[] lpImage);

	public native int ddi_thmprn_print_image_file(int nOrgLeft, int nImageWidth, int nImageHeight, byte[] lpImageName);

	public native int ddi_thmprn_print_text_sub(int align, int offset, int font, int ascsize, int asczoom,
			int nativesize, int nativezoom, byte[] text); //

	public int ddi_thmprn_print_text(strPrnTextCtrl textCtrl, byte[] text) {
		int ret;
		int align, offset, font, ascsize, asczoom, nativesize, nativezoom;

		align = textCtrl.getAlign();
		offset = textCtrl.getOffset();
		font = textCtrl.getFont();
		ascsize = textCtrl.getAscsize();
		asczoom = textCtrl.getAsczoom();
		nativesize = textCtrl.getNativesize();
		nativezoom = textCtrl.getNativezoom();

		ret = ddi_thmprn_print_text_sub(align, offset, font, ascsize, asczoom, nativesize, nativezoom, text);

		return ret;
	}

	public native int ddi_thmprn_print_comb_text_sub2(Object... list);

	public native int ddi_thmprn_print_comb_text_sub(int nNum, int[] m_x0, int[] m_y0, int[] m_font, int[] m_ascsize,
			int[] m_asczoom, int[] m_nativesize, int[] m_nativezoom, Object... list); //

	public int ddi_thmprn_print_comb_text(int nNum, strPrnCombTextCtrl[] textCtrl) {
		int ret = 0, i;

		int[] x0 = new int[256];
		int[] y0 = new int[256];
		int[] font = new int[256];
		int[] ascsize = new int[256];
		int[] asczoom = new int[256];
		int[] nativesize = new int[256];
		int[] nativezoom = new int[256];
		byte[][] text = new byte[256][256];

		if (nNum > 256)
			return -1;

		for (i = 0; i < nNum; i++) {
			x0[i] = textCtrl[i].getX0();
			y0[i] = textCtrl[i].getY0();
			font[i] = textCtrl[i].getFont();
			ascsize[i] = textCtrl[i].getAscsize();
			asczoom[i] = textCtrl[i].getAsczoom();
			nativesize[i] = textCtrl[i].getNativesize();
			nativezoom[i] = textCtrl[i].getNativezoom();
			text[i] = textCtrl[i].getText();
		}
		// ret =
		// ddi_thmprn_print_comb_text_sub(nNum,x0,y0,font,ascsize,asczoom,nativesize,nativezoom,text);

		return ret;
	}

	public native int ddi_thmprn_get_status();

	public native int ddi_thmprn_ioctl(int nCmd, int lParam, int wParam);

	public native int ddi_thmprn_test();

	public native int ddi_thmprn_print_oneBitBMPImage(byte[] imageName, int nameLen);

	public native int ddi_thmprn_print_oneBitBMPImageByBuffer(byte[] imageBuf, int bufLen);

	public native int ddi_thmprn_totalDot(byte[] dotBuf, int bufLen);

	public native int ddi_thmprn_print_blackBlock(int line);

	public native int ddi_thmprn_ioctl_for_java(int nCmd, int lplen, byte[] lParam, int[] wplen, byte[] wParam);

	public int ddi_thmprn_ioctl_getVer(byte[] wParam) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		ret = ddi_thmprn_ioctl_for_java(0, 0, buf, wlen, wParam);

		return ret;
	}

	public int ddi_thmprn_ioctl_setGray(int gray) {
		int ret;
		int[] wlen = new int[1];
		byte[] bufTemp = new byte[20];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];

		bufTemp = IntToByte(gray);
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		wlen[0] = 0;

		ret = ddi_thmprn_ioctl_for_java(1, 4, buf, wlen, buf2);

		return ret;
	}

	public native int ddi_innerkey_open();

	public native int ddi_innerkey_close();

	public native int ddi_innerkey_inject(int nKeyArea, int nIndex, byte[] lpKeyData);

	public native int ddi_innerkey_encrypt(int nKeyArea, int nIndex, int nLen, byte[] lpIn, byte[] lpOut);

	public native int ddi_innerkey_decrypt(int nKeyArea, int nIndex, int nLen, byte[] lpIn, byte[] lpOut);

	public native int ddi_innerkey_ioctl(int nCmd, int lParam, int wParam);

	public native int ddi_innerkey_ioctl_for_java(int nCmd, int lplen, byte[] lParam, int[] wplen, byte[] wParam);

	public int ddi_innerkey_ioctl_getVer(byte[] wParam) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		// ALOGE("ddi_innerkey_ioctl_getVer....................................................................\n");
		ret = ddi_innerkey_ioctl_for_java(0, 0, buf, wlen, wParam);

		return ret;
	}

	public int ddi_innerkey_ioctl_tkey_inject(int area, int index) {
		int ret;
		int[] wlen = new int[1];
		byte[] bufTemp = new byte[20];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];

		bufTemp = IntToByte(area);
		System.arraycopy(bufTemp, 0, buf, 0, 4);

		bufTemp = IntToByte(index);
		System.arraycopy(bufTemp, 0, buf2, 0, 4);
		wlen[0] = 4;

		ret = ddi_innerkey_ioctl_for_java(1, 4, buf, wlen, buf2);

		return ret;
	}

	public int ddi_innerkey_ioctl_tkey_encrypt(strTkey tkey) {
		int ret, i, len;
		int[] wlen = new int[1];
		byte[] buf = new byte[512];
		byte[] buf2 = new byte[512];
		len = tkey.getLen();
		if (len > 512)
			return -3;
		buf = tkey.getIndata();
		wlen[0] = 0;

		ret = ddi_innerkey_ioctl_for_java(2, len, buf, wlen, buf2);

		tkey.setOutdata(buf2);

		return ret;
	}

	public int ddi_innerkey_ioctl_tkey_decrypt(strTkey tkey) {
		int ret, i, len;
		int[] wlen = new int[1];
		byte[] buf = new byte[512];
		byte[] buf2 = new byte[512];
		len = tkey.getLen();
		if (len > 512)
			return -3;
		buf = tkey.getIndata();
		wlen[0] = 0;

		ret = ddi_innerkey_ioctl_for_java(3, len, buf, wlen, buf2);

		tkey.setOutdata(buf2);

		return ret;
	}

	public int ddi_innerkey_ioctl_hkey_encrypt(strHkey hkey) {
		int ret, i, len;
		int[] wlen = new int[1];
		byte[] bufTemp = new byte[512];
		byte[] buf = new byte[512];
		byte[] buf2 = new byte[512];
		int lpLen;
		len = hkey.getLen();
		if (len > 512)
			return -3;

		bufTemp = IntToByte(hkey.getArea());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(hkey.getIndex());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		bufTemp = IntToByte(hkey.getHalf());
		System.arraycopy(bufTemp, 0, buf, 8, 4);

		bufTemp = hkey.getIndata();
		System.arraycopy(bufTemp, 0, buf, 12, len);

		lpLen = 12 + len;
		wlen[0] = 0;

		ret = ddi_innerkey_ioctl_for_java(4, lpLen, buf, wlen, buf2);
		hkey.setOutdata(Arrays.copyOfRange(buf2, 0, wlen[0]));
		// if(wlen<=512)
		// {
		// byte[] buf3 = new byte[wlen];
		// for(i=0;i<wlen;i++)
		// {
		// buf3[i] = buf2[i];
		// }
		// hkey.setOutdata(buf3);
		// }
		return ret;
	}

	public int ddi_innerkey_ioctl_hkey_decrypt(strHkey hkey) {
		int ret, i, len;
		int[] wlen = new int[1];
		byte[] bufTemp = new byte[512];
		byte[] buf = new byte[512];
		byte[] buf2 = new byte[512];
		int lpLen;
		len = hkey.getLen();
		if (len > 512)
			return -3;

		bufTemp = IntToByte(hkey.getArea());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(hkey.getIndex());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		bufTemp = IntToByte(hkey.getHalf());
		System.arraycopy(bufTemp, 0, buf, 8, 4);

		bufTemp = hkey.getIndata();
		System.arraycopy(bufTemp, 0, buf, 12, len);

		lpLen = 12 + len;
		wlen[0] = 0;

		ret = ddi_innerkey_ioctl_for_java(5, lpLen, buf, wlen, buf2);
		hkey.setOutdata(buf2);

		return ret;
	}

	public int ddi_innerkey_ioctl_key_check(int area, int index) {
		int ret;
		int[] wlen = new int[1];
		byte[] bufTemp = new byte[4];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];

		bufTemp = IntToByte(area);
		System.arraycopy(bufTemp, 0, buf, 0, 4);

		bufTemp = IntToByte(index);
		System.arraycopy(bufTemp, 0, buf2, 0, 4);
		wlen[0] = 4;

		ret = ddi_innerkey_ioctl_for_java(6, 4, buf, wlen, buf2);

		return ret;
	}

	public int ddi_innerkey_ioctl_tak_encryptTak(strKeyTAKpara TAKpara) {
		int ret;
		int[] wlen = new int[1];
		byte[] bufTemp = new byte[4];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];

		bufTemp = IntToByte(TAKpara.getArea());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(TAKpara.getTargetIndex());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		bufTemp = IntToByte(TAKpara.getSourceIndex());
		System.arraycopy(bufTemp, 0, buf, 8, 4);
		bufTemp = IntToByte(TAKpara.getSavetIndex());
		System.arraycopy(bufTemp, 0, buf, 12, 4);

		wlen[0] = 0;

		ret = ddi_innerkey_ioctl_for_java(7, 16, buf, wlen, buf2);

		return ret;
	}

	public native int spi_ddi_certmodule_open();

	public native int spi_ddi_certmodule_close();

	public native int spi_ddi_certmodule_save_sub(byte[] CertName, byte[] StrID, byte[] StrLabel, byte ObjectType,
			byte DataType, byte[] Length, byte[] pObjectData, int nDataLength);

	public int spi_ddi_certmodule_save(HSM_ObjectProperty pObjectProperty, byte[] pObjectData, int nDataLength) {
		int ret, i;

		byte[] CertName = new byte[10];
		byte[] StrID = new byte[32];
		byte[] StrLabel = new byte[32];
		byte ObjectType, DataType;
		byte[] DataLength = new byte[4];

		CertName = pObjectProperty.getCertName();
		StrID = pObjectProperty.getStrID();
		StrLabel = pObjectProperty.getStrLabel();
		ObjectType = pObjectProperty.getObjectType();
		DataType = pObjectProperty.getDataType();
		DataLength = pObjectProperty.getDataLength();

		ret = spi_ddi_certmodule_save_sub(CertName, StrID, StrLabel, ObjectType, DataType, DataLength, pObjectData,
				nDataLength);

		return ret;
	}

	public native int spi_ddi_certmodule_readByName_sub(byte[] CertName, byte[] StrID, byte[] StrLabel,
			byte[] ObjectType, byte[] DataType, byte[] Length, byte[] pObjectData, int[] nDataLength);

	public int spi_ddi_certmodule_readByName(HSM_ObjectProperty pObjectProperty, byte[] pObjectData,
			int[] nDataLength) {
		int ret;
		byte[] CertName = new byte[10];
		byte[] StrID = new byte[32];
		byte[] StrLabel = new byte[32];
		byte[] ObjectType = new byte[1];
		byte[] DataType = new byte[1];
		byte[] DataLength = new byte[4];

		CertName = pObjectProperty.getCertName();

		ret = spi_ddi_certmodule_readByName_sub(CertName, StrID, StrLabel, ObjectType, DataType, DataLength,
				pObjectData, nDataLength);

		pObjectProperty.setCertName(CertName);
		pObjectProperty.setStrID(StrID);
		pObjectProperty.setStrLabel(StrLabel);
		pObjectProperty.setObjectType(ObjectType[0]);
		pObjectProperty.setDataType(DataType[0]);
		
		return ret;

	}

	public int getPemFile(byte[] certContent, int length) {
		HSM_ObjectProperty hsm = new HSM_ObjectProperty();
		int[] len = new int[1];
		len[0] = length;
		String nameStr = "pem";
		hsm.setCertName(nameStr.getBytes());
		return spi_ddi_certmodule_readByName(hsm, certContent, len);
	}
    public native int ddi_pk_verify_hash(int length, byte[] hashdata);
	public int ddiPkVerifyHash(int length,byte[] hashdata) {
		return ddi_pk_verify_hash(length,hashdata);
	}

	// public native int spi_ddi_certmodule_readByID_sub(byte[] CertName,byte[]
	// StrID,byte[] StrLabel,byte[] StrPassword,
	// int ObjectType,int DataType,int CertUseMode,int CertLevel,byte[]
	// Signer_StrID ,byte[] Signer_StrLabel,
	// byte[] pObjectData,int[] nDataLength);

	// public int spi_ddi_certmodule_readByID(HSM_ObjectProperty[]
	// pObjectProperty,byte[] pObjectData,int[] nDataLength)
	// {
	// int ret,i;
	//
	// byte[] CertName = new byte[10];
	// byte[] StrID = new byte[32];
	// byte[] StrLabel = new byte[32];
	// byte[] StrPassword = new byte[32];
	// int ObjectType,DataType,CertUseMode,CertLevel;
	// byte[] Signer_StrID = new byte[32];
	// byte[] Signer_StrLabel = new byte[32];
	//

	// CertName = pObjectProperty[0].getCertName();
	// StrID = pObjectProperty[0].getStrID();
	// StrLabel = pObjectProperty[0].getStrLabel();
	// StrPassword = pObjectProperty[0].getPassword();
	// ObjectType = pObjectProperty[0].getObjectType();
	// DataType = pObjectProperty[0].getDataType();
	// CertUseMode = pObjectProperty[0].getCertUseMode();
	// CertLevel = pObjectProperty[0].getCertLevel();
	// Signer_StrID = pObjectProperty[0].getSignerStrID();
	// Signer_StrLabel = pObjectProperty[0].getSignerStrLabel();

	//
	// ret =
	// spi_ddi_certmodule_readByID_sub(CertName,StrID,StrLabel,StrPassword,ObjectType,DataType,
	// CertUseMode,CertLevel,Signer_StrID,Signer_StrLabel,pObjectData,nDataLength);

	// return ret;
	// }
	//

	public native int spi_ddi_certmodule_querycount(int[] certnum);

	public native int spi_ddi_certmodule_delete_sub(int certid, byte[] CertName, byte[] StrID, byte[] StrLabel,
			byte ObjectType, byte DataType, byte[] Length, byte[] verifydata);

	public int spi_ddi_certmodule_delete(int certid, HSM_ObjectProperty pObjectProperty, byte[] verifydata) {
		int ret, i;

		byte[] CertName = new byte[10];
		byte[] StrID = new byte[32];
		byte[] StrLabel = new byte[32];
		byte ObjectType = 0, DataType = 0;
		byte[] DataLength = new byte[4];

		CertName = pObjectProperty.getCertName();
		ret = spi_ddi_certmodule_delete_sub(certid, CertName, StrID, StrLabel, ObjectType, DataType, DataLength,
				verifydata);

		return ret;
	}

	public native int spi_ddi_certmodule_deleteall();

	public native int ddi_led_open();

	public native int ddi_led_close();

	public native int ddi_led_sta_set(int nLed, int nSta);

	public native int ddi_led_ioctl_for_java(int nCmd, int lplen, byte[] lParam, int[] wplen, byte[] wParam);

	public int ddi_led_ioctl_getVer(byte[] wParam) {
		int ret;
		int[] wlen = new int[1];
		byte[] buf = new byte[20];
		ret = ddi_led_ioctl_for_java(0, 0, buf, wlen, wParam);

		return ret;
	}

	public int ddi_led_ioctl_gleam(strLedGleamPara Para) {
		int ret;
		int[] wlen = new int[1];
		byte[] bufTemp = new byte[4];
		byte[] buf = new byte[20];
		byte[] buf2 = new byte[20];

		bufTemp = IntToByte(Para.getLed());
		System.arraycopy(bufTemp, 0, buf, 0, 4);
		bufTemp = IntToByte(Para.getOntime());
		System.arraycopy(bufTemp, 0, buf, 4, 4);
		bufTemp = IntToByte(Para.getOfftime());
		System.arraycopy(bufTemp, 0, buf, 8, 4);
		bufTemp = IntToByte(Para.getDuration());
		System.arraycopy(bufTemp, 0, buf, 12, 4);
		wlen[0] = 0;
		ret = ddi_led_ioctl_for_java(1, 16, buf, wlen, buf2);

		return ret;
	}

	public native int ddi_dukpt_open();

	public native int ddi_dukpt_close();

	public native int ddi_dukpt_inject_sub(byte m_groupindex, byte m_keyindex, byte[] m_initkey, byte m_keylen,
			byte m_ksnindex, byte[] m_initksn, byte m_ksnlen); //

	public int ddi_dukpt_inject(strDukptInitInfo initInfo) {
		int ret;
		byte groupindex, keyindex, keylen, ksnindex, ksnlen;
		byte[] initkey = new byte[24];
		byte[] initksn = new byte[20];

		groupindex = initInfo.getGroupindex();
		keyindex = initInfo.getKeyindex();
		keylen = initInfo.getKeylen();
		ksnindex = initInfo.getKeyindex();
		ksnlen = initInfo.getKsnlen();
		initkey = initInfo.getInitkey();
		initksn = initInfo.getInitksn();

		ret = ddi_dukpt_inject_sub(groupindex, keyindex, initkey, keylen, ksnindex, initksn, ksnlen);

		return ret;
	}

	public native int ddi_dukpt_encrypt(int nKeyGroup, int nKeyIndex, int nLen, byte[] lpIn, byte[] lpOut, int nMode);

	public native int ddi_dukpt_decrypt(int nKeyGroup, int nKeyIndex, int nLen, byte[] lpIn, byte[] lpOut, int nMode);

	public native int ddi_dukpt_getksn(int nKeyGroup, int nKeyIndex, int[] nLen, byte[] lpOut);

	public native int ddi_dukpt_ioctl(int nCmd, int lParam, int wParam);

	public native int setGpioValue(int gpio_no, int gpio_val);

	public native int getGpioValue(int gpio_no);

	public native int setScannerEnable(int on);

	public native int getScannerEnableStatus();

	public native int setScannerPowerOn(int on);

	public native int getScannerPowerOnStatus();

	public native int setScannerUsbSwitch(int on);

	public native int getScannerUsbSwitchStatus();

	public native int ddi_key_open();

	public native int ddi_key_close();

	public native int ddi_key_clear();

	public native int ddi_key_read(int[] key);

	public native int ddi_key_ioctl(int nCmd, int lParam, int wParam);

	public native int ddi_pin_input(int pinlenmin, int pinlenmax, int timeout, int bypassflag);

	public native int ddi_pin_input_press(byte[] keycode);

	public native int ddi_pin_input_cancel();

	public native int ddi_pin_getonlinepinblock(int pinAlgorithmMode, int keyindex, int cardlen, byte[] carddata,
			byte[] pinblockdata);

	public native int ddi_security_getstatus(byte[] statusdata);

	public native int ddi_spi_communication_test(byte[] data, int nLen);

	public native int ddi_sys_get_Hardware_Ver();

	public native int ddi_spi_ddi_sys_set_dsn(int nLen, byte[] data);
	// public native int ddi_TcpClientEntry();

	public int ddi_get_model() {
		Build bd = new Build();
		String s1 = "Octopus A83 F1";
		String s2 = "msm8909";
		String model = bd.MODEL;
		System.err.println("ddi_get_version:" + model);
		if (s1.equals(model) == true)
			return 1;
		else if (s2.equals(model) == true)
			return 2;
		else
			return 0;
	}

	int b_ntohl(byte[] buff) {
		int data = 0;
		data += ((buff[0] << 24) & 0xff000000);
		data += ((buff[1] << 16) & 0x00ff0000);
		data += ((buff[2] << 8) & 0x0000ff00);
		data += ((buff[3] << 0) & 0x000000ff);
		return data;
	}

	byte[] IntToByte(int data) {
		byte[] buf = new byte[4];
		buf[0] = (byte) (data & 0xff);
		buf[1] = (byte) (data >> 8 & 0xff);
		buf[2] = (byte) (data >> 16 & 0xff);
		buf[3] = (byte) (data >> 24 & 0xff);
		return buf;
	}

	int ByteToInt(byte[] data) {
		int temp = 0;
		temp += ((data[0] << 0) & 0x000000ff);
		temp += ((data[1] << 8) & 0x0000ff00);
		temp += ((data[2] << 16) & 0x00ff0000);
		temp += ((data[3] << 24) & 0xff000000);

		return temp;
	}

	class Printer {
		public static final int DEFAULT_WIDTH = 48 * 8;
		public static final int DEFAULT_HEIGHT = 1600;

		private int width = DEFAULT_WIDTH; // 打印机装载的打印纸的�?
		private int height = DEFAULT_HEIGHT; // 打印机装载的打印纸的长度
		private Bitmap paper; // 打印�?
		private int line_space = 0; // 行距
		private Paint paint; // 设置字体
		private Canvas canvas;
		private int cord_y; // 定位纵向 走纸时需要用�?
		private int cord_x; // 定位横向设置左间距需要用�?
		public final String RECEIPT = "/sdcard/receipt.bmp";//Environment.getExternalStorageDirectory() + "/receipt.bmp";
		private String fontFace = "aaa";

		public String getFontFace() {
			return fontFace;
		}

		public void setFontFace(String fontFace) {
			this.fontFace = fontFace;
		}

		public int getCord_y() {
			return cord_y;
		}

		public void setCord_y(int cord_y) {
			this.cord_y = cord_y;
		}

		public int getCord_x() {
			return cord_x;
		}

		public void setCord_x(int cord_x) {
			this.cord_x = cord_x;
		}

		/**
		 * 打印机初始化，并预走�?
		 * 
		 * @param step
		 *            预走纸步�?
		 * @return
		 */
		public void Printer_Init(int step) {
			Log.e("SDK", "Printer_Init step=" + step);
			// start_cord_Y = cord_y;
			cord_y = step;
			paper = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_4444);
			paper.eraseColor(Color.WHITE);
			paint = new Paint();
			paint.setColor(Color.BLACK);
			paint.setTextSize(24);
			// paint.setAntiAlias(true);
			// paint.setFilterBitmap(true);
			canvas = new Canvas(paper);
		}

		/**
		 * 设置打印灰度
		 * 
		 * @param gray
		 *            灰度�? 0 - 100
		 * @return
		 */
		public void Printer_SetGray(int gray) {

		}

		/**
		 * 走纸
		 * 
		 * @param step
		 *            步数
		 * @return
		 */
		public void Printer_Feed(int step) {
			cord_y += step;
		}

		/**
		 * 打印字符�?
		 * 
		 * @param marginLeft
		 *            左边�?
		 * @param text
		 *            字符�?
		 * @param fontsize
		 *            字体大小
		 * @param fontType
		 *            类型�? 黑体，斜体等
		 * @param reverse
		 *            前景背景色反�?
		 * @return
		 */
		// Style
		public final int NORMAL = 0;
		public final int BOLD = 1;
		public final int ITALIC = 2;
		public final int BOLD_ITALIC = 3;

		public int Printer_Print_Text(int marginLeft, String text, int fontsize, int fontType, int reverse,
				Align align) {
			int ret = 0;
			Typeface typeface;
			StringBuilder textBuffer = new StringBuilder();

			switch (fontType) {
			default:
			case NORMAL:
				typeface = Typeface.create(fontFace, Typeface.NORMAL);
				break;
			case BOLD:
				typeface = Typeface.create(fontFace, Typeface.BOLD);
				break;
			case ITALIC:
				typeface = Typeface.create(fontFace, Typeface.ITALIC);
				break;
			case BOLD_ITALIC:
				typeface = Typeface.create(fontFace, Typeface.BOLD_ITALIC);
				break;
			}

			paint.setTypeface(typeface);

			switch (align) {
			default:
			case LEFT:
				cord_x = 0;
				paint.setTextAlign(Align.LEFT);
				break;
			case CENTER:
				cord_x = (paper.getWidth()-marginLeft) / 2;
				paint.setTextAlign(Align.CENTER);
				break;
			case RIGHT:
				cord_x = paper.getWidth();
				paint.setTextAlign(Align.RIGHT);
				break;
			}

			if (fontsize > 0) {
				paint.setTextSize(fontsize);
			}

			FontMetrics fm = paint.getFontMetrics();
			textBuffer = new StringBuilder();
			for (int i = 0; i < text.length(); i++) {
				textBuffer.append(text.charAt(i));
				// Log.e("print text", textBuffer.toString());
				if (Math.round(paint.measureText(textBuffer.toString())) > width || (text.charAt(i) == '\n')) {
					if (text.charAt(i) != '\n') {
						textBuffer.deleteCharAt(textBuffer.length() - 1);
						i--;
					}

					cord_y -= fm.ascent;
					canvas.drawText(textBuffer.toString(), marginLeft+cord_x, cord_y, paint);
					cord_y += fm.descent;
					cord_y += line_space;

					textBuffer.delete(0, textBuffer.length());
				}
			}

			// Log.e("printer", "text buffer:" + textBuffer);
			if (textBuffer.length() > 0) {
				cord_y -= fm.ascent;
				canvas.drawText(textBuffer.toString(), marginLeft + cord_x, cord_y, paint);
				cord_y += fm.descent;
				cord_y += line_space;
				textBuffer.delete(0, textBuffer.length());
			}

			return ret;
		}

		public int Printer_Insert_Text(int x, int y, String text, int fontsize, int fontType, int reverse) {
			int ret = 0;
			Typeface typeface;

			// switch (fontType) {
			// default:
			// case NORMAL:
			// typeface = Typeface.create(fontFace, Typeface.NORMAL);
			// break;
			// case BOLD:
			// typeface = Typeface.create(fontFace, Typeface.BOLD);
			// break;
			// case ITALIC:
			// typeface = Typeface.create(fontFace, Typeface.ITALIC);
			// break;
			// case BOLD_ITALIC:
			// typeface = Typeface.create(fontFace, Typeface.BOLD_ITALIC);
			// break;
			// }

			typeface = Typeface.create(fontFace, fontType);
			paint.setTypeface(typeface);

			if (fontsize > 0) {
				paint.setTextSize(fontsize);
			}

			Log.i(getClass().getName(), "x:y=>" + x + ":" + y);
			canvas.drawText(text, x, y, paint);

			return ret;
		}

		/**
		 * 打印bmp图片
		 * 
		 * @param marginLeft
		 *            左边�?
		 * @param bmp
		 *            图片
		 * @return
		 */
		public int Printer_Print_Bmp(int marginLeft, Bitmap bmp) {
			int ret = 0;
			canvas.drawBitmap(bmp, cord_x + marginLeft, cord_y, paint);

			// cord_x += (marginLeft + bmp.getWidth());
			cord_y += bmp.getHeight();
			return ret;
		}

		/**
		 * 
		 * @param space
		 *            行距
		 * @return
		 */
		public void Printer_Set_Line_Space(int space) {
			line_space = space;
		}

		public Bitmap GetReceipt() {
			Bitmap rcp = Bitmap.createBitmap(paper, 0, 0, width, cord_y);

			return rcp;
		}

		public int Printer_Print() {

			Log.e("Printer", "convert receipt path" + RECEIPT + "!!!");
			File receipt = new File(RECEIPT);// Environment.getExternalStorageDirectory()
												// + "/" + RECEIPT);

			try {
				receipt.deleteOnExit();
				receipt.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Log.e("Printer", "new convertor");
			Log.e("Printer", "paper (w:h): " + paper.getWidth() + ":" + paper.getHeight() + "width:" + width + "cord_y"
					+ cord_y);
			BitmapConvertor convertor = new BitmapConvertor();
			Log.e("Printer", "convert receipt");
			Bitmap receiptBmp = Bitmap.createBitmap(paper, 0, 0, width, cord_y);
			FileOutputStream OUT = null;

			try {
				Log.e(getClass().getName(),
						"tmp file is:" + Environment.getExternalStorageDirectory() + "/tmpreceipt.png");
				OUT = new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/tmpreceipt.png"));
				receiptBmp.compress(Bitmap.CompressFormat.PNG, 100, OUT);
				OUT = new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/tmpreceipt.jpg"));
				receiptBmp.compress(Bitmap.CompressFormat.JPEG, 100, OUT);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (OUT != null) {
					try {
						OUT.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			convertor.convertBitmap(receiptBmp, RECEIPT);
			Log.e("Printer", "gonna print receipt!!!");
			return Ddi_Print_Receipt(RECEIPT);

		}
	}

	public native int Ddi_Print_Receipt(String filename);

	public int Ddi_Print_Init(int step) {
		int ret = 0;

		if (printer == null) {
			printer = new Printer();
		}

		printer.Printer_Init(step);

		return ret;
	}

	public native int Ddi_Printer_set_gray(int level);

	public int Ddi_Printer_Print_Bmp(int marginLeft, Bitmap bmp) {
		int ret = 0;
		if (printer == null) {
			return -1;
		}
		ret = printer.Printer_Print_Bmp(marginLeft, bmp);

		return ret;
	}

	public int Ddi_Printer_Load_Text(int marginLeft, String text, int fontsize, int fontType, int reverse,
			Align align) {
		int ret = 0;
		if (printer == null) {
			return -1;
		}
		ret = printer.Printer_Print_Text(marginLeft, text, fontsize, fontType, reverse, align);
		return ret;
	}

	public int Ddi_Printer_Insert_Text(int x, int y, String text, int fontsize, int fontType, int reverse) {
		int ret = 0;
		if (printer == null) {
			return -1;
		}
		ret = printer.Printer_Insert_Text(x, y, text, fontsize, fontType, reverse);
		return ret;
	}

	public int Ddi_Printer_Start() {
		int ret = 0;
		if (printer == null) {
			return -1;
		}
		ret = printer.Printer_Print();
		return ret;
	}

	public Bitmap Ddi_Printer_get_receipt() {
		if (printer == null) {
			return null;
		}
		return printer.GetReceipt();
	}

	public static native int Ddi_Printer_Set_Gray(int level);

	public int Ddi_Printer_feed(int step) {
		int ret = 0;
		if (printer == null) {
			return -1;
		}
		printer.Printer_Feed(step);
		return ret;
	}

	public void Ddi_Printer_Set_Fontface(String fontfamily) {
		if (printer == null) {
			return;
		}
		printer.setFontFace(fontfamily);
		;
	}

	public void Ddi_Printer_Set_Cusor(int cordX, int cordY) {
		if (printer == null) {
			return;
		}
		printer.setCord_x(cordX);
		printer.setCord_y(cordY);
	}
	
	public native int Ddi_Printer_ft_fill_text(String text);
	
	public native void Ddi_Printer_Test();
	
	public native int ddi_printer_open();
	public native int ddi_printer_feed(int lines);
	public native int ddi_printer_get_status();
	public native int ddi_printer_print(String text);
	public native int ddi_printer_printBmp(int offset, String filename);
	public native int ddi_printer_printMatrix(int width, int height, int offset, byte[] data);
	public native int ddi_printer_close();
	public native int ddi_printer_start(int lin);
	public native int ddi_printer_set_fill_finish();
	
	
	public native int ddi_prnt_init(int step);
	public native int ddi_prnt_start(int step);
	public native int ddi_prnt_get_status();
	public native int ddi_prnt_printf(String text);
	public native int ddi_prnt_command(byte [] comm,int len);
	public native int ddi_prnt_graphics(int offset,byte [] imagepath);
	public native int ddi_prnt_matrix(int offset,int w,int h,byte[] data,int nlen);
	public native int ddi_sys_beep_ctrl(int o1,int o2,int o3);
	public native int ddi_prnt_set_gray(int len);
	public native int ddi_sys_get_config(byte [] data1,int [] data2);



	public int ddiVerifyPublickey(byte[] data, int len) {
		return  ddi_verify_publickey(data, len);
	}


	public int ddiGetPemFile(byte[] data) {
		return getPemFile(data, data.length);
		//return  ddi_get_pem_file(data);
	}

	public int ddiSecurityUnlock(byte[] password) {
		return  ddi_security_unlock(password);
	}
	
	private native int ddi_verify_publickey(byte[] data, int len);
	private native int ddi_get_pem_file(byte[] data);
	
	private native int ddi_security_unlock(byte[] password);



	private static byte[] startLight = new byte[]{(byte)22, (byte)84, (byte)13};
    private static byte[] stopLight = new byte[]{(byte)22, (byte)85, (byte)13};
    private static File power = new File("/sys/devices/soc.0/78b7000.spi/spi_master/spi0/spi0.0/spidev-k21/spidev-k21-0.0/scaner_power");
    private static File openCom = new File("/sys/devices/soc.0/78b0000.serial/multi_uart");
    private static byte[] readdata = new byte[512];
    private static byte[] readataalen = new byte[5];
    private static String fileNames500 = "/dev/ttyHSL0";
    //private static Timer timer;
	
	public int ddiScannOpen() {
        if (openSerialPort()){
        	return 0;
        }else {
        	return -1;
        }
    }

    public void ddiScannClose() {
        closeSerialPort();
    }

    public void ddiScannPowerOn() {
        powerOnScanner();
    }

    public void ddiScannPowerOff() {
        powerOffScanner();
    }

    public void ddiScannCode() {
        writeSerialPort(stopLight, stopLight.length);
        writeSerialPort(startLight, startLight.length);
    }
    
    
    public void ddiScannWriteCommd(byte[] commd) {
    	writeSerialPort(commd, commd.length);
    }

    public void ddiScannContinuousCode(int time) {
        //timer = new Timer();
        //if(time < 200) {
       //     timer.schedule(new TimerScann(), 200, 200);
       // } else {
       //     timer.schedule(new TimerScann(), time, time);
       // }
    }

    public void ddiScannStopContinuousCode() {
        //writeSerialPort(stopLight, stopLight.length);
       // if(timer != null) {
       //     timer.cancel();
       //     timer = null;
       // }
    }

    public byte[] ddiScannReadData(int timeout) {
        Arrays.fill(readdata, (byte)0);
        readSerialPort(readataalen, readdata, timeout);
        System.out.println("req === " + (new String(readdata)).trim());
        return readdata;
    }
/*
    private static class TimerScann extends TimerTask {
        private TimerScann() {
        }

        public void run() {
            writeSerialPort(stopLight, stopLight.length);
            writeSerialPort(startLight, startLight.length);
        }
    }

*/
	//Scanner
	private static native void powerOnScanner();
	private static native void powerOffScanner();
	private static native boolean openSerialPort();
	private static native boolean closeSerialPort();
	private static native boolean writeSerialPort(byte[] writeBytes,int size);
	private static native boolean readSerialPort(byte[] numBytes,byte[] commRead,int outTimes);

/*
	public int ddi_scann_Open() {
        if (ScannController.ScannOpen()){
        	return 0;
        }else {
        	return -1;
        }
    }

    public void ddi_scann_close() {
        ScannController.ScannClose();
    }

    public void ddi_scann_power_on() {
        ScannController.ScannPowerOn();
    }

    public void ddi_scann_power_off() {
        ScannController.ScannPowerOff();
    }

    public void ddi_scann_code() {
        ScannController.ScannCode();
    }
    
    
    public void ScannWriteCommd(byte[] commd){
    	ScannController.ScannWriteCommd(commd);
    }

    public void ddi_scann_continuous_code(int time) {
        ScannController.ContinuousScannCode(time);
    }

    public void ddi_scann_stop_continuous_code() {
        ScannController.StopContinuousScannCode();
    }

    public byte[] ddi_scann_read_data(int timeout) {
        return ScannController.ScannReadData();
    }
*/
}
