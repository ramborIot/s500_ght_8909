package com.android.server.ddi;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 
 */
public class BmpUtilsWithBytes {
    private static final String TAG = "xgdBmpUtils";

    /**
     * 
     *
     * @param bitmap 
     * @return
     */
    public byte[] bmpToBytes(Bitmap bitmap) {

        if (bitmap == null)
            return null;

        int nBmpWidth = bitmap.getWidth();
        int nBmpHeight = bitmap.getHeight();

        int biBitCount = 1;
        int pixSize = nBmpWidth * nBmpHeight;
        int biSizeImage = ((((nBmpWidth * biBitCount) + 31) & ~31) / 8) * nBmpHeight;
        try {
            int headerLength = 62;
            byte[] bmpHeaders = new byte[headerLength];
            int count = 0;

            //bmp
            int bfType = 0x4d42;
            long bfOffBits = 14 + 40 + 8;
            long bfSize = biSizeImage + bfOffBits;
            int bfReserved1 = 0;
            int bfReserved2 = 0;

            writeWord(bmpHeaders,count, bfType);
            count += 2;
            writeDword(bmpHeaders,count, bfSize);
            count += 4;
            writeWord(bmpHeaders,count, bfReserved1);
            count += 2;
            writeWord(bmpHeaders,count, bfReserved2);
            count += 2;
            writeDword(bmpHeaders,count, bfOffBits);
            count += 4;

            //bmp
            long biSize = 40L;
            long biWidth = nBmpWidth;
            long biHeight = nBmpHeight;
            int biPlanes = 1;

            long biCompression = 0L;

            long biXpelsPerMeter = 0L;
            long biYPelsPerMeter = 0L;
            long biClrUsed = 0L;
            long biClrImportant = 0L;
            //
            writeDword(bmpHeaders,count, biSize);
            count += 4;
            writeLong(bmpHeaders,count, biWidth);
            count += 4;
            writeLong(bmpHeaders,count, biHeight);
            count += 4;
            writeWord(bmpHeaders,count, biPlanes);
            count += 2;
            writeWord(bmpHeaders,count, biBitCount);
            count += 2;
            writeDword(bmpHeaders,count, biCompression);
            count += 4;
            writeDword(bmpHeaders,count, biSizeImage);
            count += 4;
            writeLong(bmpHeaders,count, biXpelsPerMeter);
            count += 4;
            writeLong(bmpHeaders,count, biYPelsPerMeter);
            count += 4;
            writeLong(bmpHeaders,count, biClrUsed);
            count += 4;
            writeLong(bmpHeaders,count, biClrImportant);
            count += 4;
            writeLong(bmpHeaders,count, 0xff000000L);
            count += 4;
            writeLong(bmpHeaders,count, 0xffffffffL);

            byte data[] = new byte[biSizeImage];
            int wWidth = biSizeImage / nBmpHeight;
            Arrays.fill(data,(byte)0);

            for (int nCol = 0, nRealCol = nBmpHeight - 1; nCol < nBmpHeight; ++nCol, --nRealCol) {
                String binary = "";
                for (int wRow = 0; wRow < wWidth * 8; wRow++) {
                    if (wRow < nBmpWidth) {
                        int clr = bitmap.getPixel(wRow, nCol);
                        int tmp = Color.green(clr) > 128 ? 1 : 0;
                        binary += tmp + "";
                    } else {
                        binary += "0";
                    }

                    if ((wRow + 1) % 8 == 0) {
                        String hex = binaryString2hexString(binary);
                        //data[wWidth * nRealCol + ((wRow + 1) / 8 - 1)] = hexStringToByte(hex);
                        binary = "";
                    }
                }
            }

            if (!bitmap.isRecycled()) {
                bitmap.recycle();
            }

            byte[] mBuffer = new byte[headerLength + biSizeImage];
            System.arraycopy(bmpHeaders,0,mBuffer,0,headerLength);
            System.arraycopy(data,0,mBuffer,headerLength,biSizeImage);

            return mBuffer;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    protected static void writeWord(byte[] buffer,int count, int value) throws IOException {
        buffer[count++] = (byte) (value & 0xff);
        buffer[count] = (byte) (value >> 8 & 0xff);
    }

    protected static void writeDword(byte[] buffer,int count, long value) throws IOException {
        buffer[count++] = (byte) (value & 0xff);
        buffer[count++] = (byte) (value >> 8 & 0xff);
        buffer[count++] = (byte) (value >> 16 & 0xff);
        buffer[count] = (byte) (value >> 24 & 0xff);
    }

    protected static void writeLong(byte[] buffer,int count, long value) throws IOException {
        buffer[count++] = (byte) (value & 0xff);
        buffer[count++] = (byte) (value >> 8 & 0xff);
        buffer[count++] = (byte) (value >> 16 & 0xff);
        buffer[count] = (byte) (value >> 24 & 0xff);
        if (value == 300) {
        }
        if (value == 256) {
        }
    }
	public static byte[] hexStringToByte(String hex) {
    int len = (hex.length() / 2);
    byte[] result = new byte[len];
    char[] achar = hex.toCharArray();
    for (int i = 0; i < len; i++) {
     int pos = i * 2;
     result[i] = (byte) (toByte(achar[pos]) << 4 | toByte(achar[pos + 1]));
    }
    return result;
   }
	private static byte toByte(char c) {
    byte b = (byte) "0123456789ABCDEF".indexOf(c);
    return b;
   }

	public static String binaryString2hexString(String bString)  
    {  
        if (bString == null || bString.equals("") || bString.length() % 8 != 0)  
            return null;  
        StringBuffer tmp = new StringBuffer();  
        int iTmp = 0;  
        for (int i = 0; i < bString.length(); i += 4)  
        {  
            iTmp = 0;  
            for (int j = 0; j < 4; j++)  
            {  
                iTmp += Integer.parseInt(bString.substring(i + j, i + j + 1)) << (4 - j - 1);  
            }  
            tmp.append(Integer.toHexString(iTmp));  
        }  
        return tmp.toString();  
    }  
}
