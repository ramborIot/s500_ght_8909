package com.android.server.manage;

import android.text.TextUtils;
import android.util.Log;


public class StringUtility 
{
	/**@author 閸炴劕浼�
	 * @param strInput 娴肩姴鍙哠tring
	 * @return boolean 娴肩姴鍙嗛惃鍑穞ring閺勵垰鎯佹稉铏光敄
	 * */
	static public boolean isEmpty(String strInput)
	{
		/*
		if(strInput == null)
			return true;
		return strInput.length() == 0 ? true : false;
		*/
	    return TextUtils.isEmpty(strInput);

	}

	public static String getStringFormat(byte[] bytes){
		String str = "";
		for(byte b : bytes){
			str += String.format("%02X ", b);
		}
		return str;
	}
	
	static protected boolean CheckByte(byte byteIn)
	{
		//'0' - '9'
		if(byteIn <= 0x39 && byteIn >= 0x30)
			return true;
		//'A' - 'F'
		if(byteIn <= 0x46 && byteIn >= 0x41)
			return true;
		//'a' - 'f'
		if(byteIn <= 0x66 && byteIn >= 0x61)
			return true;
		return false;
	}
	static protected boolean CheckString(String strInput)
	{
		strInput = strInput.trim();
		if(strInput.length() != 2)
			return false;
		byte[] byteArry = strInput.getBytes();
		for(int i = 0; i < 2; i++)
		{
			if(!CheckByte(byteArry[i]))
				return false;
		}
		return true;
	}
	
	static protected byte StringToByte(String strInput)
	{
		byte[] byteArry = strInput.getBytes();
		for(int i = 0; i < 2; i++)
		{
			
			if(byteArry[i] <= 0x39 && byteArry[i] >= 0x30)
			{
				byteArry[i] -= 0x30; 
			}
			else if(byteArry[i] <= 0x46 && byteArry[i] >= 0x41)
			{
				byteArry[i] -= 0x37;
			}
			else if(byteArry[i] <= 0x66 && byteArry[i] >= 0x61)
			{
				byteArry[i] -= 0x57;
			}
		}
		//Log.i("APP", String.format("byteArry[0] = 0x%X\n", byteArry[0]));
		//Log.i("APP", String.format("byteArry[1] = 0x%X\n", byteArry[1]));
		return (byte)((byteArry[0] << 4) | (byteArry[1] & 0x0F));
	}
	/** @author 閸炴劕浼�
	 *  @param String strInput 
	 *  @param byte[] arryByte 
	 *  @return int
	 * */
	static public int StringToByteArray(String strInput, byte[] arryByte)
	{
		strInput = strInput.trim();
		String[] arryString = strInput.split(" ");
		if(arryByte.length < arryString.length)
			return -1;
		for(int i = 0; i < arryString.length; i++)
		{
			if(!CheckString(arryString[i]))
				return -1;
			arryByte[i] = StringToByte(arryString[i]);
			//Log.i("APP", String.format("%02X", arryByte[i]));
		}
		
		return arryString.length;
	}
	static public String ByteArrayToString(byte[] arryByte, int nDataLength)
	{
		String strOut = new String();
		for(int i = 0; i < nDataLength; i++)
			strOut += String.format("%02X ", arryByte[i]);
		return strOut;
	}
	/** @author john.li
	 *  @param String str 娴肩姴鍙嗙�妤冾儊閿燂拷
	 *  @param String reg 閹稿鍙庨崫顏嗩瀸閺傜懓绱￠幋鏍ф憿娑擃亜鐡у▓鍨閿燂拷
	 *  @return Stringp[] 鏉╂柨娲栭幏鍡楀瀻閸氬海娈戦弫鎵矋閿燂拷
	 * */
	static public String[] spiltStrings(String str, String reg){
		String [] arrayStr = str.split(reg); 
		return arrayStr;
	}
	static public byte[] StringToByteArray(String strInput){
		byte[] data=new byte[2048*8];
		int len=StringToByteArray(strInput, data);
		if(len>=0){
			byte[] result=new byte[len];
			System.arraycopy(data, 0, result, 0, len);
			return result;
		}
		return null;
	}
}
