package com.android.server.ddi;

import com.android.server.ddi.UART;
import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

public class ScannController {
    private static final byte SYN = 22;
    private static final byte T = 84;
    private static final byte U = 85;
    private static final byte CR = 13;
    private static byte[] startLight = new byte[]{(byte)22, (byte)84, (byte)13};
    private static byte[] stopLight = new byte[]{(byte)22, (byte)85, (byte)13};
    private static File power = new File("/sys/devices/soc.0/78b7000.spi/spi_master/spi0/spi0.0/spidev-k21/spidev-k21-0.0/scaner_power");
    private static File openCom = new File("/sys/devices/soc.0/78b0000.serial/multi_uart");
    private static File p800Poweron = new File("/sys/bus/platform/devices/extpwr.0/driver/extpwr_val");
    private static File p800Poweroff = new File("/sys/bus/platform/devices/extpwr.0/driver/extpwr_val");
    private static FileWriter writer = null;
    private static byte[] readdata = new byte[512];
    private static byte[] readataalen = new byte[5];
    private static String fileNames500 = "/dev/ttyHSL0";
    private static String fileNamep800 = "/dev/ttyMT1";
    private static Timer timer;

    public ScannController() {
    	
    }

    public static boolean ScannOpen() {
        boolean open = UART.rs232_OpenPort(fileNames500, 115200, 'N', 8, 1);
        if(open) {
            writeSysDev(power, "1");
            writeSysDev(openCom, "1");
            return true;
        } else {
            return false;
        }
    }

    public static void ScannClose() {
        UART.rs232_ClosePort();
        writeSysDev(power, "0");
        writeSysDev(openCom, "0");
    }

    public static void ScannPowerOn() {
        writeSysDev(power, "1");
    }

    public static void ScannPowerOff() {
        writeSysDev(power, "0");
    }

    private static void writeSysDev(File file, String value) {
        try {
            writer = new FileWriter(file);
            writer.write(value);
            writer.flush();
            writer.close();
        } catch (Exception var3) {
            var3.printStackTrace();
        }

    }

    public static void ScannCode() {
        UART.rs232_WritePort(stopLight, stopLight.length);
        UART.rs232_WritePort(startLight, startLight.length);
        System.out.println("====ScannCode====");
    }
    
    
    public static void ScannWriteCommd(byte[] commd){
    	UART.rs232_WritePort(commd, commd.length);
    }

    public static void ContinuousScannCode(int time) {
        timer = new Timer();
        if(time < 200) {
            timer.schedule(new TimerScann(), 200, 200);
        } else {
            timer.schedule(new TimerScann(), time, time);
        }

    }

    public static void StopContinuousScannCode() {
        UART.rs232_WritePort(stopLight, stopLight.length);
        if(timer != null) {
            timer.cancel();
            timer = null;
        }

    }

    public static byte[] ScannReadData() {
        Arrays.fill(readdata, (byte)0);
        UART.rs232_ReadPort(readataalen, readdata, 0);
        System.out.println("req === " + (new String(readdata)).trim());
        return readdata;
    }

    private static class TimerScann extends TimerTask {
        private TimerScann() {
        }

        public void run() {
            UART.rs232_WritePort(stopLight, stopLight.length);
            UART.rs232_WritePort(startLight, startLight.length);
        }
    }
}
