package com.android.server.ddi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Bitmap.Config;
import android.util.Log;

public class BitmapConvertor {

	private int mDataWidth;
	private byte mRawBitmapData[];
	private byte[] mDataArray;
	private static final String TAG = "BitmapConvertor";
	private int mWidth, mHeight;
	private String mStatus;
	private String mFileName;

	public BitmapConvertor() {
		// TODO Auto-generated constructor stub
		Log.e("in convertor", "convertor's constructor");
	}

	/**
	 * Converts the input image to 1bpp-monochrome bitmap
	 * 
	 * @param inputBitmap
	 *            : Bitmpa to be converted
	 * @param fileName
	 *            : Save-As filename
	 * @return : Returns a String. Success when the file is saved on memory card
	 *         or error.
	 */
	public String convertBitmap(Bitmap inputBitmap, String fileName) {
		Log.e(getClass().getName(), "in convertBitmap save file Name:" + fileName);
		mWidth = inputBitmap.getWidth();
		mHeight = inputBitmap.getHeight();
		mFileName = fileName;
		mDataWidth = ((mWidth + 31) / 32) * 4 * 8;
		mDataArray = new byte[(mDataWidth * mHeight)];
		mRawBitmapData = new byte[(mDataWidth * mHeight) / 8];
		convertArgbToGrayscale(inputBitmap, mWidth, mHeight);
		createRawMonochromeData();
		mStatus = saveImage(mFileName, mWidth, mHeight);
		return mStatus;

	}
	

	private void convertArgbToGrayscale(Bitmap bmpOriginal, int width, int height) {
		int pixel;
		int k = 0;
		int B = 0, G = 0, R = 0;
		int Bpai = 0, Gpai = 0, Rpai = 0;
		int A = 0;
		int gray = 0;
		
		try {
			for (int x = 0; x < height; x++) {
				for (int y = 0; y < width; y++, k++) {
					// get one pixel color
					pixel = bmpOriginal.getPixel(y, x);

					// retrieve color of all channels
					R = Color.red(pixel);
					Rpai = (pixel & 0xf800) >> 11;
					G = Color.green(pixel);
					Gpai = (pixel & 0x07e0) >> 5;
					B = Color.blue(pixel);
					Bpai = (pixel & 0x001f) >> 5;
					R = Color.alpha(pixel);
					
					
//					float[] hsv = new float[ 3 ];
//					Color.colorToHSV(pixel, hsv );
//					if (hsv[2]>0.5f) {
//						mDataArray[k] = 1;
//					}
//					else {
//						mDataArray[k] = 0;
//					}
					
					// take conversion up to one single value by calculating
					// pixel intensity.	
					gray =  (pixel & 0x80) >> 7;
					mDataArray[k] = (byte) gray;
//					Log.e(getClass().getName(), "pixel is:"+pixel);
//					
//					if (pixel == Color.WHITE) {
//						mDataArray[k] = 1;
//					}else{
//						mDataArray[k] = 0;
//					}
				
//					gray = (int) ((0.2125 * R) + (0.7154 * G) + (0.0721 * B));
//					gray = (int) ((0.3 * R) + (0.59 * G) + (0.11 * B));
					
					
//					R = G = B = (int) (0.299 * R + 0.587 * G + 0.114 * B);
					// set new pixel color to output bitmap
//					Log.e(getClass().getName(), "gray is:" + gray + "R is:" + R);

//					Log.e(getClass().getName(), "R:"+R+"G:"+G+"B:"+B+"===Rpai:"+Rpai+"Gpai:"+Gpai+"Bpai:"+Bpai+"gray:"+gray+"A is:"+A);
//					if (R < 128) {
//						if (Rpai < 20 || Gpai < 40) {
//					if (gray < 128) {
//						mDataArray[k] = 0;
//					} else {
//						mDataArray[k] = 1;
//					}
//					
//					if (pixel == Color.BLACK) {
//						mDataArray[k] = 0;
//					}else {
//						mDataArray[k] = 1;
//					}
					
				}
				/*
				if (mDataWidth > width) {
					for (int p = width; p < mDataWidth; p++, k++) {
						mDataArray[k] = 1;
					}
				}
				*/
			}
		} catch (Exception e) {
			// TODO: handle exception
			Log.e(TAG, e.toString());
		}
	}

	private void createRawMonochromeData() {
		int length = 0;
		for (int i = 0; i < mDataArray.length; i = i + 8) {
			mRawBitmapData[length] = mDataArray[i];
			for (int j = 1; j < 8; j++) {
				mRawBitmapData[length] = (byte) ((mRawBitmapData[length]<< 1) | mDataArray[i + j]);
			}
			length++;
		}
	}

	private String saveImage(String fileName, int width, int height) {
		FileOutputStream fileOutputStream;
		BMPFile bmpFile = new BMPFile();
		File file = new File(fileName);

		try {
			file.createNewFile();
			fileOutputStream = new FileOutputStream(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return "Memory Access Denied";
		}
		bmpFile.saveBitmap(fileOutputStream, mRawBitmapData, width, height);
		try {
			fileOutputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "Success";
	}
	
	
	public static byte[] bitmapToByteArray(Bitmap bm) {
        // Create the buffer with the correct size
        int iBytes = bm.getWidth() * bm.getHeight() ;
        byte[] res = new byte[iBytes];
        Bitmap.Config format = bm.getConfig();
        if (format == Bitmap.Config.ARGB_8888)
        {
            ByteBuffer buffer = ByteBuffer.allocate(iBytes*4);
            // Log.e("DBG", buffer.remaining()+""); -- Returns a correct number based on dimensions
            // Copy to buffer and then into byte array
            bm.copyPixelsToBuffer(buffer);
            byte[] arr = buffer.array();
            for(int i=0;i<iBytes;i++)
            {
                int A,R,G,B;
                R=(int)(arr[i*4+0]) & 0xff;
                G=(int)(arr[i*4+1]) & 0xff;
                B=(int)(arr[i*4+2]) & 0xff;
                //A=arr[i*4+3];
                byte r = (byte)(0.2989 * R + 0.5870 * G + 0.1140 * B) ;
                res[i] = r;
            }
        }
        if (format == Bitmap.Config.RGB_565)
        {
            ByteBuffer buffer = ByteBuffer.allocate(iBytes*2);
            // Log.e("DBG", buffer.remaining()+""); -- Returns a correct number based on dimensions
            // Copy to buffer and then into byte array
            bm.copyPixelsToBuffer(buffer);
            byte[] arr = buffer.array();
            for(int i=0;i<iBytes;i++)
            {
                float A,R,G,B;
                R = ((arr[i*2+0] & 0xF8) );
                G = ((arr[i*2+0] & 0x7) << 5) + ((arr[i*2+1] & 0xE0) >> 5);
                B = ((arr[i*2+1] & 0x1F) << 3 );
                byte r = (byte)(0.2989 * R + 0.5870 * G + 0.1140 * B) ;
                res[i] = r;
            }
        }
        // Log.e("DBG", buffer.remaining()+""); -- Returns 0
        return res;
    }
}
	
