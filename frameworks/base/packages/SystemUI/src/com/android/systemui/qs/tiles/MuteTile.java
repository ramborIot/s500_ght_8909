/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui.qs.tiles;

import android.media.AudioManager;
import android.provider.Settings;
import android.provider.Settings.Global;

import com.android.systemui.R;
import com.android.systemui.qs.GlobalSetting;
import com.android.systemui.qs.QSTile;

public class MuteTile extends QSTile<QSTile.BooleanState> {

	private boolean mListening;
	private AudioManager mAudioManager;
	private final GlobalSetting mSetting;
	
	public MuteTile(Host host) {
		super(host);
		// TODO Auto-generated constructor stub
		mAudioManager = (AudioManager)mContext.getSystemService(mContext.AUDIO_SERVICE);
		mSetting = new GlobalSetting(mContext, mHandler, Settings.System.MODE_RINGER_STREAMS_AFFECTED) {
            @Override
            protected void handleValueChanged(int value) {
                handleRefreshState(value);
            }
        };
	}

	public void setListening(boolean listening) {
		// TODO Auto-generated method stub
		if (mListening == listening) return;
        mListening = listening;
        mSetting.setListening(listening);
	}

	@Override
	protected BooleanState newTileState() {
		// TODO Auto-generated method stub
		return new BooleanState();
	}	

	@Override
	protected void handleClick() {
		// TODO Auto-generated method stub
		setMute();		
	}

	@Override
	protected void handleUpdateState(
			BooleanState state, Object arg) {
		// TODO Auto-generated method stub	
		//int mode = mSetting.getValue();
		state.visible = true;
		state.value = true;
		state.label = mContext.getString(R.string.quick_settings_mute_label);
		if (mAudioManager.getStreamVolume(AudioManager.STREAM_RING) != 0) {
			state.icon = ResourceIcon.get(R.drawable.ic_qs_mute_off);
			state.contentDescription =  mContext.getString(R.string.accessibility_quick_settings_mute_off);
		} else {
			state.icon = ResourceIcon.get(R.drawable.ic_qs_mute_on);
			state.contentDescription =  mContext.getString(R.string.accessibility_quick_settings_mute_on);
		}
        
	}

	private void setMute() {
		if (mAudioManager.getStreamVolume(AudioManager.STREAM_RING) != 0) {
			mAudioManager.setStreamMute(AudioManager.STREAM_RING, true);
		} else {
			if (mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {
				mAudioManager.setStreamMute(AudioManager.STREAM_RING, false);
			}			
		}
		mSetting.setValue(mAudioManager.getStreamVolume(AudioManager.STREAM_RING));
	}
}
