/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui.qs.tiles;

import android.media.AudioManager;
import android.provider.Settings;
import android.provider.Settings.Global;
import android.provider.Settings.SettingNotFoundException;

import com.android.systemui.R;
import com.android.systemui.qs.GlobalSetting;
import com.android.systemui.qs.QSTile;
import com.android.systemui.qs.QSTile.ResourceIcon;

public class AudioProfileTile extends QSTile<QSTile.BooleanState> {
    
	private boolean mListening;
	private AudioManager mAudioManager;
	private final GlobalSetting mSetting;
	
    public AudioProfileTile(Host host) {
		super(host);
		// TODO Auto-generated constructor stub
		mAudioManager = (AudioManager)mContext.getSystemService(mContext.AUDIO_SERVICE);
		mSetting = new GlobalSetting(mContext, mHandler, Settings.System.MODE_RINGER_STREAMS_AFFECTED) {
            @Override
            protected void handleValueChanged(int value) {
                handleRefreshState(value);
            }
        };
	}

	public void setListening(boolean listening) {
		// TODO Auto-generated method stub
		if (mListening == listening) return;
        mListening = listening;
        mSetting.setListening(listening);
	}

	@Override
	protected BooleanState newTileState() {
		// TODO Auto-generated method stub
		return new BooleanState();
	}

	@Override
	protected void handleClick() {
		// TODO Auto-generated method stub
		changeRingerMode();
	}

	@Override
	protected void handleUpdateState(
			BooleanState state, Object arg) {
		// TODO Auto-generated method stub		
		state.visible = true;
		state.label = mContext.getString(R.string.quick_settings_audioprofile_label);        
        	if (mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {
			state.contentDescription =  mContext.getString(R.string.accessibility_quick_settings_audioprofile_ring);
			state.icon = ResourceIcon.get(R.drawable.ic_qs_audioprofile_ring);			
		} else if (mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {
			state.contentDescription =  mContext.getString(R.string.accessibility_quick_settings_audioprofile_mute);
			state.icon = ResourceIcon.get(R.drawable.ic_qs_audioprofile_mute);
		} else if (mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE) {
			state.icon = ResourceIcon.get(R.drawable.ic_qs_audioprofile_vibrate);
			state.contentDescription =  mContext.getString(R.string.accessibility_quick_settings_audioprofile_mute);
		}
		mSetting.setValue(mAudioManager.getRingerMode());
	}
	
	private void changeRingerMode() {
		int ring_general_setting_volume;
		if (mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE) {
			Settings.System.putInt(mContext.getContentResolver(), Settings.System.VIBRATE_IN_SILENT, 0);
			mAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
			mAudioManager.setStreamVolume(AudioManager.STREAM_RING, 0, 0);			
		} else if (mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {
			Settings.System.putInt(mContext.getContentResolver(), Settings.System.VIBRATE_IN_SILENT, 1);
			mAudioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
			mAudioManager.setStreamVolume(AudioManager.STREAM_RING, 0, 0);			
		} else if (mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {
			Settings.System.putInt(mContext.getContentResolver(), Settings.System.VIBRATE_IN_SILENT, 0);
			mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
			try {
                        ring_general_setting_volume = Settings.System.getInt(
                                mContext.getContentResolver(),
                                "ring_and_notification_progress");
                     } catch (SettingNotFoundException e) {
                        ring_general_setting_volume = mAudioManager
                                .getStreamMaxVolume(AudioManager.STREAM_RING);
                        e.printStackTrace();
                     }
			mAudioManager.setStreamVolume(AudioManager.STREAM_RING, ring_general_setting_volume, 0);			
		}
		mSetting.setValue(mAudioManager.getRingerMode());
	}
}
