/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.systemui;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.SystemProperties;
import android.provider.Settings;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import android.provider.Settings;

import android.os.Handler;
import android.os.Message;
import java.io.IOException;
import com.qualcomm.qcnvitems.IQcNvItems;
import com.qualcomm.qcnvitems.QcNvItems;
// Bevis added for displaying wifi only ui in 20170629 ++
import com.fibocom.factorytool.FiboFactoryTool;
// Bevis added for displaying wifi only ui in 20170629 --
/**
 * Application class for SystemUI.
 */
public class SystemUIApplication extends Application {

    private static final String TAG = "SystemUIService";
    private static final boolean DEBUG = false;

	// Bevis added for displaying wifi only ui in 20170629 ++
	private FiboFactoryTool factorytool = null;
	// Bevis added for displaying wifi only ui in 20170629 --

    /**
     * The classes of the stuff to start.
     */
    private final Class<?>[] SERVICES = new Class[] {
            com.android.systemui.keyguard.KeyguardViewMediator.class,
            com.android.systemui.recent.Recents.class,
            com.android.systemui.volume.VolumeUI.class,
            com.android.systemui.statusbar.SystemBars.class,
            com.android.systemui.usb.StorageNotification.class,
            com.android.systemui.power.PowerUI.class,
            com.android.systemui.media.RingtonePlayer.class
    };

    /**
     * Hold a reference on the stuff we start.
     */
    private final SystemUI[] mServices = new SystemUI[SERVICES.length];
    private boolean mServicesStarted;
    private boolean mBootCompleted;
    private IQcNvItems mQcNvItemValue = null;
    private static final int EVENT_TICK = 1;
    private final Map<Class<?>, Object> mComponents = new HashMap<Class<?>, Object>();

    @Override
    public void onCreate() {
        super.onCreate();
        // Set the application theme that is inherited by all services. Note that setting the
        // application theme in the manifest does only work for activities. Keep this in sync with
        // the theme set there.
        setTheme(R.style.systemui_theme);
        mQcNvItemValue = new QcNvItems(getApplicationContext());
        // donot call setCmCallType(1) to disable phone call
        //mHandlerNV.sendEmptyMessageDelayed(EVENT_TICK, 2000);

		// Bevis added for displaying wifi only ui in 20170629 ++
		factorytool = new FiboFactoryTool();
		String bpversion = factorytool.getBasebandVersion();
		Log.v("csuntnt", "=====>>>>  bpversion: " + bpversion);
		if (bpversion.contains("NV.WI")) {
			Settings.Secure.putInt(getApplicationContext().getContentResolver(), 
							Settings.Secure.WIFI_ONLY, 1);
		}
		// Bevis added for displaying wifi only ui in 20170629 --

        IntentFilter filter = new IntentFilter(Intent.ACTION_BOOT_COMPLETED);
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateAirplaneMode();
                if (mBootCompleted) return;

                if (DEBUG) Log.v(TAG, "BOOT_COMPLETED received");
                unregisterReceiver(this);
                mBootCompleted = true;
                if (mServicesStarted) {
                    final int N = mServices.length;
                    for (int i = 0; i < N; i++) {
                        mServices[i].onBootCompleted();
                    }
                }
            }
        }, filter);
    }

    private Handler mHandlerNV = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EVENT_TICK:
                    int val = 0;
                    if((Settings.System.getInt(getApplicationContext().getContentResolver(), "NVItem_phone", 0)) == 1 ) {
                        Log.d(TAG, "NVItem_phone == true, not need set again");
                    } else {
                        Log.d(TAG, "NVItem_phone == false, do set");
                        try {
                            Log.w(TAG, "NVItem  sendEmptyMessageDelayed handleMessage");
                            if (mQcNvItemValue != null) {
                                mQcNvItemValue.setCmCallType(1);        //write 1 to disable phone call
                                Log.w(TAG, "write NV done");
                                val = mQcNvItemValue.getCmCallType();   //read it to check
                                Log.w(TAG, "read back is  val = " + val);
                            } else {
                                Log.e(TAG, "mQcNvItemValue=null");
                            }
                        } catch (IOException e) {
                            Log.w(TAG, "Cannot read NV");
                        }
                    }
                    if (val == 1) {
                        Log.w(TAG, " val = " + val + " set  NVItem_phone = 1");
                        Settings.System.putInt(getApplicationContext().getContentResolver(), "NVItem_phone", 1);
                    }
                    mHandlerNV.removeMessages(EVENT_TICK);
                    //finish();
                    break;
            }
        }
    };

    public void updateAirplaneMode() {
        boolean setAirPlane = Settings.System.getInt(this.getContentResolver(), Settings.System.IS_AIRPLANE_ENABLE, 0) == 1;
        Settings.Global.putInt(this.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, setAirPlane ? 1 : 0);
        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intent.putExtra("state", setAirPlane);
        this.sendBroadcast(intent);
    }

    /**
     * Makes sure that all the SystemUI services are running. If they are already running, this is a
     * no-op. This is needed to conditinally start all the services, as we only need to have it in
     * the main process.
     *
     * <p>This method must only be called from the main thread.</p>
     */
    public void startServicesIfNeeded() {
        if (mServicesStarted) {
            return;
        }

        if (!mBootCompleted) {
            // check to see if maybe it was already completed long before we began
            // see ActivityManagerService.finishBooting()
            if ("1".equals(SystemProperties.get("sys.boot_completed"))) {
                mBootCompleted = true;
                if (DEBUG) Log.v(TAG, "BOOT_COMPLETED was already sent");
            }
        }

        Log.v(TAG, "Starting SystemUI services.");
        final int N = SERVICES.length;
        for (int i=0; i<N; i++) {
            Class<?> cl = SERVICES[i];
            if (DEBUG) Log.d(TAG, "loading: " + cl);
            try {
                mServices[i] = (SystemUI)cl.newInstance();
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            } catch (InstantiationException ex) {
                throw new RuntimeException(ex);
            }
            mServices[i].mContext = this;
            mServices[i].mComponents = mComponents;
            if (DEBUG) Log.d(TAG, "running: " + mServices[i]);
            mServices[i].start();

            if (mBootCompleted) {
                mServices[i].onBootCompleted();
            }
        }
        mServicesStarted = true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (mServicesStarted) {
            int len = mServices.length;
            for (int i = 0; i < len; i++) {
                mServices[i].onConfigurationChanged(newConfig);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T getComponent(Class<T> interfaceType) {
        return (T) mComponents.get(interfaceType);
    }

    public SystemUI[] getServices() {
        return mServices;
    }
}
