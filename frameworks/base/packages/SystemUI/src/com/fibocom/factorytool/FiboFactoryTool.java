// Bevis added for displaying wifi only ui in 20170629 ++
package com.fibocom.factorytool;

import android.util.Log;

public class FiboFactoryTool {

    public String getBasebandVersion() {
        return getBaseband();
    }

    public native String getBaseband();

    static {
        System.loadLibrary("factorytool");
    }
}
// Bevis added for displaying wifi only ui in 20170629 --
