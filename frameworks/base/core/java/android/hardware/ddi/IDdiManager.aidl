

package android.hardware.ddi;

interface IDdiManager {
	
	int ddiMagcardOpen();
	int ddiMagcardClose ();
	int ddiMagcardClear();
	int ddiMagcardRead(out byte[] lpTrack1,out byte[] lpTrack2,out byte[] lpTrack3);
	int ddiMagcardIoctl(in int nCmd,in int lParam,in int wParam);
	int ddiMagcardIoctlGetVersion(out byte[] wParam);
	
	int ddiIccpsamOpen(in int nSlot);
	int ddiIccpsamClose(in int nSlot);
	int ddiIccpsamPoweron(in int nSlot,out byte[] lpAtr);
	int ddiIccpsamPoweroff(in int nSlot);
	int ddiIccpsamGetStatus(in int nSlot);
	int ddiIccpsamExchangeApdu(in int nSlot,in byte[] lpCApdu, in int lpCApduLen,out byte[] lpRApdu,
			out int[] lpRApduLen,in int lpRApduSize);
	
	
	int ddiRfOpen();
	int ddiRfClose();
	int ddiRfPoweron(in int nType);
	int ddiRfPoweroff();
	int ddiRfGetStatus();
	int ddiRfActivate();
	int ddiRfExchangeApdu(in byte[] lpCApdu,in int lpCApduLen,out byte[] lpRApdu,out int[] lpRApduLen,
			in int lpRApduSize);
	
	
	int ddiPrintInit(in int step);
	int ddiPrintStart(in int step);
	int ddiPrintGetStatus();
	int ddiPrintPrintf(in byte[] text);
	int ddiPrintCommand(in byte[] comm,in int len);
	int ddiPrintGraphics(in int offset,in byte [] imagepath);
	int ddiPrintMatrix(in int offset,in int w,in int h,in byte[] data,in int nlen);
	int ddiPrintSetGray(in int len);
	
	int ddiInnerkeyOpen();
	int ddiInnerkeyClose();
	int ddiInnerkeyInject(in int nKeyArea,in int nIndex,in byte[] lpKeyData);
	int ddiInnerkeyEncrypt(in int nKeyArea,in int nIndex,in int nLen,in byte[] lpIn,out byte[] lpOut);
	int ddiInnerkeyDecrypt(in int nKeyArea,in int nIndex,in int nLen,in byte[] lpIn,out byte[] lpOut);
	
	
	int ddiLedOpen();
	int ddiLedClose();
	int ddiLedSetStatus(in int nLed,in int nSta);
	//int ddiLedIoctlGleam(strLedGleamPara Para)
	
	int ddiScannOpen();
    void ddiScannClose();
    void ddiScannPowerOn();
    void ddiScannPowerOff();
    void ddiScannCode();
    void ddiScannWriteCommd(in byte[] commd);
    void ddiScannContinuousCode(in int time);
    void ddiScannStopContinuousCode();
    byte[] ddiScannReadData(in int timeout);
	
	int ddiSysGetFirmwarever(out byte[] lpOut, in int nType);
	
	int ddiSysSetBeep(in int nTime);
	int ddiSysBeepCtrl(int o1,int o2,int o3);
	
	int ddiVerifyPublickey(in byte[] data,in int len);
	int ddiPkVerifyHash(in int len,in byte[] data);
	int ddiGetPemFile(out byte[] data);
	

	int ddiSecurityUnlock(in byte[] password);
	int ddiSysGetConfig(out byte[] data1,out int [] data2);
	void ddiDdiSysUnInit();
}









































