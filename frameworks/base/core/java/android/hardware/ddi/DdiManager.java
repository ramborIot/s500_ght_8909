

package android.hardware.ddi;


import android.content.Context;
import android.util.Log;
import android.os.RemoteException;
import android.os.Binder;
import android.os.IBinder;
import android.os.ServiceManager;

public class DdiManager {
	private static final String TAG = "DdiManager";
	final IDdiManager mService;
	final Context mContext;
	private static DdiManager sInstance; 
	
	public DdiManager(Context context, IDdiManager service) {
		mService = service;
		mContext = context;
	}

	public DdiManager(IDdiManager service) {
		mService = service;
		mContext = null;
	}


	public static DdiManager getInstance() {
		synchronized(DdiManager.class) {
			if (sInstance == null) {
				IBinder b = ServiceManager.getService(Context.DDI_SERVICE);
				sInstance = new DdiManager(IDdiManager.Stub.asInterface(b));
			}
		}
		return sInstance;
	} 


	public int ddiMagcardOpen() {
		try {
			return mService.ddiMagcardOpen();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not open Magcard.", e);
			return -1;
		}
	}

	public int ddiMagcardClose() {
		try {
			return mService.ddiMagcardClose();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Close Magcard.", e);
			return -1;
		}
	}

	public int ddiMagcardClear() {
		try {
			return mService.ddiMagcardClear();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Clear Magcard.", e);
			return -1;
		}
	}

	public int ddiMagcardRead(byte[] lpTrack1, byte[] lpTrack2, byte[] lpTrack3) {
		try {
			return mService.ddiMagcardRead(lpTrack1, lpTrack2, lpTrack3);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Read Magcard.", e);
			return -1;
		}
	}

	public int ddiMagcardIoctl(int nCmd, int lParam, int wParam) {
		try {
			return mService.ddiMagcardIoctl(nCmd, lParam, wParam);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Ioctl Magcard.", e);
			return -1;
		}
	}

	public int ddiMagcardIoctlGetVersion(byte[] wParam) {
		try {
			return mService.ddiMagcardIoctlGetVersion(wParam);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not get  Magcard  version.", e);
			return -1;
		}
	}


	public int ddiIccpsamOpen(int nSlot) {
		try {
			return mService.ddiIccpsamOpen(nSlot);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not open Iccpsam.", e);
			return -1;
		}
	}

	public int ddiIccpsamClose(int nSlot) {
		try {
			return mService.ddiIccpsamClose(nSlot);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Close Iccpsam.", e);
			return -1;
		}
	}

	public int ddiIccpsamPoweron(int nSlot,byte[] lpAtr) {
		try {
			return mService.ddiIccpsamPoweron(nSlot, lpAtr);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Poweron Iccpsam.", e);
			return -1;
		}
	}

	public int ddiIccpsamPoweroff(int nSlot) {
		try {
			return mService.ddiIccpsamPoweroff(nSlot);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not get  Magcard  version.", e);
			return -1;
		}
	}

	public int ddiIccpsamGetStatus(int nSlot) {
		try {
			return mService.ddiIccpsamGetStatus(nSlot);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not get  Iccpsam  Status.", e);
			return -1;
		}
	}

	public int ddiIccpsamExchangeApdu(int nSlot, byte[] lpCApdu, int lpCApduLen, byte[] lpRApdu,
				int[] lpRApduLen, int lpRApduSize) {
		try {
			return mService. ddiIccpsamExchangeApdu(nSlot, lpCApdu, lpCApduLen, lpRApdu,
				  lpRApduLen, lpRApduSize);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Exchange Apdu Iccpsam.", e);
			return -1;
		}
	}

	public int ddiRfOpen() {
		try {
			return mService.ddiRfOpen();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not open Rf.", e);
			return -1;
		}
	}

	public int ddiRfClose() {
		try {
			return mService.ddiRfClose();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not close  rf.", e);
			return -1;
		}
	}

	public int ddiRfPoweron(int nType) {
		try {
			return mService.ddiRfPoweron(nType);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Poweron  rf.", e);
			return -1;
		}
	}

	public int ddiRfPoweroff() {
		try {
			return mService.ddiRfPoweroff();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Poweroff  rf.", e);
			return -1;
		}
	}

	public int ddiRfGetStatus() {
		try {
			return mService.ddiRfGetStatus();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Get rf Status.", e);
			return -1;
		}
	}

	public int ddiRfActivate() {
		try {
			return mService.ddiRfActivate();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Activate  rf.", e);
			return -1;
		}
	}

	public int ddiRfExchangeApdu(byte[] lpCApdu,int lpCApduLen,byte[] lpRApdu,int[] lpRApduLen,
				int lpRApduSize) {
		try {
			return mService.ddiRfExchangeApdu(lpCApdu, lpCApduLen, lpRApdu, lpRApduLen, lpRApduSize);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Exchange  rf.", e);
			return -1;
		}
	}

	public int ddiPrintInit(int step) {
		try {
			return mService.ddiPrintInit(step);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Init Print.", e);
			return -1;
		}
	}

	public int ddiPrintStart(int step) {
		try {
			return mService.ddiPrintStart(step);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Start Print .", e);
			return -1;
		}
	}

	public int ddiPrintGetStatus() {
		try {
			return mService.ddiPrintGetStatus();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not get print status.", e);
			return -1;
		}
	}

	public int ddiPrintPrintf(byte[] text) {
		try {
			return mService.ddiPrintPrintf(text);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Printff.", e);
			return -1;
		}
	}

	public int ddiPrintCommand(byte[] comm,int len) {
		try {
			return mService.ddiPrintCommand(comm, len);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Print Command.", e);
			return -1;
		}
	}


	public int ddiPrintGraphics(int offset, byte[] imagepath) {
		try {
			return mService.ddiPrintGraphics(offset, imagepath);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Print Graphics.", e);
			return -1;
		}
	}

	public int ddiPrintMatrix(int offset,int w,int h,byte[] data,int nlen) {
		try {
			return mService.ddiPrintMatrix(offset, w, h, data, nlen);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Printff.", e);
			return -1;
		}
	}

	public int ddiPrintSetGray(int len) {
		try {
			return mService.ddiPrintSetGray(len);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Set print gray.", e);
			return -1;
		}
	}


	
	
	public int ddiInnerkeyOpen() {
		try {
			return mService.ddiInnerkeyOpen();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Open Innerkey .", e);
			return -1;
		}
	}


	public int ddiInnerkeyClose() {
		try {
			return mService.ddiInnerkeyClose();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Close Innerkey.", e);
			return -1;
		}
	}


	public int ddiInnerkeyInject(int nKeyArea,int nIndex,byte[] lpKeyData) {
		try {
			return mService.ddiInnerkeyInject(nKeyArea, nIndex, lpKeyData);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Inject Innerkey.", e);
			return -1;
		}
	}

	public int ddiInnerkeyEncrypt(int nKeyArea,int nIndex,int nLen,byte[] lpIn,byte[] lpOut) {
		try {
			return mService.ddiInnerkeyEncrypt(nKeyArea, nIndex, nLen, lpIn, lpOut);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Encrypt Innerkey.", e);
			return -1;
		}
	}


	public int ddiInnerkeyDecrypt(int nKeyArea,int nIndex,int nLen,byte[] lpIn,byte[] lpOut) {
		try {
			return mService.ddiInnerkeyDecrypt(nKeyArea, nIndex, nLen, lpIn, lpOut);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Decrypt Innerkey.", e);
			return -1;
		}
	}


	public int ddiLedOpen() {
		try {
			return mService.ddiLedOpen();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Open Led.", e);
			return -1;
		}
	}


	public int ddiLedClose() {
		try {
			return mService.ddiLedClose();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Close Led .", e);
			return -1;
		}
	}


	public int ddiLedSetStatus(int nLed,int nSta) {
		try {
			return mService.ddiLedSetStatus(nLed, nSta);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Set Led Status.", e);
			return -1;
		}
	}



	public int ddiScannOpen() {
		try {
			return mService.ddiScannOpen();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Open Scann.", e);
			return -1;
		}
	}


	public void ddiScannClose() {
		try {
			mService.ddiScannClose();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not close Scann.", e);
		}
	}

	public void ddiScannPowerOn() {
		try {
			mService.ddiScannPowerOn();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not PowerOn Scann.", e);
		}
	}

	public void ddiScannPowerOff() {
		try {
			mService.ddiScannPowerOff();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not PowerOff Scann.", e);
		}
	}

	public void ddiScannCode() {
		try {
			mService.ddiScannCode();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Code Scann.", e);
		}
	}

	public void ddiScannWriteCommd(byte[] commd) {
		try {
			mService.ddiScannWriteCommd(commd);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Write Commd on scann.", e);
		}
	}

	public void ddiScannContinuousCode(int time) {
		try {
			mService.ddiScannContinuousCode(time);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Continuous Scann Code.", e);
		}
	}
	
	
	public void ddiScannStopContinuousCode() {
		try {
			mService.ddiScannStopContinuousCode();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not stop Continuous Scann Code.", e);
		}
	}
	
	
	public byte[] ddiScannReadData(int timeout) {
		try {
			return mService.ddiScannReadData(timeout);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Read Scann Data.", e);
			return null;
		}
	}


	public int ddiSysSetBeep(int nTime) {
		try {
			return mService.ddiSysSetBeep(nTime);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Set Beep.", e);
			return -1;
		}
	}

	public int ddiSysBeepCtrl(int o1,int o2,int o3) {
		try {
			return mService.ddiSysBeepCtrl(o1, o2, o3);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not Ctrl Beep.", e);
			return -1;
		}
	}


	public int ddiVerifyPublickey(byte[] data, int len) {
		try {
			return mService.ddiVerifyPublickey(data, len);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not verify public key", e);
			return -1;
		}
	}

		public int ddiPkVerifyHash( int len,byte[] data) {
		try {
			return mService.ddiPkVerifyHash(len,data);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not verify public key hash!", e);
			return -1;
		}
	}


	public int ddiGetPemFile(byte[] data) {
		try {
			return mService.ddiGetPemFile(data);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not get pem file", e);
			return -1;
		}
	}

	public int ddiSecurityUnlock(byte[] password) {
		try {
			return mService.ddiSecurityUnlock(password);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not unlock security.", e);
			return -1;
		}
	}

	public int ddiSysGetFirmwarever(byte[] lpOut, int nType) {
		try {
			return mService.ddiSysGetFirmwarever(lpOut, nType);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not get Firmwarever.", e);
			return -1;
		}
	}
	
	public int ddiSysGetConfig(byte[] data1,int [] data2){
		try {
			return mService.ddiSysGetConfig(data1, data2);
		} catch (RemoteException e) {
			Log.e(TAG, "Could not get Firmwarever.", e);
			return -1;
		}
	}
	
	public void ddiDdiSysUnInit(){
		try {
			 mService.ddiDdiSysUnInit();
		} catch (RemoteException e) {
			Log.e(TAG, "Could not get ddiDdiSysUnInit.", e);
			
		}
	}

	
}









































