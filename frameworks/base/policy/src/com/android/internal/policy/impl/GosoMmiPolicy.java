
package com.android.internal.policy.impl;

import java.util.HashMap;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;

public class GosoMmiPolicy {

    public static final String TAG = GosoMmiPolicy.class.getSimpleName();

    private static GosoMmiPolicy policy;

    public static GosoMmiPolicy getInstance(Context context) {
        if (policy == null) {
            policy = new GosoMmiPolicy(context);
        }
        return policy;
    }

    //if setting 0 false,1 true
    public static final String KEY_IS_FTM = "key_is_ftm";
    public static final String KEY_POWER_DISABLE = "key_power_disable";
    public static final String KEY_HOME_DISABLE = "key_home_disable";

    private Context mContext;
    private SettingsObserver mObserver;
    private ContentResolver mResolver;
    private boolean mInited = false;

    private BoolObj mIsFtm = new BoolObj(false);
    private BoolObj mKeyPowerDisable = new BoolObj(false);
    private BoolObj mKeyHomeDisable = new BoolObj(false);

    private HashMap<Integer, BoolObj> mDisableKeyMap = new HashMap<Integer, BoolObj>();

    class BoolObj {
        boolean value = false;

        public BoolObj(boolean b) {
            value = b;
        }
    }

    class SettingsObserver extends ContentObserver {
        SettingsObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            updateSettings();
        }
    }

    private GosoMmiPolicy(Context context) {
        mContext = context;
        mResolver = mContext.getContentResolver();

        mDisableKeyMap.put(KeyEvent.KEYCODE_POWER, mKeyPowerDisable);
        mDisableKeyMap.put(KeyEvent.KEYCODE_HOME, mKeyHomeDisable);
        mDisableKeyMap.put(KeyEvent.KEYCODE_MENU, mIsFtm);
        mDisableKeyMap.put(KeyEvent.KEYCODE_APP_SWITCH, mIsFtm);
        mDisableKeyMap.put(KeyEvent.KEYCODE_SEARCH, mIsFtm);
        mDisableKeyMap.put(KeyEvent.KEYCODE_CALL, mIsFtm);
        mDisableKeyMap.put(KeyEvent.KEYCODE_ENDCALL, mIsFtm);
        mDisableKeyMap.put(KeyEvent.KEYCODE_DPAD_UP, mIsFtm);
        mDisableKeyMap.put(KeyEvent.KEYCODE_DPAD_DOWN, mIsFtm);
        mDisableKeyMap.put(KeyEvent.KEYCODE_DPAD_LEFT, mIsFtm);
        mDisableKeyMap.put(KeyEvent.KEYCODE_DPAD_RIGHT, mIsFtm);
        mDisableKeyMap.put(KeyEvent.KEYCODE_CAMERA, mIsFtm);
        //mDisableKeyMap.put(KeyEvent.KEYCODE_SLEEP, mIsFtm);
        //mDisableKeyMap.put(KeyEvent.KEYCODE_WAKEUP, mIsFtm);
    }

    public synchronized void init(Handler handler) {
        if (!mInited) {//just init once.
            mInited = true;
            updateSettings();

            Log.i(TAG, "init: reset FTM.");
            setFtm(false); //reset mmi settings when boot up.

            Log.i(TAG, "init: register settings observer.");
            mObserver = new SettingsObserver(handler);
            mResolver.registerContentObserver(
                    Settings.System.getUriFor(KEY_IS_FTM), false, mObserver);
            mResolver.registerContentObserver(
                    Settings.System.getUriFor(KEY_POWER_DISABLE), false, mObserver);
            mResolver.registerContentObserver(
                    Settings.System.getUriFor(KEY_HOME_DISABLE), false, mObserver);
        }
    }

    private void updateSettings() {
        Log.i(TAG, "updateSettings");
        boolean isFtm = (Settings.System.getInt(mResolver, KEY_IS_FTM, 0) == 1);
        mKeyPowerDisable.value = (Settings.System.getInt(mResolver, KEY_POWER_DISABLE, 0) == 1);
        mKeyHomeDisable.value = (Settings.System.getInt(mResolver, KEY_HOME_DISABLE, 0) == 1);

        Log.i(TAG, "isFtm=" + isFtm);
        Log.i(TAG, "mKeyPowerDisable=" + mKeyPowerDisable.value);
        Log.i(TAG, "mKeyHomeDisable=" + mKeyHomeDisable.value);

        if (isFtm != mIsFtm.value) {
            Log.i(TAG, "FTM setting had be changed: " + mIsFtm.value + "->" + isFtm);
            mIsFtm.value = isFtm;
            setPowerKeyDisable(isFtm);
            setHomeKeyDisable(isFtm);
        }
    }

    public static boolean isSystemKeyDisabled(ContentResolver resolver, int keyCode) {
        if (KeyEvent.isSystemKey(keyCode)) {
            return isFtm_(resolver);
        }
        return false;
    }

    public static boolean isFtm_(ContentResolver resolver) {
        return (Settings.System.getInt(resolver, KEY_IS_FTM, 0) == 1);
    }

    public boolean isFtmKeyDisabled(int keyCode) {
        BoolObj value = mDisableKeyMap.get(keyCode);
        boolean disabled = value != null ? value.value : false;
        //Log.d(TAG, "keyCode: " + keyCode + ";disabled: " + disabled);
        return disabled;
    }

    public void setFtmKeyDisabled(int keyCode, boolean disabled) {
        if (keyCode == KeyEvent.KEYCODE_POWER) {
            setPowerKeyDisable(disabled);
        } else if (keyCode == KeyEvent.KEYCODE_HOME) {
            setHomeKeyDisable(disabled);
        } else {
            //TODO add other key disable function.
        }
    }

    public boolean isFtm() {
        return mIsFtm.value;
    }

    public void setFtm(boolean enable) {
        if (mIsFtm.value != enable) {
            mIsFtm.value = enable;
            Log.i(TAG, "setFtm: " + mIsFtm.value);
            Settings.System.putInt(mResolver, KEY_IS_FTM, enable ? 1 : 0);
            setPowerKeyDisable(enable);
            setHomeKeyDisable(enable);
        }
    }

    public boolean isPowerKeyDisable() {
        return mKeyPowerDisable.value;
    }

    public void setPowerKeyDisable(boolean disable) {
        if (mKeyPowerDisable.value != disable) {
            mKeyPowerDisable.value = disable;
            Log.i(TAG, "setPowerKeyDisable: " + mKeyPowerDisable.value);
            Settings.System.putInt(
                    mResolver, KEY_POWER_DISABLE, disable ? 1 : 0);
        }
    }

    public boolean isHomeKeyDisable() {
        return mKeyHomeDisable.value;
    }

    public void setHomeKeyDisable(boolean disable) {
        if (mKeyHomeDisable.value != disable) {
            mKeyHomeDisable.value = disable;
            Log.i(TAG, "setHomeKeyDisable: " + mKeyHomeDisable.value);
            Settings.System.putInt(
                    mResolver, KEY_HOME_DISABLE, disable ? 1 : 0);
        }
    }

}
