/*
 * pop800_charger.c - Power supply consumer driver for the 
 *
 *  Copyright (C) 2011 MeigSmart
 *  Zouwubin <zouwubin@meigsmart.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  */

#include <linux/debugfs.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/of_gpio.h>
#include <linux/pinctrl/consumer.h>
#include <linux/platform_device.h>
#include <linux/power_supply.h>
#include <linux/qpnp/qpnp-adc.h>
#include <linux/slab.h>
#include <linux/usb/otg.h>

#define POP800_DEBUG
#if defined(POP800_DEBUG)
#define pop800_err(...) dev_err(__VA_ARGS__)
#define pop800_dbg(...) dev_dbg(__VA_ARGS__)
#define pop800_pr_err(...) pr_err(__VA_ARGS__)
#else
#define pop800_err(...) 
#define pop800_dbg(...) 
#define pop800_pr_err(...)
#endif

#define GPIO_MAX 6
#define GPIO_INT_MAX 3
#define GPIO_CHG_USB_IN 0
#define GPIO_CHG_DONE 1
#define GPIO_CHG_TYPE 2

#define GPIO_GPIO_MAX 3
#define GPIO_CHG_3V3 3
#define GPIO_CHG_5V 4
#define GPIO_CHG_LEDR 5

struct charger_data {
	struct device *dev;
	struct power_supply	*usb_psy;
	struct power_supply *bms_psy;
	struct power_supply	battery;
	struct power_supply	wall;
	struct pinctrl *p;
	const char	*bms_psy_name;
	unsigned int charge_volt;
	unsigned int charge_soc;
	unsigned int charge_state;
	int	chg_gpios[GPIO_MAX];
	u32	chg_gpios_flag[GPIO_MAX];
	u32	chg_gpios_val[GPIO_MAX];
	int	chg_irqs[GPIO_INT_MAX];
	struct pop800_dev *iodev;
	struct qpnp_vadc_chip	*vadc_dev;
	struct delayed_work		init_interrupts_work;
	struct mutex			irq_complete;
	bool				resume_completed;
};

struct charger_data *pop800_chip = NULL;

const char* irq_gpio_name[GPIO_MAX] = {
	"pop800_charge_type",
	"pop800_charge_usb_in",
	"pop800_charge_done"
};

static enum power_supply_property pop800_battery_props[] = {
	POWER_SUPPLY_PROP_STATUS,
	POWER_SUPPLY_PROP_VOLTAGE_NOW,
	POWER_SUPPLY_PROP_CAPACITY,
	POWER_SUPPLY_PROP_CAPACITY_LEVEL,
	POWER_SUPPLY_PROP_TEMP,
	POWER_SUPPLY_PROP_TECHNOLOGY,
};

#define DEFAULT_BATT_CAPACITY	50

static int pop800_curr_capacity = 0;

static enum power_supply_property mps26123_charger_props[] = {
	POWER_SUPPLY_PROP_ONLINE,
};


static bool mps26123_charger_is_present(struct charger_data *charger)
{
	if ( (charger->chg_gpios_val[GPIO_CHG_DONE] == 0) &&(charger->chg_gpios_val[GPIO_CHG_TYPE] == 0)) {
		return true;	
	}
	else if( (charger->chg_gpios_val[GPIO_CHG_DONE] == 1) &&(charger->chg_gpios_val[GPIO_CHG_TYPE] == 0)){
		return false;
	}
	else {
		return false;

	}

}


static int mps26123_charger_get_property(struct power_supply *psy,
					enum power_supply_property psp,
					union power_supply_propval *val)
{
	struct charger_data *charger = container_of(psy,
			struct charger_data, wall);

	switch (psp) {
	case POWER_SUPPLY_PROP_ONLINE:
		val->intval = mps26123_charger_is_present(charger) ? 1 : 0;
		break;
	default:
		return -EINVAL;
	}

	return 0;
}
static int pop800_get_chg_state(struct charger_data *charger)
{
	union power_supply_propval prop = {0,};
	union power_supply_propval prop_present = {0,};
	int rc;


	//charger->chg_gpios_val[GPIO_CHG_USB_IN] = gpio_get_value(charger->chg_gpios[GPIO_CHG_USB_IN]);
	charger->chg_gpios_val[GPIO_CHG_DONE] = gpio_get_value(charger->chg_gpios[GPIO_CHG_DONE]);
	charger->chg_gpios_val[GPIO_CHG_TYPE] = gpio_get_value(charger->chg_gpios[GPIO_CHG_TYPE]);
	//pop800_err(charger->dev, "1:Not connect with usb; 0: Connect with usb. Interrupt gpio\n");
	//pop800_err(charger->dev, "pop800_get_chg_state   type is = %d\n", charger->chg_gpios_val[GPIO_CHG_TYPE]);
	//pop800_err(charger->dev, "1: AC charger; 0: Computer charge\n");
	//pop800_err(charger->dev, "pop800_get_chg_state   done is = %d\n", charger->chg_gpios_val[GPIO_CHG_DONE]);
	//pop800_err(charger->dev, "1: Not full ; 0: Full.  Interrupt gpio\n");

	rc = charger->usb_psy->get_property(charger->usb_psy,
				POWER_SUPPLY_PROP_ONLINE, &prop);
	if (rc < 0)
		pr_err("could not read USB ONLINE property, rc=%d\n", rc);
	
	rc = charger->usb_psy->get_property(charger->usb_psy,
				POWER_SUPPLY_PROP_PRESENT, &prop_present);
	if (rc < 0)
		pr_err("could not read USB ONLINE property, rc=%d\n", rc);

	pop800_err(charger->dev, "pop800_get_chg_state prop.intval= %d, prop_present.intval=%d,  usb in is = %d,  type is = %d,  done is = %d\n", 
		prop.intval, prop_present.intval,
		charger->chg_gpios_val[GPIO_CHG_USB_IN], charger->chg_gpios_val[GPIO_CHG_TYPE] ,charger->chg_gpios_val[GPIO_CHG_DONE]);
	
	//pop800_err(charger->dev, "pop800_get_chg_state  prop.intval= %d, prop_present.intval=%d\n", prop.intval,  prop_present.intval);

	//rc = get_charger_type_from_otg();
	//pop800_err(charger->dev,  "get_charger_type_from_otg()-------------rc=%d",rc);


	#if 1
	if ( (charger->chg_gpios_val[GPIO_CHG_DONE] == 0) &&(charger->chg_gpios_val[GPIO_CHG_TYPE] == 0)) {
		charger->charge_state = POWER_SUPPLY_STATUS_CHARGING;
		if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_5V]))
			gpio_direction_output(charger->chg_gpios[GPIO_CHG_5V],0);
		if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_LEDR]))
			gpio_direction_output(charger->chg_gpios[GPIO_CHG_LEDR],1);
		if (prop.intval == 0){
		     //rc = power_supply_set_online(charger->usb_psy, true);
		    // rc = power_supply_set_present(charger->usb_psy, true);
			}
	}
	else if( (charger->chg_gpios_val[GPIO_CHG_DONE] == 1) &&(charger->chg_gpios_val[GPIO_CHG_TYPE] == 0)){
		charger->charge_state = POWER_SUPPLY_STATUS_FULL;
		if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_LEDR]))
			gpio_direction_output(charger->chg_gpios[GPIO_CHG_LEDR],0);
		if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_5V]))
			gpio_direction_output(charger->chg_gpios[GPIO_CHG_5V],1);
		if (prop.intval == 1){
		     //rc = power_supply_set_online(charger->usb_psy, false);
		     //rc = power_supply_set_present(charger->usb_psy, false);
			}
	}
	else {
		if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_LEDR]))
			gpio_direction_output(charger->chg_gpios[GPIO_CHG_LEDR],0);
		if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_5V]))
			gpio_direction_output(charger->chg_gpios[GPIO_CHG_5V],0);
		charger->charge_state = POWER_SUPPLY_STATUS_NOT_CHARGING;
		if (prop.intval == 1){
		     //rc = power_supply_set_online(charger->usb_psy, false);
		     //rc = power_supply_set_present(charger->usb_psy, false);
			}
	}

	#endif

//if ((prop.intval == 1)&(prop_present.intval == 1))
	//pop800_err(charger->dev, "pop800_get_chg_state is = %d\n", charger->charge_state);
	return charger->charge_state;
}

#if 0
int pop800_get_prop_battery_voltage(struct charger_data *chip)
{
	int rc = 0;
	struct qpnp_vadc_result results;

	rc = qpnp_vadc_read(chip->vadc_dev, VBAT_SNS, &results);//VBAT_SNS
	pop800_err(chip->dev, "pop800_get_prop_battery_voltage  results.physical  is = %lld\n", results.physical);
	
	if (rc) {
		pop800_err(chip->dev, "Unable to read VBAT_SNS rc=%d\n", rc);
		return 0;
	} else {
		return results.physical;
	}
}
#else
int pop800_get_prop_battery_voltage(struct charger_data *chip)
{
	union power_supply_propval ret = {0, };

	if (chip->bms_psy) {
		chip->bms_psy->get_property(chip->bms_psy, POWER_SUPPLY_PROP_VOLTAGE_NOW, &ret);
		printk(KERN_ERR "xingkun=====voltage:%d\n",ret.intval);
		return ret.intval;
	}
	return 4000000;
}	
#endif

int pop800_get_prop_batt_capacity_level(struct charger_data *chip)
{
	union power_supply_propval ret = {0, };

	if (chip->bms_psy) {
		chip->bms_psy->get_property(chip->bms_psy, POWER_SUPPLY_PROP_CAPACITY_LEVEL, &ret);
		printk(KERN_ERR "xingkun=====capacity level:%d\n",ret.intval);
		return ret.intval;
	}
	return 3;
}

static int is_battery_present(struct charger_data *chip)
{
#if 0
	int rc,result_uv;
	struct qpnp_vadc_result adc_result;

	rc = qpnp_vadc_read(chip->vadc_dev , VBAT_SNS, &adc_result);//VBAT_SNS [bugid:3804]  lidan modified for jiulei s500 charge ic charge
	if (rc) {
		pop800_err(pop800_chip->dev,"xingkun===error reading adc channel = %d, rc = %d\n",VBAT_SNS, rc);
		return rc;
	}
	pop800_err(pop800_chip->dev,"xingkun===mvolts phy=%lld meas=0x%llx\n", adc_result.physical,
						adc_result.measurement);
	result_uv = (int)adc_result.physical;

	return 1;
#else
union power_supply_propval ret;

if (chip->bms_psy) {
	chip->bms_psy->get_property(chip->bms_psy,
			POWER_SUPPLY_PROP_PRESENT, &ret);
	return ret.intval;
}
else {
	if (chip->bms_psy_name) {
		chip->bms_psy = power_supply_get_by_name((char *)chip->bms_psy_name);
		if (chip->bms_psy) {
			chip->bms_psy->get_property(chip->bms_psy,
					POWER_SUPPLY_PROP_PRESENT, &ret);
			return ret.intval;
		}
	}

}
return 1;

#endif
}

static int pop800_get_prop_batt_temp(struct charger_data *chip)
{
	union power_supply_propval ret = {0, };

	if (chip->bms_psy) {
		chip->bms_psy->get_property(chip->bms_psy, POWER_SUPPLY_PROP_TEMP, &ret);
		//printk(KERN_ERR "xingkun=====temp:%d\n",ret.intval);
		return ret.intval;
	}
	return 200;
}

static int pop800_get_prop_batt_capacity(struct charger_data *chip)
{
	union power_supply_propval ret = {0, };
	int rc = 0;
	//pop800_err(chip->dev, "pop800_get_prop_batt_capacity\n");

    rc = pop800_get_chg_state(chip);
    if (POWER_SUPPLY_STATUS_FULL == rc ||!is_battery_present(chip))
    {
		//pop800_err(chip->dev, "pop800_get_prop_batt_capacity full\n");
		pop800_curr_capacity = 100;
		return pop800_curr_capacity;
    }
	if (chip->bms_psy) {
		//pop800_err(chip->dev, "pop800 get bms_psy capacity\n");
		chip->bms_psy->get_property(chip->bms_psy,
				POWER_SUPPLY_PROP_CAPACITY, &ret);
		pop800_curr_capacity = ret.intval;
		return ret.intval;
	}
	else {
		if (chip->bms_psy_name) {
			chip->bms_psy = power_supply_get_by_name((char *)chip->bms_psy_name);
			if (chip->bms_psy) {
				//pop800_err(chip->dev, "pop800 get bms_psy capacity check again\n");
				chip->bms_psy->get_property(chip->bms_psy,
						POWER_SUPPLY_PROP_CAPACITY, &ret);
				pop800_curr_capacity = ret.intval;
				return ret.intval;
			}
		}
		if (pop800_curr_capacity == 0) {
			pop800_curr_capacity = DEFAULT_BATT_CAPACITY;
		}
	}
	return pop800_curr_capacity;
}
static int pop800_get_prop_batt_present(struct charger_data *chip)
{
	//chip->chg_gpios_val[GPIO_CHG_USB_IN] = gpio_get_value(chip->chg_gpios[GPIO_CHG_USB_IN]);
	//pop800_err(chip->dev, "lidan pop800_get_prop_battery_chip->chg_gpios_val[GPIO_CHG_USB_IN] = %d\n", chip->chg_gpios_val[GPIO_CHG_USB_IN]);
	//return chip->chg_gpios_val[GPIO_CHG_USB_IN]
	return is_battery_present(chip);
}

static int pop800_get_prop_charge_type(struct charger_data *charger)
{

	//charger->chg_gpios_val[GPIO_CHG_USB_IN] = gpio_get_value(charger->chg_gpios[GPIO_CHG_USB_IN]);
	charger->chg_gpios_val[GPIO_CHG_DONE] = gpio_get_value(charger->chg_gpios[GPIO_CHG_DONE]);
	charger->chg_gpios_val[GPIO_CHG_TYPE] = gpio_get_value(charger->chg_gpios[GPIO_CHG_TYPE]);
	//pop800_err(charger->dev, "chg_gpios_val usb in is = %d\n", charger->chg_gpios_val[GPIO_CHG_USB_IN]);
	//pop800_err(charger->dev, "1:Not connect with usb; 0: Connect with usb. Interrupt gpio\n");
	//pop800_err(charger->dev, "chg_gpios_val type is = %d\n", charger->chg_gpios_val[GPIO_CHG_TYPE]);
	//pop800_err(charger->dev, "1: AC charger; 0: Computer charge\n");
	//pop800_err(charger->dev, "chg_gpios_val done is = %d\n", charger->chg_gpios_val[GPIO_CHG_DONE]);
	//pop800_err(charger->dev, "1: Not full ; 0: Full.  Interrupt gpio\n");
	if ( (charger->chg_gpios_val[GPIO_CHG_DONE] == 0) &&(charger->chg_gpios_val[GPIO_CHG_TYPE] == 0)) {
		//charger->charge_state = POWER_SUPPLY_STATUS_CHARGING;
		return POWER_SUPPLY_CHARGE_TYPE_FAST;
	}
	else if ( (charger->chg_gpios_val[GPIO_CHG_DONE] == 1) &&(charger->chg_gpios_val[GPIO_CHG_TYPE] == 0)) {
		//charger->charge_state = POWER_SUPPLY_STATUS_FULL;
		return POWER_SUPPLY_CHARGE_TYPE_NONE;
	}
	else {
		//charger->charge_state = POWER_SUPPLY_STATUS_NOT_CHARGING;
		return POWER_SUPPLY_CHARGE_TYPE_NONE;
	}

	return POWER_SUPPLY_CHARGE_TYPE_NONE;
}

static int pop800_battery_get_property(struct power_supply *psy,
		enum power_supply_property psp,
		union power_supply_propval *val)
{
	struct charger_data *charger = container_of(psy,
			struct charger_data, battery);

	switch (psp) {
	case POWER_SUPPLY_PROP_STATUS:
		val->intval = pop800_get_chg_state(charger);
		break;
	case POWER_SUPPLY_PROP_VOLTAGE_NOW:
		val->intval = pop800_get_prop_battery_voltage(charger);
		break;
	case POWER_SUPPLY_PROP_CAPACITY:
		val->intval = pop800_get_prop_batt_capacity(charger);
		break;
	case POWER_SUPPLY_PROP_CAPACITY_LEVEL:
		val->intval = pop800_get_prop_batt_capacity_level(charger);
		break;		
	case POWER_SUPPLY_PROP_PRESENT:
		val->intval = pop800_get_prop_batt_present(charger);
		break;
	case POWER_SUPPLY_PROP_CHARGE_TYPE:
		val->intval = pop800_get_prop_charge_type(charger);
		break;
	case POWER_SUPPLY_PROP_TEMP:
		val->intval = pop800_get_prop_batt_temp(charger);
		break;
	case POWER_SUPPLY_PROP_TECHNOLOGY:
		val->intval = POWER_SUPPLY_TECHNOLOGY_LION;
		break;		
	default:
		return -ENODEV;
	}

	return 0;
}

static int pop800_battery_set_property(struct power_supply *psy,
					enum power_supply_property prop,
					const union power_supply_propval *val)
{
	struct charger_data *charger = container_of(psy,
			struct charger_data, battery);

	switch (prop) {
	case POWER_SUPPLY_PROP_STATUS:
		switch (val->intval) {
		case POWER_SUPPLY_STATUS_FULL:
			charger->charge_state = POWER_SUPPLY_STATUS_FULL;
			pop800_curr_capacity = 100;
			power_supply_changed(&charger->battery);
			break;
		case POWER_SUPPLY_STATUS_NOT_CHARGING:
		case POWER_SUPPLY_STATUS_CHARGING:
			break;
		default:
			return -EINVAL;
		}
		break;
	case POWER_SUPPLY_PROP_VOLTAGE_NOW:
		break;
	case POWER_SUPPLY_PROP_CAPACITY:
		break;
	case POWER_SUPPLY_PROP_TYPE:
		psy->type = val->intval;
		switch (psy->type) {
		case POWER_SUPPLY_TYPE_USB:
			//charger->chg_gpios_val[GPIO_CHG_TYPE] = 0;
			break;
		case POWER_SUPPLY_TYPE_USB_DCP:
		case POWER_SUPPLY_TYPE_USB_CDP:
		case POWER_SUPPLY_TYPE_USB_ACA:
			//charger->chg_gpios_val[GPIO_CHG_TYPE] = 1;
			break;
		default:
			break;
		}
		//gpio_direction_output(charger->chg_gpios[GPIO_CHG_TYPE], charger->chg_gpios_val[GPIO_CHG_TYPE]);
		//pop800_err(charger->dev, "pop800 chg_gpios_val[GPIO_CHG_TYPE] = %d\n",
		//	charger->chg_gpios_val[GPIO_CHG_TYPE]);
		break;
	default:
		return -ENODEV;
	}

	return 0;
}

int pop800_set_usb_power_supply_type(enum power_supply_type type)
{


	if (!pop800_chip)
		return 0;

	switch(type)
    {
	case POWER_SUPPLY_TYPE_USB_DCP:
	case POWER_SUPPLY_TYPE_USB_CDP:
	case POWER_SUPPLY_TYPE_USB_ACA:
	case POWER_SUPPLY_TYPE_USB:
		break;
	default:
		//pop800_chip->chg_gpios_val[GPIO_CHG_USB_IN] = 0;
		break;
	}
	//gpio_direction_output(pop800_chip->chg_gpios[GPIO_CHG_TYPE], pop800_chip->chg_gpios_val[GPIO_CHG_TYPE]);
	pop800_err(pop800_chip->dev, "pop800_set_usb_power_supply_type  pop800 chg_gpios_val[GPIO_CHG_TYPE] = %d\n",
		pop800_chip->chg_gpios_val[GPIO_CHG_TYPE]);

	return 0;
}

static int pop800_parse_dt(struct device *dev, struct charger_data *charger)
{
	struct device_node *np = dev->of_node;
	struct pinctrl_state *pin_state_default;
	int err = 0;

	charger->p = pinctrl_get(dev);
	pin_state_default = pinctrl_lookup_state(charger->p, "chg_default");
	if (IS_ERR(pin_state_default)) {
		pop800_err(charger->dev, "failed to lookup the default state\n");
		return -1;
	}
	pinctrl_select_state(charger->p, pin_state_default);
	if (err) {
		pop800_err(charger->dev, "failed to select the default state : %d\n", err);
		return err;
	}

	charger->chg_gpios[GPIO_CHG_USB_IN] = 
		of_get_named_gpio_flags(np, "chg_usb_in_gpio", 0, &charger->chg_gpios_flag[GPIO_CHG_USB_IN]);
#if 0
	if (charger->chg_gpios[GPIO_CHG_USB_IN] < 0)
		return charger->chg_gpios[GPIO_CHG_USB_IN];
#endif

	charger->chg_gpios[GPIO_CHG_DONE] = 
		of_get_named_gpio_flags(np, "chg_done_gpio", 0, &charger->chg_gpios_flag[GPIO_CHG_DONE]);
	if (charger->chg_gpios[GPIO_CHG_DONE] < 0)
		return charger->chg_gpios[GPIO_CHG_DONE];

	charger->chg_gpios[GPIO_CHG_TYPE] =
		of_get_named_gpio_flags(np, "chg_type_gpio", 0, &charger->chg_gpios_flag[GPIO_CHG_TYPE]);
	if (charger->chg_gpios[GPIO_CHG_TYPE] < 0)
		return charger->chg_gpios[GPIO_CHG_TYPE];

	charger->chg_gpios[GPIO_CHG_3V3] =
		of_get_named_gpio_flags(np, "chg_3v3_gpio", 0, &charger->chg_gpios_flag[GPIO_CHG_3V3]);
#if 0
	if (charger->chg_gpios[GPIO_CHG_3V3] < 0)
		return charger->chg_gpios[GPIO_CHG_3V3];
#endif

	charger->chg_gpios[GPIO_CHG_5V] =
		of_get_named_gpio_flags(np, "chg_5v_gpio", 0, &charger->chg_gpios_flag[GPIO_CHG_5V]);
#if 0
	if (charger->chg_gpios[GPIO_CHG_5V] < 0)
		return charger->chg_gpios[GPIO_CHG_5V];
#endif

	charger->chg_gpios[GPIO_CHG_LEDR] =
		of_get_named_gpio_flags(np, "chg_ledr_gpio", 0, &charger->chg_gpios_flag[GPIO_CHG_LEDR]);
#if 0
	if (charger->chg_gpios[GPIO_CHG_LEDR] < 0)
		return charger->chg_gpios[GPIO_CHG_LEDR];
	pop800_err(charger->dev, "xingkun====GPIO_CHG_LEDR : %d\n", charger->chg_gpios[GPIO_CHG_LEDR]);
#endif


	err = of_property_read_string(np, "qcom,bms-psy-name",
						&charger->bms_psy_name);
	if (err)
		charger->bms_psy_name = NULL;

	return err;
}

static irqreturn_t pop800_chg_stat_handler(int irq, void *dev_id)
{
	struct charger_data *charger = dev_id;
	
	//union power_supply_propval prop = {0,};
	//union power_supply_propval prop_present = {0,};
	//int rc;
	
	if (!charger->resume_completed) {
		pop800_err(charger->dev, "IRQ triggered before device-resume\n");
		//disable_irq_nosync(charger->chg_irqs[GPIO_CHG_USB_IN]);
		disable_irq_nosync(charger->chg_irqs[GPIO_CHG_DONE]);
		//mutex_unlock(&charger->irq_complete);
		return IRQ_HANDLED;
	}
	if(!is_battery_present(charger))
	{
		pop800_err(charger->dev, "xingkun disable IRQ triggered\n");
		disable_irq_nosync(charger->chg_irqs[GPIO_CHG_DONE]);
		return IRQ_HANDLED;
	}
	//pop800_err(charger->dev, " pop800_chg_stat_handler  chg_gpios %d irq is chg_irqs[GPIO_CHG_USB_IN], val is %d",
	//	charger->chg_gpios[GPIO_CHG_USB_IN], charger->chg_gpios_val[GPIO_CHG_USB_IN]);
	//pop800_err(charger->dev, "pop800_chg_stat_handler  chg_gpios %d irq is chg_irqs[GPIO_CHG_DONE], val is %d",
	//	charger->chg_gpios[GPIO_CHG_DONE], charger->chg_gpios_val[GPIO_CHG_DONE]);
	/*if (irq == charger->chg_irqs[GPIO_CHG_USB_IN]) {
		charger->chg_gpios_val[GPIO_CHG_USB_IN] = gpio_get_value(charger->chg_gpios[GPIO_CHG_USB_IN]);
		pop800_err(charger->dev, "pop800_chg_stat_handler----- chg_irqs[GPIO_CHG_USB_IN] is %d",
			charger->chg_gpios_val[GPIO_CHG_USB_IN]);
		power_supply_changed(&charger->battery);
		power_supply_changed(charger->usb_psy);	
		}
	*/
	if (irq == charger->chg_irqs[GPIO_CHG_DONE]) {
		charger->chg_gpios_val[GPIO_CHG_DONE] = gpio_get_value(charger->chg_gpios[GPIO_CHG_DONE]);
		pop800_err(charger->dev, "pop800_chg_stat_handler----- chg_irqs[GPIO_CHG_DONE] is %d",
			charger->chg_gpios_val[GPIO_CHG_DONE]);
		power_supply_changed(&charger->battery);
		//power_supply_changed(charger->usb_psy);
		power_supply_changed(&charger->wall);
	}
	if (irq == charger->chg_irqs[GPIO_CHG_TYPE]) {
		charger->chg_gpios_val[GPIO_CHG_TYPE] = gpio_get_value(charger->chg_gpios[GPIO_CHG_TYPE]);
		pop800_err(charger->dev, "pop800_chg_stat_handler----- chg_irqs[GPIO_CHG_TYPE] is %d",
			charger->chg_gpios_val[GPIO_CHG_TYPE]);
		power_supply_changed(&charger->battery);
		//power_supply_changed(charger->usb_psy);
		power_supply_changed(&charger->wall);
	}
	/*

	rc = charger->usb_psy->get_property(charger->usb_psy,
				POWER_SUPPLY_PROP_ONLINE, &prop);
	if (rc < 0)
		pr_err("could not read USB ONLINE property, rc=%d\n", rc);
	
	rc = charger->usb_psy->get_property(charger->usb_psy,
				POWER_SUPPLY_PROP_PRESENT, &prop_present);
	if (rc < 0)
		pr_err("could not read USB ONLINE property, rc=%d\n", rc);
	
	//pop800_err(charger->dev, "pop800_chg_stat_handler  prop.intval= %d, prop_present.intval=%d\n", prop.intval,  prop_present.intval);

	pop800_err(charger->dev, "pop800_chg_stat_handler  prop.intval= %d, prop_present.intval=%d, \n usb in is = %d,   type is = %d,  done is = %d\n\n", 
		prop.intval, prop_present.intval,
		charger->chg_gpios_val[GPIO_CHG_USB_IN], charger->chg_gpios_val[GPIO_CHG_TYPE],
		charger->chg_gpios_val[GPIO_CHG_DONE]);
	//pop800_err(charger->dev, "1:Not connect with usb; 0: Connect with usb. Interrupt gpio\n");
	//pop800_err(charger->dev, "pop800_chg_stat_handler   type is = %d\n", charger->chg_gpios_val[GPIO_CHG_TYPE]);
	//pop800_err(charger->dev, "1: AC charger; 0: Computer charge\n");
	//pop800_err(charger->dev, "pop800_chg_stat_handler   done is = %d\n", charger->chg_gpios_val[GPIO_CHG_DONE]);
	//pop800_err(charger->dev, "1: Not full ; 0: Full.  Interrupt gpio\n");


	if ( (charger->chg_gpios_val[GPIO_CHG_DONE] == 0) &&(charger->chg_gpios_val[GPIO_CHG_TYPE] == 0)) {
		//charger->charge_state = POWER_SUPPLY_STATUS_CHARGING;
		if (prop.intval == 0){
		     rc = power_supply_set_online(charger->usb_psy, true);
		     rc = power_supply_set_present(charger->usb_psy, true);
			}
	}
	else if( (charger->chg_gpios_val[GPIO_CHG_DONE] == 1) &&(charger->chg_gpios_val[GPIO_CHG_TYPE] == 0)){
		//charger->charge_state = POWER_SUPPLY_STATUS_FULL;
		if (prop.intval == 1){
		     rc = power_supply_set_online(charger->usb_psy, false);
		     rc = power_supply_set_present(charger->usb_psy, false);
			}
	}
	else {
		//charger->charge_state = POWER_SUPPLY_STATUS_NOT_CHARGING;
		if (prop.intval == 1){
		     rc = power_supply_set_online(charger->usb_psy, false);
		     rc = power_supply_set_present(charger->usb_psy, false);
			}
	}
*/
	return IRQ_HANDLED;
}


int pop800_init_gpio_not_int(struct charger_data *charger)
{
	int err = 0;

	if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_3V3])) {
		err = gpio_request(charger->chg_gpios[GPIO_CHG_3V3], irq_gpio_name[GPIO_CHG_3V3]);
		if (err) {
			pop800_err(charger->dev, "chg_gpios[GPIO_CHG_3V3] request failed");
			return err;
		}
		if (err) {
			pop800_err(charger->dev, "set_direction for chg_gpios[GPIO_CHG_3V3] failed\n");
			goto free_current_gpio;
		}
		
		//gpio_direction_output(charger->chg_gpios[GPIO_CHG_3V3], 0);
		//msleep(100);
		gpio_direction_output(charger->chg_gpios[GPIO_CHG_3V3], 1);
		pop800_err(charger->dev, "pop800 chg_gpios_val[GPIO_CHG_3V3] is %d\n",
			charger->chg_gpios_val[GPIO_CHG_3V3]);
		//return err;
	}

	if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_5V])) {
		err = gpio_request(charger->chg_gpios[GPIO_CHG_5V], irq_gpio_name[GPIO_CHG_5V]);
		if (err) {
			pop800_err(charger->dev, "chg_gpios[GPIO_CHG_5V] request failed");
			return err;
		}
		if (err) {
			pop800_err(charger->dev, "set_direction for chg_gpios[GPIO_CHG_5V] failed\n");
			goto free_current_gpio;
		}
		//if (get_charger_type_from_otg() < 0) {
			charger->chg_gpios_val[GPIO_CHG_5V] = 0;
		//}
		gpio_direction_output(charger->chg_gpios[GPIO_CHG_5V], charger->chg_gpios_val[GPIO_CHG_5V]);
		pop800_err(charger->dev, "pop800 chg_gpios_val[GPIO_CHG_5V] is %d\n",
			charger->chg_gpios_val[GPIO_CHG_5V]);
		//return err;
	}

	if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_LEDR])) {
		err = gpio_request(charger->chg_gpios[GPIO_CHG_LEDR], irq_gpio_name[GPIO_CHG_LEDR]);
		if (err) {
			pop800_err(charger->dev, "chg_gpios[GPIO_CHG_LEDR] request failed");
			return err;
		}
		if (err) {
			pop800_err(charger->dev, "set_direction for chg_gpios[GPIO_CHG_LEDR] failed\n");
			goto free_current_gpio;
		}
		//if (get_charger_type_from_otg() < 0) {
			charger->chg_gpios_val[GPIO_CHG_LEDR] = 0;
		//}
		gpio_direction_output(charger->chg_gpios[GPIO_CHG_LEDR], charger->chg_gpios_val[GPIO_CHG_LEDR]);
		pop800_err(charger->dev, "pop800 chg_gpios_val[GPIO_CHG_LEDR] is %d\n",
			charger->chg_gpios_val[GPIO_CHG_LEDR]);
		//return err;
	}
	return err;
	
free_current_gpio:
	if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_3V3])) {
		pop800_err(charger->dev, "free chg_gpios in pop800_init_gpio_not_int\n");
		gpio_free(charger->chg_gpios[GPIO_CHG_3V3]);
	}
	if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_5V])) {
		gpio_free(charger->chg_gpios[GPIO_CHG_5V]);
	}
	if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_LEDR])) {
		gpio_free(charger->chg_gpios[GPIO_CHG_LEDR]);
	}

	return err;
}


int pop800_init_interrupt(struct charger_data *charger, int count)
{
	int err = 0;
#if defined(POP800_DEBUG)
	int irq = 0;
#endif
     disable_irq(charger->chg_irqs[count]); 
	 
	if (gpio_is_valid(charger->chg_gpios[count])) {
		err = gpio_request(charger->chg_gpios[count], irq_gpio_name[count]);
		if (err) {
			pop800_err(charger->dev, "chg_gpios[%d] request failed", count);
			return err;
		}
		err = gpio_direction_input(charger->chg_gpios[count]);
		if (err) {
			pop800_err(charger->dev, "set_direction for chg_gpios[%d] failed\n", count);
			goto free_current_gpio;
		}
		charger->chg_gpios_val[count] = gpio_get_value(charger->chg_gpios[count]);
		charger->chg_irqs[count] = gpio_to_irq(charger->chg_gpios[count]);
		if (charger->chg_irqs[count] < 0) {
			pop800_err(charger->dev, "Invalid chg_irqs[%d] = %d\n", count, charger->chg_irqs[count]);
			goto free_current_gpio;
		}
		err = devm_request_threaded_irq(charger->dev, charger->chg_irqs[count], NULL,
				pop800_chg_stat_handler,
				IRQF_TRIGGER_FALLING | IRQF_TRIGGER_RISING | IRQF_ONESHOT,
				"pop800_chg_stat_irq", charger);
		if (err) {
			pop800_err(charger->dev, "Failed STAT irq=%d request rc = %d\n", irq, err);
			goto free_current_gpio;
		}
		//enable_irq_wake(charger->chg_irqs[count]);
		enable_irq(charger->chg_irqs[count]);
		pop800_err(charger->dev, "enable_irq_wake chg_irqs[%d] value is %d\n",
			count, charger->chg_gpios_val[count]);

		return err;
	}
free_current_gpio:
	if (gpio_is_valid(charger->chg_gpios[count])) {
		pop800_err(charger->dev, "free chg_gpios %d\n", charger->chg_gpios[count]);
		gpio_free(charger->chg_gpios[count]);
	}
	return err;
}

int pop800_init_interrupts(struct charger_data *charger)
{
	int err = 0, i;
	for (i = 0; i < GPIO_INT_MAX; i++) {
		err = pop800_init_interrupt(charger, i);
		if (err) {
			pop800_err(charger->dev, "pop800_init_interrupts err at %d\n", i);
			break;
		}
	}
	if (err) {
		//	if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_USB_IN]))
		//		gpio_free(charger->chg_gpios[GPIO_CHG_USB_IN]);
			if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_DONE]))
				gpio_free(charger->chg_gpios[GPIO_CHG_DONE]);
			if (gpio_is_valid(charger->chg_gpios[GPIO_CHG_TYPE]))
				gpio_free(charger->chg_gpios[GPIO_CHG_TYPE]);
	}
	return err;
}


static void init_interrupts_work(struct work_struct *work)
{
	struct charger_data *charger =
		container_of(work, struct charger_data,
				init_interrupts_work.work);

	/* unsuspend dc */
	pop800_init_interrupts(charger);
}

static int pop800_battery_probe(struct platform_device *pdev)
{
	int ret = 0, err;
	struct charger_data *charger;
	struct power_supply *usb_psy;

	usb_psy = power_supply_get_by_name("usb");
	if (!usb_psy) {
		pop800_dbg(&pdev->dev, "USB psy not found; deferring probe\n");
		return -EPROBE_DEFER;
	}

	pop800_err(&pdev->dev, "pop800 : enter probe\n");
	charger = devm_kzalloc(&pdev->dev, sizeof(struct charger_data), GFP_KERNEL);
	if (charger == NULL) {
		pop800_err(&pdev->dev, "Cannot allocate memory.\n");
		return -ENOMEM;
	}

	platform_set_drvdata(pdev, charger);

	charger->dev = &pdev->dev;
	charger->usb_psy = usb_psy;
	/* early for VADC get, defer probe if needed */
	charger->vadc_dev = qpnp_get_vadc(charger->dev, "chg");
	if (IS_ERR(charger->vadc_dev)) {
		ret = PTR_ERR(charger->vadc_dev);
		if (ret != -EPROBE_DEFER)
			pop800_err(&pdev->dev, "vadc property missing\n");
		return ret;
	}
	mutex_init(&charger->irq_complete);
	charger->resume_completed = true;
	
	err = pop800_parse_dt(&pdev->dev, charger);
	if (err) {
		pop800_err(charger->dev, "pop800_parse_dt failed");
		return err;
	}
	if (charger->bms_psy_name)
		charger->bms_psy = power_supply_get_by_name((char *)charger->bms_psy_name);

	INIT_DELAYED_WORK(&(charger->init_interrupts_work),
					init_interrupts_work);

	schedule_delayed_work(&(charger->init_interrupts_work),
		msecs_to_jiffies(12000));

	/* irq configuration */
	err = pop800_init_interrupts(charger);
	if (err) {
		pop800_err(&pdev->dev, "pop800_init_interrupts fail\n");
		return err;
	}
	pop800_init_gpio_not_int(charger);

	charger->battery.name = "battery";
	charger->battery.type = POWER_SUPPLY_TYPE_BATTERY;
	charger->battery.get_property = pop800_battery_get_property;
	charger->battery.set_property = pop800_battery_set_property;
	charger->battery.properties = pop800_battery_props;
	charger->battery.num_properties = ARRAY_SIZE(pop800_battery_props);

	ret = power_supply_register(&pdev->dev, &charger->battery);
	if (ret) {
		pop800_err(&pdev->dev, "pop800 failed: power supply register\n");
		return ret;
	}
	charger->wall.name = "mps26123";
	charger->wall.type = POWER_SUPPLY_TYPE_MAINS;
	charger->wall.get_property = mps26123_charger_get_property;
	//charger->wall.set_property = mps26123_charger_set_property;
	charger->wall.properties = mps26123_charger_props;
	charger->wall.num_properties = ARRAY_SIZE(mps26123_charger_props);
	ret = power_supply_register(&pdev->dev, &charger->wall);
	if (ret) {
		pop800_err(&pdev->dev, "pop800 failed: power supply register\n");
		return ret;
	}
	pop800_chip = charger;

	return 0;
}

static int pop800_battery_remove(struct platform_device *pdev)
{
	struct charger_data *charger = platform_get_drvdata(pdev);
	int i;

	for (i = 0; i < GPIO_MAX; i++) {
		if (gpio_is_valid(charger->chg_gpios[i]))
			gpio_free(charger->chg_gpios[i]);
	}

	power_supply_unregister(&charger->battery);
	mutex_destroy(&charger->irq_complete);
	return 0;
}

void pop800_disable_irq(void)
{
	//disable_irq(pop800_chip->chg_irqs[GPIO_CHG_USB_IN]);
	disable_irq(pop800_chip->chg_irqs[GPIO_CHG_DONE]);
	disable_irq(pop800_chip->chg_irqs[GPIO_CHG_TYPE]);
	
}

void pop800_enable_irq(void)
{
	//pop800_chg_stat_handler(pop800_chip->chg_irqs[GPIO_CHG_USB_IN], pop800_chip);
	//enable_irq(pop800_chip->chg_irqs[GPIO_CHG_USB_IN]);
	//pop800_chg_stat_handler(pop800_chip->chg_irqs[GPIO_CHG_DONE], pop800_chip);
	enable_irq(pop800_chip->chg_irqs[GPIO_CHG_DONE]);
	//pop800_chg_stat_handler(pop800_chip->chg_irqs[GPIO_CHG_TYPE], pop800_chip);
	enable_irq(pop800_chip->chg_irqs[GPIO_CHG_TYPE]);
}

static int pop800_suspend(struct device *dev)
{
	printk(KERN_ERR"---pop800_suspend---\n");
	printk(KERN_ERR"pop800_suspend");
	pop800_chip->resume_completed = false;	
	//mutex_lock(&pop800_chip->irq_complete);
	pop800_disable_irq();
	//mutex_unlock(&pop800_chip->irq_complete);
	return 0;
}

static int pop800_resume(struct device *dev)
{
	printk(KERN_ERR"---pop800_resume---\n");
	//mutex_lock(&pop800_chip->irq_complete);
	pop800_chip->resume_completed = true;	
	pop800_enable_irq();
	//mutex_unlock(&pop800_chip->irq_complete);
	return 0;
}

static const struct of_device_id pop800_match[] = {
	{ .compatible = "pop800,chg", },
	{ },
};

static const struct platform_device_id pop800_battery_id[] = {
	{ "pop800,chg", 0 },
	{ }
};

static const struct dev_pm_ops pop800_pm_ops = {
	.suspend	= pop800_suspend,
	.resume		= pop800_resume,
};

static struct platform_driver pop800_battery_driver = {
	.driver = {
		.name = "pop800,chg",
		.owner = THIS_MODULE,
		.of_match_table = pop800_match,
		.pm = &pop800_pm_ops,
	},
	.probe = pop800_battery_probe,
	.remove = pop800_battery_remove,
	.id_table = pop800_battery_id,
};

static int __init pop800_battery_init(void)
{
	printk(KERN_DEBUG "pop800 :%s,%d\n",__func__,__LINE__);
	return platform_driver_register(&pop800_battery_driver);
}
//subsys_initcall(pop800_battery_init);
//module_init(pop800_battery_init);
late_initcall(pop800_battery_init);


static void __exit pop800_battery_cleanup(void)
{
	platform_driver_unregister(&pop800_battery_driver);
}
module_exit(pop800_battery_cleanup);

MODULE_DESCRIPTION("pop 800 battery control driver");
MODULE_AUTHOR("Zouwubin <zouwubin@meigsmart.com>");
MODULE_LICENSE("GPL");
