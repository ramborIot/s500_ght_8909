/* Copyright (c) 2013-2014, GOSO Tec Co.,Ltd. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#define DEBUG
#define pr_fmt(fmt) "modem_state: %s,%d: " fmt, __func__, __LINE__

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/syscalls.h>
#include <linux/io.h>
#include <linux/platform_device.h>
#include <linux/gpio.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/delay.h>
#include <soc/qcom/clock-local2.h>
#include <linux/wakelock.h>

#define MODEM_STATE_DRIVER_NAME "qcom,modem-state"
#define MODEM_STATE_ID_NAME "qcom,modem-id"
#define MODEM_POWER_ON_NAME "qcom,smodem-pwr-on"
#define MODEM_POWER_OFF_NAME "qcom,smodem-pwr-off"
#define MODEM_WAKEUP_SMODEM_NAME "qcom,modem-wakeup-smodem"
#define SMODEM_WAKEUP_MODEM_NAME "qcom,smodem-wakeup-modem"
#define MODEM_ACK "qcom,modem-ack"
#define SMODEM_ACK "qcom,smodem-ack"
#define MODEM_STATE_LOCK_TIME_NAME "qcom,modem-lock-time"

enum {
    GPIO_LOW,
    GPIO_HIGH,
};

enum {
    MAIN_MODEM,
    SUB_MODEM,
};

enum {
    SMODEM_STATE_OFF,
    SMODEM_STATE_ON,
    SMODEM_STATE_ACTIVE,
    SMODEM_STATE_SLEEP,
    SMODEM_STATE_MAX,
};

static char *modem_state_str[] = {
    [SMODEM_STATE_OFF] = "off",
    [SMODEM_STATE_ON] = "on",
    [SMODEM_STATE_ACTIVE] = "active",
    [SMODEM_STATE_SLEEP] = "sleep",
};

//modem ---> The current modem
//smodem ---> Another modem

struct modem_state{
    uint8_t modem_id;
    struct device *dev;
    struct wake_lock lock;
    uint32_t lock_time;
    struct pinctrl *pinctrl;
    struct pinctrl_state *gpio_state_default;
    uint32_t smodem_pwr_on;
    uint32_t smodem_pwr_on_lvl;
    uint32_t smodem_pwr_off;
    uint32_t smodem_pwr_off_lvl;
    uint32_t smodem_wakeup;  //smodem wake up modem
    uint32_t smodem_wakeup_lvl;
    uint32_t modem_wakeup;  //modem wake up smodem
    uint32_t modem_wakeup_lvl;
    uint32_t smodem_ack;  //modem send ack to smodem
    uint32_t smodem_ack_lvl;
    uint32_t modem_ack;  //smodem send ack to modem
    uint32_t modem_ack_lvl;
    uint32_t smodem_state;
    uint32_t modem_state;
};

struct class *modem_state_class = NULL;
struct modem_state *modem_state = NULL;

static ssize_t smodem_poweron_store(struct class *c, struct class_attribute *attr,
        const char *buf, size_t count)
{
    uint32_t state;
    char _buf[10] = {0};

    pr_debug("buf = %p, %s\n", buf, buf);

    if(buf == NULL)
    {
        pr_err("invalid command\n");
        return count;
    }

    count = min(sizeof(_buf), count);
    strncpy(_buf, buf, count);

    if(_buf[count - 1] == '\n')
        _buf[count - 1] = '\0';

    for(state = SMODEM_STATE_OFF; state < SMODEM_STATE_MAX; state++)
    {
        if(!strcmp(modem_state_str[state], _buf))
            break;
    }

    pr_debug("set modem_state_str[%d] = %s\n", state, modem_state_str[state]);

    switch(state)
    {
        case SMODEM_STATE_ON:
            if(modem_state->smodem_state == SMODEM_STATE_OFF)
            {
                modem_state->smodem_state = SMODEM_STATE_ON;
                /*
                   Simulation pwr key pressed
                   keep pwr key high before VPH on
                 */
                gpio_direction_output(modem_state->smodem_pwr_on, !modem_state->smodem_pwr_on_lvl);
                gpio_direction_output(modem_state->smodem_pwr_off, modem_state->smodem_pwr_off_lvl);
                usleep_range(5, 10);
                gpio_direction_output(modem_state->smodem_pwr_on, modem_state->smodem_pwr_on_lvl);
                msleep(4000);
                gpio_direction_output(modem_state->smodem_pwr_on, !modem_state->smodem_pwr_on_lvl);
                //usleep_range(5, 10);

                enable_irq(gpio_to_irq(modem_state->smodem_wakeup));
                if(gpio_is_valid(modem_state->smodem_ack))
                    enable_irq(gpio_to_irq(modem_state->smodem_ack));
            }
            break;
        case SMODEM_STATE_OFF:
            if(modem_state->smodem_state != SMODEM_STATE_OFF)
            {
                modem_state->smodem_state = SMODEM_STATE_OFF;

                disable_irq(gpio_to_irq(modem_state->smodem_wakeup));
                if(gpio_is_valid(modem_state->smodem_ack))
                    disable_irq(gpio_to_irq(modem_state->smodem_ack));

                gpio_direction_output(modem_state->smodem_pwr_off, !modem_state->smodem_pwr_off_lvl);
                gpio_direction_output(modem_state->smodem_pwr_on, !modem_state->smodem_pwr_on_lvl);
            }
            break;
        case SMODEM_STATE_ACTIVE:
            if(modem_state->smodem_state == SMODEM_STATE_SLEEP)
            {
                modem_state->smodem_state = SMODEM_STATE_ACTIVE;

                gpio_direction_output(modem_state->smodem_pwr_on, !modem_state->smodem_pwr_on_lvl);
                usleep_range(5, 10);
                gpio_direction_output(modem_state->smodem_pwr_on, modem_state->smodem_pwr_on_lvl);
                usleep_range(5000, 10000);
                gpio_direction_output(modem_state->smodem_pwr_on, !modem_state->smodem_pwr_on_lvl);
            }

            break;
        case SMODEM_STATE_SLEEP:
            if(modem_state->smodem_state == SMODEM_STATE_ACTIVE)
            {
                modem_state->smodem_state = SMODEM_STATE_SLEEP;

                gpio_direction_output(modem_state->smodem_pwr_on, !modem_state->smodem_pwr_on_lvl);
                usleep_range(5, 10);
                gpio_direction_output(modem_state->smodem_pwr_on, modem_state->smodem_pwr_on_lvl);
                usleep_range(5000, 10000);
                gpio_direction_output(modem_state->smodem_pwr_on, !modem_state->smodem_pwr_on_lvl);
            }

            break;
    }

    return count;
}

static ssize_t smodem_wakeup_store(struct class *c, struct class_attribute *attr,
        const char *buf, size_t count)
{
    if(modem_state->smodem_state == SMODEM_STATE_SLEEP)
    {
        modem_state->smodem_state = SMODEM_STATE_ACTIVE;
        /*Simulation an interrupt, generate an Square wave*/
        gpio_direction_output(modem_state->modem_wakeup, modem_state->modem_wakeup_lvl);
        usleep_range(100, 150);
        gpio_direction_output(modem_state->modem_wakeup, !modem_state->modem_wakeup_lvl);
        msleep(10);
        gpio_direction_output(modem_state->modem_wakeup, modem_state->modem_wakeup_lvl);
    }

    return count;
}

static ssize_t smodem_state_show(struct class *c,
        struct class_attribute *attr, char *buf)
{
    ssize_t count = 0;

    count = sprintf(buf, "%s\n", modem_state_str[modem_state->smodem_state]);

    return count;
}

static struct class_attribute mode_state_attrs[] = {
    __ATTR(poweron, 0600, NULL, smodem_poweron_store),
    __ATTR(wakeup, 0600, NULL, smodem_wakeup_store),
    __ATTR(state, 0400, smodem_state_show, NULL),
    __ATTR_NULL
};

static struct class_attribute smode_state_attrs[] = {
    __ATTR(wakeup, 0600, NULL, smodem_wakeup_store),
    __ATTR(state, 0400, smodem_state_show, NULL),
    __ATTR_NULL
};

static void modem_state_class_release(struct class *cls)
{
    pr_err("\n");
    kfree(cls);
}

static int modem_state_parse_dt(struct modem_state *modem_state)
{
    int ret = 0;
    struct device_node *np = modem_state->dev->of_node;

    ret = of_property_read_u32(np, MODEM_STATE_ID_NAME, (uint32_t *)&modem_state->modem_id);
    if(ret < 0)
    {
        pr_err("not set " MODEM_STATE_ID_NAME ", using default.\n");
        modem_state->modem_id = MAIN_MODEM;
    }
    pr_debug("modem_id = %d\n", modem_state->modem_id);

    if(modem_state->modem_id == MAIN_MODEM)
    {
        modem_state->smodem_pwr_on = of_get_named_gpio_flags(np, MODEM_POWER_ON_NAME, 0, &modem_state->smodem_pwr_on_lvl);
        if(!gpio_is_valid(modem_state->smodem_pwr_on))
        {
            ret = -EINVAL;
            pr_err("missing set property" MODEM_POWER_ON_NAME "\n");
            goto parse_dt_err;
        }

        modem_state->smodem_pwr_off = of_get_named_gpio_flags(np, MODEM_POWER_OFF_NAME, 0, &modem_state->smodem_pwr_off_lvl);
        if(!gpio_is_valid(modem_state->smodem_pwr_off))
        {
            ret = -EINVAL;
            pr_err("missing set property" MODEM_POWER_OFF_NAME "\n");
            goto parse_dt_err;
        }
    }

    modem_state->modem_wakeup = of_get_named_gpio_flags(np, MODEM_WAKEUP_SMODEM_NAME, 0, &modem_state->modem_wakeup_lvl);
    if(!gpio_is_valid(modem_state->modem_wakeup))
    {
        ret = -EINVAL;
        pr_err("missing set property" MODEM_WAKEUP_SMODEM_NAME "\n");
        goto parse_dt_err;
    }

    modem_state->smodem_wakeup = of_get_named_gpio_flags(np, SMODEM_WAKEUP_MODEM_NAME, 0, &modem_state->smodem_wakeup_lvl);
    if(!gpio_is_valid(modem_state->smodem_wakeup))
    {
        ret = -EINVAL;
        pr_err("missing set property" SMODEM_WAKEUP_MODEM_NAME "\n");
        goto parse_dt_err;
    }

    modem_state->smodem_ack = of_get_named_gpio_flags(np, SMODEM_ACK , 0, &modem_state->smodem_ack_lvl);
    if(!gpio_is_valid(modem_state->smodem_ack))
    {
        pr_debug("missing set property" SMODEM_ACK ", not use smodem ack pin\n");
    }

    modem_state->modem_ack = of_get_named_gpio_flags(np, MODEM_ACK , 0, &modem_state->modem_ack_lvl);
    if(!gpio_is_valid(modem_state->modem_ack))
    {
        pr_debug("missing set property" MODEM_ACK ", not use modem ack pin\n");
    }

    ret = of_property_read_u32(np, MODEM_STATE_LOCK_TIME_NAME, (uint32_t *)&modem_state->lock_time);
    if(ret < 0)
    {
        pr_err("not set " MODEM_STATE_LOCK_TIME_NAME ", using default.\n");
        modem_state->lock_time = 10000; //10s
    }

parse_dt_err:
    return ret;
}

static void modem_state_io_deinit(struct modem_state *modem_state)
{
    devm_pinctrl_put(modem_state->pinctrl);
    if(modem_state->modem_id == MAIN_MODEM)
    {
        gpio_free(modem_state->smodem_pwr_on);
        gpio_free(modem_state->smodem_pwr_off);
    }
    gpio_free(modem_state->smodem_wakeup);
    gpio_free(modem_state->modem_wakeup);
    if(gpio_is_valid(modem_state->smodem_ack))
        gpio_free(modem_state->smodem_ack);
    if(gpio_is_valid(modem_state->modem_ack))
        gpio_free(modem_state->modem_ack);
}

static int modem_state_io_init(struct modem_state *modem_state)
{
    int ret = 0;

    modem_state->pinctrl = devm_pinctrl_get(modem_state->dev);
    if (IS_ERR_OR_NULL(modem_state->pinctrl)) {
        ret = PTR_ERR(modem_state->pinctrl);
        pr_err("failed to get pinctrl ret = %d\n", ret);
        goto devm_pinctrl_get_failed;
    }

    modem_state->gpio_state_default = pinctrl_lookup_state(modem_state->pinctrl,
            "default");
    if (IS_ERR_OR_NULL(modem_state->gpio_state_default)) {
        pr_err("can not get default pinstate\n");
        ret = -EINVAL;
        goto pinctrl_lookup_state_failed;
    }

    ret = pinctrl_select_state(modem_state->pinctrl, modem_state->gpio_state_default);
    if (ret)
    {
        ret = -EINVAL;
        pr_err("set state failed!\n");
        goto pinctrl_select_state_failed;
    }

    if(modem_state->modem_id == MAIN_MODEM)
    {
        ret = gpio_request(modem_state->smodem_pwr_on, "smodem pwr on pin");
        if(ret < 0)
        {
            pr_err("gpio request smodem pwr on pin failed.\n");
            goto smodem_pwr_on_request_failed;
        }
        gpio_direction_output(modem_state->smodem_pwr_on, !modem_state->smodem_pwr_on_lvl);


        ret = gpio_request(modem_state->smodem_pwr_off, "smodem pwr off pin");
        if(ret < 0)
        {
            pr_err("gpio request smodem pwr off pin failed.\n");
            goto smodem_pwr_off_request_failed;
        }
        gpio_direction_output(modem_state->smodem_pwr_off, !modem_state->smodem_pwr_off_lvl);
    }

    ret = gpio_request(modem_state->smodem_wakeup, "smodem wakeup pin");
    if(ret < 0)
    {
        pr_err("gpio request smodem wakeup pin failed.\n");
        goto smodem_wakeup_request_failed;
    }
    gpio_direction_input(modem_state->smodem_wakeup);

    ret = gpio_request(modem_state->modem_wakeup, "modem wakeup pin");
    if(ret < 0)
    {
        pr_err("gpio request modem wakeup pin failed.\n");
        goto modem_wakeup_request_failed;
    }
    gpio_direction_output(modem_state->modem_wakeup, !modem_state->modem_wakeup_lvl);

    if(gpio_is_valid(modem_state->smodem_ack))
    {
        ret = gpio_request(modem_state->smodem_ack, "smodem ack pin");
        if(ret < 0)
        {
            pr_err("gpio request smodem ack pin failed.\n");
            goto smodem_ack_request_failed;
        }
        gpio_direction_input(modem_state->smodem_ack);
    }

    if(gpio_is_valid(modem_state->modem_ack))
    {
        ret = gpio_request(modem_state->modem_ack, "modem ack pin");
        if(ret < 0)
        {
            pr_err("gpio request modem ack pin failed.\n");
            goto modem_ack_request_failed;
        }
        gpio_direction_output(modem_state->modem_ack, !modem_state->modem_ack_lvl);
    }

    return ret;

modem_ack_request_failed:
    if(gpio_is_valid(modem_state->smodem_ack))
        gpio_free(modem_state->smodem_ack);
smodem_ack_request_failed:
    gpio_free(modem_state->modem_wakeup);
modem_wakeup_request_failed:
    gpio_free(modem_state->smodem_wakeup);
    if(modem_state->modem_id == MAIN_MODEM)
    {
smodem_wakeup_request_failed:
        gpio_free(modem_state->smodem_pwr_off);
smodem_pwr_off_request_failed:
        gpio_free(modem_state->smodem_pwr_on);
    }
smodem_pwr_on_request_failed:
pinctrl_select_state_failed:
pinctrl_lookup_state_failed:
    devm_pinctrl_put(modem_state->pinctrl);
devm_pinctrl_get_failed:
    return ret;
}

static void modem_state_interrupt_deinit(struct modem_state *modem_state)
{
    devm_free_irq(modem_state->dev, gpio_to_irq(modem_state->smodem_wakeup), modem_state);
    if(gpio_is_valid(modem_state->smodem_ack))
        devm_free_irq(modem_state->dev, gpio_to_irq(modem_state->smodem_ack), modem_state);
}

static irqreturn_t modem_state_smodem_wakeup_handler(int irq, void *dev_id)
{
    pr_debug("\n");
    wake_lock_timeout(&modem_state->lock, msecs_to_jiffies(modem_state->lock_time));
    return IRQ_HANDLED;
}

static irqreturn_t modem_state_smodem_ack_handler(int irq, void *dev_id)
{
    int smodem_ack_level = 0;
    
    smodem_ack_level = gpio_get_value(modem_state->smodem_ack);

    pr_debug("smodem_ack_level = %d\n", smodem_ack_level);
    if(smodem_ack_level == modem_state->smodem_ack_lvl)
    {
        modem_state->smodem_state = SMODEM_STATE_ACTIVE;
    }
    else
    {
        modem_state->smodem_state = SMODEM_STATE_SLEEP;
    }

    sysfs_notify(&modem_state->dev->kobj, NULL, "wakeup");

    return IRQ_HANDLED;
}

static int modem_state_interrupt_init(struct modem_state *modem_state)
{
    int ret = 0;
    int smodem_wakeup_irq;
    int smodem_ack_irq;

    smodem_wakeup_irq = gpio_to_irq(modem_state->smodem_wakeup);
    if(smodem_wakeup_irq < 0)
    {
        pr_err("invalid smodem_wakeup irq.\n");
        ret = -EINVAL;
        goto set_smodem_wakeup_irq_failed;
    }
    ret = devm_request_threaded_irq(modem_state->dev, smodem_wakeup_irq, 
            NULL,
            modem_state_smodem_wakeup_handler,
            modem_state->smodem_wakeup_lvl,
            SMODEM_WAKEUP_MODEM_NAME,
            (void *)modem_state);
    if(ret)
    {
        pr_err("devm_request_threaded_irq modem_wakeup failed. lvl = %d\n", modem_state->smodem_wakeup_lvl);
        goto set_smodem_wakeup_irq_failed;
    }

    enable_irq_wake(smodem_wakeup_irq);
    if(modem_state->modem_id == MAIN_MODEM)
    {
        disable_irq(smodem_wakeup_irq);
    }

    if(gpio_is_valid(modem_state->smodem_ack))
    {
        smodem_ack_irq = gpio_to_irq(modem_state->smodem_ack);
        if(smodem_ack_irq < 0)
        {
            pr_err("invalid smodem_ack irq.\n");
            ret = -EINVAL;
            goto set_smodem_ack_irq_failed;
        }
        ret = devm_request_threaded_irq(modem_state->dev, smodem_ack_irq, 
                NULL,
                modem_state_smodem_ack_handler,
                modem_state->smodem_ack_lvl,
                SMODEM_ACK,
                modem_state);
        if(ret)
        {
            pr_err("devm_request_threaded_irq smodem_ack failed.\n");
            goto set_smodem_ack_irq_failed;
        }

        if(modem_state->modem_id == MAIN_MODEM)
        {
            disable_irq(smodem_ack_irq);
        }
    }

    return ret;

set_smodem_ack_irq_failed:
    devm_free_irq(modem_state->dev, smodem_wakeup_irq, modem_state);
set_smodem_wakeup_irq_failed:
    
    return ret;
}

static int modem_state_probe(struct platform_device *pdev)
{
    int ret = 0;

    modem_state = kzalloc(sizeof(*modem_state), GFP_KERNEL);
    if(IS_ERR_OR_NULL(modem_state))
    {
        pr_err("modem_state create failed.\n");
        ret = -ENOMEM;
        goto modem_state_probe_out;
    }

    modem_state->dev = &pdev->dev;

    ret = modem_state_parse_dt(modem_state);
    if(ret)
        goto modem_state_failed;

    if(modem_state->modem_id == MAIN_MODEM)
    {
        modem_state->smodem_state = SMODEM_STATE_OFF;
        modem_state->modem_state = SMODEM_STATE_OFF;
    }
    else
    {
        modem_state->smodem_state = SMODEM_STATE_ON;
        modem_state->modem_state = SMODEM_STATE_ON;
    }

    ret = modem_state_io_init(modem_state);
    if(ret)
        goto modem_state_failed;

    wake_lock_init(&modem_state->lock, WAKE_LOCK_SUSPEND, "modem_state_lock");

    ret = modem_state_interrupt_init(modem_state);
    if(ret)
        goto modem_state_interrupt_init_failed;

    modem_state_class = kzalloc(sizeof(*modem_state_class), GFP_KERNEL);
    if(IS_ERR_OR_NULL(modem_state_class))
    {
        pr_err("modem_state_class create failed.\n");
        ret = -ENOMEM;
        goto modem_state_class_alloc_failed;
    }
    modem_state_class->name = "modem_state";
    modem_state_class->owner = THIS_MODULE;
    modem_state_class->class_release = modem_state_class_release;
    if(modem_state->modem_id == MAIN_MODEM)
    {
        modem_state_class->class_attrs = mode_state_attrs;
    }
    else
    {
        modem_state_class->class_attrs = smode_state_attrs;
    }

    ret = class_register(modem_state_class);
    if(ret)
    {
        pr_err("modem_state_class register failed.\n");
        goto class_register_failed;
    }

    pr_info("success\n");

    return 0;

class_register_failed:
    kfree(modem_state_class);
    modem_state_class = NULL;
modem_state_class_alloc_failed:
    modem_state_interrupt_deinit(modem_state);
modem_state_interrupt_init_failed:
    wake_lock_destroy(&modem_state->lock);
    modem_state_io_deinit(modem_state);
modem_state_failed:
    kfree(modem_state);
    modem_state = NULL;
modem_state_probe_out:
    return ret;
}

static int modem_state_remove(struct platform_device *pdev)
{
    return 0;
}

static void modem_state_shutdown(struct platform_device *pdev)
{
    if(modem_state->smodem_state != SMODEM_STATE_OFF)
    {
        modem_state->smodem_state = SMODEM_STATE_OFF;

        disable_irq(gpio_to_irq(modem_state->smodem_wakeup));
        if(gpio_is_valid(modem_state->smodem_ack))
            disable_irq(gpio_to_irq(modem_state->smodem_ack));

        if(modem_state->modem_id == MAIN_MODEM)
        {
            gpio_direction_output(modem_state->smodem_pwr_off, !modem_state->smodem_pwr_off_lvl);
            gpio_direction_output(modem_state->smodem_pwr_on, !modem_state->smodem_pwr_on_lvl);
        }
    }
}

static struct of_device_id modem_state_match_table[] = {
    { .compatible = MODEM_STATE_DRIVER_NAME, },
    {},
};

static struct platform_driver modem_state_driver = {
    .probe = modem_state_probe,
    .remove = modem_state_remove,
    .shutdown = modem_state_shutdown,
    .driver = {
        .name = MODEM_STATE_DRIVER_NAME,
        .owner = THIS_MODULE,
        .of_match_table = modem_state_match_table,
    },
};

static int __init modem_state_init(void)
{
    return platform_driver_register(&modem_state_driver);
}

module_init(modem_state_init);
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("modem state control driver");
