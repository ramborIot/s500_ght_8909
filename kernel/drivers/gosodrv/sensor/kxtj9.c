/*
 * Copyright (C) 2011 Kionix, Inc.
 * Written by Chris Hudson <chudson@kionix.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 */

#include <linux/delay.h>
#include <linux/i2c.h>
#include <linux/input.h>
#include <linux/sensors.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/input-polldev.h>
#include <linux/regulator/consumer.h>

#include "kxtj9.h"
#ifdef CONFIG_OF
#include <linux/of_gpio.h>
#endif /* CONFIG_OF */


#define ACCEL_INPUT_DEV_NAME	"accelerometer"
#define DEVICE_NAME		"kxtj9"

#define G_MAX			8000
/* OUTPUT REGISTERS */
#define XOUT_L			0x06
#define WHO_AM_I		0x0F
/* CONTROL REGISTERS */
#define INT_REL			0x1A
#define CTRL_REG1		0x1B
#define INT_CTRL1		0x1E
#define DATA_CTRL		0x21
/* CONTROL REGISTER 1 BITS */
#define PC1_OFF			0x7F
#define PC1_ON			(1 << 7)
/* Data ready funtion enable bit: set during probe if using irq mode */
#define DRDYE			(1 << 5)
/* DATA CONTROL REGISTER BITS */
#define ODR3_125F		0x0A
#define ODR6_25F		0x0B
#define ODR12_5F		0
#define ODR25F			1
#define ODR50F			2
#define ODR100F		3
#define ODR200F		4
#define ODR400F		5
#define ODR800F		6
/* INTERRUPT CONTROL REGISTER 1 BITS */
/* Set these during probe if using irq mode */
#define KXTJ9_IEL		(1 << 3)
#define KXTJ9_IEA		(1 << 4)
#define KXTJ9_IEN		(1 << 5)
/* INPUT_ABS CONSTANTS */
#define FUZZ			3
#define FLAT			3
/* RESUME STATE INDICES */
#define RES_DATA_CTRL		0
#define RES_CTRL_REG1		1
#define RES_INT_CTRL1		2
#define RESUME_ENTRIES		3
/* POWER SUPPLY VOLTAGE RANGE */
#define KXTJ9_VDD_MIN_UV	2000000
#define KXTJ9_VDD_MAX_UV	3300000
#define KXTJ9_VIO_MIN_UV	1750000
#define KXTJ9_VIO_MAX_UV	1950000

#define DEBUG_TAG    "[KXTJ9]: "
static int kxtj9_debug_mask = 0;
module_param_named(debug_mask, kxtj9_debug_mask, int, S_IRUGO | S_IWUSR | S_IWGRP);

#define DEBUG_LOG(fmt, args...)   \
    do { \
        if(kxtj9_debug_mask & 0x01) \
        printk(KERN_INFO DEBUG_TAG fmt, ##args); \
    } while (0)


/*
 * The following table lists the maximum appropriate poll interval for each
 * available output data rate.
 */

static struct sensors_classdev sensors_cdev = {
	.name = "kxtj9-accel",
	.vendor = "Kionix",
	.version = 1,
	.handle = 0,
	.type = 1,
	.max_range = "19.6",
	.resolution = "0.01",
	.sensor_power = "0.2",
	.min_delay = 2000,	/* microsecond */
	.fifo_reserved_event_count = 0,
	.fifo_max_event_count = 0,
	.enabled = 0,
	.delay_msec = 200,	/* millisecond */
	.sensors_enable = NULL,
	.sensors_poll_delay = NULL,
};

static const struct {
	unsigned int cutoff;
	u8 mask;
} kxtj9_odr_table[] = {
	{ 3,	ODR800F },
	{ 5,	ODR400F },
	{ 10,	ODR200F },
	{ 20,	ODR100F },
	{ 40,	ODR50F  }, //20ms
	{ 60,	ODR25F  },
	{ 80,	ODR12_5F}, //66.7
	{ 300,	ODR6_25F}, //200ms
	{ 0,	ODR3_125F},
};

struct kxtj9_data {
	struct i2c_client *client;
	struct kxtj9_platform_data pdata;
	struct input_dev *input_dev;
#ifdef CONFIG_INPUT_KXTJ9_POLLED_MODE
	struct input_polled_dev *poll_dev;
#endif
	unsigned int last_poll_interval;
	bool	enable;
	u8 shift;
	u8 ctrl_reg1;
	u8 data_ctrl;
	u8 int_ctrl;
	bool	power_enabled;
	struct regulator *vdd;
	struct regulator *vio;
	struct sensors_classdev cdev;
	struct pinctrl *pinctrl;
	struct pinctrl_state *pin_default;
	struct pinctrl_state *pin_sleep;
	int irq1;
	struct work_struct irq1_work;
	struct workqueue_struct *irq1_work_queue;
	int irq2;
	struct work_struct irq2_work;
	struct workqueue_struct *irq2_work_queue;
	struct delayed_work polling_work;
	struct workqueue_struct *acc_polling_work_queue;
};

static int kxtj9_i2c_read(struct kxtj9_data *tj9, u8 addr, u8 *data, int len)
{
	struct i2c_msg msgs[] = {
		{
			.addr = tj9->client->addr,
			.flags = tj9->client->flags,
			.len = 1,
			.buf = &addr,
		},
		{
			.addr = tj9->client->addr,
			.flags = tj9->client->flags | I2C_M_RD,
			.len = len,
			.buf = data,
		},
	};

	return i2c_transfer(tj9->client->adapter, msgs, 2);
}

static void kxtj9_report_acceleration_data(struct kxtj9_data *tj9)
{
	s16 acc_data[3]; /* Data bytes from hardware xL, xH, yL, yH, zL, zH */
	s16 x, y, z;
	s16 tmp_x, tmp_y, tmp_z;
	int err;

	err = kxtj9_i2c_read(tj9, XOUT_L, (u8 *)acc_data, 6);
	if (err < 0)
		dev_err(&tj9->client->dev, "accelerometer data read failed\n");

	x = le16_to_cpu(acc_data[tj9->pdata.axis_map_x]);
	y = le16_to_cpu(acc_data[tj9->pdata.axis_map_y]);
	z = le16_to_cpu(acc_data[tj9->pdata.axis_map_z]);

	/* 8 bits output mode support */
	if (!(tj9->ctrl_reg1 & RES_12BIT)) {
		x <<= 4;
		y <<= 4;
		z <<= 4;
	}

	x >>= tj9->shift;
	y >>= tj9->shift;
	z >>= tj9->shift;

	tmp_x = tj9->pdata.negate_x ? -x : x;
	tmp_y = tj9->pdata.negate_y ? -y : y;
	tmp_z = tj9->pdata.negate_z ? -z : z;
	DEBUG_LOG("%s: x = %d, y= %d, z = %d\n", __func__, tmp_x, tmp_y, tmp_z);

	input_report_abs(tj9->input_dev, ABS_X, tmp_x);
	input_report_abs(tj9->input_dev, ABS_Y, tmp_y);
	input_report_abs(tj9->input_dev, ABS_Z, tmp_z);
	input_sync(tj9->input_dev);
}

#if 0
static irqreturn_t kxtj9_isr(int irq, void *dev)
{
	struct kxtj9_data *tj9 = dev;

	int err;
	/* data ready is the only possible interrupt type */
	kxtj9_report_acceleration_data(tj9);

	err = i2c_smbus_read_byte_data(tj9->client, INT_REL);
	if (err < 0)
		dev_err(&tj9->client->dev,
			"error clearing interrupt status: %d\n", err);

	return IRQ_HANDLED;
}
#endif

static irqreturn_t kxtj9_isr1(int irq, void *dev)
{
	struct kxtj9_data *tj9 = dev;

	disable_irq_nosync(irq);
	queue_work(tj9->irq1_work_queue, &tj9->irq1_work);

	return IRQ_HANDLED;
}

static void kxtj9_acc_irq1_work_func(struct work_struct *work)
{
	struct kxtj9_data *tj9 = container_of(work,
					struct kxtj9_data, irq1_work);
	int err;

	/* data ready is the only possible interrupt type */
	kxtj9_report_acceleration_data(tj9);

	// clear irq for the next irq
	err = i2c_smbus_read_byte_data(tj9->client, INT_REL);
	if (err < 0) {
		dev_err(&tj9->client->dev,
			"error clearing interrupt status: %d\n", err);
	}

	enable_irq(tj9->irq1);
}

#if 1
static irqreturn_t kxtj9_isr2(int irq, void *dev)
{
	struct kxtj9_data *tj9 = dev;

	disable_irq_nosync(irq);
	queue_work(tj9->irq2_work_queue, &tj9->irq2_work);

	return IRQ_HANDLED;
}


static void kxtj9_acc_irq2_work_func(struct work_struct *work)
{
	struct kxtj9_data *tj9 = container_of(work,
					struct kxtj9_data, irq2_work);
	int err;

	/* data ready is the only possible interrupt type */
	kxtj9_report_acceleration_data(tj9);

	// clear irq for the next irq
	err = i2c_smbus_read_byte_data(tj9->client, INT_REL);
	if (err < 0) {
		dev_err(&tj9->client->dev,
			"error clearing interrupt status: %d\n", err);
	}

	enable_irq(tj9->irq2);
}
#endif

static void kxtj9_acc_polling_work_func(struct work_struct *delay_work)
{
	struct kxtj9_data *tj9 = container_of((struct delayed_work*)delay_work,
				struct kxtj9_data, polling_work);
	unsigned int delay = tj9->last_poll_interval;

	/* data ready is the only possible interrupt type */
	kxtj9_report_acceleration_data(tj9);

	queue_delayed_work(tj9->acc_polling_work_queue,
				&tj9->polling_work, msecs_to_jiffies(delay));
}

static int kxtj9_update_g_range(struct kxtj9_data *tj9, u8 new_g_range)
{
	switch (new_g_range) {
	case KXTJ9_G_2G:
		tj9->shift = 4;
		break;
	case KXTJ9_G_4G:
		tj9->shift = 3;
		break;
	case KXTJ9_G_8G:
		tj9->shift = 2;
		break;
	default:
		return -EINVAL;
	}

	tj9->ctrl_reg1 &= 0xe7;
	tj9->ctrl_reg1 |= new_g_range;

	return 0;
}

static int kxtj9_update_odr(struct kxtj9_data *tj9, unsigned int poll_interval)
{
	int err;
	int i;

	/* Use the lowest ODR that can support the requested poll interval */
	for (i = 0; i < ARRAY_SIZE(kxtj9_odr_table); i++) {
		tj9->data_ctrl = kxtj9_odr_table[i].mask;
		if (poll_interval < kxtj9_odr_table[i].cutoff)
			break;
	}

	err = i2c_smbus_write_byte_data(tj9->client, CTRL_REG1, 0);
	if (err < 0) {
		dev_err(&tj9->client->dev, "%s:write CTRL_REG1 to zero failed\n", __func__);
		return err;
	}

	err = i2c_smbus_write_byte_data(tj9->client, DATA_CTRL, tj9->data_ctrl);
	if (err < 0) {
		dev_err(&tj9->client->dev, "%s:write DATA_CTRL failed\n", __func__);
		return err;
	}

	err = i2c_smbus_write_byte_data(tj9->client, CTRL_REG1, tj9->ctrl_reg1);
	if (err < 0) {
		dev_err(&tj9->client->dev, "%s:write CTRL_REG1 failed\n", __func__);
		return err;
	}

	return 0;
}

static int kxtj9_power_on(struct kxtj9_data *data, bool on)
{
	int rc = 0;

	if (!on && data->power_enabled) {
		rc = regulator_disable(data->vdd);
		if (rc) {
			dev_err(&data->client->dev,
				"Regulator vdd disable failed rc=%d\n", rc);
			return rc;
		}

		rc = regulator_disable(data->vio);
		if (rc) {
			dev_err(&data->client->dev,
				"Regulator vio disable failed rc=%d\n", rc);
			rc = regulator_enable(data->vdd);
			if (rc) {
				dev_err(&data->client->dev,
					"Regulator vdd enable failed rc=%d\n",
					rc);
			}
		}
		data->power_enabled = false;
	} else if (on && !data->power_enabled) {
		rc = regulator_enable(data->vdd);
		if (rc) {
			dev_err(&data->client->dev,
				"Regulator vdd enable failed rc=%d\n", rc);
			return rc;
		}

		rc = regulator_enable(data->vio);
		if (rc) {
			dev_err(&data->client->dev,
				"Regulator vio enable failed rc=%d\n", rc);
			regulator_disable(data->vdd);
		}
		data->power_enabled = true;
	} else {
		dev_warn(&data->client->dev,
				"Power on=%d. enabled=%d\n",
				on, data->power_enabled);
	}
    dev_info(&data->client->dev,"%s: power on status: %d, enable: %d\n",
                                __func__,on,data->power_enabled);
	return rc;
}

static int kxtj9_power_init(struct kxtj9_data *data, bool on)
{
	int rc;

	if (!on) {
		if (regulator_count_voltages(data->vdd) > 0)
			regulator_set_voltage(data->vdd, 0, KXTJ9_VDD_MAX_UV);

		regulator_put(data->vdd);

		if (regulator_count_voltages(data->vio) > 0)
			regulator_set_voltage(data->vio, 0, KXTJ9_VIO_MAX_UV);

		regulator_put(data->vio);
	} else {
		data->vdd = regulator_get(&data->client->dev, "vdd");
		if (IS_ERR(data->vdd)) {
			rc = PTR_ERR(data->vdd);
			dev_err(&data->client->dev,
				"Regulator get failed vdd rc=%d\n", rc);
			return rc;
		}

		if (regulator_count_voltages(data->vdd) > 0) {
			rc = regulator_set_voltage(data->vdd, KXTJ9_VDD_MIN_UV,
						   KXTJ9_VDD_MAX_UV);
			if (rc) {
				dev_err(&data->client->dev,
					"Regulator set failed vdd rc=%d\n",
					rc);
				goto reg_vdd_put;
			}
		}

		data->vio = regulator_get(&data->client->dev, "vio");
		if (IS_ERR(data->vio)) {
			rc = PTR_ERR(data->vio);
			dev_err(&data->client->dev,
				"Regulator get failed vio rc=%d\n", rc);
			goto reg_vdd_set;
		}

		if (regulator_count_voltages(data->vio) > 0) {
			rc = regulator_set_voltage(data->vio, KXTJ9_VIO_MIN_UV,
						   KXTJ9_VIO_MAX_UV);
			if (rc) {
				dev_err(&data->client->dev,
				"Regulator set failed vio rc=%d\n", rc);
				goto reg_vio_put;
			}
		}
	}

	return 0;

reg_vio_put:
	regulator_put(data->vio);
reg_vdd_set:
	if (regulator_count_voltages(data->vdd) > 0)
		regulator_set_voltage(data->vdd, 0, KXTJ9_VDD_MAX_UV);
reg_vdd_put:
	regulator_put(data->vdd);
	return rc;
}
static int kxtj9_device_power_on(struct kxtj9_data *tj9)
{
	int err = 0;
	if (tj9->pdata.power_on) {
		err = tj9->pdata.power_on();
	} else {
		err = kxtj9_power_on(tj9, true);
		if (err) {
			dev_err(&tj9->client->dev, "power on failed");
			goto err_exit;
		}
		/* Use 80ms as vendor suggested. */
		msleep(80);
	}

err_exit:
	dev_dbg(&tj9->client->dev, "soft power on complete err=%d.\n", err);
	return err;
}

static void kxtj9_device_power_off(struct kxtj9_data *tj9)
{
	int err;

	tj9->ctrl_reg1 &= PC1_OFF;
	err = i2c_smbus_write_byte_data(tj9->client, CTRL_REG1, tj9->ctrl_reg1);
	if (err < 0)
		dev_err(&tj9->client->dev, "soft power off failed\n");

	if (tj9->pdata.power_off) {
		tj9->pdata.power_off();
	} else {
		kxtj9_power_on(tj9, false);
	}

	dev_dbg(&tj9->client->dev, "soft power off complete.\n");
	return ;
}

static int kxtj9_enable(struct kxtj9_data *tj9)
{
	struct i2c_client *client = tj9->client;
	int err;

	err = kxtj9_device_power_on(tj9);
	if (err < 0) {
		dev_err(&client->dev, "%s:kxtj9_device_power_on  failed\n", __func__);
		return err;
	}

	if (pinctrl_select_state(tj9->pinctrl, tj9->pin_default)) {
		dev_err(&tj9->client->dev,
				"Can't get select pinctl slepp state\n");
	}
	/* ensure that PC1 is cleared before updating control registers */
	err = i2c_smbus_write_byte_data(tj9->client, CTRL_REG1, 0);
	if (err < 0) {
		dev_err(&client->dev, "%s:write clear PC1 failed\n", __func__);
		return err;
	}

	/* only write INT_CTRL_REG1 if in irq mode */
	if (tj9->pdata.use_irq > 0) {
		err = i2c_smbus_write_byte_data(tj9->client,
						INT_CTRL1, tj9->int_ctrl);
		if (err < 0) {
			dev_err(&client->dev,
						"%s:write interrupt register failed\n", __func__);
			return err;
		}
	}

	err = kxtj9_update_g_range(tj9, tj9->pdata.g_range);
	if (err < 0) {
		dev_err(&client->dev,
					"%s:kxtj9_update_g_range failed\n", __func__);
		return err;
	}

	/* turn on outputs */
	tj9->ctrl_reg1 |= PC1_ON;
	err = i2c_smbus_write_byte_data(tj9->client, CTRL_REG1, tj9->ctrl_reg1);
	if (err < 0) {
		dev_err(&client->dev,
					"%s:turn on outputs failed\n", __func__);
		return err;
	}

	err = kxtj9_update_odr(tj9, tj9->last_poll_interval);
	if (err < 0) {
		dev_err(&client->dev,
					"%s:kxtj9_update_odr failed\n", __func__);
		return err;
	}

	/* clear initial interrupt if in irq mode */
	if (tj9->pdata.use_irq > 0) {
		err = i2c_smbus_read_byte_data(tj9->client, INT_REL);
		if (err < 0) {
			dev_err(&client->dev,
				"error clearing interrupt: %d\n", err);
			goto fail;
		}
	}

	printk(KERN_INFO "%s: kxtj9 enable ok\n", __func__);
	return 0;

fail:
	kxtj9_device_power_off(tj9);
	return err;
}

static void kxtj9_disable(struct kxtj9_data *tj9)
{
	kxtj9_device_power_off(tj9);
	if (pinctrl_select_state(tj9->pinctrl, tj9->pin_sleep)) {
		dev_err(&tj9->client->dev,
				"Can't get select pinctl slepp state\n");
	}
	printk(KERN_INFO "%s: kxtj9 disable ok\n", __func__);
}


static void  kxtj9_init_input_device(struct kxtj9_data *tj9,
					      struct input_dev *input_dev)
{
	__set_bit(EV_ABS, input_dev->evbit);
	input_set_abs_params(input_dev, ABS_X, -G_MAX, G_MAX, FUZZ, FLAT);
	input_set_abs_params(input_dev, ABS_Y, -G_MAX, G_MAX, FUZZ, FLAT);
	input_set_abs_params(input_dev, ABS_Z, -G_MAX, G_MAX, FUZZ, FLAT);

	input_dev->name = ACCEL_INPUT_DEV_NAME;
	input_dev->id.bustype = BUS_I2C;
	input_dev->dev.parent = &tj9->client->dev;
}

static int kxtj9_setup_input_device(struct kxtj9_data *tj9)
{
	struct input_dev *input_dev;
	int err;

	input_dev = input_allocate_device();
	if (!input_dev) {
		dev_err(&tj9->client->dev, "input device allocate failed\n");
		return -ENOMEM;
	}

	tj9->input_dev = input_dev;

	input_set_drvdata(input_dev, tj9);

	kxtj9_init_input_device(tj9, input_dev);

	err = input_register_device(tj9->input_dev);
	if (err) {
		dev_err(&tj9->client->dev,
			"unable to register input polled device %s: %d\n",
			tj9->input_dev->name, err);
		input_free_device(tj9->input_dev);
		return err;
	}

	return 0;
}

static int kxtj9_enable_set(struct sensors_classdev *sensors_cdev,
					unsigned int enabled)
{
	struct kxtj9_data *tj9 = container_of(sensors_cdev,
					struct kxtj9_data, cdev);
	struct input_dev *input_dev = tj9->input_dev;

	mutex_lock(&input_dev->mutex);

	printk(KERN_INFO "%s:enabled=%d,use_irq=%d\n",
					__func__, enabled, tj9->pdata.use_irq);
	if (enabled == 0) {
		if (tj9->pdata.use_irq > 0) {
			disable_irq(tj9->client->irq);
		} else {
			cancel_delayed_work_sync(&tj9->polling_work);
		}
		kxtj9_disable(tj9);
		tj9->enable = false;
	} else if (enabled == 1) {
		if (!kxtj9_enable(tj9)) {
			if (tj9->pdata.use_irq > 0) {
				enable_irq(tj9->client->irq);
			} else {
				queue_delayed_work(tj9->acc_polling_work_queue,
						&tj9->polling_work, msecs_to_jiffies(tj9->last_poll_interval));
			}
			tj9->enable = true;
		}
	} else {
		dev_err(&tj9->client->dev,
			"Invalid value of input, input=%d\n", enabled);
		mutex_unlock(&input_dev->mutex);
		return -EINVAL;
	}

	mutex_unlock(&input_dev->mutex);

	return 0;
}

static ssize_t kxtj9_enable_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct kxtj9_data *tj9 = i2c_get_clientdata(client);

	return snprintf(buf, 4, "%d\n", tj9->enable);
}

static ssize_t kxtj9_enable_store(struct device *dev,
					struct device_attribute *attr,
					const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct kxtj9_data *tj9 = i2c_get_clientdata(client);
	unsigned long data;
	int error;

	error = kstrtoul(buf, 10, &data);
	if (error < 0) {
		dev_err(&client->dev,
					"%s:invalid parameter\n", __func__);
		return error;
	}

	dev_info(&client->dev,
				"%s:data=%lu\n",  __func__, data);
	error = kxtj9_enable_set(&tj9->cdev, data);
	if (error < 0) {
		dev_err(&client->dev,
					"%s:kxtj9_enable_set failed\n", __func__);
		return error;
	}
	return count;
}

static DEVICE_ATTR(enable, S_IRUGO|S_IWUSR|S_IWGRP,
			kxtj9_enable_show, kxtj9_enable_store);

/*
 * When IRQ mode is selected, we need to provide an interface to allow the user
 * to change the output data rate of the part.  For consistency, we are using
 * the set_poll method, which accepts a poll interval in milliseconds, and then
 * calls update_odr() while passing this value as an argument.  In IRQ mode, the
 * data outputs will not be read AT the requested poll interval, rather, the
 * lowest ODR that can support the requested interval.  The client application
 * will be responsible for retrieving data from the input node at the desired
 * interval.
 */
static int kxtj9_poll_delay_set(struct sensors_classdev *sensors_cdev,
					unsigned int delay_msec)
{
	struct kxtj9_data *tj9 = container_of(sensors_cdev,
					struct kxtj9_data, cdev);
	struct input_dev *input_dev = tj9->input_dev;

	/* Lock the device to prevent races with open/close (and itself) */
	mutex_lock(&input_dev->mutex);

	dev_info(&tj9->client->dev, "%s:delay_msec=%u\n", __func__, delay_msec);
	if (tj9->enable) {
		if (tj9->pdata.use_irq > 0) {
			disable_irq(tj9->client->irq);
		}
	}

	// there are four polling rate leve at android later, use max value between
	// the application register polling rate and min_interval.
	tj9->last_poll_interval = max(delay_msec, tj9->pdata.min_interval);
	if (tj9->enable) {
		kxtj9_update_odr(tj9, tj9->last_poll_interval);
		if (tj9->pdata.use_irq > 0) {
			enable_irq(tj9->client->irq);
		}
	}
	mutex_unlock(&input_dev->mutex);

	return 0;
}

/* Returns currently selected poll interval (in ms) */
static ssize_t kxtj9_get_poll_delay(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct kxtj9_data *tj9 = i2c_get_clientdata(client);

	return sprintf(buf, "%d\n", tj9->last_poll_interval);
}

/* Allow users to select a new poll interval (in ms) */
static ssize_t kxtj9_set_poll_delay(struct device *dev,
					struct device_attribute *attr,
					const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct kxtj9_data *tj9 = i2c_get_clientdata(client);
	unsigned int interval;
	int error;

	dev_info(&client->dev, "%s:\n", __func__);
	error = kstrtouint(buf, 10, &interval);
	if (error < 0) {
		dev_err(&client->dev, "%s:!!!invalid poll set\n", __func__);
		return error;
	}

	error = kxtj9_poll_delay_set(&tj9->cdev, interval);
	if (error < 0) {
		dev_err(&client->dev, "%s:!!!kxtj9_poll_delay_set failed\n", __func__);
		return error;
	}
	return count;
}

static DEVICE_ATTR(poll_delay, S_IRUGO|S_IWUSR|S_IWGRP,
			kxtj9_get_poll_delay, kxtj9_set_poll_delay);

static struct attribute *kxtj9_attributes[] = {
	&dev_attr_enable.attr,
	&dev_attr_poll_delay.attr,
	NULL
};

static struct attribute_group kxtj9_attribute_group = {
	.attrs = kxtj9_attributes
};

#ifdef CONFIG_INPUT_KXTJ9_POLLED_MODE
static void kxtj9_poll(struct input_polled_dev *dev)
{
	struct kxtj9_data *tj9 = dev->private;
	unsigned int poll_interval = dev->poll_interval;

	kxtj9_report_acceleration_data(tj9);

	if (poll_interval != tj9->last_poll_interval) {
		kxtj9_update_odr(tj9, poll_interval);
		tj9->last_poll_interval = poll_interval;
	}
}

static void kxtj9_polled_input_open(struct input_polled_dev *dev)
{
	struct kxtj9_data *tj9 = dev->private;

	dev_info(&tj9->client->dev, "%s\n", __func__);
	kxtj9_enable(tj9);
}

static void kxtj9_polled_input_close(struct input_polled_dev *dev)
{
	struct kxtj9_data *tj9 = dev->private;

	dev_info(&tj9->client->dev, "%s\n", __func__);
	kxtj9_disable(tj9);
}

static int kxtj9_setup_polled_device(struct kxtj9_data *tj9)
{
	int err;
	struct input_polled_dev *poll_dev;
	poll_dev = input_allocate_polled_device();

	if (!poll_dev) {
		dev_err(&tj9->client->dev,
			"Failed to allocate polled device\n");
		return -ENOMEM;
	}

	tj9->poll_dev = poll_dev;
	tj9->input_dev = poll_dev->input;

	poll_dev->private = tj9;
	poll_dev->poll = kxtj9_poll;
	poll_dev->open = kxtj9_polled_input_open;
	poll_dev->close = kxtj9_polled_input_close;

	kxtj9_init_input_device(tj9, poll_dev->input);

	err = input_register_polled_device(poll_dev);
	if (err) {
		dev_err(&tj9->client->dev,
			"Unable to register polled device, err=%d\n", err);
		input_free_polled_device(poll_dev);
		return err;
	}
    dev_info(&tj9->client->dev,"%s:kxtj9 input device register OK!\n",__func__);
	return 0;
}

static void  kxtj9_teardown_polled_device(struct kxtj9_data *tj9)
{
	input_unregister_polled_device(tj9->poll_dev);
	input_free_polled_device(tj9->poll_dev);
}

#else

static inline int kxtj9_setup_polled_device(struct kxtj9_data *tj9)
{
	return -ENOSYS;
}

static inline void kxtj9_teardown_polled_device(struct kxtj9_data *tj9)
{
}

#endif

static int  kxtj9_verify(struct kxtj9_data *tj9)
{
	int retval;

	retval = i2c_smbus_read_byte_data(tj9->client, WHO_AM_I);
	if (retval < 0) {
		dev_err(&tj9->client->dev, "read err int source\n");
		goto out;
	}

	printk(KERN_ERR "WHO_AM_I %d\n",retval);

	retval = (retval != 0x05 && retval != 0x07 && retval != 0x08 && retval != 0x09)
			? -EIO : 0;

out:
	return retval;
}

static int kxtj9_pinctl_init(struct kxtj9_data *kxtj9)
{
	struct i2c_client *client = kxtj9->client;

	kxtj9->pinctrl = devm_pinctrl_get(&client->dev);
	if (IS_ERR_OR_NULL(kxtj9->pinctrl)) {
		dev_err(&client->dev, "Failed to get pinctl\n");
		return PTR_ERR(kxtj9->pinctrl);
	}

	kxtj9->pin_default =
		pinctrl_lookup_state(kxtj9->pinctrl, KXTJ9_DEFAULT_PIN);
	if (IS_ERR_OR_NULL(kxtj9->pin_default)) {
		dev_err(&client->dev, "Failed to look up default state\n");
		return PTR_ERR(kxtj9->pin_default);
	}

	kxtj9->pin_sleep = pinctrl_lookup_state(kxtj9->pinctrl, KXTJ9_SUSPEND_PIN);
	if (IS_ERR_OR_NULL(kxtj9->pin_sleep)) {
		dev_err(&client->dev, "Failed to look up sleep state\n"); 
		return PTR_ERR(kxtj9->pin_sleep);
	}

	return 0;
}

#ifdef CONFIG_OF
static int kxtj9_parse_dt(struct device *dev,
				struct kxtj9_platform_data *kxtj9_pdata)
{
	struct device_node *np = dev->of_node;
	u32 temp_val;
	int rc;

	rc = of_property_read_u32(np, "kionix,min-interval", &temp_val);
	if (rc && (rc != -EINVAL)) {
		dev_err(dev, "Unable to read min-interval\n");
		return rc;
	} else {
		kxtj9_pdata->min_interval = temp_val;
	}

	rc = of_property_read_u32(np, "kionix,init-interval", &temp_val);
	if (rc && (rc != -EINVAL)) {
		dev_err(dev, "Unable to read init-interval\n");
		return rc;
	} else {
		kxtj9_pdata->init_interval = temp_val;
	}

	rc = of_property_read_u32(np, "kionix,axis-map-x", &temp_val);
	if (rc && (rc != -EINVAL)) {
		dev_err(dev, "Unable to read axis-map_x\n");
		return rc;
	} else {
		kxtj9_pdata->axis_map_x = (u8)temp_val;
	}

	rc = of_property_read_u32(np, "kionix,axis-map-y", &temp_val);
	if (rc && (rc != -EINVAL)) {
		dev_err(dev, "Unable to read axis_map_y\n");
		return rc;
	} else {
		kxtj9_pdata->axis_map_y = (u8)temp_val;
	}

	rc = of_property_read_u32(np, "kionix,axis-map-z", &temp_val);
	if (rc && (rc != -EINVAL)) {
		dev_err(dev, "Unable to read axis-map-z\n");
		return rc;
	} else {
		kxtj9_pdata->axis_map_z = (u8)temp_val;
	}

	rc = of_property_read_u32(np, "kionix,g-range", &temp_val);
	if (rc && (rc != -EINVAL)) {
		dev_err(dev, "Unable to read g-range\n");
		return rc;
	} else {
		switch (temp_val) {
		case 2:
			kxtj9_pdata->g_range = KXTJ9_G_2G;
			break;
		case 4:
			kxtj9_pdata->g_range = KXTJ9_G_4G;
			break;
		case 8:
			kxtj9_pdata->g_range = KXTJ9_G_8G;
			break;
		default:
			kxtj9_pdata->g_range = KXTJ9_G_2G;
			break;
		}
	}

	kxtj9_pdata->negate_x = of_property_read_bool(np, "kionix,negate-x");

	kxtj9_pdata->negate_y = of_property_read_bool(np, "kionix,negate-y");

	kxtj9_pdata->negate_z = of_property_read_bool(np, "kionix,negate-z");

	if (of_property_read_bool(np, "kionix,res-12bit")) {
		kxtj9_pdata->res_ctl = RES_12BIT;
	} else {
		kxtj9_pdata->res_ctl = RES_8BIT;
	}

    kxtj9_pdata->gpio_int1 = of_get_named_gpio_flags(np,
                "kionix,gpio-int1", 0, NULL);

    kxtj9_pdata->gpio_int2 = of_get_named_gpio_flags(np,
                "kionix,gpio-int2", 0, NULL);

	kxtj9_pdata->use_irq = of_property_read_bool(np, "kionix,use_irq");

	return 0;
}
#else
static int kxtj9_parse_dt(struct device *dev,
				struct kxtj9_platform_data *kxtj9_pdata)
{
	return -ENODEV;
}
#endif /* !CONFIG_OF */

static int  kxtj9_probe(struct i2c_client *client,
				 const struct i2c_device_id *id)
{
	struct kxtj9_data *tj9;
	int err;

	if (!i2c_check_functionality(client->adapter,
				I2C_FUNC_I2C | I2C_FUNC_SMBUS_BYTE_DATA)) {
		dev_err(&client->dev, "client is not i2c capable\n");
		return -ENXIO;
	}

	tj9 = kzalloc(sizeof(*tj9), GFP_KERNEL);
	if (!tj9) {
		dev_err(&client->dev,
			"failed to allocate memory for module data\n");
		return -ENOMEM;
	}

	if (client->dev.of_node) {
		memset(&tj9->pdata, 0 , sizeof(tj9->pdata));
		err = kxtj9_parse_dt(&client->dev, &tj9->pdata);
		if (err) {
			dev_err(&client->dev,
				"Unable to parse platfrom data err=%d\n", err);
			goto err_free_mem;
		}
	} else {
		if (client->dev.platform_data)
			tj9->pdata = *(struct kxtj9_platform_data *)
					client->dev.platform_data;
		else {
			dev_err(&client->dev,
				"platform data is NULL; exiting\n");
			err = -EINVAL;
			goto err_free_mem;
		}
	}

	tj9->client = client;
	tj9->power_enabled = false;
	if (tj9->pdata.init) {
		err = tj9->pdata.init();
		if (err < 0) {
			dev_err(&client->dev, "driver private init failed\n");
			goto err_free_mem;
		}
	}

	/* initialize pinctrl */
	err = kxtj9_pinctl_init(tj9);
	if (err) {
		dev_err(&client->dev, "Can't initialize pinctrl\n");
		goto err_free_mem;
	}
	err = pinctrl_select_state(tj9->pinctrl, tj9->pin_default);
	if (err) {
		 dev_err(&client->dev,
				 "Set pinctrl to default state failed\n");
		 goto err_free_mem;
	}

	if (gpio_is_valid(tj9->pdata.gpio_int1)) {
		tj9->irq1 = gpio_to_irq(tj9->pdata.gpio_int1);
	}
	if (gpio_is_valid(tj9->pdata.gpio_int2)) {
		tj9->irq2 = gpio_to_irq(tj9->pdata.gpio_int2);
	}

	err = kxtj9_power_init(tj9, true);
	if (err < 0) {
		dev_err(&tj9->client->dev, "power init failed! err=%d", err);
		goto err_pdata_exit;
	}

	err = kxtj9_device_power_on(tj9);
	if (err < 0) {
		dev_err(&client->dev, "power on failed! err=%d\n", err);
		goto err_power_deinit;
	}
	err = kxtj9_verify(tj9);
	if (err < 0) {
		dev_err(&client->dev, "device not recognized\n");
		goto err_power_off;
	}

	i2c_set_clientdata(client, tj9);

	tj9->ctrl_reg1 = tj9->pdata.res_ctl | tj9->pdata.g_range;
	tj9->last_poll_interval = tj9->pdata.init_interval;

	err = kxtj9_setup_input_device(tj9);
	if (err) {
		dev_err(&client->dev,
				"kxtj9_setup_input_device failed\n");
		goto err_power_off;
	}

	tj9->cdev = sensors_cdev;
	/* The min_delay is used by userspace and the unit is microsecond.
	 * at android layer, there are for polling rate leve:200, 66.6,20,and 0.
	 */
	tj9->cdev.min_delay = tj9->pdata.min_interval;
	tj9->cdev.delay_msec = tj9->pdata.init_interval;
	tj9->cdev.sensors_enable = kxtj9_enable_set;
	tj9->cdev.sensors_poll_delay = kxtj9_poll_delay_set;
	err = sensors_classdev_register(&tj9->input_dev->dev, &tj9->cdev);
	if (err) {
		dev_err(&client->dev, "class device create failed: %d\n", err);
		goto err_destroy_input;
	}

	if (tj9->pdata.use_irq > 0) {
		if (gpio_is_valid(tj9->pdata.gpio_int1) && tj9->irq1) {
			/* If in irq mode, populate INT_CTRL_REG1 and enable DRDY. */
			tj9->int_ctrl |= KXTJ9_IEN | KXTJ9_IEA | KXTJ9_IEL;
			tj9->ctrl_reg1 |= DRDYE;

			INIT_WORK(&tj9->irq1_work, kxtj9_acc_irq1_work_func);
			tj9->irq1_work_queue =
				create_singlethread_workqueue("kxtj9_acc_wq1");
			if (!tj9->irq1_work_queue) {
				err = -ENOMEM;
				dev_err(&client->dev,
						"cannot create work queue1: %d\n", err);
				goto err_unregister_sensor_class;
			}
			err = request_irq(tj9->irq1, kxtj9_isr1,
					IRQF_TRIGGER_RISING,
					"kxtj9_acc_irq1", tj9);
			if (err < 0) {
				dev_err(&client->dev, "request irq1 failed: %d\n", err);
				goto err_destoyworkqueue1;

			}
			disable_irq(tj9->irq1);
		}

		/* only one irq of kxtj9, the following code is ueseless for this chip*/
		if (gpio_is_valid(tj9->pdata.gpio_int2) && tj9->irq2) {
			INIT_WORK(&tj9->irq2_work, kxtj9_acc_irq2_work_func);
			tj9->irq2_work_queue =
				create_singlethread_workqueue("kxtj9_acc_wq1");
			if (!tj9->irq2_work_queue) {
				err = -ENOMEM;
				dev_err(&client->dev,
						"cannot create work queue1: %d\n", err);
				goto err_destoyworkqueue1;
			}
			err = request_irq(tj9->irq2, kxtj9_isr2,
					IRQF_TRIGGER_RISING,
					"kxtj9_acc_irq1", tj9);
			if (err < 0) {
				dev_err(&client->dev, "request irq1 failed: %d\n", err);
				goto err_destoyworkqueue2;
			}
			disable_irq(tj9->irq2);
		}
	} else {
		// Fuck, polled device is polling controled by Evenghub, which never cancel the
		// polling work queuw. As a result, more power consume when system running with
		// screen off, such as lisen Music FM, and download at background.
		#ifdef CONFIG_INPUT_KXTJ9_POLLED_MODE
		err = kxtj9_setup_polled_device(tj9);
		if (err) {
			dev_err(&client->dev,
					"setup_polled_device failed\n");
			goto err_unregister_sensor_class;
		}
		#else
		INIT_DELAYED_WORK(&tj9->polling_work, kxtj9_acc_polling_work_func);
		tj9->acc_polling_work_queue =
			create_singlethread_workqueue("kktj9_polling_work");
		if (!tj9->acc_polling_work_queue) {
			dev_err(&client->dev, "can't creat polling work\n");
			goto err_unregister_sensor_class;
		}
		#endif
	}

	err = sysfs_create_group(&client->dev.kobj, &kxtj9_attribute_group);
	if (err) {
		dev_err(&client->dev, "sysfs create failed: %d\n", err);
		goto err_free_irq;
	}
	if (pinctrl_select_state(tj9->pinctrl, tj9->pin_sleep)) {
		dev_err(&client->dev,
				 "Can't select pinctrl sleep state\n");
	}

	kxtj9_device_power_off(tj9);
	dev_dbg(&client->dev, "%s: kxtj9_probe OK.\n", __func__);

	return 0;

err_free_irq:
	if (tj9->pdata.use_irq > 0) {
		free_irq(client->irq, tj9);
	}
err_destoyworkqueue2:
	if (tj9->irq2_work_queue) {
		destroy_workqueue(tj9->irq2_work_queue);
	}
err_destoyworkqueue1:
	if (tj9->irq1_work_queue) {
		destroy_workqueue(tj9->irq1_work_queue);
	}
err_unregister_sensor_class:
	sensors_classdev_unregister(&tj9->cdev);
err_destroy_input:
	input_unregister_device(tj9->input_dev);
err_power_off:
	kxtj9_device_power_off(tj9);
err_power_deinit:
	kxtj9_power_init(tj9, false);
err_pdata_exit:
	if (tj9->pdata.exit)
		tj9->pdata.exit();
err_free_mem:
	kfree(tj9);

	dev_err(&client->dev, "%s: kxtj9_probe err=%d\n", __func__, err);
	return err;
}

static int kxtj9_remove(struct i2c_client *client)
{
	struct kxtj9_data *tj9 = i2c_get_clientdata(client);

	if (tj9->pdata.use_irq > 0) {
		sysfs_remove_group(&client->dev.kobj, &kxtj9_attribute_group);
		free_irq(client->irq, tj9);
		input_unregister_device(tj9->input_dev);
	} else {
		kxtj9_teardown_polled_device(tj9);
	}

	if (tj9->irq2_work_queue) {
		destroy_workqueue(tj9->irq2_work_queue);
	}

	if (tj9->irq1_work_queue) {
		destroy_workqueue(tj9->irq1_work_queue);
	}

	sensors_classdev_unregister(&tj9->cdev);

	kxtj9_device_power_off(tj9);
	kxtj9_power_init(tj9, false);

	if (tj9->pdata.exit)
		tj9->pdata.exit();

	kfree(tj9);

	return 0;
}

#ifdef CONFIG_PM_SLEEP
static int kxtj9_suspend(struct device *dev)
{
#if 0
	struct i2c_client *client = to_i2c_client(dev);
	struct kxtj9_data *tj9 = i2c_get_clientdata(client);
	struct input_dev *input_dev = tj9->input_dev;

	mutex_lock(&input_dev->mutex);

	if (input_dev->users && tj9->enable)
		kxtj9_disable(tj9);

	mutex_unlock(&input_dev->mutex);
#endif

	return 0;
}

static int kxtj9_resume(struct device *dev)
{
	int retval = 0;
#if 0
	struct i2c_client *client = to_i2c_client(dev);
	struct kxtj9_data *tj9 = i2c_get_clientdata(client);
	struct input_dev *input_dev = tj9->input_dev;

	mutex_lock(&input_dev->mutex);

	if (input_dev->users && tj9->enable)
		kxtj9_enable(tj9);

	mutex_unlock(&input_dev->mutex);
#endif
	return retval;
}
#endif

static SIMPLE_DEV_PM_OPS(kxtj9_pm_ops, kxtj9_suspend, kxtj9_resume);

static const struct i2c_device_id kxtj9_id[] = {
	{ DEVICE_NAME, 0 },
	{ },
};

static struct of_device_id kxtj9_match_table[] = {
	{ .compatible = "kionix,kxtj9", },
	{ },
};


MODULE_DEVICE_TABLE(i2c, kxtj9_id);

static struct i2c_driver kxtj9_driver = {
	.driver = {
		.name	= DEVICE_NAME,
		.owner	= THIS_MODULE,
		.of_match_table = kxtj9_match_table,
		.pm	= &kxtj9_pm_ops,
	},
	.probe		= kxtj9_probe,
	.remove		= kxtj9_remove,
	.id_table	= kxtj9_id,
};

module_i2c_driver(kxtj9_driver);

MODULE_DESCRIPTION("KXTJ9 accelerometer driver");
MODULE_AUTHOR("Chris Hudson <chudson@kionix.com>");
MODULE_LICENSE("GPL");
