/*
 * Simple synchronous userspace interface to SPI devices
 *
 * Copyright (C) 2006 SWAPP
 *	Andrea Paterniani <a.paterniani@swapp-eng.it>
 * Copyright (C) 2007 David Brownell (simplification, cleanup)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/ioctl.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/list.h>
#include <linux/errno.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/compat.h>

#include <linux/spi/spi.h>
#include <linux/spi/spidev.h>

#include <asm/uaccess.h>


#include <linux/platform_device.h>
//#include <mach/sys_config.h>
//#include <mach/gpio.h>
#include <linux/wait.h>

#include <linux/interrupt.h>

#include <linux/io.h>

#include <linux/kthread.h>

#include <linux/gpio.h>
#include <linux/wait.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/semaphore.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/delay.h>
#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/pm.h>
#include <linux/earlysuspend.h>
#endif
#include <linux/poll.h> 
#include <linux/input.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_address.h>
#include <linux/of_platform.h>
#include <linux/of_gpio.h>
#include <linux/time.h>



#define K21_SPI 1
#define DRV_NAME	"spidev-k21"
#define TATOL_DATA_LEN 1024

static u32 irq = 0;
static struct task_struct * _tsk;  
//script_item_u pio; //中断管脚 PG12
int spi_wake_k21_pio_vaild=0;
//script_item_u spi_wake_k21_pio;  //唤醒k21复位spi

int k21_wakeup_status_pio_vaild=0;
//script_item_u k21_wakeup_status_pio;  //k21状态
static bool have_data = false; /*表明设备有足够数据可供读*/
static bool have_test = false;
static bool bootFlag = true;

static  struct input_dev * spikeydev;
struct spidev_data	*g_spidev = 0;
static int suspend_status = 0;

#ifdef CONFIG_OF
static int spi_int_gpio;
static int ap_wakeup_sp_gpio;
static int check_sp_status_gpio;
//static int en_sp_gpio;
static int ctrl_scaner_gpio;
static int ctrl_id_gpio;
//static int ctrl_uart_0_gpio;
//static int ctrl_uart_1_gpio;
#endif


static DECLARE_WAIT_QUEUE_HEAD(read_wait);
static DECLARE_WAIT_QUEUE_HEAD(write_wait);


#ifdef CONFIG_HAS_EARLYSUSPEND
	struct early_suspend early_suspend;
#endif

#define K21_SPI_DEBUG 1

#ifdef K21_SPI_DEBUG
#define  k21_spi_dbg(format,args...)      printk(format,##args)
#else
#define  k21_spi_dbg(format,args...)	////
#endif

/*
 * This supports access to SPI devices using normal userspace I/O calls.
 * Note that while traditional UNIX/POSIX I/O semantics are half duplex,
 * and often mask message boundaries, full SPI support requires full duplex
 * transfers.  There are several kinds of internal message boundaries to
 * handle chipselect management and other protocol options.
 *
 * SPI has a character major number assigned.  We allocate minor numbers
 * dynamically using a bitmask.  You must use hotplug tools, such as udev
 * (or mdev with busybox) to create and destroy the /dev/spidevB.C device
 * nodes, since there is no fixed association of minor numbers with any
 * particular SPI bus or device.
 */
#define SPIDEV_MAJOR			153	/* assigned */
#define N_SPI_MINORS			32	/* ... up to 256 */

static DECLARE_BITMAP(minors, N_SPI_MINORS);


/* Bit masks for spi_device.mode management.  Note that incorrect
 * settings for some settings can cause *lots* of trouble for other
 * devices on a shared bus:
 *
 *  - CS_HIGH ... this device will be active when it shouldn't be
 *  - 3WIRE ... when active, it won't behave as it should
 *  - NO_CS ... there will be no explicit message boundaries; this
 *	is completely incompatible with the shared bus model
 *  - READY ... transfers may proceed when they shouldn't.
 *
 * REVISIT should changing those flags be privileged?
 */
#define SPI_MODE_MASK		(SPI_CPHA | SPI_CPOL | SPI_CS_HIGH \
				| SPI_LSB_FIRST | SPI_3WIRE | SPI_LOOP \
				| SPI_NO_CS | SPI_READY)

struct spidev_data {
	dev_t			devt;
	spinlock_t		spi_lock;
	struct spi_device	*spi;
	struct list_head	device_entry;

	/* buffer is NULL unless this device is open (users > 0) */
	struct mutex		buf_lock;
	unsigned		users;
	u8			*buffer;
	//wait_queue_head_t poll_queue;

};

static LIST_HEAD(device_list);
static DEFINE_MUTEX(device_list_lock);

static unsigned bufsiz = (4096+1024);
module_param(bufsiz, uint, S_IRUGO);
MODULE_PARM_DESC(bufsiz, "data bytes in biggest supported SPI message");
static void spidev_wakeup_k21(void);

/*-------------------------------------------------------------------------*/

/*
 * We can't use the standard synchronous wrappers for file I/O; we
 * need to protect against async removal of the underlying spi_device.
 */
static void spidev_complete(void *arg)
{
	complete(arg);
}

static ssize_t
spidev_sync(struct spidev_data *spidev, struct spi_message *message)
{
	DECLARE_COMPLETION_ONSTACK(done);
	int status;

	message->complete = spidev_complete;
	message->context = &done;
	//edit xum 20161201
	spidev_wakeup_k21();
	
	//printk("max_speed_hz:  %u  \n", spidev->spi->max_speed_hz);

	//gpio_set_value(ap_wakeup_sp_gpio, 0);//低电平
	spin_lock_irq(&spidev->spi_lock);
	if (spidev->spi == NULL)
		status = -ESHUTDOWN;
	else {
		status = spi_async(spidev->spi, message);
	}
	spin_unlock_irq(&spidev->spi_lock);
	//gpio_set_value(ap_wakeup_sp_gpio, 1);//低电平
	if (status == 0) {
		wait_for_completion(&done);
		status = message->status;
		if (status == 0)
			status = message->actual_length;
	}
	return status;
}

static inline ssize_t
spidev_sync_write(struct spidev_data *spidev, size_t len)
{
	struct spi_transfer	t = {
			.tx_buf		= spidev->buffer,
			.len		= len,
		};
	struct spi_message	m;

	spi_message_init(&m);
	spi_message_add_tail(&t, &m);
	return spidev_sync(spidev, &m);
}


static inline ssize_t
spidev_sync_write_offset(struct spidev_data *spidev, size_t len, int offset)
{
	struct spi_transfer	t = {
			.tx_buf		= spidev->buffer + offset,
			.len		= len,
		};
	struct spi_message	m;


	spi_message_init(&m);
	spi_message_add_tail(&t, &m);
	return spidev_sync(spidev, &m);
}

static inline ssize_t
spidev_sync_read(struct spidev_data *spidev, size_t len)
{
	struct spi_transfer	t = {
			.rx_buf		= spidev->buffer,
			.len		= len,
			.speed_hz   = 5000000,
			.bits_per_word = 8,
		};
	struct spi_message	m;
	
	spi_message_init(&m);
	spi_message_add_tail(&t, &m);
	return spidev_sync(spidev, &m);
}

/*-------------------------------------------------------------------------*/
/*读取的k21数据存储在 链表Lhead中， */
#if K21_SPI

typedef struct read_data
{
    u8* Rddata;
	u16 Rdlen;
    struct read_data *next;  
}NODE;

NODE * Lhead=NULL;

NODE *create_nod(u8 *buf,u16 len)
{
 NODE *nod;
   
  nod=(NODE *)kmalloc(sizeof(NODE),GFP_KERNEL);  //创建头节点。 
  if(nod==NULL)  //创建失败返回 
  {
   k21_spi_dbg("创建失败！");
   return NULL;
   }
 nod->next=NULL;  //头节点指针域置NULL
 nod->Rddata=(u8 *)kmalloc(len,GFP_KERNEL);  // 开始时尾指针指向头节点
 memcpy(nod->Rddata,buf,len); 
 nod->Rdlen = len;
 return nod;  //返回创建链表的头指针 
}
void insert(NODE *head,NODE *nod)
{
	 NODE *p;
	 int j=0;
//	 k21_spi_dbg("%s\n",__func__);   
	 p=head;
	      
	 while(p->next !=NULL)
	 {
	 	j++;
	 	p=p->next;
	 }
	 p->next = nod;    //犟第i个节点的指针域指向插入的新节点 
	// p->id = j+1;//记录节点位置
	 head->Rdlen = j+1;//头结点id记录链表节点总数
}
void pdelete(NODE *head,int i)
{
	 NODE *p,*q;
	 int j;
	 if(i==0)  //删除的是头指针，返回 
	   return;
	 p=head;
	 for(j=1;j<i&&p->next!=NULL;j++)
	   p=p->next;  //将p指向要删除的第i个节点的前驱节点 
	 if(p->next==NULL)  //表明链表中的节点不存在 
	 {
	  k21_spi_dbg("不存在！");
	  return;
	 }  
	 q=p->next;  //q指向待删除的节点 
	 p->next=q->next;  //删除节点i，也可写成p->next=p->next->next 
	 kfree(q->Rddata);
	 kfree(q);   //释放节点i的内存单元 
	 head->Rdlen -=1;
}
/*默认返回第一节点*/
NODE * getnod(NODE *head,int i)
{
	 NODE *p;
	 int j=1;//
	 for(p=head->next;p!=NULL;p=p->next,j++)
	 {
	   if(j == i)
	   	return p;
	 }
	return NULL;  

}

/*默认返回第一节点*/
void dispnod(NODE *head)
{
	 NODE *p;
	 int j=1,i;//
	 for(p=head->next;p!=NULL;p=p->next,j++)
	 {
	   printk("nod:%d len=%d:",j,p->Rdlen);
	   for(i=0;i<p->Rdlen;i++)
	   		printk("%02x ",(u8)p->Rddata[i]);
	   printk("\n");
	   ;
	 }
}
//edit 20161201 xuml
#if 0
static void spidev_wakeup_k21(void)
{
	int k21_status = 0;
	int i = 0;
	if (likely(spi_wake_k21_pio_vaild && k21_wakeup_status_pio_vaild))
	{	
		k21_status = __gpio_get_value(check_sp_status_gpio);
		k21_spi_dbg( "++++  k21_status %s ++++++\n",k21_status?"hi":"low");

		if( !k21_status) 
		{
			gpio_set_value(ap_wakeup_sp_gpio, 0);//低电平
			udelay(100);
			gpio_set_value(ap_wakeup_sp_gpio, 1);//高电平
			//mdelay(15);
			
			i = 0;
			while (i++ <= 60)
			{
				if (__gpio_get_value(check_sp_status_gpio)) break;
				mdelay(1);
			}

			if (i>49) 
			{
				gpio_set_value(ap_wakeup_sp_gpio, 0);//低电平
				udelay(100);
				gpio_set_value(ap_wakeup_sp_gpio, 1);//高电平
				mdelay(50);		
			}
		}
	}	
	
}
#endif

static void spidev_wakeup_k21(void)
{
	int k21_status = 0;
	int i = 0;
	if (likely(spi_wake_k21_pio_vaild && k21_wakeup_status_pio_vaild))
	{	
		k21_status = __gpio_get_value(check_sp_status_gpio);
		//k21_spi_dbg(KERN_ERR "++++  k21_status %s ++++++\n",k21_status?"hi":"low");

		if( !k21_status) 
		{
			gpio_set_value(ap_wakeup_sp_gpio, 0);//低电平
			udelay(100);
			gpio_set_value(ap_wakeup_sp_gpio, 1);//高电平
			//mdelay(15);
			//k21_spi_dbg(KERN_ERR "LINE:%d\n", __LINE__);
			i = 0;
			while (i++ <= 60)
			{
				if (__gpio_get_value(check_sp_status_gpio)) break;
				mdelay(1);
				k21_spi_dbg(KERN_ERR "LINE:%d i:%d \n", __LINE__, i);
			}
			
			if (i>49) 
			{
				gpio_set_value(ap_wakeup_sp_gpio, 0);//低电平
				udelay(100);
				gpio_set_value(ap_wakeup_sp_gpio, 1);//高电平
				mdelay(50);		
			}
			//k21_spi_dbg(KERN_ERR "LINE:%d\n", __LINE__);
		}
	}	
	
}


/* Read-block caoliang add  */
//static wait_queue_head_t rdwait;
//static wait_queue_head_t wrwait;
static wait_queue_head_t log_wait_queue;

static struct semaphore mutex;
static int k21_spi_readflag = 0;

static unsigned int spidev_poll(struct file *filp, poll_table *wait)
{	
	//struct spidev_data *dev = filp->private_data;
	unsigned int mask = 0;

	poll_wait(filp, &read_wait, wait);	/* 把进程添加到等待队列*/
	poll_wait(filp, &write_wait, wait);
	
	if(have_data)	// 有数据可读
		mask |= POLLIN |POLLRDNORM;
	
	if(have_test){
		mask |= POLLOUT | POLLWRNORM;
		have_test = false;

		}
	return mask;
}


static ssize_t
spidev_read_noblock(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{

	//k21_spi_dbg("\n%s\n",__func__);
	NODE *nod = NULL;
	
//	if (filp->f_flags & O_NONBLOCK)
//		return -EAGAIN;
	
	wait_event_interruptible(read_wait, have_data);
    //dispnod(Lhead);//for test
	 
	nod = getnod(Lhead,1);
	if (nod == NULL) {
		k21_spi_dbg("no more data!\n");
		have_data = false;
		return -1;
	}
	
	 //printk("spidev_read_noblock nod->Rdlen %d\n",nod->Rdlen);
	if (count >= nod->Rdlen)
	{
	    /*copy from kernel to user space*/
	    if(copy_to_user(buf,nod->Rddata,nod->Rdlen) != 0){
	       
	        return -EFAULT;
	    }
		count = nod->Rdlen;
	}
	else
	{
		  /*copy from kernel to user space*/
	    if(copy_to_user(buf,nod->Rddata,count) != 0){
	       
	        return -EFAULT;
	    }
		//k21_spi_dbg("user Buffer overflow! count =%lu Rlen=%u\n",count,nod->Rdlen);
	}
	
    /*data unaccessible flag*/
    pdelete(Lhead,1);
     
   if (Lhead->next == NULL)
   	have_data = false;
   
  // printk("spidev_read_noblock return %d\n",count);
    return count;
}

#if 0
/* Write-only message with current device setup */
static ssize_t
spidev_write_block(struct file *filp, const char __user *buf,
		size_t count, loff_t *f_pos)
{
	
	struct spidev_data	*spidev;
	ssize_t			status = 0;
	unsigned long		missing;
	k21_spi_dbg("%s\n",__func__);

	/* chipselect only toggles at start or end of operation */
	if (count > bufsiz)
		return -EMSGSIZE;


	/*wait until condition become true*/
	if( wait_event_interruptible(wrwait,k21_spi_readflag ==0) ){
		return -ERESTARTSYS;
	}

	/*get semaphore*/
	if(down_interruptible(&mutex)){
	   return -ERESTARTSYS;
	}
		   

	spidev = filp->private_data;

	mutex_lock(&spidev->buf_lock);
	missing = copy_from_user(spidev->buffer, buf, count);
	if (missing == 0) {
		status = spidev_sync_write(spidev, count);
	} else
		status = -EFAULT;
	mutex_unlock(&spidev->buf_lock);

	/*release semaphore*/
	up(&mutex);
	
	/*data ready*/
	
	/*wake up the waiting task*/
	wake_up_interruptible(&rdwait);
		  

	return status;
}
#endif

#endif

#if 0
/* Read-only message with current device setup */
static ssize_t
spidev_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
	struct spidev_data	*spidev;
	ssize_t			status = 0;

	/* chipselect only toggles at start or end of operation */
	if (count > bufsiz)
		return -EMSGSIZE;

	k21_spi_dbg("%s count=%lu\n",__func__,count);	

	spidev = filp->private_data;

	mutex_lock(&spidev->buf_lock);
	status = spidev_sync_read(spidev, count);
	if (status > 0) {
		unsigned long	missing;

		missing = copy_to_user(buf, spidev->buffer, status);
		if (missing == status)
			status = -EFAULT;
		else
			status = status - missing;
	}
	mutex_unlock(&spidev->buf_lock);
	
	return status;
}
#endif

/* Write-only message with current device setup */
static ssize_t
spidev_write(struct file *filp, const char __user *buf,
		size_t count, loff_t *f_pos)
{
	struct spidev_data	*spidev;
	ssize_t			status = 0;
	ssize_t			tmpcnt = 0;
	unsigned long		missing;

	/* chipselect only toggles at start or end of operation */
	if (count > bufsiz)
		return -EMSGSIZE;
	
	spidev = filp->private_data;
	mutex_lock(&spidev->buf_lock);
#if 0
	spidev->buffer[0]=0xFF;
	spidev->buffer[1]=0xFF;
	missing = copy_from_user(&spidev->buffer[2], buf, count);
	if (missing == 0) {
		status = spidev_sync_write(spidev, count+2);
	} else
		status = -EFAULT;

#else
	missing = copy_from_user(&spidev->buffer[0], buf, count);
	if (missing == 0) {
		if(count > 1024){
			int total,curr,sendcnt;
			total = count/1024;

			curr = 0;
			//k21_spi_dbg(KERN_ERR "spidev_write: total=%d \n",total);
			while (curr <= total)
			{
				if (curr == total)
					sendcnt = count - curr*1024;
				else
					sendcnt = 1024;
				
				//k21_spi_dbg(KERN_ERR"spidev_write: curr=%d, sendcnt = %d\n",curr,sendcnt);
				//gpio_set_value(ap_wakeup_sp_gpio, 0);//低电平
				tmpcnt = spidev_sync_write_offset(spidev, sendcnt, curr*1024);
				//gpio_set_value(ap_wakeup_sp_gpio, 1);//低电平
				//k21_spi_dbg(KERN_ERR" tmpcnt=%ld,\n", tmpcnt);
				if (tmpcnt >= 0) {
					status += sendcnt;
					curr ++;
				}
				else {
				//	k21_spi_dbg(KERN_ERR"Warning spidev_write: tmpcnt=%ld,\n", tmpcnt);
					break;
				}
			}
		} else {
			//gpio_set_value(ap_wakeup_sp_gpio, 0);//低电平
			status = spidev_sync_write(spidev, count);
			//gpio_set_value(ap_wakeup_sp_gpio, 1);//低电平
			//k21_spi_dbg(" status=%ld,\n", status);
		}
	} else
		status = -EFAULT;
#endif
	mutex_unlock(&spidev->buf_lock);

	return status;
}

static int spidev_message(struct spidev_data *spidev,
		struct spi_ioc_transfer *u_xfers, unsigned n_xfers)
{
	struct spi_message	msg;
	struct spi_transfer	*k_xfers;
	struct spi_transfer	*k_tmp;
	struct spi_ioc_transfer *u_tmp;
	unsigned		n, total;
	u8			*buf;
	int			status = -EFAULT;

	spi_message_init(&msg);
	k_xfers = kcalloc(n_xfers, sizeof(*k_tmp), GFP_KERNEL);
	if (k_xfers == NULL)
		return -ENOMEM;

	/* Construct spi_message, copying any tx data to bounce buffer.
	 * We walk the array of user-provided transfers, using each one
	 * to initialize a kernel version of the same transfer.
	 */
	buf = spidev->buffer;
	total = 0;
	for (n = n_xfers, k_tmp = k_xfers, u_tmp = u_xfers;
			n;
			n--, k_tmp++, u_tmp++) {
		k_tmp->len = u_tmp->len;

		total += k_tmp->len;
		if (total > bufsiz) {
			status = -EMSGSIZE;
			goto done;
		}

		if (u_tmp->rx_buf) {
			k_tmp->rx_buf = buf;
			if (!access_ok(VERIFY_WRITE, (u8 __user *)
						(uintptr_t) u_tmp->rx_buf,
						u_tmp->len))
				goto done;
		}
		if (u_tmp->tx_buf) {
			k_tmp->tx_buf = buf;
			if (copy_from_user(buf, (const u8 __user *)
						(uintptr_t) u_tmp->tx_buf,
					u_tmp->len))
				goto done;
		}
		buf += k_tmp->len;

		k_tmp->cs_change = !!u_tmp->cs_change;
		k_tmp->bits_per_word = u_tmp->bits_per_word;
		k_tmp->delay_usecs = u_tmp->delay_usecs;
		k_tmp->speed_hz = u_tmp->speed_hz;
#ifdef VERBOSE
		dev_dbg(&spidev->spi->dev,
			"  xfer len %zd %s%s%s%dbits %u usec %uHz\n",
			u_tmp->len,
			u_tmp->rx_buf ? "rx " : "",
			u_tmp->tx_buf ? "tx " : "",
			u_tmp->cs_change ? "cs " : "",
			u_tmp->bits_per_word ? : spidev->spi->bits_per_word,
			u_tmp->delay_usecs,
			u_tmp->speed_hz ? : spidev->spi->max_speed_hz);
#endif
		spi_message_add_tail(k_tmp, &msg);
	}

	status = spidev_sync(spidev, &msg);
	if (status < 0)
		goto done;

	/* copy any rx data out of bounce buffer */
	buf = spidev->buffer;
	for (n = n_xfers, u_tmp = u_xfers; n; n--, u_tmp++) {
		if (u_tmp->rx_buf) {
			if (__copy_to_user((u8 __user *)
					(uintptr_t) u_tmp->rx_buf, buf,
					u_tmp->len)) {
				status = -EFAULT;
				goto done;
			}
		}
		buf += u_tmp->len;
	}
	status = total;

done:
	kfree(k_xfers);
	return status;
}

static long
spidev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int			err = 0;
	int			retval = 0;
	struct spidev_data	*spidev;
	struct spi_device	*spi;
	u32			tmp;
	unsigned		n_ioc;
	struct spi_ioc_transfer	*ioc;

	
	/* Check type and command number */
	if (_IOC_TYPE(cmd) != SPI_IOC_MAGIC)
		return -ENOTTY;

	/* Check access direction once here; don't repeat below.
	 * IOC_DIR is from the user perspective, while access_ok is
	 * from the kernel perspective; so they look reversed.
	 */
	if (_IOC_DIR(cmd) & _IOC_READ)
		err = !access_ok(VERIFY_WRITE,
				(void __user *)arg, _IOC_SIZE(cmd));
	if (err == 0 && _IOC_DIR(cmd) & _IOC_WRITE)
		err = !access_ok(VERIFY_READ,
				(void __user *)arg, _IOC_SIZE(cmd));
	if (err)
		return -EFAULT;

	/* guard against device removal before, or while,
	 * we issue this ioctl.
	 */
	spidev = filp->private_data;
	spin_lock_irq(&spidev->spi_lock);
	spi = spi_dev_get(spidev->spi);
	spin_unlock_irq(&spidev->spi_lock);

	if (spi == NULL)
		return -ESHUTDOWN;

	/* use the buffer lock here for triple duty:
	 *  - prevent I/O (from us) so calling spi_setup() is safe;
	 *  - prevent concurrent SPI_IOC_WR_* from morphing
	 *    data fields while SPI_IOC_RD_* reads them;
	 *  - SPI_IOC_MESSAGE needs the buffer locked "normally".
	 */
	mutex_lock(&spidev->buf_lock);
//	k21_spi_dbg("%s cmd=%d\n",__func__,cmd);
	switch (cmd) {
	/* read requests */
	case SPI_IOC_RD_MODE:
		retval = __put_user(spi->mode & SPI_MODE_MASK,
					(__u8 __user *)arg);
		break;
	case SPI_IOC_RD_LSB_FIRST:
		retval = __put_user((spi->mode & SPI_LSB_FIRST) ?  1 : 0,
					(__u8 __user *)arg);
		break;
	case SPI_IOC_RD_BITS_PER_WORD:
		retval = __put_user(spi->bits_per_word, (__u8 __user *)arg);
		break;
	case SPI_IOC_RD_MAX_SPEED_HZ:
		retval = __put_user(spi->max_speed_hz, (__u32 __user *)arg);
		break;
	/* write requests */
	case SPI_IOC_WR_MODE:
		retval = __get_user(tmp, (u8 __user *)arg);
		if (retval == 0) {
			u8	save = spi->mode;

			if (tmp & ~SPI_MODE_MASK) {
				retval = -EINVAL;
				break;
			}

			tmp |= spi->mode & ~SPI_MODE_MASK;
			spi->mode = (u8)tmp;
			retval = spi_setup(spi);
			if (retval < 0)
				spi->mode = save;
			else
				dev_dbg(&spi->dev, "spi mode %02x\n", tmp);
		}
			spidev_wakeup_k21();
		break;
	case SPI_IOC_WR_LSB_FIRST:
		retval = __get_user(tmp, (__u8 __user *)arg);
		if (retval == 0) {
			u8	save = spi->mode;

			if (tmp)
				spi->mode |= SPI_LSB_FIRST;
			else
				spi->mode &= ~SPI_LSB_FIRST;
			retval = spi_setup(spi);
			if (retval < 0)
				spi->mode = save;
			else
				dev_dbg(&spi->dev, "%csb first\n",
						tmp ? 'l' : 'm');
		}
			spidev_wakeup_k21();
		break;
	case SPI_IOC_WR_BITS_PER_WORD:
		retval = __get_user(tmp, (__u8 __user *)arg);
		if (retval == 0) {
			u8	save = spi->bits_per_word;
		
			spi->bits_per_word = tmp;
			retval = spi_setup(spi);
				k21_spi_dbg("save =%d tmp=%d retval=%d\n",save,tmp,retval);
			if (retval < 0)
				spi->bits_per_word = save;
			else
				dev_dbg(&spi->dev, "%d bits per word\n", tmp);
		}
		
		break;
	case SPI_IOC_WR_MAX_SPEED_HZ:
		retval = __get_user(tmp, (__u32 __user *)arg);
		if (retval == 0) {
			u32	save = spi->max_speed_hz;

			spi->max_speed_hz = tmp;
			retval = spi_setup(spi);
			if (retval < 0)
				spi->max_speed_hz = save;
			else
				dev_dbg(&spi->dev, "%d Hz (max)\n", tmp);
		}
		
		break;

	default:
		/* segmented and/or full-duplex I/O request */
		if (_IOC_NR(cmd) != _IOC_NR(SPI_IOC_MESSAGE(0))
				|| _IOC_DIR(cmd) != _IOC_WRITE) {
			retval = -ENOTTY;
			break;
		}

		tmp = _IOC_SIZE(cmd);
		if ((tmp % sizeof(struct spi_ioc_transfer)) != 0) {
			retval = -EINVAL;
			break;
		}
		n_ioc = tmp / sizeof(struct spi_ioc_transfer);
		if (n_ioc == 0)
			break;

		/* copy into scratch area */
		ioc = kmalloc(tmp, GFP_KERNEL);
		if (!ioc) {
			retval = -ENOMEM;
			break;
		}
		if (__copy_from_user(ioc, (void __user *)arg, tmp)) {
			kfree(ioc);
			retval = -EFAULT;
			break;
		}

		/* translate to spi_message, execute */
		retval = spidev_message(spidev, ioc, n_ioc);
		kfree(ioc);
		break;
	}

	mutex_unlock(&spidev->buf_lock);
	spi_dev_put(spi);

	return retval;
}



#ifdef CONFIG_COMPAT
static long
spidev_compat_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	return spidev_ioctl(filp, cmd, (unsigned long)compat_ptr(arg));
}
#else
#define spidev_compat_ioctl NULL
#endif /* CONFIG_COMPAT */

static int spidev_open(struct inode *inode, struct file *filp)
{
	struct spidev_data	*spidev;
	int			status = -ENXIO;
	
	mutex_lock(&device_list_lock);
	//enable_irq(irq);
	list_for_each_entry(spidev, &device_list, device_entry) {
		if (spidev->devt == inode->i_rdev) {
			status = 0;
			break;
		}
	}
	if (status == 0) {
		if (!spidev->buffer) {
			spidev->buffer = kmalloc(bufsiz, GFP_KERNEL);
			if (!spidev->buffer) {
				dev_dbg(&spidev->spi->dev, "open/ENOMEM\n");
				status = -ENOMEM;
			}
		}
		
		if (status == 0) {
			spidev->users++;
			filp->private_data = spidev;
			nonseekable_open(inode, filp);
		}
		k21_spi_dbg("==== %s ,spidev->users = %d======\n",__func__,spidev->users);
		//k21_spi_readflag = 1;
		//wake_up_interruptible(&log_wait_queue);//唤醒 读线程
	} else
		pr_debug("spidev: nothing for minor %d\n", iminor(inode));

	mutex_unlock(&device_list_lock);
	
	return status;
}

static int spidev_release(struct inode *inode, struct file *filp)
{
	struct spidev_data	*spidev;
	int			status = 0;
	

	mutex_lock(&device_list_lock);
	spidev = filp->private_data;
	filp->private_data = NULL;
	k21_spi_dbg("==== %s ,spidev->users = %d======\n",__func__,spidev->users);
	//suspend_status = 1;
	/* last close? */
	spidev->users--;
	if (!spidev->users) {
		int		dofree;

		kfree(spidev->buffer);
		spidev->buffer = NULL;

		/* ... after we unbound from the underlying device? */
		spin_lock_irq(&spidev->spi_lock);
		dofree = (spidev->spi == NULL);
		spin_unlock_irq(&spidev->spi_lock);

		if (dofree)
			kfree(spidev);
		disable_irq(irq);
		disable_irq_wake(irq);
	}
	mutex_unlock(&device_list_lock);

	return status;
}

static const struct file_operations spidev_fops = {
	.owner =	THIS_MODULE,
	/* REVISIT switch to aio primitives, so that userspace
	 * gets more complete API coverage.  It'll simplify things
	 * too, except for the locking.
	 */
	.write =	spidev_write,
	.read =		/*spidev_read*//*spidev_read_block*/spidev_read_noblock,
	.unlocked_ioctl = spidev_ioctl,
	.compat_ioctl = spidev_compat_ioctl,
	.open =		spidev_open,
	.release =	spidev_release,
	.poll = spidev_poll,
	.llseek =	no_llseek,
};
#if K21_SPI

static int g_spidev_tampered = 0;
/*
static ssize_t spide_k21_show_tamper(struct device *dev,
                struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", g_spidev_tampered);
}
*/
/*
static ssize_t spide_k21_set_tamper(struct device *dev,
                struct device_attribute *attr,
		        const char *buf, size_t count)
{
    int value = 0;

    sscanf(buf, "%d", &value);
    g_spidev_tampered = value;
    if(g_spidev_tampered){
	printk(KERN_EMERG"spide_k21_set_tamper %d\n",g_spidev_tampered);	
    }else{
		printk(KERN_EMERG"spide_k21_set_tamper %d\n",g_spidev_tampered);	    
    }

	return count;
}
*/

//static struct device_attribute spide_k21_attrs[] = {
//	__ATTR(k21_tampered, 0777, spide_k21_show_tamper, NULL/* spide_k21_set_tamper*/),
//	__ATTR_NULL,
//};



/*****************************************************
接收数据
*****************************************************/


//typedef struct read_data NODE;

static ssize_t thread_read_addbuf(u8 *buf,u16 len)
{
	NODE *pnew;
	int i = 0;

	if(buf[i] ==0x02 && buf[i+1] ==0x4A&& buf[i+2] ==0x4C&& buf[i+3] ==0x54&& buf[i+4] ==0x53&& buf[i+5] ==0x45&& buf[i+6] ==0x43)
	{
		if(down_interruptible(&mutex)){
			 return -ERESTARTSYS;
		 }
		
		pnew = create_nod(buf,len);
		if(pnew==NULL)  //创建失败返回 
		{
		   k21_spi_dbg("创建失败！");
		   return -ENOMEM;
		} 
		insert(Lhead,pnew );   //将新节点插入链表尾
		
		up(&mutex);
	}
	else
	{
		printk("vaild command, droped!");
		//printk(KERN_EMERG"len=%d Rdata:",len);
		//for(i=0;i<len;i++)
		//	printk(KERN_EMERG"%02X  ",buf[i]);
		//printk(KERN_EMERG"\n");
		return  -EFAULT;
	}
		
	return 0;
}

#define DMA_BUFFER_SIZE	1024
static ssize_t thread_read(struct spidev_data *spidev,char *buf, size_t count, int offset)
{
	ssize_t	status = 0;
	//int total,curr,sendcnt;
	//char *p;
	if (count > bufsiz)
		return -EMSGSIZE;

	mutex_lock(&spidev->buf_lock);
	status = spidev_sync_read(spidev, count + offset);
	if (status >= 0) {
		memcpy(buf, spidev->buffer + offset, count);
	} else
		k21_spi_dbg(KERN_ERR "Warning spidev_sync_read failed !!! status:%ld \n", (long int)status);

#if 0
	//total = count/DMA_BUFFER_SIZE;
	//p = buf;
	//curr = 0;
	//k21_spi_dbg( "thread_read1: total=%d, curr = %d\n",total,curr);
	//while (curr <= total)
	//{
	//	if (curr == total)
	//		sendcnt = count - curr*DMA_BUFFER_SIZE;
	//	else
	//		sendcnt = DMA_BUFFER_SIZE;
		
	//	status = spidev_sync_read(spidev, sendcnt);
	//	if (status >= 0) {
	//		memcpy(p, spidev->buffer, sendcnt);
	//		p +=sendcnt;
	//		curr ++;
	//	}
	//	else
	//		break;
	//}
#endif
	mutex_unlock(&spidev->buf_lock);
	
	return status;
}

u16 crc_ta_4[16] = { /* CRC 半字节余式表 */
    0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
    0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
};

static u16 crc_checksum(u8 * pBuf, int crcDataLen)
{
	
	u16 crc = 0;
	u8 high;

	while(0 != crcDataLen--)
	{
		high = (u8) (crc / 4096); //暂存CRC的高4位
		crc <<= 4;
		crc ^= crc_ta_4[high ^ (*pBuf / 16)];
//		android_debug("%02X ",*pBuf);
		high = (u8) (crc / 4096);
		crc <<= 4;
		crc ^= crc_ta_4[high ^ (*pBuf & 0x0f)];
	
		pBuf++;
		
	}
//	android_debug("crc =%02X \n",crc);
	return crc;
	
}

static ssize_t thread_write(struct spidev_data *spidev,char *buf, size_t count)
{
	ssize_t	status = 0;

	if (count > DMA_BUFFER_SIZE)
		return -EMSGSIZE;

	mutex_lock(&spidev->buf_lock);
	
	memcpy( spidev->buffer, buf,count);
	status = spidev_sync_write(spidev, count);
	
	mutex_unlock(&spidev->buf_lock);
	
	return status;
}

static void handle_tampered (struct spidev_data	*spidev ,char *buf, size_t count)
{
	u8 ackbuf[] = {0x02, 0x4A, 0x4C, 0x54, 0x53, 0x45, 0x43, 0x00, 0x6C, 0x03, 0x00, 0x00,0x03,0x00,0x00};
	u16 crc; 
	//int i;

	printk("**** handle_tampered() ******\n");
	crc = crc_checksum(&ackbuf[8],4);
	//printk(KERN_EMERG"crc = 0x%x\n",crc);
	ackbuf[14] = crc ;
	ackbuf[13] = crc >> 8;
	/*
	printk(KERN_EMERG"ackbuf count=%d Rdata:",count);
	for(i=0; i<count; i++)
		printk(KERN_EMERG"%02X  ",buf[i]);
	printk(KERN_EMERG"\n");
	*/
	spidev_wakeup_k21();
	thread_write(spidev, ackbuf,15);
	
	input_report_key(spikeydev, KEY_F1, 1);
	input_sync(spikeydev);
	udelay(100);
	input_report_key(spikeydev, KEY_F1/*KEY_POWER*/, 0);
	input_sync(spikeydev);
	g_spidev_tampered = 1;

}
static unsigned int k21_scankeycodes[16] = {
		[0 ] = KEY_RESERVED,
		[1 ] = KEY_1,
		[2 ] = KEY_2,
		[3 ] = KEY_3,
		[4 ] = KEY_4,
		[5 ] = KEY_5,
		[6 ] = KEY_6,
		[7 ] = KEY_7,
		[8 ] = KEY_8,
		[9 ] = KEY_9,
		[10] = KEY_NUMERIC_STAR,
		[11] = KEY_0,
		[12] = KEY_NUMERIC_POUND,
		[13] = KEY_BACK,
		[14] = KEY_BACKSPACE,
		[15] = KEY_ENTER,
	};

static void handle_key_event(char *buf, size_t count)
{
	
	int scancode;
	u16 crc;

	//printk(KERN_EMERG"*** handle_key_event *** count = %d\n",count);
	if (count <21) return;
	
	crc = crc_checksum(&buf[8],13);
	scancode = buf[17];
	if (scancode < 1 || scancode > 15 ) 
	{
		//printk(KERN_EMERG"handle_key_event, unknown scancode %d \n",scancode);
		return;
	}
	//printk(KERN_EMERG"handle_key_event, crc = 0x%x, scancode = %d[%d] \n",crc,scancode,k21_scankeycodes[scancode]);
	input_report_key(spikeydev, k21_scankeycodes[scancode], 1);
	input_sync(spikeydev);
	input_report_key(spikeydev, k21_scankeycodes[scancode], 0);
	input_sync(spikeydev);
	
}

static int thread_k21_get_spi_data(void *spidevdata)
{
	u8 bufdata[TATOL_DATA_LEN]={0};
	u16 datanum, len;
	char *p = NULL;
	struct spidev_data *spidev = (struct spidev_data *)spidevdata;
	int val,i;
	//xuml add 0828
 
    do {  
		if(	gpio_get_value(spi_int_gpio) == 1 || bootFlag) {
        	wait_event_interruptible(log_wait_queue,k21_spi_readflag == 1);
		}
		if(k21_spi_readflag == 3)//结束标志
			return 0;

		while(gpio_get_value(spi_int_gpio) == 0 && suspend_status == 0)
		{
			int remian, total;
			p = bufdata;
			datanum = 0;
			len = 30;
			memset(p, 0, TATOL_DATA_LEN);
			val = thread_read(spidev, p, len, 0);//读命令头13byte
			for(i=0;i<19;i++)//根据ACK结构体知包头最大允许包含8字节乱数据
			{	
				if(bufdata[i] ==0x02 && bufdata[i+1] ==0x4A&& bufdata[i+2] ==0x4C&& bufdata[i+3] ==0x54&& bufdata[i+4] ==0x53&& bufdata[i+5] ==0x45&& bufdata[i+6] ==0x43)
				{
					break;
				}	
			}

			//if(i >= len-10 -1 )//数据帧错误
			if (i >= 19)//数据帧错误
			{
				p += len;

				while(gpio_get_value(spi_int_gpio) == 0) {
					thread_read(spidev, p, TATOL_DATA_LEN-len, 0);
				}
				/*//SPI驱动打印太多导致开机异常
				k21_spi_dbg(KERN_ERR "xuml data err\r\n");
				for (j=0; j<TATOL_DATA_LEN; j++)
				{	
					k21_spi_dbg(KERN_ERR " %X ", bufdata[j]);
					if (j%32 == 0) k21_spi_dbg(KERN_ERR "\r\n");
				}
				*/
				k21_spi_readflag = 0;
				have_test = true;
				wake_up_interruptible(&write_wait);  //wakeup APP read task 
				
				break;
			}

			len -= i;//有效的数据长度
			memcpy(bufdata,&bufdata[i],len);
			//bufdata[0] = 0x02;
			//bufdata[1] = 0x4A;
			p += len;
			datanum +=len;
			//      bufdata[10] [11](数据长度)+ i(每次获取前面i个字节是错误的,所以此处还要加上i)
			//len = bufdata[10]*256+bufdata[11]+i-4;//内容end sig 1byte, crc 2bytes, respone code 1 byte
			total = bufdata[10]*256+bufdata[11];//
			//remain表示还需要读取多少个字节
			//12 表示Start of Text,Flag,Serial No.,Command,Data length. 
			//len-12表示已经读取了多少个数据字节,
			//4表示最后的校验码等。
			remian = total - (len - 12) + 4;
			//k21_spi_dbg("len =%d total=%d  remian =%d i=%d \n",len, total, remian, i);
			if((remian > 0)&&(remian <= 768))
			{
				val = thread_read(spidev, p, remian, 0);//读命令内容
				datanum = datanum + remian;

				#if 0
				k21_spi_dbg(KERN_EMERG"2 datanum=%d Rdata:",datanum);
				for(i=0;i<datanum;i++)
					k21_spi_dbg(KERN_EMERG"%02X  ",bufdata[i]);
				k21_spi_dbg(KERN_EMERG"\n");
				#endif
			} else 
				printk("bufdata[8],bufdata[9]: %02X  %02X,   Wrong data size = %d !!!!!!!\n",bufdata[8],bufdata[9],remian);
							
			if (datanum > 19 && bufdata[8] ==  0x6C &&  bufdata[9] == 0x03 )	
				handle_tampered(spidev,bufdata,datanum);	//响应触发报文
			else if (datanum > 19 && bufdata[8] ==  0x61 &&  bufdata[9] == 0x05 )	
				handle_key_event(bufdata,datanum);	//响应按键
			else  if ( spidev->users > 1 && !thread_read_addbuf(bufdata,datanum))
			{
				have_data = true;
				while(gpio_get_value(spi_int_gpio) == 0)
				{
					//k21_spi_dbg(KERN_EMERG"V %d \n",gpio_get_value(spi_int_gpio) == 0);
				}

				k21_spi_readflag = 0; 
				wake_up_interruptible(&read_wait);  //wakeup APP read task 
			}

		}

		k21_spi_readflag = 0;  //必须加这一行，内核才会进行调度。内核线程不像应用程序会主动调度，我们需要显式的使用调度函数，想要在thread_function_1中去重置tc的值是不可能的，因为线程不会被调度，该线程会一直占用CPU
	}while(!kthread_should_stop());  
	return 0;
}

/*****************************************************
接收数据中断信号 下降沿
*****************************************************/

static irqreturn_t k21_interrupt(int irq, void* dev_id)
{
	//printk(KERN_EMERG"k21 irq\n");
			
	if(gpio_get_value(spi_int_gpio) == 0)
	{
		#if 1
		if(1 == k21_spi_readflag)
		{
			//wake_up_interruptible(&log_wait_queue);//唤醒 读线程
			return IRQ_HANDLED;//避免K21紊乱导致快速唤醒收中断
		}
		#endif
		k21_spi_readflag = 1;
		wake_up_interruptible(&log_wait_queue);//唤醒 读线程
	}
	else {
		k21_spi_readflag = 0;
	}
		
	return IRQ_HANDLED;
}
#endif




static int spidev_parse_dt(struct device *dev) {	
	struct device_node *np;	
	
	if (!dev) {		
		return -ENODEV;	
	}	
	k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	
	
	 
	np = dev->of_node;	
	spi_int_gpio = of_get_named_gpio(np, "jolly,irq-gpio", 0);	
		k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	ap_wakeup_sp_gpio = of_get_named_gpio(np, "jolly,ap-wakeup-sp-gpio", 0);	
		k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	check_sp_status_gpio = of_get_named_gpio(np, "jolly,check-sp-status-gpio", 0);
		k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	//en_sp_gpio = of_get_named_gpio(np, "jolly,en-sp-gpio", 0);
	ctrl_scaner_gpio = of_get_named_gpio(np, "jolly,ctrl-scaner-power-gpio", 0);
		k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	ctrl_id_gpio = of_get_named_gpio(np, "jolly,ctrl-id-power-gpio", 0);
		k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	//ctrl_uart_0_gpio = of_get_named_gpio(np, "jolly,ctrl-uart-zero-gpio", 0);
	//ctrl_uart_1_gpio = of_get_named_gpio(np, "jolly,ctrl-uart-one-gpio", 0);
	
	if (!gpio_is_valid(spi_int_gpio) || !gpio_is_valid(ap_wakeup_sp_gpio)
		|| !gpio_is_valid(check_sp_status_gpio)) { 
		k21_spi_dbg(KERN_ERR"Invalid GPIO, irq-gpio:%d, wakeup-gpio:%d, status-gpio:%d \n", 
			spi_int_gpio, ap_wakeup_sp_gpio, check_sp_status_gpio);
		return -EINVAL;
	}
	k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
/*
	if (!gpio_is_valid(ctrl_scaner_gpio) || !gpio_is_valid(ctrl_uart_0_gpio)
		|| !gpio_is_valid(ctrl_uart_1_gpio)) { 
		k21_spi_dbg(KERN_ERR"Invalid GPIO, ctrl_scaner_gpio:%d, ctrl_uart_0_gpio:%d, ctrl_uart_1_gpio:%d \n", 
			ctrl_scaner_gpio, ctrl_uart_0_gpio, ctrl_uart_1_gpio);
		return -EINVAL;
	}
*/
	return 0;
	
}


static int  spidev_request_io_port(struct spi_device *spi) {
	int ret = 0;

	if (!spi) {
		return -ENODEV;
	}
	k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	
 
	ret = gpio_request(spi_int_gpio, "SPI_INT_IRQ");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request spi_int_gpio:%d, ERROR:%d", spi_int_gpio, ret);
		return -ENODEV;
	} else {
		gpio_set_value(spi_int_gpio, 1);
		gpio_direction_input(spi_int_gpio);
		spi->irq = gpio_to_irq(spi_int_gpio);
		irq = spi->irq;
	}
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	ret = gpio_request(ap_wakeup_sp_gpio, "AP_WAKEUP_SP");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request ap_wakeup_sp_gpio:%d, ERROR:%d", ap_wakeup_sp_gpio, ret);
		return -ENODEV;
	} else {
		gpio_direction_output(ap_wakeup_sp_gpio, 0);
		spi_wake_k21_pio_vaild = 1;
	}
/*
	ret = gpio_request(en_sp_gpio, "AP_EN_SP");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request ap_wakeup_sp_gpio:%d, ERROR:%d", en_sp_gpio, ret);
		return -ENODEV;
	} else {
		gpio_direction_output(en_sp_gpio, 1);
	}
*/
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	ret = gpio_request(check_sp_status_gpio, "CHECK_SP_STATUS");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request check_sp_status_gpio:%d, ERROR:%d", check_sp_status_gpio, ret);
		return -ENODEV;
	} else {
		gpio_direction_input(check_sp_status_gpio);
		k21_wakeup_status_pio_vaild = 1;
	}
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	ret = gpio_request(ctrl_scaner_gpio, "CTRL_SCANER");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request ctrl_scaner_gpio:%d, ERROR:%d", ctrl_scaner_gpio, ret);
		return -ENODEV;
	} else {
		gpio_direction_output(ctrl_scaner_gpio, 0);
	}

k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	ret = gpio_request(ctrl_id_gpio, "CTRL_ID");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request ctrl_id_gpio:%d, ERROR:%d", ctrl_id_gpio, ret);
		return -ENODEV;
	} else {
		gpio_direction_output(ctrl_id_gpio, 0);
	}
/*
	ret = gpio_request(ctrl_uart_0_gpio, "CTRL_UART_0");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request ctrl_uart_0_gpio:%d, ERROR:%d", ctrl_uart_0_gpio, ret);
		return -ENODEV;
	} else {
		gpio_direction_output(ctrl_uart_0_gpio, 0);
	}

	ret = gpio_request(ctrl_uart_1_gpio, "CTRL_UART_1");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request ctrl_uart_1_gpio:%d, ERROR:%d", ctrl_uart_1_gpio, ret);
		return -ENODEV;
	} else {
		gpio_direction_output(ctrl_uart_1_gpio, 0);
	}
*/
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	return ret;
}


static int spidev_request_irq (struct spi_device *spi) {
	int ret = 0;
	
	if (!spi) {
		return -ENODEV;
	}

 
	k21_spi_dbg(KERN_EMERG "spidev_request_irq, spi->irq%d", spi->irq);
	ret = request_irq(spi->irq, k21_interrupt, IRQF_TRIGGER_FALLING, DRV_NAME, spi);
	if (ret) {
		k21_spi_dbg(KERN_EMERG "Failed to request irq, ERROR:%d", ret);
		gpio_free(spi_int_gpio);
	}

	return ret;
}


/*-------------------------------------------------------------------------*/

/* The main reason to have this class is to make mdev/udev create the
 * /dev/spidevB.C character device nodes exposing our userspace API.
 * It also simplifies memory management.
 */

static struct class *spidev_class;

/*-------------------------------------------------------------------------*/


/*
*控制扫描头电源开关
*/
static ssize_t spidev_scaner_show(struct device *dev,
                struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(ctrl_scaner_gpio));
}


static ssize_t spidev_scaner_store(struct device *dev,
                struct device_attribute *attr,
		        const char *buf, size_t count)
{
    int value = 0;

    sscanf(buf, "%d", &value);
    gpio_set_value(ctrl_scaner_gpio, value);
	
	return count;
}

static DEVICE_ATTR(scaner_power, 0666, spidev_scaner_show, spidev_scaner_store);




static ssize_t spidev_id_show(struct device *dev,
                struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(ctrl_id_gpio));
}


static ssize_t spidev_id_store(struct device *dev,
                struct device_attribute *attr,
		        const char *buf, size_t count)
{
    int value = 0;

    sscanf(buf, "%d", &value);
    gpio_set_value(ctrl_id_gpio, value);
	
	return count;
}

static DEVICE_ATTR(id_power, 0666, spidev_id_show, spidev_id_store);


/*
*控制blsp1_uart2功能
*/
	/*

static ssize_t spidev_uart_show(struct device *dev,
                struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "S1:%d,S2:%d \n", gpio_get_value(ctrl_uart_0_gpio), gpio_get_value(ctrl_uart_1_gpio));
}


static ssize_t spidev_uart_store(struct device *dev,
                struct device_attribute *attr,
		        const char *buf, size_t count)
{
    int value = 0;
	
    sscanf(buf, "%d", &value);
	switch (value) {
		case 0:
			gpio_set_value(ctrl_uart_0_gpio, 0);
			gpio_set_value(ctrl_uart_1_gpio, 0);
			break;
		case 1:
			gpio_set_value(ctrl_uart_0_gpio, 1);
			gpio_set_value(ctrl_uart_1_gpio, 0);
			break;
		case 2:
			gpio_set_value(ctrl_uart_0_gpio, 0);
			gpio_set_value(ctrl_uart_1_gpio, 1);
			break;
		case 3:
			gpio_set_value(ctrl_uart_0_gpio, 1);
			gpio_set_value(ctrl_uart_1_gpio, 1);
			break;
	}
    
	return count;
}

static DEVICE_ATTR(ctrl_uart, 0664, spidev_uart_show, spidev_uart_store);

static struct attribute *scaner_attrs[] = {
	&dev_attr_scaner_power.attr,
	&dev_attr_ctrl_uart.attr,
	NULL
};

static struct attribute_group scaner_attr_group = {
	.attrs = scaner_attrs,
};
*/

static int spidev_probe(struct spi_device *spi)
{
	struct spidev_data	*spidev;
	int			status,i;
	unsigned long		minor;
	int ret;

	k21_spi_dbg(KERN_ERR "[%s, %d] has enter  \n", __func__, __LINE__);

	/* Allocate driver data */
	spidev = kzalloc(sizeof(*spidev), GFP_KERNEL);
	if (!spidev)
		return -ENOMEM;
	 k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
#if K21_SPI
 
	/* Initialize the driver irq */
#ifdef  CONFIG_OF	
	if (spi->dev.of_node) {		
		if (spidev_parse_dt(&spi->dev) != 0) {
			k21_spi_dbg(KERN_ERR "spidev_parse_dt Failed!\n");  
			return -1;
		}	
	}
#endif
 
	spidev_request_io_port(spi);
	if (spidev_request_irq(spi)) {
		k21_spi_dbg(KERN_ERR "spidev_request_irq Failed!\n");  
		return -1;
	}
 
	
	sema_init(&mutex,1);
//	init_waitqueue_head(&rdwait);//阻塞读
//	init_waitqueue_head(&wrwait);//阻塞写
	/* Initialize the driver read data thread 内核线程*/	
	init_waitqueue_head(&log_wait_queue);
	//init_waitqueue_head(&spidev->poll_queue);

//	kthread_create(k21_get_spi_read_data, NULL, CLONE_KERNEL|SIGCHLD);
	g_spidev = spidev;
	_tsk = kthread_run(thread_k21_get_spi_data, (void*)spidev, "mythread");
 	if (IS_ERR(_tsk)) {  //需要使用IS_ERR()来判断线程是否有效，后面会有文章介绍IS_ERR()

        k21_spi_dbg(KERN_ERR"first create kthread failed!\n");  
    }  
	disable_irq(irq);

	Lhead = create_nod(NULL,0);
	
	if(spi_wake_k21_pio_vaild) 
		gpio_set_value(ap_wakeup_sp_gpio, 1);//初始高电平
		
	//k21_spi_firstreadflag = 2;//特殊标记 第一次读spi需要越过两个字节
#endif	
 

	/* Initialize the driver data */
	spidev->users = 0;
	spidev->spi = spi;
	spin_lock_init(&spidev->spi_lock);
	mutex_init(&spidev->buf_lock);

	INIT_LIST_HEAD(&spidev->device_entry);

	/* If we can allocate a minor number, hook up this device.
	 * Reusing minors is fine so long as udev or mdev is working.
	 */
	mutex_lock(&device_list_lock);
	minor = find_first_zero_bit(minors, N_SPI_MINORS);
	if (minor < N_SPI_MINORS) {
		struct device *dev;
		spidev->devt = MKDEV(SPIDEV_MAJOR, minor);
		dev = device_create(spidev_class, &spi->dev, spidev->devt,
				    spidev, DRV_NAME"-%d.%d",
				    spi->master->bus_num, spi->chip_select);
		status = IS_ERR(dev) ? PTR_ERR(dev) : 0;

		if (dev) {
			ret = device_create_file(dev, &dev_attr_scaner_power);
			ret = device_create_file(dev, &dev_attr_id_power);
		}
	} else {
		dev_dbg(&spi->dev, "no minor number available!\n");
		status = -ENODEV;
	}
	if (status == 0) {
		set_bit(minor, minors);
		list_add(&spidev->device_entry, &device_list);
	}
	mutex_unlock(&device_list_lock);
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	if (status == 0)
		spi_set_drvdata(spi, spidev);
	else
		kfree(spidev);
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	spikeydev = input_allocate_device();
	if (!spikeydev) {
		kfree(spikeydev);
		return -ENODEV;
	}
	spikeydev->name ="spidev-k21";
	spikeydev->phys = "spikbd/input0";
	spikeydev->id.bustype = BUS_HOST;
	spikeydev->id.vendor = 0x0001;
	spikeydev->id.product = 0x0001;
	spikeydev->id.version = 0x0100;
	spikeydev->open = NULL;
	spikeydev->close = NULL;
	//spikeydev->dev.parent = &pdev->dev;
	set_bit(EV_KEY, spikeydev->evbit);
	set_bit(EV_REL, spikeydev->evbit);
	set_bit(KEY_F1, spikeydev->keybit);
	k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	for ( i = 0; i < 16; i++)
		set_bit( k21_scankeycodes[i], spikeydev->keybit);
	
	ret = input_register_device(spikeydev);
	if(ret)
		printk(KERN_EMERG"Unable to Register the spi key\n");
	k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	if (!spidev->buffer) {
		spidev->buffer = kmalloc(bufsiz, GFP_KERNEL);
		if (!spidev->buffer) {
			dev_dbg(&spidev->spi->dev, "open/ENOMEM\n");
		}
	}
	spidev->users++;
	bootFlag = false;
	enable_irq(irq);
	enable_irq_wake(irq);
	printk(KERN_EMERG"********** spidev-k21 irq = %d ************\n",irq);
	return status;
}

static int  spidev_remove(struct spi_device *spi)
{
	struct spidev_data	*spidev = spi_get_drvdata(spi);

#if K21_SPI
	k21_spi_dbg("%s\n",__func__);

	k21_spi_readflag = 3;
	wake_up_interruptible(&log_wait_queue);//唤醒 读线程

	disable_irq(irq);
	free_irq(irq, spidev->spi );
	if (!IS_ERR(_tsk)){  

        int ret = kthread_stop(_tsk);  

        k21_spi_dbg(KERN_INFO "First thread function has stopped ,return %d\n", ret);  

    } 
	kfree(Lhead);
#endif
	/* make sure ops on existing fds can abort cleanly */
	spin_lock_irq(&spidev->spi_lock);
	spidev->spi = NULL;
	spi_set_drvdata(spi, NULL);
	spin_unlock_irq(&spidev->spi_lock);

	/* prevent new opens */
	mutex_lock(&device_list_lock);
	list_del(&spidev->device_entry);
	device_destroy(spidev_class, spidev->devt);
	clear_bit(MINOR(spidev->devt), minors);
	if (spidev->users == 0)
		kfree(spidev);
	mutex_unlock(&device_list_lock);

	input_unregister_device(spikeydev);
	kfree(spikeydev);
	return 0;
}
#ifdef CONFIG_HAS_EARLYSUSPEND

//停滞状态
static int spidev_suspend(struct early_suspend *h)
{
	#if 0
    k21_spi_dbg("==== spidev_suspend ===== \n");
   spidev_wakeup_k21(0);
	#endif
    return 0;
}
//唤醒
static int spidev_resume(struct early_suspend *h)
{
    //printk(KERN_EMERG"===== early spidev_resume ===== \n");
   spidev_wakeup_k21();

    return 0;
}
#endif
 
static int pm_spidev_suspend(struct device *dev)
{
	suspend_status = 1;
//	gpio_set_value(en_sp_gpio, 0); 
	enable_irq_wake(irq);
	return 0;
}

static int pm_spidev_resume(struct device *dev)
{
	disable_irq_wake(irq);
	if(gpio_get_value(spi_int_gpio) == 0)
	{
		printk(KERN_ERR"spi interrupt!\n");
		k21_spi_readflag = 1;
		wake_up_interruptible(&log_wait_queue);//唤醒 读线程
	}
//	gpio_set_value(en_sp_gpio, 1); 
	suspend_status = 0;
	return 0;
}

static const struct dev_pm_ops spidev_pm_ops = {
	.suspend = pm_spidev_suspend,
	.resume  = pm_spidev_resume,
};
#define SPI_DEV_PM_OPS (&spidev_pm_ops)



static struct of_device_id k21spi_match_table[] = {
        {.compatible = "jolly,ap-k21-spi",},
        { },
};

MODULE_DEVICE_TABLE(of, k21spi_match_table);

static struct spi_driver spidev_spi_driver = {
	.driver = {
		.name =		DRV_NAME,
		.owner =	THIS_MODULE,
		.pm 	= SPI_DEV_PM_OPS,
		.of_match_table = of_match_ptr(k21spi_match_table),
	},
	.probe =	spidev_probe,
	.remove =	spidev_remove,//__devexit_p(spidev_remove),

	/* NOTE:  suspend/resume methods are not necessary here.
	 * We don't do anything except pass the requests to/from
	 * the underlying controller.  The refrigerator handles
	 * most issues; the controller driver handles the rest.
	 */
};



/*-------------------------------------------------------------------------*/

static int __init spidev_init(void)
{
	int status;

	/* Claim our 256 reserved device numbers.  Then register a class
	 * that will key udev/mdev to add/remove /dev nodes.  Last, register
	 * the driver which manages those device numbers.
	 */
	 k21_spi_dbg(KERN_ERR "%s\n",__func__);
	BUILD_BUG_ON(N_SPI_MINORS > 256);
	k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	status = register_chrdev(SPIDEV_MAJOR, DRV_NAME, &spidev_fops);
	if (status < 0)
		return status;
k21_spi_dbg(KERN_ERR "%s *1\n",__func__);
	spidev_class = class_create(THIS_MODULE, DRV_NAME);
	if (IS_ERR(spidev_class)) {
		unregister_chrdev(SPIDEV_MAJOR, spidev_spi_driver.driver.name);
		return PTR_ERR(spidev_class);
	}
k21_spi_dbg(KERN_ERR "%s *2\n",__func__);
	//spidev_class->dev_attrs = &dev_attr_scaner;
	
	status = spi_register_driver(&spidev_spi_driver);
	if (status < 0) {
		class_destroy(spidev_class);
		unregister_chrdev(SPIDEV_MAJOR, spidev_spi_driver.driver.name);
	}
 #ifdef CONFIG_HAS_EARLYSUSPEND
		early_suspend.suspend = spidev_suspend;
		early_suspend.resume = spidev_resume;
		register_early_suspend(&early_suspend);
#endif
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	return status;
}
module_init(spidev_init);

static void __exit spidev_exit(void)
{
	k21_spi_dbg("%s\n",__func__);
	spi_unregister_driver(&spidev_spi_driver);
	class_destroy(spidev_class);
	unregister_chrdev(SPIDEV_MAJOR, spidev_spi_driver.driver.name);
 #ifdef CONFIG_HAS_EARLYSUSPEND
	  unregister_early_suspend(&early_suspend);
#endif 
}
module_exit(spidev_exit);

MODULE_AUTHOR("chuck");
MODULE_DESCRIPTION("User mode SPI device interface");
MODULE_LICENSE("GPL");
MODULE_ALIAS("spi:spidev");

