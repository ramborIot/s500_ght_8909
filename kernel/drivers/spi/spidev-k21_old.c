#include <linux/init.h>
#include <linux/module.h>
#include <linux/ioctl.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/list.h>
#include <linux/errno.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/compat.h>

#include <linux/spi/spi.h>
#include <linux/spi/spidev.h>

#include <asm/uaccess.h>


#include <linux/platform_device.h>
//#include <mach/sys_config.h>
//#include <mach/gpio.h>
#include <linux/wait.h>

#include <linux/interrupt.h>

#include <linux/io.h>

#include <linux/kthread.h>

#include <linux/gpio.h>
#include <linux/wait.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/semaphore.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/delay.h>
#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/pm.h>
#include <linux/earlysuspend.h>
#endif
#include <linux/poll.h> 
#include <linux/input.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_address.h>
#include <linux/of_platform.h>
#include <linux/of_gpio.h>
#include <linux/time.h>



#define K21_SPI 1
#define DRV_NAME	"spidev-k21"
#define TATOL_DATA_LEN 1024

static u32 irq = 0;
static struct task_struct * _tsk;  

int spi_wake_k21_pio_vaild=0;


int k21_wakeup_status_pio_vaild=0;
static bool have_data = false; 
static bool have_test = false;
static bool bootFlag = true;

static  struct input_dev * spikeydev;
struct spidev_data	*g_spidev = 0;
static int suspend_status = 0;

#ifdef CONFIG_OF
static int spi_int_gpio;
static int ap_wakeup_sp_gpio;
static int check_sp_status_gpio;
//static int en_sp_gpio;
static int ctrl_scaner_gpio;
static int ctrl_id_gpio;
//static int ctrl_uart_0_gpio;
//static int ctrl_uart_1_gpio;
#endif


static DECLARE_WAIT_QUEUE_HEAD(read_wait);
static DECLARE_WAIT_QUEUE_HEAD(write_wait);


#ifdef CONFIG_HAS_EARLYSUSPEND
	struct early_suspend early_suspend;
#endif

#define K21_SPI_DEBUG 1

#ifdef K21_SPI_DEBUG
#define  k21_spi_dbg(format,args...)      printk(format,##args)
#else
#define  k21_spi_dbg(format,args...)	////
#endif

/*
 * This supports access to SPI devices using normal userspace I/O calls.
 * Note that while traditional UNIX/POSIX I/O semantics are half duplex,
 * and often mask message boundaries, full SPI support requires full duplex
 * transfers.  There are several kinds of internal message boundaries to
 * handle chipselect management and other protocol options.
 *
 * SPI has a character major number assigned.  We allocate minor numbers
 * dynamically using a bitmask.  You must use hotplug tools, such as udev
 * (or mdev with busybox) to create and destroy the /dev/spidevB.C device
 * nodes, since there is no fixed association of minor numbers with any
 * particular SPI bus or device.
 */
#define SPIDEV_MAJOR			153	/* assigned */
#define N_SPI_MINORS			32	/* ... up to 256 */

static DECLARE_BITMAP(minors, N_SPI_MINORS);


/* Bit masks for spi_device.mode management.  Note that incorrect
 * settings for some settings can cause *lots* of trouble for other
 * devices on a shared bus:
 *
 *  - CS_HIGH ... this device will be active when it shouldn't be
 *  - 3WIRE ... when active, it won't behave as it should
 *  - NO_CS ... there will be no explicit message boundaries; this
 *	is completely incompatible with the shared bus model
 *  - READY ... transfers may proceed when they shouldn't.
 *
 * REVISIT should changing those flags be privileged?
 */
#define SPI_MODE_MASK		(SPI_CPHA | SPI_CPOL | SPI_CS_HIGH \
				| SPI_LSB_FIRST | SPI_3WIRE | SPI_LOOP \
				| SPI_NO_CS | SPI_READY)

struct spidev_data {
	dev_t			devt;
	spinlock_t		spi_lock;
	struct spi_device	*spi;
	struct list_head	device_entry;

	/* buffer is NULL unless this device is open (users > 0) */
	struct mutex		buf_lock;
	unsigned		users;
	u8			*buffer;
	//wait_queue_head_t poll_queue;

};

static LIST_HEAD(device_list);
static DEFINE_MUTEX(device_list_lock);

static unsigned bufsiz = (4096+1024);
module_param(bufsiz, uint, S_IRUGO);
MODULE_PARM_DESC(bufsiz, "data bytes in biggest supported SPI message");
static void spidev_wakeup_k21(void);

/*-------------------------------------------------------------------------*/

/*
 * We can't use the standard synchronous wrappers for file I/O; we
 * need to protect against async removal of the underlying spi_device.
 */
static void spidev_complete(void *arg)
{
	complete(arg);
}

static ssize_t
spidev_sync(struct spidev_data *spidev, struct spi_message *message)
{
	DECLARE_COMPLETION_ONSTACK(done);
	int status;

	message->complete = spidev_complete;
	message->context = &done;
	//edit xum 20161201
	spidev_wakeup_k21();
	
	//printk("max_speed_hz:  %u  \n", spidev->spi->max_speed_hz);

	//gpio_set_value(ap_wakeup_sp_gpio, 0);//µÍµçÆ½
	spin_lock_irq(&spidev->spi_lock);
	if (spidev->spi == NULL)
		status = -ESHUTDOWN;
	else {
		status = spi_async(spidev->spi, message);
	}
	spin_unlock_irq(&spidev->spi_lock);
	//gpio_set_value(ap_wakeup_sp_gpio, 1);//µÍµçÆ½
	if (status == 0) {
		wait_for_completion(&done);
		status = message->status;
		if (status == 0)
			status = message->actual_length;
	}
	return status;
}

static inline ssize_t
spidev_sync_write(struct spidev_data *spidev, size_t len)
{
	struct spi_transfer	t = {
			.tx_buf		= spidev->buffer,
			.len		= len,
		};
	struct spi_message	m;

	spi_message_init(&m);
	spi_message_add_tail(&t, &m);
	return spidev_sync(spidev, &m);
}


static inline ssize_t
spidev_sync_write_offset(struct spidev_data *spidev, size_t len, int offset)
{
	struct spi_transfer	t = {
			.tx_buf		= spidev->buffer + offset,
			.len		= len,
		};
	struct spi_message	m;


	spi_message_init(&m);
	spi_message_add_tail(&t, &m);
	return spidev_sync(spidev, &m);
}

static inline ssize_t
spidev_sync_read(struct spidev_data *spidev, size_t len)
{
	struct spi_transfer	t = {
			.rx_buf		= spidev->buffer,
			.len		= len,
			.speed_hz   = 5000000,
			.bits_per_word = 8,
		};
	struct spi_message	m;
	
	spi_message_init(&m);
	spi_message_add_tail(&t, &m);
	return spidev_sync(spidev, &m);
}
typedef struct read_data
{
    u8* Rddata;
	u16 Rdlen;
    struct read_data *next;  
}NODE;

NODE * Lhead=NULL;
/*-------------------------------------------------------------------------*/

static void spidev_wakeup_k21(void)
{
	int k21_status = 0;
	int i = 0;
	if (likely(spi_wake_k21_pio_vaild && k21_wakeup_status_pio_vaild))
	{	
		k21_status = __gpio_get_value(check_sp_status_gpio);
		//k21_spi_dbg(KERN_ERR "++++  k21_status %s ++++++\n",k21_status?"hi":"low");

		if( !k21_status) 
		{
			gpio_set_value(ap_wakeup_sp_gpio, 0);//低电平
			udelay(100);
			gpio_set_value(ap_wakeup_sp_gpio, 1);//高电平
			//mdelay(15);
			//k21_spi_dbg(KERN_ERR "LINE:%d\n", __LINE__);
			i = 0;
			while (i++ <= 60)
			{
				if (__gpio_get_value(check_sp_status_gpio)) break;
				mdelay(1);
				k21_spi_dbg(KERN_ERR "LINE:%d i:%d \n", __LINE__, i);
			}
			
			if (i>49) 
			{
				gpio_set_value(ap_wakeup_sp_gpio, 0);//低电平
				udelay(100);
				gpio_set_value(ap_wakeup_sp_gpio, 1);//高电平
				mdelay(50);		
			}
			//k21_spi_dbg(KERN_ERR "LINE:%d\n", __LINE__);
		}
	}	
	
}


/* Read-block caoliang add  */
//static wait_queue_head_t rdwait;
//static wait_queue_head_t wrwait;
static wait_queue_head_t log_wait_queue;

static struct semaphore mutex;
static int k21_spi_readflag = 0;

static unsigned int spidev_poll(struct file *filp, poll_table *wait)
{	
	//struct spidev_data *dev = filp->private_data;
	unsigned int mask = 0;

	poll_wait(filp, &read_wait, wait);	/* 把进程添加到等待队列*/
	poll_wait(filp, &write_wait, wait);
	
	if(have_data)	// 有数据可读
		mask |= POLLIN |POLLRDNORM;
	
	if(have_test){
		mask |= POLLOUT | POLLWRNORM;
		have_test = false;

		}
	return mask;
}

static ssize_t
spidev_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
	struct spidev_data	*spidev;
	ssize_t			status = 0;

	/* chipselect only toggles at start or end of operation */
	if (count > bufsiz)
		return -EMSGSIZE;

//	k21_spi_dbg("%s count=%lu\n",__func__,count);	

	spidev = filp->private_data;

	mutex_lock(&spidev->buf_lock);
	status = spidev_sync_read(spidev, count);
	if (status > 0) {
		unsigned long	missing;

		missing = copy_to_user(buf, spidev->buffer, status);
		if (missing == status)
			status = -EFAULT;
		else
			status = status - missing;
	}
	mutex_unlock(&spidev->buf_lock);
	
	return status;
}
/* Write-only message with current device setup */
static ssize_t
spidev_write(struct file *filp, const char __user *buf,
		size_t count, loff_t *f_pos)
{
	struct spidev_data	*spidev;
	ssize_t			status = 0;
	unsigned long		missing;

	/* chipselect only toggles at start or end of operation */
	if (count > bufsiz)
		return -EMSGSIZE;

	spidev = filp->private_data;

	mutex_lock(&spidev->buf_lock);
	missing = copy_from_user(spidev->buffer, buf, count);
	if (missing == 0) {
		status = spidev_sync_write(spidev, count);
	} else
		status = -EFAULT;
	mutex_unlock(&spidev->buf_lock);

	return status;
}

static int spidev_message(struct spidev_data *spidev,
		struct spi_ioc_transfer *u_xfers, unsigned n_xfers)
{
	struct spi_message	msg;
	struct spi_transfer	*k_xfers;
	struct spi_transfer	*k_tmp;
	struct spi_ioc_transfer *u_tmp;
	unsigned		n, total;
	u8			*buf;
	int			status = -EFAULT;

	spi_message_init(&msg);
	k_xfers = kcalloc(n_xfers, sizeof(*k_tmp), GFP_KERNEL);
	if (k_xfers == NULL)
		return -ENOMEM;

	/* Construct spi_message, copying any tx data to bounce buffer.
	 * We walk the array of user-provided transfers, using each one
	 * to initialize a kernel version of the same transfer.
	 */
	buf = spidev->buffer;
	total = 0;
	for (n = n_xfers, k_tmp = k_xfers, u_tmp = u_xfers;
			n;
			n--, k_tmp++, u_tmp++) {
		k_tmp->len = u_tmp->len;

		total += k_tmp->len;
		if (total > bufsiz) {
			status = -EMSGSIZE;
			goto done;
		}

		if (u_tmp->rx_buf) {
			k_tmp->rx_buf = buf;
			if (!access_ok(VERIFY_WRITE, (u8 __user *)
						(uintptr_t) u_tmp->rx_buf,
						u_tmp->len))
				goto done;
		}
		if (u_tmp->tx_buf) {
			k_tmp->tx_buf = buf;
			if (copy_from_user(buf, (const u8 __user *)
						(uintptr_t) u_tmp->tx_buf,
					u_tmp->len))
				goto done;
		}
		buf += k_tmp->len;

		k_tmp->cs_change = !!u_tmp->cs_change;
		k_tmp->bits_per_word = u_tmp->bits_per_word;
		k_tmp->delay_usecs = u_tmp->delay_usecs;
		k_tmp->speed_hz = u_tmp->speed_hz;
#ifdef VERBOSE
		dev_dbg(&spidev->spi->dev,
			"  xfer len %zd %s%s%s%dbits %u usec %uHz\n",
			u_tmp->len,
			u_tmp->rx_buf ? "rx " : "",
			u_tmp->tx_buf ? "tx " : "",
			u_tmp->cs_change ? "cs " : "",
			u_tmp->bits_per_word ? : spidev->spi->bits_per_word,
			u_tmp->delay_usecs,
			u_tmp->speed_hz ? : spidev->spi->max_speed_hz);
#endif
		spi_message_add_tail(k_tmp, &msg);
	}

	status = spidev_sync(spidev, &msg);
	if (status < 0)
		goto done;

	/* copy any rx data out of bounce buffer */
	buf = spidev->buffer;
	for (n = n_xfers, u_tmp = u_xfers; n; n--, u_tmp++) {
		if (u_tmp->rx_buf) {
			if (__copy_to_user((u8 __user *)
					(uintptr_t) u_tmp->rx_buf, buf,
					u_tmp->len)) {
				status = -EFAULT;
				goto done;
			}
		}
		buf += u_tmp->len;
	}
	status = total;

done:
	kfree(k_xfers);
	return status;
}

static long
spidev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int			err = 0;
	int			retval = 0;
	struct spidev_data	*spidev;
	struct spi_device	*spi;
	u32			tmp;
	unsigned		n_ioc;
	struct spi_ioc_transfer	*ioc;

	
	/* Check type and command number */
	if (_IOC_TYPE(cmd) != SPI_IOC_MAGIC)
		return -ENOTTY;

	/* Check access direction once here; don't repeat below.
	 * IOC_DIR is from the user perspective, while access_ok is
	 * from the kernel perspective; so they look reversed.
	 */
	if (_IOC_DIR(cmd) & _IOC_READ)
		err = !access_ok(VERIFY_WRITE,
				(void __user *)arg, _IOC_SIZE(cmd));
	if (err == 0 && _IOC_DIR(cmd) & _IOC_WRITE)
		err = !access_ok(VERIFY_READ,
				(void __user *)arg, _IOC_SIZE(cmd));
	if (err)
		return -EFAULT;

	/* guard against device removal before, or while,
	 * we issue this ioctl.
	 */
	spidev = filp->private_data;
	spin_lock_irq(&spidev->spi_lock);
	spi = spi_dev_get(spidev->spi);
	spin_unlock_irq(&spidev->spi_lock);

	if (spi == NULL)
		return -ESHUTDOWN;

	/* use the buffer lock here for triple duty:
	 *  - prevent I/O (from us) so calling spi_setup() is safe;
	 *  - prevent concurrent SPI_IOC_WR_* from morphing
	 *    data fields while SPI_IOC_RD_* reads them;
	 *  - SPI_IOC_MESSAGE needs the buffer locked "normally".
	 */
	mutex_lock(&spidev->buf_lock);
//	k21_spi_dbg("%s cmd=%d\n",__func__,cmd);
	switch (cmd) {
	/* read requests */
	case SPI_IOC_RD_MODE:
		retval = __put_user(spi->mode & SPI_MODE_MASK,
					(__u8 __user *)arg);
		break;
	case SPI_IOC_RD_LSB_FIRST:
		retval = __put_user((spi->mode & SPI_LSB_FIRST) ?  1 : 0,
					(__u8 __user *)arg);
		break;
	case SPI_IOC_RD_BITS_PER_WORD:
		retval = __put_user(spi->bits_per_word, (__u8 __user *)arg);
		break;
	case SPI_IOC_RD_MAX_SPEED_HZ:
		retval = __put_user(spi->max_speed_hz, (__u32 __user *)arg);
		break;
	/* write requests */
	case SPI_IOC_WR_MODE:
		retval = __get_user(tmp, (u8 __user *)arg);
		if (retval == 0) {
			u8	save = spi->mode;

			if (tmp & ~SPI_MODE_MASK) {
				retval = -EINVAL;
				break;
			}

			tmp |= spi->mode & ~SPI_MODE_MASK;
			spi->mode = (u8)tmp;
			retval = spi_setup(spi);
			if (retval < 0)
				spi->mode = save;
			else
				dev_dbg(&spi->dev, "spi mode %02x\n", tmp);
		}
			spidev_wakeup_k21();
		break;
	case SPI_IOC_WR_LSB_FIRST:
		retval = __get_user(tmp, (__u8 __user *)arg);
		if (retval == 0) {
			u8	save = spi->mode;

			if (tmp)
				spi->mode |= SPI_LSB_FIRST;
			else
				spi->mode &= ~SPI_LSB_FIRST;
			retval = spi_setup(spi);
			if (retval < 0)
				spi->mode = save;
			else
				dev_dbg(&spi->dev, "%csb first\n",
						tmp ? 'l' : 'm');
		}
			spidev_wakeup_k21();
		break;
	case SPI_IOC_WR_BITS_PER_WORD:
		retval = __get_user(tmp, (__u8 __user *)arg);
		if (retval == 0) {
			u8	save = spi->bits_per_word;
		
			spi->bits_per_word = tmp;
			retval = spi_setup(spi);
				k21_spi_dbg("save =%d tmp=%d retval=%d\n",save,tmp,retval);
			if (retval < 0)
				spi->bits_per_word = save;
			else
				dev_dbg(&spi->dev, "%d bits per word\n", tmp);
		}
		
		break;
	case SPI_IOC_WR_MAX_SPEED_HZ:
		retval = __get_user(tmp, (__u32 __user *)arg);
		if (retval == 0) {
			u32	save = spi->max_speed_hz;

			spi->max_speed_hz = tmp;
			retval = spi_setup(spi);
			if (retval < 0)
				spi->max_speed_hz = save;
			else
				dev_dbg(&spi->dev, "%d Hz (max)\n", tmp);
		}
		
		break;

	default:
		/* segmented and/or full-duplex I/O request */
		if (_IOC_NR(cmd) != _IOC_NR(SPI_IOC_MESSAGE(0))
				|| _IOC_DIR(cmd) != _IOC_WRITE) {
			retval = -ENOTTY;
			break;
		}

		tmp = _IOC_SIZE(cmd);
		if ((tmp % sizeof(struct spi_ioc_transfer)) != 0) {
			retval = -EINVAL;
			break;
		}
		n_ioc = tmp / sizeof(struct spi_ioc_transfer);
		if (n_ioc == 0)
			break;

		/* copy into scratch area */
		ioc = kmalloc(tmp, GFP_KERNEL);
		if (!ioc) {
			retval = -ENOMEM;
			break;
		}
		if (__copy_from_user(ioc, (void __user *)arg, tmp)) {
			kfree(ioc);
			retval = -EFAULT;
			break;
		}

		/* translate to spi_message, execute */
		retval = spidev_message(spidev, ioc, n_ioc);
		kfree(ioc);
		break;
	}

	mutex_unlock(&spidev->buf_lock);
	spi_dev_put(spi);

	return retval;
}

#ifdef CONFIG_COMPAT
static long
spidev_compat_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	return spidev_ioctl(filp, cmd, (unsigned long)compat_ptr(arg));
}
#else
#define spidev_compat_ioctl NULL
#endif /* CONFIG_COMPAT */

static int spidev_open(struct inode *inode, struct file *filp)
{
	struct spidev_data	*spidev;
	int			status = -ENXIO;
	
	mutex_lock(&device_list_lock);
	//enable_irq(irq);
	list_for_each_entry(spidev, &device_list, device_entry) {
		if (spidev->devt == inode->i_rdev) {
			status = 0;
			break;
		}
	}
	if (status == 0) {
		if (!spidev->buffer) {
			spidev->buffer = kmalloc(bufsiz, GFP_KERNEL);
			if (!spidev->buffer) {
				dev_dbg(&spidev->spi->dev, "open/ENOMEM\n");
				status = -ENOMEM;
			}
		}
		
		if (status == 0) {
			spidev->users++;
			filp->private_data = spidev;
			nonseekable_open(inode, filp);
		}
		k21_spi_dbg("==== %s ,spidev->users = %d======\n",__func__,spidev->users);

	} else
		pr_debug("spidev: nothing for minor %d\n", iminor(inode));

	mutex_unlock(&device_list_lock);
	
	return status;
}

static int spidev_release(struct inode *inode, struct file *filp)
{
	struct spidev_data	*spidev;
	int			status = 0;
	

	mutex_lock(&device_list_lock);
	spidev = filp->private_data;
	filp->private_data = NULL;
	k21_spi_dbg("==== %s ,spidev->users = %d======\n",__func__,spidev->users);
	//suspend_status = 1;
	/* last close? */
	spidev->users--;
	if (!spidev->users) {
		int		dofree;

		kfree(spidev->buffer);
		spidev->buffer = NULL;

		/* ... after we unbound from the underlying device? */
		spin_lock_irq(&spidev->spi_lock);
		dofree = (spidev->spi == NULL);
		spin_unlock_irq(&spidev->spi_lock);

		if (dofree)
			kfree(spidev);
		disable_irq(irq);
		disable_irq_wake(irq);
	}
	mutex_unlock(&device_list_lock);

	return status;
}

static const struct file_operations spidev_fops = {
	.owner =	THIS_MODULE,
	/* REVISIT switch to aio primitives, so that userspace
	 * gets more complete API coverage.  It'll simplify things
	 * too, except for the locking.
	 */
	.write =	spidev_write,
	.read =		spidev_read,
	.unlocked_ioctl = spidev_ioctl,
	.compat_ioctl = spidev_compat_ioctl,
	.open =		spidev_open,
	.release =	spidev_release,
	.poll = spidev_poll,
	.llseek =	no_llseek,
};

static int thread_get_spi_data(void *spidevdata)
{
 

	 
	return 0;
}

static irqreturn_t k21_interrupt(int irq, void* dev_id)
{
	//printk(KERN_EMERG"k21 irq\n");
			
	if(gpio_get_value(spi_int_gpio) == 0)
	{
		#if 1
		if(1 == k21_spi_readflag)
		{
			//wake_up_interruptible(&log_wait_queue); 
			return IRQ_HANDLED; 
		}
		#endif
		k21_spi_readflag = 1;
		wake_up_interruptible(&log_wait_queue); 
	}
	else {
		k21_spi_readflag = 0;
	}
		
	return IRQ_HANDLED;
}
static int spidev_parse_dt(struct device *dev) {	
	struct device_node *np;	
	
	if (!dev) {		
		return -ENODEV;	
	}	
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	np = dev->of_node;	
	spi_int_gpio = of_get_named_gpio(np, "jolly,irq-gpio", 0);	
		k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	ap_wakeup_sp_gpio = of_get_named_gpio(np, "jolly,ap-wakeup-sp-gpio", 0);	
		k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	check_sp_status_gpio = of_get_named_gpio(np, "jolly,check-sp-status-gpio", 0);
		k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	//en_sp_gpio = of_get_named_gpio(np, "jolly,en-sp-gpio", 0);
	ctrl_scaner_gpio = of_get_named_gpio(np, "jolly,ctrl-scaner-power-gpio", 0);
		k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	ctrl_id_gpio = of_get_named_gpio(np, "jolly,ctrl-id-power-gpio", 0);
		k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	//ctrl_uart_0_gpio = of_get_named_gpio(np, "jolly,ctrl-uart-zero-gpio", 0);
	//ctrl_uart_1_gpio = of_get_named_gpio(np, "jolly,ctrl-uart-one-gpio", 0);
	
	if (!gpio_is_valid(spi_int_gpio) || !gpio_is_valid(ap_wakeup_sp_gpio)
		|| !gpio_is_valid(check_sp_status_gpio)) { 
		k21_spi_dbg(KERN_ERR"Invalid GPIO, irq-gpio:%d, wakeup-gpio:%d, status-gpio:%d \n", 
			spi_int_gpio, ap_wakeup_sp_gpio, check_sp_status_gpio);
		return -EINVAL;
	}
	k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
/*
	if (!gpio_is_valid(ctrl_scaner_gpio) || !gpio_is_valid(ctrl_uart_0_gpio)
		|| !gpio_is_valid(ctrl_uart_1_gpio)) { 
		k21_spi_dbg(KERN_ERR"Invalid GPIO, ctrl_scaner_gpio:%d, ctrl_uart_0_gpio:%d, ctrl_uart_1_gpio:%d \n", 
			ctrl_scaner_gpio, ctrl_uart_0_gpio, ctrl_uart_1_gpio);
		return -EINVAL;
	}
*/
	return 0;
	
}


static int  spidev_request_io_port(struct spi_device *spi) {
	int ret = 0;

	if (!spi) {
		return -ENODEV;
	}
	k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	ret = gpio_request(spi_int_gpio, "SPI_INT_IRQ");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request spi_int_gpio:%d, ERROR:%d", spi_int_gpio, ret);
		return -ENODEV;
	} else {
		gpio_set_value(spi_int_gpio, 1);
		gpio_direction_input(spi_int_gpio);
		spi->irq = gpio_to_irq(spi_int_gpio);
		irq = spi->irq;
	}
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	ret = gpio_request(ap_wakeup_sp_gpio, "AP_WAKEUP_SP");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request ap_wakeup_sp_gpio:%d, ERROR:%d", ap_wakeup_sp_gpio, ret);
		return -ENODEV;
	} else {
		gpio_direction_output(ap_wakeup_sp_gpio, 0);
		spi_wake_k21_pio_vaild = 1;
	}
/*
	ret = gpio_request(en_sp_gpio, "AP_EN_SP");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request ap_wakeup_sp_gpio:%d, ERROR:%d", en_sp_gpio, ret);
		return -ENODEV;
	} else {
		gpio_direction_output(en_sp_gpio, 1);
	}
*/
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	ret = gpio_request(check_sp_status_gpio, "CHECK_SP_STATUS");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request check_sp_status_gpio:%d, ERROR:%d", check_sp_status_gpio, ret);
		return -ENODEV;
	} else {
		gpio_direction_input(check_sp_status_gpio);
		k21_wakeup_status_pio_vaild = 1;
	}
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	ret = gpio_request(ctrl_scaner_gpio, "CTRL_SCANER");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request ctrl_scaner_gpio:%d, ERROR:%d", ctrl_scaner_gpio, ret);
		return -ENODEV;
	} else {
		gpio_direction_output(ctrl_scaner_gpio, 0);
	}

k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	ret = gpio_request(ctrl_id_gpio, "CTRL_ID");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request ctrl_id_gpio:%d, ERROR:%d", ctrl_id_gpio, ret);
		return -ENODEV;
	} else {
		gpio_direction_output(ctrl_id_gpio, 0);
	}
/*
	ret = gpio_request(ctrl_uart_0_gpio, "CTRL_UART_0");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request ctrl_uart_0_gpio:%d, ERROR:%d", ctrl_uart_0_gpio, ret);
		return -ENODEV;
	} else {
		gpio_direction_output(ctrl_uart_0_gpio, 0);
	}

	ret = gpio_request(ctrl_uart_1_gpio, "CTRL_UART_1");
	if (ret < 0) {
		k21_spi_dbg(KERN_ERR "Failed to request ctrl_uart_1_gpio:%d, ERROR:%d", ctrl_uart_1_gpio, ret);
		return -ENODEV;
	} else {
		gpio_direction_output(ctrl_uart_1_gpio, 0);
	}
*/
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	return ret;
}


static int spidev_request_irq (struct spi_device *spi) {
	int ret = 0;
	
	if (!spi) {
		return -ENODEV;
	}

	k21_spi_dbg(KERN_EMERG "spidev_request_irq, spi->irq%d", spi->irq);
	ret = request_irq(spi->irq, k21_interrupt, IRQF_TRIGGER_FALLING, DRV_NAME, spi);
	if (ret) {
		k21_spi_dbg(KERN_EMERG "Failed to request irq, ERROR:%d", ret);
		gpio_free(spi_int_gpio);
	}

	return ret;
}


/*-------------------------------------------------------------------------*/

/* The main reason to have this class is to make mdev/udev create the
 * /dev/spidevB.C character device nodes exposing our userspace API.
 * It also simplifies memory management.
 */

static struct class *spidev_class;

/*-------------------------------------------------------------------------*/



static ssize_t spidev_scaner_show(struct device *dev,
                struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(ctrl_scaner_gpio));
}


static ssize_t spidev_scaner_store(struct device *dev,
                struct device_attribute *attr,
		        const char *buf, size_t count)
{
    int value = 0;

    sscanf(buf, "%d", &value);
    gpio_set_value(ctrl_scaner_gpio, value);
	
	return count;
}

static DEVICE_ATTR(scaner_power, 0666, spidev_scaner_show, spidev_scaner_store);




static ssize_t spidev_id_show(struct device *dev,
                struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(ctrl_id_gpio));
}


static ssize_t spidev_id_store(struct device *dev,
                struct device_attribute *attr,
		        const char *buf, size_t count)
{
    int value = 0;

    sscanf(buf, "%d", &value);
    gpio_set_value(ctrl_id_gpio, value);
	
	return count;
}

static DEVICE_ATTR(id_power, 0666, spidev_id_show, spidev_id_store);




static int spidev_probe(struct spi_device *spi)
{
	struct spidev_data	*spidev;
	int			status ;
	unsigned long		minor;
	int ret;

	k21_spi_dbg(KERN_ERR "[%s, %d] has enter  \n", __func__, __LINE__);

	/* Allocate driver data */
	spidev = kzalloc(sizeof(*spidev), GFP_KERNEL);
	if (!spidev)
		return -ENOMEM;
	 k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
#if K21_SPI
	/* Initialize the driver irq */
#ifdef  CONFIG_OF	
	if (spi->dev.of_node) {		
		if (spidev_parse_dt(&spi->dev) != 0) {
			k21_spi_dbg(KERN_ERR "spidev_parse_dt Failed!\n");  
			return -1;
		}	
	}
#endif

	spidev_request_io_port(spi);
	if (spidev_request_irq(spi)) {
		k21_spi_dbg(KERN_ERR "spidev_request_irq Failed!\n");  
		return -1;
	}

	
	sema_init(&mutex,1);
//	init_waitqueue_head(&rdwait);//阻塞读
//	init_waitqueue_head(&wrwait);//阻塞写
	/* Initialize the driver read data thread 内核线程*/	
	init_waitqueue_head(&log_wait_queue);
	//init_waitqueue_head(&spidev->poll_queue);

//	kthread_create(k21_get_spi_read_data, NULL, CLONE_KERNEL|SIGCHLD);
	g_spidev = spidev;
	_tsk = kthread_run(thread_get_spi_data, (void*)spidev, "mythread");
 	if (IS_ERR(_tsk)) {  //需要使用IS_ERR()来判断线程是否有效，后面会有文章介绍IS_ERR()

        k21_spi_dbg(KERN_ERR"first create kthread failed!\n");  
    }  
	disable_irq(irq);


	
	if(spi_wake_k21_pio_vaild) 
		gpio_set_value(ap_wakeup_sp_gpio, 1);//初始高电平
		
	//k21_spi_firstreadflag = 2;//特殊标记 第一次读spi需要越过两个字节
#endif	
		
	/* Initialize the driver data */
	spidev->users = 0;
	spidev->spi = spi;
	spin_lock_init(&spidev->spi_lock);
	mutex_init(&spidev->buf_lock);

	INIT_LIST_HEAD(&spidev->device_entry);

	/* If we can allocate a minor number, hook up this device.
	 * Reusing minors is fine so long as udev or mdev is working.
	 */
	mutex_lock(&device_list_lock);
	minor = find_first_zero_bit(minors, N_SPI_MINORS);
	if (minor < N_SPI_MINORS) {
		struct device *dev;
		spidev->devt = MKDEV(SPIDEV_MAJOR, minor);
		dev = device_create(spidev_class, &spi->dev, spidev->devt,
				    spidev, DRV_NAME"-%d.%d",
				    spi->master->bus_num, spi->chip_select);
		status = IS_ERR(dev) ? PTR_ERR(dev) : 0;

		if (dev) {
			ret = device_create_file(dev, &dev_attr_scaner_power);
			ret = device_create_file(dev, &dev_attr_id_power);
		}
	} else {
		dev_dbg(&spi->dev, "no minor number available!\n");
		status = -ENODEV;
	}
	if (status == 0) {
		set_bit(minor, minors);
		list_add(&spidev->device_entry, &device_list);
	}
	mutex_unlock(&device_list_lock);
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	if (status == 0)
		spi_set_drvdata(spi, spidev);
	else
		kfree(spidev);
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	spikeydev = input_allocate_device();
	if (!spikeydev) {
		kfree(spikeydev);
		return -ENODEV;
	}
	spikeydev->name ="spidev-k21";
	spikeydev->phys = "spikbd/input0";
	spikeydev->id.bustype = BUS_HOST;
	spikeydev->id.vendor = 0x0001;
	spikeydev->id.product = 0x0001;
	spikeydev->id.version = 0x0100;
	spikeydev->open = NULL;
	spikeydev->close = NULL;
	//spikeydev->dev.parent = &pdev->dev;
	set_bit(EV_KEY, spikeydev->evbit);
	set_bit(EV_REL, spikeydev->evbit);
	set_bit(KEY_F1, spikeydev->keybit);
	k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);

	ret = input_register_device(spikeydev);
	if(ret)
		printk(KERN_EMERG"Unable to Register the spi key\n");
	k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	if (!spidev->buffer) {
		spidev->buffer = kmalloc(bufsiz, GFP_KERNEL);
		if (!spidev->buffer) {
			dev_dbg(&spidev->spi->dev, "open/ENOMEM\n");
		}
	}
	spidev->users++;
	bootFlag = false;
	enable_irq(irq);
	enable_irq_wake(irq);
	printk(KERN_EMERG"********** spidev-k21 irq = %d ************\n",irq);
	return status;
}

static int  spidev_remove(struct spi_device *spi)
{
	struct spidev_data	*spidev = spi_get_drvdata(spi);


	k21_spi_dbg("%s\n",__func__);
#if 0

	disable_irq(irq);
	free_irq(irq, spidev->spi );
	if (!IS_ERR(_tsk)){  

        int ret = kthread_stop(_tsk);  

        k21_spi_dbg(KERN_INFO "First thread function has stopped ,return %d\n", ret);  

    } 
	kfree(Lhead);
#endif
	/* make sure ops on existing fds can abort cleanly */
	spin_lock_irq(&spidev->spi_lock);
	spidev->spi = NULL;
	spi_set_drvdata(spi, NULL);
	spin_unlock_irq(&spidev->spi_lock);

	/* prevent new opens */
	mutex_lock(&device_list_lock);
	list_del(&spidev->device_entry);
	device_destroy(spidev_class, spidev->devt);
	clear_bit(MINOR(spidev->devt), minors);
	if (spidev->users == 0)
		kfree(spidev);
	mutex_unlock(&device_list_lock);

	input_unregister_device(spikeydev);
	kfree(spikeydev);
	return 0;
}
#ifdef CONFIG_HAS_EARLYSUSPEND

//停滞状态
static int spidev_suspend(struct early_suspend *h)
{
	#if 0
    k21_spi_dbg("==== spidev_suspend ===== \n");
   spidev_wakeup_k21(0);
	#endif
    return 0;
}
//唤醒
static int spidev_resume(struct early_suspend *h)
{
    //printk(KERN_EMERG"===== early spidev_resume ===== \n");
   spidev_wakeup_k21();

    return 0;
}
#endif
 
static int pm_spidev_suspend(struct device *dev)
{
	suspend_status = 1;
//	gpio_set_value(en_sp_gpio, 0); 
	enable_irq_wake(irq);
	return 0;
}

static int pm_spidev_resume(struct device *dev)
{
	disable_irq_wake(irq);
	if(gpio_get_value(spi_int_gpio) == 0)
	{
		printk(KERN_ERR"spi interrupt!\n");
		k21_spi_readflag = 1;
		wake_up_interruptible(&log_wait_queue); 
	}
//	gpio_set_value(en_sp_gpio, 1); 
	suspend_status = 0;
	return 0;
}

static const struct dev_pm_ops spidev_pm_ops = {
	.suspend = pm_spidev_suspend,
	.resume  = pm_spidev_resume,
};
#define SPI_DEV_PM_OPS (&spidev_pm_ops)



static struct of_device_id k21spi_match_table[] = {
        {.compatible = "jolly,ap-k21-spi",},
        { },
};

MODULE_DEVICE_TABLE(of, k21spi_match_table);

static struct spi_driver spidev_spi_driver = {
	.driver = {
		.name =		DRV_NAME,
		.owner =	THIS_MODULE,
		.pm 	= SPI_DEV_PM_OPS,
		.of_match_table = of_match_ptr(k21spi_match_table),
	},
	.probe =	spidev_probe,
	.remove =	spidev_remove,//__devexit_p(spidev_remove),

	/* NOTE:  suspend/resume methods are not necessary here.
	 * We don't do anything except pass the requests to/from
	 * the underlying controller.  The refrigerator handles
	 * most issues; the controller driver handles the rest.
	 */
};



/*-------------------------------------------------------------------------*/

static int __init spidev_init(void)
{
	int status;

	/* Claim our 256 reserved device numbers.  Then register a class
	 * that will key udev/mdev to add/remove /dev nodes.  Last, register
	 * the driver which manages those device numbers.
	 */
	 k21_spi_dbg(KERN_ERR "%s\n",__func__);
	BUILD_BUG_ON(N_SPI_MINORS > 256);
	k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	status = register_chrdev(SPIDEV_MAJOR, DRV_NAME, &spidev_fops);
	if (status < 0)
		return status;
k21_spi_dbg(KERN_ERR "%s *1\n",__func__);
	spidev_class = class_create(THIS_MODULE, DRV_NAME);
	if (IS_ERR(spidev_class)) {
		unregister_chrdev(SPIDEV_MAJOR, spidev_spi_driver.driver.name);
		return PTR_ERR(spidev_class);
	}
k21_spi_dbg(KERN_ERR "%s *2\n",__func__);
	//spidev_class->dev_attrs = &dev_attr_scaner;
	
	status = spi_register_driver(&spidev_spi_driver);
	if (status < 0) {
		class_destroy(spidev_class);
		unregister_chrdev(SPIDEV_MAJOR, spidev_spi_driver.driver.name);
	}
 #ifdef CONFIG_HAS_EARLYSUSPEND
		early_suspend.suspend = spidev_suspend;
		early_suspend.resume = spidev_resume;
		register_early_suspend(&early_suspend);
#endif
k21_spi_dbg(KERN_ERR "%s %d\n",__func__,__LINE__);
	return status;
}
module_init(spidev_init);

static void __exit spidev_exit(void)
{
	k21_spi_dbg("%s\n",__func__);
	spi_unregister_driver(&spidev_spi_driver);
	class_destroy(spidev_class);
	unregister_chrdev(SPIDEV_MAJOR, spidev_spi_driver.driver.name);
 #ifdef CONFIG_HAS_EARLYSUSPEND
	  unregister_early_suspend(&early_suspend);
#endif 
}
module_exit(spidev_exit);

MODULE_AUTHOR("chuck");
MODULE_DESCRIPTION("User mode SPI device interface");
MODULE_LICENSE("GPL");
MODULE_ALIAS("spi:spidev");

