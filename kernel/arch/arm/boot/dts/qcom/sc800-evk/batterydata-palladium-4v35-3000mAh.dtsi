/* Copyright (c) 2014, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

qcom,palladium-battery-4v35-3000mAh-data {
	qcom,battery-type = "palladium-battery-4v35-3000mAh";
	qcom,batt-id-kohm = <100>;
	qcom,chg-term-ua = <100000>;
	qcom,default-rbatt-mohm = <150>;
	qcom,fcc-mah = <3000>;
	qcom,max-voltage-uv = <4350000>;
	qcom,rbatt-capacitive-mohm = <50>;
	qcom,v-cutoff-uv = <3400000>;
	qcom,flat-ocv-threshold-uv = <3700000>;

	qcom,fcc-temp-lut {
		qcom,lut-col-legend = <(-10) 0 25 40 60>;
		qcom,lut-data = <3000 3000 3100 3066 3063>;
	};

	/*qcom,ibat-acc-lut {
		qcom,lut-col-legend = <(-20) 0 25>;
		qcom,lut-row-legend = <0 250 500 1440>;
		qcom,lut-data = <3000 3000 3100>,
				<1151 1962 2024>,
				<680 1864 2008>,
				<51 1548 1970>;
	};*/

	qcom,rbatt-sf-lut {
		qcom,lut-col-legend = <(-20) 0 25 40 60>;
		qcom,lut-row-legend = <100 95 90 85 80>,
					<75 70 65 60 55>,
					<50 45 40 35 30>,
					<25 20 16 13 11>,
					<10 9 8 7 6>,
					<5 4 3 2 1>,
					<0>;
		qcom,lut-data = <1355 273 100 81 74>,
				<1352 273 100 81 74>,
				<1268 280 102 83 75>,
				<1197 286 106 85 76>,
				<1143 285 110 87 78>,
				<1109 281 116 90 79>,
				<1090 272 124 94 82>,
				<1080 269 134 100 85>,
				<1077 267 133 108 91>,
				<1079 266 111 100 90>,
				<1090 266 101 83 76>,
				<1116 267 101 84 77>,
				<1156 268 103 86 79>,
				<1205 270 103 88 83>,
				<1266 274 106 88 82>,
				<1337 276 108 87 78>,
				<1431 276 107 85 79>,
				<1560 284 104 84 78>,
				<1680 293 99 81 75>,
				<2078 306 99 81 77>,
				<2438 318 100 83 78>,
				<2875 333 102 85 80>,
				<3411 354 104 87 81>,
				<4092 392 108 89 84>,
				<5118 448 116 92 86>,
				<6939 551 121 95 85>,
				<10433 791 120 90 81>,
				<17054 1280 121 93 84>,
				<29375 2077 133 101 92>,
				<52518 3457 173 135 133>,
				<230352 176376 150360 117059 92159>;
	};

	qcom,pc-temp-ocv-lut {
		qcom,lut-col-legend = <(-10) 0 25 50>;
		qcom,lut-row-legend = <100 95 90 85 80>,
					<75 70 65 60 55>,
					<50 45 40 35 30>,
					<25 20 16 13 11>,
					<10 9 8 7 6>,
					<5 4 3 2 1>,
					<0>;
		qcom,lut-data = <4250 4258 4292 4298>,
				<4104 4192 4252 4250>,
				<4024 4111 4173 4181>,
				<3966 4064 4124 4132>,
				<3911 4002 4075 4085>,	//80
				<3850 3960 4005 4013>,
				<3787 3907 3950 3969>,
				<3758 3843 3900 3920>,
				<3726 3806 3860 3872>,	//60
				<3685 3765 3812 3830>,
				<3668 3738 3789 3790>,
				<3622 3715 3767 3771>,
				<3603 3689 3749 3752>,	//40
				<3593 3675 3725 3729>,
				<3577 3664 3704 3711>,
				<3558 3645 3683 3686>,
				<3535 3629 3670 3670>,	//20
				<3521 3612 3665 3651>,
				<3504 3597 3647 3638>,
				<3493 3589 3632 3622>,
				<3488 3582 3627 3619>,	//10
				<3482 3578 3623 3617>,
				<3473 3573 3622 3613>,
				<3467 3568 3620 3612>,
				<3453 3563 3618 3611>,
				<3448 3542 3617 3608>,	//5
				<3441 3520 3605 3604>,
				<3430 3503 3587 3565>,
				<3421 3482 3557 3500>,
				<3410 3468 3467 3496>,
				<3400 3400 3400 3400>;
	};
};

