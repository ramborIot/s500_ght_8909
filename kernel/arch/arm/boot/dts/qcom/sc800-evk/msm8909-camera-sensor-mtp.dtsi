/*
 * Copyright (c) 2014-2015, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

&soc {
	flash_sgm3141b:flashlight {
		  compatible = "qcom,leds-gpio-flash";
		  cell-index = <0>;
		  status = "okay";
		  pinctrl-names = "flash_default";
		  pinctrl-0 = <&flash_default>;
		  qcom,flash-en = <&msm_gpio 31 0>;
		  qcom,flash-now = <&msm_gpio 23 0>;
		  qcom,flash-seq-val = <1 1>;
		  qcom,torch-seq-val = <1 0>;
		  qcom,op-seq = "flash_en", "flash_now";
		  linux,name = "flashlight";
		  linux,default-trigger = "flashlight-trigger";
		  qcom,current = <250>;
	};

	led_flash0: qcom,camera-led-flash {
		cell-index = <0>;
		compatible = "qcom,camera-led-flash";
		qcom,flash-type = <3>;
		qcom,flash-source = <&flash_sgm3141b>;
		qcom,torch-source = <&flash_sgm3141b>;
	};
};

&i2c_3 {

	actuator0: qcom,actuator@6e {
		   cell-index = <5>;
		   reg = <0x6c>;
		   compatible = "qcom,actuator";
		   qcom,cci-master = <0>;
	   };

        eeprom0: qcom,eeprom@6c {
		        cell-index = <0>;
		        reg = <0x20>;
		        qcom,eeprom-name = "sunny_q8v18a";
		        compatible = "qcom,eeprom";
		        qcom,slave-addr = <0x20>;
		        qcom,num-blocks = <4>;
		        qcom,page0 = <1 0x0100 2 0x01 1 1>;
		        qcom,poll0 = <0 0x0 2 0 1 1>;
		        qcom,mem0 = <0 0x0 2 0 1 0>;
		        qcom,page1 = <1 0x3d84 2 0xc0 1 1>;
		        qcom,poll1 = <0 0x0 2 0 1 1>;
		        qcom,mem1 = <0 0x3d00 2 0 1 0>;
		        qcom,page2 = <1 0x3d88 2 0x7010 2 1>;
		        qcom,poll2 = <0 0x0 2 0 1 1>;
		        qcom,mem2 = <0 0x3d00 2 0 1 0>;
		        qcom,page3 = <1 0x3d8A 2 0x70F4 2 1>;
		        qcom,pageen3 = <1 0x3d81 2 0x01 1 10>;
		        qcom,poll3 = <0 0x0 2 0 1 1>;
		        qcom,mem3 = <228 0x7010 2 0 1 1>;

		        cam_vio-supply = <&pm8909_l6>;
		        cam_vaf-supply = <&pm8909_l8>;
		        qcom,cam-vreg-name = "cam_vio", "cam_vana", "cam_vaf";
		        qcom,cam-vreg-type = <0 0 0>;
		        qcom,cam-vreg-min-voltage = <0 2800000 2850000>;
		        qcom,cam-vreg-max-voltage = <0 2850000 2900000>;
		        qcom,cam-vreg-op-mode = <0 80000 120000>;
		        pinctrl-names = "cam_default", "cam_suspend";
		        pinctrl-0 = <&cam_sensor_mclk0_default
				        &cam_sensor_rear_default>;
		        pinctrl-1 = <&cam_sensor_mclk0_sleep &cam_sensor_rear_sleep>;
		        gpios = <&msm_gpio 26 0>,
			        <&msm_gpio 35 0>,
			        <&msm_gpio 34 0>,
			        <&msm_gpio 14 0>,
			        <&msm_gpio 11 0>;
		        qcom,gpio-reset = <1>;
		        qcom,gpio-standby = <2>;
		        qcom,gpio-vdig = <3>;
		        qcom,gpio-vana = <4>;
		        qcom,gpio-req-tbl-num = <0 1 2 3 4>;
		        qcom,gpio-req-tbl-flags = <1 0 0 0 0>;
		        qcom,gpio-req-tbl-label = "CAMIF_MCLK",
			        "CAM_RESET1",
			        "CAM_STANDBY",
			        "CAM_VDIG_EN",
			        "CAM_VANA_EN";
		        qcom,cam-power-seq-type = "sensor_gpio","sensor_gpio",
		                "sensor_vreg", "sensor_clk",
		                "sensor_gpio", "sensor_gpio";
		        qcom,cam-power-seq-val = "sensor_gpio_vdig","sensor_gpio_vana",
		                "cam_vio", "sensor_cam_mclk",
		                "sensor_gpio_reset",
		                "sensor_gpio_standby";
		        qcom,cam-power-seq-cfg-val = <1 1 1 24000000 1 1>;
		        qcom,cam-power-seq-delay = <15 15 1 15 15 10>;

		        clocks = <&clock_gcc clk_mclk0_clk_src>,
				        <&clock_gcc clk_gcc_camss_mclk0_clk>;
		        clock-names = "cam_src_clk", "cam_clk";
	        };
        eeprom1: qcom,eeprom@6c {
		        cell-index = <0>;
		        reg = <0x20>;
		        qcom,eeprom-name = "truly_s5k4h5yc";
		        compatible = "qcom,eeprom";
		        qcom,slave-addr = <0x20>;
		        qcom,num-blocks = <3>;

		qcom,init0 =   <1 0x0100 2 0x01 1 50>;
		qcom,page0 =   <0 0 2 0 1 1>;
		qcom,pageen0 = <0 0 2 0 1 1>;
		qcom,poll0 =   <0 0 2 0 1 1>;
		qcom,mem0 =   <0 0x0 2 0 1 0>;
		
		qcom,init1 =   <1 0x3A02 2 0x00 1 1>;
		qcom,page1=   <1 0x3A00 2 0x01 1 20>;
		qcom,pageen1 = <0 0 2 0 1 1>;
		qcom,poll1 =   <0 0 2 0 1 1>;
		qcom,mem1 =   <64 0x3A04 2 0x00 1 0>;

		qcom,init2 =   <1 0x3A00 2 0x00 1 1>;
		qcom,page2 =   <0 0 2 0 1 1>;
		qcom,pageen2 = <0 0 2 0 1 1>;
		qcom,poll2 =   <0 0 2 0 1 1>;
		qcom,mem2 =   <0 0x0 2 0 1 0>;

		        cam_vio-supply = <&pm8909_l6>;
		        cam_vaf-supply = <&pm8909_l8>;
		        qcom,cam-vreg-name = "cam_vio", "cam_vana", "cam_vaf";
		        qcom,cam-vreg-type = <0 0 0>;
		        qcom,cam-vreg-min-voltage = <0 2800000 2850000>;
		        qcom,cam-vreg-max-voltage = <0 2850000 2900000>;
		        qcom,cam-vreg-op-mode = <0 80000 120000>;
		        pinctrl-names = "cam_default", "cam_suspend";
		        pinctrl-0 = <&cam_sensor_mclk0_default
				        &cam_sensor_rear_default>;
		        pinctrl-1 = <&cam_sensor_mclk0_sleep &cam_sensor_rear_sleep>;
		        gpios = <&msm_gpio 26 0>,
			        <&msm_gpio 35 0>,
			        <&msm_gpio 34 0>,
			        <&msm_gpio 14 0>,
			        <&msm_gpio 11 0>;
		        qcom,gpio-reset = <1>;
		        qcom,gpio-standby = <2>;
		        qcom,gpio-vdig = <3>;
		        qcom,gpio-vana = <4>;
		        qcom,gpio-req-tbl-num = <0 1 2 3 4>;
		        qcom,gpio-req-tbl-flags = <1 0 0 0 0>;
		        qcom,gpio-req-tbl-label = "CAMIF_MCLK",
			        "CAM_RESET1",
			        "CAM_STANDBY",
			        "CAM_VDIG_EN",
			        "CAM_VANA_EN";
		        qcom,cam-power-seq-type = "sensor_gpio","sensor_gpio",
		                "sensor_vreg", "sensor_clk",
		                "sensor_gpio", "sensor_gpio";
		        qcom,cam-power-seq-val = "sensor_gpio_vdig","sensor_gpio_vana",
		                "cam_vio", "sensor_cam_mclk",
		                "sensor_gpio_reset",
		                "sensor_gpio_standby";
		        qcom,cam-power-seq-cfg-val = <1 1 1 24000000 1 1>;
		        qcom,cam-power-seq-delay = <15 15 1 15 15 10>;

		        clocks = <&clock_gcc clk_mclk0_clk_src>,
				        <&clock_gcc clk_gcc_camss_mclk0_clk>;
		        clock-names = "cam_src_clk", "cam_clk";
	        };
	qcom,camera@0 {
		cell-index = <0>;
		compatible = "qcom,camera";
		reg = <0x2>;
		qcom,csiphy-sd-index = <0>;
		qcom,csid-sd-index = <0>;
		qcom,mount-angle = <90>;
		qcom,actuator-src = <&actuator0>;
		qcom,eeprom-src = <&eeprom0 &eeprom1>;
		qcom,led-flash-src = <&led_flash0>;
		cam_vana-supply = <&pm8909_l17>;
		cam_vio-supply = <&pm8909_l6>;
		//cam_vdig-supply = <&pm8909_l6>;
		//cam_vaf-supply = <&pm8909_l17>;
		qcom,cam-vreg-name = "cam_vio", "cam_vana";
		qcom,cam-vreg-type = <0 0>;
		qcom,cam-vreg-min-voltage = <0 2800000>;
		qcom,cam-vreg-max-voltage = <0 2850000>;
		qcom,cam-vreg-op-mode = <0 80000>;
		pinctrl-names = "cam_default", "cam_suspend";
		pinctrl-0 = <&cam_sensor_mclk0_default
				&cam_sensor_rear_default>;
		pinctrl-1 = <&cam_sensor_mclk0_sleep &cam_sensor_rear_sleep>;
		gpios = <&msm_gpio 26 0>,
			<&msm_gpio 35 0>,
			<&msm_gpio 33 0>;
			//<&msm_gpio 14 0>,
			//<&msm_gpio 11 0>,
			//<&msm_gpio 95 0>;
		qcom,gpio-reset = <1>;
		qcom,gpio-standby = <2>;
		//qcom,gpio-vdig = <3>;
		//qcom,gpio-vana = <4>;
		//qcom,gpio-af-pwdm = <5>;
		qcom,gpio-req-tbl-num = <0 1 2>;
		qcom,gpio-req-tbl-flags = <1 0 0>;
		qcom,gpio-req-tbl-label = "CAMIF_MCLK",
			"CAM_RESET1",
			"CAM_STANDBY";
			//"CAM_VDIG_EN",
			//"CAM_VANA_EN",
			//"CAM_AF_PWDM";
		qcom,sensor-position = <0>;
		qcom,sensor-mode = <0>;
		qcom,cci-master = <0>;
		status = "ok";
		clocks = <&clock_gcc clk_mclk0_clk_src>,
				<&clock_gcc clk_gcc_camss_mclk0_clk>;
		clock-names = "cam_src_clk", "cam_clk";
	};

	qcom,camera@1 {
		cell-index = <1>;
		compatible = "qcom,camera";
		reg = <0x1>;
		qcom,csiphy-sd-index = <0>;
		qcom,csid-sd-index = <0>;
		qcom,mount-angle = <90>;
		cam_vana-supply = <&pm8909_l17>;
		cam_vio-supply = <&pm8909_l6>;
		//cam_vdig-supply = <&pm8909_l6>;
		qcom,cam-vreg-name = "cam_vio", "cam_vana";
		qcom,cam-vreg-type = <0 0>;
		qcom,cam-vreg-min-voltage = <0 2850000>;
		qcom,cam-vreg-max-voltage = <0 2850000>;
		qcom,cam-vreg-op-mode = <0 80000>;
		pinctrl-names = "cam_default", "cam_suspend";
		pinctrl-0 = <&cam_sensor_mclk1_default &cam_sensor_front_default>;
		pinctrl-1 = <&cam_sensor_mclk1_sleep &cam_sensor_front_sleep>;
		gpios = <&msm_gpio 27 0>,
			<&msm_gpio 28 0>,
			<&msm_gpio 34 0>;
			//<&msm_gpio 11 0>,
			//<&msm_gpio 15 0>;
		qcom,gpio-reset = <1>;
		qcom,gpio-standby = <2>;
		//qcom,gpio-vdig = <3>;
		//qcom,gpio-vana = <4>;
		qcom,gpio-req-tbl-num = <0 1 2>;
		qcom,gpio-req-tbl-flags = <1 0 0>;
		qcom,gpio-req-tbl-label = "CAMIF_MCLK",
			"CAM_RESET",
			"CAM_STANDBY";
			//"CAM_VANA_EN",
			//"CAM_VDIG_EN";
		qcom,cci-master = <0>;
		status = "ok";
		clocks = <&clock_gcc clk_mclk1_clk_src>,
				<&clock_gcc clk_gcc_camss_mclk1_clk>;
		clock-names = "cam_src_clk", "cam_clk";
	};
};

