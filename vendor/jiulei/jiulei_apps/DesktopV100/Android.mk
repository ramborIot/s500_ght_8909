ifeq ($(YJ_PRELOAD_APP),true)

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional

#LOCAL_PREBUILT_JNI_LIBS:= \
#	@lib/armeabi/libDDIApi.so \
#	@lib/armeabi/libS500Protocol.so \
#	@lib/armeabi/libuatr.so \



LOCAL_MODULE := DesktopV100
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk
LOCAL_MODULE_CLASS := APPS
#LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_MODULE_SUFFIX := .apk
#LOCAL_MODULE_PATH := $(TARGET_OUT_DATA_APPS) 
LOCAL_CERTIFICATE := platform
#LOCAL_DEX_PREOPT    := false
#LOCAL_PROGUARD_ENABLED := disabled
include $(BUILD_PREBUILT)

endif