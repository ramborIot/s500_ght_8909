package com.szzt.shutdowndialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Adapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.widget.AdapterView;

public class FloatWindowSmallView extends LinearLayout{


    /**
     * 记录小悬浮窗的宽度
     */
    public static int viewWidth;

    /**
     * 记录小悬浮窗的高度
     */
    public static int viewHeight;

    private Context mContext = null;
    
    private String TAG = FloatWindowSmallView.class.getSimpleName();
   
    
    public FloatWindowSmallView(Context context) {
        super(context);
        View view = LayoutInflater.from(context).inflate(R.layout.shutdowndialog_time_select, this);
     
        mContext = getContext();
       
        showSuperProgressDialog();
    }
    private void removeWindow()
    {
        if(MyWindowManager.isWindowShowing())
    	{
    		MyWindowManager.removeSmallWindow(getContext());
    	}
    		
    }
	/**
	 * @param paramContext
	 * @param paramString1
	 * @param paramString2
	 * @param paramInt
	 * @param paramOnTimeOutListener
	 * @return
	 */
	public SuperProgressDialog showSuperProgressDiaglog(Context paramContext,
			String paramString1, String paramString2, int paramInt,
			SuperProgressDialog.OnTimeOutListener paramOnTimeOutListener) {
		SuperProgressDialog localSuperProgressDialog = SuperProgressDialog
				.createProgressDialog(paramContext, paramInt,
						paramOnTimeOutListener);
		localSuperProgressDialog.setMessage(paramString2);
		localSuperProgressDialog.setCancelable(false);
		
		localSuperProgressDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		localSuperProgressDialog.show();
		return localSuperProgressDialog;
	}
    private void showSuperProgressDialog()
    {
    showSuperProgressDiaglog(mContext, "shutdown", "waitting shutdown...",
			10000, new SuperProgressDialog.OnTimeOutListener() {
				public void onTimeOut(
						SuperProgressDialog paramAnonymousSuperProgressDialog) {
					Toast.makeText(mContext, "one minutes", 1).show();
				

				}
			});
    
  
    }
   

}
