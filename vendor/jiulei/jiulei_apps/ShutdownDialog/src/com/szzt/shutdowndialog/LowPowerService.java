package com.szzt.shutdowndialog;

import java.util.Timer;
import java.util.TimerTask;

import com.szzt.configure.Configure;
import com.szzt.shutdowndialog.SuperProgressDialog.OnTimeOutListener;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.IBinder;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

public class LowPowerService extends Service {

	private LowpowerReceiver receiver = null;
	private Context mContext = null;
	private int mBatteryLevel = 100;
	private int mBatteryStatus = BatteryManager.BATTERY_STATUS_UNKNOWN;
	private static boolean isSchedule = false;
	private Timer shutdownTimer;
	private TimerTask shutdownTimerTask;
	private static int DEFAULT_SHUTDOWN_TIME = 20000;
	private SuperProgressDialog progressDialog = null;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		
		if(Configure.DEBUG)
		{
			Log.d(Configure.TAG, "service onCreate");
		}
		mContext = getApplicationContext();
		receiver = new LowpowerReceiver();
		initReceiver();
		super.onCreate();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		destroyReceiver();
		super.onDestroy();
	}

	// 自定义方法

	private void initReceiver() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_BATTERY_CHANGED);
		registerReceiver(receiver, filter);

	}

	private void destroyReceiver() {
		unregisterReceiver(receiver);
	}

	/**
	 * fanyanming add
	 */

	private void restartShutdownTask() {
		cacelShutdownTimer();
		shutdownTimer = new Timer();
		shutdownTimerTask = new TimerTask() {

			@Override
			public void run() {

				Intent intent = new Intent();
				intent.setAction("com.szzt.shutdown.broadcast");
				mContext.sendBroadcast(intent);

			}
		};
		shutdownTimer.schedule(shutdownTimerTask, DEFAULT_SHUTDOWN_TIME);

	}

	
	private void cacelShutdownTimer() {
		if (shutdownTimer != null) {
			shutdownTimer.cancel();
			shutdownTimer = null;
		}
		if (shutdownTimerTask != null) {
			shutdownTimerTask.cancel();
			shutdownTimerTask = null;
		}
	}

	private void popupWindow()
	{
		
		MyWindowManager windowmanager = new MyWindowManager();
		windowmanager.createSmallWindow(mContext);
		
	}
	
	private void showShutdownDialog() {

		showSuperProgressDialog();
		restartShutdownTask();
	}
	/**
	 * @param paramContext
	 * @param paramString1
	 * @param paramString2
	 * @param paramInt
	 * @param paramOnTimeOutListener
	 * @return
	 */
	public SuperProgressDialog showSuperProgressDiaglog(Context paramContext,
			String paramString1, String paramString2, int paramInt,
			SuperProgressDialog.OnTimeOutListener paramOnTimeOutListener) {
		SuperProgressDialog localSuperProgressDialog = SuperProgressDialog
				.createProgressDialog(paramContext, paramInt,
						paramOnTimeOutListener);
		localSuperProgressDialog.setMessage(paramString2);
		localSuperProgressDialog.setCancelable(false);
		
		localSuperProgressDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		localSuperProgressDialog.show();
		return localSuperProgressDialog;
	}
    private void showSuperProgressDialog()
    {
    	progressDialog =  showSuperProgressDiaglog(mContext, "", getResources().getString(R.string.shuttingdown),
			DEFAULT_SHUTDOWN_TIME, new SuperProgressDialog.OnTimeOutListener() {
				public void onTimeOut(
						SuperProgressDialog paramAnonymousSuperProgressDialog) {

				}
			});
    
  
    }


	private class LowpowerReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context arg0, Intent intent) {
			// TODO Auto-generated method stub

			String action = intent.getAction();
			if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {

				mBatteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL,
						100);

				intent.getIntExtra(BatteryManager.EXTRA_STATUS,
						BatteryManager.BATTERY_STATUS_UNKNOWN);
				mBatteryStatus = intent.getIntExtra(
						BatteryManager.EXTRA_STATUS,
						BatteryManager.BATTERY_STATUS_UNKNOWN);
				// fanyanming add begin

				if (mBatteryLevel < 5 && !isSchedule && mBatteryStatus!=BatteryManager.BATTERY_STATUS_CHARGING ) //
				{
					isSchedule = true;
					showShutdownDialog();
					//popupWindow();

				}
				else if(mBatteryStatus == BatteryManager.BATTERY_STATUS_CHARGING)
				{
					if(isSchedule == true)
					{
						//取消关机
						isSchedule = false;
						if(progressDialog != null && progressDialog.isShowing())
						{
							
						progressDialog.dismiss();
						}
						
						cacelShutdownTimer();
					}
				}

				// add end

			}

		}

	}
	//

}
