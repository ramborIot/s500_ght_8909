package com.szzt.shutdowndialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import java.util.Timer;
import java.util.TimerTask;

public class SuperProgressDialog extends ProgressDialog
{
  public static final String TAG = "ProgressDialog";
  private Handler mHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (SuperProgressDialog.this.mTimeOutListener != null)
      {
        SuperProgressDialog.this.mTimeOutListener.onTimeOut(SuperProgressDialog.this);
        SuperProgressDialog.this.dismiss();
      }
    }
  };
  private long mTimeOut = 0L;
  private OnTimeOutListener mTimeOutListener = null;
  private Timer mTimer = null;

  public SuperProgressDialog(Context paramContext)
  {
    super(paramContext);
  }

  public static SuperProgressDialog createProgressDialog(Context paramContext, long paramLong, OnTimeOutListener paramOnTimeOutListener)
  {
    SuperProgressDialog localSuperProgressDialog = new SuperProgressDialog(paramContext);
    if (paramLong != 0L)
      localSuperProgressDialog.setTimeOut(paramLong, paramOnTimeOutListener);
    return localSuperProgressDialog;
  }

  public void onStart()
  {
    super.onStart();
    if (this.mTimeOut != 0L)
    {
      this.mTimer = new Timer();
      TimerTask local2 = new TimerTask()
      {
        public void run()
        {
          Message localMessage = SuperProgressDialog.this.mHandler.obtainMessage();
          SuperProgressDialog.this.mHandler.sendMessage(localMessage);
        }
      };
      this.mTimer.schedule(local2, this.mTimeOut);
    }
  }

  protected void onStop()
  {
    super.onStop();
    if (this.mTimer != null)
    {
      this.mTimer.cancel();
      this.mTimer = null;
    }
  }

  public void setTimeOut(long paramLong, OnTimeOutListener paramOnTimeOutListener)
  {
    this.mTimeOut = paramLong;
    if (paramOnTimeOutListener != null)
      this.mTimeOutListener = paramOnTimeOutListener;
  }

  public static abstract interface OnTimeOutListener
  {
    public abstract void onTimeOut(SuperProgressDialog paramSuperProgressDialog);
  }
}
