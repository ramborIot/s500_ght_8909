package com.szzt.shutdowndialog;

import com.szzt.configure.Configure;

import android.content.Context;
import android.graphics.PixelFormat;
import android.graphics.drawable.AnimationDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;

public class MyWindowManager {


    private static FloatWindowSmallView smallWindow;
    private static WindowManager mWindowManager;
    private static AnimationDrawable anim = null;
    private static Button img = null;


    private static float mTouchStartX;
    private static float mTouchStartY;
    private static float x;
    private static float y;
    private static WindowManager windowManager = null;
    private  static  WindowManager.LayoutParams smallWindowParams ;
    /**
     * @param context 必须为应用程序的Context.
     */
    public static void createSmallWindow(final Context context) {
         windowManager = getWindowManager(context);

        
        if (smallWindow == null) {
            smallWindow = new FloatWindowSmallView(context);
            if (smallWindowParams == null) {
                smallWindowParams = new LayoutParams();
                smallWindowParams.type = LayoutParams.TYPE_PHONE;
                smallWindowParams.format = PixelFormat.RGBA_8888;
               
                smallWindowParams.flags = LayoutParams.FLAG_ALT_FOCUSABLE_IM&LayoutParams.FLAG_NOT_FOCUSABLE; 
                
                smallWindowParams.gravity = Gravity.CENTER ;
                smallWindowParams.width = 400;
                smallWindowParams.height = 400;
                smallWindowParams.x = 0;
                smallWindowParams.y = 0;
            }
            smallWindow.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent event) {

                    //获取相对屏幕的坐标，即以屏幕左上角为原点
                    x = event.getRawX();
                    y = event.getRawY()-25;   //25是系统状态栏的高度
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            //获取相对View的坐标，即以此View左上角为原点
                            mTouchStartX =  event.getX();
                            mTouchStartY =  event.getY();

                            break;
                       
                    }
                    return true;
                }
            });
            windowManager.addView(smallWindow, smallWindowParams);
        if(Configure.DEBUG)
        {
        	Log.d(Configure.TAG, "windowManager.addView");
        }
   
        }
    }

    private static void updateViewPosition(){
        //更新浮动窗口位置参数
   
        smallWindowParams.x =(int)( x-mTouchStartX);
        smallWindowParams.y=(int) (y-mTouchStartY);
        windowManager.updateViewLayout(smallWindow, smallWindowParams);

    }
    /**
     * 将小悬浮窗从屏幕上移除。
     *
     * @param context 必须为应用程序的Context.
     */
    public static void removeSmallWindow(Context context) {
        if (smallWindow != null) {

            WindowManager windowManager = getWindowManager(context);
            windowManager.removeView(smallWindow);
            smallWindow = null;
        }
    }

    /**
     * 是否有悬浮窗显示在屏幕上。
     *
     * @return 有悬浮窗显示在桌面上返回true，没有的话返回false。
     */
    public static boolean isWindowShowing() {
        return smallWindow != null;
    }

    /**
     * 如果WindowManager还未创建，则创建一个新的WindowManager返回。否则返回当前已创建的WindowManager。
     *
     * @param context 必须为应用程序的Context.
     * @return WindowManager的实例，用于控制在屏幕上添加或移除悬浮窗。
     */
    private static WindowManager getWindowManager(Context context) {
        if (mWindowManager == null) {
            mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        }
        return mWindowManager;
    }
	
}
