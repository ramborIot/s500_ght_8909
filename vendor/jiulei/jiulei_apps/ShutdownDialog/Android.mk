ifeq ($(JL_PRELOAD_APP),true)
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_PACKAGE_NAME := shutdowndialog
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(call all-java-files-under, src) \
		
LOCAL_CERTIFICATE := platform


include $(BUILD_PACKAGE)
include $(call all-makefiles-under,$(LOCAL_PATH))

endif