ifeq ($(YD_PRELOAD_APP),true)
LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional

LOCAL_PREBUILT_JNI_LIBS:= \
	@lib/armeabi/libDDIApi.so \
	@lib/armeabi/libS500Protocol.so \
	@lib/armeabi/libuatr.so \
	@lib/armeabi/libCrypto.so \
	@lib/armeabi/libEMV_PBOC_Kernel.so \
	@lib/armeabi/libEMVLib.so \
	@lib/armeabi/libJniUtils.so \
	@lib/armeabi/libmsc.so \
	@lib/armeabi/libSDK.so



LOCAL_MODULE := PayLauncher
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/app 
LOCAL_CERTIFICATE := platform
LOCAL_DEX_PREOPT    := false
LOCAL_PROGUARD_ENABLED := disabled
include $(BUILD_PREBUILT)
endif