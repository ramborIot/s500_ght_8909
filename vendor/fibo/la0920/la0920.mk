export TARGET_BRAND=fibo
export TARGET_CHIPCODE=msm8909

TARGET_SYSTEM_PROP := device/qcom/$(TARGET_CHIPCODE)/system.prop    
TARGET_RELEASETOOLS_EXTENSIONS := device/qcom/common    
TARGET_RECOVERY_FSTAB := device/qcom/$(TARGET_CHIPCODE)/recovery.fstab
$(call inherit-product, device/qcom/$(TARGET_CHIPCODE)/$(TARGET_CHIPCODE).mk)  

#PRODUCT_NAME must be unique
PRODUCT_NAME := la0920
PRODUCT_DEVICE := la0920
PRODUCT_MODEL := S500
PRODUCT_BRAND := Fibocom
PRODUCT_MANUFACTURER := Fibocom
PRODUCT_MANUFACTURER_CODE := Fibocom


#defaut language is en_US
PRODUCT_LOCALES := zh_CN en_US zh_TW
PRODUCT_LOCALES += hdpi mdpi

PRODUCT_DEFAULT_DEV_CERTIFICATE := build/target/product/security/releasekey

BUILD_DISPLAY_ID := S500AFU2171116006
PRODUCT_SUPPORTS_VERITY := true
PRODUCT_SYSTEM_VERITY_PARTITION := /dev/block/bootdevice/by-name/system


#[WARNING] DON'T SET DEVICE_PACKAGE_OVERLAYS
#PRODUCT_PACKAGE_OVERLAYS is already be set in $(GCPATH)/common.mk

#default.prop
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
		persist.sys.usb.config=diag,serial_smd,rmnet_qti_bam \
		ro.usb.default.config=mtp,adb \
		ro.sys.usb.default.config=mtp,adb \

#build.prop
PRODUCT_PROPERTY_OVERRIDES += \
			 persist.radio.start_ota_daemon=1 \
			 sys.ramdump.settings=0 \

#new additional
ADDITIONAL_BUILD_PROPERTIES += \
       persist.sys.language=zh \
       persist.sys.country=CN \
       persist.sys.timezone=Asia/Shanghai \
       ro.sys.languages=zh,en_US \
	   ro.product.locale.language=zh \
	   ro.product.locale.region=CN    \
       persist.debug.wfd.enable=0 \
       ro.sf.lcd_density=320 


# The maximum number of hidden processes we will keep
ADDITIONAL_BUILD_PROPERTIES += \
       ro.sys.fw.bg_apps_limit=6

ADDITIONAL_BUILD_PROPERTIES += \
       persist.camera.tintless=enable \
       persist.camera.longshot.max=20

#vendor app define
  YJ_PRELOAD_APP := false
  JL_PRELOAD_APP := true
  YT_PRELOAD_APP := false
  YD_PRELOAD_APP := true
  
#如果make clean了要全编，则要打开以下宏，编译之后，在关闭。才能全编通过，因为全编需要的中间lib不能裁剪  
# JL_NO_CJ :=true  

	   
	   
PRODUCT_PACKAGE_OVERLAYS := \
    vendor/$(TARGET_BRAND)/$(TARGET_PRODUCT)/overlay \
    $(PRODUCT_PACKAGE_OVERLAYS)

#add memsic sensor calibration lib
PRODUCT_PACKAGES += \
    libcalmodule_memsic

# WifiAp join-in monitor
PRODUCT_PACKAGES += \
    new.sat.join.monitor.sh \
    StaInfoMonitor

REMOVE_PACKAGES += \
    AutoStartManager \
    Upgrade

PRODUCT_PACKAGES += \
    AccountAndSyncSettings \
    AntHalService \
    BackupRestoreConfirmation \
    BasicDreams \
    Bluetooth \
    Calculator \
    Calculator2 \
    CalculatorX \
    Calendar \
    CalendarLocalAccount \
    CalendarWidget \
    CalendarProvider \
    Camera \
    Camera2 \
    CellBroadcastReceiver \
    CellBroadcastWidget \
    ConfigurationClient \
    Contacts \
    ContactsProvider \
    DataMonitor \
    DeskClock \
    Dialer \
    DocumentsUI \
    Email \
    Email2 \
    Evt \
    Exchange \
    Exchange2 \
    FlashLight \
    FM2 \
    FMRecord \
    Gallery \
    Gallery2 \
    Galaxy4 \
    goso_charger \
    GosoSystemService \
    GsFileExplorer \
    GsCamera \
    GsGpsNmea \
    GsHelper \
    Home \
    InCallUI \
    Launcher2 \
    LiveWallpapers \
    LiveWallpapersPicker \
    MMITool \
    Mms \
    MmsService \
    Music \
    MusicFX \
    ntfs-3g \
    OneTimeInitializer \
    PackageInstaller \
    PhaseBeam \
    Phone \
    PhoneCommon \
    PhoneFeatures \
    PhotoTable \
    PicoTts \
    PPPreference \
    PrintSpooler \
    Protips \
    QtiBackupAgent \
    QtiDdsSwitchService \
    QuickSearchBox \
    Settings \
    SimContacts \
    SoundRecorder \
    SnapdragonCamera \
    StopTimer \
    SystemUpdate \
    Stk \
    TcpTool \
    TimerSwitch \
    Updater \
    UsbPortSwitcher \
    UserDictionaryProvider \
    VideoCall \
    VideoEditor \
    WAPPushManager \
    WorldClock \

REMOVE_PACKAGES += \
    AntiTheftDemo \
    EngineerTool \
    EngineerToolOp \
    EmbmsTestApp \
    QualcommSettings \
    ModemTestMode \
    MSDC_UI \
    VoiceDialer \
    TouchPal_Global \
    UnifiedEmail \

FILTER_OUT_PACKAGES += \
    LunarInfoProvider \
    OmaDownload \
    OmaDrmEngine \
    OmaDrmUiConroller \
    CNEService \

REMOVE_PACKAGES += \
    embms \
    embms.xml

PRODUCT_PACKAGES += \
    tftp_server \
    ATFWD-daemon \
    mm-qcamera-daemon


#ims
#IMS_CONNECTIONMANAGER
IMS_CONNECTIONMANAGER := imscmservice
IMS_CONNECTIONMANAGER += lib-imsrcscmservice
IMS_CONNECTIONMANAGER += lib-imsrcscmclient
IMS_CONNECTIONMANAGER += lib-imsrcscm
IMS_CONNECTIONMANAGER += lib-ims-rcscmjni
IMS_CONNECTIONMANAGER += imscmlibrary
IMS_CONNECTIONMANAGER += imscm.xml

#IMS_NEWARCH
IMS_NEWARCH := imsdatadaemon
IMS_NEWARCH += imsqmidaemon
IMS_NEWARCH += ims_rtp_daemon
IMS_NEWARCH += lib-dplmedia
IMS_NEWARCH += lib-imsdpl
IMS_NEWARCH += lib-imsqimf
IMS_NEWARCH += lib-imsSDP
IMS_NEWARCH += lib-rtpcommon
IMS_NEWARCH += lib-rtpcore
IMS_NEWARCH += lib-rtpdaemoninterface
IMS_NEWARCH += lib-rtpsl

#IMS_REGMGR
IMS_REGMGR := RegmanagerApi

#IMS_VT
IMS_VT := lib-imsvt
IMS_VT += lib-imscamera
IMS_VT += libvcel

#IMS_SETTINGS
IMS_SETTINGS := lib-imss
IMS_SETTINGS += lib-rcsimssjni

#IMS_RCS
IMS_RCS := lib-imsxml
IMS_RCS += lib-imsrcs
IMS_RCS += lib-rcsjni

#IMS
IMS := exe-ims-regmanagerprocessnative
#IMS += exe-ims-videoshareprocessnative
IMS += lib-imsdpl
IMS += lib-imsfiledemux
IMS += lib-imsfilemux
IMS += lib-imsqimf
IMS += lib-ims-regmanagerbindernative
IMS += lib-ims-regmanagerjninative
IMS += lib-ims-regmanager
IMS += lib-ims-videosharebindernative
IMS += lib-ims-videosharejninative
IMS += lib-ims-videoshare

# IMS Telephony Libs
IMS_TEL := ims.xml
IMS_TEL += imslibrary
IMS_TEL += ims


REMOVE_PACKAGES += \
    $(IMS_CONNECTIONMANAGER) \
    $(IMS_NEWARCH) \
    $(IMS_REGMGR) \
    $(IMS_VT) \
    $(IMS_SETTINGS) \
    $(IMS_RCS) \
    $(IMS) \
    $(IMS_TEL) \
    libimsmedia_jni \
    libimscamera_jni \
    ims \
    imstests

#CNE
CNE := andsfCne.xml
CNE += cnd
CNE += cneapiclient
CNE += cneapiclient.xml
CNE += com.quicinc.cne
CNE += com.quicinc.cne.xml
CNE += com.quicinc.cneapiclient
CNE += CNEService
CNE += libcne
CNE += libcneapiclient
CNE += libcneconn
CNE += libcneqmiutils
#CNE += libmasc
#CNE += libNimsWrap
#CNE += libvendorconn
#CNE += libwqe
#CNE += libxml
#CNE += SwimConfig.xml
#CNE += testclient
#CNE += testserver

FILTER_OUT_PACKAGES += \
    $(CNE)

DPM := com.qti.dpmframework
DPM += dpm.conf
DPM += dpmapi
DPM += dpmapi.xml
DPM += dpmd
DPM += libdpmctmgr
DPM += libdpmfdmgr
DPM += libdpmframework
DPM += libdpmnsrm
DPM += libdpmtcm
DPM += NsrmConfiguration.xml
DPM += tcmclient

FILTER_OUT_PACKAGES += \
    $(DPM)

#FLASH
FLASH := install_flash_player.apk
FLASH += libflashplayer.so
FLASH += libstagefright_froyo.so
FLASH += libstagefright_honeycomb.so
FLASH += libysshared.so
FLASH += oem_install_flash_player.apk
PRODUCT_PACKAGES += \
    $(FLASH)

#FM
FM := fmconfig
FM += fmfactorytest
FM += fmfactorytestserver
FM += fm_qsoc_patches
FILTER_OUT_PACKAGES += \
    $(FM)

#TV_TUNER
TV_TUNER := atv_fe_test
TV_TUNER += dtv_fe_test
TV_TUNER += lib_atv_rf_fe
TV_TUNER += lib_dtv_rf_fe
TV_TUNER += lib_MPQ_RFFE
TV_TUNER += libmpq_bsp8092_cdp_h1
TV_TUNER += libmpq_bsp8092_cdp_h5
TV_TUNER += libmpq_bsp8092_rd_h1
TV_TUNER += lib_tdsn_c231d
TV_TUNER += lib_tdsq_g631d
TV_TUNER += lib_tdss_g682d
TV_TUNER += libmpq_rf_utils
TV_TUNER += lib_sif_demod_stub
TV_TUNER += lib_tv_bsp_mpq8064_dvb
TV_TUNER += lib_tv_receiver_stub
TV_TUNER += libtv_tuners_io
TV_TUNER += tv_driver_test
TV_TUNER += tv_fe_test
TV_TUNER += libUCCP330
TV_TUNER += libForza

REMOVE_PACKAGES += \
    $(TV_TUNER)

#WFD
WFD := capability.xml
WFD += libmmwfdinterface
WFD += libmmwfdsinkinterface
WFD += libmmwfdsrcinterface
WFD += libwfduibcinterface
WFD += libwfduibcsrcinterface
WFD += libwfduibcsrc
WFD += libOmxMux
WFD += libwfdcommonutils
WFD += libwfdhdcpcp
WFD += libwfdlinkstub
WFD += libwfdmmsrc
WFD += libwfdmmutils
WFD += libwfdnative
WFD += libwfdsm
WFD += libwfdservice
WFD += libwfdrtsp
WFD += libextendedremotedisplay
WFD += WfdCommon
WFD += WfdService
WFD += WfdClient
WFD += wfdconfig.xml
WFD += wfdconfigsink.xml
WFD += WfdP2pCommon
WFD += WfdP2pService
WFD += com.qualcomm.wfd.permissions.xml
WFD += wfdservice
REMOVE_PACKAGES += \
    $(WFD)

#WIPOWER
WIPOWER := wbc_hal.default
WIPOWER += com.quicinc.wbc
WIPOWER += com.quicinc.wbc.xml
WIPOWER += com.quicinc.wbcservice
WIPOWER += libwbc_jni
WIPOWER += com.quicinc.wipoweragent
REMOVE_PACKAGES += \
    $(WIPOWER)

#LOG_SYSTEM 
LOG_SYSTEM := Logkit
LOG_SYSTEM += SystemAgent
LOG_SYSTEM += qlogd
LOG_SYSTEM += qlog-conf.xml
LOG_SYSTEM += qdss.cfg
LOG_SYSTEM += default_diag_mask.cfg
LOG_SYSTEM += rootagent
LOG_SYSTEM += init.qcom.rootagent.sh
LOG_SYSTEM += dynamic_debug_mask.cfg
PRODUCT_PACKAGES += \
    $(LOG_SYSTEM) 

#SNAPDRAGON_SDK_DISPLAY
SNAPDRAGON_SDK_DISPLAY := com.qti.snapdragon.sdk.display
SNAPDRAGON_SDK_DISPLAY += com.qti.snapdragon.sdk.display.xml
SNAPDRAGON_SDK_DISPLAY += colorservice
SNAPDRAGON_SDK_DISPLAY += libsd_sdk_display
SNAPDRAGON_SDK_DISPLAY += DisplaySDKSample
SNAPDRAGON_SDK_DISPLAY += QDCMMobileApp
REMOVE_PACKAGES += \
    $(SNAPDRAGON_SDK_DISPLAY)

#NFC
NFC := GsmaNfcService
NFC += libqnfc_nci_jni
NFC += QNfc
NFC += Signedrompatch_v20.bin
NFC += Signedrompatch_v21.bin
NFC += Signedrompatch_v24.bin
NFC += Signedrompatch_v30.bin
NFC += Signedrompatch_va10.bin
NFC += nfc_test.bin
NFC += nfcee_access.xml
NFC += nfc-nci.conf
NFC += hardfault.cfg
NFC += com.android.nfc.helper.xml
REMOVE_PACKAGES += \
    $(NFC)

#MM_CORE
MM_CORE := CABLService
MM_CORE += libdisp-aba
MM_CORE += libmm-abl
MM_CORE += libmm-abl-oem
MM_CORE += libscale
MM_CORE += mm-pp-daemon
MM_CORE += SVIService
MM_CORE += libmm-hdcpmgr
MM_CORE += libvpu
MM_CORE += libvfmclientutils
MM_CORE += libmm-qdcm
MM_CORE += libmm-disp-apis
MM_CORE += libmm-als
MM_CORE += PPPreference
PRODUCT_PACKAGES += \
    $(MM_CORE)

# OMA_IMPL
OMA_IMPL := libomadrmengine
OMA_IMPL += OmaDrmUiController
OMA_IMPL += ConfigurationClient
PRODUCT_PACKAGES += \
    $(OMA_IMPL)

QCOM_SETTINGS_DBG := QualcommSettings
QCOM_SETTINGS_DBG += libDiagService
QCOM_SETTINGS_DBG += libDiagService_32
QCOM_SETTINGS_DBG += QTIDiagServices
REMOVE_PACKAGES += \
    $(QCOM_SETTINGS_DBG)

REMOVE_PACKAGES += \
    VoiceDialer \

PRODUCT_PACKAGES += \
    OpenWnn

PRODUCT_PACKAGES += \
   HoloSpiralWallpaper \
   NoiseField \
   VisualizationWallpapers \

PRODUCT_PACKAGES += \
   GosoSystemService \
   UsbPortSwitcher \
   libdevice_info

#PRODUCT_PACKAGES += \
#    GoogleServicesFramework \
#    GmsCore

PRODUCT_PACKAGES += \
	Camera2 \
	libactuator_dw9714 \
	libmmcamera_ov5648_p5v18g \
    libchromatix_ov5648_p5v18g_common \
    libchromatix_ov5648_p5v18g_default_video \
    libchromatix_ov5648_p5v18g_preview \
    libchromatix_ov5648_p5v18g_snapshot \
	libmmcamera_ov2680 \
    libchromatix_ov2680_common \
    libchromatix_ov2680_default_video \
    libchromatix_ov2680_preview \
    libchromatix_ov2680_snapshot \

# remove some test apk in vendor/qcom/proprietary/common/config/device-vendor.mk
REMOVE_PACKAGES += \
       CarrierLoadService \
	CarrierConfigure \
	CarrierCacheService \
	NativeAudioLatency \
	BatteryGuruSystemApp \
	HomeLocation \
	ODLT
    
# remove some test apk in vendor/qcom/proprietary/common/config/device-vendor.mk
REMOVE_PACKAGES += \
	EmbmsTestApp \
	MultiplePdpTest \
	imstests \
	BTTestApp \
	HiddTestApp \
	PlayreadyDrmTesting \
	QSensorTest \
	QVTester \
	QtiMmsTestApp \
	
PRODUCT_COPY_FILES += vendor/fibo/la0920/privdata1.img:privdata1.img

ifeq ($(YJ_PRELOAD_APP),true)

PRODUCT_COPY_FILES += vendor/fibo/la0920/yijielogo/splash.img:splash.img	


else ifeq ($(YT_PRELOAD_APP),true)
PRODUCT_COPY_FILES += vendor/fibo/la0920/yuantiaologo/splash.img:splash.img	
 
else ifeq ($(YD_PRELOAD_APP),true)
PRODUCT_COPY_FILES += vendor/fibo/la0920/yindianlogo/splash.img:splash.img	
  
else
PRODUCT_COPY_FILES += vendor/fibo/la0920/jollylogo/splash.img:splash.img	

endif
		

PRODUCT_COPY_FILES += vendor/fibo/la0920/jollysp/ACQUIRER.pk:persist/sp/ACQUIRER.pk \
                      vendor/fibo/la0920/jollysp/APK.pem:persist/sp/APK.pem \
					  vendor/fibo/la0920/jollysp/APK.pk:persist/sp/APK.pk \
					  vendor/fibo/la0920/jollysp/dowload_config.ini:persist/sp/dowload_config.ini \
					  vendor/fibo/la0920/jollysp/file.adr:persist/sp/file.adr \
					  vendor/fibo/la0920/jollysp/firmware.bin:persist/sp/firmware.bin \
					  vendor/fibo/la0920/jollysp/FIRMWARE.pk:persist/sp/FIRMWARE.pk \
					  vendor/fibo/la0920/jollysp/ROOT.pk:persist/sp/ROOT.pk \
					  vendor/fibo/la0920/jollysp/s500_config.ini:persist/sp/s500_config.pk \
					  vendor/fibo/la0920/jollysp/spiflash_config.ini:persist/sp/spiflash_config.ini \
					  vendor/fibo/la0920/startdo.sh:system/etc/startdo.sh
					  

#include vendor/google/products/gms.mk
