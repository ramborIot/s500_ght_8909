export TARGET_BRAND=fibo
export TARGET_CHIPCODE=msm8909_512

TARGET_SYSTEM_PROP := device/qcom/$(TARGET_CHIPCODE)/system.prop    
TARGET_RELEASETOOLS_EXTENSIONS := device/qcom/common    
TARGET_RECOVERY_FSTAB := device/qcom/$(TARGET_CHIPCODE)/recovery.fstab

$(call inherit-product, device/qcom/$(TARGET_CHIPCODE)/$(TARGET_CHIPCODE).mk)  

#PRODUCT_NAME must be unique
PRODUCT_NAME := la0910
PRODUCT_DEVICE := la0910
PRODUCT_MODEL := SC800
PRODUCT_BRAND := Fibocom
PRODUCT_MANUFACTURER := Fibocom
PRODUCT_MANUFACTURER_CODE := Fibocom


#defaut language is en_US
PRODUCT_LOCALES := en_US zh_TW zh_CN
PRODUCT_LOCALES += hdpi mdpi

PRODUCT_DEFAULT_DEV_CERTIFICATE := build/target/product/security/releasekey
BUILD_DISPLAY_ID := SC800-V7C.B2.01.001

#PRODUCT_SUPPORTS_VERITY := true
#PRODUCT_SYSTEM_VERITY_PARTITION := /dev/block/bootdevice/by-name/system

#[WARNING] DON'T SET DEVICE_PACKAGE_OVERLAYS
#PRODUCT_PACKAGE_OVERLAYS is already be set in $(GCPATH)/common.mk

#default.prop
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
		persist.sys.usb.config=diag,serial_smd,rmnet_qti_bam \
		ro.usb.default.config=mtp,adb \
		ro.sys.usb.default.config=mtp,adb \

#build.prop
PRODUCT_PROPERTY_OVERRIDES += \

#new additional
ADDITIONAL_BUILD_PROPERTIES += \
       persist.sys.language=zh_CN \
       persist.sys.country=CN \
       persist.sys.timezone=Asia/Shanghai \
       ro.sys.languages=zh_CN,en_US \
       persist.debug.wfd.enable=0 \
       ro.sf.lcd_density=240 \
       qemu.hw.mainkeys=0


# The maximum number of hidden processes we will keep
ADDITIONAL_BUILD_PROPERTIES += \
       ro.sys.fw.bg_apps_limit=6
 
ADDITIONAL_BUILD_PROPERTIES += \
       persist.camera.tintless=enable \
       persist.camera.longshot.max=20


PRODUCT_PACKAGE_OVERLAYS := \
    vendor/$(TARGET_BRAND)/$(TARGET_PRODUCT)/overlay \
    $(PRODUCT_PACKAGE_OVERLAYS)       

PRODUCT_PACKAGES += \
    osi_vsim_client \
    Skyroam \
	libttlv_jni

REMOVE_PACKAGES += \
    Launcher2 \
    WorldClock \
    Music \
    FlashLight \
    GsFileExplorer \
    goso_charger \
    CalculatorX

#add memsic sensor calibration lib
REMOVE_PACKAGES += \
    libcalmodule_memsic

# WifiAp join-in monitor
REMOVE_PACKAGES += \
    new.sat.join.monitor.sh \
    StaInfoMonitor

REMOVE_PACKAGES += \
    AutoStartManager \
    Upgrade

REMOVE_PACKAGES += \
    Calculator \
    Calculator2 \
    SoundRecorder \
    Home

REMOVE_PACKAGES += \
    WorldClock \
    Music \
    FlashLight \
    Music \
    MusicFX \
    UnifiedEmail \
    Email \
    Email2 \
    Gallery2 \
    Gallery \
    Contacts \
    DeskClock \
    Exchange2 \
    Exchange \
    Calendar \
    CalendarProvider \
    VoiceDialer \
    Mms \
    MmsService \
    AccountAndSyncSettings \
    Updater \
    Camera \
    Camera2 \
    VideoEditor \
    FMRecord \
    FM2 \
    QuickSearchBox \
    EmbmsTestApp \
    CalendarLocalAccount \
    CalendarWidget \
    CalendarLocalAccount \
    GsCamera \
    LiveWallpapers \
    LiveWallpapersPicker \
    PhotoTable \
    BasicDreams \
    PhaseBeam \
    Camera2 \
    CellBroadcastWidget \
    CalendarLocalAccount \
    CalendarWidget \
    AntHalService \
    BasicDreams \
    DocumentsUI \
    Galaxy4 \
    GsCamera \
    GsGpsNmea \
    HTMLViewer \
    LiveWallpapers \
    LiveWallpapersPicker \
    PhaseBeam \
    PhotoTable \
    PicoTts \
    PrintSpooler \
    Protips \
    PPPreference \
    QtiBackupAgent \
    QtiDdsSwitchService \
    SimContacts \
    BackupRestoreConfirmation \
    CellBroadcastReceiver \
    VideoCall \
    ConfigurationClient \
    PhoneFeatures \
    QualcommSettings \
    GsHelper \
    GosoSystemService \
    DataMonitor \
    OneTimeInitializer \
    WAPPushManager \
    UserDictionaryProvider \
    InCallUI \
    Stk \

REMOVE_PACKAGES += \
    LunarInfoProvider \
    OmaDownload \
    OmaDrmEngine \
    OmaDrmUiConroller \
    TimerSwitch \
    CNEService \

REMOVE_PACKAGES += \
    embms \
    embms.xml

REMOVE_PACKAGES += \
    tftp_server \
    ATFWD-daemon \
    mm-qcamera-daemon


#ims
#IMS_CONNECTIONMANAGER
IMS_CONNECTIONMANAGER := imscmservice
IMS_CONNECTIONMANAGER += lib-imsrcscmservice
IMS_CONNECTIONMANAGER += lib-imsrcscmclient
IMS_CONNECTIONMANAGER += lib-imsrcscm
IMS_CONNECTIONMANAGER += lib-ims-rcscmjni
IMS_CONNECTIONMANAGER += imscmlibrary
IMS_CONNECTIONMANAGER += imscm.xml

#IMS_NEWARCH
IMS_NEWARCH := imsdatadaemon
IMS_NEWARCH += imsqmidaemon
IMS_NEWARCH += ims_rtp_daemon
IMS_NEWARCH += lib-dplmedia
IMS_NEWARCH += lib-imsdpl
IMS_NEWARCH += lib-imsqimf
IMS_NEWARCH += lib-imsSDP
IMS_NEWARCH += lib-rtpcommon
IMS_NEWARCH += lib-rtpcore
IMS_NEWARCH += lib-rtpdaemoninterface
IMS_NEWARCH += lib-rtpsl

#IMS_REGMGR
IMS_REGMGR := RegmanagerApi

#IMS_VT
IMS_VT := lib-imsvt
IMS_VT += lib-imscamera
IMS_VT += libvcel

#IMS_SETTINGS
IMS_SETTINGS := lib-imss
IMS_SETTINGS += lib-rcsimssjni

#IMS_RCS
IMS_RCS := lib-imsxml
IMS_RCS += lib-imsrcs
IMS_RCS += lib-rcsjni

#IMS
IMS := exe-ims-regmanagerprocessnative
#IMS += exe-ims-videoshareprocessnative
IMS += lib-imsdpl
IMS += lib-imsfiledemux
IMS += lib-imsfilemux
IMS += lib-imsqimf
IMS += lib-ims-regmanagerbindernative
IMS += lib-ims-regmanagerjninative
IMS += lib-ims-regmanager
IMS += lib-ims-videosharebindernative
IMS += lib-ims-videosharejninative
IMS += lib-ims-videoshare

# IMS Telephony Libs
IMS_TEL := ims.xml
IMS_TEL += imslibrary
IMS_TEL += ims


REMOVE_PACKAGES += \
    $(IMS_CONNECTIONMANAGER) \
    $(IMS_NEWARCH) \
    $(IMS_REGMGR) \
    $(IMS_VT) \
    $(IMS_SETTINGS) \
    $(IMS_RCS) \
    $(IMS) \
    $(IMS_TEL) \
    libimsmedia_jni \
    libimscamera_jni \
    ims \
    imstests

#CNE
CNE := andsfCne.xml
CNE += cnd
CNE += cneapiclient
CNE += cneapiclient.xml
CNE += com.quicinc.cne
CNE += com.quicinc.cne.xml
CNE += com.quicinc.cneapiclient
CNE += CNEService
CNE += libcne
CNE += libcneapiclient
CNE += libcneconn
CNE += libcneqmiutils
#CNE += libmasc
#CNE += libNimsWrap
#CNE += libvendorconn
#CNE += libwqe
#CNE += libxml
#CNE += SwimConfig.xml
#CNE += testclient
#CNE += testserver

#REMOVE_PACKAGES += \
    $(CNE)

DPM := com.qti.dpmframework
DPM += dpm.conf
DPM += dpmapi
DPM += dpmapi.xml
DPM += dpmd
DPM += libdpmctmgr
DPM += libdpmfdmgr
DPM += libdpmframework
DPM += libdpmnsrm
DPM += libdpmtcm
DPM += NsrmConfiguration.xml
DPM += tcmclient

REMOVE_PACKAGES += \
    $(DPM)

#FLASH
FLASH := install_flash_player.apk
FLASH += libflashplayer.so
FLASH += libstagefright_froyo.so
FLASH += libstagefright_honeycomb.so
FLASH += libysshared.so
FLASH += oem_install_flash_player.apk
REMOVE_PACKAGES += \
    $(FLASH)

#FM
FM := fmconfig
FM += fmfactorytest
FM += fmfactorytestserver
FM += fm_qsoc_patches
REMOVE_PACKAGES += \
    $(FM)

#TV_TUNER
TV_TUNER := atv_fe_test
TV_TUNER += dtv_fe_test
TV_TUNER += lib_atv_rf_fe
TV_TUNER += lib_dtv_rf_fe
TV_TUNER += lib_MPQ_RFFE
TV_TUNER += libmpq_bsp8092_cdp_h1
TV_TUNER += libmpq_bsp8092_cdp_h5
TV_TUNER += libmpq_bsp8092_rd_h1
TV_TUNER += lib_tdsn_c231d
TV_TUNER += lib_tdsq_g631d
TV_TUNER += lib_tdss_g682d
TV_TUNER += libmpq_rf_utils
TV_TUNER += lib_sif_demod_stub
TV_TUNER += lib_tv_bsp_mpq8064_dvb
TV_TUNER += lib_tv_receiver_stub
TV_TUNER += libtv_tuners_io
TV_TUNER += tv_driver_test
TV_TUNER += tv_fe_test
TV_TUNER += libUCCP330
TV_TUNER += libForza

REMOVE_PACKAGES += \
    $(TV_TUNER)

#WFD
WFD := capability.xml
WFD += libmmwfdinterface
WFD += libmmwfdsinkinterface
WFD += libmmwfdsrcinterface
WFD += libwfduibcinterface
WFD += libwfduibcsrcinterface
WFD += libwfduibcsrc
WFD += libOmxMux
WFD += libwfdcommonutils
WFD += libwfdhdcpcp
WFD += libwfdlinkstub
WFD += libwfdmmsrc
WFD += libwfdmmutils
WFD += libwfdnative
WFD += libwfdsm
WFD += libwfdservice
WFD += libwfdrtsp
WFD += libextendedremotedisplay
WFD += WfdCommon
WFD += WfdService
WFD += WfdClient
WFD += wfdconfig.xml
WFD += wfdconfigsink.xml
WFD += WfdP2pCommon
WFD += WfdP2pService
WFD += com.qualcomm.wfd.permissions.xml
WFD += wfdservice
REMOVE_PACKAGES += \
    $(WFD)

#WIPOWER
WIPOWER := wbc_hal.default
WIPOWER += com.quicinc.wbc
WIPOWER += com.quicinc.wbc.xml
WIPOWER += com.quicinc.wbcservice
WIPOWER += libwbc_jni
WIPOWER += com.quicinc.wipoweragent
REMOVE_PACKAGES += \
    $(WIPOWER)

#LOG_SYSTEM 
LOG_SYSTEM := Logkit
LOG_SYSTEM += SystemAgent
LOG_SYSTEM += qlogd
LOG_SYSTEM += qlog-conf.xml
LOG_SYSTEM += qdss.cfg
LOG_SYSTEM += default_diag_mask.cfg
LOG_SYSTEM += rootagent
LOG_SYSTEM += init.qcom.rootagent.sh
LOG_SYSTEM += dynamic_debug_mask.cfg
REMOVE_PACKAGES += \
    $(LOG_SYSTEM) 

#SNAPDRAGON_SDK_DISPLAY
SNAPDRAGON_SDK_DISPLAY := com.qti.snapdragon.sdk.display
SNAPDRAGON_SDK_DISPLAY += com.qti.snapdragon.sdk.display.xml
SNAPDRAGON_SDK_DISPLAY += colorservice
SNAPDRAGON_SDK_DISPLAY += libsd_sdk_display
SNAPDRAGON_SDK_DISPLAY += DisplaySDKSample
SNAPDRAGON_SDK_DISPLAY += QDCMMobileApp
REMOVE_PACKAGES += \
    $(SNAPDRAGON_SDK_DISPLAY)

#NFC
NFC := GsmaNfcService
NFC += libqnfc_nci_jni
NFC += QNfc
NFC += Signedrompatch_v20.bin
NFC += Signedrompatch_v21.bin
NFC += Signedrompatch_v24.bin
NFC += Signedrompatch_v30.bin
NFC += Signedrompatch_va10.bin
NFC += nfc_test.bin
NFC += nfcee_access.xml
NFC += nfc-nci.conf
NFC += hardfault.cfg
NFC += com.android.nfc.helper.xml
REMOVE_PACKAGES += \
    $(NFC)

#MM_CORE
MM_CORE := CABLService
MM_CORE += libdisp-aba
MM_CORE += libmm-abl
MM_CORE += libmm-abl-oem
MM_CORE += libscale
MM_CORE += mm-pp-daemon
MM_CORE += SVIService
MM_CORE += libmm-hdcpmgr
MM_CORE += libvpu
MM_CORE += libvfmclientutils
MM_CORE += libmm-qdcm
MM_CORE += libmm-disp-apis
MM_CORE += libmm-als
MM_CORE += PPPreference
REMOVE_PACKAGES += \
    $(MM_CORE)

# OMA_IMPL
OMA_IMPL := libomadrmengine
OMA_IMPL += OmaDrmUiController
OMA_IMPL += ConfigurationClient
REMOVE_PACKAGES += \
    $(OMA_IMPL)

QCOM_SETTINGS_DBG := QualcommSettings
QCOM_SETTINGS_DBG += libDiagService
QCOM_SETTINGS_DBG += libDiagService_32
QCOM_SETTINGS_DBG += QTIDiagServices
REMOVE_PACKAGES += \
    $(QCOM_SETTINGS_DBG)

REMOVE_PACKAGES += \
    VoiceDialer \

REMOVE_PACKAGES += \
    CarrierConfigure \

REMOVE_PACKAGES += \
    OpenWnn

REMOVE_PACKAGES += \
   HoloSpiralWallpaper \
   NoiseField \
   VisualizationWallpapers \

#AntiTheftDemo
ANTITHEFTDEMO := AntiTheftDemo     
REMOVE_PACKAGES += \
	$(ANTITHEFTDEMO)


REMOVE_PACKAGES += \
	FactoryKit \
	EmbmsTestApp \
	MultiplePdpTest \
	BtTest \
	HiddTestApp \
	FccTest

#vendor/qcom/proprietary/common/config/device-vendor.mk
REMOVE_PACKAGES += \
	QSensorTest \
	DigitalPenSettings \
	UltrasoundSettings \
	QSSEP11EncryptorDecryptor \
	QSSEPKCS11OtpGen \
	Trepn

#vendor/qcom/proprietary/prebuilt_HY11/target/product/msm8610/Android.mk
REMOVE_PACKAGES += \
	Perfect365 \
	CameraHawk \
	WhipForPhone \
	VideoCall \
	ODLT \
	com.qualcomm.msapu \
	com.qualcomm.msapm \
	com.qualcomm.qlogcat \
	SVIService \
	Logkit \
	CABLService \
	com.qualcomm.criteria.quipsmaptest \
	com.qualcomm.quips.criteria.basicapp \

#vendor/qcom/proprietary/qrdplus/Extension/products.mk
REMOVE_PACKAGES += \
	CarrierLoadService \
	CarrierConfigure \
	CarrierCacheService \
	HomeLocation \
	LEDFlashlight \
	QRDFeatureSettings \
	StopTimer \
	BatteryGuruSystemApp \
	DisplaySDKSample \
	RIDLC2SService \
	RIDLClient \
    AntiTheftDemo \
    ModemTestMode \
    TouchPal_Global \
    GestureMgr

#device/qcom/common/common.mk
REMOVE_PACKAGES += \
	BTTestApp \
	VideoEditor

REMOVE_PACKAGES += \
	BTTestApp \
	HiddTestApp

# remove some test apk in vendor/qcom/proprietary/common/config/device-vendor.mk
REMOVE_PACKAGES += \
	NativeAudioLatency \
	BatteryGuruSystemApp \
	HomeLocation \
	EmbmsTestApp \
	MultiplePdpTest \
	imstests

REMOVE_PACKAGES += \
	QTIDiagServices

# remove cyanogenmod's music and CMFileManager
#REMOVE_PACKAGES += \
	CMFileManager \
	Eleven

REMOVE_PACKAGES += \
	ScopeDebuggerRecordingTool \
	PenPairingApp

REMOVE_PACKAGES += \
	EngineerTool \
	EngineerToolOp
	
PRODUCT_COPY_FILES += vendor/fibo/la0910/privdata1.img:privdata1.img
PRODUCT_COPY_FILES += vendor/fibo/la0910/splash.img:splash.img

#include vendor/google/products/gms.mk	

