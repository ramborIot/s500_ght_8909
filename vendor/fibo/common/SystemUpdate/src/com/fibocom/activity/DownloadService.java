package com.fibocom.activity;

import java.io.File;

import com.fibocom.activity.R;
import com.fibocom.appupdate.AppUpdate;
import com.fibocom.download.HttpDownloader;
import com.fibocom.file.FileUtils;
import com.fibocom.model.UpgradeFileInfo;
import com.fibocom.model.XmlParse;

import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.IBinder;
import android.os.Message;
import android.widget.Toast;

public class DownloadService extends Service{

	private Message msg = new Message();
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		if (intent != null) {
			int downloadtype = intent.getIntExtra("downloadtype", 0);
			if (downloadtype == 1) {
				DownloadThread1 dt = new DownloadThread1("system recovery");
				Thread thread = new Thread(dt);
				thread.start();
			} else if (downloadtype == 2) {
				DownloadThread2 dt = new DownloadThread2("external sdcard update");
				Thread thread = new Thread(dt);
				thread.start();
			} else if (downloadtype == 3) {
				DownloadThread3 dt = new DownloadThread3("usb update");
				Thread thread = new Thread(dt);
				thread.start();
			} else if (downloadtype == 4) {
				DownloadThread4 dt = new DownloadThread4("online update");
				Thread thread = new Thread(dt);
				thread.start();
			}
		}		
		return super.onStartCommand(intent, flags, startId);
	}
	
	class DownloadThread1 implements Runnable{

		public DownloadThread1(String fileName){
			
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			/*msg = MainActivity.handler.obtainMessage();//Add this in order to avoid error"This message is already in use."
			msg.what = 0x0a;
		    MainActivity.handler.sendMessage(msg);
		    try {
				Thread.sleep(5000);//Delay 5s and do next
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			FileUtils.enterRecoveryMode();
		}
		
	}
	
	class DownloadThread2 implements Runnable{

		public DownloadThread2(String fileName){
			
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			msg = MainActivity.handler.obtainMessage();//Add this in order to avoid error"This message is already in use."
			if (FileUtils.getUpgradeFileName(FileUtils.SDCardPath2) != null) {
					//if(!FileUtils.sdcardUpdatePackage.equals(XmlParse.BuildVersion)) {
					try {
						if(FileUtils.readZipFile(FileUtils.SDCardPath2 + FileUtils.getUpgradeFileName(FileUtils.SDCardPath2))
								|| !FileUtils.sdcardUpdatePackage.equals(XmlParse.BuildVersion)) {
							FileUtils.execCommand(FileUtils.SDCardPath2, FileUtils.SDCardPath3);
							msg.what = 0x08;
							/*new AlertDialog.Builder(MainActivity.mContext).setTitle(R.string.system_prompt)
							.setMessage(R.string.system_prompt_content)
							.setPositiveButton(R.string.alert_dialog_yes, new OnClickListener() {
								
								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									// TODO Auto-generated method stub
									FileUtils.execCommand(FileUtils.SDCardPath2, FileUtils.SDCardPath3);
									msg.what = 0x08;
								    MainActivity.handler.sendMessage(msg);
								}
							}).setNegativeButton(R.string.alert_dialog_no, new OnClickListener() {
								
								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									// TODO Auto-generated method stub
									
								}
							});*/
							
						} else {				
							msg.what = 0x07;
						}
						MainActivity.handler.sendMessage(msg);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
		
	}
	
	class DownloadThread3 implements Runnable{

		public DownloadThread3(String fileName){
			
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			msg = MainActivity.handler.obtainMessage();//Add this in order to avoid error"This message is already in use."
			if (FileUtils.getUpgradeFileName(FileUtils.USBPath) != null) {
					//if(!FileUtils.sdcardUpdatePackage.equals(XmlParse.BuildVersion)) {
					try {
						if(FileUtils.readZipFile(FileUtils.USBPath2 + FileUtils.getUpgradeFileName(FileUtils.USBPath))
								|| !FileUtils.sdcardUpdatePackage.equals(XmlParse.BuildVersion)) {
							FileUtils.execCommand(FileUtils.USBPath, FileUtils.USBPath2);
							msg.what = 0x08;
						} else {
							msg.what = 0x07;
						}
						MainActivity.handler.sendMessage(msg);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
		
	}
	
	class DownloadThread4 implements Runnable{

		public DownloadThread4(String fileName){
			
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			msg = MainActivity.handler.obtainMessage();//Add this in order to avoid error"This message is already in use."
			if (HttpDownloader.checkIfUrlAvailable(XmlParse.upgradeXMLFileUrl)) {
				boolean isOk = XmlParse.getXmlContent();
				if (isOk) {				
					//msg.what = 0x04;
				    //MainActivity.handler.sendMessage(msg);
					String fileUrl = XmlParse.upgradeUrl + XmlParse.fileInfos.get(0).getFileName();
					//String fileUrl = "http://192.168.1.25:8888/Battle/" + XmlParse.fileInfos.get(0).getFileName();
					if (HttpDownloader.checkIfUrlAvailable(fileUrl)) {
						//System.out.println("FileUtils.SDCardPath2 " + FileUtils.SDCardPath2);
						HttpDownloader hd = new HttpDownloader();
						int result = hd.downFile(fileUrl, FileUtils.SystemPath2, XmlParse.fileInfos.get(0).getFileName());
						String resultMessage = null;
						if(result == -1){
							resultMessage = "download failed!";
						}
						else if(result == 0){
							resultMessage = "download success";
						}
						else if(result == 1){
							resultMessage = "duplicate download!";
						}
						System.out.println("resultMessage " + resultMessage);
					} else {				
						msg.what = 0x06;
					    MainActivity.handler.sendMessage(msg);
					}
				} else {
					msg.what = 0x05;
				    MainActivity.handler.sendMessage(msg);
				}
			} else {
				msg.what = 0x06;
			    MainActivity.handler.sendMessage(msg);
			}
			
		}
		
	}
	
}
