package com.fibocom.activity;

import java.lang.reflect.Method;

import com.fibocom.activity.R;
import com.fibocom.file.FileUtils;
import com.fibocom.model.XmlParse;
import com.fibocom.sdcard.CheckIfSDCardExits;
import com.fibocom.sdcard.UsbSdcardReceiver;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.widget.Toast;

public class MainActivity extends PreferenceActivity {

	//private static TextView checkExtSDCard, progressText;
	private static Preference extSDcardUpgrade, USBUpgrade, onlineUpgrade, systemRecovery;
	//private static LinearLayout progressbarlayout;
	//private static ProgressBar progressBar;
	private static Intent intent = new Intent();
	private static int networkType = ConnectivityManager.TYPE_BLUETOOTH;
	public static boolean isActivityAlive = false, onlineUpdateIdle = false;
	public static Context mContext;
	private static int check_result = 0;
	private static int check_result_available = 1, check_result_unavailable = 2, check_result_url_unavailable = 3;
	private static int isDownloading = 0;
	private static int type_download_none = 1, type_downloading = 2, type_download_complete = 3, type_download_app = 4;
	private static final String KEY_SYSTEM_RECOVERY = "system_recovery";
    private static final String KEY_SDCARD_UPDATE = "sdcard_update";
    private static final String KEY_USB_UPDATE = "usb_update";
    private static final String KEY_ONLINE_UPDATE = "online_update";
    private static long progress;
    private UsbSdcardReceiver mUsbSdcardReceiver; 
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.system_update);
        systemRecovery = (Preference)findPreference(KEY_SYSTEM_RECOVERY);
        extSDcardUpgrade = (Preference)findPreference(KEY_SDCARD_UPDATE);
        USBUpgrade = (Preference)findPreference(KEY_USB_UPDATE);
        onlineUpgrade = (Preference)findPreference(KEY_ONLINE_UPDATE);
        systemRecovery.setOnPreferenceClickListener(new SystemRecovery());
		if (ActivityManager.isUserAMonkey()) {
			systemRecovery.setEnabled(false);
		}
        extSDcardUpgrade.setOnPreferenceClickListener(new SDCardUpdate());
        USBUpgrade.setOnPreferenceClickListener(new USBUpdate());
        onlineUpgrade.setOnPreferenceClickListener(new OnlineUpdate());
        
        //progressText = (TextView)findViewById(R.id.progresstext);
        //progressbarlayout = (LinearLayout)findViewById(R.id.progressbarlayout);
        //progressBar = (ProgressBar)findViewById(R.id.downloadprogress);
        isActivityAlive = true;
        mContext = getApplicationContext();
    }
    
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		mUsbSdcardReceiver = new UsbSdcardReceiver(mContext);
		mUsbSdcardReceiver.registerReceiver();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		mUsbSdcardReceiver.unregisterReceiver();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		isActivityAlive = false;
		stopService(intent);
		
		ActivityManager mAm;
        mAm = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE); 
        Method method;
		try {
			method = Class.forName("android.app.ActivityManager").getMethod("forceStopPackage", String.class);
			method.invoke(mAm, "com.fibocom.activity");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//Check if inner SDCard has ota package.
		if (CheckIfSDCardExits.getSecondaryStoragePath(mContext)) {
			extSDcardUpgrade.setEnabled(false);
			extSDcardUpgrade.setSummary(R.string.sdcard_update_summary);
		} else {
			extSDcardUpgrade.setEnabled(true);
			extSDcardUpgrade.setSummary(R.string.sdcard_update_summary_2);
		}
		
		if (CheckIfSDCardExits.isUSBExist(mContext)) {
			USBUpgrade.setEnabled(false);
			USBUpgrade.setSummary(R.string.usb_update_summary);
		} else {
			USBUpgrade.setEnabled(true);
			USBUpgrade.setSummary(R.string.usb_update_summary_2);
		}
		
        if (checkNetworkConnectedState()) {
        	onlineUpgrade.setEnabled(true);
        	onlineUpgrade.setSummary(R.string.online_update_summary_2);
        	if (check_result == check_result_available) {
        		
        	} else if (check_result == check_result_unavailable) {
        		onlineUpgrade.setSummary(R.string.online_update_summary_4);
        	} else if (check_result == check_result_url_unavailable) {
        		onlineUpgrade.setSummary(R.string.online_update_summary_5);
        	} 
        	
        	if (networkType == ConnectivityManager.TYPE_WIFI) {
        		onlineUpgrade.setSummary(R.string.online_update_summary_2);
        	}
        	
        } else {
        	onlineUpgrade.setEnabled(false);
        	onlineUpgrade.setSummary(R.string.online_update_summary);
        }
        
        /*if (isActivityAlive == true && FileUtils.downloadedFileLength != 0 && isDownloading == type_download_complete) {
        	//progressbarlayout.setVisibility(View.VISIBLE);
        	//progressBar.setMax(Integer.parseInt(String.valueOf(FileUtils.fileLength)));
        	//progressBar.setProgress(Integer.parseInt(String.valueOf(FileUtils.downloadedFileLength)));
        	long progress = FileUtils.downloadedFileLength*100/FileUtils.fileLength;
			//progressText.setText(progress + "%");
        	onlineUpgrade.setEnabled(false);
        	onlineUpgrade.setSummary(R.string.online_update_summary_7);
        } else if(XmlParse.fileInfos != null) {
        	onlineUpgrade.setEnabled(true);
        	onlineUpgrade.setSummary(R.string.online_update_summary_6);
        } */
        
        if (isDownloading == type_downloading) {
        	systemRecovery.setEnabled(false);
        	extSDcardUpgrade.setEnabled(false);
        	onlineUpgrade.setEnabled(false);
        	onlineUpgrade.setSummary("Downloading:"+ progress + "%");
        }
        
        if (FileUtils.getUpgradeFileName(FileUtils.SystemPath2) != null && isDownloading != type_downloading) {
        	if(!FileUtils.sdcardUpdatePackage.equals(XmlParse.BuildVersion)) {
        		isDownloading = type_download_complete;
				onlineUpgrade.setEnabled(true);
				onlineUpgrade.setSummary(R.string.online_update_summary_3);				
        	}
        }        
        
	}
	
	private boolean checkNetworkConnectedState() {
    	ConnectivityManager mConnectivityManager = (ConnectivityManager)
    			getApplicationContext().getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);
    	NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
    	if (mNetworkInfo != null) {
    		if (mNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {    			
    			networkType = ConnectivityManager.TYPE_WIFI;
			return mNetworkInfo.isAvailable();
    		} else {
    			networkType = ConnectivityManager.TYPE_MOBILE;//Only support wifi environment
    		}    		
    	}
    	networkType = ConnectivityManager.TYPE_BLUETOOTH;
    	return false;
    }
    
    private class SystemRecovery implements OnPreferenceClickListener {

		@Override
		public boolean onPreferenceClick(Preference arg0) {
			// TODO Auto-generated method stub
			new AlertDialog.Builder(MainActivity.this).setTitle(R.string.system_prompt)
				.setMessage(R.string.system_recovery_prompt)
				.setPositiveButton(R.string.alert_dialog_yes, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						intent.putExtra("downloadtype", 1);
						intent.setClass(MainActivity.this, DownloadService.class);
						startService(intent);
					}
				}).setNegativeButton(R.string.alert_dialog_no, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						
					}
				}).show();
			return false;
		}
    	
    }
    
    private class SDCardUpdate implements OnPreferenceClickListener {

		public boolean onPreferenceClick(Preference arg0) {
			// TODO Auto-generated method stub
			/*if (networkType == ConnectivityManager.TYPE_MOBILE) {
			new AlertDialog.Builder(getApplicationContext()).setTitle(R.string.alert_dialog_title)
			.setMessage(R.string.alert_dialog_gprs)
			.setPositiveButton(R.string.alert_dialog_yes, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					// TODO Auto-generated method stub
					intent.putExtra("downloadtype", 1);
					intent.setClass(MainActivity.this, DownloadService.class);
					startService(intent);
					Toast.makeText(getApplicationContext(), "onlie upgrading...", Toast.LENGTH_SHORT).show();
				}
			}).setNegativeButton(R.string.alert_dialog_no, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					// TODO Auto-generated method stub
					
				}
			}).show();
		} else {
			intent.putExtra("downloadtype", 1);
			intent.setClass(MainActivity.this, DownloadService.class);
			startService(intent);
			Toast.makeText(getApplicationContext(), "onlie upgrading...", Toast.LENGTH_SHORT).show();
		}*/
		
		/*if (networkType == ConnectivityManager.TYPE_WIFI) {
			intent.putExtra("downloadtype", 1);
			intent.setClass(MainActivity.this, DownloadService.class);
			startService(intent);
			Toast.makeText(getApplicationContext(), "onlie upgrading...", Toast.LENGTH_SHORT).show();			
		} else {
			//Toast.makeText(getApplicationContext(), R.string.alert_dialog_gprs, Toast.LENGTH_SHORT).show();
			Toast.makeText(getApplicationContext(), "Please update under wifi environment!", Toast.LENGTH_SHORT).show();
		}*/
		
		new AlertDialog.Builder(MainActivity.this).setTitle(R.string.system_prompt)
			.setMessage(R.string.system_prompt_content)
			.setPositiveButton(R.string.alert_dialog_yes, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					// TODO Auto-generated method stub
					intent.putExtra("downloadtype", 2);
					intent.setClass(MainActivity.this, DownloadService.class);
					startService(intent);
				}
			}).setNegativeButton(R.string.alert_dialog_no, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					// TODO Auto-generated method stub
					
				}
			}).show();
			return false;
		}
    	
    }
    
    private class USBUpdate implements OnPreferenceClickListener {

		@Override
		public boolean onPreferenceClick(Preference arg0) {
			// TODO Auto-generated method stub
			new AlertDialog.Builder(MainActivity.this).setTitle(R.string.system_prompt)
			.setMessage(R.string.system_prompt_content)
			.setPositiveButton(R.string.alert_dialog_yes, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					// TODO Auto-generated method stub
					intent.putExtra("downloadtype", 3);
					intent.setClass(MainActivity.this, DownloadService.class);
					startService(intent);
				}
			}).setNegativeButton(R.string.alert_dialog_no, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					// TODO Auto-generated method stub
					
				}
			}).show();
			return false;
		}
    	
    }
    
    private class OnlineUpdate implements OnPreferenceClickListener {

		public boolean onPreferenceClick(Preference arg0) {
			// TODO Auto-generated method stub
			if (isDownloading == type_download_complete) {
				new AlertDialog.Builder(MainActivity.this).setTitle(R.string.system_prompt)
					.setMessage(R.string.system_prompt_content)
					.setPositiveButton(R.string.alert_dialog_yes, new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							// TODO Auto-generated method stub
							try {
								if (FileUtils.readZipFile(FileUtils.SystemPath2 + 
										FileUtils.getUpgradeFileName(FileUtils.SystemPath2))
										|| !FileUtils.sdcardUpdatePackage.equals(XmlParse.BuildVersion)) {
									new Thread(new Runnable() {
										
										@Override
										public void run() {
											// TODO Auto-generated method stub
											try {
												Thread.sleep(200);
												FileUtils.execCommand(FileUtils.SystemPath2, FileUtils.SystemPath2);
											} catch (InterruptedException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
										}
									}).start();
								} else {
									Toast.makeText(mContext, "The package is Illegal!", Toast.LENGTH_SHORT).show();
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}).setNegativeButton(R.string.alert_dialog_no, new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							// TODO Auto-generated method stub
							new AlertDialog.Builder(MainActivity.this).setTitle(R.string.system_prompt)
							.setMessage(R.string.system_prompt_delete_file)
							.setPositiveButton(R.string.alert_dialog_yes, new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									// TODO Auto-generated method stub
									FileUtils.deleteFile(FileUtils.SystemPath2 + FileUtils.sdcardUpdatePackage + ".zip");
									if (checkNetworkConnectedState()) {
							        	onlineUpgrade.setEnabled(true);
							        	onlineUpgrade.setSummary(R.string.online_update_summary_6);
							        	
							        } else {
							        	onlineUpgrade.setEnabled(false);
							        	onlineUpgrade.setSummary(R.string.online_update_summary);
							        }
									isDownloading = type_download_none;
									FileUtils.downloadedFileLength = 0;
								}
							}).setNegativeButton(R.string.alert_dialog_no, new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									// TODO Auto-generated method stub
									
								}
							}).show();
						}
					}).show();
				return false;
			} 
			
			if (networkType == ConnectivityManager.TYPE_WIFI) {			
				intent.putExtra("downloadtype", 4);
				intent.setClass(MainActivity.this, DownloadService.class);
				startService(intent);
			} else {				
				Toast.makeText(getApplicationContext(), "Please update under wifi environment!", Toast.LENGTH_SHORT).show();
			}
			
			/*intent.putExtra("downloadtype", 2);
			intent.setClass(MainActivity.this, DownloadService.class);
			startService(intent);
			Toast.makeText(getApplicationContext(), "CheckUpgrade", Toast.LENGTH_SHORT).show();*/
			return false;
		}
    	
    }        
        
    public static Handler handler = new Handler() {

    	@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
    		if (!Thread.currentThread().isInterrupted()) {
    			switch (msg.what) {
    			case 0x01:
    				isDownloading = type_downloading;
    				systemRecovery.setEnabled(false);
    				extSDcardUpgrade.setEnabled(false);
    				onlineUpgrade.setEnabled(false);
    				onlineUpgrade.setSummary("Downloading:"+ progress + "%");
    				//progressbarlayout.setVisibility(View.VISIBLE);
    				//progressBar.setMax(Integer.parseInt(String.valueOf(FileUtils.fileLength)));
    				//System.out.println("fileLength:" + FileUtils.fileLength);
    				break;
    			case 0x02:
    				//progressBar.setProgress(Integer.parseInt(String.valueOf(FileUtils.downloadedFileLength)));
    				progress = FileUtils.downloadedFileLength*100 / FileUtils.fileLength;
    				onlineUpgrade.setSummary("Downloading:"+ progress + "%");
    				//progressText.setText(progress + "%");
    				break;
    			case 0x03:        			
        			isDownloading = type_download_complete;
        			systemRecovery.setEnabled(true);
    				onlineUpgrade.setEnabled(true);
    				onlineUpgrade.setSummary(R.string.online_update_summary_3);
    				//progressText.setText(R.string.download_complete);
    				break;
    			case 0x04:
    				extSDcardUpgrade.setEnabled(true);
    				onlineUpgrade.setEnabled(false);
    				onlineUpdateIdle = true;
    				check_result = check_result_available;
    				break;
    			case 0x05:
    				onlineUpgrade.setEnabled(false);
				onlineUpdateIdle = false;
				check_result = check_result_unavailable;
				onlineUpgrade.setSummary(R.string.online_update_summary_4);
    				break;
    			case 0x06:    				
    				onlineUpgrade.setEnabled(false);
    				onlineUpdateIdle = false;
    				check_result = check_result_url_unavailable;
				onlineUpgrade.setSummary(R.string.online_update_summary_5);
    				break;
    			case 0x07:
    				extSDcardUpgrade.setEnabled(false);
    				extSDcardUpgrade.setSummary(R.string.nofileinextsdcard);
    				break;
    			case 0x08:
    				Toast.makeText(mContext, "Rebooting...", Toast.LENGTH_SHORT).show();
    				break;
    			case 0x09:
    				break;
    			case 0x0a:
    				Toast.makeText(mContext, "The machine will reboot in 5s", Toast.LENGTH_SHORT).show();
    				break;
    			case 0x0b:
    				Toast.makeText(mContext, "Duplicate download!", Toast.LENGTH_SHORT).show();
    				break;
    			case 0x0c:
    				extSDcardUpgrade.setEnabled(true);
					extSDcardUpgrade.setSummary(R.string.sdcard_update_summary_2);
    				break;
    			case 0x0d:
    				extSDcardUpgrade.setEnabled(false);
					extSDcardUpgrade.setSummary(R.string.sdcard_update_summary);
    				break;
    			case 0x0e:
					USBUpgrade.setEnabled(true);
		        	USBUpgrade.setSummary(R.string.usb_update_summary_2);
    				break;
    			case 0x0f:
    				USBUpgrade.setEnabled(false);
					USBUpgrade.setSummary(R.string.usb_update_summary);
    				break;
    			default:
    				break;
    			}
    		}			
		}
    	
    };
	
}
