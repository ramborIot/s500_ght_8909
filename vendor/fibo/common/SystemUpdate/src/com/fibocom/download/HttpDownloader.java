package com.fibocom.download;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.os.Message;

import com.fibocom.activity.MainActivity;
import com.fibocom.file.FileUtils;

public class HttpDownloader {

	private URL url = null;		

	public String download(String urlStr) {
		StringBuffer sb = new StringBuffer();
		String line = null;
		BufferedReader buffer = null;
		try {
			url = new URL(urlStr);
			HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
			buffer = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
			if (buffer == null) {
				return null;
			}
			while ((line = buffer.readLine()) != null) {
				sb.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (buffer != null)  {
					buffer.close();
				}				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
		return sb.toString();
	}

	public int downFile(String urlStr, String path, String fileName) {
		InputStream inputStream = null;
		try {
			FileUtils fileUtils = new FileUtils();
			
			if (fileUtils.isFileExist(fileName, path)) {
				return 1;
			} else {
				inputStream = getInputStreamFromUrl(urlStr);
				if (inputStream == null) {
					return -1;
				}
				//System.out.println("inputStream-->" + inputStream);
				File resultFile = fileUtils.write2SDFromInput(path, fileName, inputStream);
				if (resultFile == null) {
					return -1;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	public InputStream getInputStreamFromUrl(String urlStr)
			throws MalformedURLException, IOException {
		url = new URL(urlStr);
		HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
		FileUtils.fileLength = urlConn.getContentLength();
		Message msg = MainActivity.handler.obtainMessage();
		msg.what = 0x01;
	    MainActivity.handler.sendMessage(msg);
		InputStream inputStream = urlConn.getInputStream();
		return inputStream;
	}
	
	public static boolean checkIfUrlAvailable(String url) {
		boolean isAvailable = false;
		try {
			HttpURLConnection conn = (HttpURLConnection)new URL(url).openConnection();
			if (conn.getResponseCode() != 200) {
				isAvailable = false;
			} else {
				isAvailable = true;
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isAvailable;
	}
}
