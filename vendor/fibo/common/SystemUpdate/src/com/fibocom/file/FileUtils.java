package com.fibocom.file;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import com.fibocom.activity.MainActivity;
import com.fibocom.download.HttpDownloader;
import com.fibocom.model.XmlParse;

import android.os.Message;
import android.os.RecoverySystem;
import android.util.Log;
import android.widget.Toast;


public class FileUtils {

	public static String filePath = "/storage/emulated/0/recovery/";
	public static String SDCardPath1 = "/storage/emulated/0/";//"/storage/sdcard/";
	public static String SDCardPath3 = "/sdcard/";
	public static String SDCardPath2 = "/storage/sdcard1/";
	public static String SystemPath = "/cache/recovery/";
	public static String SystemPath2 = "/cache/";
	public static String USBPath = "/storage/usbotg/";
	public static String USBPath2 = "/usb-otg/";
	public static String mContent = "--update_package=/storage/emulated/0/";
	public static String mContent2 = "--update_package=/sdcard/";
	public static String commandPath = " > /cache/recovery/command";
	public static long downloadedFileLength, fileLength;
	public static File updateFile;
	public static String sdcardUpdatePackage;
	private static final String TAG = "SystemUpdate";
	public static String SBL1FILENAME = "sbl1.mbn";
	public static String SBL1FILEPATH = "/data/sbl1.mbn";
	public static String SBL1FILEPATH2 = "/dev/block/platform/7824900.sdhci/by-name/sbl1";
	public static String FIBOCOM = "OEM_IMAGE_VERSION_STRING=FIBOCOM";
	public static void enterRecoveryMode() {
    	//writeTxtToFile("boot-recovery", SystemPath, "command");
    	try {
    		RecoverySystem.installPackage(MainActivity.mContext, new File(SDCardPath1));
    		//Runtime.getRuntime().exec("adb reboot recovery");//user version can do it too.
    		//Runtime.getRuntime().exec("adb shell input keyevent 26;input keyevent 24");//power key and vol+ key.
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	public static void deleteFile(String filePath) {
		File file = new File(filePath);	
		file.delete();
	}
	
	public static void execCommand(String filePath, String updatePath) {
		try {
			//Runtime.getRuntime().exec("mkdir /cache/recovery");
			//FileUtils.newDirectory(path, content);
			RecoverySystem.installPackage(MainActivity.mContext, new File(updatePath + getUpgradeFileName(filePath)));
			//Runtime.getRuntime().exec("reboot recovery");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }	
	
	public static String getUpgradeFileName(String path) {
    	File file = new File(path);
    	File[] fileNames = file.listFiles(new FileNameSelector("zip"));
	if (fileNames == null || fileNames.length == 0) {
		return null;
	} else {
		int dot = fileNames[0].getName().lastIndexOf('.');		
		sdcardUpdatePackage = fileNames[0].getName().substring(0, dot);
		return fileNames[0].getName();
	}		
    }
    
	public static void writeTxtToFile(String content, String filePath, String fileName) {
    	makeFilePath(filePath, fileName);
    	String strFilePath = filePath + fileName;
    	String strContent = content + "\r\n";
    	File file = new File(strFilePath);	
		RandomAccessFile raf = null;
		try {		
			if (!file.exists()) {
	    		file.delete();
	    		file.getParentFile().mkdirs();
				file.createNewFile();
	    	}
	    	raf = new RandomAccessFile(file, "rwd");
	    	raf.setLength(file.length());
	    	raf.write(strContent.getBytes());
	    	raf.close();
    	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (raf != null) {
					raf.close();
				}			
			} catch (Exception e) {
				e.printStackTrace();
			}
		}	
    }
    
	public static File makeFilePath(String filePath, String fileName) {
    	File file = null;
    	makeRootDirectory(filePath);
    	file = new File(filePath + fileName);
    	if (!file.exists()) {
    		try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
		return file;
    	
    }
    
	public static void makeRootDirectory(String filePath) {
    	File file = null;
    	file = new File(filePath);
    	if (!file.exists()) {
    		file.mkdir();
    	}
    }
	
	public boolean isFileExist(String fileName, String path){
		File file = new File(SDCardPath1 + fileName);
		return file.exists();
	}
	
	public File creatFileInSDCard(String fileName, String dir) throws IOException {
		File file = new File(dir + fileName);
		//System.out.println("file" + file);
		file.createNewFile();		
		return file;
	}

	public File write2SDFromInput(String path, String fileName, InputStream input){
		File file = null;
		OutputStream output = null;
		try{
			//makeRootDirectory(path);
			file = creatFileInSDCard(fileName, path);
			output = new FileOutputStream(file);
			if (output == null) {
				return null;
			}
			byte buffer [] = new byte[4 * 1024];
			//System.out.println("output-->" + output);
			int temp;
			while((temp = input.read(buffer)) != -1){
				output.write(buffer, 0, temp);
				Message msg = MainActivity.handler.obtainMessage();
				downloadedFileLength += temp;
				if (downloadedFileLength < fileLength) {					
					msg.what = 0x02;
				    MainActivity.handler.sendMessage(msg);
				} else {
					msg.what = 0x03;
				    MainActivity.handler.sendMessage(msg);
				}
				
			}
			output.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			try{
				if (output != null) {
					output.close();
				}				
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return file;
	}
	
	/**
	 * Java无需解压直接读取Zip文件和文件内容
	 * @param file
	 * @throws Exception
	 */
	public static boolean readZipFile(String file) throws Exception {
		File mFile = new File(file);
		if (!mFile.exists()) {
			return false;
		}
        ZipFile zf = new ZipFile(file);
        InputStream in = new BufferedInputStream(new FileInputStream(file));
        ZipInputStream zin = new ZipInputStream(in);
        ZipEntry ze;
        while ((ze = zin.getNextEntry()) != null) {
            if (ze.isDirectory()) {
            	//Do nothing
            } else {
            	Log.e(TAG,"file - " + ze.getName() + " : " + ze.getSize() + " bytes");
                if (ze.getName().equals(SBL1FILENAME)) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(zf.getInputStream(ze)));
                    String line;
                    while ((line = br.readLine()) != null) {
                    	Log.e(TAG, line);
                    	if (line.contains(FIBOCOM)) {
                    		br.close();
                    		zin.closeEntry();
                    		return true;
                    	}
                    }
                    br.close();
                }
                System.out.println();
            }
        }
        zin.closeEntry();
        return false;
    }
	
	public static boolean readFile(String path) {		
		File mFile = new File(path);
		Log.e(TAG, "######################################");
		Log.e(TAG, "######################################");
		Log.e(TAG, "######################################");
		if (mFile.isDirectory()) {
			Log.e(TAG, "File not found!!!!");
			return false;
		}
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(mFile)));
			String line;
			while ((line = br.readLine()) != null) {
				//Log.e(TAG, "line:" + line);
				if (line.contains(FIBOCOM)) {
					br.close();
					Log.e(TAG, "line:" + line);
					return true;
				}
			}
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		return false;
	}
	
}
