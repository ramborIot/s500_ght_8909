package com.fibocom.file;

import java.io.File;
import java.io.FilenameFilter;

public class FileNameSelector implements FilenameFilter {

	private String extension = ".";
	
	public FileNameSelector(String fileExtensionNoDot) {  
		 extension += fileExtensionNoDot;  
	}
	
	@Override
	public boolean accept(File arg0, String name) {
		// TODO Auto-generated method stub
		return name.endsWith(extension);
	}

}
