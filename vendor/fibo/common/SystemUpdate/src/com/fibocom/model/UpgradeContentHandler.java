package com.fibocom.model;

import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class UpgradeContentHandler extends DefaultHandler {

	private List<UpgradeFileInfo> infos = null;
	private UpgradeFileInfo fileInfo = null;
	private String tagName = null;
	@Override
	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub
		super.startDocument();
	}

	@Override
	public void endDocument() throws SAXException {
		super.endDocument();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		this.tagName = localName;
		if(tagName.equals("resource")){
			fileInfo = new UpgradeFileInfo();
			System.out.println("resource");
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if(qName.equals("resource")){
			infos.add(fileInfo);
			System.out.println("end resource");
		}
		tagName = "";
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		String temp = new String(ch, start, length);
		if(tagName.equals("id")){
			fileInfo.setId(temp);
			System.out.println("id " + temp);
		}
		else if(tagName.equals("name")){
			fileInfo.setFileName(temp);
			System.out.println("name " + temp);
		}
		else if(tagName.equals("size")){
			fileInfo.setFileSize(temp);
			System.out.println("size " + temp);
		}
		else if(tagName.equals("version")){
			fileInfo.setfileVersion(temp);
			System.out.println("version " + temp);
		}
	}

	public List<UpgradeFileInfo> getInfos() {
		return infos;
	}

	public void setInfos(List<UpgradeFileInfo> infos) {
		this.infos = infos;
	}

	public UpgradeContentHandler(List<UpgradeFileInfo> infos) {
		super();
		this.infos = infos;
	}

	
}
