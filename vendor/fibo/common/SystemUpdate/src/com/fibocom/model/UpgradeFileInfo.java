package com.fibocom.model;

import java.io.Serializable;

public class UpgradeFileInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String fileName;
	private String fileSize;
	private String fileVersion;
	public UpgradeFileInfo() {
		super();
	}
	public UpgradeFileInfo(String id, String fileName, String fileSize, String fileVersion) {
		super();
		this.id = id;
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.fileVersion = fileVersion;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public String getFileVersion() {
		return fileVersion;
	}
	public void setfileVersion(String fileVersion) {
		this.fileVersion = fileVersion;
	}
	@Override
	public String toString() {
		return "FileInfo [id=" + id + ", fileName=" + fileName + ", fileSize="
				+ fileSize + ", fileVersion=" + fileVersion + "]";
	}
	
}
