package com.fibocom.model;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

import com.fibocom.activity.MainActivity;
import com.fibocom.download.HttpDownloader;
import android.os.SystemProperties;

public class XmlParse {

	public static List<UpgradeFileInfo> fileInfos = null;
	public static String upgradeUrl = "http://192.168.1.64/Qualcomm/";
	public static String upgradeXMLFileUrl = "http://192.168.1.64/Qualcomm/UpgradeFileInfo.xml";
	public static String systemupdateXMLFileUrl = "http://192.168.1.64/SystemUpdate/UpgradeFileInfo.xml";
	//public static String upgradeXMLFileUrl = "http://192.168.1.25:8888/Battle/UpgradeFileInfo.xml";
	
	/** Value used for when a build property is unknown. */
    public static final String UNKNOWN = "unknown";
    /** A build ID string meant for displaying to the user */
	public static String BuildVersion = getString("ro.build.display.id");
	
	private static String getString(String property) {
        return SystemProperties.get(property, UNKNOWN);
    }
	
	public static String downloadXML(String xmlStr){
		HttpDownloader hd = new HttpDownloader();
		String resultStr = hd.download(xmlStr);
		return resultStr;
	}
	
	public static boolean getXmlContent() {
		String xmlStr = downloadXML(upgradeXMLFileUrl);
		/*for(int i=0; i < 10; i++) {
			xmlStr = downloadXML(upgradeXMLFileUrl);
		}*/
		System.out.println("sb.toString():" + xmlStr);
		fileInfos = parse(xmlStr);
		if (fileInfos.size() > 0) {
			if (!fileInfos.get(0).getFileVersion().equals(BuildVersion)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean getXmlContentApp() {
		String xmlStr = downloadXML(systemupdateXMLFileUrl);
		try {
			PackageInfo mInfo = MainActivity.mContext.getPackageManager()
					.getPackageInfo(MainActivity.mContext.getPackageName(), 0);
			BuildVersion = mInfo.versionCode + "";
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("sb.toString():" + xmlStr);
		System.out.println("BuildVersion:" + BuildVersion);
		fileInfos = parse(xmlStr);
		if (fileInfos != null) {
			if (!fileInfos.get(0).getFileVersion().equals(BuildVersion)) {
				return true;
			}
		}
		return false;
	}
	
	public static List<UpgradeFileInfo> parse(String xmlStr){
    	SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
    	List<UpgradeFileInfo> infos = new ArrayList<UpgradeFileInfo>();
    	try{
    		XMLReader xmlReader = saxParserFactory.newSAXParser().getXMLReader();
    		UpgradeContentHandler upgradeContentHandler = new UpgradeContentHandler(infos);
    		xmlReader.setContentHandler(upgradeContentHandler);
    		xmlReader.parse(new InputSource(new StringReader(xmlStr)));
    		for (Iterator iterator = infos.iterator(); iterator.hasNext();) {
    			UpgradeFileInfo upgradeInfo = (UpgradeFileInfo) iterator.next();
				//System.out.println("upgradeInfo" + upgradeInfo.getFileName());
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	//System.out.println("infos:" + infos.get(0).getFileName());
		return infos;    	
    }
}
