package com.fibocom.sdcard;

import com.fibocom.activity.MainActivity;
import com.fibocom.file.FileUtils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.widget.Toast;

public class UsbSdcardReceiver extends BroadcastReceiver{

	public IntentFilter filter = new IntentFilter();
	public Context uContext;
	public Message msg = new Message();
	
	public UsbSdcardReceiver(Context context) {
		uContext = context;
		filter.addAction(Intent.ACTION_MEDIA_CHECKING);
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_EJECT);
        filter.addAction(Intent.ACTION_MEDIA_REMOVED);
        filter.addDataScheme("file");// 必须要有此行，否则无法收到广播
	}
	
	public Intent registerReceiver() {
        return uContext.registerReceiver(this, filter);
	}

	public void unregisterReceiver() {
		uContext.unregisterReceiver(this);
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		msg = MainActivity.handler.obtainMessage();//Add this in order to avoid error"This message is already in use."
		if (intent.getAction().equals(Intent.ACTION_MEDIA_MOUNTED)
                || intent.getAction().equals(Intent.ACTION_MEDIA_CHECKING)) {
			if (FileUtils.USBPath.contains(intent.getData().getPath())) {
				msg.what = 0x0e;
			} else {
				msg.what = 0x0c;
			}
		} else {
			if (FileUtils.USBPath.contains(intent.getData().getPath())) {
				msg.what = 0x0f;
			} else {
				msg.what = 0x0d;
			}
		}
		MainActivity.handler.sendMessage(msg);
	}

}
