package com.fibocom.sdcard;

import com.fibocom.file.FileUtils;

import android.content.Context;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;

public class CheckIfSDCardExits {

	public static boolean isSDCardExit() {    	
		return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) || 
				Environment.MEDIA_MOUNTED_READ_ONLY.equals(Environment.getExternalStorageState());
    }
	
    /*public static boolean isSDCardExit(StorageManager mStorageManager) {
		final StorageVolume[] storageVolumes = mStorageManager.getVolumeList();
		// index 1 is sdcard
        final String state = mStorageManager.getVolumeState(storageVolumes[1].getPath());
        if (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
	}*/
	
	public static boolean getSecondaryStoragePath(Context context) {
        /*try {
            StorageManager sm = (StorageManager) getSystemService(STORAGE_SERVICE);
            Method getVolumePathsMethod = StorageManager.class.getMethod("getVolumePaths", null);
            String[] paths = (String[]) getVolumePathsMethod.invoke(sm, null);
            // second element in paths[] is secondary storage path
            System.out.println("paths[0]" + paths[0]);
            System.out.println("paths[1]" + paths[1]);
            return paths[1];
        } catch (Exception e) {
            System.out.println("getSecondaryStoragePath() failed");
        }
        System.out.println("paths[1]:null");*/
    	StorageManager storageManager = StorageManager.from(context);
    	final StorageVolume[] storageVolumes = storageManager.getVolumeList();
        // index 1 is sdcard
        final String state = storageManager.getVolumeState(storageVolumes[1].getPath());
        if (!(Environment.MEDIA_MOUNTED.equals(state) ||  Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))) {
        	return true;
        }
        return false;
    }
	
	public static boolean isUSBExist(Context context) {
		StorageManager storageManager = StorageManager.from(context);
		final String state = storageManager.getVolumeState("/storage/usbotg");
		if (!(Environment.MEDIA_MOUNTED.equals(state) ||  Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))) {
        	return true;
        }
        return false;
	}
}
