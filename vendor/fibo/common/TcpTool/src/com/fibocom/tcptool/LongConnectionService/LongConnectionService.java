package com.fibocom.tcptool.LongConnectionService;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.net.UnknownHostException;
import java.lang.InterruptedException;
import java.util.Arrays;

import android.os.Bundle;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class LongConnectionService extends Service {
	private static final String TAG = "LongConnectionService";
    private static final String HEARTBEAT_KEYWORD = "HBKW";
	public static final String MESSAGE_ACTION = "com.fibocom.message_ACTION";
	public static final String HEARTBEAT_ACTION = "com.fibocom.heartbeat_ACTION";
    
	private static final long HEARTBEAT_RATE = 1 * 1000;
    private static final int RUNNING_MAX = 3;
	private int SEND_SIZE = 0;
	private String HOST = "119.29.142.102";
	private int PORT = 7979;
    private boolean INIT_OK = false;
    private boolean RUNNING_STATUS = true;
    private int RUNNING_CNT = 0;

	private LocalBroadcastManager mLocalBroadcastManager = null;
	private WeakReference<Socket> mSocket = null;
	private ReadThread mReadThread = null;

	private ILongConnectionService.Stub iLongConnectionService = new ILongConnectionService.Stub() {
		@Override
		public void setServerParams(String ip, int port) throws RemoteException {
            HOST = ip;
            PORT = port;
            INIT_OK = true;
            setServiceStatus(true);
        }
        
		@Override
		public void setServiceStatus(boolean status) throws RemoteException {
            if (status) {
				new InitSocketThread().start();
            } else {
                if (mReadThread != null) {
                    mReadThread.release();
                }
                releaseLastSocket(mSocket);
                INIT_OK = false;
            }
            RUNNING_CNT = 0;
            RUNNING_STATUS = status;
            Log.i(TAG, "RUNNING_STATUS = " + (status ? "true" : "false"));
        }
        
		@Override
		public boolean sendMessage(String message) throws RemoteException {
            SEND_SIZE = message.length();
			return sendMsg(message);
		}
        
		@Override
		public boolean sendMessageInBytes(byte[] message, int size) throws RemoteException {
            SEND_SIZE = size;
            return sendMsgInBytes(message, size);
		}
	};

	@Override
	public IBinder onBind(Intent arg0) {
		return (IBinder) iLongConnectionService;
	}

	@Override
	public void onCreate() {
		super.onCreate();
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
	}


	private Handler mHandler = new Handler();
	private Runnable heartBeatRunnable = new Runnable() {
		@Override
		public void run() {
            if (RUNNING_STATUS) {
                boolean isSuccess = sendMsg(HEARTBEAT_KEYWORD);
                RUNNING_CNT = isSuccess ? 0 : RUNNING_CNT;
                if (!isSuccess) {
                    RUNNING_CNT = (RUNNING_CNT < RUNNING_MAX) ? (RUNNING_CNT + 1) : RUNNING_MAX;
                    if (RUNNING_CNT >= RUNNING_MAX) {
                        RUNNING_CNT = 0;
                        mHandler.removeCallbacks(heartBeatRunnable);
                        if (mReadThread != null) {
                            mReadThread.release();
                        }
                        releaseLastSocket(mSocket);
                        new InitSocketThread().start();
                        Log.i(TAG, "===================================>>> heartbeat error, reset now!");
                    }
				}
                mHandler.postDelayed(this, HEARTBEAT_RATE);
            }
		}
	};

	public boolean sendMsg(String msg) {
		if (null == mSocket || null == mSocket.get()) {
			return false;
		}
		Socket soc = mSocket.get();
		try {
			if (!soc.isClosed() && !soc.isOutputShutdown()) {
				OutputStream os = soc.getOutputStream();
				String message = msg + "\r\n";
				os.write(message.getBytes());
				os.flush();
			} else {
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
    
	public boolean sendMsgInBytes(byte[] msg, int size) {
		if (null == mSocket || null == mSocket.get()) {
			return false;
		}
		Socket soc = mSocket.get();
		try {
			if (!soc.isClosed() && !soc.isOutputShutdown()) {
				OutputStream os = soc.getOutputStream();
                DataOutputStream dos = new DataOutputStream(os);
                dos.write(msg, 0, size);
				os.flush();
			} else {
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private void initSocket() {
		try {
            Log.i(TAG, "Connecting HOST: " + HOST + ", PORT: " + PORT);
			Socket so = new Socket(HOST, PORT);
            if (so != null) {
                so.setTcpNoDelay(true);
                so.setReuseAddress(true);
                so.setSoLinger(true, 60);
                so.setSoTimeout(15000);
                so.setSendBufferSize(10240);
                so.setReceiveBufferSize(10240);
                mSocket = new WeakReference<Socket>(so);
                mReadThread = new ReadThread(so);
                mReadThread.start();
                mHandler.postDelayed(heartBeatRunnable, HEARTBEAT_RATE); 
            }
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        Log.i(TAG, "initSocket OK, mSocket = " + mSocket);
	}

    WeakReference<Socket> mmSocket = null;
	private void releaseLastSocket(WeakReference<Socket> mSocket) {
        mmSocket = mSocket;
        new Thread() {  
            @Override  
            public void run() {
                try {
                    if (null != mmSocket) {
                        Socket sk = mmSocket.get();
                        if (sk != null && !sk.isClosed()) {
                            sk.close();
                        }
                        sk = null;
                        mmSocket = null;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }  
        }.start();
        mSocket = null;
	}

	class InitSocketThread extends Thread {
		@Override
		public void run() {
			super.run();
            do {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace(); 
                }
            } while (!INIT_OK);
			initSocket();
		}
	}

	class ReadThread extends Thread {
		private WeakReference<Socket> mWeakSocket;
		private boolean isStart = true;

		public ReadThread(Socket socket) {
			mWeakSocket = new WeakReference<Socket>(socket);
		}

		public void release() {
            isStart = false;
			releaseLastSocket(mWeakSocket);
            Log.i(TAG, "ReadThread.release mWeakSocket: " + mWeakSocket + ", mSocket: " + mSocket);
		}
        
        public int indexOfArrayGet(byte[] arr, byte val) {
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] == val/* && arr[i+1] != val*/) {  // for: 7e 7e-xx-xx-xx ..., but: ... 7e 7e xx-xx-xx-xx ...
                    return i;
                }
            }
            return -1;
        }

		@Override
		public void run() {
			super.run();
			Socket socket = mWeakSocket.get();
			if (null != socket) {
				try {
					InputStream is = socket.getInputStream();
					byte[] buffer = new byte[4096];
					int length = 0;
                    int leftPos = 0;
                    int message_length = 0;
                    byte[] message_buffer = new byte[10240];
					while (null != socket && !socket.isClosed() && !socket.isInputShutdown()
							&& isStart && ((length = is.read(buffer)) != -1)) {
						if (length > 0) {
                            if (length != SEND_SIZE && length != 6) {
                                Log.i("csuntnt", "<SERVICE>: recv msg_len = " + length + ", buf[0] = " + buffer[0] + ", sendBytes = " + SEND_SIZE);
                            }
							String message = new String(Arrays.copyOf(buffer, length)).trim(); 
							if (message.equals(HEARTBEAT_KEYWORD)) {
								//Intent intent = new Intent(HEARTBEAT_ACTION);
								//mLocalBroadcastManager.sendBroadcast(intent);
							} else {
                                if (length > 6 && message.contains(HEARTBEAT_KEYWORD)) {
                                    int i = 0;
                                    message_length = leftPos;
                                    for (i = 0; i < length ; i++) {
                                        if (i < length - 5 && buffer[i] == (byte)0x48 && buffer[i+1] == (byte)0x42 && buffer[i+2] == (byte)0x4B && 
                                            buffer[i+3] == (byte)0x57 && buffer[i+4] == (byte)0x0D && buffer[i+5] == (byte)0x0A) {
                                            i += 5;
                                        } else {
                                            message_buffer[message_length/* + leftPos*/] = buffer[i];
                                            message_length++;
                                        }
                                    }
                                } else {
                                    if (leftPos >= 0 && length > 0) {
                                        System.arraycopy(buffer, 0, message_buffer, leftPos, length);
                                        message_length = leftPos + length;
                                    }
                                }
                                leftPos = message_length;

                                if (message_length > 0) {
                                    int startPos = indexOfArrayGet(message_buffer, (byte)0x7E);
                                    if (startPos < 0) {
                                        leftPos = 0;
                                        continue;
                                    } else if (startPos > 0) {
                                        if (message_length > startPos) {
                                            for (int m = 0; m < message_length - startPos; m++) {
                                                message_buffer[m] = message_buffer[m+startPos];
                                            }
                                            message_length = message_length - startPos;
                                            leftPos = message_length;
                                        }
                                    }
                                }

                                int packageCnt = message_length / SEND_SIZE;
                                if (message_length > 0 && packageCnt > 0) {
                                    int lastByteIndex = packageCnt * SEND_SIZE - 1;
                                    if ((message_buffer[0] == (byte)0x7E) && (message_buffer[lastByteIndex] == (byte)0x7E)) {
                                        Intent intent = new Intent(MESSAGE_ACTION);
                                        Bundle bundle = new Bundle();
                                        bundle.putByteArray("msgbuf", message_buffer);
                                        bundle.putInt("msglen", packageCnt * SEND_SIZE);
                                        intent.putExtras(bundle);
                                        mLocalBroadcastManager.sendBroadcast(intent);

                                        leftPos = message_length % SEND_SIZE;
                                        message_length = leftPos;
                                        for (int m = 0; m < leftPos; m++) {
                                            message_buffer[m] = message_buffer[m+packageCnt * SEND_SIZE];
                                        }
                                    } else {
                                        message_length = leftPos = 0;
                                    }
                                }
							}
						}
                        socket = mWeakSocket.get();
                        if (null != socket) {
                            is = socket.getInputStream();
                        }
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
