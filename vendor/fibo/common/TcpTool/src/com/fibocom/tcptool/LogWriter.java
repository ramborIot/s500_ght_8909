package com.fibocom.tcptool;

import android.annotation.SuppressLint;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressLint("SimpleDateFormat")
public class LogWriter {
	
	private static LogWriter mLogFile;

	private static String mPath;
	
	private static Writer mWriter;
	
	private static SimpleDateFormat df;
	
	@SuppressWarnings("static-access")
	private LogWriter(String file_path) {
		this.mPath = file_path;
		this.mWriter = null;
	}

	@SuppressWarnings("unused")
	public static LogWriter open(String file_path) throws IOException {
		if (mLogFile == null) {
			mLogFile = new LogWriter(file_path);
		}
		File mFile = new File(mPath);
		mWriter = new BufferedWriter(new FileWriter(mPath), 2048);
		df = new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss:SSS]: "); //hh  12Сʱ
		
		return mLogFile;
	}
	
	public void close() throws IOException {
		if (mWriter != null) {
			mWriter.close();
		}
	}
	
	public void print(String log) throws IOException {
		if (mWriter != null) {
			mWriter.write(df.format(new Date()));
			mWriter.write(log);
			mWriter.write("\r\n");
			mWriter.flush();
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void print(Class cls, String log) throws IOException {
		if (mWriter != null) {
			mWriter.write(df.format(new Date()));
			mWriter.write(cls.getSimpleName() + " ");
			mWriter.write(log);
			mWriter.write("\r\n");
			mWriter.flush();
		}
	}
}

