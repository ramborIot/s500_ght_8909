package com.fibocom.tcptool;

import java.lang.ref.WeakReference;
import com.fibocom.tcptool.LongConnectionService.LongConnectionService;
import com.fibocom.tcptool.LongConnectionService.ILongConnectionService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.RemoteException;
import android.support.v4.content.LocalBroadcastManager;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fibocom.tcptool.R;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StatFs;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;  
import android.net.wifi.WifiManager;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.ToggleButton;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import com.fibocom.tcptool.TestCaseConfigs;
import com.fibocom.tcptool.CustomDialog;
import com.fibocom.tcptool.CustomAdapter;
import android.telephony.TelephonyManager;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import java.lang.reflect.InvocationTargetException;

@SuppressLint("DefaultLocale")
public class MainActivity extends Activity implements OnClickListener {
	final private String TAG = "TCPtool"; 
	final private int TEST_CASE_MAX = 10;
	final private int TEST_FOR_AVGCNT = 20;
	final private int LLT_SENDTIME_MAX = 1000;
	final private int RECV_TIME_MAX = 3 * 1000;
	final private int SOCKET_RECWAIT_TIMEOUT = 15 * 1000;
	final private int SOCKET_CONNECT_TIMEOUT = 20 * 1000;
	final private int DATALINK_SETUP_TIMEOUT = 120 * 1000;
    final private int SST_TIME_INTERVAL_MIN = 120 * 1000;
	final private String SPLIT_LINE = "==========================================================================================";
	
	private LogWriter mLogWriter = null;
	
	private TelephonyManager telephoneManager = null;
	private String stringOfsignalStrengths = null;
	
	private Context mContext = null;
    private ListView lvTestCase = null;
    private TestCaseAdapter testCaseAdapter = null;
    
    private int eProtocolType = 0;
    private int eSendBytes = 5;
    private int eItemContent = 1;
    private int eNetType = 1;
    private int eIsIPAddress = 0;
    
	private Button btnCreate = null;
	private Button btnExport = null;
	private Button btnScreenshot = null;
	private Button btnExit = null;
	
	private boolean useWifiAsDefaultNetwork = true;
	private int item = 0;
    private String host = null;
    private String ghost = null;
    private int port = 0;
    private int cycles = 0;
    private int times = 0;
    private int interval = 0;
    private int sendBytes = 0;
    private int isIPAddress = 0;
    private int readIndex = 0;
    private Socket socket = null;
    private DatagramSocket udp_socket = null;
    private DatagramPacket udp_package = null;
    private DatagramPacket udp_recvpackage = null;
    private InetAddress udp_socAddress = null;
    private DataOutputStream dos = null;
    private DataInputStream dis = null;
    private byte[] content = null;
    private Thread recvThread = null;
    private Thread udpSendThread = null;
    private Thread udpRecvThread = null;
    private long[] sendPackageTime = null;
    private int llt_li = 0;
    private volatile boolean recvThreadExit = false;

    private boolean isLongLongTesting = false;
    private boolean useTestCount = true;
    private long startTimeInSeconds = 0;
    private long runningTimeInSeconds = 0;
    private int recvOntimeCnt = 0;
    private String testStartTime = null;		//测试起始时间
    private String testEndTime = null;			//测试结束时间
    private int sendSuccessCnt = 0;				//发送成功次数
    private int recvSuccessCnt = 0;				//接收成功次数    
    private int datalinkSetupCnt = 0;			//建立数据连接次数
    private int datalinkDestroyCnt = 0;			//断开数据连接次数
    private int serverConnectCnt = 0;			//连接服务器次数
    private int serverDisconnectCnt = 0;		//断开服务器次数
    private int reconnectCount = 0;				//自动重连次数
    private int datalinkSetupTimeMin = 0;		//建立数据连接最小耗时
    private int datalinkSetupTimeMax = 0;		//建立数据连接最大耗时
    private int datalinkSetupTimeTotal = 0;		//建立数据连接总耗时
    private int datalinkDestroyTimeMin = 0;		//断开数据连接最小耗时
    private int datalinkDestroyTimeMax = 0;		//断开数据连接最大耗时
    private int datalinkDestroyTimeTotal = 0;	//断开数据连接总耗时
    private int serverConnectTimeMin = 0;		//连接服务器最小耗时
    private int serverConnectTimeMax = 0;		//连接服务器最大耗时
    private int serverConnectTimeTotal = 0;		//连接服务器总耗时
    private int serverDisconnectTimeMin = 0;	//断开服务器最小耗时
    private int serverDisconnectTimeMax = 0;	//断开服务器最大耗时
    private int serverDisconnectTimeTotal = 0;	//断开服务器总耗时
    private int sendToRecvTimeMin = 0;			//发送到接收最小耗时
    private int sendToRecvTimeMax = 0;			//发送到接收最大耗时
    private int sendToRecvTimeTotal = 0;		//发送到接收总耗时
    
    private int datalinkSetupSuccessCnt = 0;
    private int datalinkSetupTotalCnt = 0;
    private int connectServerSuccessCnt = 0;
    private int connectServerTotalCnt = 0;
    private int sendToRecvSuccessCnt = 0;
    private int sendToRecvTotalCnt = 0;
    private int disconnectServerSuccessCnt = 0;
    private int disconnectServerTotalCnt = 0;
    private int datalinkDestroySuccessCnt = 0;
    private int datalinkDestroyTotalCnt = 0;
    private int testCaseSuccessCnt = 0;
    private int testCaseTotalCnt = 0;
    private int caseidx = 0;

	private Intent mServiceIntent = null;
	private MessageReciver mReciver = null;
	private IntentFilter mIntentFilter = null;
	private LocalBroadcastManager mLocalBroadcastManager = null;
	private ILongConnectionService iLongConnectionService = null;

	private ServiceConnection serviceConnection = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName name) {
			iLongConnectionService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			iLongConnectionService = ILongConnectionService.Stub.asInterface(service);
		}
	};

    class MessageReciver extends BroadcastReceiver {
        String LLT_LOG = null;
        int lastLeftBytes = 0;
        int readSize = 0;
        byte[] readBuffer = new byte[10240];
        int recvLeft = 0;
        byte[] recvBytes = new byte[4096];

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ((isLongLongTesting == true) && action.equals(LongConnectionService.MESSAGE_ACTION)) {
                Bundle bundle = intent.getExtras();
                readBuffer = bundle.getByteArray("msgbuf");
                readSize = bundle.getInt("msglen");
                if (readSize != sendBytes) {
                    Log.i("csuntnt", "recv msg_len = " + readSize + ", buf[0] = " + readBuffer[0] + ", sendBytes = " + sendBytes);
                }
                if (readSize > 0) {
                    readIndex++;
                    int recvPackage = (lastLeftBytes + readSize) / sendBytes;
                    if (recvPackage > 0) {
                        long rt = System.currentTimeMillis();
                        int pkg = 0;
                        int srcpos = 0;
                        do {
                            if (sendBytes > recvLeft && srcpos < readSize && srcpos >= 0 && recvLeft >= 0) {
                                System.arraycopy(readBuffer, srcpos, recvBytes, recvLeft, sendBytes - recvLeft);
                                srcpos += (sendBytes - recvLeft);
                            }

                            int idx = getIndexFromRecvBytes(recvBytes);
                            int srt = 0;
                            if (idx >= 0 && idx < LLT_SENDTIME_MAX) {
                                srt = (int)(rt - sendPackageTime[idx % LLT_SENDTIME_MAX]);
                                if (recvSuccessCnt == 0) {
                                    sendToRecvTimeMin = sendToRecvTimeMax = srt;
                                } else {
                                    sendToRecvTimeMax = (srt > sendToRecvTimeMax) ? srt : sendToRecvTimeMax;
                                    sendToRecvTimeMin = (srt < sendToRecvTimeMin) ? srt : sendToRecvTimeMin;
                                }
                                sendToRecvTimeTotal += srt;
                                recvSuccessCnt++;
                                if (srt <= RECV_TIME_MAX) {recvOntimeCnt++;}
                            }
                            recvLeft = 0;
                            pkg++;
                            recvPackage--;
                            testCaseSuccessCnt = recvSuccessCnt;
                            if (srt > 0) {
                                LLT_LOG = String.format("[LLT_%08d] SRT: %dms", recvSuccessCnt, srt);
                            }/* else {
                                LLT_LOG = String.format("[LLT_%08d] SRT: TMOUT", recvSuccessCnt);
                            }*/
                            logFileWrite(LLT_LOG, true);
                            if (recvSuccessCnt > 0 && (recvSuccessCnt % TEST_FOR_AVGCNT == 0)) {
                                llt_AvgRecordWrite_new(0);
                            }
                        } while (recvPackage > 0);
                    }
                    sendMsgForRefreshUI(0, sendSuccessCnt);
                    int tmpLastLeft = lastLeftBytes;
                    sendToRecvSuccessCnt += (lastLeftBytes + readSize) / sendBytes;
                    sendToRecvSuccessCnt = testCaseSuccessCnt; // some idx was wrong!!!
                    lastLeftBytes = (lastLeftBytes + readSize) % sendBytes;
                    recvLeft = lastLeftBytes;
                    if (recvLeft > 0) {
                        int srcPos = 0, dstPos = 0, cpSize = 0;
                        if (readSize + tmpLastLeft < sendBytes) {
                            srcPos = 0;
                            dstPos = tmpLastLeft;
                            cpSize = readSize;
                        } else {
                            srcPos = readSize - recvLeft;
                            dstPos = 0;
                            cpSize = recvLeft;
                        }
                        if (srcPos >= 0 && dstPos >= 0 && cpSize > 0) {
                            System.arraycopy(readBuffer, srcPos, recvBytes, dstPos, cpSize);
                        }
                    }
                }
			}
		};
	}

	private void onLongConnectionServiceStart() {
		mReciver = new MessageReciver();
		mLocalBroadcastManager.registerReceiver(mReciver, mIntentFilter);
		bindService(mServiceIntent, serviceConnection, BIND_AUTO_CREATE);
	}

	private void onLongConnectionServiceStop() {
		unbindService(serviceConnection);
		mLocalBroadcastManager.unregisterReceiver(mReciver);
		mReciver = null;
	}

    private boolean onLongConnectionServiceMessageSend(byte[] content, int size) {
        boolean isSend = false;
        if (iLongConnectionService != null) {
            try {
                isSend = iLongConnectionService.sendMessageInBytes(content, size);
                if (!isSend) {
                    //Toast.makeText(this, "send fail!", Toast.LENGTH_SHORT).show();
                    Log.i(TAG, "sendMessageInBytes fail!");
                }
            } catch (RemoteException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            Toast.makeText(this, "maybe disconnected?", Toast.LENGTH_SHORT).show();
        }
        return isSend;
    }
    
    @SuppressLint("SimpleDateFormat")
	public void logFileCreate() {
    	Date date = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    	String timeStamp = sdf.format(date);
    	
		if (mLogWriter != null) logFileClose();
		File logf = new File(Environment.getExternalStorageDirectory()
        		+ File.separator + "TCP" + timeStamp + ".TXT");
        try {
			mLogWriter = LogWriter.open(logf.getAbsolutePath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    	String casestr = String.format("【CASE %03d】", testCaseAdapter.getCount() - caseidx);
    	String title = null;
    	if (eProtocolType == 0) { 
	    	title = casestr + mContext.getResources().getString((item == 0) ? R.string.sp_test_for_long_short : 
				 ((item == 1) ? R.string.sp_test_for_long_long : 
			     ((item == 2) ? R.string.sp_test_for_short_short : R.string.sp_test_for_datalink_setup)));
    	} else {
    		title = casestr + mContext.getResources().getString(R.string.sp_test_for_udp_loss);
    	}
        logFileWrite("===== " + title + " =====", false);
	}

	public void logFileWrite(String log, boolean isWriteSignalStrength) {
    	try {
			String logWrite = log;
			if (isWriteSignalStrength == true) {
				logWrite = logWrite + ", " + getStringOfSignalStrength();
			}
			mLogWriter.print(/*MainActivity.class, */logWrite);
		} catch (IOException ioe) {
			// TODO Auto-generated catch block
			ioe.printStackTrace();
		} catch (NullPointerException npe) {
			// TODO Auto-generated catch block
			npe.printStackTrace();
		}
    }
	
	public void logFileClose() {
    	try {
			mLogWriter.close();
			mLogWriter = null;
		} catch (IOException ioe) {
			// TODO Auto-generated catch block
			ioe.printStackTrace();
		} catch (NullPointerException npe) {
			// TODO Auto-generated catch block
			npe.printStackTrace();
		}
    }
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_main);

		mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
		mServiceIntent = new Intent(this, LongConnectionService.class);
		mIntentFilter = new IntentFilter();
		mIntentFilter.addAction(LongConnectionService.HEARTBEAT_ACTION);
		mIntentFilter.addAction(LongConnectionService.MESSAGE_ACTION);
		//mReciver = new MessageReciver();
		//onLongConnectionServiceStart();

		mContext = getApplicationContext();
		telephoneManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
	
		PhoneStateListener phoneStateListener = new PhoneStateListener() {
			@Override  
			public void onSignalStrengthsChanged(SignalStrength signalStrength) {  
				// TODO Auto-generated method stub  
				super.onSignalStrengthsChanged(signalStrength); 

				int type = telephoneManager.getNetworkType();
				String signalInfo = signalStrength.toString();
				String [] parts = signalInfo.split(" ");
				int ltedbm = Integer.parseInt(parts[9]);
				int asu = signalStrength.getGsmSignalStrength();
				int dbm = -113 + 2 * asu;
				
				if (type != TelephonyManager.NETWORK_TYPE_UNKNOWN) {
					if (type == TelephonyManager.NETWORK_TYPE_LTE) {
						StringBuffer sb = new StringBuffer();
						sb.append("LTE-RSSI: ").append(ltedbm).append("dBm");
						stringOfsignalStrengths = sb.toString();
					} else if (type == TelephonyManager.NETWORK_TYPE_GSM) {
						StringBuffer sb = new StringBuffer();
						sb.append("GSM-RSSI: ").append(dbm).append("dBm");
						stringOfsignalStrengths = sb.toString();
					} else if (type == TelephonyManager.NETWORK_TYPE_GPRS || 
							   type == TelephonyManager.NETWORK_TYPE_EDGE || 
							   type == TelephonyManager.NETWORK_TYPE_CDMA) {
						StringBuffer sb = new StringBuffer();
						sb.append("2G-RSSI: ").append(dbm).append("dBm");
						stringOfsignalStrengths = sb.toString();
					} else {
						StringBuffer sb = new StringBuffer();
						sb.append("3G-RSSI: ").append(dbm).append("dBm");
						stringOfsignalStrengths = sb.toString();
					}
				} else {
					StringBuffer sb = new StringBuffer();
					String strength = String.valueOf(signalStrength.getGsmSignalStrength());
					sb.append("UNKNOWN-RSSI: ").append(strength).append("dBm");
					stringOfsignalStrengths = sb.toString();
				}
			}
		};
	
		telephoneManager.listen(phoneStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
		networkInit();
        
    	btnCreate = (Button) findViewById(R.id.btn_create);
    	btnCreate.setOnClickListener(this);
    	btnExport = (Button) findViewById(R.id.btn_export);
    	btnExport.setOnClickListener(this);
    	btnScreenshot = (Button) findViewById(R.id.btn_screenshot);
    	btnScreenshot.setOnClickListener(this);
		btnExit = (Button) findViewById(R.id.btn_exit);
		btnExit.setOnClickListener(this);
		
        lvTestCase = (ListView) findViewById(R.id.lv_test_case);  
        testCaseAdapter = new TestCaseAdapter(this);  
        lvTestCase.setAdapter(testCaseAdapter);
	}
	
    @Override
	protected void onStart() {
		super.onStart();
        //onLongConnectionServiceStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
        //onLongConnectionServiceStop();
	}

	private int getProtocolType(int ev) {
		return ev;
	}

    private int getRandomBytes(int min, int max) {
        if (max > min && min > 0) {
            Random rand = new Random();
            return (min + rand.nextInt(max - min + 1));
        }
        return 1024;
    }
	private int getSendBytes(int ev) {
		int sendBytes = 0;
		
		switch (ev) {
			case 0:
				sendBytes = 32;
				break;
			case 1:
				sendBytes = 100;
				break;
			case 2:
				sendBytes = 128;
				break;
			case 3:
				sendBytes = 256;
				break;
			case 4:
				sendBytes = 512;
				break;
			case 5:
				sendBytes = 768;
				break;
			case 6:
				sendBytes = 1024;
				break;
			case 7:
				sendBytes = 1460;
				break;
			case 8:
				sendBytes = 2048;
				break;
			case 9:
				sendBytes = 3072;
				break;
			case 10:
				sendBytes = 4096;
				break;
			case 11:
				sendBytes = getRandomBytes(1, 2048);
				break;
			default:
				sendBytes = 1024;
				break;
		}
		return sendBytes;
	}
	
	private int getItemContent(int ev) {
		return ev;
	}
	
	private void initResult() {
	    recvOntimeCnt = 0;
	    datalinkSetupSuccessCnt = 0;
	    datalinkSetupTotalCnt = 0;
	    connectServerSuccessCnt = 0;
	    connectServerTotalCnt = 0;
	    sendToRecvSuccessCnt = 0;
	    sendToRecvTotalCnt = 0;
	    disconnectServerSuccessCnt = 0;
	    disconnectServerTotalCnt = 0;
	    datalinkDestroySuccessCnt = 0;
	    datalinkDestroyTotalCnt = 0;
	    testCaseSuccessCnt = 0;
	    testCaseTotalCnt = 0;
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_create:
				buildNewConnectionDialog();
				break;
			case R.id.btn_export:
				exportTestResult(); 
				break;
			case R.id.btn_screenshot:
				takeScreenshot();
				break;
			case R.id.btn_exit:
				buildExitConfirmDialog();
				break;
		}
	}

	@Override								
    protected void onDestroy() {
        super.onDestroy();
        //onLongConnectionServiceStop();
    }

	private void buildNewConnectionDialog() {
        // TODO Auto-generated method stub
    	if (testCaseAdapter.getCount() >= TEST_CASE_MAX) {
	        Toast toast = Toast.makeText(getApplicationContext(), "不好意思，最多支持" + TEST_CASE_MAX + "个测试用例！", Toast.LENGTH_SHORT); 
	        toast.show();
	        return;
    	}
    	
        final Dialog dialog = new Dialog(MainActivity.this, R.style.Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.new_connection_dialog);

        /* ProtocolType */
        eProtocolType = 0;
        Spinner spinnerProtocolType = (Spinner)dialog.findViewById(R.id.sp_protocol_type);
        List<String> protocol_type_list = new ArrayList<String>();
        protocol_type_list.add(getResources().getString(R.string.sp_protocol_use_tcp));
        protocol_type_list.add(getResources().getString(R.string.sp_protocol_use_udp));
        
        String[] protocolTypeArray = new String[protocol_type_list.size()];
        ArrayAdapter<String> protocolTypeAdapter = new CustomAdapter(getApplicationContext(), protocol_type_list.toArray(protocolTypeArray));
        spinnerProtocolType.setAdapter(protocolTypeAdapter);
        spinnerProtocolType.setSelection(eProtocolType, true);
		
        spinnerProtocolType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View v,
                    int arg2, long arg3) {
                // TODO Auto-generated method stub
            	eProtocolType = arg2;
            	
                Spinner spinnerTestItem = (Spinner)dialog.findViewById(R.id.sp_test_item);
                EditText etServerPort = (EditText) dialog.findViewById(R.id.et_server_port);
                List<String> test_item_list = null;
                if (eProtocolType == 0) {
        	        eItemContent = 1;
        	        test_item_list = new ArrayList<String>();
        	        test_item_list.add("一、" + getResources().getString(R.string.sp_test_for_long_short));
        	        test_item_list.add("二、" + getResources().getString(R.string.sp_test_for_long_long));
        	        test_item_list.add("三、" + getResources().getString(R.string.sp_test_for_short_short));
        	        test_item_list.add("四、" + getResources().getString(R.string.sp_test_for_datalink_setup));
        	        etServerPort.setText(R.string.tv_server_port_def);
                } else {
        	        eItemContent = 0;
        	        test_item_list = new ArrayList<String>();
        	        test_item_list.add("一、" + getResources().getString(R.string.sp_test_for_udp_loss));
        	        etServerPort.setText(R.string.tv_server_udp_port_def);
                }
                String[] testItemArray = new String[test_item_list.size()];
                ArrayAdapter<String> testItemAdapter = new CustomAdapter(getApplicationContext(), test_item_list.toArray(testItemArray));
        		spinnerTestItem.setAdapter(testItemAdapter);
        		spinnerTestItem.setSelection(eItemContent, true);
            }
            
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
		
        /* SendBytes */
        eSendBytes = 5;
        Spinner spinnerSendBytes = (Spinner)dialog.findViewById(R.id.sp_send_bytes);
        List<String> send_bytes_list = new ArrayList<String>();
        send_bytes_list.add(getResources().getString(R.string.sp_send_32_bytes));
        send_bytes_list.add(getResources().getString(R.string.sp_send_100_bytes));
        send_bytes_list.add(getResources().getString(R.string.sp_send_128_bytes));
        send_bytes_list.add(getResources().getString(R.string.sp_send_256_bytes));
        send_bytes_list.add(getResources().getString(R.string.sp_send_512_bytes));
        send_bytes_list.add(getResources().getString(R.string.sp_send_768_bytes));
        send_bytes_list.add(getResources().getString(R.string.sp_send_1024_bytes));
        send_bytes_list.add(getResources().getString(R.string.sp_send_1460_bytes));
        send_bytes_list.add(getResources().getString(R.string.sp_send_2048_bytes));
        send_bytes_list.add(getResources().getString(R.string.sp_send_3072_bytes));
        send_bytes_list.add(getResources().getString(R.string.sp_send_4096_bytes));
        send_bytes_list.add(getResources().getString(R.string.sp_send_random_bytes));
        
        String[] sendBytesArray = new String[send_bytes_list.size()];
        ArrayAdapter<String> sendBytesAdapter = new CustomAdapter(getApplicationContext(), send_bytes_list.toArray(sendBytesArray));
        spinnerSendBytes.setAdapter(sendBytesAdapter);
        spinnerSendBytes.setSelection(eSendBytes, true);
		
        spinnerSendBytes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View v,
                    int arg2, long arg3) {
                // TODO Auto-generated method stub
            	eSendBytes = arg2;
            }
            
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        
        /* TestItem */
        Spinner spinnerTestItem = (Spinner)dialog.findViewById(R.id.sp_test_item);
        List<String> test_item_list = null;
        if (eProtocolType == 0) {
	        eItemContent = 1;
	        test_item_list = new ArrayList<String>();
	        test_item_list.add("一、" + getResources().getString(R.string.sp_test_for_long_short));
	        test_item_list.add("二、" + getResources().getString(R.string.sp_test_for_long_long));
	        test_item_list.add("三、" + getResources().getString(R.string.sp_test_for_short_short));
	        test_item_list.add("四、" + getResources().getString(R.string.sp_test_for_datalink_setup));
        } else {
	        eItemContent = 0;
	        test_item_list = new ArrayList<String>();
	        test_item_list.add("一、" + getResources().getString(R.string.sp_test_for_udp_loss));
        }
        String[] testItemArray = new String[test_item_list.size()];
        ArrayAdapter<String> testItemAdapter = new CustomAdapter(getApplicationContext(), test_item_list.toArray(testItemArray));
		spinnerTestItem.setAdapter(testItemAdapter);
		spinnerTestItem.setSelection(eItemContent, true);
		
		spinnerTestItem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View v,
                    int arg2, long arg3) {
                // TODO Auto-generated method stub
            	eItemContent = arg2;
            }
            
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
		
        /* NetType */
        eNetType = 1;
        Spinner spinnerNetType = (Spinner)dialog.findViewById(R.id.sp_net_type);
        List<String> net_type_list = new ArrayList<String>();
        net_type_list.add(getResources().getString(R.string.sp_net_type_wifi));
        net_type_list.add(getResources().getString(R.string.sp_net_type_data));
        
        String[] netTypeArray = new String[net_type_list.size()];
        ArrayAdapter<String> netTypeAdapter = new CustomAdapter(getApplicationContext(), net_type_list.toArray(netTypeArray));
        spinnerNetType.setAdapter(netTypeAdapter);
        spinnerNetType.setSelection(eNetType, true);
		
        spinnerNetType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View v,
                    int arg2, long arg3) {
                // TODO Auto-generated method stub
            	eNetType = arg2;
            }
            
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        useTestCount = true;
        ToggleButton swTermination = (ToggleButton) dialog.findViewById(R.id.sw_termination);
        swTermination.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton btn, boolean isChecked) {
                useTestCount = isChecked;

                TextView tvTermination = (TextView) dialog.findViewById(R.id.tv_test_cycles);
                EditText etTermination = (EditText) dialog.findViewById(R.id.et_test_cycles);
                if (isChecked) {
                    tvTermination.setText(R.string.tv_test_cycles);
                    etTermination.setText(R.string.tv_test_cycles_def);
                } else {
                    tvTermination.setText(R.string.tv_test_times);
                    etTermination.setText(R.string.tv_test_times_def);
                }
            }
        });

        Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);   
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        
        Button btnSave = (Button) dialog.findViewById(R.id.btn_save);   
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            	if (isUserInputValid()) {
            		addTestCaseToArrayList(useTestCount);
            		dialog.dismiss();
            	} else {
    		        Toast toast = Toast.makeText(getApplicationContext(), "骚年，你的输入有点随心所欲哦！", Toast.LENGTH_SHORT); 
    		        toast.show();
            	}
            }

			private void addTestCaseToArrayList(boolean isUseCycles) {
				// TODO Auto-generated method stub
            	EditText etServerAddr = (EditText) dialog.findViewById(R.id.et_server_addr);
            	EditText etServerPort = (EditText) dialog.findViewById(R.id.et_server_port);
            	EditText etTestCycles = (EditText) dialog.findViewById(R.id.et_test_cycles);
            	EditText etTestInterval = (EditText) dialog.findViewById(R.id.et_test_interval);
            	
		    	TestCaseConfigs tcc = new TestCaseConfigs();
		    	tcc.caseSignature = String.format("CASE %03d", testCaseAdapter.getCount() + 1);
		    	tcc.isIPAddress = eIsIPAddress;
		    	tcc.itemContent = getItemContent(eItemContent);
		    	tcc.netType = getItemContent(eNetType);
		    	tcc.serverAddr = etServerAddr.getText().toString();
		    	tcc.serverPort = Integer.parseInt(etServerPort.getText().toString());
		    	tcc.protocolType = getProtocolType(eProtocolType);
		    	tcc.sendBytes = getSendBytes(eSendBytes);
                if (isUseCycles == true) {
                    tcc.testTimes = 0;
                    tcc.testCycles = Integer.parseInt(etTestCycles.getText().toString());
                } else {
                    tcc.testCycles = 0;
                    tcc.testTimes = Integer.parseInt(etTestCycles.getText().toString());
                }
		    	tcc.sendInterval = Integer.parseInt(etTestInterval.getText().toString());
		    	testCaseAdapter.testCaseData.add(0, tcc);
		        testCaseAdapter.notifyDataSetChanged();
		        lvTestCase.invalidate();
			}

			private boolean isUserInputValid() {
				// TODO Auto-generated method stub
				int inputAsInteger = 0;
				
            	EditText etServerAddr = (EditText) dialog.findViewById(R.id.et_server_addr);
            	EditText etServerPort = (EditText) dialog.findViewById(R.id.et_server_port);
            	EditText etTestCycles = (EditText) dialog.findViewById(R.id.et_test_cycles);
            	EditText etTestInterval = (EditText) dialog.findViewById(R.id.et_test_interval);
            	
				String serverAddrString = etServerAddr.getText().toString();
				String serverPortString = etServerPort.getText().toString();
				String testCyclesString = etTestCycles.getText().toString();
				String testIntervalString = etTestInterval.getText().toString();
				
            	if (TextUtils.isEmpty(serverAddrString) || TextUtils.isEmpty(serverPortString) ||
					TextUtils.isEmpty(testCyclesString) || TextUtils.isEmpty(testIntervalString)) {
					return false;
				}
            	
            	if (!isValidIPv4Address(serverAddrString)) {
            		if (!isValidDomainAddress(serverAddrString)) {
            			return false;
            		}
            		eIsIPAddress = 0;
            	} else {
            		eIsIPAddress = 1;
            	}
            	
				if (serverPortString.charAt(0) == '0') return false;
				try {
					inputAsInteger = Integer.parseInt(serverPortString);
				} catch (NumberFormatException e) {
					return false;
				}
				if (inputAsInteger == 0 || inputAsInteger > 65535) return false;

				if (testCyclesString.charAt(0) == '0') return false;
				try {
					inputAsInteger = Integer.parseInt(testCyclesString);
				} catch (NumberFormatException e) {
					return false;
				}
				if (inputAsInteger == 0) return false;

				if (testIntervalString.charAt(0) == '0') return false;
				try {
					inputAsInteger = Integer.parseInt(testIntervalString);
				} catch (NumberFormatException e) {
					return false;
				}
				
				return true;
			}

			private boolean isValidDomainAddress(String serverAddrString) {
				// TODO Auto-generated method stub
		        Pattern p = Pattern.compile("^([a-z0-9]+(([-][a-z0-9])?[a-z0-9]*)*\\.){1,3}(com|cn|net|org|fr|me|cc|tv)$", Pattern.CASE_INSENSITIVE);

		        Matcher matcher = p.matcher(serverAddrString);  
		        return matcher.find();
			}

			private boolean isValidIPv4Address(String ipString) {
				// TODO Auto-generated method stub
		        String regex = "^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\."
		        			 + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
		        			 + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
		        			 + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$";
		        return Pattern.matches(regex, ipString);
			}
        });
                 
        dialog.show();
    }

	private void exportTestResult() {
        Toast toast = Toast.makeText(getApplicationContext(), "烦着呢别催，攻城狮正在：\n汗流浃背搬砖，紧锣密鼓筑墙。", Toast.LENGTH_SHORT);
        toast.show(); 
	}
	
	@SuppressLint({ "NewApi", "SimpleDateFormat" })
	private void takeScreenshot() {
        View view = getWindow().getDecorView();
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap bmp = view.getDrawingCache();
        if (bmp != null) {
            try {
            	if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
	                String sdCardPath = Environment.getExternalStorageDirectory().getPath();
	                StatFs sf = new StatFs(sdCardPath);
	                long blockSize = sf.getBlockSizeLong();
	                long availaBlocks = sf.getAvailableBlocksLong();
	                if ((availaBlocks * blockSize) / 1024 /1024 > 1) {
	                	Date date = new Date();
	                	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	                	String timeStamp = sdf.format(date);
		                String filePath = sdCardPath + File.separator + "TCP" + timeStamp + ".PNG";
		                File file = new File(filePath);
		                FileOutputStream fos = new FileOutputStream(file);
		                bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
		                fos.flush();
		                fos.close();
	    		        Toast toast = Toast.makeText(getApplicationContext(), "截屏成功，噢耶！", Toast.LENGTH_SHORT); 
	    		        toast.show();
	                } else {
	    		        Toast toast = Toast.makeText(getApplicationContext(), "骚年，你的小电影好像下多了，\n现在是不是灰常纠结该删掉哪部？", Toast.LENGTH_SHORT); 
	    		        toast.show();
	                }
            	} else {
            		Toast toast = Toast.makeText(getApplicationContext(), "我存在，你婶婶的脑海里。\n天哪木有存储设备，你让我存哪里？", Toast.LENGTH_SHORT);
    		        toast.show();
            	}
            } catch (Exception e) {
		        Toast toast = Toast.makeText(getApplicationContext(), "谁知道发生了什么，\n反正截屏没有成功。", Toast.LENGTH_SHORT);
		        toast.show();
            }
        }
    }
	
	private void buildExitConfirmDialog() {
		CustomDialog.Builder builder = new CustomDialog.Builder(MainActivity.this);
		builder.setTitle(">>  偶买嘎德  <<");
		builder.setMessage("你，真滴真滴确认要离我而去？");

		builder.setPositiveButton("逗你玩呢", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		builder.setNegativeButton("我心好累", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				android.os.Process.killProcess(android.os.Process.myPid());
				System.exit(0);
			}
		});

		builder.create().show();
	}
	
	@SuppressLint("InflateParams")
	private class TestCaseAdapter extends BaseAdapter {
        @SuppressWarnings("unused")
		private Context context;  
        private LayoutInflater inflater;
        public ArrayList<TestCaseConfigs> testCaseData;
        public TestCaseAdapter(Context context) {  
            super();  
            this.context = context;  
            inflater = LayoutInflater.from(context);
            testCaseData = new ArrayList<TestCaseConfigs>();
        }  
        @Override
        
        public int getCount() {  
            // TODO Auto-generated method stub  
        	return testCaseData.size();
        }
        
        @Override  
        public Object getItem(int arg0) {  
            // TODO Auto-generated method stub  
            return arg0;  
        }

        @Override  
        public long getItemId(int arg0) {  
            // TODO Auto-generated method stub  
            return arg0;  
        }

        @Override  
        public View getView(final int position, View view, ViewGroup arg2) {  
            // TODO Auto-generated method stub  
            if(view == null){  
                view = inflater.inflate(R.layout.test_case_listview, null);  
            }
            Button btn_case = (Button) view.findViewById(R.id.btn_test_case);
            btn_case.setText(String.format("CASE %03d", getCount() - position)); 
            
            btn_case.setOnClickListener(new OnClickListener() {  
                @Override  
                public void onClick(View arg0) {  
                    // TODO Auto-generated method stub
                	startRunTestCase(position);
                }  
            });
            
            btn_case.setOnLongClickListener(new OnLongClickListener() {  
                @Override  
                public boolean onLongClick(View arg0) {  
                    // TODO Auto-generated method stub
                	buildDeleteCaseDialog(position);
                    return true;
                }  
            });
            return view;  
        }
        
    	private void buildDeleteCaseDialog(final int position) {
    		CustomDialog.Builder builder = new CustomDialog.Builder(MainActivity.this);
    		builder.setTitle(">>  稍安勿躁  <<");
    		TestCaseConfigs tcc = getTestCaseFromArrayList(position);
    		String testItemContent = null;
        	if (eProtocolType == 0) {
        		testItemContent = (tcc.itemContent == 0) ? getResources().getString(R.string.sp_test_for_long_short) :
        			((tcc.itemContent == 1) ? getResources().getString(R.string.sp_test_for_long_long) : 
        			((tcc.itemContent == 2) ? getResources().getString(R.string.sp_test_for_short_short) : 
        			getResources().getString(R.string.sp_test_for_datalink_setup)));
        	} else {
        		testItemContent = getResources().getString(R.string.sp_test_for_udp_loss);
        	}
			String testTermination = String.format((tcc.testCycles > 0) ? (tcc.testCycles + "cycles, ") : (tcc.testTimes + "mins, "));
    		String msgPrompt = testItemContent + String.format(" [C-%03d]\n", getCount() - position)
			 + "服务器地址：" + tcc.serverAddr + ":" + tcc.serverPort + "\n"
			 + "参数：" + ((tcc.netType == 0) ? "wifi" : "mnet") + ", " + tcc.sendBytes + "bytes, " + testTermination + tcc.sendInterval + "ms";
    		builder.setMessage(msgPrompt);

    		builder.setPositiveButton("亦触即发", new DialogInterface.OnClickListener() {
    			public void onClick(DialogInterface dialog, int which) {
    				dialog.dismiss();
    				startRunTestCase(position);
    			}
    		});

    		builder.setNegativeButton("后会无期", new DialogInterface.OnClickListener() {
    			public void onClick(DialogInterface dialog, int which) {
    				dialog.dismiss();
    				removeGivenTestCase(position);
    			}
    		});

    		builder.create().show();
    	}
    	
    	protected TestCaseConfigs getTestCaseFromArrayList(int position) {
    		// TODO Auto-generated method stub
    		return testCaseData.get(position);
    	}
    	
		protected void removeGivenTestCase(int position) {
			// TODO Auto-generated method stub
            testCaseData.remove(position); 
            testCaseAdapter.notifyDataSetChanged();
		}
		
		protected void startRunTestCase(int position) {
			// TODO Auto-generated method stub
            TestCaseConfigs tcc = getTestCaseFromArrayList(position);
            caseidx = position;
            isLongLongTesting = false;

            if (tcc.protocolType == 0) {
                /* TCP测试 */
            	switch (tcc.itemContent) {
	            	case 0:	//长短连接测试
	            		testForLongShort(tcc);
	            		break;
	            	case 1:	//长长连接测试
	            		testForLongLong(tcc);
	            		break;
	            	case 2:	//短短连接测试
	            		testForShortShort(tcc);
	            		break;
	            	case 3:	//数据链路测试
	            		testForDatalinkSetup(tcc);
	            		break;
	            	default:
	            		break;
            	}
            } else if (tcc.protocolType == 1) {
                /* UDP测试 */
            	testForUDP(tcc);
            }
		}
    }

	public void testForUDP(TestCaseConfigs tcc) {
		// TODO Auto-generated method stub
		refreshGlobalTestCaseConfigs(tcc);
		testStartTime = getCurrentTimeString();
		startTimeInSeconds = getCurrentTimeInSeconds();
		initTestResult();

		new Thread(udp_networkTask).start();
	}

	Runnable udp_networkTask = new Runnable() {
		String UDP_LOG = null;

		@Override
		public void run() {
			// TODO Auto-generated method stub
			if (TextUtils.isEmpty(host)) {
				sendMsgForRefreshUI(1, 0);
				return;
			}

			initResult();
			logFileCreate();
			readIndex = 0;
			reconnectCount = 0;
			sendSuccessCnt = recvSuccessCnt = 0;
			datalinkSetupCnt = datalinkDestroyCnt = serverConnectCnt = serverDisconnectCnt = 0;
			datalinkSetupTimeTotal = datalinkDestroyTimeTotal = serverConnectTimeTotal = serverDisconnectTimeTotal = sendToRecvTimeTotal = 0;
			serverConnectTimeMin = serverConnectTimeMax = 0;
			datalinkSetupTimeMin = datalinkSetupTimeMax = 0;
			sendToRecvTimeMin = sendToRecvTimeMax = 0;
			serverDisconnectTimeMin = serverDisconnectTimeMax = 0;
			datalinkDestroyTimeMin = datalinkDestroyTimeMax = 0;
			llt_li = 0;
			sendPackageTime = new long[LLT_SENDTIME_MAX];
			recvThreadExit = false; 
			content = new byte[sendBytes];
			sendBytesInit(content, (byte)0x7E, (byte)0x55);

			int t = 0;
			do { 
				t = openDatalink();
				if (datalinkSetupCnt == 0) {
					datalinkSetupTimeMin = datalinkSetupTimeMax = t;
				} else {
					datalinkSetupTimeMin = (t < datalinkSetupTimeMin) ? t : datalinkSetupTimeMin;
					datalinkSetupTimeMax = (t > datalinkSetupTimeMax) ? t : datalinkSetupTimeMax;
				}
				datalinkSetupTimeTotal += t;
				datalinkSetupCnt++;
				datalinkSetupTotalCnt++;
				if (t == DATALINK_SETUP_TIMEOUT) {
					UDP_LOG = String.format("[UDP_%08d] ENT: TMOUT", datalinkSetupCnt);
				} else {
					datalinkSetupSuccessCnt++;
					UDP_LOG = String.format("[UDP_%08d] ENT: %dms", datalinkSetupCnt, t);
				}
				logFileWrite(UDP_LOG, true);
				sendMsgForRefreshUI(0, /*datalinkSetupCnt*/0);
				runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
			} while (t == DATALINK_SETUP_TIMEOUT && ((cycles > 0 && datalinkSetupCnt < cycles) || (runningTimeInSeconds >= 0 && runningTimeInSeconds < 60 * times)));

			if (t < DATALINK_SETUP_TIMEOUT) {
				logFileWrite(SPLIT_LINE, false);
				if (isIPAddress == 0) {
					host = getIPV4AddressFromDomain(host);
					isIPAddress = 1;
				}

				try {
					udp_socket = new DatagramSocket(port);
					udp_socket.setSoTimeout(SOCKET_RECWAIT_TIMEOUT);
					udp_socAddress = InetAddress.getByName(host);
				} catch (IOException ex) {
					ex.printStackTrace();
				}

				udpSendThread = new Thread(new Runnable() {
					@Override
					public void run() {
						while (true) {
							runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
							if ((cycles > 0 && sendSuccessCnt >= cycles) || (times > 0 && runningTimeInSeconds >= 60 * times)) {
								//SystemClock.sleep(10 * 1000);
								recvThreadExit = true;
								return;
							} else {
								try {
									sendPackageTime[llt_li % LLT_SENDTIME_MAX] = System.currentTimeMillis();
									sendBytesIndexSet(content, llt_li % LLT_SENDTIME_MAX);
									llt_li++;
									udp_package = new DatagramPacket(content, content.length, udp_socAddress, port);
									udp_socket.send(udp_package);
									testCaseTotalCnt++;
									sendSuccessCnt++;
									sendToRecvTotalCnt++;
									sendMsgForRefreshUI(0, sendSuccessCnt);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								SystemClock.sleep(interval);
							}
						}
					}
				});
				udpSendThread.start();
				
				udpRecvThread = new Thread(new Runnable() {					
					@Override
					public void run() {
						int lastLeftBytes = 0;
						byte[] readBuffer = new byte[10240];
						int recvLeft = 0;
						byte[] recvBytes = new byte[sendBytes];

						while (!recvThreadExit) {
							try {
								udp_recvpackage = new DatagramPacket(readBuffer, readBuffer.length);
								udp_socket.receive(udp_recvpackage);
								//new String(udp_recvpackage.getData() , udp_recvpackage.getOffset() , udp_recvpackage.getLength());
								int readSize = udp_recvpackage.getLength();
								if (readSize <= 0) {
									continue;
								}
								readIndex++;
								int recvPackage = (lastLeftBytes + readSize) / sendBytes;
								if (recvPackage > 0) {
									long rt = System.currentTimeMillis();
									int pkg = 0;
									int srcpos = 0;
									do {
										if (sendBytes > recvLeft && srcpos < readSize && srcpos >= 0 && recvLeft >= 0) {
											System.arraycopy(udp_recvpackage.getData(), srcpos, recvBytes, recvLeft, sendBytes - recvLeft);
											srcpos += (sendBytes - recvLeft);
										}

										int idx = getIndexFromRecvBytes(recvBytes);
										int srt = 0;
										if (idx >= 0 && idx < LLT_SENDTIME_MAX) {
											srt = (int)(rt - sendPackageTime[idx % LLT_SENDTIME_MAX]);
											if (recvSuccessCnt == 0) {
												sendToRecvTimeMin = sendToRecvTimeMax = srt;
											} else {
												sendToRecvTimeMax = (srt > sendToRecvTimeMax) ? srt : sendToRecvTimeMax;
												sendToRecvTimeMin = (srt < sendToRecvTimeMin) ? srt : sendToRecvTimeMin;
											}
											sendToRecvTimeTotal += srt;
											recvSuccessCnt++;
											if (srt <= RECV_TIME_MAX) {recvOntimeCnt++;}
										}
										recvLeft = 0;
										pkg++;
										recvPackage--;
										testCaseSuccessCnt = recvSuccessCnt;
										if (srt > 0) {
											UDP_LOG = String.format("[UDP_%08d] SRT: %dms", recvSuccessCnt, srt);
										}/* else {
											UDP_LOG = String.format("[UDP_%08d] SRT: TMOUT", recvSuccessCnt);
										}*/
										logFileWrite(UDP_LOG, true);
										if (recvSuccessCnt > 0 && (recvSuccessCnt % TEST_FOR_AVGCNT == 0)) {
											udp_AvgRecordWrite(0);
										}
									} while (recvPackage > 0);
								}
								sendMsgForRefreshUI(0, sendSuccessCnt);
								int tmpLastLeft = lastLeftBytes;
								sendToRecvSuccessCnt += (lastLeftBytes + readSize) / sendBytes;
								sendToRecvSuccessCnt = testCaseSuccessCnt; // some idx was wrong!!!
								lastLeftBytes = (lastLeftBytes + readSize) % sendBytes;
								recvLeft = lastLeftBytes;
								if (recvLeft > 0) {
									int srcPos = 0, dstPos = 0, cpSize = 0;
									if (readSize + tmpLastLeft < sendBytes) {
										srcPos = 0;
										dstPos = tmpLastLeft;
										cpSize = readSize;
									} else {
										srcPos = readSize - recvLeft;
										dstPos = 0;
										cpSize = recvLeft;
									}
									if (srcPos >= 0 && dstPos >= 0 && cpSize > 0) {
										System.arraycopy(udp_recvpackage.getData(), srcPos, recvBytes, dstPos, cpSize);
									}
								}

								//Log.i(TAG, "RECV: " + readIndex + ", readSize: " + readSize + ", recvSuccessCnt: " + recvSuccessCnt);
								runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
								if ((cycles > 0 && recvSuccessCnt >= cycles) || (times > 0 && runningTimeInSeconds >= 60 * times)) {
									recvThreadExit = true;
								}
							} catch (SocketTimeoutException ste) {
								// TODO Auto-generated catch block
								ste.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						udp_AvgRecordWrite(1);
						udp_socket.close();
						udp_destroyDatalink();
						logFileClose();
					}
				});
				udpRecvThread.start();
			} else {
				sendMsgForRefreshUI(1, sendSuccessCnt);
			}
		}
		
		private void udp_destroyDatalink() {
			int tt = 0;
			
			if (recvSuccessCnt > 0) logFileWrite(SPLIT_LINE, false);
			do {
				tt = closeDatalink();
				datalinkDestroyTotalCnt++;
				if (datalinkDestroyCnt == 0) {
					datalinkDestroyTimeMin = datalinkDestroyTimeMax = tt;
				} else {
					datalinkDestroyTimeMin = (tt < datalinkDestroyTimeMin) ? tt : datalinkDestroyTimeMin;
					datalinkDestroyTimeMax = (tt > datalinkDestroyTimeMax) ? tt : datalinkDestroyTimeMax;
				}
				datalinkDestroyTimeTotal += tt;
				datalinkDestroyCnt++;
				if (tt < DATALINK_SETUP_TIMEOUT) {
					datalinkDestroySuccessCnt++;
					UDP_LOG = String.format("[UDP_%08d] DNT: %dms", datalinkDestroyCnt, tt);
				} else {
					UDP_LOG = String.format("[UDP_%08d] DNT: TMOUT", datalinkDestroyCnt);
				}
				logFileWrite(UDP_LOG, true);
				runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
			} while (tt == DATALINK_SETUP_TIMEOUT && ((cycles > 0 && datalinkDestroyCnt < cycles) || (runningTimeInSeconds >= 0 && runningTimeInSeconds < 60 * times)));

			sendMsgForRefreshUI(1, sendSuccessCnt);
		}
		
		private void udp_AvgRecordWrite(int flag) {
			// TODO Auto-generated method stub
			if (flag == 1 && (/*cycles*/recvSuccessCnt % TEST_FOR_AVGCNT == 0)) return;

			String avgRecord = null;
			int idx = (flag == 0 ) ? (recvSuccessCnt / TEST_FOR_AVGCNT) : (/*cycles*/recvSuccessCnt / TEST_FOR_AVGCNT + 1);

			avgRecord = String.format("[UDP_AVG_%04d] ENT: %d-%d-%.2f", idx, datalinkSetupTimeMin, datalinkSetupTimeMax, (datalinkSetupCnt > 0) ? (float)datalinkSetupTimeTotal / datalinkSetupCnt : 0.00);
			avgRecord = avgRecord + String.format(", SRT: %d-%d-%.2f", sendToRecvTimeMin, sendToRecvTimeMax, (recvSuccessCnt > 0) ? (float)sendToRecvTimeTotal / recvSuccessCnt : 0.00);
			avgRecord = avgRecord + String.format(", DNT: %d-%d-%.2f", datalinkDestroyTimeMin, datalinkDestroyTimeMax, (datalinkDestroyCnt > 0)? (float)datalinkDestroyTimeTotal / datalinkDestroyCnt : 0.00);
			avgRecord = avgRecord + String.format(", \n%42cSuccess: %d, Failure: %d [%.2f%%], Ontime = %.2f%%.", ' ', testCaseSuccessCnt, (testCaseTotalCnt - testCaseSuccessCnt), ((testCaseTotalCnt > 0) ? 100.00 * testCaseSuccessCnt / testCaseTotalCnt : 0.00), ((testCaseSuccessCnt > 0) ? 100.00 * recvOntimeCnt / testCaseSuccessCnt : 0.00));

			logFileWrite(avgRecord, false);
		}
	};
    
	public void testForDatalinkSetup(TestCaseConfigs tcc) {
		// TODO Auto-generated method stub
		refreshGlobalTestCaseConfigs(tcc);
		testStartTime = getCurrentTimeString();
		startTimeInSeconds = getCurrentTimeInSeconds();
		initTestResult();
		
		new Thread(dst_networkTask).start();
	}

	Runnable dst_networkTask = new Runnable() {
		String DST_LOG = null;
	    @Override  
	    public void run() {
	    	// TODO Auto-generated method stub
            
	    	initResult();
	    	logFileCreate();
            readIndex = 0;
            reconnectCount = 0;
            sendSuccessCnt = recvSuccessCnt = 0;
            datalinkSetupCnt = datalinkDestroyCnt = serverConnectCnt = serverDisconnectCnt = 0;
            datalinkSetupTimeTotal = datalinkDestroyTimeTotal = serverConnectTimeTotal = serverDisconnectTimeTotal = sendToRecvTimeTotal = 0;
			serverConnectTimeMin = serverConnectTimeMax = 0;
			datalinkSetupTimeMin = datalinkSetupTimeMax = 0;
			sendToRecvTimeMin = sendToRecvTimeMax = 0;
            serverDisconnectTimeMin = serverDisconnectTimeMax = 0;
            datalinkDestroyTimeMin = datalinkDestroyTimeMax = 0;
            llt_li = 0;
            sendPackageTime = new long[LLT_SENDTIME_MAX];
            recvThreadExit = false;
            content = new byte[sendBytes];
            sendBytesInit(content, (byte)0x7E, (byte)0x55);
	    	
        	while (!recvThreadExit) {
    			int t = openDatalink();
	            if (datalinkSetupCnt == 0) {
	            	datalinkSetupTimeMin = datalinkSetupTimeMax = t;
	            } else {
	            	datalinkSetupTimeMin = (t < datalinkSetupTimeMin) ? t : datalinkSetupTimeMin;
					datalinkSetupTimeMax = (t > datalinkSetupTimeMax) ? t : datalinkSetupTimeMax;
	            }
	            datalinkSetupTimeTotal += t;
	            datalinkSetupCnt++;
	            datalinkSetupTotalCnt++;
	            testCaseTotalCnt++;
    		    if (t < DATALINK_SETUP_TIMEOUT) {
    		    	DST_LOG = String.format("[DST_%08d] ENT: %dms", datalinkSetupCnt, t);
    		        datalinkSetupSuccessCnt++;
	    		    int tt = closeDatalink();
	    		    datalinkDestroyTotalCnt++;
		            if (datalinkDestroyCnt == 0) {
		            	datalinkDestroyTimeMin = datalinkDestroyTimeMax = tt;
		            } else {
		            	datalinkDestroyTimeMin = (tt < datalinkDestroyTimeMin) ? tt : datalinkDestroyTimeMin;
		            	datalinkDestroyTimeMax = (tt > datalinkDestroyTimeMax) ? tt : datalinkDestroyTimeMax;
		            }
		            datalinkDestroyTimeTotal += tt;
		            datalinkDestroyCnt++;
		            if (tt < DATALINK_SETUP_TIMEOUT) {
		                datalinkDestroySuccessCnt++;
		                DST_LOG = DST_LOG + String.format(", DNT: %dms", tt);
		            } else {
	                    DST_LOG = DST_LOG + String.format(", DNT: TMOUT");
		            }
		            testCaseSuccessCnt = (datalinkSetupSuccessCnt < datalinkDestroySuccessCnt) ? datalinkSetupSuccessCnt : datalinkDestroySuccessCnt;
        		} else {
        			DST_LOG = String.format("[DST_%08d] ENT: TMOUT", datalinkSetupCnt);
        		}
    		    
	            logFileWrite(DST_LOG, true);
	            if (datalinkSetupCnt > 0 && (datalinkSetupCnt % TEST_FOR_AVGCNT == 0)) {
	            	dst_AvgRecordWrite(0);
	            }
	            sendMsgForRefreshUI(0, datalinkSetupCnt);

                runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
                if ((cycles > 0 && datalinkSetupCnt >= cycles) || (times > 0 && runningTimeInSeconds >= 60 * times)) {
            		recvThreadExit = true;
                } else {
                	SystemClock.sleep(((interval < SST_TIME_INTERVAL_MIN) ? SST_TIME_INTERVAL_MIN : interval));
                }
        	}
        	dst_AvgRecordWrite(1);
        	sendMsgForRefreshUI(1, datalinkSetupCnt);
        	logFileClose();
	    }

		private void dst_AvgRecordWrite(int flag) {
			// TODO Auto-generated method stub
			if (flag == 1 && (/*cycles*/datalinkSetupCnt % TEST_FOR_AVGCNT == 0)) return;
			
			String avgRecord = null;
			int idx = (flag == 0 ) ? (datalinkSetupCnt / TEST_FOR_AVGCNT) : (/*cycles*/datalinkSetupCnt / TEST_FOR_AVGCNT + 1);
			
			avgRecord = String.format("[DST_AVG_%04d] ENT: %d-%d-%.2f", idx, datalinkSetupTimeMin, datalinkSetupTimeMax, (datalinkSetupCnt > 0) ? (float)datalinkSetupTimeTotal / datalinkSetupCnt : 0.00);
			avgRecord = avgRecord + String.format(", DNT: %d-%d-%.2f", datalinkDestroyTimeMin, datalinkDestroyTimeMax, (datalinkDestroyCnt > 0) ? (float)datalinkDestroyTimeTotal / datalinkDestroyCnt : 0.00);
			avgRecord = avgRecord + String.format(", Success: %d, Failure: %d [%.2f%%]", testCaseSuccessCnt, (testCaseTotalCnt - testCaseSuccessCnt), ((testCaseTotalCnt > 0) ? 100.00 * testCaseSuccessCnt / testCaseTotalCnt : 0.00));
        	
			logFileWrite(avgRecord, false);
		}
	};
	
	public void testForLongShort(TestCaseConfigs tcc) {
		// TODO Auto-generated method stub
		refreshGlobalTestCaseConfigs(tcc);
		testStartTime = getCurrentTimeString();
		startTimeInSeconds = getCurrentTimeInSeconds();
		initTestResult();
		
		new Thread(lst_networkTask).start();
	}

	Runnable lst_networkTask = new Runnable() {
		String LST_LOG = null;
		
	    @Override  
	    public void run() {
	    	// TODO Auto-generated method stub
	    	boolean isConnected = false;
	    	
	    	if (TextUtils.isEmpty(host)) {
	    		sendMsgForRefreshUI(1, 0);
	    		return;
	    	}
            
	    	initResult();
	    	logFileCreate();
            readIndex = 0;
            reconnectCount = 0;
            sendSuccessCnt = recvSuccessCnt = 0;
            datalinkSetupCnt = datalinkDestroyCnt = serverConnectCnt = serverDisconnectCnt = 0;
            datalinkSetupTimeTotal = datalinkDestroyTimeTotal = serverConnectTimeTotal = serverDisconnectTimeTotal = sendToRecvTimeTotal = 0;
			serverConnectTimeMin = serverConnectTimeMax = 0;
			datalinkSetupTimeMin = datalinkSetupTimeMax = 0;
			sendToRecvTimeMin = sendToRecvTimeMax = 0;
            serverDisconnectTimeMin = serverDisconnectTimeMax = 0;
            datalinkDestroyTimeMin = datalinkDestroyTimeMax = 0;
            llt_li = 0;
            sendPackageTime = new long[LLT_SENDTIME_MAX];
            recvThreadExit = false;
            content = new byte[sendBytes];
            sendBytesInit(content, (byte)0x7E, (byte)0x55);
            byte[] readBuffer = new byte[10240];
	    	
            int t = 0;
			do {
    			t = openDatalink();
	            if (datalinkSetupCnt == 0) {
	            	datalinkSetupTimeMin = datalinkSetupTimeMax = t;
	            } else {
	            	datalinkSetupTimeMin = (t < datalinkSetupTimeMin) ? t : datalinkSetupTimeMin;
					datalinkSetupTimeMax = (t > datalinkSetupTimeMax) ? t : datalinkSetupTimeMax;
	            }
	            datalinkSetupTimeTotal += t;
	            datalinkSetupCnt++;
	            datalinkSetupTotalCnt++;
				if (t == DATALINK_SETUP_TIMEOUT) {
					LST_LOG = String.format("[LST_%08d] ENT: TMOUT", datalinkSetupCnt);
				} else {
					datalinkSetupSuccessCnt++;
					LST_LOG = String.format("[LST_%08d] ENT: %dms", datalinkSetupCnt, t);
				}
				logFileWrite(LST_LOG, true);
				sendMsgForRefreshUI(0, /*datalinkSetupCnt*/0);
				runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
			} while (t == DATALINK_SETUP_TIMEOUT && ((cycles > 0 && datalinkSetupCnt < cycles) || (runningTimeInSeconds >= 0 && runningTimeInSeconds < 60 * times)));
				
            if (t < DATALINK_SETUP_TIMEOUT) {
            	logFileWrite(SPLIT_LINE, false);
        		if (isIPAddress == 0) {
        			host = getIPV4AddressFromDomain(host);
        			isIPAddress = 1;
        		}
				
            	while (!recvThreadExit) {
			        try {
			        	int cst = SOCKET_CONNECT_TIMEOUT;
			        	long cs = System.currentTimeMillis();
			        	isConnected = false;
			            socket = new Socket();
			            SocketAddress socAddress = new InetSocketAddress(InetAddress.getByName(host), port);
                        do {
                            try {
                                socket.connect(socAddress, SOCKET_CONNECT_TIMEOUT);
                                cst = 0;
                                break;
                            } catch (SocketTimeoutException ste) {
                                // TODO Auto-generated catch block
                                ste.printStackTrace();
                                socket.close();
                                //cst = SOCKET_CONNECT_TIMEOUT;
                                continue;
                            } catch (SocketException se) {
                                // TODO Auto-generated catch block
                                se.printStackTrace();
                                socket.close();
                                //cst = SOCKET_CONNECT_TIMEOUT;
                                continue;
                            } catch (UnknownHostException uhe) {
                                // TODO Auto-generated catch block
                                uhe.printStackTrace();
                                socket.close();
                                //cst = SOCKET_CONNECT_TIMEOUT;
                                Log.i(TAG, "UnknownHostException then close datalink");
                                continue;
                            }
                        } while (System.currentTimeMillis() < cs + SOCKET_CONNECT_TIMEOUT);
			            
			            if (cst != SOCKET_CONNECT_TIMEOUT) {
			            	isConnected = true;
				            connectServerSuccessCnt++;
				            socket.setSoLinger(true, 60);
				            socket.setTcpNoDelay(true);
				            socket.setReuseAddress(true);
				            socket.setSoTimeout(SOCKET_RECWAIT_TIMEOUT);
				            socket.setSendBufferSize(10240);
				            socket.setReceiveBufferSize(10240);
			            	cst = (int)(System.currentTimeMillis() - cs);
			            	LST_LOG = String.format("[LST_%08d] CST: %dms", serverConnectCnt + 1, cst);
			            } else {
			            	LST_LOG = String.format("[LST_%08d] CST: TMOUT", serverConnectCnt + 1);
			            }
			            
			            if (serverConnectCnt == 0) {
			            	serverConnectTimeMin = serverConnectTimeMax = cst;
			            } else {
							serverConnectTimeMin = (cst < serverConnectTimeMin) ? cst : serverConnectTimeMin;
							serverConnectTimeMax = (cst > serverConnectTimeMax) ? cst : serverConnectTimeMax;
			            }
					    serverConnectTimeTotal += cst;
					    serverConnectCnt++;
					    connectServerTotalCnt++;
					    testCaseTotalCnt++;

					    if (isConnected) {
							dis = new DataInputStream(socket.getInputStream());
							dos = new DataOutputStream(socket.getOutputStream());
							if (!socket.isClosed()) {
								if (socket.isConnected()) {
									if (!socket.isOutputShutdown()) {
										int srt = 0;
										long sr = System.currentTimeMillis();
										dos.write(content, 0, content.length);
										sendSuccessCnt++;
										sendToRecvTotalCnt++;
										
										int lastLeftBytes = 0;
										do {
											try {
												int readSize = dis.read(readBuffer);
												if (readSize <= 0) {
													break;
												}
												lastLeftBytes += readSize;
												if (lastLeftBytes >= sendBytes) {
													srt = (int)(System.currentTimeMillis() - sr);
													if (recvSuccessCnt == 0) {
														sendToRecvTimeMin = sendToRecvTimeMax = srt;
													} else {
														sendToRecvTimeMin = (srt < sendToRecvTimeMin) ? srt : sendToRecvTimeMin;
														sendToRecvTimeMax = (srt > sendToRecvTimeMax) ? srt : sendToRecvTimeMax;
													}
													sendToRecvTimeTotal += srt;
													recvSuccessCnt++;
													sendToRecvSuccessCnt++;
													if (srt <= RECV_TIME_MAX) {recvOntimeCnt++;}
												}
												readIndex++;
												Log.i(TAG, "RECV: " + readIndex + ", readSize: " + readSize + ", recvSuccessCnt: " + recvSuccessCnt);
											} catch (SocketTimeoutException ste) {
												// TODO Auto-generated catch block
												ste.printStackTrace();
												break;
											} catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												break;
											}
										} while (lastLeftBytes < sendBytes);
										if (srt > 0) {
											LST_LOG = LST_LOG + String.format(", SRT: %dms", srt);
										} else {
											LST_LOG = LST_LOG + String.format(", SRT: TMOUT");
										}
									}
								}
							}
						
                            int sdt = 0;
                            long sd = System.currentTimeMillis();
                            try {
                                socket.close();
                                disconnectServerSuccessCnt++;
                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            disconnectServerTotalCnt++;
                            sdt = (int)(System.currentTimeMillis() - sd);
                            if (serverDisconnectCnt == 0) {
                                serverDisconnectTimeMin = serverDisconnectTimeMax = sdt;
                            } else {
                                serverDisconnectTimeMin = (sdt < serverDisconnectTimeMin) ? sdt : serverDisconnectTimeMin;
                                serverDisconnectTimeMax = (sdt > serverDisconnectTimeMax) ? sdt : serverDisconnectTimeMax;
                            }
                            serverDisconnectTimeTotal += sdt;
                            serverDisconnectCnt++;
                            /* if (sdt > 0) {
                                LST_LOG = LST_LOG + String.format(", DST: %dms", sdt);
                            } else {
                                LST_LOG = LST_LOG + String.format(", DST: TMOUT");
                            } */
                            LST_LOG = LST_LOG + String.format(", DST: %dms", sdt);
                        }
						logFileWrite(LST_LOG, true);
			        } catch (IOException ex) {
			            ex.printStackTrace();
						//continue;
			        }
			        
			        testCaseSuccessCnt = (sendToRecvSuccessCnt < disconnectServerSuccessCnt) ? sendToRecvSuccessCnt : disconnectServerSuccessCnt;
			        testCaseSuccessCnt = (testCaseSuccessCnt < connectServerSuccessCnt) ? testCaseSuccessCnt : connectServerSuccessCnt;
			        
			        if (serverConnectCnt > 0 && (serverConnectCnt % TEST_FOR_AVGCNT == 0)) {
						lst_AvgRecordWrite(0);
					}
					sendMsgForRefreshUI(0, serverConnectCnt);

                    runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
                    if ((cycles > 0 && serverConnectCnt >= cycles) || (times > 0 && runningTimeInSeconds >= 60 * times)) {
                		recvThreadExit = true;
                    } else {
                    	SystemClock.sleep(interval);
                    }
            	}
            	logFileWrite(SPLIT_LINE, false);
				
				int tt = 0;
            	do {
					tt = closeDatalink();
					datalinkDestroyTotalCnt++;
					if (datalinkDestroyCnt == 0) {
						datalinkDestroyTimeMin = datalinkDestroyTimeMax = tt;
					} else {
						datalinkDestroyTimeMin = (tt < datalinkDestroyTimeMin) ? tt : datalinkDestroyTimeMin;
						datalinkDestroyTimeMax = (tt > datalinkDestroyTimeMax) ? tt : datalinkDestroyTimeMax;
					}
					datalinkDestroyTimeTotal += tt;
					datalinkDestroyCnt++;
					if (tt < DATALINK_SETUP_TIMEOUT) {
						datalinkDestroySuccessCnt++;
						LST_LOG = String.format("[LST_%08d] DNT: %dms", datalinkDestroyCnt, tt);
					} else {
						LST_LOG = String.format("[LST_%08d] DNT: TMOUT", datalinkDestroyCnt);
					}
					logFileWrite(LST_LOG, true);
                    runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
			    } while (tt == DATALINK_SETUP_TIMEOUT && ((cycles > 0 && datalinkDestroyCnt < cycles) || (runningTimeInSeconds >= 0 && runningTimeInSeconds < 60 * times)));
				lst_AvgRecordWrite(1);
			}
        	sendMsgForRefreshUI(1, serverConnectCnt);
        	logFileClose();
	    }  

		private void lst_AvgRecordWrite(int flag) {
			// TODO Auto-generated method stub
			if (flag == 1 && (/*cycles*/serverConnectCnt % TEST_FOR_AVGCNT == 0)) return;
			
			String avgRecord = null;
			int idx = (flag == 0 ) ? (serverConnectCnt / TEST_FOR_AVGCNT) : (/*cycles*/serverConnectCnt / TEST_FOR_AVGCNT + 1);
			
			avgRecord = String.format("[LST_AVG_%04d] ENT: %d-%d-%.2f", idx, datalinkSetupTimeMin, datalinkSetupTimeMax, (datalinkSetupCnt > 0) ? (float)datalinkSetupTimeTotal / datalinkSetupCnt : 0.00);
			avgRecord = avgRecord + String.format(", CST: %d-%d-%.2f", serverConnectTimeMin, serverConnectTimeMax, (serverConnectCnt > 0) ? (float)serverConnectTimeTotal / serverConnectCnt : 0.00);
			avgRecord = avgRecord + String.format(", SRT: %d-%d-%.2f", sendToRecvTimeMin, sendToRecvTimeMax, (recvSuccessCnt > 0) ? (float)sendToRecvTimeTotal / recvSuccessCnt : 0.00);
			avgRecord = avgRecord + String.format(", DST: %d-%d-%.2f", serverDisconnectTimeMin, serverDisconnectTimeMax, (serverDisconnectCnt > 0) ? (float)serverDisconnectTimeTotal / serverDisconnectCnt : 0.00);
			avgRecord = avgRecord + String.format(", \n%42cDNT: %d-%d-%.2f", ' ', datalinkDestroyTimeMin, datalinkDestroyTimeMax, (datalinkDestroyCnt > 0) ? (float)datalinkDestroyTimeTotal / datalinkDestroyCnt : 0.00);
			avgRecord = avgRecord + String.format(", Success: %d, Failure: %d [%.2f%%], Ontime = %.2f%%.", testCaseSuccessCnt, (testCaseTotalCnt - testCaseSuccessCnt), ((testCaseTotalCnt > 0) ? 100.00 * testCaseSuccessCnt / testCaseTotalCnt : 0.00), ((testCaseSuccessCnt > 0) ? 100.00 * recvOntimeCnt / testCaseSuccessCnt : 0.00));
        	
			logFileWrite(avgRecord, false);
		}
	};
	
	public void testForLongLong(TestCaseConfigs tcc) {
		// TODO Auto-generated method stub
		refreshGlobalTestCaseConfigs(tcc);
		testStartTime = getCurrentTimeString();
		startTimeInSeconds = getCurrentTimeInSeconds();
		initTestResult();

		new Thread(llt_networkTask_new).start();
	}

    Handler llt_handler_new = new Handler();
    Runnable llt_runnable_new = new Runnable() {
        @Override
        public void run() {
            runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
            if ((cycles > 0 && sendSuccessCnt >= cycles) || (times > 0 && runningTimeInSeconds >= 60 * times)) {
				llt_handler_new.removeCallbacks(this);
				recvThreadExit = true;
			} else {
                llt_handler_new.postDelayed(this, interval);
                sendPackageTime[llt_li % LLT_SENDTIME_MAX] = System.currentTimeMillis();
                sendBytesIndexSet(content, llt_li % LLT_SENDTIME_MAX);
                llt_li++;
                boolean s = onLongConnectionServiceMessageSend(content, content.length);
                testCaseTotalCnt++;
                sendSuccessCnt++;
                sendToRecvTotalCnt++;
                sendMsgForRefreshUI(0, sendSuccessCnt);
			}
        }
    };

    private void llt_AvgRecordWrite_new(int flag) {
        // TODO Auto-generated method stub
        if (flag == 1 && (/*cycles*/recvSuccessCnt % TEST_FOR_AVGCNT == 0)) return;

        String avgRecord = null;
        int idx = (flag == 0 ) ? (recvSuccessCnt / TEST_FOR_AVGCNT) : (/*cycles*/recvSuccessCnt / TEST_FOR_AVGCNT + 1);

        avgRecord = String.format("[LLT_AVG_%04d] ENT: %d-%d-%.2f", idx, datalinkSetupTimeMin, datalinkSetupTimeMax, (datalinkSetupCnt > 0) ? (float)datalinkSetupTimeTotal / datalinkSetupCnt : 0.00);
        avgRecord = avgRecord + String.format(", CST: %d-%d-%.2f", serverConnectTimeMin, serverConnectTimeMax, (serverConnectCnt > 0) ? (float)serverConnectTimeTotal / serverConnectCnt : 0.00);
        avgRecord = avgRecord + String.format(", SRT: %d-%d-%.2f", sendToRecvTimeMin, sendToRecvTimeMax, (recvSuccessCnt > 0) ? (float)sendToRecvTimeTotal / recvSuccessCnt : 0.00);
        avgRecord = avgRecord + String.format(", DST: %d-%d-%.2f", serverDisconnectTimeMin, serverDisconnectTimeMax, (serverDisconnectCnt > 0) ? (float)serverDisconnectTimeTotal / serverDisconnectCnt : 0.00);
        avgRecord = avgRecord + String.format(", \n%42cDNT: %d-%d-%.2f", ' ', datalinkDestroyTimeMin, datalinkDestroyTimeMax, (datalinkDestroyCnt > 0)? (float)datalinkDestroyTimeTotal / datalinkDestroyCnt : 0.00);
        avgRecord = avgRecord + String.format(", Success: %d, Failure: %d [%.2f%%], Ontime = %.2f%%.", testCaseSuccessCnt, (testCaseTotalCnt - testCaseSuccessCnt), ((testCaseTotalCnt > 0) ? 100.00 * testCaseSuccessCnt / testCaseTotalCnt : 0.00), ((testCaseSuccessCnt > 0) ? 100.00 * recvOntimeCnt / testCaseSuccessCnt : 0.00));

        logFileWrite(avgRecord, false);
    }

    Runnable llt_networkTask_new = new Runnable() {
        String LLT_LOG = null;

        @Override
        public void run() {
            // TODO Auto-generated method stub
            boolean isConnected = false;

            if (TextUtils.isEmpty(host)) {
                sendMsgForRefreshUI(1, 0);
                return;
            }

            isLongLongTesting = true;
            onLongConnectionServiceStart();
            initResult();
            logFileCreate();
            readIndex = 0;
            reconnectCount = 0;
            sendSuccessCnt = recvSuccessCnt = 0;
            datalinkSetupCnt = datalinkDestroyCnt = serverConnectCnt = serverDisconnectCnt = 0;
            datalinkSetupTimeTotal = datalinkDestroyTimeTotal = serverConnectTimeTotal = serverDisconnectTimeTotal = sendToRecvTimeTotal = 0;
            serverConnectTimeMin = serverConnectTimeMax = 0;
            datalinkSetupTimeMin = datalinkSetupTimeMax = 0;
            sendToRecvTimeMin = sendToRecvTimeMax = 0;
            serverDisconnectTimeMin = serverDisconnectTimeMax = 0;
            datalinkDestroyTimeMin = datalinkDestroyTimeMax = 0;
            llt_li = 0;
            sendPackageTime = new long[LLT_SENDTIME_MAX];
            recvThreadExit = false;
            content = new byte[sendBytes];
            sendBytesInit(content, (byte)0x7E, (byte)0x55);

            int t = 0;
            do {
                t = openDatalink();
                if (datalinkSetupCnt == 0) {
                    datalinkSetupTimeMin = datalinkSetupTimeMax = t;
                } else {
                    datalinkSetupTimeMin = (t < datalinkSetupTimeMin) ? t : datalinkSetupTimeMin;
                    datalinkSetupTimeMax = (t > datalinkSetupTimeMax) ? t : datalinkSetupTimeMax;
                }
                datalinkSetupTimeTotal += t;
                datalinkSetupCnt++;
                datalinkSetupTotalCnt++;
                if (t == DATALINK_SETUP_TIMEOUT) {
                    LLT_LOG = String.format("[LLT_%08d] ENT: TMOUT", datalinkSetupCnt);
                } else {
                    datalinkSetupSuccessCnt++;
                    LLT_LOG = String.format("[LLT_%08d] ENT: %dms", datalinkSetupCnt, t);
                }
                logFileWrite(LLT_LOG, true);
                sendMsgForRefreshUI(0, /*datalinkSetupCnt*/0);
                runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
            } while (t == DATALINK_SETUP_TIMEOUT && ((cycles > 0 && datalinkSetupCnt < cycles) || (runningTimeInSeconds >= 0 && runningTimeInSeconds < 60 * times)));

            if (t < DATALINK_SETUP_TIMEOUT) {
                logFileWrite(SPLIT_LINE, false);
                if (isIPAddress == 0) {
                    host = getIPV4AddressFromDomain(host);
                    isIPAddress = 1;
                }

                int cst = 0;
                long cs = System.currentTimeMillis();

                do {
                    SystemClock.sleep(45);
                } while (iLongConnectionService == null);

                if (iLongConnectionService != null) {
                    try {
                        iLongConnectionService.setServerParams(host, port);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                cst = (int)(System.currentTimeMillis() - cs);
                LLT_LOG = String.format("[LLT_%08d] CST: %dms", serverConnectCnt + 1, cst);
                logFileWrite(LLT_LOG, true);

                if (serverConnectCnt == 0) {
                    serverConnectTimeMin = serverConnectTimeMax = cst;
                } else {
                    serverConnectTimeMin = (cst < serverConnectTimeMin) ? cst : serverConnectTimeMin;
                    serverConnectTimeMax = (cst > serverConnectTimeMax) ? cst : serverConnectTimeMax;
                }
                serverConnectTimeTotal += cst;
                serverConnectCnt++;
                connectServerTotalCnt++;
                connectServerSuccessCnt++;
                sendMsgForRefreshUI(0, /*serverConnectCnt*/0);
                logFileWrite(SPLIT_LINE, false);

                llt_handler.postDelayed(llt_runnable_new, interval);

                while (!recvThreadExit) {
                    SystemClock.sleep(interval);
                }

                logFileWrite(SPLIT_LINE, false);
                llt_AvgRecordWrite_new(1);

                logFileWrite(SPLIT_LINE, false);

                int sdt = 0;
                long sd = System.currentTimeMillis();

                do {
                    SystemClock.sleep(35);
                } while (iLongConnectionService == null);

                if (iLongConnectionService != null) {
                    try {
                        iLongConnectionService.setServiceStatus(false);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                disconnectServerTotalCnt++;
                disconnectServerSuccessCnt++;
                sdt = (int)(System.currentTimeMillis() - sd);
                if (serverDisconnectCnt == 0) {
                    serverDisconnectTimeMin = serverDisconnectTimeMax = sdt;
                } else {
                    serverDisconnectTimeMin = (sdt < serverDisconnectTimeMin) ? sdt : serverDisconnectTimeMin;
                    serverDisconnectTimeMax = (sdt > serverDisconnectTimeMax) ? sdt : serverDisconnectTimeMax;
                }
                serverDisconnectTimeTotal += sdt;
                serverDisconnectCnt++;
                /* if (sdt > 0) {
                    LLT_LOG = String.format("[LLT_%08d] DST: %dms", serverDisconnectCnt, sdt);
                } else {
                    LLT_LOG = String.format("[LLT_%08d] DST: TMOUT", serverDisconnectCnt);
                } */
                LLT_LOG = String.format("[LLT_%08d] DST: %dms", serverDisconnectCnt, sdt);
                logFileWrite(LLT_LOG, true);

                llt_destroyDatalink_new();
                logFileWrite(SPLIT_LINE, false);
                llt_AvgRecordWrite_new(1);
                //logFileClose();
            } else {
                sendMsgForRefreshUI(1, sendSuccessCnt);
            }
            logFileClose();
            onLongConnectionServiceStop();
            isLongLongTesting = false;
        }

        private void llt_destroyDatalink_new() {
            logFileWrite(SPLIT_LINE, false);
            int tt = 0;
            do {
                tt = closeDatalink();
                datalinkDestroyTotalCnt++;
                if (datalinkDestroyCnt == 0) {
                    datalinkDestroyTimeMin = datalinkDestroyTimeMax = tt;
                } else {
                    datalinkDestroyTimeMin = (tt < datalinkDestroyTimeMin) ? tt : datalinkDestroyTimeMin;
                    datalinkDestroyTimeMax = (tt > datalinkDestroyTimeMax) ? tt : datalinkDestroyTimeMax;
                }
                datalinkDestroyTimeTotal += tt;
                datalinkDestroyCnt++;
                if (tt < DATALINK_SETUP_TIMEOUT) {
                    datalinkDestroySuccessCnt++;
                    LLT_LOG = String.format("[LLT_%08d] DNT: %dms", datalinkDestroyCnt, tt);
                } else {
                    LLT_LOG = String.format("[LLT_%08d] DNT: TMOUT", datalinkDestroyCnt);
                }
                logFileWrite(LLT_LOG, true);
                runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
            } while (tt == DATALINK_SETUP_TIMEOUT && ((cycles > 0 && datalinkDestroyCnt < cycles) || (runningTimeInSeconds >= 0 && runningTimeInSeconds < 60 * times)));

            sendMsgForRefreshUI(1, sendSuccessCnt);
        }
    };

	Runnable llt_networkTask = new Runnable() {
		String LLT_LOG = null;

		@Override
		public void run() {
			// TODO Auto-generated method stub
			boolean isConnected = false;

			if (TextUtils.isEmpty(host)) {
				sendMsgForRefreshUI(1, 0);
				return;
			}

			initResult();
			logFileCreate();
			readIndex = 0;
			reconnectCount = 0;
			sendSuccessCnt = recvSuccessCnt = 0;
			datalinkSetupCnt = datalinkDestroyCnt = serverConnectCnt = serverDisconnectCnt = 0;
			datalinkSetupTimeTotal = datalinkDestroyTimeTotal = serverConnectTimeTotal = serverDisconnectTimeTotal = sendToRecvTimeTotal = 0;
			serverConnectTimeMin = serverConnectTimeMax = 0;
			datalinkSetupTimeMin = datalinkSetupTimeMax = 0;
			sendToRecvTimeMin = sendToRecvTimeMax = 0;
			serverDisconnectTimeMin = serverDisconnectTimeMax = 0;
			datalinkDestroyTimeMin = datalinkDestroyTimeMax = 0;
			llt_li = 0;
			sendPackageTime = new long[LLT_SENDTIME_MAX];
			recvThreadExit = false; 
			content = new byte[sendBytes];
			sendBytesInit(content, (byte)0x7E, (byte)0x55);

			int t = 0;
			do { 
				t = openDatalink();
				if (datalinkSetupCnt == 0) {
					datalinkSetupTimeMin = datalinkSetupTimeMax = t;
				} else {
					datalinkSetupTimeMin = (t < datalinkSetupTimeMin) ? t : datalinkSetupTimeMin;
					datalinkSetupTimeMax = (t > datalinkSetupTimeMax) ? t : datalinkSetupTimeMax;
				}
				datalinkSetupTimeTotal += t;
				datalinkSetupCnt++;
				datalinkSetupTotalCnt++;
				if (t == DATALINK_SETUP_TIMEOUT) {
					LLT_LOG = String.format("[LLT_%08d] ENT: TMOUT", datalinkSetupCnt);
				} else {
					datalinkSetupSuccessCnt++;
					LLT_LOG = String.format("[LLT_%08d] ENT: %dms", datalinkSetupCnt, t);
				}
				logFileWrite(LLT_LOG, true);
				sendMsgForRefreshUI(0, /*datalinkSetupCnt*/0);
                runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
			} while (t == DATALINK_SETUP_TIMEOUT && ((cycles > 0 && datalinkSetupCnt < cycles) || (runningTimeInSeconds >= 0 && runningTimeInSeconds < 60 * times)));

			if (t < DATALINK_SETUP_TIMEOUT) {
				logFileWrite(SPLIT_LINE, false);
				if (isIPAddress == 0) {
					host = getIPV4AddressFromDomain(host);
					isIPAddress = 1;
				}

				isConnected = false;
				do {
					try {
						int cst = 0;
						long cs = System.currentTimeMillis();
						socket = new Socket();
						SocketAddress socAddress = new InetSocketAddress(InetAddress.getByName(host), port);
						try {
							socket.connect(socAddress, SOCKET_CONNECT_TIMEOUT);
						} catch (SocketTimeoutException ste) {
							// TODO Auto-generated catch block
							ste.printStackTrace();
							socket.close();
							cst = SOCKET_CONNECT_TIMEOUT;
							Log.i(TAG, "SocketTimeoutException then close datalink");
							//continue;
						} catch (SocketException se) {
							// TODO Auto-generated catch block
							se.printStackTrace();
							socket.close();
							cst = SOCKET_CONNECT_TIMEOUT;
							Log.i(TAG, "SocketTimeoutException then close datalink");
							//continue;
						} catch (UnknownHostException uhe) {
							// TODO Auto-generated catch block
							uhe.printStackTrace();
							socket.close();
							cst = SOCKET_CONNECT_TIMEOUT;
							Log.i(TAG, "UnknownHostException then close datalink");
							//continue;
						}

						//if (cst != SOCKET_CONNECT_TIMEOUT) {
							isConnected = true;
							connectServerSuccessCnt++;
							socket.setSoLinger(true, 60);
							socket.setTcpNoDelay(true);
							socket.setReuseAddress(true);
							socket.setSoTimeout(SOCKET_RECWAIT_TIMEOUT);
							socket.setSendBufferSize(10240);
							socket.setReceiveBufferSize(10240);
							cst = (int)(System.currentTimeMillis() - cs);
							LLT_LOG = String.format("[LLT_%08d] CST: %dms", serverConnectCnt + 1, cst);
						//} else {
						//	LLT_LOG = String.format("[LLT_%08d] CST: TMOUT", serverConnectCnt + 1);
						//}
						logFileWrite(LLT_LOG, true);

						if (serverConnectCnt == 0) {
							serverConnectTimeMin = serverConnectTimeMax = cst;
						} else {
							serverConnectTimeMin = (cst < serverConnectTimeMin) ? cst : serverConnectTimeMin;
							serverConnectTimeMax = (cst > serverConnectTimeMax) ? cst : serverConnectTimeMax;
						}
						serverConnectTimeTotal += cst;
						serverConnectCnt++;
						connectServerTotalCnt++;

						if (isConnected) {
							dis = new DataInputStream(socket.getInputStream());
							dos = new DataOutputStream(socket.getOutputStream());
						}
					} catch (IOException ex) {
						ex.printStackTrace();
						//continue;
					}
					
					sendMsgForRefreshUI(0, /*serverConnectCnt*/0);
                    runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
			    } while (isConnected == false && ((cycles > 0 && serverConnectCnt < cycles) || (runningTimeInSeconds >= 0 && runningTimeInSeconds < 60 * times)));
				
				if (isConnected == false) {
					llt_destroyDatalink();
					return;
				} else {
					llt_handler.postDelayed(llt_runnable, 0/*interval*/);
				}

				logFileWrite(SPLIT_LINE, false);
				recvThread = new Thread(new Runnable() {
					@Override
					public void run() {
						int lastLeftBytes = 0;
						byte[] readBuffer = new byte[10240];
						int recvLeft = 0;
						byte[] recvBytes = new byte[sendBytes];

						while (!recvThreadExit) {
							if (!socket.isClosed() && socket.isConnected()) {
								try {
									int readSize = dis.read(readBuffer);
									if (readSize <= 0) {
										continue;
									}
									readIndex++;
									int recvPackage = (lastLeftBytes + readSize) / sendBytes;
									if (recvPackage > 0) {
										long rt = System.currentTimeMillis();
										int pkg = 0;
										int srcpos = 0;
										do {
											if (sendBytes > recvLeft && srcpos < readSize && srcpos >= 0 && recvLeft >= 0) {
												System.arraycopy(readBuffer, srcpos, recvBytes, recvLeft, sendBytes - recvLeft);
												srcpos += (sendBytes - recvLeft);
											}

											int idx = getIndexFromRecvBytes(recvBytes);
											int srt = 0;
											if (idx >= 0 && idx < LLT_SENDTIME_MAX) {
												srt = (int)(rt - sendPackageTime[idx % LLT_SENDTIME_MAX]);
												if (recvSuccessCnt == 0) {
													sendToRecvTimeMin = sendToRecvTimeMax = srt;
												} else {
													sendToRecvTimeMax = (srt > sendToRecvTimeMax) ? srt : sendToRecvTimeMax;
													sendToRecvTimeMin = (srt < sendToRecvTimeMin) ? srt : sendToRecvTimeMin;
												}
												sendToRecvTimeTotal += srt;
												recvSuccessCnt++;
												if (srt <= RECV_TIME_MAX) {recvOntimeCnt++;}
											}
											recvLeft = 0;
											pkg++;
											recvPackage--;
											testCaseSuccessCnt = recvSuccessCnt;
											if (srt > 0) {
												LLT_LOG = String.format("[LLT_%08d] SRT: %dms", recvSuccessCnt, srt);
											}/* else {
												LLT_LOG = String.format("[LLT_%08d] SRT: TMOUT", recvSuccessCnt);
											}*/
											logFileWrite(LLT_LOG, true);
											if (recvSuccessCnt > 0 && (recvSuccessCnt % TEST_FOR_AVGCNT == 0)) {
												llt_AvgRecordWrite(0);
											}
										} while (recvPackage > 0);
									}
									sendMsgForRefreshUI(0, sendSuccessCnt);
									int tmpLastLeft = lastLeftBytes;
									sendToRecvSuccessCnt += (lastLeftBytes + readSize) / sendBytes;
									sendToRecvSuccessCnt = testCaseSuccessCnt; // some idx was wrong!!!
									lastLeftBytes = (lastLeftBytes + readSize) % sendBytes;
									recvLeft = lastLeftBytes;
									if (recvLeft > 0) {
										int srcPos = 0, dstPos = 0, cpSize = 0;
										if (readSize + tmpLastLeft < sendBytes) {
											srcPos = 0;
											dstPos = tmpLastLeft;
											cpSize = readSize;
										} else {
											srcPos = readSize - recvLeft;
											dstPos = 0;
											cpSize = recvLeft;
										}
										if (srcPos >= 0 && dstPos >= 0 && cpSize > 0) {
											System.arraycopy(readBuffer, srcPos, recvBytes, dstPos, cpSize);
										}
									}

                                    runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
                                    if ((cycles > 0 && recvSuccessCnt >= cycles) || (times > 0 && runningTimeInSeconds >= 60 * times)) {
										recvThreadExit = true;
									}
								} catch (SocketTimeoutException ste) {
									// TODO Auto-generated catch block
									ste.printStackTrace();
									//llt_ReconnectServer();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									//llt_ReconnectServer();
								}
							} else {
								//llt_ReconnectServer();
								SystemClock.sleep(interval);
							}
						}
						llt_AvgRecordWrite(1);
						llt_cleanupForTestFinish();
						logFileClose();
					}

					private void llt_ReconnectServer() {
						// TODO Auto-generated method stub
						boolean isConnected = false;

						do {
							try {
								int cst = 0;
								long cs = System.currentTimeMillis();
								socket = new Socket();
								SocketAddress socAddress = new InetSocketAddress(InetAddress.getByName(host), port);
								try {
									socket.connect(socAddress, SOCKET_CONNECT_TIMEOUT);
								} catch (SocketTimeoutException ste) {
									// TODO Auto-generated catch block
									ste.printStackTrace();
									socket.close();
									cst = SOCKET_CONNECT_TIMEOUT;
									Log.i(TAG, "llt_ReconnectServer SocketTimeoutException then close datalink");
									//continue;
								} catch (SocketException se) {
									// TODO Auto-generated catch block
									se.printStackTrace();
									socket.close();
									cst = SOCKET_CONNECT_TIMEOUT;
									Log.i(TAG, "llt_ReconnectServer SocketTimeoutException then close datalink");
									//continue;
								} catch (UnknownHostException uhe) {
									// TODO Auto-generated catch block
									uhe.printStackTrace();
									socket.close();
									cst = SOCKET_CONNECT_TIMEOUT;
									Log.i(TAG, "UnknownHostException then close datalink");
									//continue;
								}

								if (cst != SOCKET_CONNECT_TIMEOUT) {
									isConnected = true;
									connectServerSuccessCnt++;
									socket.setSoLinger(true, 60);
									socket.setTcpNoDelay(true);
									socket.setReuseAddress(true);
									socket.setSoTimeout(SOCKET_RECWAIT_TIMEOUT);
									socket.setSendBufferSize(10240);
									socket.setReceiveBufferSize(10240);
									cst = (int)(System.currentTimeMillis() - cs);
									LLT_LOG = String.format("[LLT_%08d] CST: %dms", serverConnectCnt + 1, cst);
								} else {
									LLT_LOG = String.format("[LLT_%08d] CST: TMOUT", serverConnectCnt + 1);
								}

								if (serverConnectCnt == 0) {
									serverConnectTimeMin = serverConnectTimeMax = cst;
								} else {
									serverConnectTimeMin = (cst < serverConnectTimeMin) ? cst : serverConnectTimeMin;
									serverConnectTimeMax = (cst > serverConnectTimeMax) ? cst : serverConnectTimeMax;
								}
								serverConnectTimeTotal += cst;
								serverConnectCnt++;
								connectServerTotalCnt++;

								if (isConnected) {
									dis = new DataInputStream(socket.getInputStream());
									dos = new DataOutputStream(socket.getOutputStream());
									logFileWrite(SPLIT_LINE, false);
									llt_handler.postDelayed(llt_runnable, interval);
								}
							} catch (IOException ex) {
								ex.printStackTrace();
								//continue;
							}
							logFileWrite(LLT_LOG, true);
							sendMsgForRefreshUI(0, sendSuccessCnt);
                            runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
			            } while (isConnected == false && ((cycles > 0 && serverConnectCnt < cycles) || (runningTimeInSeconds >= 0 && runningTimeInSeconds < 60 * times)));
						
						if (isConnected == false) {
							recvThreadExit = true;
						} else {
							llt_handler.postDelayed(llt_runnable, interval);
						}
					}
				});
				recvThread.start();
			} else {
				sendMsgForRefreshUI(1, sendSuccessCnt);
			}
		}

		private void llt_cleanupForTestFinish() {
			// TODO Auto-generated method stub
			String LLT_LOG = null;
			
			logFileWrite(SPLIT_LINE, false);
			int sdt = 0;
			long sd = System.currentTimeMillis();
			try {
				socket.close();
				disconnectServerSuccessCnt++;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			disconnectServerTotalCnt++;
			sdt = (int)(System.currentTimeMillis() - sd);
			if (serverDisconnectCnt == 0) {
				serverDisconnectTimeMin = serverDisconnectTimeMax = sdt;
			} else {
				serverDisconnectTimeMin = (sdt < serverDisconnectTimeMin) ? sdt : serverDisconnectTimeMin;
				serverDisconnectTimeMax = (sdt > serverDisconnectTimeMax) ? sdt : serverDisconnectTimeMax;
			}
			serverDisconnectTimeTotal += sdt;
			serverDisconnectCnt++;
			/* if (sdt > 0) {
				LLT_LOG = String.format("[LLT_%08d] DST: %dms", serverDisconnectCnt, sdt);
			} else {
				LLT_LOG = String.format("[LLT_%08d] DST: TMOUT", serverDisconnectCnt);
			} */
			LLT_LOG = String.format("[LLT_%08d] DST: %dms", serverDisconnectCnt, sdt);
			logFileWrite(LLT_LOG, true);
			llt_destroyDatalink();
		}
		
		private void llt_destroyDatalink() {
			logFileWrite(SPLIT_LINE, false);
			int tt = 0;
			do {
				tt = closeDatalink();
				datalinkDestroyTotalCnt++;
				if (datalinkDestroyCnt == 0) {
					datalinkDestroyTimeMin = datalinkDestroyTimeMax = tt;
				} else {
					datalinkDestroyTimeMin = (tt < datalinkDestroyTimeMin) ? tt : datalinkDestroyTimeMin;
					datalinkDestroyTimeMax = (tt > datalinkDestroyTimeMax) ? tt : datalinkDestroyTimeMax;
				}
				datalinkDestroyTimeTotal += tt;
				datalinkDestroyCnt++;
				if (tt < DATALINK_SETUP_TIMEOUT) {
					datalinkDestroySuccessCnt++;
					LLT_LOG = String.format("[LLT_%08d] DNT: %dms", datalinkDestroyCnt, tt);
				} else {
					LLT_LOG = String.format("[LLT_%08d] DNT: TMOUT", datalinkDestroyCnt);
				}
				logFileWrite(LLT_LOG, true);
                runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
			} while (tt == DATALINK_SETUP_TIMEOUT && ((cycles > 0 && datalinkDestroyCnt < cycles) || (runningTimeInSeconds >= 0 && runningTimeInSeconds < 60 * times)));

			sendMsgForRefreshUI(1, sendSuccessCnt);
		}
		
		private void llt_AvgRecordWrite(int flag) {
			// TODO Auto-generated method stub
			if (flag == 1 && (/*cycles*/recvSuccessCnt % TEST_FOR_AVGCNT == 0)) return;

			String avgRecord = null;
			int idx = (flag == 0 ) ? (recvSuccessCnt / TEST_FOR_AVGCNT) : (/*cycles*/recvSuccessCnt / TEST_FOR_AVGCNT + 1);

			avgRecord = String.format("[LLT_AVG_%04d] ENT: %d-%d-%.2f", idx, datalinkSetupTimeMin, datalinkSetupTimeMax, (datalinkSetupCnt > 0) ? (float)datalinkSetupTimeTotal / datalinkSetupCnt : 0.00);
			avgRecord = avgRecord + String.format(", CST: %d-%d-%.2f", serverConnectTimeMin, serverConnectTimeMax, (serverConnectCnt > 0) ? (float)serverConnectTimeTotal / serverConnectCnt : 0.00);
			avgRecord = avgRecord + String.format(", SRT: %d-%d-%.2f", sendToRecvTimeMin, sendToRecvTimeMax, (recvSuccessCnt > 0) ? (float)sendToRecvTimeTotal / recvSuccessCnt : 0.00);
			avgRecord = avgRecord + String.format(", DST: %d-%d-%.2f", serverDisconnectTimeMin, serverDisconnectTimeMax, (serverDisconnectCnt > 0) ? (float)serverDisconnectTimeTotal / serverDisconnectCnt : 0.00);
			avgRecord = avgRecord + String.format(", \n%42cDNT: %d-%d-%.2f", ' ', datalinkDestroyTimeMin, datalinkDestroyTimeMax, (datalinkDestroyCnt > 0)? (float)datalinkDestroyTimeTotal / datalinkDestroyCnt : 0.00);
			avgRecord = avgRecord + String.format(", Success: %d, Failure: %d [%.2f%%], Ontime = %.2f%%.", testCaseSuccessCnt, (testCaseTotalCnt - testCaseSuccessCnt), ((testCaseTotalCnt > 0) ? 100.00 * testCaseSuccessCnt / testCaseTotalCnt : 0.00), ((testCaseSuccessCnt > 0) ? 100.00 * recvOntimeCnt / testCaseSuccessCnt : 0.00));

			logFileWrite(avgRecord, false);
		}
	};
	
	private void sendMsgForRefreshUI(int idx, int cnt) {
		// TODO Auto-generated method stub
        Message message = new Message();
        message.what = idx;
        message.arg1 = cnt;
        ui_handler.sendMessage(message);
	}
	
    @SuppressLint("HandlerLeak")
	final Handler ui_handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
	            case 0:
	            case 1:
	            	refreshTestResult(msg.what, msg.arg1);
	                break;
	            default:
	            	break;
            }
            super.handleMessage(msg);
        }
        
        void refreshTestResult(int idx, int cnt) {
        	testEndTime = getCurrentTimeString();
            
        	TextView tvMin1 = (TextView) findViewById(R.id.tv_min1);
        	tvMin1.setText(String.valueOf(datalinkSetupTimeMin));
        	TextView tvMax1 = (TextView) findViewById(R.id.tv_max1);
        	tvMax1.setText(String.valueOf(datalinkSetupTimeMax));
        	TextView tvAvg1 = (TextView) findViewById(R.id.tv_avg1);
        	if (datalinkSetupCnt > 0) {
        		tvAvg1.setText(String.format("%.2f", (float)datalinkSetupTimeTotal / datalinkSetupCnt));
        	} else {
        		tvAvg1.setText(R.string.tv_default_avg_value);
        	}
        	TextView tvSuccess1 = (TextView) findViewById(R.id.tv_success1);
        	tvSuccess1.setText(String.valueOf(datalinkSetupSuccessCnt));
        	TextView tvFailure1 = (TextView) findViewById(R.id.tv_failure1);
        	tvFailure1.setText(String.valueOf(datalinkSetupTotalCnt - datalinkSetupSuccessCnt));
            
        	TextView tvMin2 = (TextView) findViewById(R.id.tv_min2);
        	tvMin2.setText(String.valueOf(serverConnectTimeMin));
        	TextView tvMax2 = (TextView) findViewById(R.id.tv_max2);
        	tvMax2.setText(String.valueOf(serverConnectTimeMax));
        	TextView tvAvg2 = (TextView) findViewById(R.id.tv_avg2);
        	if (serverConnectCnt > 0) {
        		tvAvg2.setText(String.format("%.2f", (float)serverConnectTimeTotal / serverConnectCnt));
        	} else {
        		tvAvg2.setText(R.string.tv_default_avg_value);
        	}
        	TextView tvSuccess2 = (TextView) findViewById(R.id.tv_success2);
        	tvSuccess2.setText(String.valueOf(connectServerSuccessCnt));
        	TextView tvFailure2 = (TextView) findViewById(R.id.tv_failure2);
        	tvFailure2.setText(String.valueOf(connectServerTotalCnt - connectServerSuccessCnt));
        	
        	TextView tvMin3 = (TextView) findViewById(R.id.tv_min3);
        	tvMin3.setText(String.valueOf(sendToRecvTimeMin));
        	TextView tvMax3 = (TextView) findViewById(R.id.tv_max3);
        	tvMax3.setText(String.valueOf(sendToRecvTimeMax));
        	TextView tvAvg3 = (TextView) findViewById(R.id.tv_avg3);
        	if (recvSuccessCnt > 0) {
        		tvAvg3.setText(String.format("%.2f", (float)sendToRecvTimeTotal / recvSuccessCnt));
        	} else {
        		tvAvg3.setText(R.string.tv_default_avg_value);
        	}
        	TextView tvSuccess3 = (TextView) findViewById(R.id.tv_success3);
        	tvSuccess3.setText(String.valueOf(sendToRecvSuccessCnt));
        	TextView tvFailure3 = (TextView) findViewById(R.id.tv_failure3);
        	tvFailure3.setText(String.valueOf(sendToRecvTotalCnt - sendToRecvSuccessCnt));
            
        	TextView tvMin4 = (TextView) findViewById(R.id.tv_min4);
        	tvMin4.setText(String.valueOf(serverDisconnectTimeMin));
        	TextView tvMax4 = (TextView) findViewById(R.id.tv_max4);
        	tvMax4.setText(String.valueOf(serverDisconnectTimeMax));
        	TextView tvAvg4 = (TextView) findViewById(R.id.tv_avg4);
        	if (serverDisconnectCnt > 0) {
        		tvAvg4.setText(String.format("%.2f", (float)serverDisconnectTimeTotal / serverDisconnectCnt));
        	} else {
        		tvAvg4.setText(R.string.tv_default_avg_value);
        	}
        	TextView tvSuccess4 = (TextView) findViewById(R.id.tv_success4);
        	tvSuccess4.setText(String.valueOf(disconnectServerSuccessCnt));
        	TextView tvFailure4 = (TextView) findViewById(R.id.tv_failure4);
        	tvFailure4.setText(String.valueOf(disconnectServerTotalCnt - disconnectServerSuccessCnt));
        	
        	TextView tvMin5 = (TextView) findViewById(R.id.tv_min5);
        	tvMin5.setText(String.valueOf(datalinkDestroyTimeMin));
        	TextView tvMax5 = (TextView) findViewById(R.id.tv_max5);
        	tvMax5.setText(String.valueOf(datalinkDestroyTimeMax));
        	TextView tvAvg5 = (TextView) findViewById(R.id.tv_avg5);
        	if (datalinkDestroyCnt > 0) {
        		tvAvg5.setText(String.format("%.2f", (float)datalinkDestroyTimeTotal / datalinkDestroyCnt));
        	} else {
        		tvAvg5.setText(R.string.tv_default_avg_value);
        	}
        	TextView tvSuccess5 = (TextView) findViewById(R.id.tv_success5);
        	tvSuccess5.setText(String.valueOf(datalinkDestroySuccessCnt));
        	TextView tvFailure5 = (TextView) findViewById(R.id.tv_failure5);
        	tvFailure5.setText(String.valueOf(datalinkDestroyTotalCnt - datalinkDestroySuccessCnt));
        	
        	TextView tvSuccessCnt = (TextView) findViewById(R.id.tv_test_success_value);
        	tvSuccessCnt.setText(String.valueOf(testCaseSuccessCnt) + " 次");
        	
        	TextView tvFailureCnt = (TextView) findViewById(R.id.tv_test_failure_value);
        	tvFailureCnt.setText(String.valueOf(testCaseTotalCnt - testCaseSuccessCnt) + " 次");
        	
        	TextView tvSuccessRatio = (TextView) findViewById(R.id.tv_ratio_value);
        	if (testCaseTotalCnt > 0) {
        		tvSuccessRatio.setText(String.format("%.2f", 100.00 * testCaseSuccessCnt / testCaseTotalCnt) + "%");
        	} else {
        		tvSuccessRatio.setText(R.string.tv_default_ratio_value);
        	}
        	
        	TextView tvReconnectCnt = (TextView) findViewById(R.id.tv_reconnect_value);
        	tvReconnectCnt.setText(String.valueOf(reconnectCount) + " 次");
        	
        	TextView tvTestContent = (TextView) findViewById(R.id.tv_content_value);
        	String casestr = String.format("  -- 【CASE %03d】", testCaseAdapter.getCount() - caseidx);
        	if (eProtocolType == 0) {
        		tvTestContent.setText(mContext.getResources().getString((item == 0) ? R.string.sp_test_for_long_short : 
	        						 ((item == 1) ? R.string.sp_test_for_long_long : 
	        					     ((item == 2) ? R.string.sp_test_for_short_short : R.string.sp_test_for_datalink_setup)))
	        						 + casestr);
        	} else {
        		tvTestContent.setText(mContext.getResources().getString(R.string.sp_test_for_udp_loss) + casestr);
        	}
        	
        	TextView tvTestTime = (TextView) findViewById(R.id.tv_time_value);
        	if (idx == 0) {
	        	tvTestTime.setTextColor(mContext.getResources().getColor(R.color.yellow));
	        	tvTestTime.setText(testStartTime + "  --  " + testEndTime + " 【正在测试】 ");
        	} else {
	        	tvTestTime.setTextColor(mContext.getResources().getColor(R.color.def_text));
	        	tvTestTime.setText(testStartTime + "  --  " + testEndTime);
        	}
        	
        	TextView tvServerAddress = (TextView) findViewById(R.id.tv_server_value);
        	tvServerAddress.setText(ghost + ":" + port);
        	
        	TextView tvSendBytes = (TextView) findViewById(R.id.tv_bytes_value);
        	tvSendBytes.setText(String.valueOf(sendBytes) + " 字节");
        	
        	TextView tvTestCycles = (TextView) findViewById(R.id.tv_cycles_value);
            if (cycles > 0) {
                tvTestCycles.setText(String.valueOf(cnt) + "/" + String.valueOf(cycles) + " 次");
            } else {
                tvTestCycles.setText(String.valueOf(cnt) + " 次");
            }
        	
        	TextView tvSendInterval = (TextView) findViewById(R.id.tv_interval_value);
        	tvSendInterval.setText(String.valueOf(interval) + " 毫秒");
        	
        	setButtonStatus((idx > 0) ? true : false);
        }
    };
	
	private void initTestResult() {
    	TextView tvMin1 = (TextView) findViewById(R.id.tv_min1);
    	tvMin1.setText(R.string.tv_default_value);
    	TextView tvMax1 = (TextView) findViewById(R.id.tv_max1);
    	tvMax1.setText(R.string.tv_default_value);
    	TextView tvAvg1 = (TextView) findViewById(R.id.tv_avg1);
    	tvAvg1.setText(R.string.tv_default_avg_value);
    	TextView tvSuccess1 = (TextView) findViewById(R.id.tv_success1);
    	tvSuccess1.setText(R.string.tv_default_value);
    	TextView tvFailure1 = (TextView) findViewById(R.id.tv_failure1);
    	tvFailure1.setText(R.string.tv_default_value);
    	
    	TextView tvMin2 = (TextView) findViewById(R.id.tv_min2);
    	tvMin2.setText(R.string.tv_default_value);
    	TextView tvMax2 = (TextView) findViewById(R.id.tv_max2);
    	tvMax2.setText(R.string.tv_default_value);
    	TextView tvAvg2 = (TextView) findViewById(R.id.tv_avg2);
    	tvAvg2.setText(R.string.tv_default_avg_value);
    	TextView tvSuccess2 = (TextView) findViewById(R.id.tv_success2);
    	tvSuccess2.setText(R.string.tv_default_value);
    	TextView tvFailure2 = (TextView) findViewById(R.id.tv_failure2);
    	tvFailure2.setText(R.string.tv_default_value);
    	
    	TextView tvMin3 = (TextView) findViewById(R.id.tv_min3);
    	tvMin3.setText(R.string.tv_default_value);
    	TextView tvMax3 = (TextView) findViewById(R.id.tv_max3);
    	tvMax3.setText(R.string.tv_default_value);
    	TextView tvAvg3 = (TextView) findViewById(R.id.tv_avg3);
    	tvAvg3.setText(R.string.tv_default_avg_value);
    	TextView tvSuccess3 = (TextView) findViewById(R.id.tv_success3);
    	tvSuccess3.setText(R.string.tv_default_value);
    	TextView tvFailure3 = (TextView) findViewById(R.id.tv_failure3);
    	tvFailure3.setText(R.string.tv_default_value);
    	
    	TextView tvMin4 = (TextView) findViewById(R.id.tv_min4);
    	tvMin4.setText(R.string.tv_default_value);
    	TextView tvMax4 = (TextView) findViewById(R.id.tv_max4);
    	tvMax4.setText(R.string.tv_default_value);
    	TextView tvAvg4 = (TextView) findViewById(R.id.tv_avg4);
    	tvAvg4.setText(R.string.tv_default_avg_value);
    	TextView tvSuccess4 = (TextView) findViewById(R.id.tv_success4);
    	tvSuccess4.setText(R.string.tv_default_value);
    	TextView tvFailure4 = (TextView) findViewById(R.id.tv_failure4);
    	tvFailure4.setText(R.string.tv_default_value);
    	
    	TextView tvMin5 = (TextView) findViewById(R.id.tv_min5);
    	tvMin5.setText(R.string.tv_default_value);
    	TextView tvMax5 = (TextView) findViewById(R.id.tv_max5);
    	tvMax5.setText(R.string.tv_default_value);
    	TextView tvAvg5 = (TextView) findViewById(R.id.tv_avg5);
    	tvAvg5.setText(R.string.tv_default_avg_value);
    	TextView tvSuccess5 = (TextView) findViewById(R.id.tv_success5);
    	tvSuccess5.setText(R.string.tv_default_value);
    	TextView tvFailure5 = (TextView) findViewById(R.id.tv_failure5);
    	tvFailure5.setText(R.string.tv_default_value);
    	
    	TextView tvSuccessCnt = (TextView) findViewById(R.id.tv_test_success_value);
    	tvSuccessCnt.setText(R.string.tv_default_times_value);
    	
    	TextView tvFailureCnt = (TextView) findViewById(R.id.tv_test_failure_value);
    	tvFailureCnt.setText(R.string.tv_default_times_value);
    	
    	TextView tvSuccessRatio = (TextView) findViewById(R.id.tv_ratio_value);
    	tvSuccessRatio.setText(R.string.tv_default_ratio_value);
    	
    	TextView tvReconnectCnt = (TextView) findViewById(R.id.tv_reconnect_value);
    	tvReconnectCnt.setText(R.string.tv_default_times_value);
    	
    	TextView tvTestContent = (TextView) findViewById(R.id.tv_content_value);
    	String casestr = String.format("  -- 【CASE %03d】", testCaseAdapter.getCount() - caseidx);
    	if (eProtocolType == 0) {
    		tvTestContent.setText(mContext.getResources().getString((item == 0) ? R.string.sp_test_for_long_short : 
        						 ((item == 1) ? R.string.sp_test_for_long_long : 
        					     ((item == 2) ? R.string.sp_test_for_short_short : R.string.sp_test_for_datalink_setup)))
        						 + casestr);
    	} else {
    		tvTestContent.setText(mContext.getResources().getString(R.string.sp_test_for_udp_loss) + casestr);
    	}
    	
    	TextView tvTestTime = (TextView) findViewById(R.id.tv_time_value);
    	tvTestTime.setTextColor(this.getResources().getColor(R.color.yellow));
    	tvTestTime.setText(testStartTime + "  --  " + " 【正在测试】 ");
    	
    	TextView tvServerAddress = (TextView) findViewById(R.id.tv_server_value);
    	tvServerAddress.setText(ghost + ":" + port);
    	
    	TextView tvSendBytes = (TextView) findViewById(R.id.tv_bytes_value);
    	tvSendBytes.setText(String.valueOf(sendBytes) + " 字节");
    	
    	TextView tvTestCycles = (TextView) findViewById(R.id.tv_cycles_value);
        if (cycles > 0) {
            tvTestCycles.setText("0/" + String.valueOf(cycles) + " 次");
        } else {
            tvTestCycles.setText("0 次");
        }
    	
    	TextView tvSendInterval = (TextView) findViewById(R.id.tv_interval_value);
    	tvSendInterval.setText(String.valueOf(interval) + " 毫秒");
    	
		setButtonStatus(false);
    }
	
    Handler llt_handler = new Handler();
    Runnable llt_runnable = new Runnable() {
        @Override
        public void run() {
            runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
            if ((cycles > 0 && sendSuccessCnt >= cycles) || (times > 0 && runningTimeInSeconds >= 60 * times)) {
				llt_handler.removeCallbacks(this);
				//SystemClock.sleep(10 * 1000);
				recvThreadExit = true;
			} else {
				try {
					if (!socket.isClosed()) {
						if (socket.isConnected()) {
							if (!socket.isOutputShutdown()) {
								llt_handler.postDelayed(this, interval);
								sendPackageTime[llt_li % LLT_SENDTIME_MAX] = System.currentTimeMillis();
								sendBytesIndexSet(content, llt_li % LLT_SENDTIME_MAX);
								llt_li++;
								dos.write(content, 0, content.length);
								testCaseTotalCnt++;
								sendSuccessCnt++;
								sendToRecvTotalCnt++;
								sendMsgForRefreshUI(0, sendSuccessCnt);
								return;
							}
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				llt_handler.removeCallbacks(this);
			}
        }
    };

	public void testForShortShort(TestCaseConfigs tcc) {
		// TODO Auto-generated method stub
		refreshGlobalTestCaseConfigs(tcc);
		testStartTime = getCurrentTimeString();
		startTimeInSeconds = getCurrentTimeInSeconds();
		initTestResult();
		
		new Thread(sst_networkTask).start();
	}

	Runnable sst_networkTask = new Runnable() {
		String SST_LOG = null;
		
	    @Override  
	    public void run() {
	    	// TODO Auto-generated method stub
	    	boolean isConnected = false;
	    	
	    	if (TextUtils.isEmpty(host)) {
	    		sendMsgForRefreshUI(1, 0);
	    		return;
	    	}
            
	    	initResult();
	    	logFileCreate();
            readIndex = 0;
            reconnectCount = 0;
            sendSuccessCnt = recvSuccessCnt = 0;
            datalinkSetupCnt = datalinkDestroyCnt = serverConnectCnt = serverDisconnectCnt = 0;
            datalinkSetupTimeTotal = datalinkDestroyTimeTotal = serverConnectTimeTotal = serverDisconnectTimeTotal = sendToRecvTimeTotal = 0;
			serverConnectTimeMin = serverConnectTimeMax = 0;
			datalinkSetupTimeMin = datalinkSetupTimeMax = 0;
			sendToRecvTimeMin = sendToRecvTimeMax = 0;
            serverDisconnectTimeMin = serverDisconnectTimeMax = 0;
            datalinkDestroyTimeMin = datalinkDestroyTimeMax = 0;
            llt_li = 0;
            sendPackageTime = new long[LLT_SENDTIME_MAX];
            recvThreadExit = false;
            content = new byte[sendBytes];
            sendBytesInit(content, (byte)0x7E, (byte)0x55);
            byte[] readBuffer = new byte[10240];
	    	
        	while (!recvThreadExit) {
    			int t = openDatalink();
	            if (datalinkSetupCnt == 0) {
	            	datalinkSetupTimeMin = datalinkSetupTimeMax = t;
	            } else {
	            	datalinkSetupTimeMin = (t < datalinkSetupTimeMin) ? t : datalinkSetupTimeMin;
					datalinkSetupTimeMax = (t > datalinkSetupTimeMax) ? t : datalinkSetupTimeMax;
	            }
	            datalinkSetupTimeTotal += t;
	            datalinkSetupCnt++;
	            datalinkSetupTotalCnt++;
	            testCaseTotalCnt++;
    		    if (t < DATALINK_SETUP_TIMEOUT) {
    		    	SST_LOG = String.format("[SST_%08d] ENT: %dms", datalinkSetupCnt, t);
    		        datalinkSetupSuccessCnt++;
    	    		if (isIPAddress == 0) {
    	    			host = getIPV4AddressFromDomain(host);
    	    			isIPAddress = 1;
    	    		}
			        try {
			        	int cst = SOCKET_CONNECT_TIMEOUT;
			        	long cs = System.currentTimeMillis();
			        	isConnected = false;
			            socket = new Socket();
			            SocketAddress socAddress = new InetSocketAddress(InetAddress.getByName(host), port);
                        do {
                            try {
                                socket.connect(socAddress, SOCKET_CONNECT_TIMEOUT);
                                cst = 0;
                                break;
                            } catch (SocketTimeoutException ste) {
                                // TODO Auto-generated catch block
                                ste.printStackTrace();
                                socket.close();
                                //cst = SOCKET_CONNECT_TIMEOUT;
                                continue;
                            } catch (SocketException se) {
                                // TODO Auto-generated catch block
                                se.printStackTrace();
                                socket.close();
                                //cst = SOCKET_CONNECT_TIMEOUT;
                                continue;
                            } catch (UnknownHostException uhe) {
                                // TODO Auto-generated catch block
                                uhe.printStackTrace();
                                socket.close();
                                //cst = SOCKET_CONNECT_TIMEOUT;
                                Log.i(TAG, "UnknownHostException then close datalink");
                                continue;
                            }
                        } while (System.currentTimeMillis() < cs + SOCKET_CONNECT_TIMEOUT);
			            
			            if (cst != SOCKET_CONNECT_TIMEOUT) {
			            	isConnected = true;
				            connectServerSuccessCnt++;
				            socket.setSoLinger(true, 60);
				            socket.setTcpNoDelay(true);
				            socket.setReuseAddress(true);
				            socket.setSoTimeout(SOCKET_RECWAIT_TIMEOUT);
				            socket.setSendBufferSize(10240);
				            socket.setReceiveBufferSize(10240);
			            	cst = (int)(System.currentTimeMillis() - cs);
			            	SST_LOG = SST_LOG + String.format(", CST: %dms", cst);
			            } else {
			            	SST_LOG = SST_LOG + String.format(", CST: TMOUT");
			            }
			            
			            if (serverConnectCnt == 0) {
			            	serverConnectTimeMin = serverConnectTimeMax = cst;
			            } else {
							serverConnectTimeMin = (cst < serverConnectTimeMin) ? cst : serverConnectTimeMin;
							serverConnectTimeMax = (cst > serverConnectTimeMax) ? cst : serverConnectTimeMax;
			            }
					    serverConnectTimeTotal += cst;
					    serverConnectCnt++;
					    connectServerTotalCnt++;

					    if (isConnected) {
				            dis = new DataInputStream(socket.getInputStream());
				            dos = new DataOutputStream(socket.getOutputStream());
			                if (!socket.isClosed()) {
				                if (socket.isConnected()) {
				                    if (!socket.isOutputShutdown()) {
							        	int srt = 0;
							        	long sr = System.currentTimeMillis();
				                    	dos.write(content, 0, content.length);
				                        sendSuccessCnt++;
				                        sendToRecvTotalCnt++;
				                        
				            	    	int lastLeftBytes = 0;
					                    do {
					                        try {
						                        int readSize = dis.read(readBuffer);
						                        if (readSize <= 0) {
						                        	break;
						                        }
						                        lastLeftBytes += readSize;
						                        if (lastLeftBytes >= sendBytes) {
						                        	srt = (int)(System.currentTimeMillis() - sr);
						                        	if (recvSuccessCnt == 0) {
						                        		sendToRecvTimeMin = sendToRecvTimeMax = srt;
						                        	} else {
							                        	sendToRecvTimeMin = (srt < sendToRecvTimeMin) ? srt : sendToRecvTimeMin;
							                        	sendToRecvTimeMax = (srt > sendToRecvTimeMax) ? srt : sendToRecvTimeMax;
						                        	}
						                        	sendToRecvTimeTotal += srt;
						                        	recvSuccessCnt++;
						                            sendToRecvSuccessCnt++;
						                            if (srt <= RECV_TIME_MAX) {recvOntimeCnt++;}
						                        }
						                        readIndex++;
						                        Log.i(TAG, "RECV: " + readIndex + ", readSize: " + readSize + ", recvSuccessCnt: " + recvSuccessCnt);
						                    } catch (SocketTimeoutException ste) {
												// TODO Auto-generated catch block
						                    	ste.printStackTrace();
						                    	break;
						                    } catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												break;
											}
					                    } while (lastLeftBytes < sendBytes);
					                    if (srt > 0) {
					                    	SST_LOG = SST_LOG + String.format(", SRT: %dms", srt);
					                    } else {
					                    	SST_LOG = SST_LOG + String.format(", SRT: TMOUT");
					                    }
				                    }
				                }
			                }
			                
					        int sdt = 0;
							long sd = System.currentTimeMillis();
							try {
								socket.close();
								disconnectServerSuccessCnt++;
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							disconnectServerTotalCnt++;
							sdt = (int)(System.currentTimeMillis() - sd);
							if (serverDisconnectCnt == 0) {
								serverDisconnectTimeMin = serverDisconnectTimeMax = sdt;
							} else {
								serverDisconnectTimeMin = (sdt < serverDisconnectTimeMin) ? sdt : serverDisconnectTimeMin;
								serverDisconnectTimeMax = (sdt > serverDisconnectTimeMax) ? sdt : serverDisconnectTimeMax;
							}
						    serverDisconnectTimeTotal += sdt;
						    serverDisconnectCnt++;
		                    /* if (sdt > 0) {
		                    	SST_LOG = SST_LOG + String.format(", DST: %dms", sdt);
		                    } else {
		                    	SST_LOG = SST_LOG + String.format(", DST: TMOUT");
		                    } */
						    SST_LOG = SST_LOG + String.format(", DST: %dms", sdt);
					    }
			        } catch (IOException ex) {
			            ex.printStackTrace();
						//continue;
			        }
			        
	    		    int tt = closeDatalink();
	    		    datalinkDestroyTotalCnt++;
		            if (datalinkDestroyCnt == 0) {
		            	datalinkDestroyTimeMin = datalinkDestroyTimeMax = tt;
		            } else {
		            	datalinkDestroyTimeMin = (tt < datalinkDestroyTimeMin) ? tt : datalinkDestroyTimeMin;
		            	datalinkDestroyTimeMax = (tt > datalinkDestroyTimeMax) ? tt : datalinkDestroyTimeMax;
		            }
		            datalinkDestroyTimeTotal += tt;
		            datalinkDestroyCnt++;
		            if (tt < DATALINK_SETUP_TIMEOUT) {
		                datalinkDestroySuccessCnt++;
		                SST_LOG = SST_LOG + String.format(", DNT: %dms", tt);
		            } else {
	                    SST_LOG = SST_LOG + String.format(", DNT: TMOUT");
		            }
		            testCaseSuccessCnt = GET_SUCCESS_CNT();
        		} else {
        			SST_LOG = String.format("[SST_%08d] ENT: TMOUT", datalinkSetupCnt);
        			closeDatalink();
        			SystemClock.sleep(10 * 1000);
        		}
    		    
	            logFileWrite(SST_LOG, true);
	            if (datalinkSetupCnt > 0 && (datalinkSetupCnt % TEST_FOR_AVGCNT == 0)) {
	            	sst_AvgRecordWrite(0);
	            }
	            sendMsgForRefreshUI(0, datalinkSetupCnt);

                runningTimeInSeconds = getCurrentTimeInSeconds() - startTimeInSeconds;
                if ((cycles > 0 && datalinkSetupCnt >= cycles) || (times > 0 && runningTimeInSeconds >= 60 * times)) {
            		recvThreadExit = true;
                } else {
                	SystemClock.sleep(((interval < SST_TIME_INTERVAL_MIN) ? SST_TIME_INTERVAL_MIN : interval));
                }
        	}
        	sst_AvgRecordWrite(1);
        	sendMsgForRefreshUI(1, datalinkSetupCnt);
        	logFileClose();
	    }

		private void sst_AvgRecordWrite(int flag) {
			// TODO Auto-generated method stub
			if (flag == 1 && (/*cycles*/datalinkSetupCnt % TEST_FOR_AVGCNT == 0)) return;
			
			String avgRecord = null;
			int idx = (flag == 0 ) ? (datalinkSetupCnt / TEST_FOR_AVGCNT) : (/*cycles*/datalinkSetupCnt / TEST_FOR_AVGCNT + 1);
			
			avgRecord = String.format("[SST_AVG_%04d] ENT: %d-%d-%.2f", idx, datalinkSetupTimeMin, datalinkSetupTimeMax, (datalinkSetupCnt > 0) ? (float)datalinkSetupTimeTotal / datalinkSetupCnt : 0.00);
			avgRecord = avgRecord + String.format(", CST: %d-%d-%.2f", serverConnectTimeMin, serverConnectTimeMax, (serverConnectCnt > 0) ? (float)serverConnectTimeTotal / serverConnectCnt : 0.00);
			avgRecord = avgRecord + String.format(", SRT: %d-%d-%.2f", sendToRecvTimeMin, sendToRecvTimeMax, (recvSuccessCnt > 0) ? (float)sendToRecvTimeTotal / recvSuccessCnt : 0.00);
			avgRecord = avgRecord + String.format(", DST: %d-%d-%.2f", serverDisconnectTimeMin, serverDisconnectTimeMax, (serverDisconnectCnt > 0) ? (float)serverDisconnectTimeTotal / serverDisconnectCnt : 0.00);
			avgRecord = avgRecord + String.format(", \n%42cDNT: %d-%d-%.2f", ' ', datalinkDestroyTimeMin, datalinkDestroyTimeMax, (datalinkDestroyCnt > 0) ? (float)datalinkDestroyTimeTotal / datalinkDestroyCnt : 0.00);
			avgRecord = avgRecord + String.format(", Success: %d, Failure: %d [%.2f%%], Ontime = %.2f%%.", testCaseSuccessCnt, (testCaseTotalCnt - testCaseSuccessCnt), ((testCaseTotalCnt > 0) ? 100.00 * testCaseSuccessCnt / testCaseTotalCnt : 0.00), ((testCaseSuccessCnt > 0) ? 100.00 * recvOntimeCnt / testCaseSuccessCnt : 0.00));
        	
			logFileWrite(avgRecord, false);
		}
	};
	
	public String getIPV4AddressFromDomain(String domain) {
	    String IPV4Address = "";
	    InetAddress ia = null;  
	    try {
	        ia = java.net.InetAddress.getByName(domain);
	        IPV4Address = ia.getHostAddress();
	    } catch (UnknownHostException e) {
	        // TODO Auto-generated catch block  
	        e.printStackTrace();
	        return IPV4Address;  
	    }
	    return IPV4Address;
	}

	protected int GET_SUCCESS_CNT() {
		// TODO Auto-generated method stub
		int min = datalinkSetupSuccessCnt;
		min = (min < connectServerSuccessCnt) ? min : connectServerSuccessCnt;
		min = (min < sendToRecvSuccessCnt) ? min : sendToRecvSuccessCnt;
		min = (min < disconnectServerSuccessCnt) ? min : disconnectServerSuccessCnt;
		min = (min < datalinkDestroySuccessCnt) ? min : datalinkDestroySuccessCnt;
		
		return min;
	}

	private void networkInit() {
		// TODO Auto-generated method stub
		boolean isEnable = false;
		
		if ((isEnable = isAirplaneModeOn())) {
			toggleAirplaneMode(!isEnable);
		}
		
		if (isWifiEnable()) {
			toggleWiFi(false);
		}
		if (isDatalinkEnable()) {
			toggleDatalink(false);
		}
	}
	
	private int closeDatalink() {
		// TODO Auto-generated method stub
		long starttime = 0;
		
		useWifiAsDefaultNetwork = (eNetType == 0) ? true : false;
		if (useWifiAsDefaultNetwork) {
			//if (isWifiEnable()) {
				starttime = System.currentTimeMillis();
				toggleWiFi(false);
				while(isWifiConnected()) {
					if (System.currentTimeMillis() - starttime >= DATALINK_SETUP_TIMEOUT) {
						return DATALINK_SETUP_TIMEOUT;
					}
				}
			//}
		} else {
			//if (isDatalinkEnable()) {
				starttime = System.currentTimeMillis();
				toggleDatalink(false);
				while(isDatalinkConnected()) {
					if (System.currentTimeMillis() - starttime >= DATALINK_SETUP_TIMEOUT) {
						return DATALINK_SETUP_TIMEOUT;
					}
				}
			//}
		}
		return (int)(System.currentTimeMillis() - starttime);
	}

	private int openDatalink() {
		// TODO Auto-generated method stub
		long starttime = 0;
		
		useWifiAsDefaultNetwork = (eNetType == 0) ? true : false;
		if (useWifiAsDefaultNetwork) {
			//if (!isWifiEnable()) {
				starttime = System.currentTimeMillis();
				toggleWiFi(true);
				while(!isWifiConnected()) {
					if (System.currentTimeMillis() - starttime >= DATALINK_SETUP_TIMEOUT) {
						return DATALINK_SETUP_TIMEOUT;
					}
					SystemClock.sleep(1000);
				}
			//}
		} else {
			//if (!isDatalinkEnable()) {
				starttime = System.currentTimeMillis();
				toggleDatalink(true);
				while(!isDatalinkConnected()) {
					if (System.currentTimeMillis() - starttime >= DATALINK_SETUP_TIMEOUT) {
						return DATALINK_SETUP_TIMEOUT;
					}
					SystemClock.sleep(1000);
				}
			//}
		}
		return (int)(System.currentTimeMillis() - starttime);
	}
	
	public boolean isWifiEnable() {
		// TODO Auto-generated method stub
		WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
		
		return wifiManager.isWifiEnabled();
	}

    public boolean toggleWiFi(boolean enabled) {  
        WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE); 
        
        return wifiManager.setWifiEnabled(enabled);
    }
	
	public boolean isDatalinkEnable() {
	    TelephonyManager telephonyService = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
	    try {
	        Method getMobileDataEnabledMethod = telephonyService.getClass().getDeclaredMethod("getDataEnabled");
	        if (null != getMobileDataEnabledMethod) {
	            boolean mobileDataEnabled = (Boolean) getMobileDataEnabledMethod.invoke(telephonyService);
	            return mobileDataEnabled;
	        }
	    } catch (Exception e) {
	        Log.v(TAG, "Error getting" + ((InvocationTargetException)e).getTargetException() + telephonyService);
	    }
	    return false;
	}
	
	public void toggleDatalink(boolean enabled) {
	    TelephonyManager telephonyService = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
	    try {
	        Method setMobileDataEnabledMethod = telephonyService.getClass().getDeclaredMethod("setDataEnabled", boolean.class);
	        if (null != setMobileDataEnabledMethod) {
	            setMobileDataEnabledMethod.invoke(telephonyService, enabled);
	        }
	    } catch (Exception e) {
	        Log.v(TAG, "Error setting" + ((InvocationTargetException)e).getTargetException() + telephonyService);
	    }
	}
	
    @SuppressWarnings("deprecation")
	public boolean isAirplaneModeOn() {
        int modeIdx = Settings.System.getInt(mContext.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0);    
        boolean isEnabled = (modeIdx == 1);  
        return isEnabled;  
    }    

    @SuppressWarnings("deprecation")
	public void toggleAirplaneMode(boolean setAirPlane) {    
        Settings.System.putInt(mContext.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, setAirPlane ? 1 : 0);
        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intent.putExtra("state", setAirPlane);    
        mContext.sendBroadcast(intent);    
    }

    public boolean isNetworkConnected() {  
    	ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkinfo = connManager.getActiveNetworkInfo();  

        if (networkinfo != null) {  
            return networkinfo.isConnected();  
        }
        return false;  
    }  

    public boolean isWifiConnected() {  
    	ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);  
  
        if (mWifi != null) {  
            return mWifi.isConnected();  
        }
        return false;  
    }  
  
    public boolean isDatalinkConnected() {  
    	ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);  
        NetworkInfo mMobile = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);  
  
        if (mMobile != null) {  
            return mMobile.isConnected();  
        }
        return false;  
    }
    
    private void sendBytesInit(byte[] b, byte tag, byte val) {
		// TODO Auto-generated method stub
    	for(int i = 0; i < b.length; i++) {
    		b[i] = (byte)val;
    	}
    	b[0] = b[b.length - 1] = tag;
	}
    
    private void sendBytesIndexSet(byte[] b, int idx) {
		// TODO Auto-generated method stub
    	b[1] = (byte) (idx & 0xff);
    	b[2] = (byte) ((idx >> 8) & 0xff);
    	b[3] = (byte) ((idx >> 16) & 0xff);
    	b[4] = (byte) ((idx >> 24) & 0xff);
	}
    
    private int getIndexFromRecvBytes(byte[] b) {
    	int ret = 0xFFFFFFFF;
    	
    	if ((b[0] & 0xff) == (byte)0x7E) {
    		ret = (b[1] & 0xff) | ((b[2] << 8) & 0xff00) | ((b[3] << 16) & 0xff0000) | ((b[4] << 24) & 0xff000000);
    	}
    	return ret;
	}
    
	private void refreshGlobalTestCaseConfigs(TestCaseConfigs tcc) {
		// TODO Auto-generated method stub
		item = tcc.itemContent;
		ghost = host = tcc.serverAddr;
		port = tcc.serverPort;
		cycles = tcc.testCycles;
		times = tcc.testTimes;
		interval = tcc.sendInterval;
		sendBytes = tcc.sendBytes;
		isIPAddress = tcc.isIPAddress;
	}

	private long getCurrentTimeInSeconds() {
        long t = System.currentTimeMillis();

        return (long) (t / 1000);
    }
	
	@SuppressWarnings("deprecation")
	@SuppressLint("SimpleDateFormat")
	private String getCurrentTimeString() {  
        long t = System.currentTimeMillis();
        String ts = null;
        Date d = new Date(t);
        //if (getWindowManager().getDefaultDisplay().getHeight() == 600) {
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS\"");
        	ts = sdf.format(d);
        //} else {
        //	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS\"");
        //	ts = sdf.format(d);
        //}
        
        return ts;
    }
	
	private void setButtonStatus(boolean status) {
		Button btn = null;
		
		btn = (Button)findViewById(R.id.btn_create);
		btn.setEnabled(status);
		btn = (Button)findViewById(R.id.btn_export);
		btn.setEnabled(status);
		btn = (Button)findViewById(R.id.btn_screenshot);
		btn.setEnabled(status);	
		btn = (Button)findViewById(R.id.btn_exit);
		btn.setEnabled(status);
		
		ListView lv = (ListView)findViewById(R.id.lv_test_case);
		for (int i = 0; i < lv.getChildCount(); i++) {
			LinearLayout lo = (LinearLayout)lv.getChildAt(i);
			btn = (Button)lo.findViewById(R.id.btn_test_case);
			btn.setEnabled(status);
		}
    }

	private String getStringOfSignalStrength() {
		StringBuffer ss = new StringBuffer();
		
		useWifiAsDefaultNetwork = (eNetType == 0) ? true : false;
		if (useWifiAsDefaultNetwork) {
			WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);  
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			ss.append("WiFi-RSSI: ").append(String.valueOf(wifiInfo.getRssi())).append("dBm");
		} else {
			ss.append(stringOfsignalStrengths);
		}
		
		return ss.toString();
    }
}
