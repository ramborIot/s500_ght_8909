package com.fibocom.tcptool;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
 
public class CustomAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private String [] mStringArray;
    public CustomAdapter(Context context, String[] stringArray) {
        super(context, R.layout.simple_spinner_item, stringArray);
        mContext = context;
        mStringArray = stringArray;
    }
 
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.simple_spinner_dropdown_item, parent,false);
        }
 
        TextView tv = (TextView) convertView.findViewById(R.id.text1);
        tv.setText(mStringArray[position]);
        tv.setTextSize(14f);
        tv.setTextColor(mContext.getResources().getColor(R.color.sky_blue));
        return convertView;
 
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.simple_spinner_item, parent, false);
        }
 
        TextView tv = (TextView) convertView.findViewById(R.id.text1);
        tv.setText(mStringArray[position]);
        tv.setTextSize(14f);
        tv.setTextColor(mContext.getResources().getColor(R.color.sky_blue));
        return convertView;
    } 
}