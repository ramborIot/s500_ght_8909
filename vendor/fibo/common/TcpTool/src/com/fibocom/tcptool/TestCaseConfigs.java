package com.fibocom.tcptool;

public class TestCaseConfigs {
	public String caseSignature = null; 		//特征码
	public int isIPAddress = 0;					//域名或IP，取值0-1
	public int itemContent = 0; 				//测试内容，取值0-2
	public int netType = 0;						//网络类型，取值0-1
	public String serverAddr = null;			//服务器地址
	public int serverPort = 0;					//服务器端口 (0， 65535)
	public int protocolType = 0;				//协议类型，取值0-1
	public int sendBytes = 0;					//单次发包字节 > 0
	public int testCycles = 0;					//循环测试次数 > 0
	public int testTimes = 0;					//循环测试时间，testCycles == 0 时使用 testTimes
	public int sendInterval = 0;				//循环发送间隔
}
