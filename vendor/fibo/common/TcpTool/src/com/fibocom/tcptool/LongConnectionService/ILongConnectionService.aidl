package com.fibocom.tcptool.LongConnectionService;

interface ILongConnectionService {
    void setServerParams(in String ip, in int port);
    void setServiceStatus(in boolean status);
	boolean sendMessage(in String message);
    boolean sendMessageInBytes(in byte[] message, in int size);
}