#!/bin/bash
TOP=$(pwd)

# Android
rm -rf $TOP/LINUX/android

#begin build Android:please customize
cd ../
./build.sh la0920 -j32
#end build Android:please customize

if [ $? -ne 0 ];then		
    echo building android failed!
    cd $TOP
    exit 1
fi

ln -s  $(pwd)  $TOP/LINUX/android
		
# Updating NON-HLOS.bin
cd $TOP/common/build/
python update_common_info.py

if [ $? -ne 0 ];then		
    echo building NON-HLOS failed!
    cd $TOP
    exit 1
fi
		
rm -rf $TOP/LINUX/android
cd $TOP/

