#!/bin/bash
TOP=../../../../../..

mkdir -p ./qmss8909/
cp ./compile_all.sh ./qmss8909/
cp ./copy_file.sh ./qmss8909/
cp ./sign_compile_all.sh ./qmss8909/
cp -rf  $TOP/contents.xml ./qmss8909/

#create boot_images dir
mkdir -p ./qmss8909/boot_images/build/ms/bin/8909/emmc/
cp -rf $TOP/boot_images/build/ms/bin/8909/emmc/* ./qmss8909/boot_images/build/ms/bin/8909/emmc/

mkdir -p ./qmss8909/boot_images/core/storage/tools/fattool/
cp -rf $TOP/boot_images/core/storage/tools/fattool/* ./qmss8909/boot_images/core/storage/tools/fattool/

mkdir -p ./qmss8909/boot_images/core/storage/tools/ptool/
cp -rf $TOP/boot_images/core/storage/tools/ptool/* ./qmss8909/boot_images/core/storage/tools/ptool/

#create common dir
mkdir -p ./qmss8909/common
cp -rf $TOP/common/* ./qmss8909/common
rm -rf ./qmss8909/common/build/bin
rm -rf ./qmss8909/common/build/*.log
rm -rf ./qmss8909/common/build/*.bin
rm -rf ./qmss8909/common/build/partition_SC800_XGD.xml
rm -rf ./qmss8909/common/build/partition_SC806_NLP.xml
rm -rf ./qmss8909/common/build/partition_SC800_CN02_XGD.xml
rm -rf ./qmss8909/common/build/patch0.xml
rm -rf ./qmss8909/common/build/rawprogram0*.xml
rm -rf ./qmss8909/common/build/wipe_rawprogram_PHY*.xml
rm -rf ./qmss8909/common/tools/meta/*.pyc
rm -rf ./qmss8909/common/tools/cmm/gen/gen_buildflavor.cmm
rm -rf ./qmss8909/common/tools/sectools/qsecureboot-fibocom.sh

#create cust dir
#mkdir -p ./qmss8909/cust
#cp -rf $TOP/cust/* ./qmss8909/cust

#create cust dir
mkdir -p ./qmss8909/flashtools
cp -rf $TOP/flashtools/* ./qmss8909/flashtools
rm -rf ./qmss8909/flashtools/SC800_XGD
rm -rf ./qmss8909/flashtools/SC806_NLP
rm -rf ./qmss8909/flashtools/SC806_SOCSI
rm -rf ./qmss8909/flashtools/NV
rm -rf ./qmss8909/flashtools/erase_update*.bat
#create LINUX dir
mkdir ./qmss8909/LINUX

#create modem_proc dir
mkdir -p ./qmss8909/modem_proc/build/ms/bin/8909.genns.prod/
cp -rf $TOP/modem_proc/build/ms/bin/8909.genns.prod/* ./qmss8909/modem_proc/build/ms/bin/8909.genns.prod/

mkdir -p ./qmss8909/modem_proc/mcfg/configs/mcfg_sw/generic/
cp -rf $TOP/modem_proc/mcfg/configs/mcfg_sw/generic/* ./qmss8909/modem_proc/mcfg/configs/mcfg_sw/generic/

#create rpm_proc dir
mkdir -p ./qmss8909/rpm_proc/build/ms/bin/8909/pm8909/
cp -rf $TOP/rpm_proc/build/ms/bin/8909/pm8909/*  ./qmss8909/rpm_proc/build/ms/bin/8909/pm8909/

mkdir -p ./qmss8909/rpm_proc/core/power/rpm/debug/scripts/
cp -rf $TOP/rpm_proc/core/power/rpm/debug/scripts/*  ./qmss8909/rpm_proc/core/power/rpm/debug/scripts/

#create trustzone_images dir
mkdir -p ./qmss8909/trustzone_images/build/ms/bin/MAZAANAA/
cp -rf $TOP/trustzone_images/build/ms/bin/MAZAANAA/* ./qmss8909/trustzone_images/build/ms/bin/MAZAANAA/

#create wcnss_proc dir
mkdir -p ./qmss8909/wcnss_proc/build/ms/bin/SCAQMAZ/reloc
cp -rf $TOP/wcnss_proc/build/ms/bin/SCAQMAZ/reloc/* ./qmss8909/wcnss_proc/build/ms/bin/SCAQMAZ/reloc

#for debug symbol
mkdir symbol
#cp -rf $TOP/LINUX/android/out/target/product/$PROJECT/obj/KERNEL_OBJ/vmlinux  ./symbol
cp -rf $TOP/modem_proc/build/ms/orig_MODEM_PROC_IMG_8909.genns.prodQ.elf*  ./symbol
cp -rf $TOP/modem_proc/build/ms/M89098909.genns.prodQ0000.elf  ./symbol
cp -rf $TOP/modem_proc/build/bsp/mapss_b/build/8909.genns.prod/MODEM_PROC_IMG_8909.genns.prodQ.elf   ./symbol
cp -rf $TOP/modem_proc/build/bsp/mapss_b/build/8909.genns.prod/MODEM_PROC_IMG_8909.genns.prodQ_patch.elf   ./symbol
cp -rf $TOP/modem_proc/core/bsp/mba_img/build/8909.genns.prod/MBA_CORE_IMG_8909.genns.prodQ.elf   ./symbol
cp -rf $TOP/boot_images/core/bsp/bootloaders/sbl1/build/DAASANAZ/SBL1_emmc.elf   ./symbol
cp -rf $TOP/trustzone_images/core/bsp/tzbsp/build/MAZAANAA/tz.elf  ./symbol
cp -rf $TOP/rpm_proc/core/bsp/rpm/build/8909/pm8909/RPM_AAAAANAAR.elf  ./symbol
cp -rf $TOP/wcnss_proc/build/ms/8909_PRONTO_SCAQMAZM.elf  ./symbol
#cp -rf $TOP/LINUX/android/out/target/product/$PROJECT/obj/EMMC_BOOTLOADER_OBJ/build-msm8909/lk  ./symbol
cp -rf $TOP/modem_proc/build/myps/* ./symbol
