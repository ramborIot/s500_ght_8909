#!/bin/bash
TOP=$(pwd)

# Android
rm -rf $TOP/LINUX/android

#begin build Android:please customize
cd ../
./build.sh la0920 -j32
#end build Android:please customize

if [ $? -ne 0 ];then		
    echo building android failed!
    cd $TOP
    exit 1
fi

#for sign bin
ln -s  $(pwd)  $TOP/LINUX/android
cd $TOP/common/tools/sectools
./qsecureboot.sh

if [ $? -ne 0 ];then		
    echo building secureboot failed!
    cd $TOP
    exit 1
fi

#begin build Android system image:please customize
rm -rf $TOP/LINUX/android
cd $TOP/../
./build.sh la0920 -j32 -i sysimg
#end build Android system image:please customize
if [ $? -ne 0 ];then		
    echo building android system image failed!
    cd $TOP
    exit 1
fi

cp $TOP/common/tools/sectools/oem_sec_images/8909/appsbl/emmc_appsboot.mbn out/target/product/la0920/emmc_appsboot.mbn

# Updating NON-HLOS.bin
ln -s  $(pwd)  $TOP/LINUX/android
cd $TOP/common/build/
python update_common_info.py
if [ $? -ne 0 ];then		
    echo building NON-HLOS failed!
    cd $TOP
    exit 1
fi

rm -rf $TOP/LINUX/android
cd $TOP/


