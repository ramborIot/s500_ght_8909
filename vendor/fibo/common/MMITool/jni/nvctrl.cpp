#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h> 
#include <android/log.h>

#include "nv.h"
#include "diagcmd.h"

#define XLOG_TAG                        "nvctrl"
#define XLOGI(...)                      ( (void) __android_log_print(ANDROID_LOG_INFO, XLOG_TAG, __VA_ARGS__) )
#define XLOGW(...)                      ( (void) __android_log_print(ANDROID_LOG_WARN, XLOG_TAG, __VA_ARGS__) )
#define XLOGD(...)                      ( (void) __android_log_print(ANDROID_LOG_DEBUG, XLOG_TAG, __VA_ARGS__) )
#define XLOGE(...)                      ( (void) __android_log_print(ANDROID_LOG_ERROR, XLOG_TAG, __VA_ARGS__) )

#define SUCCESS                         ( 0 )
#define FAILED                          ( -1 )

#define PASS                            ( 1 )
#define FAIL                            ( -1 )
#define UNKNOWN                         ( 0 )

#define NV_QCSN_CONFIG_I				( 5291 )

#define CHAR_PTR(var)                   ( (unsigned char *) &(var) )

typedef int (*nvctrl_t)(nv_items_enum_type, unsigned char *, int);
static void *handle = NULL;
static nvctrl_t nvread = NULL;
static nvctrl_t nvwrite = NULL;


static int nvctrl_read(nv_items_enum_type item,     /*!< Which NV item to read */
                       unsigned char *data_ptr,     /*!< buffer pointer to put the read data */
                       int len) {                   /*!< size of read buffer */
    unsigned char reply_buf[len + 3];
    int retry = 0;

    if (nvread) {
        do {
            memset(reply_buf, 0, sizeof(reply_buf));
            nvread(item, reply_buf, sizeof(reply_buf));
            if (reply_buf[0] == DIAG_NV_READ_F
                    && reply_buf[1] == CHAR_PTR(item)[0]
                    && reply_buf[2] == CHAR_PTR(item)[1]) {
                memcpy(data_ptr, &reply_buf[3], len);
                return SUCCESS;
            }
            usleep(100 * 1000);
        } while (++retry < 10);
    }
    return FAILED;
}

static int nvctrl_write(nv_items_enum_type item,    /*!< Which NV item to write */
                        unsigned char *data_ptr,    /*!< buffer pointer pointing to the data to be written */
                        int len) {                  /*!< size of write data */
    int ret = 0;
	if (nvwrite) {
        ret = nvwrite(item, data_ptr, len);
		XLOGI("nvwrite ret = %d.\n", ret);
		return  ret;
    }
	XLOGI("nvwrite failed.\n");
    return FAILED;
}

extern "C" JNIEXPORT jstring JNICALL Java_com_fibocom_hwtest_HardwareTestApplication_getSN(JNIEnv *env, jclass thiz) {
    int ret = 0;
    const char *sn = "UNKNOWN";
    unsigned char nv_read[128] = {0};

    XLOGI("%s Starting...", __FUNCTION__);
    ret = nvctrl_read(NV_FACTORY_DATA_1_I, nv_read, sizeof(nv_read));
    if (SUCCESS == ret) {
        XLOGI("NV SN: %s.\n", nv_read);
        if (strlen((const char *)nv_read) > 0) {
            sn = (const char *)nv_read;
        }
    }
    return env->NewStringUTF(sn);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_fibocom_hwtest_HardwareTestApplication_getIMEI(JNIEnv *env, jclass thiz) {
    int ret = 0;
    const char *imei = "UNKNOWN";
    unsigned char nv_imei[16] = {0};
    unsigned char nv_read[128] = {0};

    XLOGI("%s Starting...", __FUNCTION__);
    ret = nvctrl_read(NV_UE_IMEI_I, nv_read, sizeof(nv_read));
    if (SUCCESS == ret) {
        if (nv_read[0] == 0x08 && ((nv_read[1] & 0x0F) == 0x0A && (nv_read[1] & 0xF0) <= 0x90)) {
            memset(nv_imei, 0, sizeof(nv_imei));
            nv_imei[0] = 0x30 | ((nv_read[1] >> 4) & 0x0F);
            for (int i = 0; i < 7; i++) {
                nv_imei[2 * i + 1] = 0x30 | ((nv_read[i + 2] >> 0) & 0x0F);
                nv_imei[2 * i + 2] = 0x30 | ((nv_read[i + 2] >> 4) & 0x0F);
            }
            XLOGI("NV IMEI: %s.\n", nv_imei);
            if (strlen((const char *)nv_imei) > 0) {
                imei = (const char *)nv_imei;
            }
        }
    }
    return env->NewStringUTF(imei);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_fibocom_hwtest_HardwareTestApplication_getMEID(JNIEnv *env, jclass thiz) {
    int ret = 0;
    const char *meid = "UNKNOWN";
    unsigned char nv_meid[16] = {0};
    unsigned char nv_read[128] = {0};

    XLOGI("%s Starting...", __FUNCTION__);
    ret = nvctrl_read(/*NV_MEID_I*/(nv_items_enum_type)1943, nv_read, sizeof(nv_read));
    if (SUCCESS == ret) {
        memset(nv_meid, 0, sizeof(nv_meid));
        uint64_t *p  = (uint64_t *)nv_read;
        XLOGI("NV MEID: 0x%llx.\n", *p);
        sprintf((char *)nv_meid, "%llx", *p);
        if (strlen((const char *)nv_meid) > 0 && strlen((const char *)nv_meid) < 16) {
            meid = (const char *)nv_meid;
        }
    }
    return env->NewStringUTF(meid);
}

extern "C" JNIEXPORT jint JNICALL Java_com_fibocom_hwtest_HardwareTestApplication_isNvCalibration(JNIEnv *env, jclass thiz) {
    int ret = 0;
    unsigned char nv_read[128] = {0};

    XLOGI("%s Starting...", __FUNCTION__);
    ret = nvctrl_read(NV_FACTORY_DATA_3_I, nv_read, sizeof(nv_read));
    if (SUCCESS == ret) {
        XLOGI("NV[33]: %d.\n", nv_read[33]);
        ret = ((nv_read[33] == 'P') ? PASS : ((nv_read[33] == 'F') ? FAIL : UNKNOWN));
        return ret;
    }
    return UNKNOWN;
}

extern "C" JNIEXPORT jint JNICALL Java_com_fibocom_hwtest_HardwareTestApplication_isNvComprehensive(JNIEnv *env, jclass thiz) {
    int ret = 0;
    unsigned char nv_read[128] = {0};

    XLOGI("%s Starting...", __FUNCTION__);
    ret = nvctrl_read(NV_FACTORY_DATA_3_I, nv_read, sizeof(nv_read));
    if (SUCCESS == ret) {
        XLOGI("NV[34]: %d.\n", nv_read[34]);
        ret = ((nv_read[34] == 'P') ? PASS : ((nv_read[34] == 'F') ? FAIL : UNKNOWN));
        return ret;
    }
    return UNKNOWN;
}

extern "C" JNIEXPORT jint JNICALL Java_com_fibocom_hwtest_HardwareTestApplication_setNvTestResult(JNIEnv *env, jclass thiz, jboolean result, jboolean isLandiEdition) {
    int ret = 0;
    unsigned char nv_read[128] = {0};

    XLOGI("%s Starting...", __FUNCTION__);
    ret = nvctrl_read(NV_FACTORY_DATA_3_I, nv_read, sizeof(nv_read));
    if (SUCCESS == ret) {
        nv_read[35] = (result) ? 'P' : 'F';
        if (isLandiEdition) {
            for (int i = 0; i < 7; i++) {
                nv_read[i] = result ? 0xFF : 0x00;
            }
        }
        ret = nvctrl_write(NV_FACTORY_DATA_3_I, nv_read, sizeof(nv_read));
        XLOGI("%s Finished. <ret = %d>", __FUNCTION__, ret);
        return ret;
    }
    XLOGI("%s Finished. <Failed>", __FUNCTION__);
    return FAILED;
}

extern "C" JNIEXPORT jint JNICALL Java_com_fibocom_hwtest_HardwareTestApplication_nvctrlInit(JNIEnv *env, jclass thiz) {
	XLOGI("%s Starting...", __FUNCTION__);
    if (!Diag_LSM_Init(NULL)) {
        XLOGE("fastmmi Test App: Diag_LSM_Init() failed. \n");
        goto err_1;
    }

    XLOGI("fastmmi Test App: Diag_LSM_Init() succeeded. \n");
    register_callback();

    handle = dlopen("/system/vendor/lib/libmmi.so", RTLD_LAZY);
    if (!handle) {
        XLOGE("Function: %s, Line: %d, NULL == handle. \n", __FUNCTION__, __LINE__);
        goto err_2;
    }

    nvread = (nvctrl_t) dlsym(handle, "_Z12diag_nv_read18nv_items_enum_typePhi"/* "diag_nv_read" */);
    if (!nvread) {
        XLOGE("Function: %s, Line: %d, NULL == nvread. \n", __FUNCTION__, __LINE__);
        goto err_3;
    }
    nvwrite = (nvctrl_t) dlsym(handle, "_Z13diag_nv_write18nv_items_enum_typePhi"/* "diag_nv_write" */);
    if (!nvwrite) {
        XLOGE("Function: %s, Line: %d, NULL == nvwrite. \n", __FUNCTION__, __LINE__);
        goto err_3;
    }
    XLOGI("%s Finished. <Success>", __FUNCTION__);
    return SUCCESS;

err_3:
    if (handle) {
        dlclose(handle);
    }
err_2:
    Diag_LSM_DeInit();
err_1:
    XLOGI("%s Finished. <Failed>", __FUNCTION__);
    return FAILED;
}

extern "C" JNIEXPORT jint JNICALL Java_com_fibocom_hwtest_HardwareTestApplication_nvctrlDeinit(JNIEnv *env, jclass thiz) {
    XLOGI("%s Starting...", __FUNCTION__);
    if (handle) {
        dlclose(handle);
    }
    Diag_LSM_DeInit();
    XLOGI("%s Finished. <Success>", __FUNCTION__);
    return SUCCESS;
}
