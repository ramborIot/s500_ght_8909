package com.fibocom.hwtest.items;

import java.util.Iterator;
import java.util.Set;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.SystemProperties;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fibocom.hwtest.R;

public class FiboBluetoothTest extends FiboBaseTest {
    private static final String TAG = "FiboBluetoothTest";
    protected static final int MSG_SCAN_TIMEOUT = 1;
    protected static final int BT_REFRESH_WAITTIME = 10000;
    private TextView btList = null;
    private int btDevicesCount = 0;
    private StringBuilder btDevicesBuilder = null;
    private BluetoothAdapter bluetoothAdapter = null;
    private FiboResultCallback resultCallback = null;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    Log.i(TAG, "Unbonded: " + device.getAddress().toUpperCase() + "  <=>  " + device.getName());
                    btDevicesBuilder.append("\n【" + device.getAddress().toUpperCase() + "】 <=> 【" + device.getName() + "】");
                    btDevicesCount++;
                    btList.setText("当前搜索到的蓝牙设备个数：" + btDevicesCount + "\n" + btDevicesBuilder.toString());
                    setButtonVisibility(true);
                    resultCallback.setResult(FiboBaseTest.TEST_PASS);
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                if (btDevicesCount == 0) {
                    btList.setText(R.string.bluetooth_scan_list);
                    setButtonVisibility(true);
                    resultCallback.setResult(FiboBaseTest.TEST_FAIL);
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fibo_bluetooth, container, false);
        btList = (TextView) v.findViewById(R.id.devices_list);
        btDevicesBuilder = new StringBuilder("");
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setButtonVisibility(true);
        resultCallback.setResult(FiboBaseTest.TEST_WAIT_PASS);
    }

    @Override
    public void onHandleMessage(final int index) {
        switch (index) {
            case MSG_SCAN_TIMEOUT:
                setButtonVisibility(true);
                resultCallback.setResult((btDevicesCount == 0) ? FiboBaseTest.TEST_FAIL : FiboBaseTest.TEST_PASS);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        btDevicesCount = 0;
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        getActivity().registerReceiver(mReceiver, filter);

        startBluetooth();
        if (bluetoothAdapter != null) {
            if (bluetoothAdapter.isDiscovering()) {
                bluetoothAdapter.cancelDiscovery();
            }
            bluetoothAdapter.startDiscovery();
        }
        setTimerTask(MSG_SCAN_TIMEOUT, BT_REFRESH_WAITTIME);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mReceiver);

        if (bluetoothAdapter != null) {
            if (bluetoothAdapter.isDiscovering()) {
                bluetoothAdapter.cancelDiscovery();
            }
        }
    }

    @Override
    public String getTestName() {
        return getContext().getString(R.string.bluetooth_title);
    }

    @Override
    public boolean isNeedTest() {
        return getSystemProperties("bluetooth", true);
    }

    @Override
    public int getTestResult() {
        return FiboBaseTest.TEST_FAIL;
    }

    @Override
    public int getTestTimeout() {
        return FiboBaseTest.NO_TIMEOUT;
    }

    @Override
    public boolean setTestCallback(FiboResultCallback callback) {
        this.resultCallback = callback;
        return true;
    }

    private void startBluetooth() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null) {
            if (!bluetoothAdapter.isEnabled()) {
                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivity(intent);
            }

            Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();
            if (devices.size() > 0) {
                for (Iterator<BluetoothDevice> iterator = devices.iterator(); iterator.hasNext(); ) {
                    BluetoothDevice device = (BluetoothDevice) iterator.next();
                    Log.i(TAG, "Bonded: " + device.getAddress().toUpperCase() + "  <=>  " + device.getName());
                    btDevicesBuilder.append("\n【" + device.getAddress().toUpperCase() + "】 <=> 【" + device.getName() + "】");
                    btDevicesCount++;
                    btList.setText("当前搜索到的蓝牙设备个数：" + btDevicesCount + "\n" + btDevicesBuilder.toString());
                }
            }

            if (btDevicesCount > 0) {
                setButtonVisibility(true);
                resultCallback.setResult(FiboBaseTest.TEST_PASS);
            }
        }
    }
}
