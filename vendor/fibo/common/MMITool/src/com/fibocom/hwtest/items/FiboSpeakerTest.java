package com.fibocom.hwtest.items;

import java.io.File;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioSystem;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fibocom.hwtest.R;

public class FiboSpeakerTest extends FiboBaseTest implements OnPreparedListener {
    private static final String TAG = "FiboSpeakerTest";
    protected static final int MSG_START = 1;
    protected static final int MSG_STOP  = 2;
    protected static final int MUSIC_PLAY_TIME = 60000;

    private boolean isPlaying = false;
    private MediaPlayer mPlayer = null;
    private AudioManager mAudioManager = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fibo_speaker, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.setMode(AudioManager.MODE_NORMAL);
    }

    @Override
    public void onResume() {
        super.onResume();
        isPlaying = false;
        setTimerTask(MSG_START, 0);
    }

    @Override
    public void onPause() {
        super.onPause();
        stopMusic();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }

    @Override
    public void onHandleMessage(final int index) {
        switch (index) {
            case MSG_START:
                if (mAudioManager.isWiredHeadsetOn()) {
                    setPromptText(R.string.speaker_plug_out_headset);
                    setButtonVisibility(false);
                    break;
                }
                if (!isPlaying) {
                    isPlaying = true;
                    setTimerTask(MSG_STOP, MUSIC_PLAY_TIME);
                    setPromptText(R.string.speaker_desc);
                    setButtonVisibility(true);
                    playMusic(R.raw.soundtest);
                }
                break;
            case MSG_STOP:
                stopMusic();
                break;
        }
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        setTimerTask(MSG_START, 0);
        return true;
    }

    @Override
    public String getTestName() {
        return getContext().getString(R.string.speaker_title);
    }

    @Override
    public boolean isNeedTest() {
        return getSystemProperties("speaker", true);
    }

    @Override
    public int getTestResult() {
        return FiboBaseTest.TEST_INSPECTION;
    }

    @Override
    public int getTestTimeout() {
        return FiboBaseTest.DEF_TIMEOUT;
    }

    @Override
    public boolean setTestCallback(FiboResultCallback callback) {
        return false;
    }

    protected void playMusic(int resId) {
        stopMusic();

        AssetFileDescriptor fd = getResources().openRawResourceFd(resId);
        try {
            mPlayer = new MediaPlayer();
            mPlayer.setDataSource(fd.getFileDescriptor(), fd.getStartOffset(), fd.getLength());
            mPlayer.setOnPreparedListener(this);
            mPlayer.prepare();
        } catch (Exception e) {
            Log.e(TAG, "E:" + e.getMessage());
        }
    }

    protected void playMusic(File file) {
        stopMusic();

        try {
            mPlayer = new MediaPlayer();
            mPlayer.setDataSource(file.getAbsolutePath());
            mPlayer.setOnPreparedListener(this);
            mPlayer.prepare();
        } catch (Exception e) {
            Log.e(TAG, "Music file not found");
        }
    }

    protected void stopMusic() {
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
    }

    private void setPromptText(int resId) {
        ((TextView) getView().findViewById(R.id.prompt_text)).setText(resId);
    }
}
