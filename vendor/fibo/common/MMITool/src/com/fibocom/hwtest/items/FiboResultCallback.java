package com.fibocom.hwtest.items;

public interface FiboResultCallback {
    void setResult(int result);
}