package com.fibocom.hwtest.items;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.SystemProperties;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fibocom.hwtest.R;

public class FiboWifiTest extends FiboBaseTest {
    private static final String TAG = "FiboWifiTest";
    protected static final int MSG_SCAN_TIMEOUT = 1;
    protected static final int MSG_SCAN_IMMEDIATELY = 2;
    protected static final int WIFI_REFRESH_INTERVAL = 2000;
    protected static final int WIFI_STRENGTH_LEVEL_THRESHOLD = -70;
    private TextView wifiList = null;
    private boolean isWifiTestPass = false;
    private int wifiScanFailCount = 0;
    private int wifiHotspotCount = 0;
    private int wifiLevelMax = -100;
    private StringBuffer mWifiHotspotBuffer = null;
    private WifiManager mWifiManager = null;
    private List<ScanResult> mResultList = null;
    private List<ScanResult> mWifiList = null;
    private FiboResultCallback resultCallback = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fibo_wifi, container, false);
        wifiList = (TextView) v.findViewById(R.id.hotspot_list);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setButtonVisibility(true);
        resultCallback.setResult(FiboBaseTest.TEST_WAIT_PASS);
    }

    @Override
    public void onHandleMessage(final int index) {
        switch (index) {
            case MSG_SCAN_TIMEOUT:
                setButtonVisibility(true);
                resultCallback.setResult((wifiHotspotCount > 0 && wifiLevelMax >= WIFI_STRENGTH_LEVEL_THRESHOLD) ? FiboBaseTest.TEST_PASS : FiboBaseTest.TEST_FAIL);
                break;
            case MSG_SCAN_IMMEDIATELY:
                startWifiScan();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isWifiTestPass = false;
        wifiScanFailCount = 0;
        wifiHotspotCount = 0;

        startWifi();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public String getTestName() {
        return getContext().getString(R.string.wifi_title);
    }

    @Override
    public boolean isNeedTest() {
        return getSystemProperties("wifi", true);
    }

    @Override
    public int getTestResult() {
        return FiboBaseTest.TEST_FAIL;
    }

    @Override
    public int getTestTimeout() {
        return FiboBaseTest.NO_TIMEOUT;
    }

    @Override
    public boolean setTestCallback(FiboResultCallback callback) {
        this.resultCallback = callback;
        return true;
    }

    private void startWifi() {
        mWifiManager = (WifiManager) getContext().getSystemService(Context.WIFI_SERVICE);
        if (!mWifiManager.isWifiEnabled()) {
            mWifiManager.setWifiEnabled(true);
        }
        startWifiScan();
    }

    private void startWifiScan() {
        mWifiManager.startScan();
        mResultList = mWifiManager.getScanResults();
        if (mResultList == null) {
            /* 0: 正在关闭; 1: 已经关闭; 2: 正在开启; 3: 已经开启; */
            if (mWifiManager.getWifiState() == 3) {
                Log.i(TAG, "当前区域没有搜索到Wi-Fi热点。");
            } else if (mWifiManager.getWifiState() == 2) {
                Log.i(TAG, "Wi-Fi热点正在开启中，请稍后。");
            } else {
                Log.i(TAG, "测试之前，请先开启Wi-Fi热点。");
            }
            wifiHotspotCount = 0;
            wifiList.setText(R.string.wifi_scan_list);
            wifiScanFailCount++;
            setTimerTask((isWifiTestPass || wifiScanFailCount < 5) ? MSG_SCAN_IMMEDIATELY : MSG_SCAN_TIMEOUT, WIFI_REFRESH_INTERVAL);
        } else {
            wifiLevelMax = -100;
            wifiHotspotCount = 0;
            mWifiHotspotBuffer = new StringBuffer();
            mWifiList = new ArrayList<ScanResult>();
            for (ScanResult result : mResultList) {
                if (result.SSID == null || result.SSID.length() == 0 || result.capabilities.contains("[IBSS]")) {
                    continue;
                }
                boolean isFound = false;
                for (ScanResult item : mWifiList) {
                    if (item.SSID.equals(result.SSID) && item.capabilities.equals(result.capabilities)) {
                        isFound = true;
                        break;
                    }
                }
                if (!isFound) {
                    mWifiList.add(result);
                    mWifiHotspotBuffer = mWifiHotspotBuffer.append("\n【")
                            .append(result.level).append("】 <=> 【")
                            .append(result.BSSID.toUpperCase()).append("】 <=> 【")
                            .append(result.SSID).append("】");
                    wifiHotspotCount++;
                    if (result.level > wifiLevelMax) {
                        wifiLevelMax = result.level;
                    }
                    wifiList.setText("当前搜索到的Wi-Fi热点个数：" + wifiHotspotCount + "\n" + mWifiHotspotBuffer.toString());
                }
            }

            if (wifiHotspotCount > 0 && wifiLevelMax >= WIFI_STRENGTH_LEVEL_THRESHOLD) {
                isWifiTestPass = true;
                setButtonVisibility(true);
                resultCallback.setResult(FiboBaseTest.TEST_PASS);
            }
            wifiScanFailCount = 0;
            setTimerTask(MSG_SCAN_IMMEDIATELY, WIFI_REFRESH_INTERVAL);
        }
    }
}
