package com.fibocom.hwtest;

import android.content.Context;

import com.fibocom.hwtest.items.FiboBaseTest;
import com.fibocom.hwtest.items.FiboSystemVersionTest;
import com.fibocom.hwtest.items.FiboSpeakerTest;
import com.fibocom.hwtest.items.FiboAudioLoopbackTest;
import com.fibocom.hwtest.items.FiboKeyTest;
import com.fibocom.hwtest.items.FiboFrontCameraTest;
import com.fibocom.hwtest.items.FiboBackCameraTest;
import com.fibocom.hwtest.items.FiboUsbMouseTest;
import com.fibocom.hwtest.items.FiboGpsTest;
import com.fibocom.hwtest.items.FiboWifiTest;
import com.fibocom.hwtest.items.FiboBluetoothTest;
import com.fibocom.hwtest.items.FiboSdCardTest;
import com.fibocom.hwtest.items.FiboSimCardTest;
import com.fibocom.hwtest.items.FiboUnknownTest;

public class HardwareTestList {
    private static final FiboBaseTest[] ALL_ITEMS = {
        new FiboSystemVersionTest(),
        new FiboSpeakerTest(),
        new FiboAudioLoopbackTest(),
        new FiboKeyTest(),
        new FiboFrontCameraTest(),
        new FiboBackCameraTest(),
        new FiboUsbMouseTest(),
        new FiboGpsTest(),
        new FiboWifiTest(),
        new FiboBluetoothTest(),
        new FiboSdCardTest(),
        new FiboSimCardTest(),
    };

    // Need to test items
    private static FiboBaseTest[] sItems;

    // Unknown test item
    private static FiboBaseTest sUnknownTest = new FiboUnknownTest();

    static void updateItems(Context context) {
        sUnknownTest.setContext(context);

        int size = 0;
        for (FiboBaseTest t : ALL_ITEMS) {
            t.setContext(context);
            if (t.isNeedTest()) {
                size++;
            }
        }
        sItems = new FiboBaseTest[size];

        int i = 0;
        for (FiboBaseTest t : ALL_ITEMS) {
            if (t.isNeedTest()) {
                sItems[i] = t;
                i++;
            }
        }
    }

    static int getCount() {
        return sItems.length;
    }

    static FiboBaseTest get(int position) {
        if (position >= 0 && position < getCount()) {
            return sItems[position];
        } else {
            return sUnknownTest;
        }
    }
}
