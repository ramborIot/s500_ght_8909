package com.fibocom.hwtest;

import java.io.File;

import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemProperties;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.android.internal.policy.impl.GosoMmiPolicy;
import com.fibocom.hwtest.HardwareTestDialog;

public class HardwareTestListActivity extends ListActivity implements OnCheckedChangeListener, OnClickListener {

    private static final String TAG = "HardwareTestListActivity";
    private static final String PREFS = "test_prefs";
    private static final String KEY_SINGLE_MODE = "single_mode";
    private boolean mTestResult = false;
    private boolean mNeedDeactivate = false;
    private TestListAdapter mAdapter = null;
    private AlertDialog mAlertDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fibo_test_list);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setActionBarOptions();

        mAdapter = new TestListAdapter(this);
        setListAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int max = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
        mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, max, 0);
        max = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, max, 0);
        GosoMmiPolicy mMmiPolicy = GosoMmiPolicy.getInstance(getApplicationContext());
        mMmiPolicy.setFtm(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        GosoMmiPolicy mMmiPolicy = GosoMmiPolicy.getInstance(getApplicationContext());
        mMmiPolicy.setFtm(false);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_HOME:
            case KeyEvent.KEYCODE_BACK:
                buildExitConfirmDialog();
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Log.i(TAG, "Item clicked: id -> " + id + " position -> " + position);
        Intent intent = new Intent(this, HardwareTestItemActivity.class);
        intent.putExtra("ITEM_INDEX", position);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            mAdapter.notifyDataSetChanged();
            if (resultCode == 1) {
                Bundle bundle = data.getExtras();
                String result = bundle.getString("result");
                buildTestResultDialog(result.equals("true") ? true : false);
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        SharedPreferences sp = getSharedPreferences(PREFS, MODE_PRIVATE);
        sp.edit().putBoolean(KEY_SINGLE_MODE, isChecked).commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reset_results:
                SharedPreferences sp = getSharedPreferences(PREFS, MODE_PRIVATE);
                sp.edit().putBoolean(KEY_SINGLE_MODE, false).commit();
                for (int index = 0; index < HardwareTestList.getCount(); index++) {
                    sp.edit().putInt(String.valueOf(index), -1).commit();
                }
                Intent intent = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    private void buildTestResultDialog(boolean result) {
        mTestResult = result;
        String radioVersion = Build.getRadioVersion();
        if (radioVersion != null && radioVersion.indexOf("\nNV.SS.V") != -1) {
            mNeedDeactivate = true;
        }

        mAlertDialog = new AlertDialog.Builder(HardwareTestListActivity.this).create();
        mAlertDialog.show();
        mAlertDialog.setCancelable(false);
        int layout = mTestResult ? R.layout.fibo_pass_dialog : R.layout.fibo_fail_dialog;
        if (mNeedDeactivate) {
            layout = mTestResult ? R.layout.fibo_deactivate_pass_dialog : R.layout.fibo_deactivate_fail_dialog;
        }
        mAlertDialog.getWindow().setContentView(layout);
        mAlertDialog.getWindow().findViewById(R.id.dismiss_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialog.dismiss();
                if (mNeedDeactivate) {
                    HardwareTestApplication.getInstance().deactivateConfigs(mTestResult);
                }
            }
        });
    }

    private void buildExitConfirmDialog() {
        HardwareTestDialog.Builder builder = new HardwareTestDialog.Builder(HardwareTestListActivity.this);
        builder.setTitle(">>  稍安勿躁  <<");
        builder.setMessage("你，真滴真滴确认不是点错了？");

        builder.setPositiveButton("失误失误", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("走啦走啦", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GosoMmiPolicy mMmiPolicy = GosoMmiPolicy.getInstance(getApplicationContext());
                mMmiPolicy.setFtm(false);
                dialog.dismiss();
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        });

        builder.create().show();
    }

    private void setActionBarOptions() {
        View v = getLayoutInflater().inflate(R.layout.fibo_list_options, null);

        SharedPreferences sp = getSharedPreferences(PREFS, MODE_PRIVATE);
        CheckBox cb = (CheckBox) v.findViewById(R.id.single_mode);
        cb.setChecked(sp.getBoolean(KEY_SINGLE_MODE, false));
        cb.setOnCheckedChangeListener(this);
        Button btn = (Button) v.findViewById(R.id.reset_results);
        btn.setOnClickListener(this);

        LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
        getActionBar().setCustomView(v, lp);

        int flags = ActionBar.DISPLAY_SHOW_CUSTOM;
        int options = getActionBar().getDisplayOptions() ^ flags;
        getActionBar().setDisplayOptions(options, flags);
    }

    class TestListAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public TestListAdapter(Context context) {
            // Cache the LayoutInflate to avoid asking for a new one each time
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return HardwareTestList.getCount();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // A ViewHolder keeps references to children views to avoid unneccessary calls
            // to findViewById() on each row.
            ViewHolder holder;

            // When convertView is not null, we can reuse it directly, there is no need
            // to reinflate it. We only inflate a new View when the convertView supplied
            // by ListView is null.
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.fibo_list_item, null);

                // Creates a ViewHolder and store references to the two children views
                // we want to bind data to.
                holder = new ViewHolder();
                holder.text1 = (TextView) convertView.findViewById(android.R.id.text1);
                holder.text2 = (TextView) convertView.findViewById(android.R.id.text2);

                convertView.setTag(holder);
            } else {
                // Get the ViewHolder back to get fast access to the TextView
                // and the ImageView.
                holder = (ViewHolder) convertView.getTag();
            }

            // Bind the data with the holder.
            setTitleView(position, holder.text1);
            setResultView(position, holder.text2);

            return convertView;
        }

        private void setTitleView(int position, TextView tv) {
            tv.setText(String.format("%02d", (position + 1)) + ". " + HardwareTestList.get(position).getTestName());
        }

        private void setResultView(int position, TextView tv) {
            SharedPreferences sp = getSharedPreferences(PREFS, MODE_PRIVATE);
            int state = sp.getInt(String.valueOf(position), -1);
            switch (state) {
                case 1:
                    tv.setText(R.string.pass_text);
                    tv.setTextColor(Color.GREEN);
                    break;
                case 0:
                    tv.setText(R.string.fail_text);
                    tv.setTextColor(Color.RED);
                    break;
                default:
                    tv.setText("");
                    break;
            }
        }

        class ViewHolder {
            TextView text1;
            TextView text2;
        }
    }
}
