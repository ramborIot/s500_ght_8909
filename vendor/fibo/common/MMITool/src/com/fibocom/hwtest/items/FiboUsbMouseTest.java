package com.fibocom.hwtest.items;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.input.InputManager;
import android.os.Bundle;
import android.util.Log;
import android.view.InputDevice;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.fibocom.hwtest.items.FiboResultCallback;

import com.fibocom.hwtest.R;

public class FiboUsbMouseTest extends FiboBaseTest implements InputManager.InputDeviceListener {
    private static final String TAG = "FiboUsbMouseTest";
    private static final String MOUSE_RIGHT_CLICK_ACTION = "MouseRightClickAction";
    protected static final int MSG_TIMEOUT = 1;
    protected static final int TEST_START_TIMEOUT = 30000;
    private boolean isUsbMouseTesting = false;
    private InputManager mInputManager = null;
    private FiboResultCallback resultCallback = null;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (MOUSE_RIGHT_CLICK_ACTION.equals(action)) {
                int state = intent.getIntExtra("state", 0);
                if (state == 1) {
                    Log.i(TAG, "USB mouse right-clicking!!");
                    isUsbMouseTesting = true;
                    setButtonVisibility(true);
                    setPromptText(R.string.usbmouse_success);
					resultCallback.setResult(isUsbMouseTesting ? FiboBaseTest.TEST_PASS : FiboBaseTest.TEST_FAIL);
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fibo_usb_mouse, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setButtonVisibility(true);
        resultCallback.setResult(FiboBaseTest.TEST_WAIT_PASS);
    }

    @Override
    public void onHandleMessage(final int index) {
        switch (index) {
            case MSG_TIMEOUT:
                if (!isUsbMouseTesting) {
                    setButtonVisibility(true);
                    setPromptText(R.string.usbmouse_timeout);
                    resultCallback.setResult(isUsbMouseTesting ? FiboBaseTest.TEST_PASS : FiboBaseTest.TEST_FAIL);
                }
                break;
        }
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(MOUSE_RIGHT_CLICK_ACTION);
        getActivity().registerReceiver(mReceiver, filter);
        isUsbMouseTesting = false;
        setTimerTask(MSG_TIMEOUT, TEST_START_TIMEOUT);

        mInputManager = (InputManager) getActivity().getSystemService(Context.INPUT_SERVICE);
        mInputManager.registerInputDeviceListener(this, null);
        if (mInputManager == null) {
            mInputManager = (InputManager) getActivity().getSystemService(Context.INPUT_SERVICE);
            mInputManager.registerInputDeviceListener(this, null);
        }
        final int[] devices = InputDevice.getDeviceIds();
        for (int i = 0; i < devices.length; i++) {
            InputDevice device = InputDevice.getDevice(devices[i]);  
            if (device != null /*&& !device.isVirtual() */&& device.isExternal()) {
                if(device.getName().contains("Mouse") || device.getName().contains("MOUSE") || device.getName().contains("mouse")) {
                    Log.d(TAG, "device.getName() = " + device.getName() + ", device.getId() = " + device.getId());
                    setButtonVisibility(true);
                    resultCallback.setResult(FiboBaseTest.TEST_PASS);
                    break;
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void onInputDeviceAdded(int deviceId) {
        InputDevice device = InputDevice.getDevice(deviceId);  
        if (device != null && !device.isVirtual() && device.isExternal()) {
            if(device.getName().contains("Mouse")) {
                Log.d(TAG, "onInputDeviceAdded(): device.getName() = " + device.getName() + ", device.getId() = " + device.getId());
                if (getRunningActivityName().indexOf("HardwareTestItemActivity") != -1) {
                    //setButtonVisibility(true); /* when GPS testing, plug in USB mouse, then caused java.lang.NullPointerException */
                    resultCallback.setResult(FiboBaseTest.TEST_PASS);
                }
            }
        }
    }

    @Override
    public void onInputDeviceChanged(int deviceId) {
    }

    @Override
    public void onInputDeviceRemoved(int deviceId) {
    }

    @Override
    public String getTestName() {
        return getContext().getString(R.string.usbmouse_title);
    }

    @Override
    public boolean isNeedTest() {
        return getSystemProperties("storage", true);
    }

    @Override
    public int getTestResult() {
        return (isUsbMouseTesting ? FiboBaseTest.TEST_PASS : FiboBaseTest.TEST_FAIL);
    }

    @Override
    public int getTestTimeout() {
        return FiboBaseTest.NO_TIMEOUT;
    }

    @Override
    public boolean setTestCallback(FiboResultCallback callback) {
        this.resultCallback = callback;
        return true;
    }

    private void setPromptText(int resId) {
        ((TextView) getView().findViewById(R.id.prompt_text)).setText(resId);
    }

    private String getRunningActivityName() {
        ActivityManager activityManager = (ActivityManager) getContext().getSystemService(Context.ACTIVITY_SERVICE);
        String runningActivity = activityManager.getRunningTasks(1).get(0).topActivity.getClassName();
        Log.i(TAG, "runningActivity = " + runningActivity);
        return runningActivity;
    }
}
