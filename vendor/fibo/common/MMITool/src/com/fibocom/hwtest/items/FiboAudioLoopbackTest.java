package com.fibocom.hwtest.items;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fibocom.hwtest.R;

public class FiboAudioLoopbackTest extends FiboSpeakerTest {
    private static final String TAG = "FiboAudioLoopbackTest";
    protected static final int PLAY_AFTER_RECORD_TIME = 1000;
    protected static final int TEST_HOLD_TIME = 60000;
    private AudioManager mAudioManager = null;
    private Loopback mLoopback = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fibo_audio_loopback, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.setMode(AudioManager.MODE_NORMAL);
        setButtonVisibility(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLoopback();
    }

    @Override
    public void onHandleMessage(final int index) {
        switch (index) {
            case MSG_START:
                if (mAudioManager.isWiredHeadsetOn()) {
                    setPromptText(R.string.speaker_plug_out_headset);
                    setButtonVisibility(false);
                    break;
                }
                setTimerTask(MSG_STOP, TEST_HOLD_TIME);
                setPromptText(R.string.loopback_testing);
                setButtonVisibility(true);
                startLoopback();
                break;
            case MSG_STOP:
                setPromptText(R.string.test_again);
                stopLoopback();
                break;
        }
    }

    @Override
    public String getTestName() {
        return getContext().getString(R.string.loopback_title);
    }

    @Override
    public boolean isNeedTest() {
        return getSystemProperties("loopback", true);
    }

    @Override
    public int getTestResult() {
        return FiboBaseTest.TEST_INSPECTION;
    }

    @Override
    public int getTestTimeout() {
        return FiboBaseTest.DEF_TIMEOUT;
    }

    @Override
    public boolean setTestCallback(FiboResultCallback callback) {
        return false;
    }

    public void startLoopback() {
        if (mLoopback == null) {
            try {
                mLoopback = new Loopback();
                mLoopback.start();
            } catch (Exception e) {
                Log.e(TAG, "startLoopback: " + e.getMessage());
            }
        }
    }

    public void stopLoopback() {
        if (mLoopback != null) {
            try {
                mLoopback.stop();
            } catch (Exception e) {
                Log.e(TAG, "stopLoopback: " + e.getMessage());
            }
            mLoopback = null;
        }
    }

    private void setPromptText(int resId) {
        ((TextView) getView().findViewById(R.id.prompt_text)).setText(resId);
    }

    private class Loopback implements Runnable {
        private static final int RATE_IN_HZ = 44100;
        private AudioRecord mAudioRecord;
        private AudioTrack mAudioTrack;
        private Object mLock = new Object();

        private boolean mLoop = true;
        private int mRecordBuffer = 0;
        private int mTrackBuffer = 0;

        public Loopback() {
            mRecordBuffer = AudioRecord.getMinBufferSize(RATE_IN_HZ,
                    AudioFormat.CHANNEL_IN_STEREO,
                    AudioFormat.ENCODING_PCM_16BIT);
            mTrackBuffer = AudioTrack.getMinBufferSize(RATE_IN_HZ,
                    AudioFormat.CHANNEL_OUT_STEREO,
                    AudioFormat.ENCODING_PCM_16BIT);
            mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                    RATE_IN_HZ, AudioFormat.CHANNEL_IN_STEREO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    mRecordBuffer * 10);
            mAudioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                    RATE_IN_HZ, AudioFormat.CHANNEL_OUT_STEREO,
                    AudioFormat.ENCODING_PCM_16BIT, mTrackBuffer,
                    AudioTrack.MODE_STREAM);
            mAudioTrack.setStereoVolume(1.0f, 1.0f);
        }

        @Override
        public void run() {
            byte[] buffer = new byte[mRecordBuffer];
            int size;

            synchronized (mLock) {
                if (mAudioRecord != null && mAudioTrack != null) {
                    mAudioRecord.startRecording();
                    try {
                        Thread.sleep(PLAY_AFTER_RECORD_TIME);
                    } catch (InterruptedException e) {
                    }
                    if (mLoop && mAudioTrack != null) {
                        mAudioTrack.play();
                    }
                } else {
                    mLoop = false;
                }

                while (mLoop && (size = mAudioRecord.read(buffer, 0, mRecordBuffer)) != -1) {
                    mAudioTrack.write(buffer, 0, size);
                }
            }
        }

        public void start() {
            new Thread(this).start();
        }

        public void stop() throws IllegalStateException {
            mLoop = false;

            synchronized (mLock) {
                if (mAudioRecord != null) {
                    mAudioRecord.stop();
                    mAudioRecord.release();
                    mAudioRecord = null;
                }

                if (mAudioTrack != null) {
                    mAudioTrack.stop();
                    mAudioTrack.release();
                    mAudioTrack = null;
                }
            }
        }
    }
}
