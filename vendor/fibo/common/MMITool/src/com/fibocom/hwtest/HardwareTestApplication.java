package com.fibocom.hwtest;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import com.qualcomm.qcrilhook.QcRilHook;
import com.qualcomm.qcrilhook.QcRilHookCallback;

public class HardwareTestApplication extends Application {
    private static final String TAG = "HardwareTestApplication";
    private boolean mQcRilHookReady = false;
    private QcRilHook mQcRilHook = null;
    private static HardwareTestApplication sApp = null;

    public HardwareTestApplication() {
        sApp = this;
    }

    public static HardwareTestApplication getInstance() {
        if (sApp == null) {
            throw new IllegalStateException("no HardwareTestApplication");
        }
        return sApp;
    }

    // QcRilHook Callback
    private QcRilHookCallback mQcRilHookCallback = new QcRilHookCallback() {
        @Override
        public void onQcRilHookReady() {
            Log.i(TAG, "QcRilHook is ready");
            mQcRilHookReady = true;
        }
        public void onQcRilHookDisconnected() {
            // TODO: Handle onQcRilHookDisconnected
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        // Open Bluetooth, WiFi & GPS, etc.
        hardwareTestInit();

        mQcRilHook = new QcRilHook(this, mQcRilHookCallback);

        // Update test items when the app starts.
        HardwareTestList.updateItems(getBaseContext());
    }

    public void hardwareTestInit() {
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!btAdapter.isEnabled()) {
            btAdapter.enable();
        }

        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }

        try {
            Settings.Secure.setLocationProviderEnabled(getApplicationContext().getContentResolver(),
                    LocationManager.GPS_PROVIDER, true);
        } catch (Exception e) {
        }
    }

    public boolean deactivateConfigs(boolean state) {
        Log.i(TAG, "deactivateConfigs, mQcRilHookReady:" + mQcRilHookReady);
        if (!mQcRilHookReady) {
            return false;
        }

        boolean status = mQcRilHook.qcRilDeactivateConfigs();
        if (state) {
            boolean isLandiEdition = (Build.MODEL.equals("SC808") || Build.MODEL.equals("SC820")) ? true : false;
            setNvTestResultStatus(state, isLandiEdition);
            Log.i(TAG, "Finally set NV: " + (state ? "PASS" : "FAIL") + "!");
        }
        return status;
    }

    public String getNvSN() {
        String ret = "UNKNOWN";

        if (nvctrlInit() >= 0) {
            ret = getSN();
            nvctrlDeinit();
        }
        return ret;
    }

    public String getNvIMEI() {
        String ret = "UNKNOWN";

        if (nvctrlInit() >= 0) {
            ret = getIMEI();
            nvctrlDeinit();
        }
        return ret;
    }

    public String getNvMEID() {
        String ret = "UNKNOWN";

        if (nvctrlInit() >= 0) {
            ret = getMEID();
            nvctrlDeinit();
        }
        return ret;
    }

    public int getNvCalibrationStatus() {
        int ret = 0;

        if (nvctrlInit() >= 0) {
            ret = isNvCalibration();
            nvctrlDeinit();
        }
        return ret;
    }

    public int getNvComprehensiveStatus() {
        int ret = 0;

        if (nvctrlInit() >= 0) {
            ret = isNvComprehensive();
            nvctrlDeinit();
        }
        return ret;
    }

    public int setNvTestResultStatus(boolean result, boolean isLandiEdition) {
        int ret = -1;

        if (nvctrlInit() >= 0) {
            ret = setNvTestResult(result, isLandiEdition);
            nvctrlDeinit();
        }
        return ret;
    }

    /* return: (>= 0: SUCCESS; < 0: FAILED) */
    public native int nvctrlInit();

    /* return: (>= 0: SUCCESS; < 0: FAILED) */
    public native int nvctrlDeinit();

    /* return: SN string */
    public native String getSN();

    /* return: IMEI string */
    public native String getIMEI();

    /* return: MEID string */
    public native String getMEID();

    /* return: (> 0: PASS; < 0: FAIL; = 0: UNKNOWN) */
    public native int isNvCalibration();

    /* return: (> 0: PASS; < 0: FAIL; = 0: UNKNOWN) */
    public native int isNvComprehensive();

    /* return: (>= 0: SUCCESS; < 0: FAILED) */
    public native int setNvTestResult(boolean result, boolean isLandiEdition);

    static {
        System.loadLibrary("nvctrl");
    }
}
