package com.fibocom.hwtest.items;

import java.io.IOException;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fibocom.hwtest.R;

public class FiboFrontCameraTest extends FiboBaseTest {
    private static final String TAG = "FiboFrontCameraTest";
    protected static final int MSG_START_CAMERA = 1;
    protected static final int TEST_START_WAITTIME = 500;
    private FiboResultCallback resultCallback = null;
    private Camera mCamera = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fibo_front_camera, container, false); 
        setFrontCameraInfo(v);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setButtonVisibility(true);
        //resultCallback.setResult(FiboBaseTest.TEST_WAIT_PASS);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (hasFrontFacingCamera()) {
            setTimerTask(MSG_START_CAMERA, TEST_START_WAITTIME);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (hasFrontFacingCamera()) {
            stopCamera();
        }
    }

    @Override
    public void onHandleMessage(final int index) {
        switch (index) {
            case MSG_START_CAMERA:
                startCamera();
                setButtonVisibility(true);
                if (mCamera == null) {
                    setPromptText(R.string.front_camera_openerror, 0xffcd2626);
                    resultCallback.setResult(FiboBaseTest.TEST_FAIL);
                } else {
                    setPromptText(R.string.front_camera_testing, 0xff00ff00);
                }
                break;
        }
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        return false;
    }

    @Override
    public String getTestName() {
        return getContext().getString(R.string.front_camera_title);
    }

    @Override
    public boolean isNeedTest() {
        PackageManager pm = getContext().getPackageManager();
        boolean hasCamera = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT);
        return hasCamera && getSystemProperties("camera", true);
    }

    @Override
    public int getTestResult() {
        return hasFrontFacingCamera() ? FiboBaseTest.TEST_WAIT_PASS : FiboBaseTest.TEST_FAIL;
    }

    @Override
    public int getTestTimeout() {
        return FiboBaseTest.NO_TIMEOUT;
    }

    @Override
    public boolean setTestCallback(FiboResultCallback callback) {
        this.resultCallback = callback;
        return true;
    }

    private void startCamera() {
        int oritationAdjust = 0;

        try {
            mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
            mCamera.setDisplayOrientation(oritationAdjust);
        } catch (Exception exception) {
            mCamera = null;
        }

        if (mCamera != null) {
            try {
                SurfaceView mSurfaceView = (SurfaceView) getView().findViewById(R.id.preview_surface);
                SurfaceHolder mSurfaceHolder = mSurfaceView.getHolder();
                mCamera.setPreviewDisplay(mSurfaceHolder);
                mCamera.startPreview();
                setButtonVisibility(true);
                resultCallback.setResult(FiboBaseTest.TEST_INSPECTION);
            } catch (IOException exception) {
                mCamera.release();
                mCamera = null;
            }
        }
    }

    private void stopCamera() {
        if (mCamera != null) {
            try {
                if (mCamera.previewEnabled()) {
                    mCamera.stopPreview();
                }
                mCamera.release();
                mCamera = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setFrontCameraInfo(View v) {
        boolean isFrontCameraReady = hasFrontFacingCamera();

        if (!isFrontCameraReady) {
            TextView prompt = (TextView) v.findViewById(R.id.prompt_text);
            int textColor = isFrontCameraReady ? 0xff00ff00 : 0xffcd2626;
            prompt.setText(R.string.front_camera_notexist);
            prompt.setTextColor(textColor);
        }
        resultCallback.setResult(isFrontCameraReady ? FiboBaseTest.TEST_WAIT_PASS : FiboBaseTest.TEST_FAIL);
    }

    private void setPromptText(int resId, int color) {
        ((TextView) getView().findViewById(R.id.prompt_text)).setText(resId);
        ((TextView) getView().findViewById(R.id.prompt_text)).setTextColor(color);
    }

    private static boolean hasFrontFacingCamera() {
        return checkCameraFacing(CameraInfo.CAMERA_FACING_FRONT);
    }

    private static boolean checkCameraFacing(final int facing) {
        final int cameraCount = Camera.getNumberOfCameras();
        Log.i(TAG, "checkCameraFacing(" + facing + ") cameraCount = " + cameraCount);
        CameraInfo info = new CameraInfo();
        for (int i = 0; i < cameraCount; i++) {
            Camera.getCameraInfo(i, info);
            if (facing == info.facing) {
                Log.i(TAG, "checkCameraFacing(" + facing + ") exist.");
                return true;
            }
        }
        Log.i(TAG, "checkCameraFacing(" + facing + ") NOT exist.");
        return false;
    }
}
