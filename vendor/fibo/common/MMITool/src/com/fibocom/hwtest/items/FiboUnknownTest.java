package com.fibocom.hwtest.items;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fibocom.hwtest.items.FiboResultCallback;

import com.fibocom.hwtest.R;

public class FiboUnknownTest extends FiboBaseTest {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fibo_unknown, container, false);
        return v;
    }

    @Override
    public String getTestName() {
        return getContext().getString(R.string.unknown_title);
    }

    @Override
    public boolean isNeedTest() {
        return false;
    }

    @Override
    public int getTestResult() {
        return FiboBaseTest.TEST_INSPECTION;
    }

    @Override
    public int getTestTimeout() {
        return FiboBaseTest.DEF_TIMEOUT;
    }

    @Override
    public boolean setTestCallback(FiboResultCallback callback) {
        return false;
    }
}
