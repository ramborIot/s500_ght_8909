package com.fibocom.hwtest.items;

import android.os.Bundle;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.fibocom.hwtest.items.FiboResultCallback;

import com.fibocom.hwtest.R;

public class FiboKeyTest extends FiboBaseTest {
    private static final String TAG = "FiboKeyTest";
    private static final String KEY_POWER_DISABLE = "key_power_disable";
    protected static final int MSG_TIMEOUT = 1;
    protected static final int TEST_START_TIMEOUT = 10000;
    private boolean isKeyTestPass = false;
    private boolean isKeyVolumeDownPress = false;
    private boolean isKeyVolumeUpPress = false;
    private boolean isKeyPowerPress = false;
    
    private FiboResultCallback resultCallback = null;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fibo_key, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setButtonVisibility(true);
        resultCallback.setResult(FiboBaseTest.TEST_WAIT_PASS);
    }

    @Override
    public void onHandleMessage(final int index) {
        switch (index) {
            case MSG_TIMEOUT:
                setButtonVisibility(true);
                resultCallback.setResult(isKeyTestPass ? FiboBaseTest.TEST_PASS : FiboBaseTest.TEST_FAIL);
                break;
        }
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        isKeyTestPass = false;
        isKeyVolumeDownPress = false;
        isKeyVolumeUpPress = false;
        isKeyPowerPress = false;
        setTimerTask(MSG_TIMEOUT, TEST_START_TIMEOUT);
        Settings.System.putIntForUser(getContext().getContentResolver(), KEY_POWER_DISABLE, 1, UserHandle.USER_CURRENT);
    }

    @Override
    public void onPause() {
        super.onPause();
        Settings.System.putIntForUser(getContext().getContentResolver(), KEY_POWER_DISABLE, 0, UserHandle.USER_CURRENT);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        TextView v = (TextView) getView().findViewById(R.id.key_text);
        v.setText("当前按键: " + KeyEvent.keyCodeToString(keyCode));
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            case KeyEvent.KEYCODE_VOLUME_UP:
            case KeyEvent.KEYCODE_POWER:
                setButtonStyle(keyCode);
                return true;
            default:
                Log.d(TAG, "onKeyDown -> keyCode: " + keyCode);
                return false;
        }
    }

    @Override
    public String getTestName() {
        return getContext().getString(R.string.key_title);
    }

    @Override
    public boolean isNeedTest() {
        return getSystemProperties("key", true);
    }

    @Override
    public int getTestResult() {
        return (isKeyTestPass ? FiboBaseTest.TEST_PASS : FiboBaseTest.TEST_FAIL);
    }

    @Override
    public int getTestTimeout() {
        return FiboBaseTest.NO_TIMEOUT;
    }

    @Override
    public boolean setTestCallback(FiboResultCallback callback) {
        this.resultCallback = callback;
        return true;
    }

    private void setButtonStyle(int keyCode) {
        if (KeyEvent.KEYCODE_VOLUME_DOWN == keyCode) {
            isKeyVolumeDownPress = true;
            ((Button) getView().findViewById(R.id.btn_vol_down)).setBackgroundColor(0xff458b00);
        } else if (KeyEvent.KEYCODE_VOLUME_UP == keyCode) {
            isKeyVolumeUpPress = true;
            ((Button) getView().findViewById(R.id.btn_vol_up)).setBackgroundColor(0xff458b00);
        } else if (KeyEvent.KEYCODE_POWER == keyCode) {
            isKeyPowerPress = true;
            ((Button) getView().findViewById(R.id.btn_power)).setBackgroundColor(0xff458b00);
        }
        isKeyTestPass = (isKeyVolumeDownPress && isKeyVolumeUpPress && isKeyPowerPress) ? true : false;
        if (isKeyTestPass) {
            setButtonVisibility(true);
            resultCallback.setResult(isKeyTestPass ? FiboBaseTest.TEST_PASS : FiboBaseTest.TEST_FAIL);
        }
    }
}
