package com.fibocom.hwtest.items;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemProperties;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fibocom.hwtest.R;

public class FiboGpsTest extends FiboBaseTest {
    private static final String TAG = "FiboGpsTest";
    protected static final int MSG_GPS_TIMEOUT = 1;
    protected static final int GPS_REFRESH_WAITTIME = 30000;
    private LocationManager mLocationManager = null;
	private Location location = null;
    private final float validSnrLimit = 30.00f;
    private int firstFixTime = 0;
    private int satellitesCount = 0;
    private int satellitesValidCount = 0;
    private boolean isFixed = false;
    private boolean isGpsTestFinished = false;
	private String satellitesList = null;
    private FiboResultCallback resultCallback = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fibo_gps, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setButtonVisibility(true);
        resultCallback.setResult(FiboBaseTest.TEST_WAIT_PASS);
    }

    @Override
    public void onHandleMessage(final int index) {
        switch (index) {
            case MSG_GPS_TIMEOUT:
                isGpsTestFinished = true;
                setButtonVisibility(true);
                resultCallback.setResult(FiboBaseTest.TEST_FAIL);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        firstFixTime = 0;
        satellitesCount = 0;
        getService();
        startGPS();
        setTimerTask(MSG_GPS_TIMEOUT, GPS_REFRESH_WAITTIME);
    }

    @Override
    public void onPause() {
        super.onPause();
        stopGPS();
    }

    @Override
    public String getTestName() {
        return getContext().getString(R.string.gps_title);
    }

    @Override
    public boolean isNeedTest() {
        String radioVersion = Build.getRadioVersion();
        if (radioVersion != null && radioVersion.indexOf("\nNV.WI.V") != -1) {
            return false;
        }
        return getSystemProperties("gps", true);
    }

    @Override
    public int getTestResult() {
        return FiboBaseTest.TEST_FAIL;
    }

    @Override
    public int getTestTimeout() {
        return FiboBaseTest.NO_TIMEOUT;
    }

    @Override
    public boolean setTestCallback(FiboResultCallback callback) {
        this.resultCallback = callback;
        return true;
    }

    private void setGpsLocationInfo(Location location) {
        TextView gps_fixed_time = (TextView) getView().findViewById(R.id.gps_fixed_time);
        TextView gps_fixed_state = (TextView) getView().findViewById(R.id.gps_fixed_state);
        TextView gps_timestamp = (TextView) getView().findViewById(R.id.gps_timestamp);
        TextView gps_satellite_number = (TextView) getView().findViewById(R.id.gps_satellite_number);
        TextView gps_longitude = (TextView) getView().findViewById(R.id.gps_longitude);
        TextView gps_latitude = (TextView) getView().findViewById(R.id.gps_latitude);
        TextView gps_speed = (TextView) getView().findViewById(R.id.gps_speed);
        TextView gps_altitude = (TextView) getView().findViewById(R.id.gps_altitude);
        TextView gps_accuracy = (TextView) getView().findViewById(R.id.gps_accuracy);
        TextView gps_bearing = (TextView) getView().findViewById(R.id.gps_bearing);
		TextView gps_satellite_list = (TextView) getView().findViewById(R.id.satellite_list);

        int satellitesCnt = satellitesCount;
        int satellitesValidCnt = satellitesValidCount;
        gps_fixed_time.setText(String.valueOf(firstFixTime));
        gps_fixed_state.setText(isFixed ? "已定位" : "未定位");
		if (satellitesCnt == 0) {
			gps_satellite_list.setText(R.string.gps_satellite_list);
		} else {
			gps_satellite_list.setText("当前信噪比大于30的真星个数：" + satellitesValidCnt + satellitesList);
		}
        if (location != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            gps_timestamp.setText(sdf.format(location.getTime()));
            gps_satellite_number.setText(String.valueOf(satellitesCnt));
            gps_longitude.setText(String.valueOf(location.getLongitude()) + "°");
            gps_latitude.setText(String.valueOf(location.getLatitude()) + "°");
            gps_speed.setText(String.valueOf(location.getSpeed()) + "m/s");
            gps_altitude.setText(String.valueOf(location.getAltitude()) + "m");
            gps_accuracy.setText(String.valueOf(location.getAccuracy()) + "m");
            gps_bearing.setText(String.valueOf(location.getBearing()) + "°");
        }

        int textColor = isFixed ? 0xff00ff00 : 0xffcd2626;
        gps_fixed_state.setTextColor(textColor);
        if (/*location != null && */satellitesCnt > 0 && satellitesValidCnt > 0) {
            isGpsTestFinished = true;
            setButtonVisibility(true);
            resultCallback.setResult(FiboBaseTest.TEST_PASS);
        } else {
            if (!isGpsTestFinished) {
                setButtonVisibility(true);
                resultCallback.setResult(FiboBaseTest.TEST_WAIT_PASS);
            }
        }
    }

	private void getService() {
		mLocationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
		if (mLocationManager == null) {
			Log.e(TAG, "Fail to get LOCATION_SERVICE!");
		}
	}

    private String getGpsProvider() {
        Criteria criteria = null;
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(true);
        criteria.setBearingRequired(true);
        criteria.setSpeedRequired(true);
        criteria.setCostAllowed(false);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        String provider = mLocationManager.getBestProvider(criteria, true);
        return provider;
    }

	private void startGPS() {
		if (!mLocationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)) {
			Intent intent = new Intent();
			intent.setClassName("com.android.settings", "com.android.settings.Settings$LocationSettingsActivity");
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			return;
		}
        String provider = getGpsProvider();
		if (provider == null) {
			Log.e(TAG, "Fail to get GPS Provider!");
            return;
		}
		mLocationManager.requestLocationUpdates(provider, 500, 0, mLocationListener);
		mLocationManager.addGpsStatusListener(gpsStatusListener);
		location = mLocationManager.getLastKnownLocation(provider);
		setGpsLocationInfo(location);
	}

	private void stopGPS() {
		try {
			mLocationManager.removeUpdates(mLocationListener);
			mLocationManager.removeGpsStatusListener(gpsStatusListener);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	LocationListener mLocationListener = new LocationListener() {
		@Override
		public void onLocationChanged(Location location) {
			setGpsLocationInfo(location);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Log.i(TAG, "当前GPS状态为可见状态");
                    isFixed = true;
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Log.i(TAG, "当前GPS状态为服务区外状态");
                    isFixed = false;
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Log.i(TAG, "当前GPS状态为暂停服务状态");
                    isFixed = false;
                    break;
            }
		}

		@Override
		public void onProviderEnabled(String provider) {
            Location location = mLocationManager.getLastKnownLocation(provider);
            setGpsLocationInfo(location);
		}

		@Override
		public void onProviderDisabled(String provider) {
			setGpsLocationInfo(null);
		}
	};

    GpsStatus.Listener gpsStatusListener = new GpsStatus.Listener() {
        @Override
        public void onGpsStatusChanged(int arg0) {
            GpsStatus mGpsStatus = mLocationManager.getGpsStatus(null);
            switch (arg0) {
                case GpsStatus.GPS_EVENT_FIRST_FIX:
                    firstFixTime = mGpsStatus.getTimeToFirstFix();
                    Log.i(TAG, "第一次定位");
                    break;
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                    int count = 0, valid_count = 0;
                    int maxSatellites = mGpsStatus.getMaxSatellites();
                    Iterator<GpsSatellite> it = mGpsStatus.getSatellites().iterator();
                    ArrayList<GpsSatellite> satelliteList = new ArrayList<GpsSatellite>();
                    while (it.hasNext() && count <= maxSatellites) {
                        GpsSatellite gpsS = (GpsSatellite) it.next();
                        satelliteList.add(gpsS);
                        count++;
                    }
                    satellitesCount = count;

                    if (satellitesCount == 0) {
                        satellitesValidCount = 0;
                        satellitesList = "";
                    } else {
                        StringBuilder sb = new StringBuilder("");
                        for (int idx = 0; idx < satelliteList.size(); idx++) {
                            int prn = satelliteList.get(idx).getPrn();
                            float snr = satelliteList.get(idx).getSnr();
                            if (snr > validSnrLimit) {
                                sb.append("\nsnr[" + String.format("%02d", prn) + "] = " + snr);
                                valid_count++;
                            }
                        }
                        satellitesValidCount = valid_count;
                        satellitesList = sb.toString();
                    }
                    location = mLocationManager.getLastKnownLocation(getGpsProvider());
                    setGpsLocationInfo(location);
                    break;
                case GpsStatus.GPS_EVENT_STARTED:
                    Log.i(TAG, "启动定位");
                    break;
                case GpsStatus.GPS_EVENT_STOPPED:
                    Log.i(TAG, "结束定位");
                    break;
                default:
                    break;
            }
        }
    };
}
