package com.fibocom.hwtest.items;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.fibocom.hwtest.HardwareTestApplication;
import com.fibocom.hwtest.R;

public class FiboSystemVersionTest extends FiboBaseTest {
    private static final String TAG = "FiboSystemVersionTest";
    private boolean isTestPassed = false;
    private FiboResultCallback resultCallback = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        Context mContext = getContext().getApplicationContext();
        View v = inflater.inflate(R.layout.fibo_system_version, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setButtonVisibility(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        isTestPassed = false;
        setHardwareVersionInfo(getView());
        setSoftwareVersionInfo(getView());
        setRfTestResult(getView());
        verifyTestResult(getView());
    }

    @Override
    public String getTestName() {
        return getContext().getString(R.string.version_title);
    }

    @Override
    public boolean isNeedTest() {
        return getSystemProperties("version", true);
    }

    @Override
    public int getTestResult() {
        return FiboBaseTest.TEST_INSPECTION;
    }

    @Override
    public int getTestTimeout() {
        return FiboBaseTest.NO_TIMEOUT;
    }

    @Override
    public boolean setTestCallback(FiboResultCallback callback) {
        this.resultCallback = callback;
        return true;
    }

    private String getBluetoothMacAddress() {
        int tryCnt = 0;
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!btAdapter.isEnabled()) {
            if (btAdapter.enable()) {
                while (btAdapter.getState() != BluetoothAdapter.STATE_ON && tryCnt < 5) {
                    try {
                        tryCnt++;
                        Thread.sleep(200);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return "";
                    }
                }
            }
        }
        return (btAdapter.getState() == BluetoothAdapter.STATE_ON) ? btAdapter.getAddress() : "";
    }
    
    private String getWiFiMacAddress() {
        int tryCnt = 0;
        WifiManager wifiManager = (WifiManager) getContext().getSystemService(Context.WIFI_SERVICE);

        if (!wifiManager.isWifiEnabled()) {
            if (wifiManager.setWifiEnabled(true)) {
                while (wifiManager.getWifiState() != WifiManager.WIFI_STATE_ENABLED && tryCnt < 5) {
                    try {
                        tryCnt++;
                        Thread.sleep(400);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return "";
                    }
                }
            }
        }
        return (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) ? wifiManager.getConnectionInfo().getMacAddress() : "";
    }

    private void setHardwareVersionInfo(View v) {
        TextView board = (TextView) v.findViewById(R.id.version_hardware_board);
        TextView model = (TextView) v.findViewById(R.id.version_hardware_model);
        TextView device = (TextView) v.findViewById(R.id.version_hardware_device);
        TextView manufacturer = (TextView) v.findViewById(R.id.version_hardware_manufacturer);
        TextView serialno = (TextView) v.findViewById(R.id.version_hardware_serialno);
        TextView hardwareid = (TextView) v.findViewById(R.id.version_hardware_hardwareid);
        TextView sn = (TextView) v.findViewById(R.id.version_hardware_sn);
        TextView imei = (TextView) v.findViewById(R.id.version_hardware_imei);
        TextView meid = (TextView) v.findViewById(R.id.version_hardware_meid);
        TextView btmac = (TextView) v.findViewById(R.id.version_hardware_btmac);
        TextView wifimac = (TextView) v.findViewById(R.id.version_hardware_wifimac);

        board.setText(Build.HARDWARE + "-" + Build.BOARD);
        model.setText(Build.MODEL);
        device.setText(Build.DEVICE);
        manufacturer.setText(Build.MANUFACTURER);
        serialno.setText(Build.SERIAL.toUpperCase());
        hardwareid.setText(Secure.getString(getContext().getContentResolver(), Secure.ANDROID_ID).toUpperCase());
        sn.setText(HardwareTestApplication.getInstance().getNvSN());
        imei.setText(HardwareTestApplication.getInstance().getNvIMEI());
        meid.setText(HardwareTestApplication.getInstance().getNvMEID());
        btmac.setText(getBluetoothMacAddress().toUpperCase());
        wifimac.setText(getWiFiMacAddress().toUpperCase());
    }
    
    private void setSoftwareVersionInfo(View v) {
        TextView release = (TextView) v.findViewById(R.id.version_software_release);
        TextView sdk = (TextView) v.findViewById(R.id.version_software_sdk);
        TextView ap = (TextView) v.findViewById(R.id.version_software_ap);
        TextView bp = (TextView) v.findViewById(R.id.version_software_bp);
        TextView incremental = (TextView) v.findViewById(R.id.version_software_incremental);
        TextView buildtime = (TextView) v.findViewById(R.id.version_software_buildtime);

        release.setText(Build.VERSION.RELEASE);
        sdk.setText(Build.VERSION.SDK_INT + "");
        ap.setText(Build.DISPLAY + "-" + Build.TYPE);
        bp.setText(Build.getRadioVersion());
        incremental.setText(Build.VERSION.INCREMENTAL);
        buildtime.setText(SystemProperties.get("ro.build.date"));
        ap.setTextColor(0xffcd2626);
        bp.setTextColor(0xffcd2626);
    }

    private void setRfTestResult(View v) {
        int ret = 0;
        String calibrationStatus = "UNKNOWN";
        String comprehensiveStatus = "UNKNOWN";
        TextView calibration = (TextView) v.findViewById(R.id.version_rftest_calibration);
        TextView comprehensive = (TextView) v.findViewById(R.id.version_rftest_comprehensive);

        ret = HardwareTestApplication.getInstance().getNvCalibrationStatus();
        calibrationStatus = ((ret > 0) ? "PASS" : ((ret < 0) ? "FAIL" : "UNKNOWN"));
        ret = HardwareTestApplication.getInstance().getNvComprehensiveStatus();
        comprehensiveStatus = ((ret > 0) ? "PASS" : ((ret < 0) ? "FAIL" : "UNKNOWN"));
        calibration.setText(calibrationStatus);
        comprehensive.setText(comprehensiveStatus);
        calibration.setTextColor(0xffcd2626);
    }

    private void verifyTestResult(View v) {
        boolean isApPassed = false;
        boolean isBpPassed = false;
        TextView ap = (TextView) v.findViewById(R.id.version_software_ap);
        TextView bp = (TextView) v.findViewById(R.id.version_software_bp);
        TextView calibration = (TextView) v.findViewById(R.id.version_rftest_calibration);

        isTestPassed = false;
        if (Build.DISPLAY != null && Build.DISPLAY.length() > 0 && Build.DISPLAY.indexOf(Build.MODEL) != -1) {
            if (Build.TYPE != null && Build.TYPE.length() > 0) {
                ap.setTextColor(0xff00ff00);
                isApPassed = true;
            }
        }
        String radioVersion = Build.getRadioVersion();
        if (radioVersion != null) {
            if (Build.MODEL.equals("SC820") || Build.MODEL.equals("SC808")) {
                if (radioVersion.indexOf("\nSS.V") != -1 || radioVersion.indexOf("\nWI.V") != -1) {
                    if ((Build.MODEL.equals("SC820") && radioVersion.indexOf("19200.") != -1) ||
                            (Build.MODEL.equals("SC808") && radioVersion.indexOf("19080.") != -1)) {
                        bp.setTextColor(0xff00ff00);
                        isBpPassed = true;
                    }
                }
                if (((radioVersion.indexOf("\nSS.V") != -1) && calibration.getText().equals("PASS")) ||
                        (radioVersion.indexOf("\nWI.V") != -1)) {
                    calibration.setTextColor(0xff00ff00);
                    isTestPassed = (isApPassed && isBpPassed) ? true : false;
                }
            } else {
                if (radioVersion.indexOf("\nNV.SS.V") != -1 || radioVersion.indexOf("\nNV.WI.V") != -1) {
                    if (radioVersion.indexOf(Build.MODEL) != -1) {
                        bp.setTextColor(0xff00ff00);
                        isBpPassed = true;
                    }
                }
                if (((radioVersion.indexOf("\nNV.SS.V") != -1) && calibration.getText().equals("PASS")) ||
                        (radioVersion.indexOf("\nNV.WI.V") != -1)) {
                    calibration.setTextColor(0xff00ff00);
                    isTestPassed = (isApPassed && isBpPassed) ? true : false;
                }
            }
        }
        setButtonVisibility(true);
        if (!isTestPassed) {
            resultCallback.setResult(FiboBaseTest.TEST_FAIL);
        }
    }
}
