package com.fibocom.hwtest.items;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.android.internal.telephony.TelephonyIntents;

import com.fibocom.hwtest.R;

public class FiboSimCardTest extends FiboBaseTest {
    private static final String TAG = "FiboSimCardTest";
    protected static final int MSG_SIM_CHANGED = 1;
    protected static final int UI_REFRESH_WAITTIME = 1000;
    private FiboResultCallback resultCallback = null;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(action)) {
                // for not holds out inserting and pulling out heatedly.
                //setTimerTask(MSG_SIM_CHANGED, UI_REFRESH_WAITTIME);
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fibo_sim_card, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setButtonVisibility(true);
        resultCallback.setResult(FiboBaseTest.TEST_WAIT_PASS);
    }

    @Override
    public void onHandleMessage(final int index) {
        switch (index) {
            case MSG_SIM_CHANGED:
                setSimCardInfo(getView());
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        getActivity().registerReceiver(mReceiver, filter);
        setSimCardInfo(getView());
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public String getTestName() {
        return getContext().getString(R.string.sim_title);
    }

    @Override
    public boolean isNeedTest() {
        String radioVersion = Build.getRadioVersion();
        if (radioVersion != null && radioVersion.indexOf("\nNV.WI.V") != -1) {
            return false;
        }
        return getSystemProperties("sim", true);
    }

    @Override
    public int getTestResult() {
        TelephonyManager tm = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
        return (tm.getSimState() == TelephonyManager.SIM_STATE_READY) ? FiboBaseTest.TEST_PASS : FiboBaseTest.TEST_FAIL;
    }

    @Override
    public int getTestTimeout() {
        return FiboBaseTest.NO_TIMEOUT;
    }

    @Override
    public boolean setTestCallback(FiboResultCallback callback) {
        this.resultCallback = callback;
        return true;
    }

    private void setSimCardInfo(View v) {
        boolean isSimCardReady = false;
        boolean isSimCardNotExist = false;
        TextView status = (TextView) v.findViewById(R.id.sim_status);
        TextView iccid = (TextView) v.findViewById(R.id.sim_iccid);
        TextView network_type = (TextView) v.findViewById(R.id.sim_network_type);
        TextView network_operator = (TextView) v.findViewById(R.id.sim_network_operator);
        TextView network_operator_name = (TextView) v.findViewById(R.id.sim_network_operator_name);

        TelephonyManager tm = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
        switch (tm.getSimState()) {
            case TelephonyManager.SIM_STATE_READY:
                status.setText("SIM卡已启用");
                isSimCardReady = true;
                break; 
            case TelephonyManager.SIM_STATE_ABSENT:
                status.setText("SIM卡缺失");
                isSimCardNotExist = true;
                break; 
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                status.setText("SIM卡需要网络解锁");
                break; 
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                status.setText("SIM卡需要PIN解锁");
                break; 
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                status.setText("SIM卡需要PUK解锁");
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                status.setText("SIM卡状态未知");
                break;
            default:
                status.setText("SIM卡状态异常");
                break;
        }

        if (isSimCardNotExist) {
            iccid.setText("<Unknown>");
            network_type.setText("<Unknown>");
            network_operator.setText("<Unknown>");
            network_operator_name.setText("<Unknown>");
        } else {
            if (tm.getSimSerialNumber() == null) {
                iccid.setText("无法取得SIM卡号");
            } else {
                iccid.setText(tm.getSimSerialNumber().toString());
            }

            if (tm.getNetworkType() == 0) {
                network_type.setText("无法取得网络类型");
            } else {
                network_type.setText(Integer.toString(tm.getNetworkType()));
            }

            if (tm.getNetworkOperator().equals("")) {
                network_operator.setText("无法取得网络运营商");
            } else {
                network_operator.setText(tm.getNetworkOperator());
            }

            if (tm.getNetworkOperatorName().equals("")) {
                network_operator_name.setText("无法取得网络运营商名称");
            } else {
                network_operator_name.setText(tm.getNetworkOperatorName());
            }
        }

        int textColor = isSimCardReady ? 0xff00ff00 : 0xffcd2626;
        status.setTextColor(textColor);
        iccid.setTextColor(textColor);
        network_type.setTextColor(textColor);
        network_operator.setTextColor(textColor);
        network_operator_name.setTextColor(textColor);
        setButtonVisibility(true);
        resultCallback.setResult(isSimCardReady ? FiboBaseTest.TEST_PASS : FiboBaseTest.TEST_FAIL);
    }
}
