package com.fibocom.hwtest.items;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemProperties;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fibocom.hwtest.R;

public class FiboSdCardTest extends FiboBaseTest {
    private static final String TAG = "FiboSdCardTest";
    protected static final int MSG_STATUS_CHANGED = 1;
    protected static final int UI_REFRESH_WAITTIME = 2000;
    private FiboResultCallback resultCallback = null;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Intent.ACTION_MEDIA_MOUNTED.equals(action)
                    || Intent.ACTION_MEDIA_UNMOUNTED.equals(action)
                    || Intent.ACTION_MEDIA_REMOVED.equals(action)
                    || Intent.ACTION_MEDIA_BAD_REMOVAL.equals(action)) {
                setTimerTask(MSG_STATUS_CHANGED, UI_REFRESH_WAITTIME);
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fibo_sd_card, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setButtonVisibility(true);
        resultCallback.setResult(FiboBaseTest.TEST_WAIT_PASS);
    }

    @Override
    public void onHandleMessage(final int index) {
        switch (index) {
            case MSG_STATUS_CHANGED:
                setInnerSdCardInfo(getView());
                setExternalSdCardInfo(getView());
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_BAD_REMOVAL);
        filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_REMOVED);
        filter.addDataScheme("file");
        getActivity().registerReceiver(mReceiver, filter);
        setInnerSdCardInfo(getView());
        setExternalSdCardInfo(getView());
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public String getTestName() {
        return getContext().getString(R.string.sdcard_title);
    }

    @Override
    public boolean isNeedTest() {
        return getSystemProperties("storage", true);
    }

    @Override
    public int getTestResult() {
        return isExternalSdCardExist() ? FiboBaseTest.TEST_PASS : FiboBaseTest.TEST_FAIL;
    }

    @Override
    public int getTestTimeout() {
        return FiboBaseTest.NO_TIMEOUT;
    }

    @Override
    public boolean setTestCallback(FiboResultCallback callback) {
        this.resultCallback = callback;
        return true;
    }

    private void setInnerSdCardInfo(View v) {
        TextView sdcard_path = (TextView) v.findViewById(R.id.sdcard_inner_path);
        TextView sdcard_status = (TextView) v.findViewById(R.id.sdcard_inner_status);
        TextView sdcard_total = (TextView) v.findViewById(R.id.sdcard_inner_total_size);
        TextView sdcard_available = (TextView) v.findViewById(R.id.sdcard_inner_available_size);

        boolean state = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
        if (state) {
            long totalSpace = Environment.getExternalStorageDirectory().getTotalSpace();
            long availableSpace = Environment.getExternalStorageDirectory().getUsableSpace();
            sdcard_path.setText(Environment.getExternalStorageDirectory().getPath());
            sdcard_status.setText("normal");
            sdcard_total.setText(Formatter.formatFileSize(getContext(), totalSpace));
            sdcard_available.setText(Formatter.formatFileSize(getContext(), availableSpace));
        } else {
            sdcard_path.setText("N/A");
            sdcard_status.setText("missing or error");
            sdcard_total.setText("N/A");
            sdcard_available.setText("N/A");
        }
        int textColor = state ? 0xff00ff00 : 0xffcd2626;
        sdcard_path.setTextColor(textColor);
        sdcard_status.setTextColor(textColor);
        sdcard_total.setTextColor(textColor);
        sdcard_available.setTextColor(textColor);
    }

    private void setExternalSdCardInfo(View v) {
        TextView sdcard_path = (TextView) v.findViewById(R.id.sdcard_external_path);
        TextView sdcard_status = (TextView) v.findViewById(R.id.sdcard_external_status);
        TextView sdcard_total = (TextView) v.findViewById(R.id.sdcard_external_total_size);
        TextView sdcard_available = (TextView) v.findViewById(R.id.sdcard_external_available_size);

        boolean state = isExternalSdCardExist();
        if (state) {
            StatFs stat = new StatFs(getExternalSdCardPath());
            long blockSize = stat.getBlockSize();
            long totalBlockCount = stat.getBlockCount();
            long availableBlockCount = stat.getAvailableBlocks();
            long totalSpace = totalBlockCount * blockSize;
            long availableSpace = availableBlockCount * blockSize;
            sdcard_path.setText(getExternalSdCardPath());
            sdcard_status.setText("normal");
            sdcard_total.setText(Formatter.formatFileSize(getContext(), totalSpace));
            sdcard_available.setText(Formatter.formatFileSize(getContext(), availableSpace));
        } else {
            sdcard_path.setText("N/A");
            sdcard_status.setText("missing or error");
            sdcard_total.setText("N/A");
            sdcard_available.setText("N/A");
        }
        int textColor = state ? 0xff00ff00 : 0xffcd2626;
        sdcard_path.setTextColor(textColor);
        sdcard_status.setTextColor(textColor);
        sdcard_total.setTextColor(textColor);
        sdcard_available.setTextColor(textColor);
        if (!state) {
            ((TextView) v.findViewById(R.id.prompt_text)).setText(R.string.sdcard_unmounting);
        }
        setButtonVisibility(true);
        resultCallback.setResult(state ? FiboBaseTest.TEST_PASS : FiboBaseTest.TEST_FAIL);
    }

    private boolean isExternalSdCardExist() {
        try {
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec("mount");
            InputStream is = proc.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line = null;
            while ((line = br.readLine()) != null) {
                if (line.contains("/storage/sdcard")) {
                    String [] arr = line.split(" ");
                    File file = new File(arr[1]);
                    if (file.isDirectory()) {
                        isr.close();
                        return true;
                    }
                }
            }
            isr.close();
        } catch (Exception e) {
        }
        return false;
    }

    private String getExternalSdCardPath() {
        String path = null;
        try {
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec("mount");
            InputStream is = proc.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line = null;
            while ((line = br.readLine()) != null) {
                if (line.contains("/storage/sdcard")) {
                    String [] arr = line.split(" ");
                    path = arr[1];
                    File file = new File(path);
                    if (file.isDirectory()) {
                        isr.close();
                        return path;
                    }
                }
            }
            isr.close();
        } catch (Exception e) {
        }
        return "N/A";
    }
}
