package com.fibocom.hwtest;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.internal.policy.impl.GosoMmiPolicy;
import com.fibocom.hwtest.HardwareTestDialog;
import com.fibocom.hwtest.items.FiboBaseTest;
import com.fibocom.hwtest.items.FiboResultCallback;

public class HardwareTestItemActivity extends Activity {
    private static final String TAG = "HardwareTestItemActivity";
    private static final String MOUSE_RIGHT_CLICK_ACTION = "MouseRightClickAction";
    private static final String PREFS = "test_prefs";
    private static final String KEY_SINGLE_MODE = "single_mode";
    private static final int AUTO_SET = 0;
    private static final int MANUAL_SET = 1;
    private static final int AUTO_GET_RESULT_DEFTIME = 10000;
    private static final int AUTO_PASS_WAITTIME = 250;
    private static final int AUTO_FAIL_WAITTIME = 1000;

    private GestureDetector mDetector;
    private int mIndex = 0;
    private int mResult = 0;
    private TextView prompt = null;
    private LinearLayout confirm = null;
    private LinearLayout dismiss = null;
    private Button btnDismiss = null;
    private String promptTitle = null;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDetector = new GestureDetector(this, new OnGestureListener());

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.fibo_test_items);

        mIndex = getIntent().getIntExtra("ITEM_INDEX", 0);
        setTitle(HardwareTestList.get(mIndex).getTestName());
        addFragment(HardwareTestList.get(mIndex));
        
        prompt = (TextView) findViewById(R.id.prompt_title);
        confirm = (LinearLayout) findViewById(R.id.confirm_button);
        dismiss = (LinearLayout) findViewById(R.id.dismiss_button);
        btnDismiss = (Button) findViewById(R.id.btn_dismiss);
        updateConfirmDialog();
    }

    @Override
    protected void onResume() {
        super.onResume();
        GosoMmiPolicy mMmiPolicy = GosoMmiPolicy.getInstance(getApplicationContext());
        mMmiPolicy.setFtm(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        GosoMmiPolicy mMmiPolicy = GosoMmiPolicy.getInstance(getApplicationContext());
        mMmiPolicy.setFtm(false);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_HOME:
            case KeyEvent.KEYCODE_BACK:
                //buildExitConfirmDialog();
                //return true;
            default:
                break;
        }
        if (HardwareTestList.get(mIndex).onKeyDown(keyCode, event)) {
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        HardwareTestList.get(mIndex).onNewIntent(intent);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int button = event.getButtonState();
        int action = event.getAction();

        /* Only for USB mouse test. */
        if(button == MotionEvent.BUTTON_SECONDARY && action == MotionEvent.ACTION_DOWN) {
            Log.i(TAG, "USB mouse right-clicking!");
            Intent mIntent = new Intent(MOUSE_RIGHT_CLICK_ACTION);
            mIntent.putExtra("state", 1);
            sendBroadcast(mIntent);
            return true;
        }
        return mDetector.onTouchEvent(event);
    }

    private void buildExitConfirmDialog() {
        HardwareTestDialog.Builder builder = new HardwareTestDialog.Builder(HardwareTestItemActivity.this);
        builder.setTitle(">>  稍安勿躁  <<");
        builder.setMessage("你，真滴真滴确认不是点错了？");

        builder.setPositiveButton("失误失误", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("走啦走啦", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                HardwareTestItemActivity.this.finish();
            }
        });

        builder.create().show();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_pass:
                waitHandler.removeCallbacks(waitRunnable);
                nextTestItem(MANUAL_SET, 1);
                break;
            case R.id.btn_fail:
                waitHandler.removeCallbacks(waitRunnable);
                nextTestItem(MANUAL_SET, 0);
                break;
            case R.id.btn_dismiss:
                finish();
                break;
        }
    }

    private void updateConfirmDialog() {
        HardwareTestList.get(mIndex).setTestCallback(new FiboResultCallback() {
            @Override
            public void setResult(int result) {
                mResult = result;
                setTestResultDialog();
            }
        });
        int timeout = HardwareTestList.get(mIndex).getTestTimeout();
        timeout = (timeout == 0) ? AUTO_GET_RESULT_DEFTIME : timeout;
        initConfirmDialog(mResult);
        if (timeout > 0) {
            waitHandler.postDelayed(waitRunnable, timeout);
        }
    }

    Handler waitHandler = new Handler();

    Runnable waitRunnable = new Runnable() {
        @Override
        public void run() {
            mResult = HardwareTestList.get(mIndex).getTestResult();
            setTestResultDialog();
        } 
    };

    private void setTestResultDialog() {
        findViewById(R.id.buttons).setVisibility(View.VISIBLE);
        if (mResult == FiboBaseTest.TEST_INSPECTION || mResult == FiboBaseTest.TEST_WAIT_PASS) {
            initConfirmDialog(mResult);
        } else {
            setConfirmDialog(mResult);
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    nextTestItem(AUTO_SET, ((mResult < 0) ? 0 : 1));
                }
            }, ((mResult > 0) ? AUTO_PASS_WAITTIME : AUTO_FAIL_WAITTIME));
        }
    }

    private void initConfirmDialog(int result) {
        promptTitle = getResources().getString(R.string.prompt_prefix) + "『" + HardwareTestList.get(mIndex).getTestName() + "』" + getResources().getString(R.string.prompt_suffix);
        prompt.setText(promptTitle);
        prompt.setTextSize(20.0f);
        prompt.setTextColor(0xff8b2323);
        prompt.setVisibility(View.VISIBLE);
        confirm.setVisibility(View.VISIBLE);
        dismiss.setVisibility(View.GONE);
        Button pass = (Button) findViewById(R.id.btn_pass);
        if (result == FiboBaseTest.TEST_WAIT_PASS) {
            pass.setClickable(false);
            pass.setBackgroundColor(0x8f4f4f4f);
        } else {
            pass.setClickable(true);
            pass.setBackgroundColor(0xff458b00);
        }
    }

    private void setConfirmDialog(int result) {
        promptTitle = "『" + HardwareTestList.get(mIndex).getTestName() + "』测试项，测试" + ((result < 0) ? getResources().getString(R.string.fail_text) : getResources().getString(R.string.pass_text)) + "！";
        prompt.setText(promptTitle);
        prompt.setTextSize(24.0f);
        prompt.setTextColor((result < 0) ? 0xffcd2626 : 0xff458b00);
        prompt.setVisibility(View.VISIBLE);
        confirm.setVisibility(View.GONE);
        dismiss.setVisibility(View.GONE);
        if (isSingleMode()) {
            prompt.setVisibility(View.GONE);
            dismiss.setVisibility(View.VISIBLE);
            btnDismiss.setText(promptTitle);
            btnDismiss.setTextSize(24.0f);
            btnDismiss.setTextColor((result < 0) ? 0xffcd2626 : 0xff458b00);
        }
    }

    private void addFragment(final Fragment newFragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.item_fragment, newFragment).commit();
    }

    private void replaceFragment(final Fragment newFragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.item_fragment, newFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    private void setTestResult(int state) {
        SharedPreferences sp = getSharedPreferences(PREFS, MODE_PRIVATE);
        sp.edit().putInt(String.valueOf(mIndex), state).commit();

        boolean isLandiEdition = (Build.MODEL.equals("SC808") || Build.MODEL.equals("SC820")) ? true : false;
        if (state > 0 && isAllTestItemPassed()) {
            HardwareTestApplication.getInstance().setNvTestResultStatus(true, isLandiEdition);
            Log.i(TAG, "set NV pass!");
        }
        if (state == 0 && isOnlyThisTestItemFailed(mIndex)) {
            HardwareTestApplication.getInstance().setNvTestResultStatus(false, isLandiEdition);
            Log.i(TAG, "set NV fail!");
        }
    }

    private int getOtherTestItemResult(int curIndex) {
        SharedPreferences sp = getSharedPreferences(PREFS, MODE_PRIVATE);
        int cnt = HardwareTestList.getCount();
        boolean isOtherItemsPassed = true;

        for (int idx = 0; idx < cnt; idx++) {
            // pass -> 1; fail -> 0; unknown -> -1;
            if (idx == curIndex) continue;
            int res = sp.getInt(String.valueOf(idx), -1);
            if (res == -1) {
                return -1;
            } else if (res == 0) {
                isOtherItemsPassed = false;	
            }
        }
        return isOtherItemsPassed ? 1 : 0;
    }

    private boolean isAllTestItemPassed() {
        SharedPreferences sp = getSharedPreferences(PREFS, MODE_PRIVATE);
        int cnt = HardwareTestList.getCount();

        for (int idx = 0; idx < cnt; idx++) {
            // pass -> 1; fail -> 0; unknown -> -1;
            if (sp.getInt(String.valueOf(idx), -1) <= 0) {
                return false;
            }
        }
        return true;
    }

    private boolean isOnlyThisTestItemFailed(int failIndex) {
        SharedPreferences sp = getSharedPreferences(PREFS, MODE_PRIVATE);
        int cnt = HardwareTestList.getCount();

        for (int idx = 0; idx < cnt; idx++) {
            // pass -> 1; fail -> 0; unknown -> -1;
            if (idx == failIndex) continue;
            if (sp.getInt(String.valueOf(idx), -1) <= 0) {
                return false;
            }
        }
        return true;
    }

    private boolean isSingleMode() {
        SharedPreferences sp = getSharedPreferences(PREFS, MODE_PRIVATE);
        return sp.getBoolean(KEY_SINGLE_MODE, false);
    }

    private void sendTestResultToListActivity(int index, int state) {
        int res = getOtherTestItemResult(index);
        if (res >= 0) {
            Intent intent = new Intent();
            intent.putExtra("result", ((res > 0 && state > 0) ? "true" : "false"));
            setResult(1, intent);
        }
    }

    private void nextTestItem(int type, int state) {
        setTestResult(state);
        if (isSingleMode()) {
            setResult(RESULT_OK);
            sendTestResultToListActivity(mIndex, state);
            if (MANUAL_SET == type) {
                finish();
            }
        } else {
            mIndex++;
            if (mIndex >= HardwareTestList.getCount()) {
                setResult(RESULT_OK);
                sendTestResultToListActivity(mIndex - 1, state);
                finish();
            } else {
                findViewById(R.id.buttons).setVisibility(View.GONE);
                setTitle(HardwareTestList.get(mIndex).getTestName());
                replaceFragment(HardwareTestList.get(mIndex));
                updateConfirmDialog();
            }
        }
    }

    private class OnGestureListener extends SimpleOnGestureListener {
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            Log.i(TAG, "onDoubleTap on index: " + mIndex);
            return HardwareTestList.get(mIndex).onDoubleTap(e);
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            return HardwareTestList.get(mIndex).onSingleTapConfirmed(e);
        }
    }
}
