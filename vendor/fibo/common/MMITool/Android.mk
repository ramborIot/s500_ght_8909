ifeq ($(JL_NO_CJ),true)

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-java-files-under, src) \

LOCAL_STATIC_JAVA_LIBRARIES := android-support-v4

LOCAL_PREBUILT_JNI_LIBS := libnvctrl.so

LOCAL_PACKAGE_NAME := MMITool
LOCAL_CERTIFICATE := platform
LOCAL_DEX_PREOPT := false
LOCAL_PRIVILEGED_MODULE := true
LOCAL_JAVA_LIBRARIES := android.policy qcrilhook telephony-common

include $(BUILD_PACKAGE)

# Install the srec data files if MMITool.apk is installed to system image.
# include external/srec/config/en.us/config.mk
$(LOCAL_INSTALLED_MODULE) : | $(SREC_CONFIG_TARGET_FILES)
# SREC_CONFIG_TARGET_FILES is from external/srec/config/en.us/config.mk and now can be cleaned up.
# SREC_CONFIG_TARGET_FILES :=

# Use the following include to make our dynamic library.
include $(call all-makefiles-under,$(LOCAL_PATH))

endif
