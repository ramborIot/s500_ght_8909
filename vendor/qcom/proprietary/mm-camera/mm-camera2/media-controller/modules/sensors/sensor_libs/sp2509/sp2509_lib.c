/*============================================================================

  Copyright (c) 2014 Qualcomm Technologies, Inc. All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

============================================================================*/
#include <stdio.h>
#include "sensor_lib.h"

#define SENSOR_MODEL_NO_SP2509 "sp2509"
#define SP2509_LOAD_CHROMATIX(n) \
  "libchromatix_"SENSOR_MODEL_NO_SP2509"_"#n".so"

#define SNAPSHOT_1600X1200_PARMS  1

static sensor_lib_t sensor_lib_ptr;
  
static struct msm_sensor_power_setting power_setting[] = {
  {
    .seq_type = SENSOR_VREG,
    .seq_val = CAM_VIO,
    .config_val = 0,
    .delay = 0,
  },
  {
    .seq_type = SENSOR_VREG,
    .config_val = 0,
    .seq_val = CAM_VANA,
    .delay = 0,
  },
  {
    .seq_type = SENSOR_GPIO,
    .seq_val = SENSOR_GPIO_RESET,
    .config_val = GPIO_OUT_LOW,
    .delay = 0,
  },
  {
    .seq_type = SENSOR_GPIO,
    .seq_val = SENSOR_GPIO_RESET,
    .config_val = GPIO_OUT_HIGH,
    .delay = 10,
  },
  {
    .seq_type = SENSOR_GPIO,
    .seq_val = SENSOR_GPIO_STANDBY,
    .config_val = GPIO_OUT_HIGH/*GPIO_OUT_LOW*/,
    .delay = 0,
  },
  {
    .seq_type = SENSOR_GPIO,
    .seq_val = SENSOR_GPIO_STANDBY,
    .config_val = GPIO_OUT_LOW/*GPIO_OUT_HIGH*/,
    .delay = 5,
  },
  {
    .seq_type = SENSOR_CLK,
    .seq_val = SENSOR_CAM_MCLK,
    .config_val = 24000000,
    .delay = 10,
  },
  {
    .seq_type = SENSOR_I2C_MUX,
    .seq_val = 0,
    .config_val = 0,
    .delay = 0,
  },
};

#if 1
static struct msm_sensor_power_setting power_down_setting[] = {
        {
                .seq_type = SENSOR_I2C_MUX,
                .seq_val = 0,
                .config_val = 0,
                .delay = 0,
        },
        {
                .seq_type = SENSOR_CLK,
                .seq_val = SENSOR_CAM_MCLK,
                .config_val = 24000000,
                .delay = 5,
        },
        {
                .seq_type = SENSOR_GPIO,
                .seq_val = SENSOR_GPIO_STANDBY,
                .config_val = GPIO_OUT_HIGH,
                .delay = 1,
        },
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_RESET,
		.config_val = GPIO_OUT_LOW,
		.delay = 1,
	},
	{
		.seq_type = SENSOR_VREG,
		.seq_val = CAM_VIO,
		.config_val = 0,
		.delay = 1,
	},
	{
		.seq_type = SENSOR_VREG,
		.seq_val = CAM_VANA,
		.config_val = 0,
		.delay = 1,
	}
};

#endif
static struct msm_camera_sensor_slave_info sensor_slave_info = {
  /* Camera slot where this camera is mounted */
  .camera_id = CAMERA_1,
  /* sensor slave address */
  .slave_addr = 0x7a,
  /* sensor i2c frequency*/
  .i2c_freq_mode = I2C_FAST_MODE,
  /* sensor address type */
  .addr_type = MSM_CAMERA_I2C_BYTE_ADDR,
  /* sensor id info*/
  .sensor_id_info = {
    /* sensor id register address */
    .sensor_id_reg_addr = 0x02,
    /* sensor id */
    .sensor_id = 0x2509,
  },
  /* power up / down setting */
  .power_setting_array = {
    .power_setting = power_setting,
    .size = ARRAY_SIZE(power_setting),
    .power_down_setting = power_down_setting,
    .size_down = ARRAY_SIZE(power_down_setting),
  },
};

static struct msm_sensor_init_params sensor_init_params = {
  .modes_supported = 0,
  .position = 1,
  .sensor_mount_angle = 90,
  //.sensor_mount_angle = 270,
};

static sensor_output_t sensor_output = {
  .output_format = SENSOR_BAYER,
  .connection_mode = SENSOR_MIPI_CSI,
  .raw_output = SENSOR_10_BIT_DIRECT,
};

static struct msm_sensor_output_reg_addr_t output_reg_addr = {
  .x_output = 0xFF,
  .y_output = 0xFF,
  .line_length_pclk = 0xFF,
  .frame_length_lines = 0xFF,
};

static struct msm_sensor_exp_gain_info_t exp_gain_info = {
  .coarse_int_time_addr = 0xFF,
  .global_gain_addr = 0xFF,
  .vert_offset = 0,
};

static sensor_aec_data_t aec_info = {
  .max_gain = 9.0,
  .max_linecount = 30000,
};

static sensor_lens_info_t default_lens_info = {
  .focal_length = 2.93,
  .pix_size = 1.75,
  .f_number = 2.8,
  .total_f_dist = 1.2,
  .hor_view_angle = 54.8,
  .ver_view_angle = 42.5,
};

#ifndef VFE_40
static struct csi_lane_params_t csi_lane_params = {
  .csi_lane_assign = 0x0004,
  .csi_lane_mask = 0x18,
  .csi_if = 1,
  .csid_core = {0},
  .csi_phy_sel = 0,
};
#else
static struct csi_lane_params_t csi_lane_params = {
  .csi_lane_assign = 0x4320,
  .csi_lane_mask = 0x3,
  .csi_if = 1,
  .csid_core = {0},
  .csi_phy_sel = 1,

};
#endif

static struct msm_camera_i2c_reg_array init_reg_array0[] = {
#if 1

{0xfd,0x00},
{0x2f,0x04},
{0x34,0x00},
{0x35,0x00},
{0x30,0x1d},
{0x33,0x05},
{0xfd,0x01},
{0x44,0x00},
{0x2a,0x4c},
{0x2b,0x1e},
{0x2c,0x60},
{0x25,0x11},
{0x03,0x03},
{0x04,0xb3},
{0x09,0x00},
{0x0a,0x02},
{0x06,0x0a},
{0x24,0x60},
{0x3f,0x03},//mirror & updown
{0x01,0x01},
{0xfb,0x73},
{0xfd,0x01},
{0x16,0x04},
{0x1c,0x09},
{0x21,0x46},
{0x6c,0x00},
{0x6b,0x00},
{0x84,0x00},
{0x85,0x10},
{0x86,0x10},
{0x12,0x04},
{0x13,0x40},
{0x11,0x20},
{0x33,0x40},
{0xd0,0x03},
{0xd1,0x01},
{0xd2,0x00},
{0xd3,0x01},
{0xd4,0x20},
{0x51,0x14},
{0x52,0x10},
{0x55,0x30},
{0x58,0x10},
{0x71,0x10},
{0x6f,0x40},
{0x75,0x60},
{0x76,0x10},
{0x8a,0x22},
{0x8b,0x22},
{0x19,0x71},
{0x29,0x01},
{0xfd,0x01},
{0x9d,0xea},
{0xa0,0x05},
{0xa1,0x02},
{0xad,0x62},
{0xae,0x00},
{0xaf,0x85},
{0xb1,0x01},
//{0xac,0x01},
{0xfd,0x01},
{0xfc,0x10},
{0xfe,0x10},
{0xf9,0x00},
{0xfa,0x00},
  
#else
  {0xfd,0x00},
  {0xfd,0x00},
  {0x35,0x20}, //pll bias
  {0x30,0x05},//
  {0x33,0x05},//
  {0x2f,0x10}, //0x10 pll clk 84M//0x08 pll clk 60M
  {0x1c,0x03}, //pull down parallel pad
  {0x1b,0x1f},
  
  {0xfd,0x01},
  {0x21,0xef},//3base fliker gpw20140607
  {0x31,0x00},//
  {0x3f,0x00},
  {0x03,0x03}, //exp time, 1 base
  {0x04,0x09},
  {0x06,0x00}, //vblank
  {0x24,0x60}, //pga gain 15x
  {0x01,0x01}, //enable reg write
  {0x2b,0xc4}, //readout vref
  {0x2e,0x20}, //dclk delay
  {0x79,0x42}, //p39 p40
  {0x85,0x0f}, //p51
  {0x09,0x01}, //hblank
  {0x0a,0x40},
  {0x25,0xf2}, //reg dac 2.7v, enable bl_en,vbl 1.4v
  {0x26,0x00}, //vref2 1v, disable ramp driver
  {0x2a,0xea}, //bypass dac res, adc range 0.745, vreg counter 0.9
  {0x2c,0xf0}, //high 8bit, pldo 2.7v
  {0x8a,0x55}, //pixel bias 2uA //pixel 1ua //20140607 0x33
  {0x8b,0x55}, 
  {0x19,0xf3}, //icom1 1.7u, icom2 0.6u 
  {0x11,0x30}, //rst num
  {0xd0,0x01}, //boost2 enable
  {0xd1,0x01}, //boost2 start point h'1do
  {0xd2,0xd0},  
  {0x55,0x10},
  {0x58,0x40},//0x30 //20140607
  {0x5d,0x15},
  {0x5e,0x05},
  {0x64,0x40},
  {0x65,0x00},
  {0x66,0x66},
  {0x67,0x00},
  {0x68,0x68},
  {0x72,0x70},
  {0x75,0x50},//
  {0x76,0x05},//
  {0x14,0x03},//
  {0x15,0xff},//
  {0xd5,0x22},//
  {0xd6,0x80},//
  {0xd8,0x80},//
  {0xda,0x40},//
  {0xdc,0x80},//
  {0xdd,0x02},//
  {0xde,0x80},//
  {0xea,0x80},//
  {0xeb,0x02},//
  {0xfb,0x25},//{0xfb,0x25},
  {0xfd,0x01}, //mipi
  {0xb3,0x00},
  {0x93,0x01},
  {0x9d,0x17},
  {0xc5,0x01},
  {0xc6,0x00},
  {0xb1,0x01},
  {0x8e,0x06},
  {0x8f,0x40},
  {0x90,0x04},
  {0x91,0xb0},
  {0x92,0x01},
  {0xa1,0x05},
  {0xaa,0x00},
#endif
  {0xff,0xff},
};

static struct msm_camera_i2c_reg_setting init_reg_setting[] = {
  {
    .reg_setting = init_reg_array0,
    .size = ARRAY_SIZE(init_reg_array0),
    .addr_type = MSM_CAMERA_I2C_BYTE_ADDR,
    .data_type = MSM_CAMERA_I2C_BYTE_DATA,
    .delay = 10,
  },
};

static struct sensor_lib_reg_settings_array init_settings_array = {
  .reg_settings = init_reg_setting,
  .size = 1,
};

static struct msm_camera_i2c_reg_array start_reg_array[] = {
#if 1
  {0xfd, 0x01},
  {0xac, 0x01},
#endif
//  {0xff,0xff},
};

static  struct msm_camera_i2c_reg_setting start_settings = {
  .reg_setting = start_reg_array,
  .size = ARRAY_SIZE(start_reg_array),
  .addr_type = MSM_CAMERA_I2C_BYTE_ADDR,
  .data_type = MSM_CAMERA_I2C_BYTE_DATA,
  .delay = 0,
};

static struct msm_camera_i2c_reg_array stop_reg_array[] = {
#if 1
  {0xfd,0x01},
  {0xac,0x00},
#endif
//  {0xff,0xff},
};

static struct msm_camera_i2c_reg_setting stop_settings = {
  .reg_setting = stop_reg_array,
  .size = ARRAY_SIZE(stop_reg_array),
  .addr_type = MSM_CAMERA_I2C_BYTE_ADDR,
  .data_type = MSM_CAMERA_I2C_BYTE_DATA,
  .delay = 0,
};

static struct msm_camera_i2c_reg_array groupon_reg_array[] = {
  {0xff,0xff},
};

static struct msm_camera_i2c_reg_setting groupon_settings = {
  .reg_setting = groupon_reg_array,
  .size = ARRAY_SIZE(groupon_reg_array),
  .addr_type = MSM_CAMERA_I2C_BYTE_ADDR,
  .data_type = MSM_CAMERA_I2C_BYTE_DATA,
  .delay = 0,
};

static struct msm_camera_i2c_reg_array groupoff_reg_array[] = {
  {0xff,0xff},
};

static struct msm_camera_i2c_reg_setting groupoff_settings = {
  .reg_setting = groupoff_reg_array,
  .size = ARRAY_SIZE(groupoff_reg_array),
  .addr_type = MSM_CAMERA_I2C_BYTE_ADDR,
  .data_type = MSM_CAMERA_I2C_BYTE_DATA,
  .delay = 0,
};

static struct msm_camera_csid_vc_cfg sp2509_cid_cfg[] = {
  {0, CSI_RAW10, CSI_DECODE_10BIT},
  {1, CSI_EMBED_DATA, CSI_DECODE_8BIT},
};

static struct msm_camera_csi2_params sp2509_csi_params = {
  .csid_params = {
    .lane_cnt = 1,
    .lut_params = {
      .num_cid = ARRAY_SIZE(sp2509_cid_cfg),
      .vc_cfg = {
         &sp2509_cid_cfg[0],
         &sp2509_cid_cfg[1],
      },
    },
  },
  .csiphy_params = {
    .lane_cnt = 1,
    .settle_cnt = 0x15,
#ifndef VFE_40
    .combo_mode = 1,
#endif
  },
};

static struct sensor_pix_fmt_info_t sp2509_pix_fmt0_fourcc[] = {
  { V4L2_PIX_FMT_SRGGB10 },
};

static struct sensor_pix_fmt_info_t sp2509_pix_fmt1_fourcc[] = {
  { MSM_V4L2_PIX_FMT_META },
};

static sensor_stream_info_t sp2509_stream_info[] = {
  {1, &sp2509_cid_cfg[0], sp2509_pix_fmt0_fourcc},
  {1, &sp2509_cid_cfg[1], sp2509_pix_fmt1_fourcc},
};

static sensor_stream_info_array_t sp2509_stream_info_array = {
  .sensor_stream_info = sp2509_stream_info,
  .size = ARRAY_SIZE(sp2509_stream_info),
};

static struct msm_camera_i2c_reg_array res0_reg_array[] = {
#if 0
//stream off
  {0xfd,0x02},
  {0x36,0x02},
  {0xfd,0x01},
  {0xe7,0x03},
  {0xe7,0x00},
  {0xfd,0x01},
  {0xac,0x00},
#endif

#if 0
   //res settings
  {0xfd,0x00},
  {0xfd,0x00},
  {0x35,0x20}, //pll bias
  {0x2f,0x08}, //0x10 pll clk 84M//0x08 pll clk 60M
  {0x1c,0x03}, //pull down parallel pad
  {0x1b,0x1f},
  
  {0xfd,0x01},
  {0x21,0xee},//3base fliker gpw20140607
  {0x03,0x03}, //exp time, 1 base
  {0x04,0x09},
  {0x06,0x10}, //vblank
  {0x24,0x60}, //pga gain 15x
  {0x01,0x01}, //enable reg write
  {0x2b,0xc6}, //readout vref
  {0x2e,0x20}, //dclk delay
  {0x79,0x42}, //p39 p40
  {0x85,0x0f}, //p51
  {0x09,0x01}, //hblank
  {0x0a,0x40},
  {0x25,0xf2}, //reg dac 2.7v, enable bl_en,vbl 1.4v
  {0x26,0x00}, //vref2 1v, disable ramp driver
  {0x2a,0xea}, //bypass dac res, adc range 0.745, vreg counter 0.9
  {0x2c,0xf0}, //high 8bit, pldo 2.7v
  {0x8a,0x33}, //pixel bias 2uA //pixel 1ua //20140607 0x33
  {0x8b,0x33}, 
  {0x19,0xf3}, //icom1 1.7u, icom2 0.6u 
  {0x11,0x30}, //rst num
  {0xd0,0x00}, //boost2 enable
  {0xd1,0x01}, //boost2 start point h'1do
  {0xd2,0xd0},  
  {0x55,0x10},
  {0x58,0x40},//0x30 //20140607
  {0x5d,0x15},
  {0x5e,0x05},
  {0x64,0x40},
  {0x65,0x00},
  {0x66,0x66},
  {0x67,0x00},
  {0x68,0x68},
  {0x72,0x70},
  {0xfb,0x00},//{0xfb,0x25},
  {0xfd,0x02}, //raw data digital gain
  {0x00,0x8c},
  {0x01,0x8c},
  {0x03,0x8c},
  {0x04,0x8c},  
  {0xfd,0x01}, //mipi
  {0xb3,0x00},
  {0x93,0x01},
  {0x9d,0x17},
  {0xc5,0x01},
  {0xc6,0x00},
  {0xb1,0x01},
  {0x8e,0x06},
  {0x8f,0x50},
  {0x90,0x04},
  {0x91,0xc0},
  {0x92,0x01},
  {0xa1,0x05},
  {0xaa,0x00},
  //{0xac,0x01},
  //{0xaa,0x00},//feilianxu add yy
  //{0xaa,0a01},//lianxu  add  yyj
#endif
#if 0
//stream on
  {0xfd, 0x01},
  {0xac, 0x01},
  {0xfd, 0x02},
  {0x36, 0x00},
#endif
  {0xff,0xff},
};

static struct msm_camera_i2c_reg_setting res_settings[] = {
#if SNAPSHOT_1600X1200_PARMS
  {
    .reg_setting = res0_reg_array,
    .size = ARRAY_SIZE(res0_reg_array),
    .addr_type = MSM_CAMERA_I2C_BYTE_ADDR,
    .data_type = MSM_CAMERA_I2C_BYTE_DATA,
    .delay = 0,
  },
#endif
};

static struct sensor_lib_reg_settings_array res_settings_array = {
  .reg_settings = res_settings,
  .size = ARRAY_SIZE(res_settings),
};

static struct msm_camera_csi2_params *csi_params[] = {
#if SNAPSHOT_1600X1200_PARMS
  &sp2509_csi_params, /* RES 0*/
#endif
};

static struct sensor_lib_csi_params_array csi_params_array = {
  .csi2_params = &csi_params[0],
  .size = ARRAY_SIZE(csi_params),
};

static struct sensor_crop_parms_t crop_params[] = {
#if SNAPSHOT_1600X1200_PARMS
  {0, 0, 0, 0}, /* RES 0 */
#endif
};

static struct sensor_lib_crop_params_array crop_params_array = {
  .crop_params = crop_params,
  .size = ARRAY_SIZE(crop_params),
};

static struct sensor_lib_out_info_t sensor_out_info[] = {
#if SNAPSHOT_1600X1200_PARMS
  {
    .x_output = 1616,
    .y_output = 1216,
    .line_length_pclk = 1894,//2790,//2316,
    .frame_length_lines = 1234,//869,
    .vt_pixel_clk = 48000000,
    .op_pixel_clk = 48000000,
    .binning_factor = 1,
    .max_fps = 30.0,
    .min_fps = 7.5,
    .mode = SENSOR_DEFAULT_MODE,
  },
#endif
};

static struct sensor_lib_out_info_array out_info_array = {
  .out_info = sensor_out_info,
  .size = ARRAY_SIZE(sensor_out_info),
};

static sensor_res_cfg_type_t sp2509_res_cfg[] = {
  SENSOR_SET_STOP_STREAM,
  SENSOR_SET_NEW_RESOLUTION, /* set stream config */
  SENSOR_SET_CSIPHY_CFG,
  SENSOR_SET_CSID_CFG,
  SENSOR_LOAD_CHROMATIX, /* set chromatix prt */
  SENSOR_SEND_EVENT, /* send event */
  SENSOR_SET_START_STREAM,
};

static struct sensor_res_cfg_table_t sp2509_res_table = {
  .res_cfg_type = sp2509_res_cfg,
  .size = ARRAY_SIZE(sp2509_res_cfg),
};

static struct sensor_lib_chromatix_t sp2509_chromatix[] = {
#if SNAPSHOT_1600X1200_PARMS
  {
    .common_chromatix = SP2509_LOAD_CHROMATIX(common),
    .camera_preview_chromatix = SP2509_LOAD_CHROMATIX(snapshot), /* RES0 */
    .camera_snapshot_chromatix = SP2509_LOAD_CHROMATIX(snapshot), /* RES0 */
    .camcorder_chromatix = SP2509_LOAD_CHROMATIX(snapshot), /* RES0 default_video*/
  },
#endif
};

static struct sensor_lib_chromatix_array sp2509_lib_chromatix_array = {
  .sensor_lib_chromatix = sp2509_chromatix,
  .size = ARRAY_SIZE(sp2509_chromatix),
};

/*===========================================================================
 * FUNCTION    - SP2509_real_to_register_gain -
 *
 * DESCRIPTION:
 *==========================================================================*/
static uint16_t sp2509_real_to_register_gain(float gain)
{
    uint16_t reg_gain;
    if (gain < 1.0)
        gain = 1.0;

    if (gain > 6.0)
        gain = 6.0;    
        
    reg_gain = (uint16_t)(gain * 16.0f);
    return reg_gain;
}

/*===========================================================================
 * FUNCTION    - SP2509_register_to_real_gain -
 *
 * DESCRIPTION:
 *==========================================================================*/
static float sp2509_register_to_real_gain(uint16_t reg_gain)
{
    float gain;
    if (reg_gain > 0x60)  //6x
        reg_gain = 0x60;
  
    gain = (float)(reg_gain/16.0f);
    return gain;
}

/*===========================================================================
 * FUNCTION    - SP2509_calculate_exposure -
 *
 * DESCRIPTION:
 *==========================================================================*/
static int32_t sp2509_calculate_exposure(float real_gain,
  uint16_t line_count, sensor_exposure_info_t *exp_info)
{
  if (!exp_info) {
    return -1;
  }
  exp_info->reg_gain = sp2509_real_to_register_gain(real_gain);
  exp_info->sensor_real_gain = sp2509_register_to_real_gain(exp_info->reg_gain);
  exp_info->digital_gain = real_gain / exp_info->sensor_real_gain;
  exp_info->line_count = line_count;
  exp_info->sensor_digital_gain = 0x1;
  return 0;
}

/*===========================================================================
 * FUNCTION    - SP2509_fill_exposure_array -
 *
 * DESCRIPTION:
 *==========================================================================*/
static int32_t sp2509_fill_exposure_array(uint16_t gain, uint32_t line,
  uint32_t fl_lines, int32_t luma_avg, uint32_t fgain,
  struct msm_camera_i2c_reg_setting* reg_setting)
{
  int32_t rc = 0;
  uint16_t reg_count = 0;
  uint16_t i = 0;
  uint16_t iReg = 0;

  if (!reg_setting) {
    return -1;
  }
  
#if 0
  for (i = 0; i < sensor_lib_ptr.groupon_settings->size; i++) {
    reg_setting->reg_setting[reg_count].reg_addr =
      sensor_lib_ptr.groupon_settings->reg_setting[i].reg_addr;
    reg_setting->reg_setting[reg_count].reg_data =
      sensor_lib_ptr.groupon_settings->reg_setting[i].reg_data;
    reg_count = reg_count + 1;
  }
#endif

#if 1
   iReg = gain;

   reg_setting->reg_setting[reg_count].reg_addr = 0xfd;
   reg_setting->reg_setting[reg_count].reg_data = 0x01;
   reg_count++;
   reg_setting->reg_setting[reg_count].reg_addr = 0x03;
   reg_setting->reg_setting[reg_count].reg_data = (line >> 8) & 0xFF;
   reg_count++;
   reg_setting->reg_setting[reg_count].reg_addr = 0x04;
   reg_setting->reg_setting[reg_count].reg_data =  line & 0xFF;
   reg_count++;

  iReg = iReg < 0x10?0x10:iReg>0x60?0x60:iReg;
  reg_setting->reg_setting[reg_count].reg_addr = 0x24;
  reg_setting->reg_setting[reg_count].reg_data = (uint8_t)iReg;
   
  reg_count++;
  reg_setting->reg_setting[reg_count].reg_addr = 0x01;
  reg_setting->reg_setting[reg_count].reg_data = 0x01;
  reg_count++;
#endif

#if 0
  for (i = 0; i < sensor_lib_ptr.groupoff_settings->size; i++) {
    reg_setting->reg_setting[reg_count].reg_addr =
      sensor_lib_ptr.groupoff_settings->reg_setting[i].reg_addr;
    reg_setting->reg_setting[reg_count].reg_data =
      sensor_lib_ptr.groupoff_settings->reg_setting[i].reg_data;
    reg_count = reg_count + 1;
  }
#endif

  reg_setting->size = reg_count;
  reg_setting->addr_type = MSM_CAMERA_I2C_BYTE_ADDR;
  reg_setting->data_type = MSM_CAMERA_I2C_BYTE_DATA;
  reg_setting->delay = 0;

  return rc;
}

static sensor_exposure_table_t sp2509_expsoure_tbl = {
  .sensor_calculate_exposure = sp2509_calculate_exposure,
  .sensor_fill_exposure_array = sp2509_fill_exposure_array,
};

static sensor_lib_t sensor_lib_ptr = {
  /* sensor slave info */
  .sensor_slave_info = &sensor_slave_info,
  /* sensor init params */
  .sensor_init_params = &sensor_init_params,
  /* sensor output settings */
  .sensor_output = &sensor_output,
  /* sensor output register address */
  .output_reg_addr = &output_reg_addr,
  /* sensor exposure gain register address */
  .exp_gain_info = &exp_gain_info,
  /* sensor aec info */
  .aec_info = &aec_info,
  /* sensor snapshot exposure wait frames info */
  .snapshot_exp_wait_frames = 1,
  /* number of frames to skip after start stream */
  .sensor_num_frame_skip = 1,
  /* number of frames to skip after start HDR stream */
  .sensor_num_HDR_frame_skip = 1,
  /* sensor pipeline immediate delay */
  .sensor_max_pipeline_frame_delay = 2,
  /* sensor exposure table size */
  .exposure_table_size = 10,
  /* sensor lens info */
  .default_lens_info = &default_lens_info,
  /* csi lane params */
  .csi_lane_params = &csi_lane_params,
  /* csi cid params */
  .csi_cid_params = sp2509_cid_cfg,
  /* csi csid params array size */
  .csi_cid_params_size = ARRAY_SIZE(sp2509_cid_cfg),
  /* init settings */
  .init_settings_array = &init_settings_array,
  /* start settings */
  .start_settings = &start_settings,
  /* stop settings */
  .stop_settings = &stop_settings,
  /* group on settings */
  .groupon_settings = &groupon_settings,
  /* group off settings */
  .groupoff_settings = &groupoff_settings,
  /* resolution cfg table */
  .sensor_res_cfg_table = &sp2509_res_table,
  /* res settings */
  .res_settings_array = &res_settings_array,
  /* out info array */
  .out_info_array = &out_info_array,
  /* crop params array */
  .crop_params_array = &crop_params_array,
  /* csi params array */
  .csi_params_array = &csi_params_array,
  /* sensor port info array */
  .sensor_stream_info_array = &sp2509_stream_info_array,
  /* exposure funtion table */
  .exposure_func_table = &sp2509_expsoure_tbl,
  /* chromatix array */
  .chromatix_array = &sp2509_lib_chromatix_array,
  /* sensor pipeline immediate delay */
  .sensor_max_immediate_frame_delay = 2,
};

/*===========================================================================
 * FUNCTION    - sp2509_open_lib -
 *
 * DESCRIPTION:
 *==========================================================================*/
void *sp2509_open_lib(void)
{
  return &sensor_lib_ptr;
}
