
/******************************************************************************
  @file    qcril_qmi_ota_daemon.c
  @brief   a seperate thread for upgrading MBN in EFS

  DESCRIPTION
    Provide a seperate thread for updgrading MBN in modem EFS, and trigger
    modem SSR if needed

  ---------------------------------------------------------------------------

  Copyright (c) 2015 Qualcomm Technologies, Inc. All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/

#include <android/log.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <utils/Log.h>
#include "pdc_utils.h"
#include "mbn_utils.h"
#include "qcrili.h"

#define MAX_SUBS                    3
#define OEM_VERSION_LENGTH          4
#define MAX_MBN_NAME_LENGTH         128
#define SYSTEM_MBN_FILE_PATH        "/data/misc/radio/modem_config/"

#define OTA_CHECK_UPDATE_ERROR          -1
#define OTA_NO_UPDATED_MBN              0
#define OTA_UPDATE_NONE_ACTIVATED_MBN   1
#define OTA_UPDATE_ACTIVATED_MBN        2

//Chenyb added for mbn ota in 20170315 begin
#define HW_PLATFORM			        "/sys/devices/soc0/hw_platform"
#define MTP							"MTP"
//Chenyb added for mbn ota in 20170315 end

struct efs_modem_config_info {
    struct pdc_config_pdc_info  config_id;
    struct pdc_config_item_info config_info;
    uint32_t                    is_current:1;
    uint32_t                    sub:3;
};

static void dump_mbn_list_info(struct efs_modem_config_info* p_modem_list, int n_list)
{
    int idx, subs;
    uint32_t len;
    char *tmp_str;

    if (!p_modem_list || !n_list) {
        RLOGI("no MBN in modem EFS.");
        return;
    }

    len = (PDC_CONFIG_DESC_SIZE_MAX_V01 > PDC_CONFIG_ID_SIZE_MAX_V01) ?
        PDC_CONFIG_DESC_SIZE_MAX_V01 : PDC_CONFIG_ID_SIZE_MAX_V01;
    len += 1;
    if ((tmp_str = malloc(len)) == NULL) {
        RLOGE("Failed to alloc memory to hold config_id/config_desc for dump purpose");
        return;
    }

    RLOGI("current MBNs in modem EFS:");
    for (idx = 0; idx < n_list; idx++) {
        /* show config_id */
        len = (p_modem_list[idx].config_id.config_id_len < PDC_CONFIG_ID_SIZE_MAX_V01) ?
            p_modem_list[idx].config_id.config_id_len : PDC_CONFIG_ID_SIZE_MAX_V01;
        memcpy(tmp_str, p_modem_list[idx].config_id.config_id, len);
        tmp_str[len] = '\0';
        RLOGI("%2d: config_id: %s", idx, tmp_str);
        /* show config_desc */
        if (p_modem_list[idx].config_info.config_desc_valid) {
            len = (p_modem_list[idx].config_info.config_desc_len < PDC_CONFIG_DESC_SIZE_MAX_V01) ?
                p_modem_list[idx].config_info.config_desc_len : PDC_CONFIG_DESC_SIZE_MAX_V01;
            memcpy(tmp_str, p_modem_list[idx].config_info.config_desc, len);
            tmp_str[len] = '\0';
            RLOGI("    config_desc: %s", tmp_str);
        }
        if (p_modem_list[idx].config_info.config_version_valid) {
            RLOGI("    config_version: 0x%x", p_modem_list[idx].config_info.config_version);
        }
        if (p_modem_list[idx].is_current) {
            RLOGI("    This config is activated with SUBs:");
            for (subs = 0; subs < MAX_SUBS; subs++) {
                if (p_modem_list[idx].sub & (1 << subs))
                    RLOGI("    SUB%d", subs);
            }
        }
    }

    free(tmp_str);
}

//Chenyb added for mbn ota in 20170315 begin
static int is_mtp() {
	FILE* fp = fopen(HW_PLATFORM, "rb");
	char buffer1[10];
	int isSC800 = 0;
	if (fp != NULL) {
		// copy the file into the buffer:
		size_t result = fread (buffer1, 1, 10, fp);
		// the whole file is now loaded in the memory buffer.
		RLOGI("buffer1[0]:%c, buffer1[1]:%c, buffer1[2]:%c", buffer1[0], buffer1[1], buffer1[2]);
		if (strstr(buffer1, MTP)) {
			isSC800 = 1;
		} else {
			isSC800 = 0;
		}
		fclose (fp);
	} else {
		isSC800 = 0;
		RLOGI("fopen /sys/devices/soc0/hw_platform error:%d", errno);
	}
	RLOGI("isSC800:%d", isSC800);
	return isSC800;
}
//Chenyb added for mbn ota in 20170315 end

/* OEM version format
 * 0x AA BB CC DD
 * 0xAA = 0x02 .. Modem family  (0x01=Nikel, 0x02=Dime, 0x03=Triton, 0x04=Bolt, 0x05=Jolokia(8909), 0x06=Thor)
 * 0xBB = 0x01 .. OEM can modify this field for differing from base version
 * 0xCC = 0x20 .. Major version = Carrier index = 32
 * 0xDD = minor version
 */
static inline int is_same_type_mbn(const char* version_local, const char* version_remote)
{
	if (is_mtp() == 1) {
		return ((version_local[1] == version_remote[1])
               && (version_local[2] == version_remote[2])
               && (version_local[3] == version_remote[3]));
	}
	
    /* AA == AA, CC == CC DD == DD */
    return ((version_local[0] == version_remote[0])
               && (version_local[1] == version_remote[1])
               && (version_local[3] == version_remote[3]));
}
static inline int is_newer_version_mbn(const char* version_local, const char* version_remote)
{
    /* compare 0xBB */
	if (is_mtp() == 1) {
		return ((uint8_t)version_local[0] > (uint8_t)version_remote[0]);
	}
    return ((uint8_t)version_local[2] > (uint8_t)version_remote[2]);
}

static inline int is_config_activated(struct pdc_config_pdc_info *config_id,
                    struct pdc_config_pdc_info *config_ids_per_sub, uint32_t *p_sub_mask)
{
    uint32_t i, sub_idx, sub_mask = 0;

    if (!config_id || !config_ids_per_sub)
        return 0;
    for (sub_idx = 0; sub_idx < MAX_SUBS; sub_idx++) {
        if (config_id->config_id_len != config_ids_per_sub[sub_idx].config_id_len)
            continue;
        for (i = 0; i < config_id->config_id_len; i++) {
            if (config_id->config_id[i] != config_ids_per_sub[sub_idx].config_id[i])
                break;
        }
        if (i == config_id->config_id_len)
            sub_mask |= (1 << sub_idx);
    }

    *p_sub_mask = sub_mask;
    return (!!sub_mask);
}

static int find_same_type_mbn(struct efs_modem_config_info *modem_list, uint32_t len, struct pdc_config_item_info *config_info)
{
    uint32_t i;

    for (i = 0; i < len; i++) {
        if (is_same_type_mbn((char*)(&modem_list[i].config_info.config_version),
                                    (char*)(&config_info->config_version))) {
            return (int)i;
        }
    }
    return -1;
}

/* This function will generate all MBN list and their infos, querying from modem side
 * It will also delete the duplicated MBN(same type but with lower version
 * If the current activated MBN is deleted (should not happen), the parameter is_act_mbn_deleted
 * will be set as 1.
 * Side effect: this function will allocate memory for modem_list, need to free it after use
 */

static int get_modem_list(struct efs_modem_config_info **p_modem_list, int* is_act_mbn_deleted)
{
    uint32_t i, sub_mask, sub_idx, index = 0;
    struct efs_modem_config_info *modem_list;
    struct pdc_config_list_info config_list;
    struct pdc_config_item_info config_info;
    struct pdc_config_pdc_info config_ids_per_sub[MAX_SUBS];
    int result, idx_found, is_activated;

    /* set NULL as default */
    *p_modem_list = NULL;
    *is_act_mbn_deleted = 0;

    /* get config_id lists from modem side */
    if ((result = pdc_get_config_lists(&config_list)) != PDC_SUCCESS) {
        RLOGE("Failed to get config list from modem, error code: %d", result);
        return result;
    }

    /* allocate memory to hold configuration info */
    modem_list = (struct efs_modem_config_info*)malloc(
                        sizeof(struct efs_modem_config_info) * config_list.num_configs);
    if (modem_list == NULL) {
        RLOGE("Failed to allocate memeory to hold modem list");
        return PDC_ERROR;
    }
    memset(modem_list, 0, sizeof(struct efs_modem_config_info) * config_list.num_configs);

    /* get current config for each sub */
    for (i = 0; i < MAX_SUBS; i++) {
        result = pdc_get_current_config(config_ids_per_sub + i,  PDC_CONFIG_TYPE_MODEM_SW_V01, i);
        if (result == PDC_NOT_AVAIL) {
            /* no selected MBN on this SUB */
            config_ids_per_sub[i].config_id_len = 0;
        } else if (result != PDC_SUCCESS) {
            RLOGE("failed to get current config for SUB %d", i);
            config_ids_per_sub[i].config_id_len = 0;
#if 0
            /* Do not treat as a error */
            free(modem_list);
            return PDC_ERROR;
#endif
        }
    }

    /* go over the list, fill the config_info in the table and mark current activated config */
    /* delete the duplicated MBN(same type, and with lower version) */
    index = 0;
    for (i = 0; i < (uint32_t)config_list.num_configs; i++) {
        result = pdc_get_config_info(&config_list.config_item[i], &config_info, PDC_CONFIG_TYPE_MODEM_SW_V01);
        if (result == PDC_SUCCESS) {
            /* check if it is activated */
            is_activated = (is_config_activated(&config_list.config_item[i], config_ids_per_sub, &sub_mask));
            idx_found = find_same_type_mbn(modem_list, index, &config_info);
            if (idx_found < 0) {
                modem_list[index].config_id = config_list.config_item[i];
                modem_list[index].config_info = config_info;
                modem_list[index].is_current = is_activated;
                modem_list[index].sub = sub_mask;
                index++;
            } else {
                RLOGI("RIL detects duplicated MBNs. Version 1) 0x%08x, Version 2) 0x%08x",
                        config_info.config_version, modem_list[idx_found].config_info.config_version);
                if (is_newer_version_mbn((char*)(&config_info.config_version), (char*)(&modem_list[idx_found].config_info.config_version))) {
                    /* this MBN has newer OEM version, replace the previous one */
                    if (modem_list[idx_found].is_current) {
                        /* old version MBN is activated, SHOULD NOT happen */
                        RLOGE("replace ERROR: old version MBN is activated... (should not happen)");
                        /* deactivate it and select new one */
                        for (sub_idx = 0; sub_idx < MAX_SUBS; sub_idx++) {
                            if (modem_list[idx_found].sub & (1 << sub_idx)) {
                                pdc_deactivate_config(sub_idx, PDC_CONFIG_TYPE_MODEM_SW_V01);
                                pdc_select_config(&config_list.config_item[i], PDC_CONFIG_TYPE_MODEM_SW_V01, sub_idx);
                            }
                        }
                        *is_act_mbn_deleted = 1;
                    }
                    pdc_delete_config(&modem_list[idx_found].config_id, PDC_CONFIG_TYPE_MODEM_SW_V01);
                    modem_list[idx_found].config_id = config_list.config_item[i];
                    modem_list[idx_found].config_info = config_info;
                    /* If we delete the activated MBN, use the old value */
                    if (!(*is_act_mbn_deleted)) {
                        modem_list[idx_found].is_current = is_activated;
                        modem_list[idx_found].sub = sub_mask;
                    }
                } else {
                    /* this MBN has lower or equal OEM version, forbidden it */
                    if (is_activated) {
                        /* old version MBN is activated, SHOULD NOT happen */
                        RLOGE("forbidden ERROR: old version MBN is activated... (should not happen)");
                        /* deactivate it and select new one */
                        for (sub_idx = 0; sub_idx < MAX_SUBS; sub_idx++) {
                            if (sub_mask & (1 << sub_idx)) {
                                pdc_deactivate_config(sub_idx, PDC_CONFIG_TYPE_MODEM_SW_V01);
                                pdc_select_config(&modem_list[idx_found].config_id, PDC_CONFIG_TYPE_MODEM_SW_V01, sub_idx);
                            }
                        }
                        *is_act_mbn_deleted = 1;
                        /* overwrite the activated info */
                        modem_list[idx_found].is_current = 1;
                        modem_list[idx_found].sub = sub_mask;
                    }
                    pdc_delete_config(&config_list.config_item[i], PDC_CONFIG_TYPE_MODEM_SW_V01);
                }
            }
        } else {
            RLOGE("Failed to get config info");
            free(modem_list);
            return PDC_ERROR;
        }
    }

    *p_modem_list = modem_list;
    return index;
}

/* Generate an unique config_id
 * config_id: using OTA_"filepath"_date as a temporary solution
 * for long term: Using an algorithm to generate (like QPST??)
 */
static int generate_config_id(char* mbn_file, struct pdc_config_pdc_info* config_id)
{
    size_t ret;
    struct timeval tv;
#define MAX_TIME_STRING_LENGTH     64
    char time_str[MAX_TIME_STRING_LENGTH];

    strlcpy((char*)config_id->config_id, "OTA_", PDC_CONFIG_ID_SIZE_MAX_V01);
    ret = strlcat((char*)config_id->config_id, mbn_file, PDC_CONFIG_ID_SIZE_MAX_V01);
    if (ret >= PDC_CONFIG_ID_SIZE_MAX_V01) {
        RLOGW("boundary exceeds when generate config_id");
        config_id->config_id_len = strlen((char*)config_id->config_id);
        return PDC_SUCCESS;
    }
    gettimeofday(&tv, NULL);
    snprintf(time_str, MAX_TIME_STRING_LENGTH, "%ld:%ld", tv.tv_sec, tv.tv_usec);
    ret = strlcat((char*)config_id->config_id, time_str, PDC_CONFIG_ID_SIZE_MAX_V01);
    if (ret >= PDC_CONFIG_ID_SIZE_MAX_V01)
        RLOGW("boundary exceeds when generate config_id");

    config_id->config_id_len = strlen((char*)config_id->config_id);
    return PDC_SUCCESS;
}

static int is_mbn_file_name(char* file_name)
{
#define MBN_FILE_SUFFIX_STR     ".mbn"
#define MBN_FILE_SUFFIX_LEN     4
    size_t length;

    length = strlen(file_name);
    if (length <= MBN_FILE_SUFFIX_LEN)
        return 0;

    return (!strncasecmp(file_name + length - MBN_FILE_SUFFIX_LEN,
                    MBN_FILE_SUFFIX_STR, MBN_FILE_SUFFIX_LEN));
}

/* @return value
 * - OTA_CHECK_UPDATE_ERROR "error happens when check and update"
 * - OTA_NO_UPDATED_MBN "no need to update this mbn file"
 * - OTA_UPDATE_NONE_ACTIVATED_MBN "update this mbn to modem, which was not activated before"
 * - OTA_UPDATE_ACTIVATED_MBN "update this mbn to modem, which was activated before"
 */
static int check_and_update(char* mbn_file, struct efs_modem_config_info *modem_list, int n_list)
{
    struct pdc_config_pdc_info new_config_id;
    char* oem_version;
    uint32_t len;
    int i, result, ret_val, sub_idx;

    /* get oem_version of local mbn file */
    if (mcfg_get_oem_version(mbn_file, &oem_version, &len) != MBN_PARSE_SUCCESS)
        return OTA_CHECK_UPDATE_ERROR;
    if (len != OEM_VERSION_LENGTH) {
        free(oem_version);
        return OTA_CHECK_UPDATE_ERROR;
    }
    /* find the matched configuration in modem side */
    for (i = 0; i < n_list; i++) {
        if (is_same_type_mbn(oem_version, (char*)(&modem_list[i].config_info.config_version)))
            break;
    }
    if (i >= n_list) {
        /* case 1: a new type MBN that does not exist in EFS */
        if (generate_config_id(mbn_file, &new_config_id) == PDC_SUCCESS) {
            ret_val = pdc_load_config(mbn_file, &new_config_id, PDC_CONFIG_TYPE_MODEM_SW_V01);
            if (ret_val != PDC_SUCCESS)
                result = OTA_CHECK_UPDATE_ERROR;
            else
                result = OTA_UPDATE_NONE_ACTIVATED_MBN;
        } else
            result = OTA_CHECK_UPDATE_ERROR;
    } else if (is_newer_version_mbn(oem_version, (char*)(&modem_list[i].config_info.config_version))) {
        /* case 2: local MBN has newer version */
        /* load first and then delete */
        if (generate_config_id(mbn_file, &new_config_id) == PDC_SUCCESS) {
            ret_val = pdc_load_config(mbn_file, &new_config_id, PDC_CONFIG_TYPE_MODEM_SW_V01);
            if (ret_val != PDC_SUCCESS)
                result = OTA_CHECK_UPDATE_ERROR;
            else {
                /* the updated MBN is already activated */
                if (modem_list[i].is_current) {
                    result = OTA_UPDATE_ACTIVATED_MBN;
                    /* deactivate the corresponding SUBs */
                    ret_val = 0;
                    for (sub_idx = 0; sub_idx < MAX_SUBS; sub_idx++) {
                        if (modem_list[i].sub & (1 << sub_idx)) {
                            ret_val |= (pdc_deactivate_config(sub_idx, PDC_CONFIG_TYPE_MODEM_SW_V01) != PDC_SUCCESS);
                            pdc_select_config(&new_config_id, PDC_CONFIG_TYPE_MODEM_SW_V01, sub_idx);
                        }
                    }
                    /* Any deactivation failed, just print message and continue */
                    if (ret_val)
                        RLOGE("deactivation failed");

                    ret_val = pdc_delete_config(&modem_list[i].config_id, PDC_CONFIG_TYPE_MODEM_SW_V01);
                    if (ret_val != PDC_SUCCESS) {
                        /* just print message and go on */
                        RLOGE("deletion failed");
                    }
                } else {/* the updated MBN is not activated, delete it directly */
                    result = OTA_UPDATE_NONE_ACTIVATED_MBN;
                    ret_val = pdc_delete_config(&modem_list[i].config_id, PDC_CONFIG_TYPE_MODEM_SW_V01);
                    if (ret_val) {
                        /* just print message and go on */
                        RLOGE("deletion failed");
                    }
                }
            }
        } else {/* failed to generate config_id */
            result = OTA_CHECK_UPDATE_ERROR;
        }
    } else {
        /* case 3: no update */
        result = OTA_NO_UPDATED_MBN;
    }

    free(oem_version);
    return result;
}

void* qmi_ril_mbn_ota_thread_proc(void* empty_param)
{
    struct efs_modem_config_info *p_modem_list = NULL;
    char local_mbn_name[MAX_MBN_NAME_LENGTH];
    int result, n_list, is_act_mbn_deleted = 0, need_restart_modem = 0;
    struct dirent *p_ent;
    DIR *p_dir;

    /* avoid the "unused" warning */
    QCRIL_NOTUSED(empty_param);

    /* initialize QMI PDC */
    if (pdc_init() != PDC_SUCCESS)
        return (void*)PDC_ERROR;

    /* generate all modem configuration information */
    n_list = get_modem_list(&p_modem_list, &is_act_mbn_deleted);
    if (n_list == PDC_NOT_AVAIL) {
        n_list = 0;
    } else if (n_list < 0) {
        return (void*)PDC_ERROR;
    }
    need_restart_modem = is_act_mbn_deleted;
    /* for debug purpose */
    dump_mbn_list_info(p_modem_list, n_list);

    p_dir = opendir(SYSTEM_MBN_FILE_PATH);
    if (!p_dir) {
        RLOGE("Failed to open directory %s %s", SYSTEM_MBN_FILE_PATH, strerror(errno));
        if (p_modem_list)
            free(p_modem_list);
        return (void*)PDC_ERROR;
    }
    /* Process each MBN file in filesystem */
    while ((p_ent = readdir(p_dir)) != NULL) {
        if (!is_mbn_file_name(p_ent->d_name))
            continue;
        /* generate the MBN name with full path into local_mbn_name */
        if (strlcpy(local_mbn_name, SYSTEM_MBN_FILE_PATH, MAX_MBN_NAME_LENGTH) >= MAX_MBN_NAME_LENGTH) {
            RLOGE("MBN file path too long");
            continue;
        }
        if (strlcat(local_mbn_name, p_ent->d_name, MAX_MBN_NAME_LENGTH) >= MAX_MBN_NAME_LENGTH) {
            RLOGE("MBN full file name too long");
            continue;
        }
        RLOGI("Processing MBN file: %s", local_mbn_name);
        result = check_and_update(local_mbn_name, p_modem_list, n_list);
        switch (result) {
        case OTA_NO_UPDATED_MBN:
            RLOGI("This MBN is no need to update");
            break;
        case OTA_UPDATE_NONE_ACTIVATED_MBN:
            RLOGI("This MBN (not activated before) is updated");
            break;
        case OTA_UPDATE_ACTIVATED_MBN:
            RLOGI("This MBN (activated before) is updated");
            need_restart_modem = 1;
            break;
        case OTA_CHECK_UPDATE_ERROR:
        default:
            RLOGE("check & udpate with errors");
            break;
        }
    }

    if (p_modem_list)
        free(p_modem_list);

    if (need_restart_modem) {
        /* using activate command to trigger modem SSR */
        pdc_activate_config(PDC_CONFIG_TYPE_MODEM_SW_V01, 0);
    }

    return (void*)0;
}
