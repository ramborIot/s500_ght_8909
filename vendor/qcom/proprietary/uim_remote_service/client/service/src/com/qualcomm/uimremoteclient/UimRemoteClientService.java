/******************************************************************************
  @file    UimRemoteClientService.java

  ---------------------------------------------------------------------------
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/

package com.qualcomm.uimremoteclient;

import com.qualcomm.uimremoteclient.UimRemoteClientSocket;
import com.google.protobuf.micro.ByteStringMicro;
import com.qualcomm.uimremoteclient.UimRemoteClientMsgPacking;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.telephony.TelephonyManager;
import android.content.Context;

public class UimRemoteClientService extends Service {
    private final String LOG_TAG = "UimRemoteClientService";

    public static final int EVENT_REQ = 1;
    public static final int EVENT_RESP = 2;

    private Context context;

    private int mToken = 0;

    private int simSlots = TelephonyManager.getDefault().getSimCount();

    private UimRemoteClientSocket[] mSocket = new UimRemoteClientSocket[simSlots];
    IUimRemoteClientServiceCallback mCb = null;

    private Handler mRespHdlr = new Handler() {
        public void handleMessage (Message msg) {
            Log.i(LOG_TAG, "handleMessage()");
            if (mCb == null) {
                Log.d(LOG_TAG, "handleMessage() - null mCb");
                return;
            }

            try {
                byte[] bytes = (byte[])msg.obj;
                int slotId = msg.arg1;
                UimRemoteClient.MessageTag tag = UimRemoteClient.MessageTag.parseFrom(bytes);

                Log.d( LOG_TAG, "handleMessage() - token: " + tag.getToken() +
                       ", type: " + tag.getType() +
                       ", Id: " + tag.getId() +
                       ", error: " + tag.getError() +
                       ", slot id: " + slotId );

                if (tag.getType() == UimRemoteClient.UIM_REMOTE_MSG_RESPONSE) {
                    switch (tag.getId()) {
                    case UimRemoteClient.UIM_REMOTE_EVENT:
                        UimRemoteClient.UimRemoteEventResp event_rsp;
                        event_rsp = UimRemoteClient.UimRemoteEventResp.parseFrom(tag.getPayload().toByteArray());
                        mCb.uimRemoteEventResponse(slotId, event_rsp.getResponse());
                        break;
                    case UimRemoteClient.UIM_REMOTE_APDU:
                        UimRemoteClient.UimRemoteApduResp adpu_rsp;
                        adpu_rsp = UimRemoteClient.UimRemoteApduResp.parseFrom(tag.getPayload().toByteArray());
                        mCb.uimRemoteApduResponse(slotId, adpu_rsp.getStatus());
                        break;
                    default:
                        Log.e(LOG_TAG, "unexpected msg id");
                    }
                } else if (tag.getType() == UimRemoteClient.UIM_REMOTE_MSG_INDICATION) {
                    switch (tag.getId()) {
                    case UimRemoteClient.UIM_REMOTE_APDU:
                        UimRemoteClient.UimRemoteApduInd adpu_ind;
                        adpu_ind = UimRemoteClient.UimRemoteApduInd.parseFrom(tag.getPayload().toByteArray());
                        mCb.uimRemoteApduIndication(slotId, adpu_ind.getApduCommand().toByteArray());
                        break;
                    case UimRemoteClient.UIM_REMOTE_CONNECT:
                        mCb.uimRemoteConnectIndication(slotId);
                        break;
                    case UimRemoteClient.UIM_REMOTE_DISCONNECT:
                        mCb.uimRemoteDisconnectIndication(slotId);
                        break;
                    case UimRemoteClient.UIM_REMOTE_POWER_UP:
                        mCb.uimRemotePowerUpIndication(slotId);
                        break;
                    case UimRemoteClient.UIM_REMOTE_POWER_DOWN:
                        mCb.uimRemotePowerDownIndication(slotId);
                        break;
                    case UimRemoteClient.UIM_REMOTE_RESET:
                        mCb.uimRemoteResetIndication(slotId);
                        break;
                    default:
                        Log.e(LOG_TAG, "unexpected msg id");
                    }
                } else {
                    Log.e(LOG_TAG, "unexpected msg type");
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, "error occured when parsing the resp/ind");
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(LOG_TAG, "onCreate()");

        context = this;

        Log.i(LOG_TAG, "simCount: "+ simSlots);

        for(int i = 0; i < simSlots; i++) {
            mSocket[i] = new UimRemoteClientSocket(mRespHdlr, i);
            new Thread(mSocket[i]).start();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(LOG_TAG, "onBind()");
        return mBinder;
    }

    @Override
    public void onDestroy() {
        Log.i(LOG_TAG, "onDestroy()");

        for(int i = 0; i < simSlots; i++) {
            mSocket[i].toDestroy();
        }
        stopSelf();
        super.onDestroy();
    }

    private final IUimRemoteClientService.Stub mBinder = new IUimRemoteClientService.Stub() {
        public int registerCallback(IUimRemoteClientServiceCallback cb) {
            UimRemoteClientService.this.mCb = cb;
            if (cb == null) {
                Log.d(LOG_TAG, "registerCallback() - null cb");
            }
            return 0;
        }

        public int deregisterCallback(IUimRemoteClientServiceCallback cb) {
            UimRemoteClientService.this.mCb = null;
            return 0;
        }

        public int uimRemoteEvent(int slot, int event, byte[] atr, int errCode) {
            if(slot >= simSlots) {
                Log.e(LOG_TAG, "Sim Slot not supported!" + slot);
                return 1;
            }
            if (mSocket[slot] == null) {
                Log.e(LOG_TAG, "socket is not connected");
                return 1;
            }


            Log.d(LOG_TAG, "uimRemoteEvent() - slot: " + slot + "; event: " + event);

            // get tag;
            UimRemoteClient.MessageTag tag = new UimRemoteClient.MessageTag();
            tag.setToken(mToken++);
            tag.setType(UimRemoteClient.UIM_REMOTE_MSG_REQUEST);
            tag.setId(UimRemoteClient.UIM_REMOTE_EVENT);
            tag.setError(UimRemoteClient.UIM_REMOTE_ERR_SUCCESS);

            // get request
            UimRemoteClient.UimRemoteEventReq req = new UimRemoteClient.UimRemoteEventReq();
            req.setEvent(event);
            req.setAtr(ByteStringMicro.copyFrom(atr));
            req.setErrorCode(errCode);

            tag.setPayload(ByteStringMicro.copyFrom(
                                UimRemoteClientMsgPacking.packMsg( UimRemoteClient.UIM_REMOTE_EVENT,
                                                             UimRemoteClient.UIM_REMOTE_MSG_REQUEST,
                                                             req) ));

            Message msg = mSocket[slot].obtainMessage(EVENT_REQ, UimRemoteClientMsgPacking.packTag(tag));
            msg.sendToTarget();
            return 0;
        }

        public int uimRemoteApdu(int slot, int apduStatus, byte[] apduResp) {
            if(slot >= simSlots) {
                Log.e(LOG_TAG, "Sim Slot not supported!" + slot);
                return 1;
            }
            if (mSocket[slot] == null) {
                Log.e(LOG_TAG, "socket is not connected");
                return 1;
            }

            Log.d(LOG_TAG, "uimRemoteApdu() - slot: " + slot + "; adpuStatus: " + apduStatus);

            // get tag;
            UimRemoteClient.MessageTag tag = new UimRemoteClient.MessageTag();
            tag.setToken(mToken++);
            tag.setType(UimRemoteClient.UIM_REMOTE_MSG_REQUEST);
            tag.setId(UimRemoteClient.UIM_REMOTE_APDU);
            tag.setError(UimRemoteClient.UIM_REMOTE_ERR_SUCCESS);

            // get request
            UimRemoteClient.UimRemoteApduReq req = new UimRemoteClient.UimRemoteApduReq();
            req.setStatus(apduStatus);
            req.setApduResponse(ByteStringMicro.copyFrom(apduResp));

            tag.setPayload(ByteStringMicro.copyFrom(
                                UimRemoteClientMsgPacking.packMsg( UimRemoteClient.UIM_REMOTE_APDU,
                                                             UimRemoteClient.UIM_REMOTE_MSG_REQUEST,
                                                             req) ));

            Message msg = mSocket[slot].obtainMessage(EVENT_REQ, UimRemoteClientMsgPacking.packTag(tag));
            msg.sendToTarget();
            return 0;
        }
    };
}
