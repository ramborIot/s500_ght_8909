/******************************************************************************
  @file    UimRemoteServerService.java

  ---------------------------------------------------------------------------
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/

package com.qualcomm.uimremoteserver;

import com.qualcomm.uimremoteserver.UimRemoteServerSocket;
import com.google.protobuf.micro.ByteStringMicro;
import com.qualcomm.uimremoteserver.UimRemoteServerMsgPacking;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.telephony.TelephonyManager;
import android.content.Context;

public class UimRemoteServerService extends Service {
    private final String LOG_TAG = "UimRemoteServerService";

    public static final int EVENT_REQ = 1;
    public static final int EVENT_RESP = 2;

    private Context context;

    private int mToken = 0;

    private int simSlots = TelephonyManager.getDefault().getSimCount();

    private UimRemoteServerSocket[] mSocket = new UimRemoteServerSocket[simSlots];
    IUimRemoteServerServiceCallback mCb = null;

    private Handler mRespHdlr = new Handler() {
        public void handleMessage (Message msg) {
            Log.i(LOG_TAG, "handleMessage()");
            if (mCb == null) {
                Log.d(LOG_TAG, "handleMessage() - null mCb");
                return;
            }

            try {
                byte[] bytes = (byte[])msg.obj;
                int slotId = msg.arg1;
                SapApi.MsgHeader header = UimRemoteServerMsgPacking.unpackMsgHeader(bytes);

                Log.d( LOG_TAG, "handleMessage() - token: " + header.getToken() +
                       ", type: " + header.getType() +
                       ", Id: " + header.getId() +
                       ", error: " + header.getError() +
                       ", slot id: " + slotId );

                Object msgPayload = UimRemoteServerMsgPacking.unpackMsg(header.getId(), header.getType(), header.getPayload().toByteArray());

                if (header.getType() == SapApi.RESPONSE) {
                    switch (header.getId()) {
                    case SapApi.RIL_SIM_SAP_CONNECT:
                        mCb.uimRemoteServerConnectResp(
                            slotId,
                            ((SapApi.RIL_SIM_SAP_CONNECT_RSP)msgPayload).getResponse(),
                            ((SapApi.RIL_SIM_SAP_CONNECT_RSP)msgPayload).getMaxMessageSize() );
                        break;
                    case SapApi.RIL_SIM_SAP_DISCONNECT:
                        mCb.uimRemoteServerDisconnectResp(slotId, 0); // success
                        break;
                    case SapApi.RIL_SIM_SAP_APDU:
                        mCb.uimRemoteServerApduResp(
                            slotId,
                            ((SapApi.RIL_SIM_SAP_APDU_RSP)msgPayload).getResponse(),
                            ((SapApi.RIL_SIM_SAP_APDU_RSP)msgPayload).getApduResponse().toByteArray() );
                        break;
                    case SapApi.RIL_SIM_SAP_TRANSFER_ATR:
                        mCb.uimRemoteServerTransferAtrResp(
                            slotId,
                            ((SapApi.RIL_SIM_SAP_TRANSFER_ATR_RSP)msgPayload).getResponse(),
                            ((SapApi.RIL_SIM_SAP_TRANSFER_ATR_RSP)msgPayload).getAtr().toByteArray() );
                        break;
                    case SapApi.RIL_SIM_SAP_POWER:
                        mCb.uimRemoteServerPowerResp(
                            slotId,
                            ((SapApi.RIL_SIM_SAP_POWER_RSP)msgPayload).getResponse() );
                        break;
                    case SapApi.RIL_SIM_SAP_RESET_SIM:
                        mCb.uimRemoteServerResetSimResp(
                            slotId,
                            ((SapApi.RIL_SIM_SAP_RESET_SIM_RSP)msgPayload).getResponse() );
                        break;
                    default:
                        Log.e(LOG_TAG, "unexpected msg id");
                    }
                } else if (header.getType() == SapApi.UNSOL_RESPONSE) {
                    switch (header.getId()) {
                    case SapApi.RIL_SIM_SAP_DISCONNECT:
                        mCb.uimRemoteServerDisconnectInd(
                            slotId,
                            ((SapApi.RIL_SIM_SAP_DISCONNECT_IND)msgPayload).getDisconnectType() );
                        break;
                    case SapApi.RIL_SIM_SAP_STATUS:
                        mCb.uimRemoteServerStatusInd(
                            slotId,
                            ((SapApi.RIL_SIM_SAP_STATUS_IND)msgPayload).getStatusChange());
                        break;
                    default:
                        Log.e(LOG_TAG, "unexpected msg id");
                    }
                } else {
                    Log.e(LOG_TAG, "unexpected msg type");
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, "error occured when parsing the resp/ind");
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(LOG_TAG, "onCreate()");

        context = this;

        Log.i(LOG_TAG, "simCount: "+ simSlots);

        for(int i = 0; i < simSlots; i++) {
            mSocket[i] = new UimRemoteServerSocket(mRespHdlr, i);
            new Thread(mSocket[i]).start();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(LOG_TAG, "onBind()");
        return mBinder;
    }

    @Override
    public void onDestroy() {
        Log.i(LOG_TAG, "onDestroy()");

        for(int i = 0; i < simSlots; i++) {
            mSocket[i].toDestroy();
        }
        stopSelf();
        super.onDestroy();
    }

    private final IUimRemoteServerService.Stub mBinder = new IUimRemoteServerService.Stub() {
        public int registerCallback(IUimRemoteServerServiceCallback cb) {
            UimRemoteServerService.this.mCb = cb;
            if (cb == null) {
                Log.d(LOG_TAG, "registerCallback() - null cb");
            }
            return 0;
        }

        public int deregisterCallback(IUimRemoteServerServiceCallback cb) {
            UimRemoteServerService.this.mCb = null;
            return 0;
        }

        public int uimRemoteServerConnectReq(int slot, int maxMessageSize) {
            if(slot >= simSlots) {
                Log.e(LOG_TAG, "Sim Slot not supported!" + slot);
                return 1;
            }
            if (mSocket[slot] == null) {
                Log.e(LOG_TAG, "socket is not connected");
                return 1;
            }

            Log.d(LOG_TAG, "uimRemoteServerConnectReq() - maxMessageSize: " + maxMessageSize + " slot: " + slot);

            // get tag;
            SapApi.MsgHeader header = new SapApi.MsgHeader();
            header.setToken(mToken++);
            header.setType(SapApi.REQUEST);
            header.setId(SapApi.RIL_SIM_SAP_CONNECT);
            header.setError(SapApi.RIL_E_SUCCESS);

            // get request
            SapApi.RIL_SIM_SAP_CONNECT_REQ req = new SapApi.RIL_SIM_SAP_CONNECT_REQ();
            req.setMaxMessageSize(maxMessageSize);
            header.setPayload(ByteStringMicro.copyFrom(
                                UimRemoteServerMsgPacking.packMsg( SapApi.RIL_SIM_SAP_CONNECT,
                                                             SapApi.REQUEST,
                                                             req) ));

            Message msg = mSocket[slot].obtainMessage(EVENT_REQ, UimRemoteServerMsgPacking.packMsgHeader(header));
            msg.sendToTarget();
            return 0;
        }

        public int uimRemoteServerDisconnectReq(int slot) {
            if(slot >= simSlots) {
                Log.e(LOG_TAG, "Sim Slot not supported!" + slot);
                return 1;
            }
            if (mSocket[slot] == null) {
                Log.e(LOG_TAG, "socket is not connected");
                return 1;
            }

            Log.d(LOG_TAG, "uimRemoteServerDisconnectReq() slot: " + slot);

            // get tag;
            SapApi.MsgHeader header = new SapApi.MsgHeader();
            header.setToken(mToken++);
            header.setType(SapApi.REQUEST);
            header.setId(SapApi.RIL_SIM_SAP_DISCONNECT);
            header.setError(SapApi.RIL_E_SUCCESS);
            header.setPayload(ByteStringMicro.copyFrom(new byte[0]));

            Message msg = mSocket[slot].obtainMessage(EVENT_REQ, UimRemoteServerMsgPacking.packMsgHeader(header));
            msg.sendToTarget();
            return 0;
        }

        public int uimRemoteServerApduReq(int slot, byte[] cmd) {
            if(slot >= simSlots) {
                Log.e(LOG_TAG, "Sim Slot not supported!" + slot);
                return 1;
            }
            if (mSocket[slot] == null) {
                Log.e(LOG_TAG, "socket is not connected");
                return 1;
            }

            Log.d(LOG_TAG, "uimRemoteServerApduReq() - cmd length: " + cmd.length + " slot: " + slot);

            // get tag;
            SapApi.MsgHeader header = new SapApi.MsgHeader();
            header.setToken(mToken++);
            header.setType(SapApi.REQUEST);
            header.setId(SapApi.RIL_SIM_SAP_APDU);
            header.setError(SapApi.RIL_E_SUCCESS);

            // get request
            SapApi.RIL_SIM_SAP_APDU_REQ req = new SapApi.RIL_SIM_SAP_APDU_REQ();
            req.setType(1);
            req.setCommand(ByteStringMicro.copyFrom(cmd));
            header.setPayload(ByteStringMicro.copyFrom(
                                UimRemoteServerMsgPacking.packMsg( SapApi.RIL_SIM_SAP_APDU,
                                                             SapApi.REQUEST,
                                                             req) ));

            Message msg = mSocket[slot].obtainMessage(EVENT_REQ, UimRemoteServerMsgPacking.packMsgHeader(header));
            msg.sendToTarget();
            return 0;
        }

        public int uimRemoteServerTransferAtrReq(int slot) {
            if(slot >= simSlots) {
                Log.e(LOG_TAG, "Sim Slot not supported!" + slot);
                return 1;
            }
            if (mSocket[slot] == null) {
                Log.e(LOG_TAG, "socket is not connected");
                return 1;
            }

            Log.d(LOG_TAG, "uimRemoteServerTransferAtrReq() slot: " + slot);

            // get tag;
            SapApi.MsgHeader header = new SapApi.MsgHeader();
            header.setToken(mToken++);
            header.setType(SapApi.REQUEST);
            header.setId(SapApi.RIL_SIM_SAP_TRANSFER_ATR);
            header.setError(SapApi.RIL_E_SUCCESS);
            header.setPayload(ByteStringMicro.copyFrom(new byte[0]));

            Message msg = mSocket[slot].obtainMessage(EVENT_REQ, UimRemoteServerMsgPacking.packMsgHeader(header));
            msg.sendToTarget();
            return 0;
        }

        public int uimRemoteServerPowerReq(int slot, boolean state) {
            if(slot >= simSlots) {
                Log.e(LOG_TAG, "Sim Slot not supported!" + slot);
                return 1;
            }
            if (mSocket[slot] == null) {
                Log.e(LOG_TAG, "socket is not connected");
                return 1;
            }

            Log.d(LOG_TAG, "uimRemoteServerPowerReq() - state: " + state + " slot: " + slot);

            // get tag;
            SapApi.MsgHeader header = new SapApi.MsgHeader();
            header.setToken(mToken++);
            header.setType(SapApi.REQUEST);
            header.setId(SapApi.RIL_SIM_SAP_POWER);
            header.setError(SapApi.RIL_E_SUCCESS);

            // get request
            SapApi.RIL_SIM_SAP_POWER_REQ req = new SapApi.RIL_SIM_SAP_POWER_REQ();
            req.setState(state);
            header.setPayload(ByteStringMicro.copyFrom(
                                UimRemoteServerMsgPacking.packMsg( SapApi.RIL_SIM_SAP_POWER,
                                                             SapApi.REQUEST,
                                                             req) ));

            Message msg = mSocket[slot].obtainMessage(EVENT_REQ, UimRemoteServerMsgPacking.packMsgHeader(header));
            msg.sendToTarget();
            return 0;
        }

        public int uimRemoteServerResetSimReq(int slot) {
            if(slot >= simSlots) {
                Log.e(LOG_TAG, "Sim Slot not supported!" + slot);
                return 1;
            }
            if (mSocket[slot] == null) {
                Log.e(LOG_TAG, "socket is not connected");
                return 1;
            }

            Log.d(LOG_TAG, "uimRemoteServerResetSimReq() slot: " + slot);

            // get tag;
            SapApi.MsgHeader header = new SapApi.MsgHeader();
            header.setToken(mToken++);
            header.setType(SapApi.REQUEST);
            header.setId(SapApi.RIL_SIM_SAP_RESET_SIM);
            header.setError(SapApi.RIL_E_SUCCESS);
            header.setPayload(ByteStringMicro.copyFrom(new byte[0]));

            Message msg = mSocket[slot].obtainMessage(EVENT_REQ, UimRemoteServerMsgPacking.packMsgHeader(header));
            msg.sendToTarget();
            return 0;
        }
    };
}
