uncompress the sdk package to android root dir

1. compile not sign images, if custom have no fuse
	./compile_all.sh -a [android root dir] -p [project_name]

	eg. ./compile_all -a /work/msm8909/LINUX/android/ -p msm8909
	eg. ./compile_all -a /work/msm8909/LINUX/android/ -p msm8909 --hlos


2. compile sign images, if custom have fuse
	./sign_compile_all.sh -a [android root dir] -p [project_name] -s

	eg. ./compile_all -a /work/msm8909/LINUX/android/ -p msm8909 -s
	eg. ./compile_all -a /work/msm8909/LINUX/android/ -p msm8909 --hlos -s
