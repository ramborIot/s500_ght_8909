#!/bin/bash
CURR_PATH=`pwd`
ROOT_PATH=$CURR_PATH/../../..
echo $ROOT_PATH

python sectools.py secimage -m $ROOT_PATH -p 8909 -o oem_sec_images -sa
#python secools.py secimage �Cm $ROOT_PATH -p 8909 -o oem_sec_images -sa �Cd �Cv
python sectools.py fuseblower -p 8909 -g �Cv

#boot_images
cp oem_sec_images/8909/prog_emmc_ddr/prog_emmc_firehose_8909_ddr.mbn  						$ROOT_PATH/boot_images/build/ms/bin/8909/emmc/prog_emmc_firehose_8909_ddr.mbn
cp oem_sec_images/8909/prog_emmc_lite/prog_emmc_firehose_8909_lite.mbn 						$ROOT_PATH/boot_images/build/ms/bin/8909/emmc/prog_emmc_firehose_8909_lite.mbn
cp oem_sec_images/8909/validated_emmc_ddr/validated_emmc_firehose_8909_ddr.mbn  	$ROOT_PATH/boot_images/build/ms/bin/8909/emmc/validated_emmc_firehose_8909_ddr.mbn
cp oem_sec_images/8909/validated_emmc_lite/validated_emmc_firehose_8909_lite.mbn  $ROOT_PATH/boot_images/build/ms/bin/8909/emmc/validated_emmc_firehose_8909_lite.mbn
cp oem_sec_images/8909/sbl1/sbl1.mbn  																						$ROOT_PATH/boot_images/build/ms/bin/8909/emmc/sbl1.mbn

#LINUX\android
cp oem_sec_images/8909/venus/venus.*  												$ROOT_PATH/LINUX/android/vendor/qcom/proprietary/prebuilt_HY11/target/product/msm8909/system/etc/firmware/
cp oem_sec_images/8909/venus/venus.*  												$ROOT_PATH/LINUX/android/out/target/product/la0920/system/etc/firmware/


#modem_proc
cp oem_sec_images/8909/mba/mba.mbn  													$ROOT_PATH/modem_proc/build/ms/bin/8909.genns.prod/mba.mbn
cp oem_sec_images/8909/modem/modem.mbn  											$ROOT_PATH/modem_proc/build/ms/bin/8909.genns.prod/qdsp6sw.mbn

#rpm_proc
cp oem_sec_images/8909/rpm/rpm.mbn  													$ROOT_PATH/rpm_proc/build/ms/bin/8909/pm8909/rpm.mbn

#trustzone_images
cp oem_sec_images/8909/cmnlib/cmnlib.mbn  			 							$ROOT_PATH/trustzone_images/build/ms/bin/MAZAANAA/cmnlib.mbn
cp oem_sec_images/8909/keymaster/keymaster.mbn  							$ROOT_PATH/trustzone_images/build/ms/bin/MAZAANAA/keymaster.mbn
cp oem_sec_images/8909/qsee/tz.mbn  													$ROOT_PATH/trustzone_images/build/ms/bin/MAZAANAA/tz.mbn
cp oem_sec_images/8909/sampleapp/sampleapp.mbn  							$ROOT_PATH/trustzone_images/build/ms/bin/MAZAANAA/sampleapp.mbn
cp oem_sec_images/8909/widevine/widevine.mbn  								$ROOT_PATH/trustzone_images/build/ms/bin/MAZAANAA/widevine.mbn
cp oem_sec_images/8909/playready/playready.mbn  								$ROOT_PATH/trustzone_images/build/ms/bin/MAZAANAA/playready.mbn

#wcnss_proc
cp oem_sec_images/8909/wcnss/wcnss.mbn  											$ROOT_PATH/wcnss_proc/build/ms/bin/SCAQMAZ/reloc/wcnss.mbn
