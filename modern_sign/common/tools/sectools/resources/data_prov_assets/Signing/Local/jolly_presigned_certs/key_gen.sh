#! /bin/sh
#DIGEST=""
#if [ x$1 == x"sha256" ]; then
#DIGEST="-SHA256"
#echo "Using SHA256 to sign certificates.."
#else
#echo "Using SHA1 to sign certificates.."
#DIGEST="-SHA1"
#fi
#openssl genrsa -out qpsa_rootca.key -3 2048
#openssl req -new -key qpsa_rootca.key -x509 -out qpsa_rootca.crt ${DIGEST} -subj /C=CN/ST=Guangdong/L=Shenzhen/OU=Solution/O=Fibocom/CN="Zhangwj Root CA" -days 7300 -set_serial 1 -config opensslroot.cfg
#openssl x509 -in qpsa_rootca.crt -inform PEM -out qpsa_rootca.cer -outform DER
#openssl dgst -sha256 qpsa_rootca.cer | tee sha256rootcert.txt 

#openssl genrsa -out qpsa_attestca.key -3 2048
#openssl req -new -key qpsa_attestca.key -out qpsa_attestca.csr ${DIGEST} -subj /C=CN/ST=Guangdong/L=Shenzhen/OU=Solution/O=Fibocom/CN="Zhangwj Attestation CA" -days 7300 -config opensslroot.cfg
#openssl x509 -req -in qpsa_attestca.csr -CA qpsa_rootca.crt -CAkey qpsa_rootca.key -out qpsa_attestca.crt ${DIGEST} -set_serial 5 -days 7300 -extfile v3.ext
#openssl x509 -in qpsa_attestca.crt -inform PEM -out qpsa_attestca.cer -outform DER

#openssl genrsa -out qpsa_attest.key -3 2048
#openssl req -new -key qpsa_attest.key -out qpsa_attest.csr ${DIGEST} -subj /C=CN/ST=Guangdong/L=Shenzhen/emailAddress=zhangwj@fibocom.com/OU="07 0001 DIGEST"/OU="06 0001 MODEL_ID"/OU="05 00002000 SW_SIZE"/OU="04 0168 OEM_ID"/OU="03 0000000000000002 DEBUG"/OU="02 007180E100010001 HW_ID"/OU="01 0000000000000000 SW_ID"/O=Fibocom/CN="Zhangwj Attestation Cert" -days 7300 -config opensslroot.cfg
#openssl x509 -req -in qpsa_attest.csr -CA qpsa_attestca.crt -CAkey qpsa_attestca.key -out qpsa_attest.crt ${DIGEST} -set_serial 7 -days 7300 -extfile v3_attest.ext

#echo "test case 1"
#echo "========================================================================="
#cat qpsa_rootca.crt qpsa_attestca.crt > my.crt # add for testing only
#openssl verify -CApath . -CAfile my.crt qpsa_attest.crt # add for testing only

openssl genrsa -out oem_rootca.key -3 2048 
openssl req -new -key oem_rootca.key -x509 -out oem_rootca.crt -subj /C="CN"/ST="GD"/L="SZ"/O="JOLLY"/OU="General JOLLY rootca"/CN="JOLLY ROOT CA" -days 7300 -set_serial 1 -config opensslroot.cfg -sha256 
openssl x509 -in oem_rootca.crt -inform PEM -out qpsa_rootca.cer -outform DER 

openssl genrsa -out oem_attestca.key -3 2048 
openssl req -new -key oem_attestca.key -out oem_attestca.csr -subj /C="CN"/ST="GD"/L="SZ"/O="JOLLY"/OU="General JOLLY attestation CA"/CN="JOLLY attestation CA" -days 7300 -config opensslroot.cfg 
openssl x509 -req -in oem_attestca.csr -CA oem_rootca.crt -CAkey oem_rootca.key -out oem_attestca.crt -set_serial 5 -days 7300 -extfile v3.ext -sha256 
openssl x509 -in oem_attestca.crt -inform PEM -out qpsa_attestca.cer -outform DER 

mv oem_rootca.key qpsa_rootca.key 
mv oem_attestca.key qpsa_attestca.key 
openssl dgst -sha256 qpsa_rootca.cer | tee sha256rootcert.txt 