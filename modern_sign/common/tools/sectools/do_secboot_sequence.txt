bp/ :BP的根目录,包含 boot_images/ common/ modem_proc/ trustzone_images/ 等文件夹 
ap/ :AP的根目录,包含 packages/ prebuilts/ device/ kernel/ out/ 等文件夹 
工具目录 bp/common/tools/sectools 
重点注意 xml 的配置 

1、确保软件已经全部编译过 并将 ap 软连接到 bp/LINUX 下 
bp/LINUX$mv android android_orig 
bp/LINUX$ln -sf ap android 

2、生成好自己的证书，参考 80-NM248-1 P36 放到如下目录 如： oem_certs 
从其他的默认证书中拷贝config.xml 到oem_certs下，如：qc_presigned_certs-key2048_exp3 
用到的文件可以在 bp\common\tools\sectools\resources\data_prov_assets\General_Assets\Signing\openssl 下找到 opensslroot.cfg v3.ext 
bp/common/tools/sectools/resources/data_prov_assets/Signing/Local/oem_certs 
$openssl genrsa -out oem_rootca.key -3 2048 
$openssl req -new -key oem_rootca.key -x509 -out oem_rootca.crt -subj /C="US"/ST="CA"/L="SANDIEGO"/O="OEM"/OU="General OEM rootca"/CN="OEM ROOT CA" -days 7300 -set_serial 1 -config opensslroot.cfg -sha256 
$openssl x509 -in oem_rootca.crt -inform PEM -out qpsa_rootca.cer -outform DER 

$openssl genrsa -out oem_attestca.key -3 2048 
$openssl req -new -key oem_attestca.key -out oem_attestca.csr -subj /C="US"/ST="CA"/L="SANDIEGO"/O="OEM"/OU="General OEM attestation CA"/CN="OEM attestation CA" -days 7300 -config opensslroot.cfg 
$openssl x509 -req -in oem_attestca.csr -CA oem_rootca.crt -CAkey oem_rootca.key -out oem_attestca.crt -set_serial 5 -days 7300 -extfile v3.ext -sha256 
$openssl x509 -in oem_attestca.crt -inform PEM -out qpsa_attestca.cer -outform DER 

$mv oem_rootca.key qpsa_rootca.key 
$mv oem_attestca.key qpsa_attestca.key 
$openssl dgst -sha256 qpsa_rootca.cer | tee sha256rootcert.txt 

3、签名images 
a、修改文件 bp\common\tools\sectools\config\8909\8909_secimage.xml 
修改以下项目的内容 
<selected_cert_config>qc_presigned_certs</selected_cert_config> 证书位置 上面设置的 qc_presigned_certs ---> oem_certs 
<msm_part>0x009600E1</msm_part> 
<oem_id>0x0000</oem_id> 
<model_id>0x0000</model_id> 
<debug>0x0000000000000002</debug> 
具体修改为什么值，请参考文档及 8909_secimage.xml 中的注释 
b、$ python sectools.py secimage -m bp_path -p 8909 -o oem_sec_images -l
b、$ python sectools.py secimage -m bp_path -p 8909 -o oem_sec_images -sa 
bp_path : bp 根目录绝对路径 
oem_sec_images ：签名后的文件的输出目录 

c、将生成的 iamge 升级到机器中，确保能开机

4、使能验证，参考 80-NM248-3 
a、修改 common\tools\sectools\config\8909\8909_fuseblower_USER.xml 
<value>0000000000000000000000000000000000000000000000000000000000000000</value> 修改为 qpsa_rootca.cer 实际hash值，sha256rootcert.txt中 
<value>./../../resources/testpki/qpsa_rootca.cer</value> qpsa_rootca.cer实际路径 

<name>SEC_BOOT1_PK_Hash_in_Fuse</name> 
<value>false</value> ---> true 

<name>SEC_BOOT2_PK_Hash_in_Fuse</name> 
<value>false</value> ---> true 

<name>SEC_BOOT3_PK_Hash_in_Fuse</name> 
<value>false</value> ---> true 

<name>oem_hw_id</name> 
<value>0x0000</value> 和 <oem_id>0x0000</oem_id>一致 

<name>oem_product_id</name> 
<value>0x0000</value> 和<model_id>0x0000</model_id>一致 
b、生成 sec.dat 
python sectools.py fuseblower -p 8909 -g –v 
c、烧录sec.dat 
fastboot flash sec <sec.dat file path> 
d、看能否开机 
如果无法开机或出现一直在sbl 重启,或者 fastboot flash sec sec.dat 机器无法更改 Qfuse 的值， 
检查下 msm8909 P8 引脚 VDD_QFPROM_PRG 没有连接到 L6 (1.8v )上 。在实际使用中需要给P8供电 
另外检查 sec.dat 值是否正常 

使能secure boot 后如果无法用Qfil 下载 
参考 Solution Number	00030390 
5、更改3中的配置，确认4确实有效 



关于 其他注意的地方 
1、NON-HLOS.bin 是个压缩文件，里面包含很多 mbn 
你先对 编译后生成的 mbn 签名，然后 update_common_info.py 
切分及打包为 NON-HLOS.bin 
2、system.img 不需要签名。但 里面的 system/etc/firmware 下的venus.mbn 是需要签名的 
对 LINUX/android/vendor/qcom/proprietary/prebuilt_HY11/target/product/msm8909/system/etc/firmware/venus.mbn 签名后手动切分 
\common\tools\misc\pil-splitter.py 
pil-splitter.py <elf> <prefix> 
<elf> is same with venus.mbn and <prefix> is "venus". 
Please try "pil-splitter.py venus.mbn venus".


 
