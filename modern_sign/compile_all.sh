#!/bin/bash

set -o errexit

TOP=$(pwd)
ANDROID_ROOT=""
PROJECT_NAME=""
BUILD_HLOS=--nonhlos
BUILD_SIGN="true"
NO_BUILD_ANDROID="false"

usage() {
cat << USAGE

Usage:
	bash $0 [OPTIONS]

Decription:
	compile BP/AP files

OPTIONs:
	-a, --android_root
		Specific android root direction
	
	-p, --project
		Specific custorm project name
	
	-s, --sign
		sign iamges for secboot

	--no_build_android
		just sign images with no build android
	
	--hlos
		Build the sparse images

	-h, --help
		show help massage
USAGE
}

long_opts="android_root:,project:,hlos,help,sign,no_build_android"
getopt_cmd=$(getopt -o a:p:hs --long "$long_opts"	\
		-n $(basename $0) -- "$@") || \
		{ echo -e "\nERROR: Getopt failed. Extra args\n"; usage; exit 1;}

eval set -- "$getopt_cmd"

while true; do
    case "$1" in
		-a|--android_root) ANDROID_ROOT=$2; shift;;
		-p|--project) PROJECT_NAME=$2; shift;;
		--hlos) BUILD_HLOS=--hlos;;
		-s|--sign) BUILD_SIGN="true";;
		--no_build_android) NO_BUILD_ANDROID="true";;
		-h|--help) usage; exit 0;;
		--) shift; break;;
	esac
	shift
done

if [ "$ANDROID_ROOT" == "" ]; then
	ANDROID_ROOT=$TOP/../
fi

if [ "$PROJECT_NAME" == "" ]; then
	PROJECT_NAME=la0920
fi

# Android
rm -rf $TOP/LINUX/android

if [ "$NO_BUILD_ANDROID" == "false" ]; then
	cd $ANDROID_ROOT
	./build.sh $PROJECT_NAME -j32

	if [ $? -ne 0 ];then		
		echo building android failed!
		cd $TOP
		exit 1
	fi
fi


cd $TOP
sed -i s/"la0920"/"$PROJECT_NAME"/g contents.xml

if [ "$BUILD_SIGN" == "true" ]; then
	ln -s $ANDROID_ROOT $TOP/LINUX/android
	cd $TOP/common/tools/sectools
	./qsecureboot.sh
	if [ $? -ne 0 ];then		
		echo building secureboot failed!
		cd $TOP
		exit 1
	fi
	
	#rebuild system.img
	rm -rf $TOP/LINUX/android
	cd $ANDROID_ROOT
	./build.sh $PROJECT_NAME -j32 -i sysimg
	if [ $? -ne 0 ];then		
		echo building android system image failed!
		cd $TOP
		exit 1
	fi

	cp $TOP/common/tools/sectools/oem_sec_images/8909/appsbl/emmc_appsboot.mbn $ANDROID_ROOT/out/target/product/la0920/emmc_appsboot.mbn

fi

# Updating NON-HLOS.bin
ln -s $ANDROID_ROOT $TOP/LINUX/android
		
# Updating NON-HLOS.bin
cd $TOP/common/build/
python update_common_info.py $BUILD_HLOS

if [ $? -ne 0 ];then		
    echo building NON-HLOS failed!
    cd $TOP
    exit 1
fi

cd $TOP
./copy_file.sh $ANDROID_ROOT $BUILD_HLOS
		
rm -rf $TOP/LINUX/android
cd $TOP/

