#!/bin/bash
TOP=$(pwd)

ANDROID_ROOT=$1
NONHLOS=$2

IMAGES_DIR=$TOP/oem_sec_images
mkdir -p $IMAGES_DIR

cp $TOP/common/build/gpt_main0.bin $IMAGES_DIR
cp $TOP/common/build/gpt_both0.bin $IMAGES_DIR
cp $TOP/common/build/gpt_backup0.bin $IMAGES_DIR
cp $TOP/common/build/patch0.xml $IMAGES_DIR
cp $TOP/common/build/bin/asic/NON-HLOS.bin $IMAGES_DIR

cp $TOP/common/tools/sectools/resources/build/sec.dat $IMAGES_DIR

cp $ANDROID_ROOT/out/target/product/la0920/boot.img $IMAGES_DIR
cp $ANDROID_ROOT/out/target/product/la0920/recovery.img $IMAGES_DIR
cp $ANDROID_ROOT/out/target/product/la0920/splash.img $IMAGES_DIR

if [ "$NONHLOS" == "--hlos" ]; then
#begin for split file
cp $TOP/common/build/bin/asic/sparse_images/*.img $IMAGES_DIR
cp $TOP/common/build/bin/asic/sparse_images/rawprogram_unsparse.xml $IMAGES_DIR/rawprogram0.xml
#end for split file
else
#begin for not split file
#mkdir $IMAGES_DIR/nosplit
cp $TOP/common/build/bin/asic/sparse_images/rawprogram0.xml.bak $IMAGES_DIR/rawprogram0.xml
cp $ANDROID_ROOT/out/target/product/la0920/persist.img $IMAGES_DIR/
cp $ANDROID_ROOT/out/target/product/la0920/cache.img $IMAGES_DIR/
cp $ANDROID_ROOT/out/target/product/la0920/system.img $IMAGES_DIR/
cp $ANDROID_ROOT/out/target/product/la0920/userdata.img $IMAGES_DIR/
cp $ANDROID_ROOT/out/target/product/la0920/privdata1.img  $IMAGES_DIR/
#end for not split file
fi

cp $ANDROID_ROOT/out/target/product/la0920/emmc_appsboot.mbn $IMAGES_DIR
cp $TOP/modem_proc/build/ms/bin/8909.genns.prod/mba.mbn $IMAGES_DIR


cp $TOP/boot_images/build/ms/bin/8909/emmc/prog_emmc_firehose_8909_ddr.mbn $IMAGES_DIR
cp $TOP/boot_images/build/ms/bin/8909/emmc/sbl1.mbn $IMAGES_DIR
cp $TOP/trustzone_images/build/ms/bin/MAZAANAA/tz.mbn $IMAGES_DIR
cp $TOP/rpm_proc/build/ms/bin/8909/pm8909/rpm.mbn $IMAGES_DIR

#mkdir $IMAGES_DIR/wincmd
cp -rf $TOP/flashtools/*  $IMAGES_DIR/

#for debug symbol
mkdir -p $IMAGES_DIR/symbol
cp $ANDROID_ROOT/out/target/product/la0920/obj/KERNEL_OBJ/vmlinux  $IMAGES_DIR/symbol
cp $ANDROID_ROOT/out/target/product/la0920/obj/EMMC_BOOTLOADER_OBJ/build-msm8909/lk  $IMAGES_DIR/symbol
cp -rf $TOP/modem_proc/build/ms/* $IMAGES_DIR/symbol

echo "Copy OEM images to the directory:"
echo "$IMAGES_DIR"
