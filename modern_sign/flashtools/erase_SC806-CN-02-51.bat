fastboot flash partition .\SC806_SOCSI\gpt_both0.bin

fastboot flash fsg .\NV\fs_image-SC806-CN02-v4.15.img
fastboot flash modemst1 .\zero.bin
fastboot flash modemst2 .\zero.bin

fastboot flash sbl1 .\sbl1.mbn
fastboot flash rpm  .\rpm.mbn
fastboot flash tz   .\tz.mbn
fastboot flash aboot .\emmc_appsboot.mbn

fastboot flash sbl1bak .\sbl1.mbn
fastboot flash rpmbak  .\rpm.mbn
fastboot flash tzbak   .\tz.mbn
fastboot flash abootbak .\emmc_appsboot.mbn

fastboot flash boot  .\boot.img
fastboot flash splash .\splash.img
#fastboot flash sec  .\sec.dat
fastboot flash modem .\NON-HLOS.bin
fastboot flash system .\system.img
fastboot flash userdata .\userdata.img
fastboot flash persist .\persist.img
fastboot flash cache   .\cache.img
fastboot flash recovery .\recovery.img
