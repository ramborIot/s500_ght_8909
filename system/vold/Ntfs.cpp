/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/mount.h>
#include <sys/wait.h>

#include <linux/kdev_t.h>
//add ++
#include<signal.h>   
//add +
#define LOG_TAG "Vold"

#include <cutils/log.h>
#include <cutils/properties.h>

#include "Ntfs.h"

#include <logwrap/logwrap.h>
#include "VoldUtil.h"

/* add stephen start */
#define NTFS_SUPER_MAGIC 0x5346544e
//static char FSCK_NTFS_PATH[] = "/system/bin/fsck_ntfs";
static char NTFS_FIX_PATH[] = "/system/bin/ntfsfix";

static char MOUNT_NTFS3G_PATH[] = "/system/bin/ntfs-3g";
/* add stephen end */
extern "C" int logwrap(int argc, const char **argv, int background);
//extern "C" int mount(const char *, const char *, const char *, unsigned long, const void *);

int Ntfs::check(const char *fsPath) {
  
    // no NTFS file system check is performed, always return true
    SLOGI("Ntfs filesystem: Skipping fs checks\n");
    return 0;

}



int Ntfs::doMount(const char *fsPath, const char *mountPoint,
                 bool ro, bool remount, bool executable,
                 int ownerUid, int ownerGid, int permMask, bool createLost) {
    int rc;
	int status;
    unsigned long flags;
    char mountData[255];
 //use kernel ntfs 
    flags = MS_NODEV | MS_NOSUID | MS_DIRSYNC;

    flags |= (executable ? 0 : MS_NOEXEC);
    flags |= (ro ? MS_RDONLY : 0);
    flags |= (remount ? MS_REMOUNT : 0);

    // Testing/security, mount ro up to now
   // flags |= MS_RDONLY;
    
    /*
     * Note: This is a temporary hack. If the sampling profiler is enabled,
     * we make the SD card world-writable so any process can write snapshots.
     *
     * TODO: Remove this code once we have a drop box in system_server.
     */
#if 0
    char value[PROPERTY_VALUE_MAX];
    property_get("persist.sampling_profiler", value, "");
    if (value[0] == '1') {
        SLOGW("The SD card is world-writable because the"
            " 'persist.sampling_profiler' system property is set to '1'.");
        permMask = 0;
    }

    sprintf(mountData,
            "uid=%d,gid=%d,fmask=%o,dmask=%o,nls=utf8",
            ownerUid, ownerGid, permMask, permMask);

    rc = mount(fsPath, mountPoint, "ntfs", flags, mountData);

    if (rc && errno == EROFS) {
        SLOGE("%s appears to be a read only filesystem - retrying mount RO", fsPath);
        flags |= MS_RDONLY;
        rc = mount(fsPath, mountPoint, "ntfs", flags, mountData);
    }
/*	
	if (rc == 0 && createLost) {
       	char *lost_path=(char *)malloc(strlen(mountPoint)+100);
				if (lost_path == NULL) {
						return rc;
				}
					
        sprintf(lost_path, "%s/LOST.DIR", mountPoint);	
        if (access(lost_path, F_OK)) {
            if (mkdir(lost_path, 0755)) {
                SLOGE("Unable to create LOST.DIR (%s)", strerror(errno));
            }
        }
        free(lost_path);
    }
*/
#else //using ntfs-3g
 /* according to http://www.tuxera.com/community/ntfs-3g-manual/*/
 /*
	int len = 0;
	if(ro)
		len = sprintf(mountData,"ro,");
	if(len<0 || len>3)
		len = 0;
*/
	SLOGE("mount ntfs block by ntfs-3g %s", fsPath);
/*	
    sprintf(mountData,
                "uid=%d,gid=%d,fmask=%o,dmask=%o,nls=utf8",
                ownerUid, ownerGid, permMask, permMask);
*/	
		
    sprintf(mountData,
          "utf8,uid=%d,gid=%d,fmask=%o,dmask=%o,"
	      "shortname=mixed,nodev,nosuid,dirsync",
            ownerUid, ownerGid, permMask, permMask);
		
	
	if (!executable)
     strcat(mountData, ",noexec");
    if (ro)
        strcat(mountData, ",ro");
    if (remount)
        strcat(mountData, ",remount");

    SLOGD("Mounting ntfs with options:%s\n", mountData);
	
/*	sprintf(mountData+len,
			"noatime,nls=utf8,uid=%d,gid=%d,fmask=%o,dmask=%o",
			ownerUid, ownerGid, permMask, permMask);
*/
	do{
		const char *args[6];
		args[0] = MOUNT_NTFS3G_PATH;
		args[1] = fsPath;
		args[2] = mountPoint;
		args[3] = "-o";
		args[4] = mountData;
		args[5] = NULL;
	//debug ++	
	//debug --
		rc = android_fork_execvp(5,(char **)args, &status, false, true);
		SLOGE("args[0]= %s,args[1]= %s,args[2]= %s,args[3]= %s,args[4]= %s,args[5]= %s",args[0],args[1],args[2],args[3],args[4],args[5]);
		//rc = logwrap(5, args, 1);
		if (rc != 0) {
		/*
			SLOGE("Filesystem mount failed due to logwrap error");
			errno = EIO;
			return -1;
		*/
		SLOGE("%s appears to be a read only filesystem - retrying mount RO", fsPath);
        //flags |= MS_RDONLY;
        //rc = mount(fsPath, mountPoint, "ntfs", flags, "");
		strcat(mountData, ",ro");
		rc = android_fork_execvp(ARRAY_SIZE(args), (char **)args, &status, false,
           true);
		}

		if(!WIFEXITED(status)){
		SLOGE("Filesystem mount did not exit properly");
		errno = EIO;
		return -1;
		}
		
		status = WEXITSTATUS(status);
		if (status == 0){
			SLOGI("NTFS-3g mount completed OK");
			return 0;
		} else {
			SLOGE("NTFS-3g mount failed (unknown exit code %d)",status);
			//errno = EIO;
			//return 0;
			//return -1;
						sprintf(mountData,
									"uid=%d,gid=%d,fmask=%o,dmask=%o",
									 ownerUid, ownerGid, permMask, permMask);
						rc = mount(fsPath, mountPoint, "ntfs", flags, mountData);
						if (rc && errno == EROFS) {
							SLOGE("%s appears to be a read only filesystem - retrying mount RO", fsPath);
									flags |= MS_RDONLY;
										rc = mount(fsPath, mountPoint, "ntfs", flags, mountData);
													}		
									//return 0;
		}
		
	}while(0);

#endif

    return rc;
}

int Ntfs::format(const char *fsPath, unsigned int numSectors) {
    
    SLOGE("Format ntfs filesystem not supported\n");
    errno = EIO;
    return -1;

}