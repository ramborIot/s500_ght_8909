#ifndef _EXFAT_H

#define _EXFAT_H

 

#include <unistd.h>

 

class exFat {

public:

       static int doMount(const char *fsPath, const char *mountPoint,
                 bool ro, bool remount, bool executable,
                 int ownerUid, int ownerGid, int permMask, bool createLost);


       static int unMount(const char *mountPoint);

       static int format(const char *fsPath, unsigned int numSectors);

};

 

#endif
