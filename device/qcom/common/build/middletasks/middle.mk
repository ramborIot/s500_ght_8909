ifdef REMOVE_PACKAGES
  ALL_DEFAULT_INSTALLED_MODULES := \
      $(filter-out $(foreach p,$(REMOVE_PACKAGES),$(p) %/$(p) %/$(p).apk %/$(p).odex %/$(p).so %/$(p).jar), \
          $(ALL_DEFAULT_INSTALLED_MODULES))


  #miss pkgs:when prebuilt has local_overrides,but it was be filter-out
  #so pkgs which be overrides,should be added again
  miss_pkgs := $(filter-out $(REMOVE_PACKAGES),$(strip $(foreach p,$(REMOVE_PACKAGES),$(PACKAGES.$(p).OVERRIDES))))
  $(call expand-required-modules,miss_pkgs,$(miss_pkgs))

  ALL_DEFAULT_INSTALLED_MODULES += $(call module-installed-files,$(miss_pkgs))
endif
